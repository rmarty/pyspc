#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2018  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Fichier Setup pour l'API pyspc
Voir https://docs.python.org/3.6/distutils/setupscript.html
"""
from setuptools import setup, find_packages
import os
import re
import sys

HOME = os.path.abspath(os.path.dirname(__file__))


def find_info(obj, *file_paths):
    """
    Récupérer une information liée à la clé <obj> depuis pyspc/__init__.py
    """
    with open(os.path.join(HOME, *file_paths), 'r', encoding='utf-8') as f:
        text_file = f.read()
    text_match = re.search(
        r"^{} = ['\"]([^'\"]*)['\"]".format(obj), text_file, re.M)
    if text_match:
        return text_match.group(1)
    # Not found case
    raise RuntimeError('Unable to find {} string'.format(obj))


# Get the long description from the relevant file
try:
    with open('DESCRIPTION.rst', 'r', encoding='utf-8') as description:
        LONG_DESCRIPTION = description.read()
except Exception:
    # description is not that important but we want users to execute
    # the setup program within the root directory
    print('Unable to find description in the local directory')
    sys.exit(1)

# Get the requirements
try:
    with open('requirements.txt', 'r', encoding='utf-8') as requirements:
        INSTALL_REQUIRES = requirements.read()
except Exception:
    print('Unable to find requirements in the local directory')
    sys.exit(1)

VERSION = find_info('__version__', 'pyspc', '__init__.py')
AUTHOR = find_info('__author__', 'pyspc', '__init__.py')
EMAIL = find_info('__email__', 'pyspc', '__init__.py')

setup(
    name='pyspc',
    version=VERSION,
    description='Le paquet pySPC est destiné à traiter, convertir'
                ', afficher, analyser des données au format '
                '"AAAAMMJJ[HH];Valeur".',
    long_description=LONG_DESCRIPTION,
    url='https://bitbucket.org/rmarty/pyspc/',
    download_url='https://bitbucket.org/rmarty/pyspc/get/v{}.tar.gz'
        ''.format(VERSION),
    author=AUTHOR,
    author_email=EMAIL,
    platforms=('any',),
    classifiers=[
        # refer to https://pypi.org/pypi?:action=list_classifiers
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: '
            'GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Natural Language :: French',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Database',
        'Topic :: Documentation :: Sphinx',
        'Topic :: Scientific/Engineering :: Atmospheric Science',
        'Topic :: Scientific/Engineering :: Hydrology',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Software Development :: Testing :: Mocking',
        'Topic :: Software Development :: Testing :: Unit',
        'Topic :: Software Development :: Version Control :: Git'],
    keywords=['hydrology', 'time series', 'vigicrues'],
    packages=find_packages(exclude=["bin", "resources", "script", "test"]),
    install_requires=INSTALL_REQUIRES
)
