#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Lire les statistiques exportées depuis HYDRO-2
Documentation : voir doc/html/hydroStat.py
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import glob
import os
import os.path
import sys

# Module PySPC
from pyspc.binutils.args import hydroStat as _args
from pyspc.core.config import Config
import pyspc.core.exception as _exception
from pyspc.metadata.hydro2 import Hydro2

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Lire les statistiques exportées depuis HYDRO-2
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.hydroStat()
    if options.output_filename is not None:
        options.output_filename = Config(filename=options.output_filename)
#        options.output_filename = open(options.output_filename, 'w',
#                                       encoding='utf-8', newline='\n')
    # ==================================================================
    #    2-- LECTURE EXPORT
    # ==================================================================
    _exception.Information(
        options.verbose, "    + Lecture des réponses HYDRO2")
    hydro_filenames = glob.glob(os.path.join(options.input_dir,
                                             options.filename))
    _exception.raise_valueerror(not hydro_filenames,
                                "Aucun fichier HYDRO2")
    for hydro_filename in hydro_filenames:
        _exception.Information(
            options.verbose, "      - Fichier : {}", hydro_filename)
        stat = Hydro2(filename=hydro_filename)
        data = stat.read()
        if not data:
            continue
        if isinstance(options.output_filename, Config):
            options.output_filename.update(data)
        else:
            for s in data:
                for f, v in data[s].items():
                    print('{};{};{}'.format(s, f, v))
    # ==================================================================
    #    4-- FIN DU SCRIPT
    # ==================================================================
    if isinstance(options.output_filename, Config):
        options.output_filename.to_csv(
            filename=options.output_filename.filename,
            sectionname='code', sep=';'
        )
        _exception.Information(
            options.verbose, "    + Export dans le fichier {}",
            options.output_filename.filename)
#            options.output_filename.name)
#        options.output_filename.close()
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
