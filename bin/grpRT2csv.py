#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convertir les prévisions de GRP *Temps Réel* (grp16, grp18)
au format de type csv (grp16, grp18, pyspc)
Documentation : voir doc/html/grpRT2csv.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import sys
import glob
import os
import os.path

# Module PySPC
from pyspc.binutils.args import grpRT2csv as _args
from pyspc.binutils.csv import write_csvlike
from pyspc.binutils.grp import set_reader
import pyspc.core.exception as _exception

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Convertir les prévisions de GRP *Temps Réel* (grp16, grp18)
    au format de type csv (grp16, grp18, pyspc)
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.grpRT2csv()
    reader = set_reader(options.data_type)

    # ==================================================================
    #    2-- TRAITEMENT DES DONNEES
    # ==================================================================
    grp_filenames = glob.glob(os.path.join(
        options.input_dir, options.data_filename))
    _exception.raise_valueerror(not grp_filenames, "Aucun fichier GRP")
    _exception.Information(
        options.verbose, "    + Traitement des données GRP Temps Réel")
    for grp_filename in grp_filenames:
        _exception.Information(
            options.verbose, "      - Lecture du fichier de type '{}' : {}",
            (options.data_type, grp_filename))
        series = reader(filename=grp_filename, datatype=options.data_type)
        filenames = write_csvlike(
            series=series, csvtype=options.csv_type,
            dirname=options.output_dir, overwrite=options.overwrite,
            onefile=options.onefile)
        _exception.Information(options.verbose,
                               "        + Ecriture du fichier : {}", filenames)

    # ==================================================================
    #    3-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTE
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
