#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convertir les données BdAPBP du format *json*
au format de type csv (pyspc)
Documentation : voir doc/html/bdapbpjson2csv.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import glob
import os
import sys

# Module PySPC
from pyspc.binutils.args import bdapbpjson2csv as _args
from pyspc.binutils.csv import write_csvlike
import pyspc.core.exception as _exception
from pyspc import read_BdApbp

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Convertir les données BdAPBP du format *json*
    au format de type csv (pyspc)
    """

    # ===============================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ===============================================================
    options = _args.bdapbpjson2csv()

    # ===============================================================
    #    2-- LECTURE DES DONNEES BdLamedo
    # ===============================================================
    _exception.Information(options.verbose, "    + Lecture des données BdAPBP")
    json_filenames = glob.glob(os.path.join(options.input_dir,
                                            options.json_filename))
    _exception.raise_valueerror(not json_filenames,
                                "Aucun fichier BdAPBP")
    for json_filename in json_filenames:
        _exception.Information(
            options.verbose, "      - Fichier BdAPBP : {}", json_filename)
        if options.domain_name is not None:
            options.domain_name = options.domain_name.split('+')
        series = read_BdApbp(
            filename=json_filename,
            zones=options.domain_name,
            warning=options.warning
        )

    # ===============================================================
    #    3-- EXPORT DES DONNEES CSV
    # ===============================================================
        filenames = write_csvlike(
            series=series, csvtype=options.csv_type,
            dirname=options.output_dir, overwrite=options.overwrite,
            onefile=options.onefile)
        _exception.Information(options.verbose,
                               "        + Ecriture du fichier : {}", filenames)

    # ===============================================================
    #    4-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION DU SCRIPT mf2grp.py
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
