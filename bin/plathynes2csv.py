#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convertir les exports PLATHYNES
au format de type csv (grp16, grp18, pyspc)
Documentation : voir doc/html/plathynes2csv.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import glob
import os
import sys

# Module PySPC
from pyspc.binutils.args import plathynes2csv as _args
from pyspc.binutils.csv import write_csvlike
import pyspc.core.exception as _exception
from pyspc import read_Plathynes

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Convertir les exports PLATHYNES
    au format de type csv (grp16, grp18, pyspc)
    """

    # ===============================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ===============================================================
    options = _args.plathynes2csv()

    # ===============================================================
    #    2-- LECTURE DES DONNEES PLATHYNES
    # ===============================================================
    _exception.Information(options.verbose,
                           "    + Lecture des données PLATHYNES")
    data_filenames = glob.glob(os.path.join(options.input_dir,
                                            options.data_filename))
    _exception.raise_valueerror(not data_filenames,
                                "Aucun fichier PLATHYNES")
    for data_filename in data_filenames:
        _exception.Information(
            options.verbose, "      - Fichier de type '{}' : {}",
            [options.data_type, data_filename], unpacklist=True)
        series = read_Plathynes(
            filename=data_filename,
            datatype=options.data_type,
            warning=options.warning
        )
#        print(series)
    # ===============================================================
    #    3-- EXPORT DES DONNEES CSV
    # ===============================================================
        filenames = write_csvlike(
            series=series, csvtype=options.csv_type,
            dirname=options.output_dir, overwrite=options.overwrite,
            onefile=options.onefile)
        _exception.Information(options.verbose,
                               "        + Ecriture du fichier : {}", filenames)

    # ===============================================================
    #    4-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION DU SCRIPT mf2grp.py
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
