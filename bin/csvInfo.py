#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Informations sur la/les série(s) de type csv (grp16, grp18, pyspc)
Documentation : voir doc/html/csvInfo.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import sys

# Module PySPC
from pyspc.binutils.args import csvInfo as _args
from pyspc.binutils.csv import read_csvlike
from pyspc.binutils.get_stations_list import get_stations_list
from pyspc.binutils.processing.csvInfo import (
    apply_diff, apply_evt, apply_max, apply_mvls, apply_reg)
import pyspc.core.exception as _exception
from pyspc import Config

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Informations sur la/les série(s) de type csv (grp16, grp18, pyspc)
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.csvInfo()
    if options.output_filename is not None and \
            options.processing_method[0] in ['diff']:
        export = open(
            options.output_filename, 'w', encoding='utf-8', newline='\n')
    elif options.output_filename is not None:
        export = Config(filename=options.output_filename)
    else:
        export = None

    # ==================================================================
    #    2-- LISTE DES STATIONS A TRAITER
    # ==================================================================
    stations_list = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)
    _exception.raise_valueerror(not stations_list, "aucune station à traiter")

    # ==================================================================
    #    3-- TRAITEMENT DES DONNEES
    # ==================================================================
    _exception.Information(options.verbose, "    + Lecture des données")
    series = read_csvlike(
        stations=stations_list, varnames=options.varname,
        csvtype=options.csv_type, dirname=options.input_dir,
        warning=options.warning)
    if options.first_dtime is not None and options.last_dtime is not None:
        _exception.Information(
            options.verbose,
            "      - Sous-echantillonnage entre {0} et {1}",
            [options.first_dtime, options.last_dtime], unpacklist=True)
        series.between_dates(
            options.first_dtime, options.last_dtime, inplace=True)

    # ==================================================================
    #    4-- APPLIQUER LES METHODES DE TRAITEMENT
    # ==================================================================
    # --------------------------------------------------------------
    #    4.1 : DIFF
    # --------------------------------------------------------------
    if options.processing_method[0] in ['diff']:
        export = apply_diff(series, options, export)
    # --------------------------------------------------------------
    #    4.2 : EVENEMENTS
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['evt'] and \
            len(options.processing_method) >= 2:
        export = apply_evt(series, options, export)
    # --------------------------------------------------------------
    #    4.3 : MAXIMAS
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['max']:
        export = apply_max(series, options, export)
    # --------------------------------------------------------------
    #    4.4 : COMPLETUDE
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['mvl', 'missing_value',
                                          'mvs', 'missing_value_strip']:
        export = apply_mvls(series, options, export)
    # --------------------------------------------------------------
    #    4.5 : REGIME
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['reg', 'regime']:
        export = apply_reg(series, options, export)
        for e in export:
            _exception.Information(
                options.verbose, "    + Export dans le fichier {}", e)
    # --------------------------------------------------------------
    #    4.0 : AUTRE
    # --------------------------------------------------------------
    else:
        raise ValueError(
            "méthode de traitement '{0}' inconnue "
            "et/ou Nombre de paramètre(s) incorrect '{1}'".format(
                options.processing_method[0],
                len(options.processing_method)))

    # ==================================================================
    #    5-- FIN DU SCRIPT
    # ==================================================================
    if (isinstance(export, Config)
            and options.processing_method[0] not in ['reg', 'regime']):
        export.to_csv(filename=export.filename, sectionname='id', sep=';')
        _exception.Information(
            options.verbose,
            "    + Export dans le fichier {}", export.filename)
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
