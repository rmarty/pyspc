#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convertir les prévisions de GRP *Temps Réel*
au format prv Scores / prv OTAMIN
Documentation : voir doc/html/grpRT2prv.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
from datetime import datetime as dt
import glob
import os
import os.path
import pytz
import sys

# Module PySPC
from pyspc.binutils.args import grpRT2prv as _args
from pyspc.binutils.grp import set_reader, set_grprt_reader
from pyspc import Series

import pyspc.core.exception as _exception

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Convertir les prévisions de GRP *Temps Réel*
    au format prv Scores / prv OTAMIN
    """
    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.grpRT2prv()
    timezone = pytz.timezone('Europe/Paris').localize(dt.now()).tzinfo
    reader = set_reader(options.data_type[0])
    grprt_reader = set_grprt_reader(options.data_type[0])
    if options.codepom is not None:
        options.codepom = dict(options.codepom)

    # ==================================================================
    #    2-- TRAITEMENT DES OBSERVATIONS
    # ==================================================================
    dtderobs, dtprod = set_dtderobs(options, grprt_reader, reader, timezone)

    # ==================================================================
    #    3-- TRAITEMENT DES PREVISIONS
    # ==================================================================
    fcst_series, dtprod = set_fcst_series(
        options, grprt_reader, reader, dtderobs, dtprod, timezone)
    dtderobs = {k[0]: k[2][0] for k in fcst_series.keys()}

    # ==================================================================
    #    4-- EXPORT PRV SCORES / OTAMIN
    # ==================================================================
    basename = "_".join(["GRP", "B", dtprod.strftime("%Y%m%d_%H%M")])
    if 'otamin' in options.data_type[1]:
        basename += ".prv"
    else:
        basename += ".csv"
    filename = fcst_series.to_prv(
        dirname=options.output_dir,
        datatype=options.data_type[1],
        basename=basename
    )
    _exception.Information(
        options.verbose,
        "    + Ecriture du fichier prv de type {} : {}",
        (options.data_type[1], filename))

    # ==================================================================
    #    5-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


def set_dtderobs(options, grprt_reader, reader, timezone):
    """Définir les dates de dernière observation et de production"""
    datatype = options.data_type[0].replace('fcst', 'obs')
    subtype = '_'.join(datatype.split('_')[2:])
    filename = os.path.join(
        options.input_dir,
        '{}.txt'.format(grprt_reader.get_fileprefix(datatype=subtype)))
    _exception.Information(
        options.verbose,
        "    + Traitement des observations GRP Temps Réel : {}", datatype)
    _exception.Information(
        options.verbose,
        "      - Fichier GRP : {}", filename)
    obs_series = reader(filename=filename, datatype=datatype)
    if not obs_series:
        return {}, None
    obs_series.stripna(inplace=True)
    dtderobs = {k[0]: s.lastdt
                for k, s in obs_series.items() if k[1].startswith('Q')}
    dtprod = dt.fromtimestamp(os.path.getmtime(filename))
    dtprod = dtprod.replace(tzinfo=timezone)
    dtprod = dtprod.astimezone(pytz.utc)
    _exception.Information(
        options.verbose,
        "      - Date dernière obs à {} : {}", list(dtderobs.items()))
    _exception.Information(
        options.verbose,
        "      - Instant de production de la prévision : {}", str(dtprod))
    return dtderobs, dtprod


def set_fcst_series(options, grprt_reader, reader, dtderobs, dtprod, timezone):
    """Créer la collections des prévisions"""
    fcst_series = Series(datatype='fcst', name='GRPRT_Fcst')
    _exception.Information(
        options.verbose,
        "    + Traitement des prévisions GRP Temps Réel : {}",
        options.data_type[0])

    subtype = '_'.join(options.data_type[0].split('_')[2:])
    filenames = glob.glob(os.path.join(
        options.input_dir,
        '{}*'.format(grprt_reader.get_fileprefix(datatype=subtype))))
    for filename in filenames:
        _exception.Information(
            options.verbose,
            "      - Fichier GRP : {}", filename)
        series = reader(filename=filename, datatype=options.data_type[0])
        for k, s in series.items():
            if not k[1].startswith('Q'):
                continue
            meta = list(k[2])
            meta[0] = dtderobs.get(k[0], meta[0])
            if options.codepom is not None:
                meta[1] = options.codepom.get('Previ', meta[1])
            meta = tuple(meta)
            _exception.Information(
                options.verbose,
                "      - Mise-à-jour des méta-données de la prévision à {}",
                k[0])
            _exception.Information(
                options.verbose, "        + originales : {}",  str(k[2]))
            _exception.Information(
                options.verbose, "        + finales    : {}", str(meta))
            fcst_series.add(
                code=k[0],
                serie=s,
                meta=meta
            )
    if dtprod is not None:
        return fcst_series, dtprod
    dtprod = dt.fromtimestamp(max([os.path.getmtime(f) for f in filenames]))
    dtprod = dtprod.replace(tzinfo=timezone)
    dtprod = dtprod.astimezone(pytz.utc)
    _exception.Information(
        options.verbose,
        "      - Instant de production de la prévision : {}", str(dtprod))
    return fcst_series, dtprod


# -------------------------------------------------------------------
#      EXECUTE
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
