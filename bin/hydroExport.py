#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Créer les procédures d'export Hydro2
Documentation : voir doc/html/hydroExport.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import os.path
import sys

# Module PySPC
from pyspc.binutils.args import hydroExport as _args
from pyspc.binutils.get_stations_list import get_stations_list
import pyspc.core.exception as _exception
from pyspc.core.timeutil import str2dt
from pyspc.webservice.hydro2 import Export

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Créer les procédures d'export Hydro2
    Documentation : voir doc/html/hydroExport.html
    """
    # ===============================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ===============================================================
    options = _args.hydroExport()

    # ==================================================================
    #    2-- LISTE DES STATIONS A TRAITER
    # ==================================================================
    stations_list = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)
    _exception.raise_valueerror(
        not stations_list, "aucune station à traiter"
    )
    if options.station_name is not None:
        prefix = options.station_name
    else:
        prefix = os.path.splitext(
            os.path.basename(options.stations_list_file))[0]

    # ===============================================================
    #    3-- HYDRO2 EXPORT
    # ===============================================================
    _exception.Information(
        options.verbose, "    + Initialisation Export Hydro2")
    first_dt = str2dt(options.first_dtime)
    last_dt = str2dt(options.last_dtime)
    precision, onefile = process_options_user(options.user)
    hydro2 = Export(stations=stations_list, datatype=options.data_type,
                    first_dt=first_dt, last_dt=last_dt, precision=precision)
    hydro2.set_export(onefile=onefile)
    if options.data_type in ['H-TEMPS', 'QTFIX', 'QTVAR', 'QJM',
                             'DEBCLA', 'CRUCAL', 'TOUSMOIS']:
        filename = os.path.join(
            options.output_dir,
            '_'.join([
                prefix,
                options.data_type,
                first_dt.strftime('%Y%m%d%H%M'),
                last_dt.strftime('%Y%m%d%H%M')
            ]) + '.txt'
        )
    else:
        filename = os.path.join(
            options.output_dir,
            '_'.join([
                prefix,
                options.data_type
            ]) + '.txt'
        )
    msg = hydro2.write(filename=filename)
    if msg is not None:
        _exception.Information(
            options.verbose, "    + Procédures d'export Hydro2 : {}", filename)
    else:
        _exception.Warning(__name__, "Aucun export Hydro2")

    # ===============================================================
    #    4-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


def process_options_user(options):
    """
    Traiter les éléments de configuration utilisateur
    """
    precision = None
    onefile = None
    if options is not None:
        for u in options:
            if u[0] == 'precision':
                precision = int(float(u[1]))
            elif u[0] == 'onefile':
                onefile = u[1]
    return precision, onefile


# -------------------------------------------------------------------
#      EXECUTION DU SCRIPT mf2grp.py
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
