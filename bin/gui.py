#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2018  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Lancer l'interface graphique du module <pySPC>
Documentation : gui.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import ctypes
# import os.path
import sys

# Module PyQt5
# from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication

# Module pyspc
from pyspc.binutils.gui.gui_pyspc import MainWindow_pySPC
import pyspc.core.exception as _exception


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Lancer l'interface graphique du module <pySPC>
    """
    # ===============================================================
    #    0-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ===============================================================
    myappid = 'mycompany.myproduct.subproduct.version'  # arbitrary string
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
    # PYTHON 3 requis
    if sys.version_info[0] < 3:
        raise _exception.Error("Le module pyspc fonctionne uniquement"
                               " avec la version 3 (et +) de PYTHON")
    # ===============================================================
    #    1-- LANCEMENT INTERFACE GRAPHIQUE
    # ===============================================================
    app = QApplication(sys.argv)

#    # Francisation des boîtes de dialogue (sous Windows)
#    if sys.platform.startswith("win"):
#        qlocale = QtCore.QLocale.system().name()
#        qtranslator = QtCore.QTranslator()
#        translationpath = os.path.join(sys.prefix, "Lib", "site-packages",
#                                       "PyQt4", "translations")
#        qtranslator.load("qt_{}".format(qlocale[:2]), translationpath)
#        app.installTranslator(qtranslator)
#    # Francisation des boîtes de dialogue (sous Linux)
#    elif sys.platform.startswith("linux"):
#        qlocale = QtCore.QLocale.system().name()
#        qtranslator = QtCore.QTranslator()
#        qtranslator.load("qt_{}".format(qlocale[:2]),
#                         QtCore.QLibraryInfo.location(
#                             QtCore.QLibraryInfo.TranslationsPath))
#        app.installTranslator(qtranslator)

    mw = MainWindow_pySPC()
    mw.show()
    sys.exit(app.exec_())


# -------------------------------------------------------------------
#      EXECUTION DU SCRIPT pySPC.py
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
