#!/usr/bin/python3
# -*- coding: utf-8 -*-
#########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Dupliquer la configuration GRP Temps Réel avec modifications autorisees
selon les options choisies par l'utilisateur
Documentation : voir doc/html/duplicateGrpRTCfg.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import sys
import os.path

# Module PySPC
from pyspc.binutils.args import duplicateGrpRTCfg as _args
from pyspc.model.grp16 import GRPRT_Cfg as GRPRT16_Cfg
from pyspc.model.grp18 import GRPRT_Cfg as GRPRT18_Cfg
from pyspc.model.grp20 import GRPRT_Cfg as GRPRT20_Cfg
import pyspc.core.exception as _exception

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Dupliquer la configuration GRP Temps Réel avec modifications autorisees
    selon les options choisies par l'utilisateur
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.duplicateGrpRTCfg()
    if options.update_cfg is not None and options.data_type == 'grp20':
        cfg_to_update = {tuple(val[0].split('_')): val[1].replace('"', '')
                         for val in options.update_cfg}
    elif options.update_cfg is not None:
        cfg_to_update = {val[0]: val[1].replace('"', '')
                         for val in options.update_cfg}
    else:
        cfg_to_update = {}

    # ===============================================================
    #    2-- LECTURE DU FICHIER DE CONFIGURATION
    # ===============================================================
    filename = os.path.join(options.input_dir, options.duplicata_src)
    config = set_reader(version=options.data_type)(filename=filename)
    _exception.Information(
        options.verbose,
        "    + Lecture du fichier de configuration 'source' : {}")
    if options.data_type == 'grp20':
        config.read(encoding='iso-8859-15')
    else:
        config.read()

    # ===============================================================
    #    3-- MODIFICATION DE LA CONFIGURATION
    # ===============================================================
    _exception.Information(options.verbose, "    + Mise à jour")
    config.update_config(cfg_to_update)

    # ===============================================================
    #    4-- ECRITURE DE LA NOUVELLE CONFIGURATION
    # ===============================================================
    config.filename = os.path.join(options.output_dir, options.cfg_filename)
    _exception.Information(
        options.verbose,
        "    + Écriture de la configuration mise à jour : {}",
        config.filename)
    config.write()

    # ===============================================================
    #    5-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


def set_reader(version):
    """
    Définir le lecteur en fonction de la version de grp
    """
    if version == 'grp16':
        return GRPRT16_Cfg
    if version == 'grp18':
        return GRPRT18_Cfg
    if version == 'grp20':
        return GRPRT20_Cfg
    raise ValueError("Version '{}' inconnue".format(version))


# -------------------------------------------------------------------
#      EXECUTION DU SCRIPT duplicateGrpRTCfg.py
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
