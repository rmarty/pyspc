#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Comparer les pointes de crues entre deux sites
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import os.path
import sys

# Module PySPC
from pyspc.binutils.args import comparePeakFlow as _args
from pyspc.binutils.processing.comparePeakFlow import (
    assoc_peakflows, read_events)
import pyspc.core.exception as _exception
from pyspc.plotting.peakflow import plot_peakflow_analysis
from pyspc import Parameter

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Comparer les pointes de crues entre deux sites
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.comparePeakFlow()
    param = Parameter(varname=options.varname)

    # ==================================================================
    #    2-- LECTURE DES EVENEMENTS AMONT/AVAL
    # ==================================================================
    _exception.Information(
        options.verbose, "    + Lecture des événements")

    upfilename = os.path.join(options.input_dir, options.upfile)
    _exception.Information(
        options.verbose, "      - Fichier amont : {}", upfilename)
    updata, upsta = read_events(upfilename)

    dnfilename = os.path.join(options.input_dir, options.downfile)
    _exception.Information(
        options.verbose, "      - Fichier aval : {}", dnfilename)
    dndata, dnsta = read_events(dnfilename)

    # ==================================================================
    #    3-- ASSOCIATION DES POINTES DE CRUE
    # ==================================================================
    assoc = assoc_peakflows(updata, dndata, options)

    # ==================================================================
    #    4-- EXPORT CSV
    # ==================================================================
    csv_filename = os.path.join(
        options.output_dir,
        'peakflow_{}-{}.csv'.format(upsta, dnsta)
    )
    _exception.Information(
        options.verbose, "    + Export csv : {}", csv_filename)
    with open(csv_filename, 'w', encoding='utf-8', newline='\n') as f:
        f.write('maxdt_{0};max_{0};maxdt_{1};max_{1};maxdt_delta;max_delta\n'
                ''.format(upsta, dnsta))
        for a in assoc:
            f.write('{upt};{upm};{dnt};{dnm};{deltat};{deltam}\n'.format(
                upt=a[0].strftime(param.dtfmt),
                dnt=a[1].strftime(param.dtfmt),
                upm=assoc[a][0],
                dnm=assoc[a][1],
                deltat=int((a[1] - a[0]) / param.timestep),
                deltam=assoc[a][1]-assoc[a][0]
            ))

    # ==================================================================
    #    5-- EXPORT PNG
    # ==================================================================
    png_filename = os.path.join(
        options.output_dir,
        'peakflow_{}-{}.png'.format(upsta, dnsta)
    )
    _exception.Information(
        options.verbose, "    + Export png : {}", png_filename)
    plot_peakflow_analysis(
        data=assoc, fig_filename=png_filename,
        upsta=upsta, downsta=dnsta, param=param)

    # ==================================================================
    #    6-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
