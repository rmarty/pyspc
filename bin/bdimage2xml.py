#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Extraire les données BdImage et les stocker au format *XML* de BdImage.
Documentation : voir doc/html/bdimage2xml.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import sys

# Module PySPC
from pyspc.binutils.args import bdimage2xml as _args
from pyspc.convention.lamedo import BDIMAGE_PREFIX
from pyspc.core.parameter import Parameter
import pyspc.core.exception as _exception
from pyspc.core.timeutil import str2dt
from pyspc.webservice.lamedo import BdImage

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Extraire les données BdImage et les stocker au format *XML* de BdImage.
    """
    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.bdimage2xml()
    image = tuple(options.data_type)
    options.first_dtime = str2dt(options.first_dtime)
    options.last_dtime = str2dt(options.last_dtime)
    options.runtime = str2dt(options.runtime)
    stats, precision = process_options_user(options)
    _exception.raise_valueerror(
        BDIMAGE_PREFIX.get('_'.join(image), None) != options.varname[0],
        "Incohérence entre l'image {} et la grandeur {}"
        "".format(image, options.varname)
    )

    # ==================================================================
    #    2-- TRAITEMENT DE LA REQUETE BDLAMEDO
    # ==================================================================
    _exception.Information(
        options.verbose,
        "    + Lancement du téléchargement des données BdImages")
    bdimages = BdImage(timeout=300)
    filenames = bdimages.retrieve(
        dirname=options.output_dir,
        image=image,
        tdelta=Parameter(options.varname).timestep,
        first_dtime=options.first_dtime,
        last_dtime=options.last_dtime,
        runtime=options.runtime,
        domains=options.spatial_domain,
        stats=stats,
        precision=precision
    )
    _exception.Information(
        options.verbose, "      - Ecriture du fichier : {}", filenames)

    # ==================================================================
    #    3-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


def process_options_user(options):
    """
    Traiter les éléments de configuration utilisateur
    """
    stats = None
    precision = None
    if options.update_element is not None:
        for u in options.update_element:
            if u[0] == 'stats':
                stats = u[1]
            elif u[0] == 'precision':
                precision = u[1]
    return stats, precision


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
