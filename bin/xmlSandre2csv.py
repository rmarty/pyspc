#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convertir les donnees XML-Sandre au format lu par GRP
Documentation : voir doc/html/xmlSandre2csv.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import glob
import os
import sys

# Module PySPC
from pyspc.binutils.args import xmlSandre2csv as _args
from pyspc.binutils.csv import write_csvlike
from pyspc.binutils.get_stations_list import get_stations_list
import pyspc.core.exception as _exception
from pyspc import read_Sandre

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Convertir les données XML-Sandre
    au format *csv* des données de GRP *Calage*
    """
    # ===============================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ===============================================================
    options = _args.xmlSandre2csv()
    stations_list = set_stations(options)
    models_list, scens_list = set_modscen(options)

    # ===============================================================
    #    2-- LECTURE DES DONNEES XML
    # ===============================================================
    _exception.Information(options.verbose,
                           "    + Lecture des données XML Sandre")
    xml_filenames = glob.glob(os.path.join(options.input_dir,
                                           options.xml_filename))
    _exception.raise_valueerror(not xml_filenames, "Aucun fichier xml")
    for xml_filename in xml_filenames:
        _exception.Information(
            options.verbose, "      - Lecture du fichier de type '{}' : {}",
            [options.data_type, xml_filename], unpacklist=True)
        series = read_Sandre(
            filename=xml_filename,
            datatype=options.data_type,
            codes=stations_list,
            models=models_list,
            scens=scens_list,
            warning=options.warning
        )

    # ===============================================================
    #    3-- EXPORT DES DONNEES CSV
    # ===============================================================
        filenames = write_csvlike(
            series=series, csvtype=options.csv_type,
            dirname=options.output_dir, overwrite=options.overwrite,
            onefile=options.onefile)
        _exception.Information(options.verbose,
                               "        + Ecriture du fichier : {}", filenames)

    # ===============================================================
    #    4-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


def set_stations(options):
    """Définir les listes des stations à extraire"""
    if options.station_name is None and options.stations_list_file is None:
        return None
    return get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)


def set_modscen(options):
    """Définir les listes des modèles et scénarios à extraire"""
    if options.modscen is None:
        return None, None
    models_list = [x[1] for x in options.modscen if x[0] == 'model']
    scens_list = [x[1] for x in options.modscen if x[0] == 'scen']
    return (models_list if models_list else None,
            scens_list if scens_list else None)


# -------------------------------------------------------------------
#      EXECUTION DU SCRIPT xmlSandre2csv.py
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
