#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Traiter les données format de type csv (grp16, grp18, pyspc),
selon une méthode choisie
Documentation : voir doc/html/csv2csv.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import os
import sys

# Module PySPC
from pyspc.binutils.args import csv2csv as _args
from pyspc.binutils.csv import read_csvlike, write_csvlike
from pyspc.binutils.get_stations_list import get_stations_list
from pyspc.core.timeutil import str2dt
import pyspc.core.exception as _exception
from pyspc import Series

# Traitements
from pyspc.binutils.processing.csv2csv.computation import apply_ath, apply_bth
from pyspc.binutils.processing.csv2csv.modeling import (
    apply_etp, apply_f2s, apply_res, apply_rtc, apply_s2f)
from pyspc.binutils.processing.csv2csv.reindexing import apply_bwd, apply_cat
from pyspc.binutils.processing.csv2csv.scaling import (
    apply_dsc, apply_nsc, apply_rsc, apply_shs, apply_usc)
from pyspc.binutils.processing.csv2csv.statistics import apply_pct, apply_wav
from pyspc.binutils.processing.csv2csv.timeserie import (
    apply_fct, apply_fli, apply_lrg, apply_tlg)

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Traiter les données format de type csv (grp16, grp18, pyspc),
    selon une méthode choisie
    """
    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.csv2csv()

    # ==================================================================
    #    2-- LISTE DES STATIONS A TRAITER
    # ==================================================================
    stations_list = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)
    _exception.raise_valueerror(not stations_list, "aucune station à traiter")
    # --------------------------------------------------------------
    #    Pré-Traitement particulier : CCD et CPY
    # --------------------------------------------------------------
    new_codes = None
    if options.processing_method[0] in ['ccd', 'changeCode',
                                        'cpy', 'copyYear']:
        new_codes = set_io_assoc(options, stations_list)
    # --------------------------------------------------------------
    #    Pré-Traitement particulier : WAV
    # --------------------------------------------------------------
    if options.processing_method[0] in ['wav', 'weighted_average']:
        weights = set_weights_assoc(options, stations_list)

    # ==================================================================
    #    3-- LECTURE DES DONNEES
    # ==================================================================
    _exception.Information(options.verbose, "    + Lecture des données")
    series = read_csvlike(
        stations=stations_list,
        varnames=options.varname,
        csvtype=options.csv_type[0],
        dirname=options.input_dir,
        warning=options.warning
    )
    if series is None:
        raise ValueError("Aucune série à traiter")

    # ==================================================================
    #    4-- APPLIQUER LES METHODES DE TRAITEMENT
    # ==================================================================
    if options.stations_list_file is not None:
        series.name = os.path.splitext(
            os.path.basename(options.stations_list_file))[0]
    _exception.Information(options.verbose, "    + Traitement des données")
    # --------------------------------------------------------------
    #    4.1 : SUP-SEUIL
    # --------------------------------------------------------------
    if options.processing_method[0] in ['ath', 'aboveThreshold'] and \
            len(options.processing_method) > 1:
        series = apply_ath(series, options)
    # --------------------------------------------------------------
    #    4.2 : SUB-SEUIL
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['bth', 'belowThreshold'] and \
            len(options.processing_method) > 1:
        series = apply_bth(series, options)
    # --------------------------------------------------------------
    #    4.3 : EXTRACTION ENTRE 2 DATES
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['bwd', 'betweenDates'] and \
            len(options.processing_method) > 2:
        series = apply_bwd(series, options)
    # --------------------------------------------------------------
    #    4.4 : CONCATENATION DE SERIES
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['cat', 'concatData']:
        series = apply_cat(series, options)
    # --------------------------------------------------------------
    #    4.5 : RE-DEFINIR L'ID DE SERIES DE DONNEES
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['ccd', 'changeCode'] and \
            len(options.processing_method) == 2:
        series = apply_ccd(series, options, new_codes)
    # --------------------------------------------------------------
    #    4.6 : DUPLIQUER UN BLOC ANNUEL
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['cpy', 'copyYear'] and \
            len(options.processing_method) == 5:
        series = apply_cpy(series, options, new_codes)
    # --------------------------------------------------------------
    #    4.7 : DESAGREGATION
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['dsc', 'downscale'] and \
            len(options.processing_method) > 2:
        series = apply_dsc(series, options)
    # --------------------------------------------------------------
    #    4.8 : EVAPO-TRANSPIRATION
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['etp', 'EvapoTransPot'] and \
            len(options.processing_method) > 2 and \
            options.varname in ['TH', 'TJ']:
        series = apply_etp(series, options)
    # --------------------------------------------------------------
    #    4.9 : PREVISIONS VERS SIMULATION
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['f2s', 'fcst2sim'] and \
            len(options.processing_method) > 1:
        series = apply_f2s(series, options)
    # --------------------------------------------------------------
    #    4.10 : FILL CONSTANT
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['fct', 'fillConstant'] and \
            len(options.processing_method) > 1:
        series = apply_fct(series, options)
    # --------------------------------------------------------------
    #    4.11 : FILL LINEAR INTERPOLATION
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['fli', 'fillLinInterp'] and \
            len(options.processing_method) == 1:
        series = apply_fli(series, options)
    # --------------------------------------------------------------
    #    4.12 : REGRESSION LINEAIRE AVEC DECALAGE TEMPOREL
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['lrg', 'linearReg'] and \
            len(options.processing_method) == 4:
        series = apply_lrg(series, options)
    # --------------------------------------------------------------
    #    4.13 : REMISE A UNE ECHELLE TRES PROCHE
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['nsc', 'nearlyequalscale'] and \
            len(options.processing_method) > 2:
        series = apply_nsc(series, options)
    # --------------------------------------------------------------
    #    4.14 : PERCENTILE
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['pct', 'percentile'] and \
            len(options.processing_method) > 1:
        series = apply_pct(series, options)
    # --------------------------------------------------------------
    #    4.15 : CONVERSION PAR RESERVOIR
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['res', 'reservoir'] and \
            len(options.processing_method) > 1:
        series = apply_res(series, options)
    # --------------------------------------------------------------
    #    4.16 : INTERPOLATION LINEAIRE INSTANTANE => HORAIRE
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['rsc', 'regularscale'] and \
            len(options.processing_method) == 1:
        series = apply_rsc(series, options)
    # --------------------------------------------------------------
    #    4.17 : RATING CURVE APPLICATION
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['rtc', 'ratingCurve'] and \
            len(options.processing_method) in [2, 3, 4]:
        series = apply_rtc(series, options)
    # --------------------------------------------------------------
    #    4.18 : SIMULATIONS VERS PREVISION
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['s2f', 'sim2fcst'] and \
            len(options.processing_method) > 2:
        series = apply_s2f(series, options)
    # --------------------------------------------------------------
    #    4.19 : HORAIRE => INFRA-HORAIRE
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['shs', 'subhourlyscale'] and \
            len(options.processing_method) > 1:
        series = apply_shs(series, options)
    # --------------------------------------------------------------
    #    4.20 : DECALAGE TEMPOREL
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['tlg', 'timelag'] and \
            len(options.processing_method) > 1:
        series = apply_tlg(series, options)
    # --------------------------------------------------------------
    #    4.21 : AGREGATION
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['usc', 'upscale'] and \
            len(options.processing_method) > 3:
        series = apply_usc(series, options)
    # --------------------------------------------------------------
    #    4.22 : SOMME PONDEREE
    # --------------------------------------------------------------
    elif options.processing_method[0] in ['wav', 'weighted_average']:
        series = apply_wav(series, options, weights)
    # --------------------------------------------------------------
    #    4.0 : AUTRE
    # --------------------------------------------------------------
    else:
        raise ValueError(
            "méthode de traitement '{0}' inconnue "
            "et/ou Nombre de paramètre(s) incorrect '{1}'".format(
                options.processing_method[0],
                len(options.processing_method)))

    # ===============================================================
    #    5-- EXPORT DES DONNEES CSV
    # ===============================================================
    filenames = write_csvlike(
        series=series, csvtype=options.csv_type[1],
        dirname=options.output_dir, overwrite=options.overwrite,
        onefile=options.onefile)
    _exception.Information(
        options.verbose, "      - Ecriture du fichier : {}", filenames)

    # ==================================================================
    #    6-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


def apply_ccd(series, options, new_codes):
    """RE-DEFINIR L'ID DE SERIES DE DONNEES"""
    _exception.Information(
        options.verbose, "      - Re-définir l'identifiant de la série")
    new_series = Series(datatype=series.datatype)
    for serie in series.values():
        try:
            new_code = new_codes[serie.code]
        except KeyError:
            _exception.Warning(
                sys.argv[0],
                "Nouvel identifiant de la série {} inconnu"
                "".format(serie.code)
                )
            continue
        serie.code = new_code
        new_series.add(serie=serie)
    return new_series


def apply_cpy(series, options, new_codes):
    """ DUPLIQUER UN BLOC ANNUEL"""
    # Analyse des paramètres temporels
    year = int(options.processing_method[2])
    start = str2dt(options.processing_method[3])
    end = str2dt(options.processing_method[4])
    _exception.Information(
        options.verbose,
        "      - Dupliquer un bloc de données de l'année {} pour créer des "
        "données entre {} et {}", (year, start, end))
    new_series = Series(datatype=series.datatype)
    for serie in series.values():
        if serie.code not in new_codes:
            _exception.Warning(
                sys.argv[0],
                "Nouvel identifiant de la série {} inconnu. "
                "Aucune duplication".format(serie.code)
            )
            continue
        src_code = new_codes[serie.code]
        src_series = read_csvlike(
            stations=src_code,
            varnames=options.varname,
            csvtype=options.csv_type[0],
            dirname=options.input_dir,
            warning=options.warning
        )
        src_serie = list(src_series.values())[0]
        serie.copy_year(src=src_serie, year=year, start=start, end=end)
        _exception.Information(
            options.verbose, "          - Série : {} -> {}",
            (serie.code, src_code))
        new_series.add(serie=serie)
    return new_series


def set_io_assoc(options, stations_list):
    """
    Pré-Traitement particulier : CCD et CPY
    Définir la table d'association des codes in/out
    """
    if len(stations_list) == 1:
        new_codes = {stations_list[0]: options.processing_method[1]}
    else:
        new_codes = {}
        with open(options.processing_method[1], 'r', encoding="utf-8") as f:
            codes = [line.replace("\n", "").strip() for line in f.readlines()]
        for ks, kc in enumerate(codes):
            new_codes[stations_list[ks]] = kc
    return new_codes


def set_weights_assoc(options, stations_list):
    """
    Pré-Traitement particulier : WAV
    Définir la table d'association des pondérations in/weight
    """
    weights = {}
    filename = None
    if len(options.processing_method) > 1:
        _exception.Information(
            options.verbose, "    + Lecture de la pondération")
        filename = options.processing_method[1]
        with open(filename, 'r', encoding="utf-8") as f:
            w = [float(line.replace("\n", "").strip())
                 for line in f.readlines()]
        for ks, kw in enumerate(w):
            weights[(stations_list[ks], options.varname, None)] = kw
        _exception.raise_valueerror(
            len(stations_list) != len(weights),
            'La pondération est incomplète'
        )
    elif len(options.processing_method) == 1:
        _exception.Information(
            options.verbose, "    + Pondération uniforme")
        enssize = len(stations_list)
        for station in stations_list:
            weights[(station, options.varname, None)] = 1 / enssize
    return weights


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
