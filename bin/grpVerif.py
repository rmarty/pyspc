#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Récupérer les performances des modèles GRP (Fiches de Performance)
Documentation : voir doc/html/grpVerif.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import collections
import glob
import numpy as np
import os
import os.path
import sys

# Module PySPC
from pyspc.binutils.args import grpVerif as _args
from pyspc.binutils.get_stations_list import get_stations_list
import pyspc.core.exception as _exception
from pyspc.model.grp16 import GRP_Verif as GRP16_Verif
from pyspc.model.grp18 import GRP_Verif as GRP18_Verif

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Récupérer les performances des modèles GRP (Fiches de Performance)
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.grpVerif()
    if options.output_filename is not None:
        options.output_filename = open(options.output_filename, 'w',
                                       encoding='utf-8', newline='\n')

    # ==================================================================
    #    2-- LISTE DES STATIONS A TRAITER
    # ==================================================================
    stations_list = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)
    _exception.raise_valueerror(
        not stations_list, "aucune station à traiter"
    )

    # ==================================================================
    #    3-- TRAITEMENT DES DONNEES PAR STATION
    # ==================================================================
    _exception.Information(
        options.verbose, "    + Traitement des fiches de performance")
    # ----------------------------------------------------------
    #    INITIALISATION
    # ----------------------------------------------------------
    x = options.data_type.split('_')
    grp_version = x[0]
    grp_dtype = x[1]
    scores = collections.OrderedDict()
    names = ['Eff', 'POD', 'FAR', 'CSI']
    header = ['Code', 'SC', 'HC', 'SV']
    # ----------------------------------------------------------
    #    LECTURE FICHER PERF
    # ----------------------------------------------------------
    for station in stations_list:
        _exception.Information(
            options.verbose, "      - Station : {}", station)
        pattern = set_pattern(grp_version, grp_dtype, station)
        filenames = glob.glob(os.path.join(options.input_dir, pattern))
        for filename in filenames:
            _exception.Information(
                options.verbose, "        + Fiche de performance : {0}",
                os.path.basename(filename))
            reader = set_reader(grp_version, grp_dtype, filename)
            content = reader.read()
            key = set_key(reader, grp_version)
            scores.update(set_unique_key(key, scores, options, content))

    # ==================================================================
    #    4-- EXPORT VERS STDOUT
    # ==================================================================
    _exception.Information(
        options.verbose, "    + Récapitulatif des performances")
    cfgs = list({c for s in scores for c in scores[s]})
    cfgs.sort()
    header.extend(['{}_{}'.format(n, c) for n in names for c in cfgs])
    if options.output_filename is not None:
        options.output_filename.write(';'.join(header))
        options.output_filename.write('\n')
    else:
        print(';'.join(header))
    for code in scores:
        line = set_line(header, code, scores, names)
        if options.output_filename is not None:
            options.output_filename.write(';'.join(line))
            options.output_filename.write('\n')
        else:
            print(';'.join(line))
    if options.output_filename is not None:
        _exception.Information(
            options.verbose, "    + Export dans le fichier {}",
            options.output_filename.name)
        options.output_filename.close()

    # ==================================================================
    #    5-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


def set_pattern(version, datatype, station):
    """
    Définir le pattern de recherche des fiches de calage
    """
    if version == 'grp16' and datatype == 'cal':
        return '*_FichePerf_{}*.pdf'.format(station)
    if version == 'grp16' and datatype == 'rtime':
        return '*_FichePerf_calage_complet_{}*.pdf'.format(station)
    if version == 'grp18' and datatype == 'cal':
        return '*Perf_TESTS_GRP_{}*.DAT'.format(station)
    if version == 'grp18' and datatype == 'rtime':
        return '*Perf_CALAG_GRP_{}*.DAT'.format(station)
    raise ValueError("Version '{}' et/ou type de fiche '{}' inconnu"
                     "".format(version, datatype))


def set_reader(version, datatype, filename):
    """
    Définir le lecteur selon la version de GRP
    """
    if version == 'grp16':
        return GRP16_Verif(filename=filename, datatype=datatype)
    if version == 'grp18':
        return GRP18_Verif(filename=filename)
    raise ValueError("Version '{}' inconnue".format(version))


def set_key(reader, version):
    """
    Définir la clé primaire selon la version de GRP
    """
    if version == 'grp16':
        return [reader.loc, reader.sc, reader.hc, reader.sv]
    if version == 'grp18':
        return [reader.loc + '_' + reader.timestep,
                '', reader.leadtime, reader.threshold]
    raise ValueError("Version '{}' inconnue".format(version))


def set_unique_key(key, scores, options, content):
    """
    Traiter l'unicité des clés
    """
    # Ajout de la clé <key2> pour conserver les doublons
    # Requis par plotGrpVerif
    key2 = list(key)
    key_counter = 1
    key2.append(key_counter)
    key2 = tuple(key2)
    while key2 in scores:
        key_counter += 1
        key2 = list(key)
        key2.append(key_counter)
        key2 = tuple(key2)
        if key_counter > 100:
            break
    else:
        _exception.Information(
            options.verbose and key_counter > 1,
            "{}e exemplaire traité lié la configuration '{}' de "
            "la fiche de performance", (key_counter, key))
#        scores[key2] = content
        return {key2: content}


def set_line(header, code, scores, names):
    """
    Définir la ligne de sortie
    """
    line = ['']*len(header)
    line[0] = code[0]
    line[1] = code[1]
    line[2] = code[2]
    line[3] = code[3]
    for cfg, values in scores[code].items():
        for n in names:
            try:
                title = '{}_{}'.format(n, cfg)
                idx_title = header.index(title)
                value = values[n]
            except KeyError:
                pass
            else:
                if np.isnan(value):
                    line[idx_title] = ''
                elif title.startswith('Eff'):
                    line[idx_title] = '{0:.3f}'.format(value)
                else:
                    line[idx_title] = '{0:.1f}'.format(value)
    return line


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
