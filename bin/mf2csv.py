#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""Convertir les données MétéoFrance au format de type csv."""


# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import sys

# Module PySPC
from pyspc.binutils.args import mf2csv as _args
from pyspc.binutils.runs import mf2csv as _runs

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """Convertir les données MétéoFrance au format de type csv."""
    _runs.mf2csv(_args.mf2csv())
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION DU SCRIPT mf2csv.py
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
