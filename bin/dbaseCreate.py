#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Création de bases de données vierges
Documentation : voir doc/html/dbaseCreate.py
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import shutil
import os.path
import sys

# Module PySPC
from pyspc.binutils.args import dbaseCreate as _args
import pyspc.core.exception as _exception
from pyspc import Config
from pyspc.data.prevision import Prevision19

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Création de bases de données vierges
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.dbaseCreate()
    if options.cfg_filename is not None:
        config = Config(filename=options.cfg_filename)
        config.read()
    else:
        config = None

    # ==================================================================
    #    2-- CREATION BASE DE DONNEES
    # ==================================================================
    # --------------------------------------------------------------
    #    2.1 : BASES ACCESS
    # --------------------------------------------------------------
    _exception.Information(
        options.verbose, "    + Création de bases de données Access")
    basename = None
    dst_basename = None
    # --------------------------------------------------------------
    #    2.1.1 : ACCES > SACHA
    # --------------------------------------------------------------
    if options.data_type.startswith('sacha'):
        basename = 'sacha.mdb'
        if options.db_filename is None:
            dst_basename = basename
        else:
            dst_basename = options.db_filename
        _exception.Information(
            options.verbose, "      - Base SACHA : {}", basename)
    # --------------------------------------------------------------
    #    2.1.4 : ACCES > PREVISION 2019
    # --------------------------------------------------------------
    elif options.data_type.startswith('previ19') and \
            options.db_filename is not None and \
            options.db_filename.endswith('.mdb'):
        basename = 'Prevision_2019.mdb'
        if options.db_filename is None:
            dst_basename = basename
        else:
            dst_basename = options.db_filename
        _exception.Information(
            options.verbose, "      - Base PREVISION 2019 : {}", basename)
    # --------------------------------------------------------------
    #    2.1.0 : ACCES > ERROR
    # --------------------------------------------------------------
    _exception.raise_valueerror(
        basename is None, "Type de base de données incorrect")

    # --------------------------------------------------------------
    #    2.2.0 : ACCES > CREATION
    # --------------------------------------------------------------
    src_filename = os.path.join(
        os.path.dirname(__file__), '..', 'resources', 'dbase', basename
    )
    dst_filename = os.path.join(options.output_dir, dst_basename)
    shutil.copy2(src_filename, dst_filename)
    _exception.Information(
        options.verbose,
        "      - Création de la base Access : {}", dst_filename)
    if options.data_type.startswith('previ19') and config is not None:
        _exception.Information(
            options.verbose,
            "      - Mise à jour à partir du fichier {}",
            options.cfg_filename)
        reader = Prevision19(filename=dst_filename)
        reader.insert_models(models=config)

    # ==================================================================
    #    3-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
