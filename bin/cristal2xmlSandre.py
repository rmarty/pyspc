#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convertir les données du format Archive Cristal
au format de type csv (grp16, grp18, pyspc)
Documentation : voir doc/html/cristal2xmlSandre.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import os.path
import sys

# Module PySPC
from pyspc.binutils.args import cristal2xmlSandre as _args
from pyspc.binutils.get_stations_list import get_stations_list
from pyspc.core.timeutil import str2dt
import pyspc.core.exception as _exception
from pyspc import read_Cristal, Series

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Convertir les données du format Archive Cristal
    au format xml Sandre
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.cristal2xmlSandre()

    # ==================================================================
    #    2-- LISTE DES STATIONS HYDRO A TRAITER
    # ==================================================================
    stations_list = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)
    _exception.raise_valueerror(not stations_list, "Aucune station à traiter")
    first_dtime = str2dt(options.first_dtime)
    last_dtime = str2dt(options.last_dtime)

    # ==================================================================
    #    3-- TRAITEMENT DES DONNEES
    # ==================================================================
    _exception.Information(
        options.verbose, "    + Traitement des données Cristal")
    cristal_series = read_Cristal(
        dirname=options.input_dir,
        codes=stations_list,
        first_dtime=first_dtime,
        last_dtime=last_dtime
    )
    cristal_series = cristal_series.between_dates(
        first_dtime=first_dtime, last_dtime=last_dtime)

    # ===============================================================
    #    4-- EXPORT DES DONNEES CSV
    # ===============================================================
    sandre_series = {}
    if options.station_name is not None:
        name = options.station_name
    else:
        name = os.path.basename(
            os.path.splitext(options.stations_list_file)[0])
    for k, s in cristal_series.items():
        if k[1][0] == 'P':
            kss = ('data_obs_meteo', k[1])
        elif k[1][0] in ['H', 'Q']:
            kss = ('data_obs_hydro', k[1])
        else:
            continue
        sandre_series.setdefault(
            kss, Series(datatype='obs', name='{}_{}_{}'.format(name, *kss)))
        sandre_series[kss].add(serie=s)
    for d, series in sandre_series.items():
        if not series:
            continue
        filename = series.to_Sandre(
            dirname=options.output_dir, datatype=d[0], **options.user_sandre)
        _exception.Information(options.verbose,
                               "    + Ecriture du fichier : {}", filename)

    # ==================================================================
    #    5-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTE
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
