#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Extraire des données de base MDB (Access) ou SQLIte
Documentation : voir doc/html/dbase2csv.py
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import os
import os.path
import sys

# Module PySPC
from pyspc.binutils.args import dbase2csv as _args
from pyspc.binutils.csv import write_csvlike
from pyspc.binutils.get_stations_list import get_stations_list
from pyspc.core.timeutil import str2dt
import pyspc.core.exception as _exception
from pyspc import (Series, read_Sacha,
                   read_Prevision14, read_Prevision17, read_Prevision19)

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Extraire des données de base MDB (Access) ou SQLIte
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.dbase2csv()

    # ==================================================================
    #    2-- LISTE DES STATIONS A TRAITER
    # ==================================================================
    stations_list = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)
    _exception.raise_valueerror(not stations_list, "Aucune station à traiter")
    # Base de donnée
    dbase_filename = os.path.join(options.input_dir, options.dbase_filename)
    # Lieux: Référentiels hydro2 / hydro3
    hydro_version = set_hydro_version(options)
    # Données pluie: sol ou radar?
    prcp_src = set_prcp_src(options)
    # Données historiques ou temps-réel
    realtime = bool(options.data_type == 'sacha_TR')
    # Données validées ou brutes
    valid, released = set_valid_released(options)
    # Période de données
    first_dt = str2dt(options.first_dtime)
    last_dt = str2dt(options.last_dtime)

    # ==================================================================
    #    3-- TRAITEMENT DES DONNEES PAR STATION
    # ==================================================================
    _exception.Information(
        options.verbose, "    + Lecture de la base de type '{}' : {}",
        [options.data_type, dbase_filename], unpacklist=True)
    # --------------------------------------------------------------
    #    3.1 : BASE DE TYPE 'sacha' et 'sacha_TR'
    # --------------------------------------------------------------
    if options.data_type in ['sacha', 'sacha_TR']:
        # Données historiques ou temps-réel
        meta = set_meta_sacha(options)
        # Lecture des données
        tmp_series = read_Sacha(
            filename=dbase_filename,
            codes=stations_list,
            varname=options.varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=realtime,
            prcp_src=prcp_src,
            hydro_version=hydro_version,
            warning=options.warning
        )
        series = Series(datatype='obs', name='Sacha')
        for s in tmp_series.values():
            series.add(serie=s, meta=meta)
    # --------------------------------------------------------------
    #    3.2.1 : BASE DE TYPE 'previ'/'previ_val' du SPC LCI (2014-2016)
    # --------------------------------------------------------------
    elif options.data_type in ['previ14', 'previ14_val']:
        # Lecture des données
        series = read_Prevision14(
            filename=dbase_filename,
            codes=stations_list,
            valid=valid,
            first_dt=first_dt,
            last_dt=last_dt,
            hydro_version=hydro_version,
            warning=options.warning
        )
    # --------------------------------------------------------------
    #    3.2.2 : BASE DE TYPE 'previ'/'previ_val' du SPC LCI (2017-2019)
    # --------------------------------------------------------------
    elif options.data_type.startswith('previ17') and options.hydro3:
        # Lecture des données
        series = read_Prevision17(
            filename=dbase_filename,
            codes=stations_list,
            valid=valid,
            released=released,
            first_dt=first_dt,
            last_dt=last_dt,
            warning=options.warning
        )
    # --------------------------------------------------------------
    #    3.2.3 : BASE DE TYPE 'previ'/'previ_val' du SPC LCI (2019-....)
    # --------------------------------------------------------------
    elif options.data_type.startswith('previ19') and options.hydro3:
        # Lecture des données
        series = read_Prevision19(
            filename=dbase_filename,
            codes=stations_list,
            valid=valid,
            released=released,
            first_dt=first_dt,
            last_dt=last_dt,
            warning=options.warning
        )
    # --------------------------------------------------------------
    #    3.00 : autre
    # --------------------------------------------------------------
    else:
        _exception.raise_valueerror(
            True,
            "type de base '{0}' inconnu ou version hydro '{1}' incorrecte"
            "".format(options.data_type, hydro_version))

    # ==================================================================
    #    4-- EXPORTS
    # ==================================================================
    filenames = write_csvlike(
        series=series, csvtype=options.csv_type,
        dirname=options.output_dir, overwrite=options.overwrite,
        onefile=options.onefile)
    _exception.Information(options.verbose,
                           "    + Ecriture du fichier : {}", filenames)

    # ==================================================================
    #    5-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


def set_valid_released(options):
    """Définir le statut de la prévisio"""
    if options.data_type.endswith('_val'):
        return True, False
    if options.data_type.endswith('_diff'):
        return True, True
    return False, False


def set_hydro_version(options):
    """Définir le référentiel Hydro2 / Hydro3"""
    if options.hydro2:
        return 'hydro2'
    if options.hydro3:
        return 'hydro3'
    raise ValueError('Référentiel Hydro2 / Hydro3 incorrect')


def set_prcp_src(options):
    """Définir la source pluvio (Sacha)"""
    # Source données pluie
    if options.varname == 'PH':
        if options.prcp_src is None:
            return 'gauge'
        return options.prcp_src
    return None


def set_meta_sacha(options):
    """Définir une simulation ?"""
    meta = []
    if options.data_type == 'sacha_TR':
        meta.append('TR')
    if options.prcp_src == 'radar':
        meta.append('radar')
    return '-'.join(meta) if meta else None


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
