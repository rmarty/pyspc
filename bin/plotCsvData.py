#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Tracer les données observées et/ou prévues sous forme graphique
Documentation : voir doc/html/plotCsvData.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import sys
import os
import os.path

# Module PySPC
from pyspc.binutils.args import plotCsvData as _args
from pyspc.binutils.csv import read_csvlike
from pyspc.binutils.get_stations_list import get_stations_list
import pyspc.core.exception as _exception
from pyspc.core.timeutil import str2dt
from pyspc.plotting import colormap
from pyspc.plotting.config import Config
from pyspc import Series

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Tracer les données observées et/ou prévues sous forme graphique
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.plotCsvData()

    # ==================================================================
    #    2-- GESTION DE LA CONFIGURATION
    # ==================================================================
    # ------------------------------------------------------------------
    #    2.1 : AVEC UN FICHIER DE CONFIGURATION
    # ------------------------------------------------------------------
    if options.cfg_filename is not None:
        # Lecture de la configuration
        config = Config(filename=options.cfg_filename)
        config.load()
        # Traitement pour convenir à la configuration des images
        try:
            verbose = config['figure']['verbose']\
                .lower() in ['1', 'true', 'oui']
        except KeyError:
            verbose = True
        options.verbose = verbose
        # Lecture des données
        _exception.Information(
            options.verbose, "    + Lecture des données et méta-données")
        series = Series(datatype='obs')
#        for idserie in config['figure']['datalist']:
        idseries = [k for k in config if k not in ['figure', 'defaut']]
        for idserie in idseries:
            _exception.Information(
                options.verbose, "      - Série courante : {0}", idserie)
            tmp_series = read_csvlike(
                stations='_'.join(idserie.split('_')[:-1]),
                varnames=idserie.split('_')[-1],
                dirname=config[idserie]['inputdir'],
                csvtype=config[idserie].get('csvtype', options.csv_type),
                warning=options.warning
            )
            # Sélection entre 2 dates
            if 'first_dtime' in config[idserie] and \
                    'last_dtime' in config[idserie]:
                tmp_series.between_dates(
                    config[idserie]['first_dtime'],
                    config[idserie]['last_dtime'],
                    inplace=True
                )
            _exception.raise_valueerror(
                tmp_series is None, "aucune série à traiter")
            series.extend(tmp_series.asobs())
    # ------------------------------------------------------------------
    #    2.2 : EN L'ABSENCE DE FICHIER DE CONFIGURATION
    # ------------------------------------------------------------------
    else:
        # Initialisation de la config et de la liste des séries de données
        config = Config()
        # Récupérer la palette de couleur
        pal_rgb = colormap.build_colormap(
            cmapname='gist_rainbow', cmapsize=256)
        # Liste des stations à considérer
        stations_list = get_stations_list(
            station_name=options.station_name,
            stations_list_file=options.stations_list_file)
        _exception.raise_valueerror(
            not stations_list, "aucune station à traiter")
        _exception.Information(
            options.verbose,
            "    + Lecture des données et affectation des méta-données")
        # Lecture des données
        series = read_csvlike(
            stations=stations_list,
            varnames=options.varname,
            dirname=options.input_dir,
            csvtype=options.csv_type,
            warning=options.warning
        )
        series = series.asobs()
        # Boucle sur les séries
        ks = 0
        for key in series.keys():
            idserie = '{}_{}'.format(*key[:2])
            _exception.Information(
                options.verbose, "      - Série courante : {0}", idserie)
            # création de la configuration 'serie'
            config[idserie] = {}
            config[idserie]['label'] = '{} ({})'.format(key[0], key[1])
            # traitement de la couleur
            if len(stations_list) > 1:
                config[idserie]['color'] = \
                    pal_rgb[int((ks)*255/len(stations_list))]
            # Maj compteur couleur
            ks += 1
        # Configuration Figure
        if len(stations_list) > 1:
            filename = os.path.splitext(
                os.path.basename(options.stations_list_file))[0]
        else:
            filename = options.station_name
        filename += "_" + "-".join([options.first_dtime, options.last_dtime])
        # Mise-à-jour de la configuration 'figure'
        config['figure'] = {
            'dpi': 150,
            'dirname': options.output_dir,
            'filename': filename,
            'plottype': options.plottype,
            'verbose': options.verbose,
            'xlim': [str2dt(options.first_dtime), str2dt(options.last_dtime)]
        }

    # ==================================================================
    #    3-- REALISATION DE LA FIGURE
    # ==================================================================
    filename = series.plot_series(
        plottype=config['figure']['plottype'], config=config)
    _exception.Information(
        options.verbose, "    + Ecriture du fichier : {}", filename)

    # ==================================================================
    #    4-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
