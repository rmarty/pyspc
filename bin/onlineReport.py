#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Informations sur les produits/rapports/fiches/vigilance MF/Inrae/Vigicrues
téléchargées depuis le site des données publiques
Documentation : voir doc/html/onlineReport.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import sys

# Module PySPC
from pyspc.binutils.args import onlineReport as _args
from pyspc.binutils.runs import onlineReport as _runs


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Informations sur les produits/rapports/fiches/vigilance MF/Inrae/Vigicrues
    téléchargées depuis le site des données publiques
    """
    _runs.onlineReport(_args.onlineReport())
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
