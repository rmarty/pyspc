#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Extraire les données BdLamedo et les stocker au format *json* de BdApbp.
Documentation : voir doc/html/bdapbp2json.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import sys

# Module PySPC
from pyspc.binutils.args import bdapbp2json as _args
import pyspc.core.exception as _exception
from pyspc.core.config import Config
from pyspc.core.timeutil import str2dt
from pyspc.webservice.lamedo import BdApbp

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Extraire les données BdLamedo et les stocker au format *json* de BdApbp.
    """
    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.bdapbp2json()
    options.runtime = str2dt(options.runtime)

    # ==================================================================
    #    2-- CREATION DE LA CONFIG. BDLAMEDO
    # ==================================================================
    _exception.Information(
        options.verbose,
        "    + Lecture de la configuration BdApbp : {}", options.cfg_filename)
    bdapbp_config = Config(filename=options.cfg_filename)
    bdapbp_config.read()

    # ==================================================================
    #    3-- TRAITEMENT DE LA REQUETE BDLAMEDO
    # ==================================================================
    _exception.Information(
        options.verbose,
        "    + Lancement du téléchargement des données BdApbp")
    try:
        hostname = bdapbp_config['bdapbp']['hostname']
    except KeyError as ke:
        raise ValueError('Hostname de BdApbp mal défini') from ke
    bdapbp = BdApbp(hostname=hostname)
    bdapbp.set_url(
        zones=options.domain_name,
        date=options.runtime,
        dtype=options.data_type)
    _exception.Information(
        options.verbose,
        "      - Requête BdApbp : {}", bdapbp.url)
    bdapbp.set_filename(
        zones=options.domain_name,
        date=options.runtime,
        dtype=options.data_type,
        dirname=options.output_dir
    )
    bdapbp.retrieve_byurllib()
    _exception.Information(
        options.verbose,
        "      - Ecriture du fichier : {}", bdapbp.filename)

    # ==================================================================
    #    4-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
