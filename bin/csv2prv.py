#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convertir les données de type csv (GRP16, GRP18, pyspc)
au format prv Scores
Documentation : voir doc/html/csv2prv.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import os
import sys

# Module PySPC
from pyspc.binutils.args import csv2prv as _args
from pyspc.binutils.csv import read_csvlike
from pyspc.binutils.get_stations_list import get_stations_list
import pyspc.core.exception as _exception

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Convertir les données de type csv (GRP16, GRP18, pyspc)
    au format prv Scores.
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.csv2prv()

    # ==================================================================
    #    2-- LISTE DES STATIONS HYDRO A TRAITER
    # ==================================================================
    stations_list = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)
    _exception.raise_valueerror(not stations_list, "aucune station à traiter")

    # ==================================================================
    #    3-- TRAITEMENT DES DONNEES
    # ==================================================================
    _exception.Information(options.verbose, "    + Lecture des données")
    series = read_csvlike(
        stations=stations_list,
        varnames=options.varname,
        csvtype=options.csv_type,
        dirname=options.input_dir,
        warning=options.warning
    )
    if options.station_name is not None:
        basename = '{}_{}.prv'.format(options.station_name, options.varname)
    else:
        basename = '{}_{}.prv'.format(
            os.path.splitext(os.path.basename(
                options.stations_list_file))[0], options.varname)
    filename = series.to_prv(
        dirname=options.output_dir, datatype=options.data_type,
        basename=basename)
    _exception.Information(
        options.verbose, "    + Ecriture du fichier : {}", filename)

    # ==================================================================
    #    4-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTE
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
