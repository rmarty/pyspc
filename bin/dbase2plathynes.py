#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Extraire des données de base MDB (Access) pour PLATHYNES
Documentation : voir doc/html/dbase2plathynes.py
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import os
import os.path
import sys

# Module PySPC
from pyspc.binutils.args import dbase2plathynes as _args
from pyspc.binutils.get_stations_list import get_stations_list
from pyspc.binutils.plathynes import (
    events_in_project, process_locations, process_project, set_nodata_series)
import pyspc.core.exception as _exception
from pyspc import read_Sacha, Series

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Extraire des données de base MDB (Access) pour PLATHYNES
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    # --------------------------------------------------------------
    #    1.0 : LECTURE DES OPTIONS
    # --------------------------------------------------------------
    options = _args.dbase2plathynes()
    # --------------------------------------------------------------
    #    1.1 : LISTE DES STATIONS D'INJECTION
    # --------------------------------------------------------------
    injections_list = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)

    # ==================================================================
    #    2-- PROJET PLATHYNES
    # ==================================================================
    # --------------------------------------------------------------
    #    2.1 : LECTURE DU FICHIER DE CONFIGURATION 'PROJET'
    # --------------------------------------------------------------
    prj_cfg, locations = process_project(
        options.output_dir, options.projet_filename)
    _exception.Information(
        options.verbose,
        "    + Lecture du projet PLATHYNES : {}", prj_cfg.filename)
    # --------------------------------------------------------------
    #    2.2 : LECTURE DES INFORMATIONS SUR LES SITES / STATIONS
    # --------------------------------------------------------------
    stations, injections, suffix = process_locations(
        locations, injections_list)
    _exception.Information(
        options.verbose,
        "      - Station pluviométrique : {}", stations['P'])
    _exception.Information(
        options.verbose,
        "      - Station hydrométrique : {}", stations['Q'])
    _exception.Information(
        options.verbose,
        "      - Station injectée : {} {}",  list(injections.items()))
    # --------------------------------------------------------------
    #    2.3 : EVENEMENTS
    # --------------------------------------------------------------
    home_dirname = os.path.splitext(prj_cfg.filename)[0]
    _exception.Information(
        options.verbose,
        "      - Evenements définis dans le projet PLATHYNES")
    events = events_in_project(prj_cfg, home_dirname)
    _exception.Information(
        options.verbose,
        "        + Evénement : {}", list(events.keys()))
    # Gestion des événements définis par l'utilisateur
    if options.events is None:
        options.events = list(events.keys())
    else:
        options.events = list(set(events).intersection(set(options.events)))
    _exception.raise_valueerror(
        not options.events, "aucune événement à traiter")

    # ==================================================================
    #    3-- CONVERSION DES DONNEES
    # ==================================================================
    _exception.Information(options.verbose, "    + Conversion des données")
    hydro_version = None
    if options.hydro2:
        hydro_version = 'hydro2'
    elif options.hydro3:
        hydro_version = 'hydro3'
    realtime = bool(options.data_type == 'sacha_TR')
    # --------------------------------------------------------------
    # PAR EVENEMENT / PAR GRANDEUR
    # --------------------------------------------------------------
    for evt in options.events:
        fdt = events[evt]['first_dtime']
        ldt = events[evt]['last_dtime']
        files_todo = events[evt]['filenames']
        files_done = []
        event_dirname = os.path.join(home_dirname, 'Ev_{}'.format(evt))
        _exception.Information(
            options.verbose, "      - Evenement sélectionné : {}", evt)
        _exception.Information(
            options.verbose, "        + Premier instant : {}", str(fdt))
        _exception.Information(
            options.verbose, "        + Dernier instant : {}", str(ldt))
        # -------------------------------------------------------------
        # QH
        # -------------------------------------------------------------
        var = 'Q'
        if var in options.varname:
            sta_todo = stations[var]
        else:
            sta_todo = []
        if sta_todo:
            for dbf in options.dbase_filename:
                dbf = os.path.join(options.input_dir, dbf)
                sta_done = []
                for sta in sta_todo:
                    try:
                        series = read_Sacha(
                            filename=dbf,
                            codes=[sta],
                            realtime=realtime,
                            first_dt=fdt,
                            last_dt=ldt,
                            varname=var + 'H',
                            hydro_version=hydro_version,
                            warning=options.warning
                        )
                    except ValueError:
                        series = set_nodata_series(
                            codes=[sta],
                            first_dt=fdt,
                            last_dt=ldt,
                            varname=var + 'H',
                        )
                        _exception.Information(
                            options.verbose,
                            "Série de valeur manquante créée pour {}", sta)
                    # Si la collection est vide, je passe à la station suivante
                    if not series.codes:
                        continue
                    key = (series.codes[0], series.varnames[0], series.meta[0])
                    loc = locations[key[0]]
                    loc.code = loc.name  # Kxxx -> LaLoireChadrac
                    series[key].code = loc
                    filenames = series.to_PLATHYNES_Data(
                        dirname=event_dirname,
                        injections={loc.name: injections[key[0]]},
                        event=evt,
                        suffix=suffix  # series[key].location.code:
                    )
                    _exception.Information(
                        options.verbose,
                        "        + Export du fichier du lieu {} : {}",
                        (sta, filenames[0]))
                    sta_done.append(key[0])
                    files_done.extend(filenames)
                sta_todo = list(set(sta_todo).difference(set(sta_done)))
            _exception.Information(
                options.verbose,
                "Station inconnue des bases SACHA : {}", sta_todo)
        elif var not in options.varname:
            _exception.Information(
                options.verbose,
                "Pas d'export demandé pour la grandeur : {}", var)
        else:
            _exception.Information(
                options.verbose,
                "Pas de station pour la grandeur : {}", var)

        # -------------------------------------------------------------
        # PH
        # -------------------------------------------------------------
        var = 'P'
        if var in options.varname:
            sta_todo = stations[var]
        else:
            sta_todo = []
        if sta_todo:
            series = Series(datatype='obs', name='PLATHYNES_Data')
            for dbf in options.dbase_filename:
                dbf = os.path.join(options.input_dir, dbf)
                sta_done = []
                try:
                    tmp_series = read_Sacha(
                        filename=dbf,
                        codes=sta_todo,
                        realtime=realtime,
                        first_dt=fdt,
                        last_dt=ldt,
                        varname=var + 'H',
                        prcp_src='gauge',
                        hydro_version=hydro_version,
                        warning=options.warning
                    )
                except ValueError:
                    tmp_series = set_nodata_series(
                        codes=sta_todo,
                        first_dt=fdt,
                        last_dt=ldt,
                        varname=var + 'H',
                    )
                    _exception.Information(
                        options.verbose,
                        "Séries de valeur manquante créées pour {}",
                        str(sta_todo))
                # Si la collection est vide, je passe à la base suivante
                if not tmp_series.codes:
                    continue
                for key, serie in tmp_series.items():
                    loc = locations[key[0]]
                    loc.code = loc.name
                    serie.code = loc
                    series.add(code=loc.name, serie=serie, meta=key[2])
                    sta_done.append(key[0])
                sta_todo = list(set(sta_todo).difference(set(sta_done)))
            filenames = series.to_PLATHYNES_Data(
                dirname=event_dirname,
                event=evt
            )
            _exception.Information(
                options.verbose,
                "        + Export du fichier : {}", filenames)
            files_done.extend(filenames)
        else:
            _exception.Information(
                options.verbose,
                "Pas de station pour la grandeur : {}", var)

        # -------------------------------------------------------------
        # Contrôles des fichiers et des séries
        # -------------------------------------------------------------
        _exception.Information(
            sta_todo,
            "Station inconnue des bases SACHA : {}", sta_todo)
        files_todo = [f for f in files_todo
                      if os.path.join(home_dirname, f) not in files_done]
        _exception.Information(
            files_todo,
            "Fichier attendu mais non créé : {}", files_todo)

    # ==================================================================
    #    5-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
