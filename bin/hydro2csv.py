#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convertir les donnees Hydro2 au format lu par GRP
Documentation : voir doc/html/hydro2csv.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import glob
import os
import sys

# Module PySPC
from pyspc.binutils.args import hydro2csv as _args
from pyspc.binutils.csv import write_csvlike
from pyspc.core.convention import EXTERNAL_VARNAMES
import pyspc.core.exception as _exception
from pyspc import read_Hydro2

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Convertir les données Hydro2
    au format *csv* des données de GRP *Calage*
    """
    # ===============================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ===============================================================
    options = _args.hydro2csv()

    # ===============================================================
    #    2-- LECTURE DES DONNEES Hydro2
    # ===============================================================
    _exception.Information(options.verbose, "    + Lecture des données HYDRO2")
    hydro_filenames = glob.glob(os.path.join(options.input_dir,
                                             options.data_filename))
    _exception.raise_valueerror(not hydro_filenames,
                                "Aucun fichier HYDRO2")
#    datatype = options.datatype
    datatype = [k[1]
                for k, v in EXTERNAL_VARNAMES.items() if k[0] == 'Hydro2'
                if v == options.varname]
    _exception.raise_valueerror(
        not datatype, "Grandeur incohérente avec Hydro2")
    datatype = datatype[0]

    for hydro_filename in hydro_filenames:
        _exception.Information(options.verbose,
                               "      - Fichier HYDRO2 : {}", hydro_filename)
        series = read_Hydro2(
            filename=hydro_filename,
            datatype=datatype,
            warning=options.warning
        )
    # ===============================================================
    #    3-- EXPORT DES DONNEES CSV
    # ===============================================================
        filenames = write_csvlike(
            series=series, csvtype=options.csv_type,
            dirname=options.output_dir, overwrite=options.overwrite,
            onefile=options.onefile)
        _exception.Information(options.verbose,
                               "        + Ecriture du fichier : {}", filenames)

    # ===============================================================
    #    4-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION DU SCRIPT mf2grp.py
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
