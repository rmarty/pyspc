#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Tracer sous forme graphique les performances GRP
fournies par grpVerif.py
Documentation : voir doc/html/plotGrpVerif.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import glob
import os.path
import sys

# Module PySPC
from pyspc.binutils.args import plotGrpVerif as _args
from pyspc.model.grp16.cal_verif import fix_SV, plot_verif, read_csv, reset_key
import pyspc.core.exception as _exception

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Tracer sous forme graphique les performances GRP
    fournies par grpVerif.py
    Documentation : voir doc/html/plotGrpVerif.html
    """
    # ===============================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ===============================================================
    options = _args.plotGrpVerif()
    grp_config = {item[0]: item[1:] for item in options.grp_config}
    _exception.Information(
        options.verbose, "    + Éléments de configuration GRP")
    _exception.Information(
        options.verbose, "      - {} : {}",
        list(grp_config.items())
    )
    scores_names = ['POD', 'FAR', 'CSI']
    toprint_vars = [(s, t)
                    for s in scores_names
                    for t in grp_config['SV']]
    toprint_sets = [(m, c)
                    for m in grp_config['MODE']
                    for c in grp_config['SC']]

    # ===============================================================
    #    2-- LECTURE DU FICHIER DES PERFORMANCES
    # ===============================================================
    verif_filenames = glob.glob(os.path.join(options.input_dir,
                                             options.data_filename))
    _exception.raise_valueerror(not verif_filenames,
                                "Aucun fichier de vérification")
    for filename in verif_filenames:
        _exception.raise_valueerror(
            not os.path.exists(filename),
            "Le fichier {} n'existe pas".format(filename)
        )
        _exception.Information(
            options.verbose, "    + Lecture du fichier {}", filename)
        content = read_csv(
            filename=filename,
            scores_names=scores_names
        )

    # ===============================================================
    #    3-- UNE FIGURE PAR CODE ET PAR HORIZON DE CALAGE
    # ===============================================================
        _exception.Information(
            options.verbose, "    + Création des figures")
        for case in content:
            _exception.Information(
                options.verbose,
                "      - Code = {} / HC = {}", case)
            _exception.Information(
                options.verbose, "        + Préparation des données")
            # ----------------------------------------------------------
            #    RESTRUCTURATION DES RESULTATS
            # ----------------------------------------------------------
            sample = reset_key(content=content[case], config=grp_config,
                               scores_names=scores_names)
            # ----------------------------------------------------------
            #    NETTOYAGE DES VALEURS SUSPECTES (SV forts)
            # ----------------------------------------------------------
            _exception.Information(
                options.verbose,
                "        + Nettoyage des scores pour SV forts")
            toremove_sv, towarn_sv, toprint_vars2 = fix_SV(
                content=sample, toprint_sets=toprint_sets,
                toprint_vars=toprint_vars)
            _exception.Information(
                options.verbose,
                "          - SV = {0:5s} : incomplet mais conservé "
                "dans la figure", towarn_sv)
            _exception.Information(
                options.verbose,
                "          - SV = {0:5s} : supprimé de l'échantillon",
                toremove_sv)
            # ----------------------------------------------------------
            #    CREATION DE LA FIGURE
            # ----------------------------------------------------------
            fig_filename = os.path.join(
                options.output_dir,
                os.path.splitext(os.path.basename(options.data_filename))[0] +
                '_{}_HC-{}.png'.format(*case)
            )
            _exception.Information(
                options.verbose,
                "        + Impression de la figure")
            filename = plot_verif(
                filename=fig_filename,
                title='Score GRP / {} / HC={}'.format(*case),
                toprint_sample=sample,
                toprint_vars=toprint_vars2,
                toprint_sets=toprint_sets,
            )
            _exception.Information(
                options.verbose, "        + Fichier image : {}", filename)

    # ===============================================================
    #    4-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION DU SCRIPT mf2grp.py
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
