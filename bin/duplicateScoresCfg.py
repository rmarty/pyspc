#!/usr/bin/python3
# -*- coding: utf-8 -*-
#########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Dupliquer la configuration SCORES avec modifications autorisees
selon les options choisies par l'utilisateur
Documentation : voir doc/html/duplicateGrpRTCfg.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import sys
import os.path

# Module PySPC
from pyspc.binutils.args import duplicateScoresCfg as _args
import pyspc.core.exception as _exception
from pyspc.verification.scores import Config

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Dupliquer la configuration SCORES avec modifications autorisees
    selon les options choisies par l'utilisateur
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.duplicateScoresCfg()
    if options.update_cfg is not None:
        cfg_to_update = {(c[0], c[1]): c[2].replace('"', '')
                         for c in options.update_cfg}
    else:
        cfg_to_update = None
    # ===============================================================
    #    2-- LECTURE DU FICHIER DE CONFIGURATION
    # ===============================================================
    config = Config(
        filename=os.path.join(options.input_dir, options.duplicata_src))
    _exception.Information(
        options.verbose,
        "    + Lecture du fichier de configuration 'source' : {0}",
        config.filename)
    config.read()

    # ===============================================================
    #    3-- MODIFICATION DE LA CONFIGURATION
    # ===============================================================
    if isinstance(cfg_to_update, dict):
        _exception.Information(
            options.verbose, "    + Mise à jour")
        config.update_config(config=cfg_to_update)
        _exception.Information(
            options.verbose, "      - {}", [str(k) + ' -> ' + str(v)
                                            for k, v in cfg_to_update.items()])

    # ===============================================================
    #    4-- ECRITURE DE LA NOUVELLE CONFIGURATION
    # ===============================================================
    config.filename = os.path.join(options.output_dir, options.cfg_filename)
    _exception.Information(
        options.verbose,
        "    + Écriture de la configuration mise à jour : {0}",
        config.filename)
    config.write()

    # ===============================================================
    #    5-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION DU SCRIPT duplicateGrpRTCfg.py
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
