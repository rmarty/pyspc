#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Insérer des prévisions au format pyspc dans une base Prevision19
Documentation : voir doc/html/csv2dbase.py
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import sys
import os
import os.path

# Module PySPC
from pyspc.binutils.args import csv2dbase as _args
from pyspc.binutils.csv import read_csvlike
from pyspc.binutils.get_stations_list import get_stations_list
import pyspc.core.exception as _exception

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Insérer des prévisions au format pyspc dans une base Prevision19
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.csv2dbase()

    # ==================================================================
    #    2-- LISTE DES STATIONS A TRAITER
    # ==================================================================
    stations_list = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)
    _exception.raise_valueerror(not stations_list, "aucune station à traiter")

    # ==================================================================
    #    3-- LECTURE DES DONNEES
    # ==================================================================
    _exception.Information(options.verbose, "    + Lecture des données")
    series = read_csvlike(
        stations=stations_list,
        varnames=options.varname,
        dirname=options.input_dir,
        csvtype='pyspc',
        warning=options.warning
    )

    # ==================================================================
    #    4-- EXPORT DES DONNEES
    # ==================================================================
    dbase_filename = os.path.join(options.output_dir, options.dbase_filename)
    # --------------------------------------------------------------
    #    4.1 : BASE DE TYPE 'previ'/'previ_val' du SPC LCI (2019-....)
    # --------------------------------------------------------------
    if options.data_type.startswith('previ19'):
        _exception.Information(
            options.verbose,
            "    + Insertion dans la base Prévision 2019 : {}", dbase_filename)
        rows = series.to_Prevision19(filename=dbase_filename)
        _exception.Information(
            options.verbose,
            "      - Prévisions {}\n        - Série {}\n        - Valeurs {}",
            [(kr, kv['series'], kv['values']) for kr, kv in rows.items()])
    # --------------------------------------------------------------
    #    4.0 : autre
    # --------------------------------------------------------------
    else:
        raise NotImplementedError

    # ==================================================================
    #    5-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
