#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Extraire des données de base MDB (Access) pour PLATHYNES
Documentation : voir doc/html/duplicatePlathynesEvent.py
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import collections
import copy
import os
import os.path
import sys

# Module PySPC
from pyspc.binutils.args import duplicatePlathynesEvent as _args
from pyspc.model.plathynes import Config as Pla_Config
import pyspc.core.exception as _exception
from pyspc import Config

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Extraire des données de base MDB (Access) pour PLATHYNES
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    # --------------------------------------------------------------
    #    1.1 : DEFINITION DES ARGUMENTS
    # --------------------------------------------------------------
    options = _args.duplicatePlathynesEvent()
    # --------------------------------------------------------------
    #    1.2 : LECTURE DES EVENEMENTS
    # --------------------------------------------------------------
    events = Config.from_csv(
        filename=options.events_filename, sectionname='event', sep=';')

    # ==================================================================
    #    2-- PROJET PLATHYNES
    # ==================================================================
    # --------------------------------------------------------------
    #    2.1 : LECTURE DU FICHIER DE CONFIGURATION 'PROJET'
    # --------------------------------------------------------------
    prj_cfg = Pla_Config(
        filename=os.path.join(options.input_dir, options.projet_filename),
        datatype='project'
    )
    _exception.Information(
        options.verbose,
        "    + Lecture du projet PLATHYNES : {}", prj_cfg.filename)
    prj_cfg.read()
    # --------------------------------------------------------------
    #    2.3 : TEST DE L'EXISTENCE DE L'EVENEMENT 'SOURCE'
    # --------------------------------------------------------------
    if options.event is None:
        options.event = list(prj_cfg['Events']['events'].keys())[0][0]
    _exception.raise_valueerror(
        'events' not in prj_cfg['Events'] or
        len(prj_cfg['Events']['events']) == 0,
        "Aucun événement à traiter"
    )
    _exception.raise_valueerror(
        options.event not in [x[0]
                              for x in prj_cfg['Events']['events'].keys()],
        "événement 'source' inconnu : {}".format(options.event)
    )
    # --------------------------------------------------------------
    #    2.3 : LECTURE DU FICHIER DE CONFIGURATION 'EVENT'
    # --------------------------------------------------------------
    evt_cfg = Pla_Config(
        filename=os.path.join(os.path.splitext(prj_cfg.filename)[0],
                              'Ev_{}'.format(options.event),
                              '{}.evt'.format(options.event)),
        datatype='event'
    )
    _exception.Information(
        options.verbose,
        "    + Lecture de l'événement PLATHYNES : {}", evt_cfg.filename)
    evt_cfg.read()

    # ==================================================================
    #    3-- COPIES DES EVENEMENTS ET DU PROJET PLATHYNES
    # ==================================================================
    # --------------------------------------------------------------
    #    3.1 : COPIE DE LA CONFIGURATION DU PROJET
    # --------------------------------------------------------------
    _exception.Information(
        options.verbose, "    + Copie de la configuration du projet")
    prj_new = Pla_Config(
        filename=os.path.join(options.output_dir, options.projet_filename),
        datatype='project'
    )
    for ks, vs in prj_cfg.items():
        prj_new[ks] = vs
    # Re-initialiser la liste des événements
    if options.overwrite:
        _exception.Information(
            options.verbose, "    + Re-initialisation des événements")
        prj_new['Events']['events'] = collections.OrderedDict()
    # --------------------------------------------------------------
    #    3.2 : COPIES DES CONFIGURATIONS 'EVENEMENT'
    # --------------------------------------------------------------
    home_dirname = os.path.splitext(prj_new.filename)[0]
    for ke, ve in events.items():
        _exception.Information(options.verbose, "    + Evenement '{}'", ke)
        # --------------------------------------------------------------
        #    3.2.1 : DEFINITION 'EVENEMENT'
        # --------------------------------------------------------------
        evt_new = set_new_event_config(evt_cfg, home_dirname, options, ke, ve)
        # --------------------------------------------------------------
        #    3.2.2 : ECRITURE 'EVENEMENT'
        # --------------------------------------------------------------
        write_new_config(evt_new, options)
        # --------------------------------------------------------------
        #    3.2.4 : AJOUT 'EVENEMENT' DANS PROJET
        # --------------------------------------------------------------
        prj_new['Events']['events'].setdefault(
            (ve['event'], 'Evenement'), '{} -1 0 0.0 1'.format(ve['event']))
#        break
    # --------------------------------------------------------------
    #    3.3 : M-à-j DE LA CONFIGURATION DU PROJET
    # --------------------------------------------------------------
    write_new_config(prj_new, options)
    # ==================================================================
    #    5-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


def write_new_config(cfg, options):
    """
    Ecriture d'un nouveau fichier de configuration PLATHYNES
    """
    _exception.Information(
        options.verbose,
        "      - Ecriture du fichier {}", cfg.filename)
    if not os.path.exists(os.path.dirname(cfg.filename)):
        os.makedirs(os.path.dirname(cfg.filename))
    cfg.write()


def set_new_event_config(evt_cfg, home_dirname, options, ke, ve):
    """
    Définir la configuration d'un événement à créer
    """
    _exception.Information(options.verbose, "      - Copie de l'evenement")
    evt_new = Pla_Config(
        filename=os.path.join(
            home_dirname, 'Ev_{}'.format(ke), '{}.evt'.format(ke)),
        datatype='event'
    )
    for ks, vs in evt_cfg.items():
        evt_new[ks] = copy.deepcopy(vs)
        for ko, vo in evt_new[ks].items():
            if isinstance(vo, str):
                evt_new[ks][ko] = vo.replace(options.event, ve['event'])
            elif isinstance(vo, dict):
                keys = list(vo.keys())
                for k in keys:
                    value = vo.pop(k)
                    newkey = (k[0].replace(options.event, ve['event']),
                              k[1])
                    value = value.replace(options.event, ve['event'])
                    vo.setdefault(newkey, value)
                evt_new[ks][ko] = vo
    _exception.Information(options.verbose, "      - M-à-j de l'evenement")
    evt_new['Temporal settings']["Date de debut"] = ve['first_dtime']
    evt_new['Temporal settings']["Date de fin"] = ve['last_dtime']
    for k in evt_new['Parameters']["evenementiels"]:
        if k[0] not in ve:
            continue
        x = evt_new['Parameters']["evenementiels"][k].split(' ')
        x[1] = ve[k[0]]
        evt_new['Parameters']["evenementiels"][k] = ' '.join(x)
    return evt_new


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
