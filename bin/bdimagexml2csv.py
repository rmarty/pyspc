#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convertir les données BdImage du format *xml*
au format de type csv (pyspc)
Documentation : voir doc/html/bdimagexml2csv.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import glob
import os
import sys

# Module PySPC
from pyspc.binutils.args import bdimagexml2csv as _args
from pyspc.binutils.csv import write_csvlike
import pyspc.core.exception as _exception
from pyspc import read_BdImage

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Convertir les données BdImage du format *xml*
    au format de type csv (pyspc)
    """

    # ===============================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ===============================================================
    options = _args.bdimagexml2csv()

    # ===============================================================
    #    2-- LECTURE DES DONNEES BDImages
    # ===============================================================
    _exception.Information(options.verbose,
                           "    + Lecture des données BdImage")
    xml_filenames = glob.glob(os.path.join(options.input_dir,
                                           options.xml_filename))
    _exception.raise_valueerror(not xml_filenames,
                                "Aucun fichier BdImage")
    for xml_filename in xml_filenames:
        _exception.Information(
            options.verbose, "      - Fichier BdImage : {}", xml_filename)
        series = read_BdImage(
            filename=xml_filename,
            ratio_image=options.ratio_level.get('ratio_image', 1),
            ratio_stats=options.ratio_level.get('ratio_stats', 1),
            warning=options.warning
        )
        if series is None:
            _exception.Warning(
                None,
                "Aucune donnée valide dans le fichier {}".format(xml_filename))
            continue
    # ===============================================================
    #    3-- EXPORT DES DONNEES CSV
    # ===============================================================
        filenames = write_csvlike(
            series=series, csvtype=options.csv_type,
            dirname=options.output_dir, overwrite=options.overwrite,
            onefile=options.onefile)
        _exception.Information(options.verbose,
                               "        + Ecriture du fichier : {}", filenames)

    # ===============================================================
    #    4-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION DU SCRIPT mf2grp.py
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
