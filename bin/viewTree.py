#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Afficher l'arborescence d'un répertoire
Documentation : voir doc/html/viewTree.py
"""
# Import des paquets requis
# -------------------------
import sys

from pyspc.binutils.args import viewTree as _args
from pyspc.binutils.view_tree import view_tree
import pyspc.core.exception as _exception

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Afficher l'arborescence d'un répertoire
    """
    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.viewTree()

    # ==================================================================
    #    2-- ANALYSE DE L'ARBORESCENCE
    # ==================================================================
    view_tree(
        path=options.input_dir,
        limit=options.depth,
        size=options.size,
        path_length=options.length,
        oldtime=options.oldtime,
        oldfile=options.oldfile,
        duplicate=options.duplicate
    )

    # ==================================================================
    #    5-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
