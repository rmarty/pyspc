#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convertir les résultats Scores du xml au csv/png
Documentation : voir doc/html/xmlScores2png.html
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import glob
import os
import sys

# Modules pyspc
from pyspc.binutils.args import xmlScores2png as _args
from pyspc.core.config import Config
import pyspc.core.exception as _exception
from pyspc.verification.scores import Results

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Convertir les résultats Scores du xml au csv/png
    Documentation : voir doc/html/xmlScores2png.html
    """
    # ===============================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ===============================================================
    options = _args.xmlScores2png()
    if options.cfg_filename is not None:
        config = Config(filename=options.cfg_filename)
        config.read()
    else:
        config = {}

    # ===============================================================
    #    2-- LECTURE DU FICHIER XML
    # ===============================================================
    _exception.Information(
        options.verbose, "    + Lecture des résultats Scores")
    xml_filenames = glob.glob(os.path.join(options.input_dir,
                                           options.xml_filename))
    _exception.raise_valueerror(not xml_filenames,
                                "Aucun fichier Scores")
    for xml_filename in xml_filenames:
        _exception.Information(
            options.verbose, "      - Lecture du fichier XML Scores : {}",
            xml_filename)
        xml_scores = Results(filename=xml_filename)
#        xml_scores.read(names=options.score_name)
        xml_scores.read()

    # ===============================================================
    #    3-- EXPORTS
    # ===============================================================
    # ---------------------------------------------------------------
    # 3.1 - FORMAT CSV
    # ---------------------------------------------------------------
        _exception.Information(
            options.verbose, "      - Export des Scores au format CSV")
        filenames = xml_scores.to_csv(dirname=options.output_dir)
        _exception.Information(
            options.verbose, "        + Export du fichier CSV: {}",
            [f for f in filenames if f is not None])
    # ---------------------------------------------------------------
    # 3.2 - FORMAT PNG
    # ---------------------------------------------------------------
        _exception.Information(
            options.verbose, "      - Export des Scores au format PNG")
        filenames = xml_scores.to_png(
            dirname=options.output_dir, config=config)
        _exception.Information(
            options.verbose, "        + Export du fichier CSV: {}",
            [f for f in filenames if f is not None])

    # ===============================================================
    #    4-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


# -------------------------------------------------------------------
#      EXECUTION DU SCRIPT mf2grp.py
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
