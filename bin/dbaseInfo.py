#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Afficher les méta-données des stations et tronçons
du référentiel du SPC LCI
Documentation : voir doc/html/dbaseInfo.py
"""

# -------------------------------------------------------------------
#      IMPORT DES MODULES PYTHON
# -------------------------------------------------------------------
# Modules Python
import os
import os.path
import sys

# Module PySPC
from pyspc.binutils.args import dbaseInfo as _args
from pyspc.binutils.get_stations_list import get_stations_list
import pyspc.core.exception as _exception
from pyspc import read_RefSPC, Config, Locations, Reaches

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      PROGRAMME PRINCIPAL
# -------------------------------------------------------------------
def main():
    """
    Afficher les méta-données des stations et tronçons
    de la base Stations du SPC LCI
    """

    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    options = _args.dbaseInfo()
    if options.output_filename is not None:
        export = Config(filename=options.output_filename)
    else:
        export = None

    # ==================================================================
    #    2-- LISTE DES LIEUX DE MESURE A TRAITER
    #        LISTE DES TRONCONS
    # ==================================================================
    stations = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)
    _exception.raise_valueerror(not stations, "aucune station à traiter")

    # ==================================================================
    #    3-- TRAITEMENT DES LIEUX DE MESURE
    # ==================================================================
    filename = os.path.join(options.input_dir, options.db_filename)
    content = read_RefSPC(filename=filename, datatype=options.data_type,
                          codes=stations, hydro3=options.hydro3)

    # ==================================================================
    #    4-- SELECTION DES INFORMATIONS A AFFICHER
    #        IMPRESSION DES INFORMATIONS
    # ==================================================================
    if isinstance(content, (Locations, Reaches)) and \
            isinstance(export, Config):
        export.update(content.to_Config(filename=options.output_filename))
    elif isinstance(content, (Locations, Reaches)):
        print_locs(content)
    elif isinstance(content, Config) and isinstance(export, Config):
        export.update(content)
    elif isinstance(content, Config):
        print_config(content)

    # ==================================================================
    #    5-- FIN DU SCRIPT
    # ==================================================================
    if isinstance(export, Config):
        export.to_csv(filename=export.filename, sectionname='code', sep=';')
        _exception.Information(
            options.verbose,
            "    + Export dans le fichier {}", export.filename)
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])
    sys.exit(0)


def print_locs(content):
    """Impression écran des informations Locations"""
    atts = None
    for loc in content.values():
        if atts is None:
            atts = sorted(loc.__dict__.keys())
            print(';'.join([a.strip('_') for a in atts]))
        print(';'.join([str(getattr(loc, a)) for a in atts]))


def print_config(content):
    """Impression écran des informations Utilisateur"""
    header = content.list_unique_options()
    print(';'.join(header))
    for element in content:
        print(';'.join([str(content[element].get(h, ''))
                        for h in header]))


# -------------------------------------------------------------------
#      EXECUTION
# -------------------------------------------------------------------
if __name__ == "__main__":
    main()
