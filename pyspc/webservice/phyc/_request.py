#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Webservice - PHyC - Client et requêtes
"""
from datetime import datetime as dt, timedelta as td
import pyspc.core.exception as _exception
from pyspc.convention.sandre import ELABVARNAMES


class Request(object):
    """
    Classe spécifique pour les requêtes SUDS
    """
    def __init__(self):
        """
        Requêtes SUDS
        """
#        super().__init__()

    def request_data_fcst_hydro(self, codes=None, models=None, varname='Q',
                                first_dtime=dt.utcnow(),
                                last_dtime=dt.utcnow()):
        """
        Service PHyC <publierPrevisionsMajBase>
        -> Prévisions selon date d'insertion dans la PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations
        first_dtime : datetime
            Premier instant à considérer (défaut: dt.utcnow())
        last_dtime : datetime
            Dernier instant à considérer (défaut: dt.utcnow())
        varname : str
            Nom de la variable selon convention PHyC (défaut: Q)
        models : list
            Liste des codes des modèles au sens POM
            (défaut: [], c'es-à-dire aucun filtre sur les modèles)

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        models = _exception.set_default(models, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        _exception.check_dt(first_dtime)
        _exception.check_dt(last_dtime)
        self.check_varname(varname)
        # Requête
        try:
            result = self.client\
                .service['PrevisionsPublicationPort']\
                .publierPrevisionsMajBase(
                    idsession=self.session,
                    listecdentite=codes,
                    filtremodele=models,
                    grandeur=varname,
                    dtinsertiondebut=dt.strftime(
                        first_dtime, "%Y-%m-%dT%H:%M:%S"),
                    dtinsertionfin=dt.strftime(
                        last_dtime, "%Y-%m-%dT%H:%M:%S"))
        except Exception:
            return None
        return result

    def request_data_obs_hydro(self, codes=None, varname='Q',
                               timestep=td(hours=1),
                               first_dtime=dt.utcnow(),
                               last_dtime=dt.utcnow(),
                               elab=None, plusvalide=None):
        """
        Service PHyC <publierObservationsHydroPasDeTemps>
        -> Hauteur horaire ou Débit horaire

        ou

        Service PHyC <publierObservationsHydro>
        -> Hauteur instantanée ou Débit instantané

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations
        first_dtime : datetime
            Premier instant à considérer (défaut: dt.utcnow())
        last_dtime : datetime
            Dernier instant à considérer (défaut: dt.utcnow())
        varname : str
            Nom de la variable selon convention PHyC (défaut: Q)
        timestep : timedelta
            Pas de temps fixe (défaut: 1h)

        Other Parameters
        ----------------
        elab : bool
            Données élaborées (seulement si varname = Q)
        plusvalide : bool
            Données les plus valides (seulement si elab=False et timestep=None)

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        elab = bool(elab is not None and elab)
        plusvalide = bool(plusvalide is not None and plusvalide)
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        _exception.check_dt(first_dtime)
        _exception.check_dt(last_dtime)
        self.check_varname(varname)
        # Requête
        try:
            if elab and varname == 'Q' or varname in ELABVARNAMES:
                result = self.client\
                    .service['ObservationsHydroElaboreesPublicationPort']\
                    .publierObservationsHydroElaborees(
                        idsession=self.session,
                        listecdentite=codes,
                        typeelabore=_set_typeelabore(
                            varname=varname, timestep=timestep),
                        dtmesuredebut=dt.strftime(
                            first_dtime, "%Y-%m-%dT%H:%M:%S"),
                        dtmesurefin=dt.strftime(
                            last_dtime, "%Y-%m-%dT%H:%M:%S"))
            elif timestep is None:
                result = self.client\
                    .service['ObservationsHydroPublicationPort']\
                    .publierObservationsHydro(
                        idsession=self.session,
                        listecdentite=codes,
                        grandeur=varname,
                        plusvalide=plusvalide,
                        dtmesuredebut=dt.strftime(
                            first_dtime, "%Y-%m-%dT%H:%M:%S"),
                        dtmesurefin=dt.strftime(
                            last_dtime, "%Y-%m-%dT%H:%M:%S"))
            else:
                _exception.check_td(timestep)
                timestep = int(timestep.seconds/60) + timestep.days * 24 * 60
                self.check_tstep(tstep=timestep)
                result = self.client\
                    .service['ObservationsHydroPublicationPort']\
                    .publierObservationsHydroPasDeTemps(
                        idsession=self.session,
                        listecdentite=codes,
                        grandeur=varname,
                        pasdetemps=timestep,
                        dtmesuredebut=dt.strftime(
                            first_dtime, "%Y-%m-%dT%H:%M:%S"),
                        dtmesurefin=dt.strftime(
                            last_dtime, "%Y-%m-%dT%H:%M:%S"))

        except ZeroDivisionError:  # Exception,
            return None
        return result

    def request_data_obs_meteo(self, codes=None, varname='RR', accum=False,
                               timestep=td(hours=1),
                               first_dtime=dt.utcnow(),
                               last_dtime=dt.utcnow()):
        """
        Service PHyC <publierObservationsMeteo>
        -> Précipitation ou température au pas de temps de collecte

        ou

        Service PHyC <publierPluvioCumulGlissant>
        -> Précipitation cumulée sur une période glissante

        Le choix du service est fait en fonction de l'argument <accum>

        Parameters
        ----------
        accum : bool
            Cumul à pas de temps fixe (False)
            ou sur une période glissante (True)
        codes : list
            Liste des identifiants des stations
        first_dtime : datetime
            Premier instant à considérer (défaut: dt.utcnow())
        last_dtime : datetime
            Dernier instant à considérer (défaut: dt.utcnow())
        varname : str
            Nom de la variable selon convention PHyC (défaut: RR)
        timestep : timedelta
            Pas de temps fixe (défaut: 1h)

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        _exception.check_dt(first_dtime)
        _exception.check_dt(last_dtime)
        self.check_varname(varname)
        # fix 2022-06-08 : TA + td(hours=1) -> cas else
        _exception.raise_valueerror(
            accum and varname != 'RR',
            "Grandeur {} n'est pas 'RR'".format(varname)
        )
        # fix 2022-06-08 : TA + td(hours=1) -> cas else
        _exception.raise_valueerror(
            timestep is not None and varname == 'TA',
            "Grandeur {} instantanée: timestep doit être None".format(varname)
        )
        # Requête
        try:
            if accum:
                _exception.check_td(timestep)
                timestep = int(timestep.seconds/60) + timestep.days * 24 * 60
                self.check_tstep(tstep=timestep)
                result = self.client\
                    .service['ObservationsMeteoPublicationPort']\
                    .publierPluvioCumulGlissant(
                        idsession=self.session,
                        listecdsitemeteo=codes,
                        grandeur=varname,
                        pdtcumul=timestep,
                        dtmesuredebut=dt.strftime(
                            first_dtime, "%Y-%m-%dT%H:%M:%S"),
                        dtmesurefin=dt.strftime(
                            last_dtime, "%Y-%m-%dT%H:%M:%S"))
            # fix 2022-06-08 : TA + td(hours=1) -> cas else
            elif timestep is not None and varname == 'RR':
                _exception.check_td(timestep)
                timestep = int(timestep.seconds/60) + timestep.days * 24 * 60
                self.check_tstep(tstep=timestep)
                result = self.client\
                    .service['ObservationsMeteoPublicationPort']\
                    .publierObservationsMeteo(
                        idsession=self.session,
                        listecdsitemeteo=codes,
                        grandeur=varname,
                        cmpre=timestep,
                        dtmesuredebut=dt.strftime(
                            first_dtime, "%Y-%m-%dT%H:%M:%S"),
                        dtmesurefin=dt.strftime(
                            last_dtime, "%Y-%m-%dT%H:%M:%S"))
            else:
                result = self.client\
                    .service['ObservationsMeteoPublicationPort']\
                    .publierObservationsMeteo(
                        idsession=self.session,
                        listecdsitemeteo=codes,
                        grandeur=varname,
                        dtmesuredebut=dt.strftime(
                            first_dtime, "%Y-%m-%dT%H:%M:%S"),
                        dtmesurefin=dt.strftime(
                            last_dtime, "%Y-%m-%dT%H:%M:%S"))
        except Exception:
            return None
        return result

    def request_flowmes(self, codes=None,
                        first_dtime=dt.utcnow(), last_dtime=dt.utcnow()):
        """
        Service PHyC <publierJaugeageHydro>
        -> Jaugeages connus dans la PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations
        first_dtime : datetime
            Premier instant à considérer (défaut: dt.utcnow())
        last_dtime : datetime
            Dernier instant à considérer (défaut: dt.utcnow())

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        _exception.check_dt(first_dtime)
        _exception.check_dt(last_dtime)
        # Requête
        try:
            result = self.client\
                .service['JaugeagePublicationPort']\
                .publierJaugeageHydro(
                    idsession=self.session,
                    listecdstationhydro=codes,
                    dtdebut=dt.strftime(
                        first_dtime, "%Y-%m-%dT%H:%M:%S"),
                    dtfin=dt.strftime(
                        last_dtime, "%Y-%m-%dT%H:%M:%S"))
        except Exception:
            return None
        return result

    def request_levelcor(self, codes=None,
                         first_dtime=dt.utcnow(), last_dtime=dt.utcnow()):
        """
        Service PHyC <publierCourbeCorrection>
        -> Courbes de Correction connues dans la PHyC


        Parameters
        ----------
        codes : list
            Liste des identifiants des stations
        first_dtime : datetime
            Premier instant à considérer (défaut: dt.utcnow())
        last_dtime : datetime
            Dernier instant à considérer (défaut: dt.utcnow())

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        _exception.check_dt(first_dtime)
        _exception.check_dt(last_dtime)
        # Requête
        try:
            result = self.client\
                .service['CourbeCorrectionPublicationPort']\
                .publierCourbeCorrection(
                    idsession=self.session,
                    listecdstationhydro=codes,
                    dtdebut=dt.strftime(
                        first_dtime, "%Y-%m-%dT%H:%M:%S"),
                    dtfin=dt.strftime(
                        last_dtime, "%Y-%m-%dT%H:%M:%S"))
        except Exception:
            return None
        return result

    def request_loc_meteo(self, codes=None):
        """
        Service PHyC <publierSiteMeteoListe>
        -> Liste des sites météo connus dans la PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        # Requête
        try:
            result = self.client\
                .service['SiteMeteoPublicationPort']\
                .publierSiteMeteoListe(
                    idsession=self.session,
                    listecdsitemeteo=codes)
        except Exception:
            return None
        return result

    def request_ratingcurve(self, codes=None,
                            first_dtime=dt.utcnow(), last_dtime=dt.utcnow()):
        """
        Service PHyC <publierCourbeTarage>
        -> Courbes de Tarage connues dans la PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations
        first_dtime : datetime
            Premier instant à considérer (défaut: dt.utcnow())
        last_dtime : datetime
            Dernier instant à considérer (défaut: dt.utcnow())

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        _exception.check_dt(first_dtime)
        _exception.check_dt(last_dtime)
        # Requête
        try:
            result = self.client\
                .service['CourbeTaragePublicationPort']\
                .publierCourbeTarage(
                    idsession=self.session,
                    listecdstationhydro=codes,
                    dtdebut=dt.strftime(
                        first_dtime, "%Y-%m-%dT%H:%M:%S"),
                    dtfin=dt.strftime(
                        last_dtime, "%Y-%m-%dT%H:%M:%S"))
        except Exception:
            return None
        return result

    def request_site_hydro(self, codes=None):
        """
        Service PHyC <publierSiteHydroListe>
        -> Liste des sites hydro connus dans la PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        # Requête
        try:
            result = self.client\
                .service['SiteHydroPublicationPort']\
                .publierSiteHydroListe(
                    idsession=self.session,
                    listecdsitehydro=codes)
        except Exception:
            return None
        return result

    def request_site_hydro_by_zone(self, codes=None):
        """
        Service PHyC <publierSiteHydroZoneHydro>
        -> Liste des sites hydro associés à une zone hydro

        Parameters
        ----------
        codes : list
            Liste des identifiants des zones hydro

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        # Requête
        try:
            result = self.client\
                .service['SiteHydroPublicationPort']\
                .publierSiteHydroZoneHydro(
                    idsession=self.session,
                    listecdzonehydro=codes)
        except Exception:
            return None
        return result

    def request_station_hydro(self, codes=None):
        """
        Service PHyC <publierStationHydroListe>
        -> Liste des stations hydro connus dans la PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        # Requête
        try:
            result = self.client\
                .service['SiteHydroPublicationPort']\
                .publierStationHydroListe(
                    idsession=self.session,
                    listecdstationhydro=codes)
        except Exception:
            return None
        return result

    def request_station_hydro_by_site(self, codes=None):
        """
        Service PHyC <publierStationHydroSiteHydro>
        -> Liste des stations hydro associés à un site hydro

        Parameters
        ----------
        codes : list
            Liste des identifiants des sites hydro

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        # Requête
        try:
            result = self.client\
                .service['SiteHydroPublicationPort']\
                .publierStationHydroSiteHydro(
                    idsession=self.session,
                    listecdsitehydro=codes)
        except Exception:
            return None
        return result

    def request_capteur_hydro(self, codes=None):
        """
        Service PHyC <publierCapteurHydroListe>
        -> Liste des capteurs hydro connus dans la PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des capteurs

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        # Requête
        try:
            result = self.client\
                .service['SiteHydroPublicationPort']\
                .publierCapteurHydroListe(
                    idsession=self.session,
                    listecdcapteurhydro=codes)
        except Exception:
            return None
        return result

    def request_capteur_hydro_by_station(self, codes=None):
        """
        Service PHyC <publierCapteurHydroStationHydro>
        -> Liste des capteurs hydro associés à une station hydro

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations hydro

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        # Requête
        try:
            result = self.client\
                .service['SiteHydroPublicationPort']\
                .publierCapteurHydroStationHydro(
                    idsession=self.session,
                    listecdstationhydro=codes)
        except Exception:
            return None
        return result

    def request_user(self, codes=None):
        """
        Service PHyC <publierContactListe>
        -> Liste des contacts connus dans la PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        # Requête
        try:
            result = self.client\
                .service['ContactPublicationPort']\
                .publierContactListe(
                    idsession=self.session,
                    listecdcontact=codes)
        except Exception:
            return None
        return result

    def request_user_loc_meteo(self, codes=None):
        """
        Service PHyC <publierSiteMeteoContact>
        -> Liste des sites meteo connus dans la PHyC et affiliés aux contacts

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        # Requête
        try:
            result = self.client\
                .service['SiteMeteoPublicationPort']\
                .publierSiteMeteoContact(
                    idsession=self.session,
                    listecdcontact=codes)
        except Exception:
            return None
        return result

    def request_user_site_hydro(self, codes=None):
        """
        Service PHyC <publierSiteHydroContact>
        -> Liste des sites hydro connus dans la PHyC et affiliés aux contacts

        Parameters
        ----------
        codes : list
            Liste des identifiants des contacts

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        # Requête
        try:
            result = self.client\
                .service['SiteHydroPublicationPort']\
                .publierSiteHydroContact(
                    idsession=self.session,
                    listecdcontact=codes)
        except Exception:
            return None
        return result

    def request_user_station_hydro(self, codes=None):
        """
        Service PHyC <publierStationHydroContact>
        -> Liste des stations hydro connus dans la PHyC
        et affiliés aux contacts

        Parameters
        ----------
        codes : list
            Liste des identifiants des contacts

        Returns
        -------
        result : suds.sudsobject
            Résultat de la requête PHyC

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        # Requête
        try:
            result = self.client\
                .service['SiteHydroPublicationPort']\
                .publierStationHydroContact(
                    idsession=self.session,
                    listecdcontact=codes)
        except Exception:
            return None
        return result


def _set_typeelabore(varname=None, timestep=None):
    """
    Définir le type de la grandeur élaborée
    """
    if varname in ELABVARNAMES:
        return varname
    if timestep >= td(days=30):
        return 'QmM'
    return 'QmJ'
