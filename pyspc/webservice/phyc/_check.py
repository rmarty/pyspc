#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Webservice - PHyC - Client et requêtes
"""
import pyspc.core.exception as _exception
from pyspc.convention.sandre import DATATYPES, ELABVARNAMES, VARNAMES


class Check(object):
    """
    Classe spécifique pour la vérification des paramètres et du XML
    """
    def __init__(self):
        """
        Vérification des paramètres et du XML
        """
        super().__init__()
        self.session = None

    def check_client(self):
        """
        Contrôler si le client SUDS existe
        """
        import suds.client as _sclient
        if not isinstance(self.client, _sclient.Client):
            self.session = None
            raise ValueError("Le Client SOAP n'en est pas un.")

    def check_session(self):
        """
        Contrôler si la session SOAP existe
        """
        try:
            _exception.check_notnone(self.session)
        except ValueError as ve:
            raise ValueError("Session inconnue") from ve

    @staticmethod
    def check_dtype(dtype):
        """
        Contrôler s'il s'agit bien d'un export autorisé
        """
        try:
            DATATYPES.index(dtype)
        except ValueError as ve:
            raise ValueError("Export incorrect") from ve

    @staticmethod
    def check_tstep(tstep):
        """
        Contrôler s'il s'agit bien d'un pas de temps en minutes entières
        """
        _exception.check_int(tstep)
        if tstep <= 0:
            raise ValueError("Pas de temps fixe incorrect: il n'est pas un "
                             "entier ou est négatif")

    @staticmethod
    def check_varname(varname):
        """
        Contrôler la grandeur
        """
        if varname not in VARNAMES and varname not in ELABVARNAMES:
            raise ValueError("Grandeur incorrecte")

    @staticmethod
    def _process_xml_return(xml_return=None, key=None,
                            control_size=False):
        """
        Traiter le retour du XML.

        Parameters
        ----------
        xml_return
            Contenu XML complet de la requête
        key : tuple
            Identifiant de la requête
        control_size : bool
            Contrôler la taille du retour?
            .. warning::
                NotImplementedError

        Return
        ------
        xml.xmlprevcrues
            Contenu XML en cas de réussite de la requête

        Raises
        ------
        ValueError
            - soulève une erreur en cas non-réponse de la PHyC
            - soulève une erreur si le statut du retour est != 0

        .. seealso:: libhydro

        """
        if key is None:
            key = []
        key = [str(k) for k in key]
        if xml_return is None:
            raise ValueError("La requête PHyC ({}) n'a pas abouti"
                             "".format(', '.join(key)))
        if int(float(xml_return.statut)) != 0:
            raise ValueError("La requête PHyC ({}) n'est pas valide "
                             "(statut !=0)".format(', '.join(key)))
        if int(float(xml_return.statut)) == 0:
            if xml_return.xmlprevcrues is None:
                _exception.Warning(
                    __name__,
                    "Retour PHyC vide. "
                    'Le rapport indique "{}"'
                    "" .format(xml_return.rapport))
            return xml_return.xmlprevcrues
        if control_size:
            raise NotImplementedError
        return None

    @classmethod
    def get_datatypes(cls):
        """
        Renvoyer la liste des types de données PHyC

        Returns
        -------
        list:
            Types de données

        See Also
        --------
        pyspc.convention.sandre.DATATYPES

        """
        return sorted(DATATYPES)

    @classmethod
    def get_varnames(cls):
        """
        Renvoyer la liste des grandeurs de données PHyC

        Returns
        -------
        list:
            Grandeurs

        See Also
        --------
        pyspc.convention.sandre.VARNAMES

        """
        return sorted(VARNAMES)
