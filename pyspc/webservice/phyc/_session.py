#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Webservice - PHyC - Client et requêtes
"""
import pyspc.core.exception as _exception


class Session(object):
    """
    Classe spécifique pour les sessions SUDS
    """
    def __init__(self):
        """
        Sessions SUDS
        """
        super().__init__()
        self.session = None

    def login(self):
        """
        Authentification PHyC
        """
        # Contrôles
        self.check_client()
        # Login
        try:
            login = self.client.service['AuthentificationPort']\
                .authentifier(
                    cdcontact=self.username,
                    motdepasse=self.password
                    )
        except Exception as e:
            raise _exception.Error(
                "Erreur lors de la connexion à la PHyC avec username={} : {}"
                "".format(self.username, e.args[0]))
        if login.statut == '0':
            self.session = login.idsession
        else:
            _exception.Warning(
                __name__,
                "Connexion refusée:\n {}"
                "" .format(login.rapport))
            self.session = None

    def logout(self):
        """
        Déconnexion PHyC
        """
        # Contrôles
        self.check_client()
        # Logout
        try:
            self.check_session()
        except ValueError:
            pass
        else:
            self.client.service['AuthentificationPort']\
                       .fermerSession(cdcontact=self.username,
                                      idsession=self.session)
            self.session = None
