#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Webservice - PHyC - Client et requêtes
"""
import collections
from datetime import datetime as dt, timedelta as td
import os.path

from pyspc.convention.sandre import LOC_HYDRO_TYPES
import pyspc.core.exception as _exception


class Get(object):
    """
    Classe spécifique pour la récupération des flux XML
    """
    def __init__(self):
        """
        Récupération des flux XML
        """
#        super().__init__()

    def get(self, datatype=None,
            codes=None, varname=None, timestep=td(hours=1), models=None,
            first_dtime=dt.utcnow(), last_dtime=dt.utcnow(),
            elab=None, plusvalide=None):
        """
        Récupération de données/informations contenues dans la PHyC

        Parameters
        ----------
        datatype : str
            Type de données
            - ratingcurve     : courbes de tarage
            - levelcor        : courbes de correction
            - flowmes         : jaugeages
            - loc_hydro       : siteshydro, stationshydro
            - loc_meteo       : sitesmeteo
            - data_obs_hydro  : données observées hydro
            - data_obs_meteo  : données observées meteo
            - data_fcst_hydro : données prévues hydro
            - user            : informations utilisateur
        codes : list
            Liste des identifiants des stations
        first_dtime : datetime
            Premier instant à considérer (défaut: dt.utcnow())
        last_dtime : datetime
            Dernier instant à considérer (défaut: dt.utcnow())
        varname : str
            Nom de la variable selon convention PHyC (défaut: Q , RR)
        timestep : timedelta
            Pas de temps (défaut: timedelta(hours=1))
        models : list
            Liste des codes des modèles au sens POM
            (défaut: [], c'es-à-dire aucun filtre sur les modèles)

        Other Parameters
        ----------------
        elab : bool
            Données élaborées (seulement si varname = Q)
        plusvalide : bool
            Données les plus valides

        Returns
        -------
        data : dict
            Dictionnaire des contenus XML
            clé: (code, datatype, varname, first_dtime, last_dtime)

        """
#        if codes is None:
#            codes = []
#        if models is None:
#            models = []
        if datatype == 'data_fcst_hydro':
            if varname is None:
                varname = 'Q'
            return self.get_data_fcst_hydro(
                codes=codes,
                varname=varname,
                models=models,
                first_dtime=first_dtime,
                last_dtime=last_dtime
            )
        if datatype == 'data_obs_hydro':
            if varname is None:
                varname = 'Q'
            return self.get_data_obs_hydro(
                codes=codes,
                varname=varname,
                timestep=timestep,
                first_dtime=first_dtime,
                last_dtime=last_dtime,
                elab=elab,
                plusvalide=plusvalide
            )
        if datatype == 'data_obs_meteo':
            if varname is None:
                varname = 'RR'
            return self.get_data_obs_meteo(
                codes=codes,
                varname=varname,
                timestep=timestep,
                first_dtime=first_dtime,
                last_dtime=last_dtime
            )
        if datatype == 'flowmes':
            return self.get_flowmes(
                codes=codes,
                first_dtime=first_dtime,
                last_dtime=last_dtime
            )
        if datatype == 'levelcor':
            return self.get_levelcor(
                codes=codes,
                first_dtime=first_dtime,
                last_dtime=last_dtime
            )
        if datatype == 'loc_hydro':
            return self.get_loc_hydro(
                codes=codes
            )
        if datatype == 'loc_hydro_child':
            return self.get_loc_hydro(
                codes=codes, child=True
            )
        if datatype == 'loc_meteo':
            return self.get_loc_meteo(
                codes=codes
            )
        if datatype == 'ratingcurve':
            return self.get_ratingcurve(
                codes=codes,
                first_dtime=first_dtime,
                last_dtime=last_dtime
            )
        if datatype == 'user':
            return self.get_user(
                codes=codes
            )
        raise ValueError('Type de données incorrect')

    def get_data_fcst_hydro(self, codes=None, varname='Q', models=None,
                            first_dtime=dt.utcnow(),
                            last_dtime=dt.utcnow()):
        """
        Récupération des prévisions PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations
        first_dtime : datetime
            Premier instant à considérer (défaut: dt.utcnow())
        last_dtime : datetime
            Dernier instant à considérer (défaut: dt.utcnow())
        varname : str
            Nom de la variable selon convention PHyC (défaut: Q)
        models : list
            Liste des codes des modèles au sens POM
            (défaut: [], c'es-à-dire aucun filtre sur les modèles)

        Returns
        -------
        data : dict
            Dictionnaire des contenus XML
            clé: (code, 'data_fcst_hydro', varname, first_dtime, last_dtime)

        """
        codes = _exception.set_default(codes, default=[])
        models = _exception.set_default(models, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        _exception.check_dt(first_dtime)
        _exception.check_dt(last_dtime)
        # Requêtes et traitements des retours
        data = collections.OrderedDict()
        for code in codes:
            xml_return = self.request_data_fcst_hydro(
                codes=[code],
                models=models,
                varname=varname,
                first_dtime=first_dtime,
                last_dtime=last_dtime
            )
            key = (code, 'data_fcst_hydro', varname,
                   first_dtime, last_dtime)
            content = self._process_xml_return(
                xml_return=xml_return,
                key=key,
                control_size=True
            )
            if isinstance(content, int):
                subperiods = self._define_subperiods(
                    start=first_dtime,
                    end=last_dtime,
                    size=content
                )
                for s in subperiods:
                    try:
                        content = self.get_data_fcst_hydro(
                            codes=[code],
                            models=models,
                            varname=varname,
                            first_dtime=s[0],
                            last_dtime=s[-1]
                        )
                    except ValueError:
                        pass
                    else:
                        data.update(content)
            else:
                data[key] = content
        return data

    def get_data_obs_hydro(self, codes=None, varname='Q',
                           timestep=td(hours=1),
                           first_dtime=dt.utcnow(),
                           last_dtime=dt.utcnow(),
                           elab=None, plusvalide=None):
        """
        Récupération des observations hydrométriques en PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations
        first_dtime : datetime
            Premier instant à considérer (défaut: dt.utcnow())
        last_dtime : datetime
            Dernier instant à considérer (défaut: dt.utcnow())
        varname : str
            Nom de la variable selon convention PHyC (défaut: Q)
        timestep : timedelta
            Pas de temps (défaut: timedelta(hours=1))

        Other Parameters
        ----------------
        elab : bool
            Données élaborées (seulement si varname = Q)
        plusvalide : bool
            Données les plus valides

        Returns
        -------
        data : dict
            Dictionnaire des contenus XML
            clé: (code, 'data_obs_hydro', varname, first_dtime, last_dtime)

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        _exception.check_dt(first_dtime)
        _exception.check_dt(last_dtime)
        # Requêtes et traitements des retours
        data = collections.OrderedDict()
        for code in codes:
            xml_return = self.request_data_obs_hydro(
                codes=[code],
                varname=varname,
                timestep=timestep,
                first_dtime=first_dtime,
                last_dtime=last_dtime,
                elab=elab,
                plusvalide=plusvalide
            )
            key = (code, 'data_obs_hydro', varname,
                   first_dtime, last_dtime)
            content = self._process_xml_return(
                xml_return=xml_return,
                key=key,
                control_size=True
            )
            if isinstance(content, int):
                subperiods = self._define_subperiods(
                    start=first_dtime,
                    end=last_dtime,
                    size=content
                )
                for s in subperiods:
                    try:
                        content = self.get_data_obs_hydro(
                            codes=[code],
                            varname=varname,
                            timestep=timestep,
                            first_dtime=s[0],
                            last_dtime=s[-1],
                            elab=elab,
                            plusvalide=plusvalide
                        )
                    except ValueError:
                        pass
                    else:
                        data.update(content)
            else:
                data[key] = content
        return data

    def get_data_obs_meteo(self, codes=None, varname='RR',
                           timestep=td(hours=1),
                           first_dtime=dt.utcnow(),
                           last_dtime=dt.utcnow()):
        """
        Récupération des observations météorologiques en PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations
        first_dtime : datetime
            Premier instant à considérer (défaut: dt.utcnow())
        last_dtime : datetime
            Dernier instant à considérer (défaut: dt.utcnow())
        varname : str
            Nom de la variable selon convention PHyC (défaut: RR)
        timestep : timedelta
            Pas de temps (défaut: timedelta(hours=1))

        Returns
        -------
        data : dict
            Dictionnaire des contenus XML
            clé: (code, 'data_obs_meteo', varname, first_dtime, last_dtime)

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        _exception.check_dt(first_dtime)
        _exception.check_dt(last_dtime)
        # Requêtes et traitements des retours
        data = collections.OrderedDict()
        for code in codes:
            xml_return = self.request_data_obs_meteo(
                codes=[code],
                varname=varname,
                timestep=timestep,
                first_dtime=first_dtime,
                last_dtime=last_dtime
            )
            key = (code, 'data_obs_meteo', varname,
                   first_dtime, last_dtime)
            content = self._process_xml_return(
                xml_return=xml_return,
                key=key,
                control_size=True
            )
            if isinstance(content, int):
                subperiods = self._define_subperiods(
                    start=first_dtime,
                    end=last_dtime,
                    size=content
                )
                for s in subperiods:
                    try:
                        content = self.get_data_obs_meteo(
                            codes=[code],
                            varname=varname,
                            timestep=timestep,
                            first_dtime=s[0],
                            last_dtime=s[-1]
                        )
                    except ValueError:
                        pass
                    else:
                        data.update(content)
            else:
                data[key] = content
        return data

    def get_flowmes(self, codes=None,
                    first_dtime=dt.utcnow(), last_dtime=dt.utcnow()):
        """
        Récupérer les jaugeages de la PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations
        first_dtime : datetime
            Premier instant à considérer (défaut: dt.utcnow())
        last_dtime : datetime
            Dernier instant à considérer (défaut: dt.utcnow())

        Returns
        -------
        data : dict
            Dictionnaire des contenus XML
            clé: (code, 'flowmes', first_dtime, last_dtime)

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        _exception.check_dt(first_dtime)
        _exception.check_dt(last_dtime)
        # Requêtes et traitements des retours
        data = collections.OrderedDict()
        for code in codes:
            xml_return = self.request_flowmes(
                codes=[code],
                first_dtime=first_dtime,
                last_dtime=last_dtime
            )
            key = (code, 'flowmes', first_dtime, last_dtime)
            data[key] = self._process_xml_return(
                xml_return=xml_return,
                key=key
            )
        # Renvoi du dictionnaire de contenu XML
        return data

    def get_levelcor(self, codes=None,
                     first_dtime=dt.utcnow(), last_dtime=dt.utcnow()):
        """
        Récupérer les courbes de correction de la PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations
        first_dtime : datetime
            Premier instant à considérer (défaut: dt.utcnow())
        last_dtime : datetime
            Dernier instant à considérer (défaut: dt.utcnow())

        Returns
        -------
        data : dict
            Dictionnaire des contenus XML
            clé: (code, 'levelcor', first_dtime, last_dtime)

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        _exception.check_dt(first_dtime)
        _exception.check_dt(last_dtime)
        # Requêtes et traitements des retours
        data = collections.OrderedDict()
        for code in codes:
            if len(code) != 10:
                _exception.Warning(
                    __name__,
                    "L'identifiant {} doit contenir 10 caractères et non {}"
                    "" .format(code, len(code)))
                continue
            xml_return = self.request_levelcor(
                codes=[code],
                first_dtime=first_dtime,
                last_dtime=last_dtime
            )
            key = (code, 'levelcor', first_dtime, last_dtime)
            data[key] = self._process_xml_return(
                xml_return=xml_return,
                key=key
            )
        # Renvoi du dictionnaire de contenu XML
        return data

    def get_loc_hydro(self, codes=None, child=False):
        """
        Récupérer les sites/stations hydro connus dans la PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des sites/stations

        Other Parameters
        ----------------
        child : bool
            Entités 'filles' associée aux entités demandées. Par défaut: False

        Returns
        -------
        data : dict
            Dictionnaire des contenus XML
            clé: (code, 'loc_hydro')

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        # Requêtes et traitements des retours
        data = collections.OrderedDict()
        for code in codes:
            if len(code) == 8 and not child:
                xml_return = self.request_site_hydro(
                    codes=[code]
                )
            elif len(code) == 10 and not child:
                xml_return = self.request_station_hydro(
                    codes=[code]
                )
            elif len(code) == 12 and not child:
                xml_return = self.request_capteur_hydro(
                    codes=[code]
                )
            elif len(code) == 4 and child:
                xml_return = self.request_site_hydro_by_zone(
                    codes=[code]
                )
            elif len(code) == 8 and child:
                xml_return = self.request_station_hydro_by_site(
                    codes=[code]
                )
            elif len(code) == 10 and child:
                xml_return = self.request_capteur_hydro_by_station(
                    codes=[code]
                )
            else:
                continue
            if child:
                key = (code, 'loc_hydro_child')
            else:
                key = (code, 'loc_hydro')
            data[key] = self._process_xml_return(
                xml_return=xml_return,
                key=key
            )
        # Renvoi du dictionnaire de contenu XML
        return data

    def get_loc_meteo(self, codes=None):
        """
        Récupérer les sites météo connus dans la PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations

        Returns
        -------
        data : dict
            Dictionnaire des contenus XML
            clé: (code, 'loc_meteo')

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        # Requêtes et traitements des retours
        data = collections.OrderedDict()
        for code in codes:
            xml_return = self.request_loc_meteo(
                codes=[code]
            )
            key = (code, 'loc_meteo')
            data[key] = self._process_xml_return(
                xml_return=xml_return,
                key=key
            )
        # Renvoi du dictionnaire de contenu XML
        return data

    def get_ratingcurve(self, codes=None,
                        first_dtime=dt.utcnow(), last_dtime=dt.utcnow()):
        """
        Récupérer les courbes de tarage de la PHyC

        Parameters
        ----------
        codes : list
            Liste des identifiants des stations
        first_dtime : datetime
            Premier instant à considérer (défaut: dt.utcnow())
        last_dtime : datetime
            Dernier instant à considérer (défaut: dt.utcnow())

        Returns
        -------
        data : dict
            Dictionnaire des contenus XML
            clé: (code, 'ratingcurve', first_dtime, last_dtime)

        """
        # Contrôles
        codes = _exception.set_default(codes, default=[])
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        _exception.check_dt(first_dtime)
        _exception.check_dt(last_dtime)
        # Requêtes et traitements des retours
        data = collections.OrderedDict()
        for code in codes:
            if len(code) != 10:
                _exception.Warning(
                    __name__,
                    "L'identifiant {} doit contenir 10 caractères et non {}"
                    "" .format(code, len(code)))
                continue
            xml_return = self.request_ratingcurve(
                codes=[code],
                first_dtime=first_dtime,
                last_dtime=last_dtime
            )
            key = (code, 'ratingcurve', first_dtime, last_dtime)
            data[key] = self._process_xml_return(
                xml_return=xml_return,
                key=key
            )
        # Renvoi du dictionnaire de contenu XML
        return data

    def get_user(self, codes=None):
        """
        Récupérer les informations des contacts (user)

        Parameters
        ----------
        codes : list
            Liste des identifiants des contacts

        Returns
        -------
        data : dict
            Dictionnaire des contenus XML
            clés: (code, 'admin')     : informations administratives
            (code, 'site_hydro')      : informations Site Hydro
            (code, 'station_hydro')   : informations Station Hydro
            (code, 'site_meteo')      : informations Site Meteo

        """
        codes = _exception.set_default(codes, default=[])
        # Contrôles
        self.check_client()
        self.check_session()
        _exception.check_listlike(codes)
        # Requêtes et traitements des retours
        data = collections.OrderedDict()
        for code in codes:
            # -----------------------------------------------------------------
            # Informations administratives
            # -----------------------------------------------------------------
            xml_return = self.request_user(
                codes=[code]
            )
            key = (code, 'user_admin')
            data[key] = self._process_xml_return(
                xml_return=xml_return,
                key=key
            )
            # -----------------------------------------------------------------
            # Informations Site Hydro
            # -----------------------------------------------------------------
            xml_return = self.request_user_site_hydro(
                codes=[code]
            )
            key = (code, 'user_site_hydro')
            data[key] = self._process_xml_return(
                xml_return=xml_return,
                key=key
            )
            # -----------------------------------------------------------------
            # Informations Station Hydro
            # -----------------------------------------------------------------
            xml_return = self.request_user_station_hydro(
                codes=[code]
            )
            key = (code, 'user_station_hydro')
            data[key] = self._process_xml_return(
                xml_return=xml_return,
                key=key
            )
            # -----------------------------------------------------------------
            # Informations Site Meteo
            # -----------------------------------------------------------------
            xml_return = self.request_user_loc_meteo(
                codes=[code]
            )
            key = (code, 'user_loc_meteo')
            data[key] = self._process_xml_return(
                xml_return=xml_return,
                key=key
            )
        # Renvoi du dictionnaire de contenu XML
        return data

    def retrieve(self, dirname='.', datatype=None,
                 codes=None, varname=None, timestep=td(hours=1), models=None,
                 first_dtime=dt.utcnow(), last_dtime=dt.utcnow(),
                 elab=None, plusvalide=None):
        """
        Enregistrement de données/informations contenues dans la PHyC

        Parameters
        ----------
        dirname : str
            Dossier de sortie
        datatype : str
            Type de données

            - data_fcst_hydro : données prévues hydro
            - data_obs_hydro  : données observées hydro
            - data_obs_meteo  : données observées meteo
            - flowmes         : jaugeages
            - levelcor        : courbes de correction
            - ratingcurve     : courbes de tarage
            - loc_hydro       : siteshydro, stationshydro
            - loc_meteo       : sitesmeteo
            - user            : informations utilisateur

        codes : list
            Liste des identifiants des stations
        first_dtime : datetime
            Premier instant à considérer (défaut: dt.utcnow())
        last_dtime : datetime
            Dernier instant à considérer (défaut: dt.utcnow())
        varname : str
            Nom de la variable selon convention PHyC (défaut: Q)
        timestep : timedelta
            Pas de temps (défaut: timedelta(hours=1))
        models : list
            Liste des codes des modèles au sens POM
            (défaut: [], c'es-à-dire aucun filtre sur les modèles)

        Other Parameters
        ----------------
        elab : bool
            Données élaborées (seulement si varname = Q)
        plusvalide : bool
            Données les plus valides

        Returns
        -------
        list
            Fichiers XML contenant les données de la PHyC

        """
#        if codes is None:
#            codes = []
#        if models is None:
#            models = []
        content = self.get(
            datatype=datatype,
            codes=codes,
            varname=varname,
            timestep=timestep,
            models=models,
            first_dtime=first_dtime,
            last_dtime=last_dtime,
            elab=elab
        )
        filenames = []
        for key in content:
            try:
                _exception.check_str(content[key])
            except ValueError:
                _exception.Warning(
                    __name__,
                    "Contenu vide ou incorrect pour la clé {}"
                    "" .format(key))
                continue
            datatype = key[1]
            info = datatype.split('_')
            if info[0] == 'data':
                basename = '{code}_{fdt}_{ldt}_{vname}_{suffix}.xml'.format(
                    code=key[0],
                    fdt=key[3].strftime('%Y%m%d%H%M'),
                    ldt=key[4].strftime('%Y%m%d%H%M'),
                    vname=key[2],
                    suffix=info[1]
                )
            elif datatype in ['flowmes', 'levelcor', 'ratingcurve']:
                basename = '{code}_{fdt}_{ldt}_{suffix}.xml'.format(
                    code=key[0],
                    fdt=key[2].strftime('%Y%m%d%H%M'),
                    ldt=key[3].strftime('%Y%m%d%H%M'),
                    suffix=datatype
                )
            elif datatype == 'loc_hydro':
                code = key[0]
                suffix = LOC_HYDRO_TYPES.get(len(code), 'loc_hydro')
                basename = '{code}_{suffix}.xml'.format(
                    code=code,
                    suffix=suffix
                )
            elif datatype == 'loc_hydro_child':
                code = key[0]
                suffix = LOC_HYDRO_TYPES.get(len(code), 'loc_hydro')
                basename = '{code}_{suffix}_child.xml'.format(
                    code=code,
                    suffix=suffix
                )
            elif datatype == 'loc_meteo':
                basename = '{code}_SiteMeteo.xml'.format(
                    code=key[0]
                )
            elif datatype.startswith('user_'):
                basename = '{code}_{suffix}.xml'.format(
                    code=key[0],
                    suffix=datatype
                )
            else:
                raise NotImplementedError
            filename = os.path.join(dirname, basename)
            with open(filename, 'w', encoding='utf-8', newline="\n") as f:
                f.write(content[key])
            filenames.append(filename)
        return filenames
