#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Webservice - Projet LAMEDO - BdApBp
"""
import os.path
import pyspc.core.exception as _exception
from pyspc.webservice._basic import _Basic_webservice
from pyspc.convention.lamedo import BDAPBP_DATATYPES, HOSTNAMES


class BdApbp(_Basic_webservice):
    """
    Structure de données BdApbp

    Attributes
    ----------
    hostname : str
        Hôte du webservice
    proxies : None, dict
        Dictionnaire des proxys {'protocol': 'proxy'}
    timeout : None, int
        Durée maximale de la requête
    url : None, str
        Adresse de la requête
    filename : None, str
        Fichier enregistré en local
    """
    def __init__(self, hostname=None, proxies=None, timeout=None):
        """
        Instanciation du wbeservice

        Parameters
        ----------
        hostname : str
            Hôte du webservice
        proxies : None, dict
            Dictionnaire des proxys {'protocol': 'proxy'}
        timeout : None, int
            Durée maximale de la requête

        """
        if proxies is not None:
            _exception.Warning(
                __name__,
                "Le webservice BdApBp du projet LAMEDO est uniquement "
                "accessible depuis le RIE. Il n'est donc pas nécessaire de "
                "définir le proxy. Il est défini ainsi: proxies={}")
        proxies = {}
        if hostname is None and 'bdapbp' in HOSTNAMES:
            hostname = HOSTNAMES['bdapbp']
        super().__init__(
            hostname=hostname,
            proxies=proxies,
            timeout=timeout,
        )

    def __str__(self):
        """
        Afficher les méta-données de l'instance BdApBp
        """
        text = """
        *************************************
        ********* WEBSERVICE - BdApBp *******
        *************************************
        *  HÔTE             = {hostname}
        *  PROXYS           = {proxies}
        *  TIMEOUT          = {timeout}
        *  DERNIERE URL     = {url}
        *  DERNIER FICHIER  = {filename}
        *************************************
        """
        return text.format(**vars(self))

    def set_filename(self, zones=None, date=None, dtype=None, dirname='.'):
        """
        Définir le fichier du bulletin APBP à télécharger

        Parameters
        ----------
        zones : str, list
            Identifiants de zone AP
        date : datetime
            Date du bulletin APBP
        dtype : str
            Format du retour JSON
            - short : retour concis
            - long : retour complet (défaut)
        dirname : str
            Répertoire local de stockage

        .. seealso::
            pyspc.webservice.lamedo.convention.BDAPBP_DATATYPES

        """
        # Contrôles
        if isinstance(zones, str):
            zones = [zones]
        _exception.check_listlike(zones)
        _exception.check_dt(date)
        _exception.raise_valueerror(
            dtype not in self.get_types(),
            'Le format du retour JSON est incorrect'
        )
        self.filename = os.path.join(
            dirname,
            '{zones}_{date}_bp-{dtype}.json'.format(
                zones='-'.join(zones),
                date=date.strftime('%Y%m%d%H%M'),
                dtype=dtype
            )
        )

    def set_url(self, zones=None, date=None, dtype=None):
        """
        Définir l'url du bulletin APBP à télécharger

        Parameters
        ----------
        zones : str, list
            Identifiants de zone AP
        date : datetime
            Date du bulletin APBP
        dtype : str
            Format du retour JSON
            - short : retour concis
            - long : retour complet (défaut)

        .. seealso::
            pyspc.webservice.lamedo.convention.BDAPBP_DATATYPES

        """
        # Contrôles
        if isinstance(zones, str):
            zones = [zones]
        _exception.check_listlike(zones)
        _exception.check_dt(date)
        _exception.raise_valueerror(
            dtype not in self.get_types(),
            'Le format du retour JSON est incorrect'
        )
        # URL
        self.url = "{hostname}?service=bdlamedoGet&version=201107"\
            "&request=getReplayBpValues"\
            "&format={dtype}&zones={zones}&date={date}"\
            "".format(
                    hostname=self.hostname,
                    dtype=dtype,
                    zones='+'.join(zones),
                    date=date.strftime('%Y%m%d%H%M')
            )

    @classmethod
    def get_types(cls):
        """
        Liste des formats des données ApBp au format JSON

        - short : retour concis
        - long : retour complet

        """
        return sorted(BDAPBP_DATATYPES)
