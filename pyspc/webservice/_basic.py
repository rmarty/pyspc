#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Webservice - Basic Url Manager
"""
from ._proxies import setproxies_byconfig
import pyspc.core.exception as _exception

TIMEOUT = 300


class _Basic_webservice():
    """
    Structure de données  _Basic_webservice

    Attributes
    ----------
    hostname : str
        Hôte du webservice
    proxies : None, dict
        Dictionnaire des proxys {'protocol': 'proxy'}
    timeout : None, int
        Durée maximale de la requête
    url : None, str
        Adresse de la requête
    filename : None, str
        Fichier enregistré en local
    verify : None, str, bool
        Vérification du certificat SSL. Voir requests.get

    """
    def __init__(self, hostname=None, proxies=None, timeout=None):
        """
        Instanciation du wbeservice

        Parameters
        ----------
        hostname : str
            Hôte du webservice
        proxies : None, dict
            Dictionnaire des proxys {'protocol': 'proxy'}
        timeout : None, int
            Durée maximale de la requête

        """
        self.hostname = hostname
        if isinstance(proxies, dict):
            self.proxies = proxies
        else:
            self.proxies = setproxies_byconfig(proxies)
        if isinstance(timeout, int):
            self.timeout = timeout
        else:
            self.timeout = TIMEOUT
        self.url = None
        self.filename = None
        self.verify = None

    def retrieve(self, engine=None):
        """
        Récupérer un objet depuis une url

        Parameters
        ----------
        engine : str
            Outil de récupération. Défaut: 'urllib'
            - urllib : https://docs.python.org/fr/3/library/urllib.html
            - requests : https://requests.readthedocs.io/en/master/

        Returns
        -------
        None
            Si la requête est incorrecte
        res : http.client.HTTPResponse
            Si engine == 'urllib'
            La réponse de 'urllib.request.urlopen' si self.filename is None
        res : requests.Response
            La réponse de 'requests.get' si self.filename is None
            Si engine == 'requests'
        filename : str
            Le fichier enregistré

        """
        if engine is None or engine == 'urllib':
            return self.retrieve_byurllib()
        if engine == 'requests':
            return self.retrieve_byrequests()
        raise ValueError("'engine' est incorrect")

    def retrieve_byrequests(self):
        """
        Récupérer un objet depuis une url par 'urllib'

        Parameters
        ----------
        verify : bool, str
            Paramètre de vérification SSL de requests.get

        Returns
        -------
        None
            Si la requête est incorrecte
        res : requests.Response
            La réponse de 'requests.get' si self.filename is None
        filename : str
            Le fichier enregistré

        .. seealso:: https://requests.readthedocs.io/en/master/

        """
        # Imports
        import requests
        # Contrôles
        _exception.check_dict(self.proxies)
        _exception.check_int(self.timeout)
        _exception.check_str(self.url)
        try:
            res = requests.get(
                url=self.url, proxies=self.proxies, timeout=self.timeout,
                verify=self.verify)
        except requests.exceptions.SSLError as err:
            _exception.Warning(
                __name__,
                "Impossible de lire l'url: {0}\n"
                "La bibliothèque <requests> renvoie le code d'erreur SSL {1}. "
                "Veuillez tester avec la méthode 'urrlib' "
                "ou définir verify à False."
                "".format(self.url, err))
            # On devrait retrouver self.url à partir de
            # - err.request.url
            # - err.request.body
            return None
        except requests.ConnectionError as err:
            _exception.Warning(
                __name__,
                "Impossible de lire l'url: {0}\n"
                "La bibliothèque <requests> renvoie le code d'erreur "
                "CONNECTION {1}. Cela peut provenir d'une erreur de proxy"
                "".format(self.url, err))
            # On devrait retrouver self.url à partir de
            # - err.request.url
            # - err.request.body
            return None
        if res.status_code != requests.codes.ok:
            res.raise_for_status()  # Lève une erreur si pb dans requête
            return None
        # Renvoie de la réponse urllib si filename inconnu
        if self.filename is None:
            return res
        # Enregistrement de la réponse urllib si filename connu
        if res.encoding is not None:
            with open(self.filename, 'w', encoding=res.encoding) as f:
                f.write(res.text)
        else:
            with open(self.filename, 'wb') as f:
                f.write(res.content)
        return self.filename

    def retrieve_byurllib(self):
        """
        Récupérer un objet depuis une url par 'urllib'

        Returns
        -------
        None
            Si la requête est incorrecte
        res : http.client.HTTPResponse
            La réponse de 'urllib.request.urlopen' si self.filename is None
        filename : str
            Le fichier enregistré

        .. seealso:: https://docs.python.org/fr/3/library/urllib.html

        """
        # Imports
        import urllib.parse
        import urllib.request
        import urllib.error
        import socket
        # Contrôles
        _exception.check_dict(self.proxies)
        _exception.check_int(self.timeout)
        _exception.check_str(self.url)
        # Création de l'OPENER
        proxy = urllib.request.ProxyHandler(self.proxies)
        opener = urllib.request.build_opener(proxy)
        urllib.request.install_opener(opener)
        # Application de urllib
        try:
            res = urllib.request.urlopen(  # nosec
                url=self.url, timeout=self.timeout)
        except urllib.error.HTTPError as err:
            _exception.Warning(
                __name__,
                "Impossible de lire l'url: {0}\n"
                "La bibliothèque <urllib> renvoie le code d'erreur HTTP {1}"
                "".format(self.url, err.code))
            return None
        except urllib.error.URLError as err:
            _exception.Warning(
                __name__,
                "Impossible de lire l'url: {0}\n"
                "La bibliothèque <urllib> renvoie le message d'erreur URL {1}"
                "".format(self.url, err))
            return None
        except socket.timeout:
            _exception.Warning(
                __name__,
                "Impossible de lire l'url: {0}\nDélai supérieur à {1}"
                "" .format(self.url, TIMEOUT))
            return None
        # Renvoie de la réponse urllib si filename inconnu
        if self.filename is None:
            return res
        # Enregistrement de la réponse urllib si filename connu
        with open(self.filename, 'wb') as f:
            f.write(res.read())
        return self.filename
