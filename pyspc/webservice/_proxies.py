#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Webservice - Définition des proxies par fichier
"""
import os.path
from pyspc.core.config import Config

DEFAULT_CFG_FILENAME = os.path.join(os.path.dirname(__file__), '_proxies.txt')


def setproxies_byconfig(cfg_filename):
    """
    Traiter le fichier de configuration des proxys

    Parameters
    ----------
    cfg_filename : None, str

    Returns
    -------
    cfg : pyspc.core.config.Config
        Configuration des proxys
    """
    if cfg_filename is None:
        cfg_filename = DEFAULT_CFG_FILENAME
    if not os.path.exists(cfg_filename):
        raise OSError('Fichier de configuration inconnu : {}'
                      ''.format(cfg_filename))
    config = Config(filename=cfg_filename)
    config.read()
    return {protocol: address
            for protocol, address in config['proxies'].items()
            if address}
