#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Webservice - Online Reports

Notes
-----
API version béta : https://www.vigicrues.gouv.fr/services/1/index.php

Référence bibliographique relative à la base de données de l'Inrae:
Brigode, Pierre; Génot, Benoît; Lobligeois, Florent; Delaigue, Olivier, 2020,
"Summary sheets of watershed-scale hydroclimatic observed data for France",
https://doi.org/10.15454/UV01P1, Recherche Data Gouv, V1


"""
import os.path
from pyspc.core.config import Config


DEFAULT_CFG_FILENAME = os.path.join(os.path.dirname(__file__), 'report.txt')
"""Fichier complémentaire pour les rapports réservés à Vigicrues"""

HOSTNAMES = {
    'inrae_hydroclim': 'https://hydrogr.github.io/BDD-HydroClim/images/fr/',
    'mf_station': 'https://donneespubliques.meteofrance.fr/'
                  'metadonnees_publiques/fiches/',
    'mf_station_geojson': 'https://donneespubliques.meteofrance.fr/'
                          'metadonnees_publiques/fiches/',
    'mf_clim': 'https://donneespubliques.meteofrance.fr/FichesClim/',
    'mf_climdata': 'https://donneespubliques.meteofrance.fr/FichesClim/',
    'mf_warning': 'http://vigilance-public.meteo.fr/telechargement.php',
    'mf_dailyreport': 'https://donneespubliques.meteofrance.fr/'
                      'donnees_libres/bulletins/',
    'mf_monthlyreport': 'https://donneespubliques.meteofrance.fr/'
                        'donnees_libres/bulletins/',
    'vigicrues_fcst': 'https://www.vigicrues.gouv.fr/services/',
    'vigicrues_loc': 'https://www.vigicrues.gouv.fr/services/',
    'vigicrues_obs': 'https://www.vigicrues.gouv.fr/services/',
    'vigicrues_reach': 'https://www.vigicrues.gouv.fr/services/',
    'vigicrues_sandre': 'https://www.vigicrues.gouv.fr/services/',
    'vigicrues-1_info': 'https://www.vigicrues.gouv.fr/services/1/',
    'vigicrues-1_geoinfo': 'https://www.vigicrues.gouv.fr/services/1/',
    'vigicrues-1_domain': 'https://www.vigicrues.gouv.fr/services/1/',
    'vigicrues-1_reach': 'https://www.vigicrues.gouv.fr/services/1/',
    'vigicrues-1_loc': 'https://www.vigicrues.gouv.fr/services/1/',
}
"""Dictionnaire des hôtes des documents en ligne"""
REMOTENAMES = {
    'inrae_hydroclim': '{}_INRAE_BDD-HydroClim_fact_sheet_FR.png',
    'mf_station': 'fiche_{}.pdf',
    'mf_station_geojson': 'fiches.json',
    'mf_clim': 'FICHECLIM_{}.pdf',
    'mf_climdata': 'FICHECLIM_{}.data',
    'mf_dailyreport': '{}.pdf',
    'vigicrues_fcst':
        'previsions.json/?CdStationHydro={}&GrdSimul={}&FormatDate=iso',
    'vigicrues_loc': 'station.json/?CdStationHydro={}',
    'vigicrues_obs':
        'observations.json/?CdStationHydro={}&GrdSerie={}&FormatDate=iso',
    'vigicrues_reach': 'vigicrues.geojson',
    'vigicrues_sandre': 'observations.xml/?CdStationHydro={}&GrdSerie={}',
    'vigicrues-1_info': 'InfoVigiCru.jsonld/?CdEntVigiCru={}'
        '&TypEntVigiCru={}',
    'vigicrues-1_geoinfo': 'InfoVigiCru.geojson',
    'vigicrues-1_domain': 'TerEntVigiCru.jsonld/?CdEntVigiCru={}'
        '&TypEntVigiCru={}',
    'vigicrues-1_reach': 'TronEntVigiCru.jsonld/?CdEntVigiCru={}'
        '&TypEntVigiCru={}',
    'vigicrues-1_loc': 'StaEntVigiCru.jsonld/?CdEntVigiCru={}'
        '&TypEntVigiCru={}',
}
"""Dictionnaire des urls générique des documents en ligne"""
LOCALNAMES = {
    'inrae_hydroclim': '{}_INRAE_BDD-HydroClim_fact_sheet_FR.png',
    'mf_station': 'fiche_{}.pdf',
    'mf_clim': 'FICHECLIM_{}.pdf',
    'mf_climdata': 'FICHECLIM_{}.data',
    'mf_dailyreport': '{}.pdf',
    'mf_warning': 'vigilance_{}.zip',
    'vigicrues_fcst': 'vigicrues_fcst_{}_{}.json',
    'vigicrues_loc': 'vigicrues_loc_{}.json',
    'vigicrues_obs': 'vigicrues_obs_{}_{}.json',
    'vigicrues_reach': 'vigicrues_reaches_{}.json',
    'vigicrues_sandre': 'vigicrues_sandre_{}_{}.xml',
    'vigicrues-1_info': 'vigicrues-1_info_{}-{}_{}.json',
    'vigicrues-1_geoinfo': 'vigicrues-1_geoinfo_{}.geojson',
    'vigicrues-1_domain': 'vigicrues-1_domain_{}-{}_{}.json',
    'vigicrues-1_reach': 'vigicrues-1_reach_{}-{}_{}.json',
    'vigicrues-1_loc': 'vigicrues-1_loc_{}-{}_{}.json',
}
"""Dictionnaire des noms de fichier enregistrés localement"""

USERNAMES = {}
PASSWORDS = {}
if os.path.exists(DEFAULT_CFG_FILENAME):
    config = Config(filename=DEFAULT_CFG_FILENAME)
    config.read()
    HOSTNAMES.update({s: config[s].get('hostname', '') for s in config})
    REMOTENAMES.update({s: config[s].get('remotename', '') for s in config})
    LOCALNAMES.update({s: config[s].get('localname', '') for s in config})
    USERNAMES.update({s: config[s].get('username', '') for s in config})
    PASSWORDS.update({s: config[s].get('password', '') for s in config})
