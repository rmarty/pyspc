#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Statistiques - Période de retour
"""
import numpy as np

import pyspc.core.exception as _exception


def asstr(qmax, info=None, prefix=None):
    """
    Déterminer le temps de retour de la crue comme chaine de caractères

    Parameters
    ----------
    qmax : float
        Valeur du débit statistique
    info : dict
        Correspondance entre libellé d'un temps de retour
        et sa valeur (grandeur)
    prefix :
        Préfixe du libellé

    Returns
    -------
    tag : str
        Temps de retour sous forme de texte

    Notes
    -----
    Le code ne considère que les temps de retour suivants:
        [2, 5, 10, 20, 50, 100]

    Examples
    --------
    >>> from pyspc.statistics.period import asstr
    >>> qmax = [1, 2, 3, 5, 8, 10, 15, 20, 30, 50, 70, 100, 1000]

    CAS STATISTIQUES COMPLETES

    >>> info = {'2': 2, '5': 5, '10': 10, '20': 20, '50': 50, '100': 100}
    >>> trs = [asstr(q, info) for q in qmax]
    >>> for q, t in zip(qmax, trs):
    ...     print(q, t)
    1 < 2 ans
    2 2 ans
    3 entre 2 et 5 ans
    5 5 ans
    8 entre 5 et 10 ans
    10 10 ans
    15 entre 10 et 20 ans
    20 20 ans
    30 entre 20 et 50 ans
    50 50 ans
    70 entre 50 et 100 ans
    100 100 ans
    1000 > 100 ans

    CAS STATISTIQUES INCOMPLETES

    >>> info = {'2': 2, '5': 5, '10': 10, '20': 20}
    >>> trs = [asstr(q, info) for q in qmax]
    >>> for q, t in zip(qmax, trs):
    ...     print(q, t)
    1 < 2 ans
    2 2 ans
    3 entre 2 et 5 ans
    5 5 ans
    8 entre 5 et 10 ans
    10 10 ans
    15 entre 10 et 20 ans
    20 20 ans
    30 > 20 ans
    50 > 20 ans
    70 > 20 ans
    100 > 20 ans
    1000 > 20 ans

    CAS STATISTIQUES COMPLETES AVEC PREFIXE ('t)

    >>> info = {'t2': 2, 't5': 5, 't10': 10, 't20': 20, 't50': 50, 't100': 100}
    >>> trs = [asstr(q, info, 't') for q in qmax]
    >>> for q, t in zip(qmax, trs):
    ...     print(q, t)
    1 < 2 ans
    2 2 ans
    3 entre 2 et 5 ans
    5 5 ans
    8 entre 5 et 10 ans
    10 10 ans
    15 entre 10 et 20 ans
    20 20 ans
    30 entre 20 et 50 ans
    50 50 ans
    70 entre 50 et 100 ans
    100 100 ans
    1000 > 100 ans


    """
    if prefix is None:
        prefix = ''
    keys = ['2', '5', '10', '20', '50', '100']
    values = []
    for key in keys:
        try:
            value = float(info[prefix + key])
        except Exception:
            value = np.nan
        values.append(value)
    tag = ''
    if np.isnan(qmax):
        tag = ''
    elif np.isnan(values[0]):
        tag = ''
    elif qmax < values[0]:
        tag = '< {} ans'.format(keys[0])
    else:
        # Cas de la stat 1 à N-1
        for k, t in enumerate(keys[:-1]):
            t1 = t
            t2 = keys[k+1]
            v1 = values[k]
            v2 = values[k+1]
            if np.isnan(v1):
                pass
            elif qmax == v1:
                tag = '{} ans'.format(t1)
                break
            elif np.isnan(v2):
                tag = '> {} ans'.format(t1)
                break
            elif v1 < qmax < v2:
                tag = 'entre {} et {} ans'.format(t1, t2)
                break
        # Cas au-delà de la dernière stat (N)
        if not tag:
            if qmax == v2:
                tag = '{} ans'.format(t2)
            elif qmax > v2:
                tag = '> {} ans'.format(t2)
            else:
                pass
    return tag


def to_freq(t, highflow=None):
    """
    Convertir un temps de retour en fréquence au non-dépassement

    Parameters
    ----------
    t : int
        Temps de retour
    highflow : bool
        Hautes eaux (True) ou Basses eaux (False). Défaut: True

    Returns
    -------
    freq : float
        Fréquence au non-dépassement

    Examples
    --------
    >>> from pyspc.statistics.period import to_freq
    >>> to_freq(2)
    0.5
    >>> to_freq(10)
    0.9
    >>> to_freq(10, highflow=False)
    0.1

    """
    if highflow is None:
        highflow = True
    _exception.check_bool(highflow)
    _exception.check_int(t)
    _exception.raise_valueerror(
        t == 0, text='Temps de retour ne peut être nul')
    if highflow:
        return 1 - 1 / t
    return 1 / t
