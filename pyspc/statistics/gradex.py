#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Statistiques - Gradex, GRADient EXponentiel
"""

import pyspc.core.exception as _exception
from pyspc.statistics.freq import to_ugumbel
from pyspc.statistics.period import to_freq


def apply(pivot_value, pivot_period, target_period, gradex):
    """
    Appliquer le gradex à partir d'un point pivot

    Parameters
    ----------
    pivot_value : float
        Valeur du pivot
    pivot_period : int
        Temps de retour du pivot
    target_period : int
        Temps de retour de la cible
    gradex : float
        Valeur du gradex

    Returns
    -------
    target_value : float
        Valeur du pivot de la cible par le gradex

    See Also
    --------
    compute
    rr2q

    Examples
    --------
    >>> from pyspc.statistics.gradex import apply
    >>> pivot_value = 74
    >>> pivot_period = 10
    >>> target_period = 100
    >>> gradex = 23.2
    >>> apply(pivot_value, pivot_period, target_period, gradex)
    128.515

    """
    _exception.check_numeric(pivot_value)
    _exception.check_int(pivot_period)
    _exception.check_int(target_period)
    _exception.check_numeric(gradex)
    _exception.raise_valueerror(
        target_period < pivot_period,
        'La cible doit correspondre à un temps de retour supérieur à celui '
        'du pivot')
    pivot_ugumbel = to_ugumbel(to_freq(pivot_period))
    target_ugumbel = to_ugumbel(to_freq(target_period))
    return pivot_value + gradex * (target_ugumbel - pivot_ugumbel)


def compute(value_1, period_1, value_2, period_2):
    """
    Calculer le gradex à partir de deux points pivot

    Parameters
    ----------
    value_1 : float
        Valeur du point inférieur
    period_1 : int
        Temps de retour du point inférieur
    value_2 : float
        Valeur du point supérieur
    period_2 : int
        Temps de retour du point supérieur

    Returns
    -------
    gradex : float
        Valeur du gradex

    See Also
    --------
    apply
    rr2q

    Examples
    --------
    >>> compute(127, 10, 204, 100)
    32.77

    """
    _exception.check_numeric(value_1)
    _exception.check_int(period_1)
    _exception.check_numeric(value_2)
    _exception.check_int(period_2)
    _exception.raise_valueerror(
        period_1 >= period_2 or value_1 >= value_2,
        "L'ordre des points n'est pas respecté")
    ugumbel_1 = to_ugumbel(to_freq(period_1))
    ugumbel_2 = to_ugumbel(to_freq(period_2))
    return (value_2 - value_1) / (ugumbel_2 - ugumbel_1)


def rr2q(gradex, area, duration, rij):
    """
    Convertir le gradex pluviométrique en gradex débitmétrique

    Parameters
    ----------
    gradex : float
        Valeur du gradex pluviométrique
    area : float
        Surface du bassin versant, en km2
    duration : datetime.timedelta
        Durée de cumul de la pluviométrie
    rij : float
        Coefficient moyen entre la pointe instantanée et le débit journalière

    Returns
    -------
    gradex : float
        Valeur du gradex débitmétrique

    See Also
    --------
    apply
    compute

    Examples
    --------
    >>> from pyspc.statistics.gradex import rr2q
    >>> g = 12.1
    >>> area = 17570
    >>> duration = td(days=4)
    >>> rij = 1.5
    >>> rr2q(g, area, duration, rij)
    922.73

    """
    _exception.check_numeric(gradex)
    _exception.check_numeric(area)
    _exception.check_td(duration)
    _exception.check_numeric(rij)
    return gradex * area * 1000 / duration.total_seconds() * rij


def montana(duration, a, b):
    """
    Calculer un cumul de pluie par la formule de Montana

    Parameters
    ----------
    duration : datetime.timedelta
        Durée de cumul de la pluviométrie
    a : float
        Coefficient de Montana
    b : float
        Coefficient de Montana

    Returns
    -------
    rr : float
        Valeur du cumul de pluie

    Notes
    -----
    h(t) = a x t(1−b)
    t : min
    h : mm

    See Also
    --------
    apply
    compute

    Examples
    --------
    >>> from pyspc.statistics.gradex import montana
    >>> a = 8.929
    >>> b = 0.766
    >>> duration = td(hours=1)
    >>> montana(duration, a, b)
    23.275

    See Also
    --------
    https://publitheque.meteo.fr/Exemples/MONTANA-exemple.pdf

    """
    _exception.check_td(duration)
    _exception.check_numeric(a)
    _exception.check_numeric(b)
    return a * (duration.total_seconds() / 60) ** (1 - b)
