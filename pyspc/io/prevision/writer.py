#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Bibliothèque pyspc du projet pyspc - IO - Prevision LCI - write
"""
import pyspc.core.exception as _exception
from pyspc.core.series import Series
from pyspc.data.prevision import Prevision19


def write_Prevision14():
    """
    Raises
    ------
    NotImplementedError
    """
    raise NotImplementedError


def write_Prevision17():
    """
    Raises
    ------
    NotImplementedError
    """
    raise NotImplementedError


def write_Prevision19(series=None, filename=None, valid=False):
    """
    Ecrire un fichier prv (Otamin, Scores)

    Parameters
    ----------
    series : pyspc.core.series.Series
        Collection de séries de données
    filename : str
        Chemin de la base de données
    valid : bool
        Seulement les prévisions validées (True)
        ou toutes les prévisions produites (False)
        Défaut: False

    Returns
    -------
    rows : dict
        Dictionnaire des informations sur les séries et valeurs insérées

    See Also
    --------
    pyspc.data.prevision.Prevision19.insert_fcst

    """
    # -------------------------------------------------------------------------
    # 0- Contrôles
    # -------------------------------------------------------------------------
    _exception.check_str(filename)
    _exception.raise_valueerror(
        not isinstance(series, Series),
        "'series' doit être une collection 'Series'"
    )
    # -------------------------------------------------------------------------
    # 1- Regroupement par lieu/runtime/model/scen
    # -------------------------------------------------------------------------
    keys = {}
    dfs = {}
    for k in series.keys():
        tag = (k[0], k[2][0],  k[2][1],  k[2][2])
        keys.setdefault(tag, [])
        keys[tag].append(k)
    for case in keys:
        subkeys = keys[case]
        df = series.concat(subkeys)
        df.columns = [(c[0], c[2][0], c[2][1], 'pyspc', False, 'Val{}'.format(
                       c[2][-1]))
                      if c[2][-1] is not None
                      else (c[0], c[2][0], c[2][1], 'pyspc', False, 'Val')
                      for c in df.columns]
        df.index.name = 'DateVal'
        dfs.setdefault(case, df)
    # -------------------------------------------------------------------------
    # 2- Ecrivain
    # -------------------------------------------------------------------------
    writer = Prevision19(filename=filename)
    return writer.insert_fcst(fcst=dfs, valid=valid)
