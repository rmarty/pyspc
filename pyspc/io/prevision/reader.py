#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Bibliothèque pyspc du projet pyspc - IO - Prevision LCI - read
"""
from pyspc.core.keyseries import str2tuple, tuple2str
from pyspc.core.serie import Serie
from pyspc.core.series import Series
import pyspc.core.exception as _exception

from pyspc.convention.prevision import (
    P14_VARNAMES, P17_VARNAMES, P19_VARNAMES, P19_RATIOS)
from pyspc.data.prevision import Prevision14, Prevision17, Prevision19


def read_Prevision14(filename, codes=None, valid=False, hydro_version='hydro3',
                     first_dt=None, last_dt=None, warning=True):
    """
    Créer une instance Series
    à partir d'une base Prévision du SPC LCI, période 2014-2016

    Parameters
    ----------
    filename : str
        Chemin de la base de données
    codes : list
        Codes Hydro2/Hydro3 des stations
    valid : bool
        Seulement les prévisions validées (True)
        ou toutes les prévisions produites (False)
        Défaut: False
    hydro_version : str
        Version du référentiel: 'hydro2' ou 'hydro3'. Défaut: 'hydro3'
    first_dt : datetime
        Premier instant de prévision
    last_dt : datetime
        Dernier instant de prévision
    warning : bool
        Affiche les avertissement si True. Défaut: True

    Returns
    -------
    series : pyspc.core.series.Series
        Collection des prévisions

    Examples
    --------
    >>> from datetime import datetime as dt
    >>> from pyspc.io.prevision import read_Prevision14
    >>> filename = os.path.join('data/io/dbase', 'prevision_2014.mdb')
    >>> first_dt = dt(2013, 5, 3, 12)
    >>> last_dt = dt(2013, 5, 3, 23)
    >>> codes = ['K1251810']

    Cas des prévisions brutes - hydro2

    >>> series = read_Prevision14(
    ...     filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
    ...     valid=False, hydro_version='hydro2')
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = Prevision14
    *  TYPE DE COLLECTION   = fcst
    *  NOMBRE DE SERIES     = 27
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = K1251810
    *      - VARNAME = PH
    *      - META    = 2013-05-03 23:00:00, LCI, 2001, None
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = K1251810
    *      - VARNAME = QH
    *      - META    = 2013-05-03 23:00:00, LCI, 2001, None
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = K1251810
    *      - VARNAME = HH
    *      - META    = 2013-05-03 23:00:00, LCI, 2001, None
    *  ----------------------------------
    *  SERIE #4
    *      - CODE    = K1251810
    *      - VARNAME = PH
    *      - META    = 2013-05-03 23:00:00, LCI, 2002, None
    *  ----------------------------------
    *  SERIE #5
    *      - CODE    = K1251810
    *      - VARNAME = QH
    *      - META    = 2013-05-03 23:00:00, LCI, 2002, None
    *  ----------------------------------
    *  SERIE #6
    *      - CODE    = K1251810
    *      - VARNAME = HH
    *      - META    = 2013-05-03 23:00:00, LCI, 2002, None
    *  ----------------------------------
    *  SERIE #7
    *      - CODE    = K1251810
    *      - VARNAME = PH
    *      - META    = 2013-05-03 23:00:00, LCI, 2003, None
    *  ----------------------------------
    *  SERIE #8
    *      - CODE    = K1251810
    *      - VARNAME = QH
    *      - META    = 2013-05-03 23:00:00, LCI, 2003, None
    *  ----------------------------------
    *  SERIE #9
    *      - CODE    = K1251810
    *      - VARNAME = HH
    *      - META    = 2013-05-03 23:00:00, LCI, 2003, None
    *  ----------------------------------
    *  SERIE #10
    *      - CODE    = K1251810
    *      - VARNAME = PH
    *      - META    = 2013-05-03 23:00:00, LCI, 2004, None
    *  ----------------------------------
    *  SERIE #11
    *      - CODE    = K1251810
    *      - VARNAME = QH
    *      - META    = 2013-05-03 23:00:00, LCI, 2004, None
    *  ----------------------------------
    *  SERIE #12
    *      - CODE    = K1251810
    *      - VARNAME = HH
    *      - META    = 2013-05-03 23:00:00, LCI, 2004, None
    *  ----------------------------------
    *  SERIE #13
    *      - CODE    = K1251810
    *      - VARNAME = PH
    *      - META    = 2013-05-03 23:00:00, LCI, 2005, None
    *  ----------------------------------
    *  SERIE #14
    *      - CODE    = K1251810
    *      - VARNAME = QH
    *      - META    = 2013-05-03 23:00:00, LCI, 2005, None
    *  ----------------------------------
    *  SERIE #15
    *      - CODE    = K1251810
    *      - VARNAME = HH
    *      - META    = 2013-05-03 23:00:00, LCI, 2005, None
    *  ----------------------------------
    *  SERIE #16
    *      - CODE    = K1251810
    *      - VARNAME = PH
    *      - META    = 2013-05-03 23:00:00, LCI, 2006, None
    *  ----------------------------------
    *  SERIE #17
    *      - CODE    = K1251810
    *      - VARNAME = QH
    *      - META    = 2013-05-03 23:00:00, LCI, 2006, None
    *  ----------------------------------
    *  SERIE #18
    *      - CODE    = K1251810
    *      - VARNAME = HH
    *      - META    = 2013-05-03 23:00:00, LCI, 2006, None
    *  ----------------------------------
    *  SERIE #19
    *      - CODE    = K1251810
    *      - VARNAME = PH
    *      - META    = 2013-05-03 23:00:00, LCI, 2007, None
    *  ----------------------------------
    *  SERIE #20
    *      - CODE    = K1251810
    *      - VARNAME = QH
    *      - META    = 2013-05-03 23:00:00, LCI, 2007, None
    *  ----------------------------------
    *  SERIE #21
    *      - CODE    = K1251810
    *      - VARNAME = HH
    *      - META    = 2013-05-03 23:00:00, LCI, 2007, None
    *  ----------------------------------
    *  SERIE #22
    *      - CODE    = K1251810
    *      - VARNAME = PH
    *      - META    = 2013-05-03 23:00:00, LCI, 2008, None
    *  ----------------------------------
    *  SERIE #23
    *      - CODE    = K1251810
    *      - VARNAME = QH
    *      - META    = 2013-05-03 23:00:00, LCI, 2008, None
    *  ----------------------------------
    *  SERIE #24
    *      - CODE    = K1251810
    *      - VARNAME = HH
    *      - META    = 2013-05-03 23:00:00, LCI, 2008, None
    *  ----------------------------------
    *  SERIE #25
    *      - CODE    = K1251810
    *      - VARNAME = PH
    *      - META    = 2013-05-03 23:00:00, LCI, 2009, None
    *  ----------------------------------
    *  SERIE #26
    *      - CODE    = K1251810
    *      - VARNAME = QH
    *      - META    = 2013-05-03 23:00:00, LCI, 2009, None
    *  ----------------------------------
    *  SERIE #27
    *      - CODE    = K1251810
    *      - VARNAME = HH
    *      - META    = 2013-05-03 23:00:00, LCI, 2009, None
    *************************************

    Cas des prévisions validées - hydro2

    >>> series = read_Prevision14(
    ...     filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
    ...     valid=True, hydro_version='hydro2')
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = Prevision14
    *  TYPE DE COLLECTION   = fcst
    *  NOMBRE DE SERIES     = 2
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = K1251810
    *      - VARNAME = QH
    *      - META    = 2013-05-03 23:00:00, LCI-valid, 2006, None
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = K1251810
    *      - VARNAME = HH
    *      - META    = 2013-05-03 23:00:00, LCI-valid, 2006, None
    *************************************

    Cas des prévisions brutes - hydro3

    >>> filename = os.path.join('data/io/dbase', 'prevision_2014_hydro3.mdb')
    >>> first_dt = dt(2016, 5, 31, 12)
    >>> last_dt = dt(2016, 5, 31, 18)
    >>> codes = ['K6373020']
    >>> series = read_Prevision14(
    ...     filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
    ...     valid=False, hydro_version='hydro3')
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = Prevision14
    *  TYPE DE COLLECTION   = fcst
    *  NOMBRE DE SERIES     = 8
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 13:00:00, LCI, 2001, Val
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 13:00:00, LCI, 2001, Val10
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 13:00:00, LCI, 2001, Val50
    *  ----------------------------------
    *  SERIE #4
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 13:00:00, LCI, 2001, Val90
    *  ----------------------------------
    *  SERIE #5
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 16:00:00, LCI, 2001, Val
    *  ----------------------------------
    *  SERIE #6
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 16:00:00, LCI, 2001, Val10
    *  ----------------------------------
    *  SERIE #7
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 16:00:00, LCI, 2001, Val50
    *  ----------------------------------
    *  SERIE #8
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 16:00:00, LCI, 2001, Val90
    *************************************

    Cas des prévisions validées - hydro3

    >>> series = read_Prevision14(
    ...     filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
    ...     valid=True, hydro_version='hydro3')
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = Prevision14
    *  TYPE DE COLLECTION   = fcst
    *  NOMBRE DE SERIES     = 8
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 13:00:00, LCI-valid, 2001, Val
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 13:00:00, LCI-valid, 2001, Val10
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 13:00:00, LCI-valid, 2001, Val50
    *  ----------------------------------
    *  SERIE #4
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 13:00:00, LCI-valid, 2001, Val90
    *  ----------------------------------
    *  SERIE #5
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 16:00:00, LCI-valid, 2001, Val
    *  ----------------------------------
    *  SERIE #6
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 16:00:00, LCI-valid, 2001, Val10
    *  ----------------------------------
    *  SERIE #7
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 16:00:00, LCI-valid, 2001, Val50
    *  ----------------------------------
    *  SERIE #8
    *      - CODE    = K6373020
    *      - VARNAME = QH
    *      - META    = 2016-05-31 16:00:00, LCI-valid, 2001, Val90
    *************************************

    """
    # -------------------------------------------------------------------------
    # 0- Contrôles
    # -------------------------------------------------------------------------
    _exception.check_str(filename)
    _exception.check_listlike(codes)
    _exception.check_bool(valid)
    _exception.check_str(hydro_version)
    _exception.check_dt(first_dt)
    _exception.check_dt(last_dt)
    _exception.check_bool(warning)
    # -------------------------------------------------------------------------
    # 1- Lecture + Mise en DataFrame
    # -------------------------------------------------------------------------
    reader = Prevision14(filename=filename)
    df = reader.read_fcst(codes=codes, first_dt=first_dt, last_dt=last_dt,
                          valid=valid, hydro_version=hydro_version,
                          warning=warning)
    if df is None:
        return None
    # -------------------------------------------------------------------------
    # 4- Création de la collection de séries
    # -------------------------------------------------------------------------
    series = Series(datatype='fcst',
                    name=set_series_name('Prevision14', valid=valid))
    model = 'LCI'
    if valid:
        model += '-valid'
    if isinstance(df, dict):
        for d in df:
            for c in df[d].columns:
                serie = Serie(
                    df[d][c], code=d[0], provider='Prevision14', varname='QH',
                    warning=warning)
                k = (serie.code, 'QH', (d[1], model, str(d[2]), c))
                serie.code = str2tuple(tuple2str(k), forceobs=True)[0]
                series.add(code=d[0], serie=serie, meta=k[2])
    else:
        for c in df.columns:
            try:
                v = P14_VARNAMES[c[3]]
            except KeyError:
                continue
            serie = Serie(df[c], code=c[0], provider='Prevision14', varname=v,
                          warning=warning)
            k = (serie.code, v, (c[1], model, str(c[2]), None))
            serie.code = str2tuple(tuple2str(k), forceobs=True)[0]
            series.add(code=c[0], serie=serie, meta=k[2])
    return series


def read_Prevision17(filename=None, codes=None, valid=False, released=False,
                     first_dt=None, last_dt=None, warning=True):
    """
    Créer une instance Series
    à partir d'une base Prévision du SPC LCI, période 2017-2019

    Parameters
    ----------
    filename : str
        Chemin de la base de données
    codes : list
        Codes Hydro2/Hydro3 des stations
    valid : bool
        Seulement les prévisions validées (True)
        ou toutes les prévisions produites (False)
        Défaut: False
    released : bool
        Seulement les prévisions validées et diffusées (True)
        Défaut: False
    first_dt : datetime
        Premier instant de prévision
    last_dt : datetime
        Dernier instant de prévision
    warning : bool
        Affiche les avertissement si True. Défaut: True

    Returns
    -------
    series : pyspc.core.series.Series
        Collection des prévisions

    Examples
    --------
    >>> from datetime import datetime as dt
    >>> from pyspc.io.prevision import read_Prevision17
    >>> filename = os.path.join('data/io/dbase', 'prevision_2017.mdb')
    >>> first_dt = dt(2016, 11, 21, 12)
    >>> last_dt = dt(2016, 11, 21, 12)
    >>> codes = ['K0403010']

    Cas des prévisions brutes

    >>> series = read_Prevision17(
    ...     filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
    ...     valid=False, released=False)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = Prevision17
    *  TYPE DE COLLECTION   = fcst
    *  NOMBRE DE SERIES     = 64
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, LCI, 2001,
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, LCI, 2001, 10
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, LCI, 2001, 50
    *  ----------------------------------
    *  SERIE #4
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, LCI, 2001, 90
    *  ----------------------------------
    *  SERIE #5
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, LCI, 2001,
    *  ----------------------------------
    *  SERIE #6
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, LCI, 2001, 10
    *  ----------------------------------
    *  SERIE #7
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, LCI, 2001, 50
    *  ----------------------------------
    *  SERIE #8
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, LCI, 2001, 90
    *  ----------------------------------
    *  SERIE #9
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, LCI, 2002,
    *  ----------------------------------
    *  SERIE #10
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, LCI, 2002, 10
    *  ----------------------------------
    *  ----------------------------------
    *  ----------------------------------
    *  SERIE #60
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, LCI, 2009, 90
    *  ----------------------------------
    *  SERIE #61
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, LCI, 2009,
    *  ----------------------------------
    *  SERIE #62
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, LCI, 2009, 10
    *  ----------------------------------
    *  SERIE #63
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, LCI, 2009, 50
    *  ----------------------------------
    *  SERIE #64
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, LCI, 2009, 90
    *************************************


    Cas des prévisions validées

    >>> series = read_Prevision17(
    ...     filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
    ...     valid=True, released=False)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = Prevision17
    *  TYPE DE COLLECTION   = fcst
    *  NOMBRE DE SERIES     = 8
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003,
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003, 10
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003, 50
    *  ----------------------------------
    *  SERIE #4
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003, 90
    *  ----------------------------------
    *  SERIE #5
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003,
    *  ----------------------------------
    *  SERIE #6
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003, 10
    *  ----------------------------------
    *  SERIE #7
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003, 50
    *  ----------------------------------
    *  SERIE #8
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003, 90
    *************************************


    Cas des prévisions diffusées

    >>> series = read_Prevision17(
    ...     filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
    ...     valid=True, released=True)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = Prevision17
    *  TYPE DE COLLECTION   = fcst
    *  NOMBRE DE SERIES     = 8
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003,
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003, 10
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003, 50
    *  ----------------------------------
    *  SERIE #4
    *      - CODE    = K0403010
    *      - VARNAME = QH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003, 90
    *  ----------------------------------
    *  SERIE #5
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003,
    *  ----------------------------------
    *  SERIE #6
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003, 10
    *  ----------------------------------
    *  SERIE #7
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003, 50
    *  ----------------------------------
    *  SERIE #8
    *      - CODE    = K0403010
    *      - VARNAME = HH
    *      - META    = 2016-11-21 12:00:00, pilote, 2003, 90
    *************************************

    """
    # -------------------------------------------------------------------------
    # 0- Contrôles
    # -------------------------------------------------------------------------
    _exception.check_str(filename)
    _exception.check_listlike(codes)
    _exception.check_bool(valid)
    _exception.check_bool(released)
    _exception.check_dt(first_dt)
    _exception.check_dt(last_dt)
    _exception.check_bool(warning)
    # -------------------------------------------------------------------------
    # 1- Lecture + Mise en DataFrame
    # -------------------------------------------------------------------------
    reader = Prevision17(filename=filename)
    df = reader.read_fcst(codes=codes, first_dt=first_dt, last_dt=last_dt,
                          valid=valid, released=released, warning=warning)
    if df is None:
        return None
    # -------------------------------------------------------------------------
    # 4- Création de la collection de séries
    # -------------------------------------------------------------------------
    series = Series(datatype='fcst',
                    name=set_series_name('Prevision17',
                                         valid=valid, released=released))
    for c in df.columns:
        if valid:
            model = c[3]
        else:
            model = 'LCI'
        try:
            v = P17_VARNAMES[(len(c[0]), c[-1].endswith('conv'))]
        except KeyError:
            continue
        serie = Serie(df[c], code=c[0], provider='Prevision17', varname=v,
                      warning=warning)
        k = (serie.code, v,
             (c[1], model, str(c[2]),
              c[-1].replace('Val', '').replace('conv', '')))
        serie.code = str2tuple(tuple2str(k), forceobs=True)[0]
        series.add(code=c[0], serie=serie, meta=k[2])
    return series


def read_Prevision19(filename=None, codes=None, valid=False, released=False,
                     first_dt=None, last_dt=None, warning=True):
    """
    Créer une instance Series
    à partir d'une base Prévision du SPC LCI, période 2017-2019

    Parameters
    ----------
    filename : str
        Chemin de la base de données
    codes : list
        Codes Hydro2/Hydro3 des stations
    valid : bool
        Seulement les prévisions validées (True)
        ou toutes les prévisions produites (False)
        Défaut: False
    released : bool
        Seulement les prévisions validées et diffusées (True)
        Défaut: False
    first_dt : datetime
        Premier instant de prévision
    last_dt : datetime
        Dernier instant de prévision
    warning : bool
        Affiche les avertissement si True. Défaut: True

    Returns
    -------
    series : pyspc.core.series.Series
        Collection des prévisions

    Examples
    --------
    >>> from datetime import datetime as dt
    >>> from pyspc.io.prevision import read_Prevision19
    >>> filename = os.path.join('data/io/dbase', 'PRV_201801.mdb')
    >>> first_dt = dt(2018, 1, 4)
    >>> last_dt = dt(2018, 1, 5)
    >>> codes = ['K1321810']

    Cas des prévisions brutes

    >>> series = read_Prevision17(
    ...     filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
    ...     valid=False, released=False)
    >>> series
        *************************************
        ********** SERIES *******************
        *************************************
        *  NOM DE LA COLLECTION = Prevision19
        *  TYPE DE COLLECTION   = fcst
        *  NOMBRE DE SERIES     = 72
        *  ----------------------------------
        *  SERIE #1
        *      - CODE    = K1321810
        *      - VARNAME = QH
        *      - META    = 2018-01-04 12:00:00, LCI, 2007,
        *  ----------------------------------
        *  SERIE #2
        *      - CODE    = K1321810
        *      - VARNAME = QH
        *      - META    = 2018-01-04 12:00:00, LCI, 2007, 10
        *  ----------------------------------
        *  SERIE #3
        *      - CODE    = K1321810
        *      - VARNAME = QH
        *      - META    = 2018-01-04 12:00:00, LCI, 2007, 50
        *  ----------------------------------
        *  SERIE #4
        *      - CODE    = K1321810
        *      - VARNAME = QH
        *      - META    = 2018-01-04 12:00:00, LCI, 2007, 90
        *  ----------------------------------
        *  SERIE #5
        *      - CODE    = K1321810
        *      - VARNAME = HH
        *      - META    = 2018-01-04 12:00:00, LCI, 2007,
        *  ----------------------------------
        *  SERIE #6
        *      - CODE    = K1321810
        *      - VARNAME = HH
        *      - META    = 2018-01-04 12:00:00, LCI, 2007, 10
        *  ----------------------------------
        *  SERIE #7
        *      - CODE    = K1321810
        *      - VARNAME = HH
        *      - META    = 2018-01-04 12:00:00, LCI, 2007, 50
        *  ----------------------------------
        *  SERIE #8
        *      - CODE    = K1321810
        *      - VARNAME = HH
        *      - META    = 2018-01-04 12:00:00, LCI, 2007, 90
        *  ----------------------------------
        *  SERIE #9
        *      - CODE    = K1321810
        *      - VARNAME = QH
        *      - META    = 2018-01-04 12:00:00, LCI, 1007,
        *  ----------------------------------
        *  SERIE #10
        *      - CODE    = K1321810
        *      - VARNAME = QH
        *      - META    = 2018-01-04 12:00:00, LCI, 1007, 10
        *  ----------------------------------
        *  ----------------------------------
        *  ----------------------------------
        *  SERIE #70
        *      - CODE    = K1321810
        *      - VARNAME = HH
        *      - META    = 2018-01-04 12:00:00, LCI, 5100, 10
        *  ----------------------------------
        *  SERIE #71
        *      - CODE    = K1321810
        *      - VARNAME = HH
        *      - META    = 2018-01-04 12:00:00, LCI, 5100, 50
        *  ----------------------------------
        *  SERIE #72
        *      - CODE    = K1321810
        *      - VARNAME = HH
        *      - META    = 2018-01-04 12:00:00, LCI, 5100, 90
        *************************************


    Cas des prévisions validées

    >>> series = read_Prevision17(
    ...     filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
    ...     valid=True, released=False)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = Prevision19
    *  TYPE DE COLLECTION   = fcst
    *  NOMBRE DE SERIES     = 8
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = K1321810
    *      - VARNAME = QH
    *      - META    = 2018-01-04 12:00:00, expresso, 5200,
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = K1321810
    *      - VARNAME = QH
    *      - META    = 2018-01-04 12:00:00, expresso, 5200, 10
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = K1321810
    *      - VARNAME = QH
    *      - META    = 2018-01-04 12:00:00, expresso, 5200, 50
    *  ----------------------------------
    *  SERIE #4
    *      - CODE    = K1321810
    *      - VARNAME = QH
    *      - META    = 2018-01-04 12:00:00, expresso, 5200, 90
    *  ----------------------------------
    *  SERIE #5
    *      - CODE    = K1321810
    *      - VARNAME = HH
    *      - META    = 2018-01-04 12:00:00, expresso, 5200,
    *  ----------------------------------
    *  SERIE #6
    *      - CODE    = K1321810
    *      - VARNAME = HH
    *      - META    = 2018-01-04 12:00:00, expresso, 5200, 10
    *  ----------------------------------
    *  SERIE #7
    *      - CODE    = K1321810
    *      - VARNAME = HH
    *      - META    = 2018-01-04 12:00:00, expresso, 5200, 50
    *  ----------------------------------
    *  SERIE #8
    *      - CODE    = K1321810
    *      - VARNAME = HH
    *      - META    = 2018-01-04 12:00:00, expresso, 5200, 90
    *************************************

    """
    # -------------------------------------------------------------------------
    # 0- Contrôles
    # -------------------------------------------------------------------------
    _exception.check_str(filename)
    _exception.check_listlike(codes)
    _exception.check_bool(valid)
    _exception.check_bool(released)
    _exception.check_dt(first_dt)
    _exception.check_dt(last_dt)
    _exception.check_bool(warning)
    # -------------------------------------------------------------------------
    # 1- Lecture + Mise en DataFrame
    # -------------------------------------------------------------------------
    reader = Prevision19(filename=filename)
    dfs = reader.read_fcst(codes=codes, first_dt=first_dt, last_dt=last_dt,
                           valid=valid, released=released, unique=True,
                           warning=warning)
    if dfs is None:
        return None
    # -------------------------------------------------------------------------
    # 4- Création de la collection de séries
    # -------------------------------------------------------------------------
    series = Series(datatype='fcst',
                    name=set_series_name('Prevision19',
                                         valid=valid, released=released))
    for df in dfs.values():
        for c in df.columns:
            if valid:
                model = c[3]
            else:
                model = 'LCI'
            try:
                v = P19_VARNAMES[(len(c[0]), c[-1].endswith('onv'))]
            except KeyError:
                continue
            df[c] = df[c] * P19_RATIOS.get((v, reader.subtype), 1)
            serie = Serie(df[c], code=c[0], provider='Prevision19', varname=v,
                          warning=warning)
            k = (serie.code, v,
                 (c[1], model, str(c[2]),
                  c[-1].replace('Val', '')
                  .replace('conv', '').replace('Conv', '')))
            serie.code = str2tuple(tuple2str(k), forceobs=True)[0]
            series.add(code=c[0], serie=serie, meta=k[2])
    return series


def set_series_name(source, valid=False, released=False):
    """
    Définir le nom de la collection
    """
    if not valid:
        return source
    if valid and not released:
        return source + 'val'
    return source + 'diff'
