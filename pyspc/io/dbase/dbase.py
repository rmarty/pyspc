#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc : bases de données
"""
import os.path
import pyspc.core.exception as _exception


class Dbase():
    """
    Classe de base conçus pour manipuler les bases de données

    Attributes
    ----------
    filename : str
        Nom du fichier de la base de données
    _dbase_connect : pyodbc ou sqlite3
        Connexion à la base
    _dbase_cursor : pyodbc ou sqlite3
        Curseur de la base
    sql : str
        Requête SQL
    _tables : str
        Tables de la base

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance Dbase

        Parameters
        ----------
        filename : str
            Nom du fichier de la base de données

        """
        if filename is None:
            raise ValueError('Nom de fichier non défini')
        if not os.path.exists(filename):
            raise ValueError('Fichier inexistant : {}'.format(filename))
        self.filename = filename
        self._dbase_connect = None
        self._dbase_cursor = None
        self.sql = None
        self._tables = None
        self.tables = None

    def __str__(self):
        """
        Afficher les méta-données
        """
        cname = self.__class__.__name__
        text = """
        *************************************
        ********* {cname} *******************
        *************************************
        *  NOM FICHIER      = {filename}
        *  REQUETE SQL      = \n{sql}
        *************************************
        """
        return text.format(cname=cname, **vars(self))

    def close(self):
        """
        Fermer la connexion à la base de données
        Fermer le curseur <dbase>
        """
        self._dbase_cursor.close()
        self._dbase_connect.close()
        self._dbase_cursor = None
        self._dbase_connect = None

    def commit(self):
        """
        Commiter/accepter les enregistrements ajoutés
        """
        self._dbase_connect.commit()  # enregistrement

    def connect(self):
        """
        Créer la connexion à la base de données
        Créer le curseur

        .. warning:: Non implémenté

        """
        raise NotImplementedError

    def execute(self):
        """
        Exécution de la requête SQL

        .. warning:: Non implémenté

        """
        raise NotImplementedError

    def rollback(self):
        """
        Annuler les enregistrements ajoutés
        """
        self._dbase_connect.rollback()

    def check_sql_return(self, content=None, warning=False):
        """
        Contrôler le retour de la requête SQL
        """
        if not content:
            if warning:
                _exception.Warning(
                    __name__,
                    "aucune série ne correspond à la requête SQL\n"
                    "'{0}'".format(self.sql))
            return None
        return content
