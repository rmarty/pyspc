#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Bases de données SQLITE
"""
import os.path
import pyspc.core.exception as _exception
import pyspc.io.dbase.dbase as _dbase


class Sdb(_dbase.Dbase):
    """
    Bases SQLite (.sqlite)

    Attributes
    ----------
    filename : str
        Nom du fichier
    _dbase_connect : sqlite3
        Object de connexion sqlite3
    _dbase_cursor : sqlite3
        Curseur de sqlite3
    sql : str
        Requête SQL
    _tables : str
        Tables de la base

    """
    def __init__(self, filename=None):
        """
        Bases SQLite (.sqlite)

        Parameters
        ----------
        filename : str
            Nom du fichier Sdb

        """
        super().__init__(filename=filename)

    def connect(self):
        """
        Créer la connexion à la base de données
        Créer le curseur sqlite3

        .. warning:: Cette méthode nécessite l'import de la
                     bibliothèque tierce sqlite3

        """
        import sqlite3
        # Contrôles
        if not os.path.exists(self.filename):
            raise OSError("Base de données inconnue {}".format(self.filename))
        # Ouverture de la base
        self._dbase_connect = sqlite3.connect(self.filename)
        self._dbase_cursor = self._dbase_connect.cursor()

    def execute(self, warning=True):
        """
        Exécution de la requête SQL

        Parameters
        ----------
        warning : bool
            Afficher les avertissements. Par défaut: True

        Returns
        -------
        list or None
            Eléments retenus par la requête SQL

        .. warning:: Cette méthode nécessite l'import de la
                     bibliothèque tierce sqlite3

        """
        import sqlite3
        data = None
        if self.sql is None:
            raise ValueError('Requête SQL non définie')
        if not isinstance(self.sql, str):
            raise ValueError('Requête SQL incorrecte')
        if not isinstance(self._dbase_cursor, sqlite3.Cursor):
            raise OSError('Curseur SDB incorrect ou non défini')
        try:
            content = self._dbase_cursor.execute(self.sql)
        except sqlite3.Error:
            _exception.Warning(
                __name__,
                "erreur d'exécution de la requête SQL\n"
                "'{0}'".format(self.sql))
        else:
            try:
                data = content.fetchall()
            except sqlite3.Error:
                data = None
                if warning:
                    _exception.Warning(
                        __name__,
                        "une erreur est survenue lors de la "
                        "lecture de la base Access. "
                        "Cela peut provenir de la "
                        "lecture de la date")
            return data
        return data

    def lastrowid(self):
        """
        Récupérer l'identifiant du dernier enregistrement

        Returns
        -------
        int ou None
            Identifiant du dernier enregistrement

        """
        return self._dbase_cursor.lastrowid
