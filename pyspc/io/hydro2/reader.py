#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Bibliothèque pyspc du projet pyspc - IO - Hydro2 - read
"""
from collections import OrderedDict
from datetime import datetime as dt
import pandas as pnd

from pyspc.core.config import Config
from pyspc.core.serie import Serie
from pyspc.core.series import Series
from pyspc.convention.hydro2 import DTFMT_CONTENT, RATIOS_CONTENT
from pyspc.data.hydro2 import Hydro2 as _d_Hydro2
from pyspc.metadata.hydro2 import Hydro2 as _m_Hydro2
import pyspc.core.exception as _exception


def read_Hydro2(filename=None, datatype=None, warning=True):
    """
    Lire un fichier de données ou de méta-données HYDRO-2

    Parameters
    ----------
    filename : str
        Nom du fichier HYDRO-2
    datatype : str
        Nature du contenu
    warning : bool
        Imprimer les erreurs ?

    Returns
    -------
    series : pyspc.core.series.Series
        Si le fichier est une série de valeurs
    config : pyspc.core.config.Config
        Si le fichier est une série de méta-données

    Examples
    --------
    >>> from pyspc.io.hydro2 import read_Hydro2

    Cas d'une série de valeurs

    >>> f = 'data/data/hydro2/htemps.txt'
    >>> d = 'H-TEMPS'
    >>> series = read_Hydro2(filename=f, datatype=d)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = Hydro2_Data
    *  TYPE DE COLLECTION   = obs
    *  NOMBRE DE SERIES     = 3
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = K1251810
    *      - VARNAME = HI
    *      - META    = None
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = K1321810
    *      - VARNAME = HI
    *      - META    = None
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = K1341810
    *      - VARNAME = HI
    *      - META    = None
    *************************************

    Cas d'une série de méta-données

    >>> f = 'data/metadata/hydro2/debcla1.txt'
    >>> d = 'DEBCLA'
    >>> content = read_Hydro2(filename=f, datatype=d)
    >>> content
    *************************************
    ************ Config ****************
    *************************************
    *  NOM FICHIER      = None
    *  CONFIGURATION
    *      + K1251810
    *          + 0.0000 = 0.020
    *          + 0.0010 = 0.045
    *          + 0.0020 = 0.060
    *          + 0.0050 = 0.082
    *          + 0.0100 = 0.101
    *          + 0.0200 = 0.129
    *          + 0.0274 = 0.145
    *          + 0.0500 = 0.189
    *          + 0.1000 = 0.274
    *          + 0.1500 = 0.349
    *          + 0.2000 = 0.444
    *          + 0.2500 = 0.584
    *          + 0.3000 = 0.752
    *          + 0.3500 = 0.975
    *          + 0.4000 = 1.250
    *          + 0.4500 = 1.590
    *          + 0.5000 = 2.030
    *          + 0.5500 = 2.530
    *          + 0.6000 = 3.150
    *          + 0.6500 = 3.950
    *          + 0.7000 = 5.040
    *          + 0.7500 = 6.330
    *          + 0.8000 = 8.770
    *          + 0.8500 = 11.800
    *          + 0.9000 = 16.500
    *          + 0.9500 = 25.900
    *          + 0.9726 = 35.500
    *          + 0.9800 = 40.900
    *          + 0.9900 = 53.700
    *          + 0.9950 = 71.300
    *          + 0.9980 = 88.300
    *          + 0.9990 = 100.000
    *          + 1.0000 = 156.000
    *          + area = 776.000
    *          + end = 31-08-2020
    *          + name = L'Arroux à Dracy-Saint-Loup [Surmoulin]
    *          + provider = DREAL Bourgogne
    *          + start = 01-09-1948
    *          + station = K1251810
    *          + z0 = 291.000
    *      + K1321810
    *          + 0.0000 = 0.147
    *          + 0.0010 = 0.211
    *          + 0.0020 = 0.293
    *          + 0.0050 = 0.490
    *          + 0.0100 = 0.630
    *          + 0.0200 = 0.809
    *          + 0.0274 = 0.944
    *          + 0.0500 = 1.320
    *          + 0.1000 = 1.890
    *          + 0.1500 = 2.350
    *          + 0.2000 = 2.960
    *          + 0.2500 = 3.720
    *          + 0.3000 = 4.680
    *          + 0.3500 = 5.770
    *          + 0.4000 = 6.940
    *          + 0.4500 = 8.400
    *          + 0.5000 = 10.300
    *          + 0.5500 = 12.400
    *          + 0.6000 = 14.900
    *          + 0.6500 = 18.300
    *          + 0.7000 = 22.500
    *          + 0.7500 = 27.800
    *          + 0.8000 = 34.300
    *          + 0.8500 = 42.900
    *          + 0.9000 = 55.700
    *          + 0.9500 = 81.800
    *          + 0.9726 = 108.000
    *          + 0.9800 = 122.000
    *          + 0.9900 = 152.000
    *          + 0.9950 = 185.000
    *          + 0.9980 = 233.000
    *          + 0.9990 = 262.000
    *          + 1.0000 = 408.000
    *          + area = 1800.000
    *          + end = 31-08-2020
    *          + name = L'Arroux à Étang-sur-Arroux [Pont du Tacot]
    *          + provider = DREAL Centre
    *          + start = 01-09-1948
    *          + station = K1321810
    *          + z0 = 273.000
    *      + K1341810
    *          + 0.0000 = 0.100
    *          + 0.0010 = 0.286
    *          + 0.0020 = 0.354
    *          + 0.0050 = 0.464
    *          + 0.0100 = 0.602
    *          + 0.0200 = 0.810
    *          + 0.0274 = 0.935
    *          + 0.0500 = 1.330
    *          + 0.1000 = 2.010
    *          + 0.1500 = 2.580
    *          + 0.2000 = 3.340
    *          + 0.2500 = 4.310
    *          + 0.3000 = 5.500
    *          + 0.3500 = 6.940
    *          + 0.4000 = 8.450
    *          + 0.4500 = 10.500
    *          + 0.5000 = 12.700
    *          + 0.5500 = 15.400
    *          + 0.6000 = 18.400
    *          + 0.6500 = 22.300
    *          + 0.7000 = 27.300
    *          + 0.7500 = 33.500
    *          + 0.8000 = 41.300
    *          + 0.8500 = 52.500
    *          + 0.9000 = 70.100
    *          + 0.9500 = 106.000
    *          + 0.9726 = 137.000
    *          + 0.9800 = 153.000
    *          + 0.9900 = 186.000
    *          + 0.9950 = 218.000
    *          + 0.9980 = 264.000
    *          + 0.9990 = 296.000
    *          + 1.0000 = 391.000
    *          + area = 2280.000
    *          + end = 31-08-2020
    *          + name = L'Arroux à Rigny-sur-Arroux
    *          + provider = DREAL Centre
    *          + start = 01-09-1948
    *          + station = K1341810
    *          + z0 = 237.000
    *************************************

    See Also
    --------
    pyspc.data.hydro2.Hydro2.get_datatypes
    pyspc.metadata.hydro2.Hydro2.get_datatypes

    """
    # -------------------------------------------------------------------------
    # 0- Contrôles
    # -------------------------------------------------------------------------
    _exception.check_str(filename)
    _exception.check_str(datatype)
    _exception.check_bool(warning)
    # -------------------------------------------------------------------------
    # 1- Cas de séries de données
    # -------------------------------------------------------------------------
    if datatype in _d_Hydro2.get_datatypes():
        # Initialisation de la collection
        reader = _d_Hydro2(filename=filename)
        content = reader.read()
        series = Series(datatype='obs', name='Hydro2_Data')
        for s in content:
            # -----------------------------------------------------------------
            # Contrôle du type de donnée
            # -----------------------------------------------------------------
            try:
                d = content[s]['datatype']
            except KeyError:
                pass
            else:
                _exception.raise_valueerror(
                    datatype != d,
                    "Incohérence entre le type de fichier demandé et le "
                    "contenu du fichier : {} != {}".format(datatype, d)
                )
            # -----------------------------------------------------------------
            # Construction du DataFrame
            # -----------------------------------------------------------------
            values = []
            index = []
            for k, v in content[s].items():
                try:
                    d = dt.strptime(k, DTFMT_CONTENT[datatype])
                    v = float(v)
                except ValueError:
                    continue
                else:
                    values.append(v)
                    index.append(d)
            df = pnd.DataFrame(values, index=index)
            r = RATIOS_CONTENT.get(datatype, 1)
            df = df * r
            # -----------------------------------------------------------------
            # Insertion dans la collection
            # -----------------------------------------------------------------
            serie = Serie(df, code=s, provider='Hydro2', varname=datatype,
                          warning=warning)
            series.add(serie=serie)
        return series
    # -------------------------------------------------------------------------
    # 2- Cas de séries de données
    # -------------------------------------------------------------------------
    if datatype in _m_Hydro2.get_datatypes():
        reader = _m_Hydro2(filename=filename)
        content = reader.read()
        config = Config()
        x = OrderedDict(
            (s, OrderedDict((o, content[s][o]) for o in sorted(content[s])))
            for s in sorted(content))
        config.update(x)
#        for s in sorted(content):
#            config.setdefault(s, OrderedDict())
#            for o in sorted(content[s]):
#                config[s].setdefault(o, content[s][o])
#        print(config.list_sections_options())
        return config
    # -------------------------------------------------------------------------
    # 3- Cas inconnu
    # -------------------------------------------------------------------------
    raise ValueError('Type de fichier HYDRO-2 inconnu')
