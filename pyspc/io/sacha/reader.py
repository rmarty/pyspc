#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Bibliothèque pyspc du projet pyspc - IO - Sacha - read
"""
from pyspc.core.serie import Serie
from pyspc.core.series import Series
import pyspc.core.exception as _exception
from pyspc.data.sacha import Sacha


def read_Sacha(filename=None, codes=None, first_dt=None, last_dt=None,
               realtime=False, varname=None, prcp_src=None,
               hydro_version='hydro3', warning=False):
    """
    Créer une instance Series à partir de données SACHA

    Parameters
    ----------
    filename : str
        Chemin de la base de données
    codes : list
        Liste des identifiants des stations
    first_dt : datetime
        Premier pas de temps des données
    last_dt : datetime
        Dernier pas de temps des données
    realtime : bool
        Données temps-réel (True) ou historique (False). Défaut:
    varname : str
        Grandeur
    prcp_src : str
        Origine des données pluviométriques.
        Valeurs possibles ['gauge', 'radar']. Défaut: 'gauge'
    hydro_version : str
        Référentiel hydrométrique
    warning : bool
        Afficher les avertissements ? défaut: False

    Returns
    -------
    series : pyspc.core.series.Series
        Collection de séries de données

    Examples
    --------
    >>> from datetime import datetime as dt
    >>> from pyspc.io.sacha import read_Sacha

    Cas de données hydrométriques

    >>> f = 'data/io/dbase/sacha_montpezat.mdb'
    >>> series = read_Sacha(
    ...    filename=f,
    ...    codes=['K0030020', 'K0100020'],
    ...    varname='QH',
    ...    first_dt=dt(2016, 11, 22, 15),
    ...    last_dt=dt(2016, 11, 22, 18),
    ...    realtime=True,
    ...    hydro_version='hydro2'
    ... )
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = Sacha
    *  TYPE DE COLLECTION   = obs
    *  NOMBRE DE SERIES     = 2
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = K0030020
    *      - VARNAME = QH
    *      - META    = None
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = K0100020
    *      - VARNAME = QH
    *      - META    = None
    *************************************


    Cas de données météorologiques

    >>> series = read_Sacha(
    ...    filename=f,
    ...    codes=['K0009910', 'K0029910', 'K0100020', '07154005'],
    ...    varname='PH',
    ...    first_dt=dt(2008, 11, 1, 23),
    ...    last_dt=dt(2008, 11, 2, 2),
    ...    realtime=False,
    ...    prcp_src='gauge',
    ...    hydro_version='hydro2'
    ... )
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = Sacha
    *  TYPE DE COLLECTION   = obs
    *  NOMBRE DE SERIES     = 4
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = 07154005
    *      - VARNAME = PH
    *      - META    = None
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = K0009910
    *      - VARNAME = PH
    *      - META    = None
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = K0029910
    *      - VARNAME = PH
    *      - META    = None
    *  ----------------------------------
    *  SERIE #4
    *      - CODE    = K0100020
    *      - VARNAME = PH
    *      - META    = None
    *************************************


    """
    # -------------------------------------------------------------------------
    # 0- Contrôles
    # -------------------------------------------------------------------------
    _exception.check_str(filename)
    _exception.check_bool(warning)
    series = Series(datatype='obs', name='Sacha')
    # -------------------------------------------------------------------------
    # 1- Lecture
    # -------------------------------------------------------------------------
    # Création du lecteur
    reader = Sacha(filename=filename)
    # Lecture des données
    try:
        df = reader.read(
            codes=codes, varname=varname, first_dt=first_dt, last_dt=last_dt,
            realtime=realtime, prcp_src=prcp_src, hydro_version=hydro_version)
    except ValueError as ve:  # Si erreur lors lecture
        raise ValueError('Aucune donnée récupérée dans Sacha.') from ve
    # -------------------------------------------------------------------------
    # 2- Ajout dans la collection
    # -------------------------------------------------------------------------
    for c in df.columns:
        serie = Serie(df[c], code=c[0], varname=c[1], warning=warning)
        series.add(serie=serie)
    return series
