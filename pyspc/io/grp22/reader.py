#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Bibliothèque pyspc du projet pyspc - IO - GRP version 2022 - read
"""
from datetime import timedelta as td
import os.path
import pandas as pnd

from pyspc.convention.grp22 import (
    DATATYPES, CAL_EVENT_HEADERS, RT_FCST_LINEPREFIX, RT_FCST_VARNAMES)
from pyspc.core.convention import EXTERNAL_VARNAMES
import pyspc.core.exception as _exception
from pyspc.core.keyseries import str2tuple, tuple2str
from pyspc.core.parameter import Parameter
from pyspc.core.serie import Serie
from pyspc.core.series import Series
from pyspc.model.grp22 import (
    str2td, GRP_Data, GRP_Event, GRP_Fcst, GRPRT_Archive, GRPRT_Basin,
    GRPRT_Data, GRPRT_Metscen, GRPRT_Fcst, GRPRT_Intern)


def read_GRP22(datatype=None, dirname='.', filename=None,
               stations=None, varnames=None, warning=False):
    """
    Créer une instance Series à partir de données GRP22

    Parameters
    ----------
    filename : str
        Fichier GRP
    datatype : str
        Type de données GRP
    warning : bool
        Afficher les avertissements ? défaut: False

    Other Parameters
    ----------------
    dirname : str
        Répertoire des fichiers GRP, si filename n'est pas défini
    stations : list
        Identifiants des stations, si filename n'est pas défini
    varnames : list
        Noms des variables, si filename n'est pas défini

    Returns
    -------
    series : pyspc.core.series.Series
        Collection de séries de données

    Notes
    -----
    Les colonnes ne contenant que des valeurs manquantes ne sont pas exportées

    Examples
    --------

    >>> from pyspc.io.grp22 import read_GRP22


    CALAGE - Cas de données d'observation

    >>> d = 'grp22_cal_data'
    >>> f = 'data/model/grp22/cal/90065003_P_00J01H00M.txt'
    >>> series = read_GRP22(filename=f, datatype=d)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = GRP22
    *  TYPE DE COLLECTION   = obs
    *  NOMBRE DE SERIES     = 1
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = 90065003
    *      - VARNAME = PH
    *      - META    = None
    *************************************


    CALAGE - Cas de données d'événements horaires

    >>> d = 'grp22_cal_fcst'
    >>> f = 'data/model/grp22/cal/RH10585x-EVhor1.DAT'
    >>> series = read_GRP22(filename=f, datatype=d)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = GRP22
    *  TYPE DE COLLECTION   = obs
    *  NOMBRE DE SERIES     = 2
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = RH10585x
    *      - VARNAME = PH
    *      - META    = EVhor1
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = EVhor1
    *************************************


    CALAGE - Cas de données d'événements journaliers

    >>> d = 'grp22_cal_fcst'
    >>> f = 'data/model/grp22/cal/RH10585x-EVjour1.DAT'
    >>> series = read_GRP22(filename=f, datatype=d)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = GRP22
    *  TYPE DE COLLECTION   = obs
    *  NOMBRE DE SERIES     = 2
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = RH10585x
    *      - VARNAME = PJ
    *      - META    = EVjour1
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = RH10585x
    *      - VARNAME = QJ
    *      - META    = EVjour1
    *************************************

    CALAGE - Cas de données de prévision

    >>> d = 'grp22_cal_fcst'
    >>> f = 'data/model/grp22/cal/H_RH10585x_GRP_SMN_TAN_HOR_00J03H00M_PDT_00J01H00M_PP_P0P0.TXT'
    >>> series = read_GRP22(filename=f, datatype=d)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = GRP22
    *  TYPE DE COLLECTION   = obs
    *  NOMBRE DE SERIES     = 19
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = OBS00J00H00M
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = OBS00J01H00M
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = PRV00J01H00M
    *  ----------------------------------
    *  SERIE #4
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = OBS00J03H00M
    *  ----------------------------------
    *  SERIE #5
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = PRV00J03H00M
    *  ----------------------------------
    *  SERIE #6
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = OBS00J06H00M
    *  ----------------------------------
    *  SERIE #7
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = PRV00J06H00M
    *  ----------------------------------
    *  SERIE #8
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = OBS00J12H00M
    *  ----------------------------------
    *  SERIE #9
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = PRV00J12H00M
    *  ----------------------------------
    *  SERIE #10
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = OBS01J00H00M
    *  ----------------------------------
    *  SERIE #11
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = PRV01J00H00M
    *  ----------------------------------
    *  SERIE #12
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = OBS01J12H00M
    *  ----------------------------------
    *  SERIE #13
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = PRV01J12H00M
    *  ----------------------------------
    *  SERIE #14
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = OBS02J00H00M
    *  ----------------------------------
    *  SERIE #15
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = PRV02J00H00M
    *  ----------------------------------
    *  SERIE #16
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = OBS03J00H00M
    *  ----------------------------------
    *  SERIE #17
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = PRV03J00H00M
    *  ----------------------------------
    *  SERIE #18
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = OBS05J00H00M
    *  ----------------------------------
    *  SERIE #19
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = PRV05J00H00M
    *************************************

    TEMPS-REEL - Cas de données d'observation (Q)

    >>> d = 'grp22_rt_data'
    >>> f = 'data/model/grp22/rt/Debit.txt'
    >>> series = read_GRP22(filename=f, datatype=d)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = GRP22
    *  TYPE DE COLLECTION   = obs
    *  NOMBRE DE SERIES     = 1
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = RH10585x
    *      - VARNAME = QI
    *      - META    = None
    *************************************


    TEMPS-REEL - Cas de données d'observation (P)

    >>> d = 'grp22_rt_data'
    >>> f = 'data/model/grp22/rt/Pluie_00J01H00M.txt'
    >>> series = read_GRP22(filename=f, datatype=d)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = GRP22
    *  TYPE DE COLLECTION   = obs
    *  NOMBRE DE SERIES     = 2
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = 90052002
    *      - VARNAME = PH
    *      - META    = None
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = 90065003
    *      - VARNAME = PH
    *      - META    = None
    *************************************

    TEMPS-REEL - Cas de scénarios météorologiques

    >>> d = 'grp22_rt_metscen'
    >>> f = 'data/model/grp22/rt/Scen_006_PluMA.txt'
    >>> series = read_GRP22(filename=f, datatype=d)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = GRP22
    *  TYPE DE COLLECTION   = fcst
    *  NOMBRE DE SERIES     = 1
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = RH10585x
    *      - VARNAME = PH
    *      - META    = 2021-05-31 01:00:00, grp22, 001, None
    *************************************

    TEMPS-REEL - Cas d'export d'observation

    >>> d = 'grp22_rt_obs_diff'
    >>> f = 'data/model/grp22/rt/GRP_D_Obs.txt'
    >>> series = read_GRP22(filename=f, datatype=d)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = GRP22
    *  TYPE DE COLLECTION   = obs
    *  NOMBRE DE SERIES     = 5
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = grp22
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = RH10585x
    *      - VARNAME = PH
    *      - META    = grp22
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = RH10585x
    *      - VARNAME = QJ
    *      - META    = grp22
    *  ----------------------------------
    *  SERIE #4
    *      - CODE    = RH10585x
    *      - VARNAME = PJ
    *      - META    = grp22
    *  ----------------------------------
    *  SERIE #5
    *      - CODE    = RH10585x
    *      - VARNAME = TJ
    *      - META    = grp22
    *************************************

    TEMPS-REEL - Cas de prévision sans assimilation

    >>> d = 'grp22_rt_sim_diff'
    >>> f = 'data/model/grp22/rt/GRP_D_Simu_2001.txt'
    >>> series = read_GRP22(filename=f, datatype=d)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = GRP22
    *  TYPE DE COLLECTION   = fcst
    *  NOMBRE DE SERIES     = 5
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = 2007-01-19 00:00:00, grp22-sim, 2001, None
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = RH10585x
    *      - VARNAME = PH
    *      - META    = 2007-01-19 00:00:00, grp22-sim, 2001, None
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = RH10585x
    *      - VARNAME = QJ
    *      - META    = 2007-01-19 00:00:00, grp22-sim, 2001, None
    *  ----------------------------------
    *  SERIE #4
    *      - CODE    = RH10585x
    *      - VARNAME = PJ
    *      - META    = 2007-01-19 00:00:00, grp22-sim, 2001, None
    *  ----------------------------------
    *  SERIE #5
    *      - CODE    = RH10585x
    *      - VARNAME = TJ
    *      - META    = 2007-01-19 00:00:00, grp22-sim, 2001, None
    *************************************

    TEMPS-REEL - Cas de prévision avec assimilation

    >>> d = 'grp22_rt_fcst_diff'
    >>> f = 'data/model/grp22/rt/GRP_D_Prev_2001.txt'
    >>> series = read_GRP22(filename=f, datatype=d)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = GRP22
    *  TYPE DE COLLECTION   = fcst
    *  NOMBRE DE SERIES     = 5
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = 2007-01-19 00:00:00, grp22, 2001, None
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = RH10585x
    *      - VARNAME = PH
    *      - META    = 2007-01-19 00:00:00, grp22, 2001, None
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = RH10585x
    *      - VARNAME = QJ
    *      - META    = 2007-01-19 00:00:00, grp22, 2001, None
    *  ----------------------------------
    *  SERIE #4
    *      - CODE    = RH10585x
    *      - VARNAME = PJ
    *      - META    = 2007-01-19 00:00:00, grp22, 2001, None
    *  ----------------------------------
    *  SERIE #5
    *      - CODE    = RH10585x
    *      - VARNAME = TJ
    *      - META    = 2007-01-19 00:00:00, grp22, 2001, None
    *************************************

    TEMPS-REEL - Cas de données d'archive

    >>> d = 'grp22_rt_archive'
    >>> f = 'data/model/grp22/rt/PV_10A.DAT'
    >>> series = read_GRP22(filename=f, datatype=d)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = GRP22
    *  TYPE DE COLLECTION   = obs
    *  NOMBRE DE SERIES     = 1
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = 90065003
    *      - VARNAME = PH
    *      - META    = None
    *************************************

    TEMPS-REEL - Cas de données internes au modèle

    >>> d = 'grp22_rt_intern_diff'
    >>> f = 'data/model/grp22/rt/intern/PQE_1A_D.DAT'
    >>> series = read_GRP22(filename=f, datatype=d)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = GRP22
    *  TYPE DE COLLECTION   = obs
    *  NOMBRE DE SERIES     = 4
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = grp22
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = RH10585x
    *      - VARNAME = QH
    *      - META    = None
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = RH10585x
    *      - VARNAME = PH
    *      - META    = None
    *  ----------------------------------
    *  SERIE #4
    *      - CODE    = RH10585x
    *      - VARNAME = EH
    *      - META    = None
    *************************************

    """
    # -------------------------------------------------------------------------
    # 0- Contrôles
    # -------------------------------------------------------------------------
    _exception.raise_valueerror(
        datatype not in DATATYPES,
        "Type de données GRP22 '{}' incorrect".format(datatype)
    )
    # -------------------------------------------------------------------------
    # 1.1- CALAGE - Cas de données d'observation
    # -------------------------------------------------------------------------
    if datatype == "grp22_cal_data":
        return _grp22_cal_data(
            dirname=dirname, filename=filename, stations=stations,
            varnames=varnames, warning=warning)
    # -------------------------------------------------------------------------
    # 1.2- CALAGE - Cas de données d'événement
    # -------------------------------------------------------------------------
    if datatype == "grp22_cal_event":
        return _grp22_cal_event(filename=filename, warning=warning)
    # -------------------------------------------------------------------------
    # 1.3- CALAGE - Cas de données de prévision
    # -------------------------------------------------------------------------
    if datatype == "grp22_cal_fcst":
        return _grp22_cal_fcst(filename=filename, warning=warning)
    # -------------------------------------------------------------------------
    # 2.1- TEMPS-REEL - Cas de données d'archive
    # -------------------------------------------------------------------------
    if datatype.startswith('grp22_rt_archive'):
        return _grp22_rt_archive(filename=filename, warning=warning)
    # -------------------------------------------------------------------------
    # 2.2- TEMPS-REEL - Cas de données d'observation
    #      TEMPS-REEL - Cas de scénarios météorologiques
    # -------------------------------------------------------------------------
    if datatype.startswith('grp22_rt_data'):
        return _grp22_rt_data(filename=filename, warning=warning)
    if datatype.startswith('grp22_rt_metscen'):
        return _grp22_rt_metscen(filename=filename, warning=warning)
    # -------------------------------------------------------------------------
    # 2.3- TEMPS-REEL - Cas d'export d'observation
    #      TEMPS-REEL - Cas de prévision sans assimilation
    #      TEMPS-REEL - Cas de prévision avec assimilation
    # -------------------------------------------------------------------------
    if (datatype.startswith('grp22_rt_obs')
            or datatype.startswith('grp22_rt_sim')
            or datatype.startswith('grp22_rt_fcst')):
        return _grp22_rt_fcst(
            filename=filename, datatype=datatype.replace('grp22_rt_', ''),
            warning=warning)
    # -------------------------------------------------------------------------
    # 2.4- TEMPS-REEL - Cas de données internes au modèle
    # -------------------------------------------------------------------------
    if datatype.startswith('grp22_rt_intern'):
        return _grp22_rt_intern(filename=filename, warning=warning)
        # return _grp22_rt_intern(
            # filename=filename, datatype=datatype.replace('grp22_rt_', ''),
            # warning=warning)
    # -------------------------------------------------------------------------
    # 3- Cas inconnu
    # -------------------------------------------------------------------------
    raise NotImplementedError


def _grp22_cal_data(dirname='.', filename=None,
                    stations=None, varnames=None, warning=False):
    """
    CALAGE - Cas de données d'observation
    """
    # -------------------------------------------------------------------------
    # 0- Initialisation
    # -------------------------------------------------------------------------
    if stations is None or varnames is None:
        _exception.check_str(filename)
        filenames = [filename]
    else:
        _exception.check_str(dirname)
        if isinstance(stations, str):
            stations = [stations]
        if isinstance(varnames, str):
            varnames = [varnames]
        external = {v: k[1]
                    for k, v in EXTERNAL_VARNAMES.items()
                    if k[0] == 'GRP22'}
        varnames = [external[v] for v in varnames if v in external]
        filenames = [os.path.join(
            dirname, GRP_Data.join_basename(
                station=s, varname=v[0], timestep=v[1]))
                     for s in stations for v in varnames]
    series = Series(datatype='obs', name='GRP22')
    # -------------------------------------------------------------------------
    # 1- Lecture
    # -------------------------------------------------------------------------
    for f in filenames:
        if not os.path.exists(f):
            if warning:
                _exception.Warning(
                    __file__, "Fichier inconnu '{}'".format(f))
            continue
        reader = GRP_Data(filename=f)
        ts = str2td(reader.timestep)
        df = reader.read()
    # -------------------------------------------------------------------------
    # 2- Collection de séries
    # -------------------------------------------------------------------------
        serie = Serie(
            datval=df, code=reader.station, varname=(reader.varname, ts),
            provider='GRP22', warning=warning)
        series.add(serie)
    return series


def _grp22_cal_event(filename=None, warning=False):
    """
    CALAGE - Cas de données d'événements
    """
    # -------------------------------------------------------------------------
    # 0- Initialisation
    # -------------------------------------------------------------------------
    series = Series(datatype='obs', name='GRP22')
    # -------------------------------------------------------------------------
    # 1- Lecture
    # -------------------------------------------------------------------------
    reader = GRP_Event(filename=filename)
    df = reader.read()
    df = df.set_index('Date', drop=True)
    # -------------------------------------------------------------------------
    # 2- Collection de séries
    # -------------------------------------------------------------------------
    assoc = {v: k for k, v in CAL_EVENT_HEADERS.items()}
    for c in df.columns:
        try:
            v = assoc[c]
        except KeyError:
            continue
        ts = Parameter.infer_timestep(index=df.index, prefix=v)
        serie = Serie(
            datval=df[c].to_frame(), code=reader.station, varname=(v, ts),
            provider='GRP22', warning=warning)
        series.add(serie, meta=reader.event)
    return series


def _grp22_cal_fcst(filename=None, warning=False):
    """
    CALAGE - Cas de données de prévision
    """
    # -------------------------------------------------------------------------
    # 0- Initialisation
    # -------------------------------------------------------------------------
    series = Series(datatype='obs', name='GRP22')
    # -------------------------------------------------------------------------
    # 1- Lecture
    # -------------------------------------------------------------------------
    reader = GRP_Fcst(filename=filename)
    df = reader.read()
    ts = str2td(reader.timestep)
    # -------------------------------------------------------------------------
    # 2- Collection de séries
    # -------------------------------------------------------------------------
    for c in df.columns:
        dv = df[c].to_frame()
        try:
            dts = str2td(c[3:])
        except ValueError:
            dts = td(0)
        dv.index = dv.index + dts
        serie = Serie(
            datval=dv, code=reader.station, varname=('Q', ts),
            provider='GRP22', warning=warning)
        series.add(serie, meta=c.strip())
    return series


def _grp22_rt_archive(filename=None, warning=False):
    """
    TEMPS-REEL - Cas de données d'archive
    """
    # -------------------------------------------------------------------------
    # 0- Initialisation
    # -------------------------------------------------------------------------
    series = Series(datatype='obs', name='GRP22')
    # -------------------------------------------------------------------------
    # 1- Lecture
    # -------------------------------------------------------------------------
    reader = GRPRT_Archive(filename=filename)
    df = reader.read()
    ts = str2td(reader.timestep)
    # -------------------------------------------------------------------------
    # 2- Collection de séries
    # -------------------------------------------------------------------------
    for c in df.columns:
        serie = Serie(
            datval=df[c].to_frame(), code=c.strip(),
            varname=(reader.varname[0], ts), provider='GRP22', warning=warning)
        series.add(serie)  # , meta='archive'
    return series


def _grp22_rt_data(filename=None, warning=None):
    """
    TEMPS-REEL - Cas de données d'observation
    """
    # -------------------------------------------------------------------------
    # 0- Initialisation
    # -------------------------------------------------------------------------
    # -------------------------------------------------------------------------
    # 1- Lecture
    # -------------------------------------------------------------------------
    reader = GRPRT_Data(filename=filename)
    df = reader.read()
    # Nettoyer la colonne superflue
    df = df.dropna(how="all", axis=1)
    # Nettoyer les lignes != prefix
    df.drop(df[df.PREFIX != reader.lineprefix].index, inplace=True)
    # Supprimer la colonne PREFIX
    df.drop(columns=['PREFIX'], inplace=True)
    # Créer l'index Datetime à partir des colonnes DATE et HOUR
    df.index = pnd.to_datetime(df['DATE'].astype(str) + ' ' + df['HOUR'],
                               format='%Y%m%d %H:00')
    # Supprimer les colonne 'DATE', 'HOUR'
    df.drop(columns=['DATE', 'HOUR'], inplace=True)
    # Colonne 'CODE' -> Multi-Index (None, CODE) avec None: ['VALUE', 'CODE']
    df = df.pivot(columns='CODE')
    # Retirer VALUE en trop dans columns
    df.columns = df.columns.droplevel(0)
    ts = str2td(reader.timestep)
    # -------------------------------------------------------------------------
    # 2.1- Collection de séries OBS
    # -------------------------------------------------------------------------
    series = Series(datatype='obs', name='GRP22')
    for c in df.columns:
        serie = Serie(
            datval=df[c].to_frame(), code=str(c), varname=(reader.varname, ts),
            provider='GRP22', warning=warning)
        series.add(serie)
    return series


def _grp22_rt_metscen(filename=None, warning=None):
    """
    TEMPS-REEL - Cas de scénarios météorologiques
    """
    # -------------------------------------------------------------------------
    # 0- Initialisation
    # -------------------------------------------------------------------------
    # -------------------------------------------------------------------------
    # 1- Lecture
    # -------------------------------------------------------------------------
    reader = GRPRT_Metscen(filename=filename)
    df = reader.read()
    # Nettoyer les lignes != prefix
    df.drop(df[df.PREFIX != reader.lineprefix].index, inplace=True)
    # Supprimer la colonne PREFIX
    df.drop(columns=['PREFIX'], inplace=True)
    # Créer l'index Datetime à partir des colonnes DATE et HOUR
    df.index = pnd.to_datetime(df['DATETIME'].astype(str), format='%Y%m%d%H%M')
    # Supprimer les colonne 'DATE', 'HOUR'
    df.drop(columns=['DATETIME'], inplace=True)
    # Colonne 'CODE' -> Multi-Index (None, CODE) avec None: ['VALUE', 'CODE']
    df = df.pivot(columns='CODE')
    # Retirer VALUE en trop dans columns
    df.columns = df.columns.droplevel(0)
    ts = str2td(reader.timestep)
    # -------------------------------------------------------------------------
    # 2.2- Collection de séries SIM/PRV
    # -------------------------------------------------------------------------
    series = Series(datatype='fcst', name='GRP22')
    for c in df.columns:
        runtime = df[c].first_valid_index().to_pydatetime()
        param = Parameter(varname=(reader.varname, ts), provider='GRP22')
        key = (c, param.spc_varname, (runtime, 'grp22', reader.scen, None))
        keystr = str2tuple(tuple2str(key), forceobs=True)[0]
        serie = Serie(
            datval=df[c].to_frame(), code=keystr, varname=param.spc_varname,
            provider='GRP22', warning=warning)
        series.add(serie, code=c, meta=key[2])
    return series


def _grp22_rt_fcst(filename=None, datatype=None, warning=None):
    """
    TEMPS-REEL - Cas d'export d'observation
    TEMPS-REEL - Cas de prévision sans assimilation
    TEMPS-REEL - Cas de prévision avec assimilation
    """
    # -------------------------------------------------------------------------
    # 0- Initialisation
    # -------------------------------------------------------------------------
    # Préfixe de la ligne
    lineprefix = RT_FCST_LINEPREFIX[datatype.split('_')[0]]
    # -------------------------------------------------------------------------
    # 1- Lecture
    # -------------------------------------------------------------------------
    # Lecture du contenu
    reader = GRPRT_Fcst(filename=filename, datatype=datatype)
    df = reader.read()
    # Nettoyer les labels des colonnes
    df.columns = [c.strip() for c in df.columns]
    # Nettoyer les lignes != lineprefix
    df = df.drop(df[df.TYP != lineprefix].index)
    df = df.dropna(axis=1, how='all')
    # Supprimer la colonne TYP
    df.drop(columns=['TYP'], inplace=True)
    # Nettoyer les CODE
    df['CODE'] = df['CODE'].map(lambda x: x.strip())
    df['PDT'] = df['PDT'].map(lambda x: x.strip())
    # Colonne 'DATE' -> Index
    df = df.set_index(keys='DATE(TU)', drop=True)
    # Colonne 'Code' -> Multi-Index (None, Code) avec None: ['Debit (l/s)']
    df['CODE_PDT'] = df['CODE'] + '_' + df['PDT']
    # Supprimer les colonne 'DATE', 'HOUR'
    df.drop(columns=['CODE', 'PDT'], inplace=True)
    # -------------------------------------------------------------------------
    # 2.2- Collection de séries SIM/PRV
    # -------------------------------------------------------------------------
    if datatype.startswith('obs'):
        series = Series(datatype='obs', name='GRP22')
    else:
        series = Series(datatype='fcst', name='GRP22')
    model = 'grp22-{}'.format(lineprefix.lower()).replace('-prv', '')
    for _, x in df.groupby(df['CODE_PDT']):
        # Nettoyer les lignes != lineprefix
        x = x.dropna(axis=1, how='all')
        x = x.pivot(columns='CODE_PDT')
        # MultiIndex : (CODE, VAR)
        x.columns = x.columns.swaplevel()
        x.columns = [(c[0], RT_FCST_VARNAMES[c[1].split('(')[0]])
                     for c in x.columns]
        for c in x.columns:
            [code, ts] = c[0].split('_')
            ts = str2td(ts)
            varname = EXTERNAL_VARNAMES[('GRP22', (c[1], ts))]
            if series.datatype == 'obs':
                serie = Serie(
                    datval=x[c].to_frame(), code=code, varname=varname,
                    provider='GRP22', warning=warning)
                series.add(serie, meta='grp22')
            else:
                runtime = x.index[0] - ts
                runtime = runtime.to_pydatetime()
                key = (code, varname, (runtime, model, reader.scen, None))
                keystr = str2tuple(tuple2str(key), forceobs=True)[0]
                serie = Serie(
                    datval=x[c].to_frame(), code=keystr, varname=varname,
                    provider='GRP22', warning=warning)
                series.add(serie, code=code, meta=key[2])
    return series


def _grp22_rt_intern(filename=None, warning=False):
    """
    TEMPS-REEL - Cas de données internes au modèle
    """
    # -------------------------------------------------------------------------
    # 0- Initialisation
    # -------------------------------------------------------------------------
    series = Series(datatype='obs', name='GRP22')
    varnames = {'Qsim(mm)': 'Q', 'Qobs(mm)': 'Q', 'P(mm)': 'P', 'T(C)': 'T',
                'ETP(mm)': 'E'}
    # -------------------------------------------------------------------------
    # 1- Lecture
    # -------------------------------------------------------------------------
    reader = GRPRT_Intern(filename=filename)
#    reader.check_datatype(datatype)
    df = reader.read()
    df = df.reindex(varnames.keys(), axis=1).dropna(how='all', axis=1)
    basin = GRPRT_Basin(
        filename=os.path.join(os.path.dirname(filename), 'BASSIN.DAT'))
    basin.read()
    code = basin['Q']
    area = float(basin['S'])
    # -------------------------------------------------------------------------
    # 2- Collection de séries
    # -------------------------------------------------------------------------
    for c in df.columns:
        v = varnames[c]
        target = Parameter.infer_timestep(index=df.index, prefix=v)
        param = Parameter.find(prefix=v, timedelta=target)
        if v.startswith('Q'):
            df[c] = df[c] * area / param.timestep.total_seconds() * 1000
        if 'sim' in c:
            meta = 'grp22'
        else:
            meta = None
        serie = Serie(
            datval=df[c].to_frame(), code=code, varname=param.spc_varname,
            provider='GRP22', warning=warning)
        series.add(serie, meta=meta)
    return series
