#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Bibliothèque pyspc du projet pyspc - IO - Mordor - read
"""
from pyspc.core.keyseries import str2tuple, tuple2str
from pyspc.core.serie import Serie
from pyspc.core.series import Series
import pyspc.core.exception as _exception
from pyspc.model.mordor import Fcst


def read_Mordor(filename=None, meteo=None, warning=True):
    """
    Créer une instance Series à partir d'un fichier QMJ de Mordor

    Parameters
    ----------
    filename : str
        Nom du fichier MORDOR (observation ou prévision)
    meteo : str
        Identifiant de l'ensemble météo
    warning : bool
        Imprimer les erreurs ?

    Returns
    -------
    series : pyspc.core.series.Series
        Collection de séries de données

    Examples
    --------
    >>> from pyspc.io.mordor import read_Mordor

    Cas d'un fichier de prévisions 'spaghettis'

    >>> f = 'data/model/mordor/Spaghettis_BASSIN_20220623.txt'
    >>> series = read_Mordor(filename=f)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = MORDOR_Fcst
    *  TYPE DE COLLECTION   = fcst
    *  NOMBRE DE SERIES     = 3
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = BASSIN
    *      - VARNAME = QJ
    *      - META    = 2022-06-23 00:00:00, MORDOR, CEPQ2018, None
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = BASSIN
    *      - VARNAME = QJ
    *      - META    = 2022-06-23 00:00:00, MORDOR, CEPQ2019, None
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = BASSIN
    *      - VARNAME = QJ
    *      - META    = 2022-06-23 00:00:00, MORDOR, CEPQ2020, None
    *************************************

    """
    # -------------------------------------------------------------------------
    # 0- Lecture brute
    # -------------------------------------------------------------------------
    _exception.check_str(filename)
    _exception.check_bool(warning)
    # -------------------------------------------------------------------------
    # 2- Prévision
    # -------------------------------------------------------------------------
    series = Series(datatype='fcst', name='MORDOR_Fcst')
    # Création du lecteur
    reader = Fcst(filename=filename, meteo=meteo)
    # Récupération des méta-données
    loc = reader.station
    runtime = reader.runtime
    model = reader.model
    scen = reader.meteo
    # Lecture des données
    df = reader.read()
    # Création de Serie
    for m in df.columns:
        scen_m = '{}{}'.format(scen, m)
        serie = Serie(
            df[m], code=loc, provider='Mordor', varname=reader.varname,
            warning=warning)
        k = (loc, serie.spc_varname, (runtime, model, scen_m, None))
        series.add(serie=serie, meta=k[2])
        series[k].code = str2tuple(tuple2str(k), forceobs=True)[0]
    return series
