#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Bibliothèque pyspc du projet pyspc - IO - Premhyce - read
"""
from datetime import timedelta as td
import pandas as pnd

from pyspc.convention.premhyce import RATIO
from pyspc.core.keyseries import str2tuple, tuple2str
from pyspc.core.serie import Serie
from pyspc.core.series import Series
import pyspc.core.exception as _exception
from pyspc.model.premhyce import Data, Fcst


def read_Premhyce(filename=None, warning=True):
    """
    Créer une instance Series à partir d'un fichier QMJ de PREMHYCE

    Parameters
    ----------
    filename : str
        Nom du fichier PREMHYCE (observation ou prévision)
    warning : bool
        Imprimer les erreurs ?

    Returns
    -------
    series : pyspc.core.series.Series
        Collection de séries de données

    Examples
    --------
    >>> from pyspc.io.premhyce import read_Premhyce

    Cas d'un fichier d'observation

    >>> f = 'data/model/premhyce/Debits.txt'
    >>> series = read_Premhyce(filename=f)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = PREMHYCE_Data
    *  TYPE DE COLLECTION   = obs
    *  NOMBRE DE SERIES     = 2
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = K0253030
    *      - VARNAME = QJ
    *      - META    = None
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = K0260020
    *      - VARNAME = QJ
    *      - META    = None
    *************************************

    Cas d'un fichier de prévision

    >>> f = 'data/model/premhyce/20200612_K0253030_GR6J_CEP.txt'
    >>> series = read_Premhyce(filename=f)
    >>> series
    *************************************
    ********** SERIES *******************
    *************************************
    *  NOM DE LA COLLECTION = PREMHYCE_Fcst
    *  TYPE DE COLLECTION   = fcst
    *  NOMBRE DE SERIES     = 11
    *  ----------------------------------
    *  SERIE #1
    *      - CODE    = K0253030_2020061200_GR6J_CEPcf
    *      - VARNAME = QJ
    *      - META    = 2020-06-12 00:00:00, GR6J, CEPcf, None
    *  ----------------------------------
    *  SERIE #2
    *      - CODE    = K0253030_2020061200_GR6J_CEPpf1
    *      - VARNAME = QJ
    *      - META    = 2020-06-12 00:00:00, GR6J, CEPpf1, None
    *  ----------------------------------
    *  SERIE #3
    *      - CODE    = K0253030_2020061200_GR6J_CEPpf2
    *      - VARNAME = QJ
    *      - META    = 2020-06-12 00:00:00, GR6J, CEPpf2, None
    *  ----------------------------------
    *  SERIE #4
    *      - CODE    = K0253030_2020061200_GR6J_CEPpf3
    *      - VARNAME = QJ
    *      - META    = 2020-06-12 00:00:00, GR6J, CEPpf3, None
    *  ----------------------------------
    *  SERIE #5
    *      - CODE    = K0253030_2020061200_GR6J_CEPpf4
    *      - VARNAME = QJ
    *      - META    = 2020-06-12 00:00:00, GR6J, CEPpf4, None
    *  ----------------------------------
    *  SERIE #6
    *      - CODE    = K0253030_2020061200_GR6J_CEPpf5
    *      - VARNAME = QJ
    *      - META    = 2020-06-12 00:00:00, GR6J, CEPpf5, None
    *  ----------------------------------
    *  SERIE #7
    *      - CODE    = K0253030_2020061200_GR6J_CEPpf6
    *      - VARNAME = QJ
    *      - META    = 2020-06-12 00:00:00, GR6J, CEPpf6, None
    *  ----------------------------------
    *  SERIE #8
    *      - CODE    = K0253030_2020061200_GR6J_CEPpf7
    *      - VARNAME = QJ
    *      - META    = 2020-06-12 00:00:00, GR6J, CEPpf7, None
    *  ----------------------------------
    *  SERIE #9
    *      - CODE    = K0253030_2020061200_GR6J_CEPpf8
    *      - VARNAME = QJ
    *      - META    = 2020-06-12 00:00:00, GR6J, CEPpf8, None
    *  ----------------------------------
    *  SERIE #10
    *      - CODE    = K0253030_2020061200_GR6J_CEPpf9
    *      - VARNAME = QJ
    *      - META    = 2020-06-12 00:00:00, GR6J, CEPpf9, None
    *  ----------------------------------
    *  SERIE #11
    *      - CODE    = K0253030_2020061200_GR6J_CEPpf10
    *      - VARNAME = QJ
    *      - META    = 2020-06-12 00:00:00, GR6J, CEPpf10, None
    *************************************

    """
    # -------------------------------------------------------------------------
    # 0- Lecture brute
    # -------------------------------------------------------------------------
    _exception.check_str(filename)
    _exception.check_bool(warning)
    isdata = False
    isfcst = False
    try:
        Fcst.split_basename(filename=filename)
    except ValueError:
        isdata = True
    else:
        isfcst = True
    # -------------------------------------------------------------------------
    # 1- Observation
    # -------------------------------------------------------------------------
    if isdata:
        series = Series(datatype='obs', name='PREMHYCE_Data')
        # Création du lecteur
        reader = Data(filename=filename)
        # Lecture des données
        df = reader.read()
        # Colonne 'DATE' -> Index
        df = df.set_index(keys='DATE', drop=True)
        # Supprimer la colonne inutile
        df = df.drop('TYP', axis=1)
        # Colonne 'CODE' -> Multi-Index (None, CODE) avec None: ['DEBIT(l/s)']
        df = df.pivot(columns='CODE')
        # Colonne : CODE seulement
        df = df.droplevel(level=0, axis=1)
        df = df * RATIO
        # Création de Serie
        for c in df.columns:
            serie = Serie(
                df[c], code=c, provider='Premhyce', varname='Data',
                warning=warning)
            series.add(serie=serie)
        return series
    # -------------------------------------------------------------------------
    # 2- Prévision
    # -------------------------------------------------------------------------
    if isfcst:
        series = Series(datatype='fcst', name='PREMHYCE_Fcst')
        # Création du lecteur
        reader = Fcst(filename=filename)
        # Récupération des méta-données
        loc = reader.station
        runtime = reader.runtime
        model = reader.model
        scen = reader.meteo
        # Lecture des données
        df = reader.read()
        # Nettoyage du tableau
        df = df.set_index('Membre', drop=True)
        starttime = pnd.Timestamp(df['Date'].values[0]).to_pydatetime()
        df = df.drop('Date', axis=1).transpose()
        df.index = [starttime + td(days=int(i.replace('jp', '')))
                    for i in df.index]
        df = df * RATIO
        # Création de Serie
        for m in df.columns:
            scen_m = '{}{}'.format(scen, m)
            serie = Serie(
                df[m], code=loc, provider='Premhyce', varname='Fcst',
                warning=warning)
            k = (loc, serie.spc_varname, (runtime, model, scen_m, None))
            serie.code = str2tuple(tuple2str(k), forceobs=True)[0]
            series.add(serie=serie, meta=k[2])
        return series
    # -------------------------------------------------------------------------
    # 3- Cas inconnu
    # -------------------------------------------------------------------------
    raise ValueError('Type de fichier Premhyce inconnu')
