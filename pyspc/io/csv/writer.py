#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Bibliothèque pyspc du projet pyspc - IO - csv - write
"""
import os.path

import pyspc.core.exception as _exception
from pyspc.core.keyseries import tuple2str


def write_csv(series=None, filename=None, keys=None):
    """
    Export vers csv

    Parameters
    ----------
    series : pyspc.core.series.Series
        Collection de séries de données
    filename : str
        Fichier csv à écrire
    keys : list
        Clés des séries à concaténer

    Returns
    -------
    filename : str
        Fichier csv créé

    """
    # -------------------------------------------------------------------------
    # 0- Contrôles
    # -------------------------------------------------------------------------
    _exception.check_str(filename)
    # -------------------------------------------------------------------------
    # 1- Manipulation du DataFrame
    # -------------------------------------------------------------------------
    df = series.concat(keys=keys)
    df.columns = [tuple2str(c) for c in df.columns]
    # -------------------------------------------------------------------------
    # 2- Export
    # -------------------------------------------------------------------------
    df.to_csv(
        filename,
        sep=';',
        line_terminator='\n',
        header=True,
        index_label='Date',
        date_format='%Y-%m-%d %H:%M'
    )
    return filename


def write_xls(series=None, filename=None, sheetname=None, keys=None,
              overwrite=True):
    """
    Export vers xls

    Parameters
    ----------
    series : pyspc.core.series.Series
        Collection de séries de données
    filename : str
        Fichier xls.
    sheet_name : str
        Feuille à écrire
    keys : list
        Clés des séries à concaténer.
    overwrite : bool
        Ecraser le fichier ? Si False, un fichier existant est complété

    Returns
    -------
    filename : str
        Fichier csv créé

    .. warning:: Cette méthode nécessite l'import de la
    bibliothèque tierce pandas.ExcelWriter

    .. warning:: Cette méthode nécessite l'import de la
    bibliothèque tierce xlwt

    """
    # -------------------------------------------------------------------------
    # 0- Contrôles
    # -------------------------------------------------------------------------
    _exception.check_str(filename)
    _exception.check_str(sheetname)
    _exception.check_bool(overwrite)
    # -------------------------------------------------------------------------
    # 1- Manipulation du DataFrame
    # -------------------------------------------------------------------------
    df = series.concat(keys=keys)
    df.columns = [tuple2str(c) for c in df.columns]
    # -------------------------------------------------------------------------
    # 2- Export
    # -------------------------------------------------------------------------
    from pandas import ExcelWriter
    if not overwrite and os.path.exists(filename):
        mode = 'a'
    else:
        mode = 'w'
    with ExcelWriter(
            filename, mode=mode, datetime_format='YYYY-MM-DD HH:MM') as writer:
        df.to_excel(
            writer,
            float_format='%.3f',
            header=True,
            index_label='Date',
            sheet_name=sheetname
        )
    return filename
