#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Bibliothèque pyspc du projet pyspc - IO - csv - read
"""
import os.path
import pandas as pnd

import pyspc.core.exception as _exception
from pyspc.core.keyseries import str2tuple
from pyspc.core.serie import Serie
from pyspc.core.series import Series


def read_csv(filename=None, datatype=None,
             forceobs=False, forcesim=False, warning=True):
    """
    Lire un fichier csv

    Parameters
    ----------
    filename : str
        Fichier csv
    datatype : str
        Type de collection
    warning : bool
        Afficher les avertissements. Défaut: True

    Other Parameters
    ----------------
    forceobs : bool
        Forcer la conversion en tant que série d'observation. Défaut: False
    forcesim : bool
        Forcer la conversion en tant que série de simulation. Défaut: False
        L'option forceobs a la préséance sur forcesim

    Returns
    -------
    series : pyspc.core.series.Series
        Collection de séries de données

    Notes
    -----
    Les entêtes des colonnes des données doivent respecter
    la convention de nommage. Voir pyspc.core.keyseries.str2tuple

    """
    # -------------------------------------------------------------------------
    # 0- Contrôles
    # -------------------------------------------------------------------------
    _exception.check_str(filename)
    # -------------------------------------------------------------------------
    # 1- Lecture
    # -------------------------------------------------------------------------
    name = os.path.splitext(os.path.basename(filename))[0]
    series = Series(datatype=datatype, name=name)
    df = pnd.read_csv(
        filename,
        sep=';',
        header=0,
        index_col=0,
        parse_dates=True
    )
    for c in df.columns:
        key = str2tuple(c, forceobs=forceobs, forcesim=forcesim)
        keystr = str2tuple(c, forceobs=True)
        serie = Serie(df[c], code=keystr[0], varname=key[1], warning=warning)
        series.add(serie=serie, code=key[0], meta=key[2])
    # -------------------------------------------------------------------------
    # 2- Retour
    # -------------------------------------------------------------------------
    return series


def read_xls(filename=None, sheetname=None, datatype=None,
             forceobs=False, forcesim=False, warning=True):
    """
    Lire un fichier xls

    Parameters
    ----------
    filename : str
        Fichier xls
    sheetname : str
        Feuille du fichier xls
    datatype : str
        Type de collection
    warning : bool
        Afficher les avertissements. Défaut: True

    Other Parameters
    ----------------
    forceobs : bool
        Forcer la conversion en tant que série d'observation. Défaut: False
    forcesim : bool
        Forcer la conversion en tant que série de simulation. Défaut: False
        L'option forceobs a la préséance sur forcesim

    Returns
    -------
    series : pyspc.core.series.Series
        Collection de séries de données

    Notes
    -----
    Les entêtes des colonnes des données doivent respecter
    la convention de nommage. Voir pyspc.core.keyseries.str2tuple

    """
    # -------------------------------------------------------------------------
    # 0- Contrôles
    # -------------------------------------------------------------------------
    _exception.check_str(filename)
    _exception.check_str(sheetname)
    # -------------------------------------------------------------------------
    # 1- Lecture
    # -------------------------------------------------------------------------
    name = os.path.splitext(os.path.basename(filename))[0]
    series = Series(datatype=datatype, name=name)
    df = pnd.read_excel(
        filename,
        sheet_name=sheetname,
        sep=';',
        header=0,
        index_col=0,
        parse_dates=True
    )
    for c in df.columns:
        key = str2tuple(c, forceobs=forceobs, forcesim=forcesim)
        keystr = str2tuple(c, forceobs=True)
        serie = Serie(df[c], code=keystr[0], varname=key[1], warning=warning)
        series.add(serie=serie, code=key[0], meta=key[2])
    # -------------------------------------------------------------------------
    # 2- Retour
    # -------------------------------------------------------------------------
    return series
