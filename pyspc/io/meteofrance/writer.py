#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Bibliothèque pyspc du projet pyspc - IO - Météo-France - write
"""
import numpy as np

from pyspc.core.convention import EXTERNAL_VARNAMES
from pyspc.core.series import Series
import pyspc.core.exception as _exception
from pyspc.data.meteofrance import MF_Data


def write_MF_Data(series=None, filename=None):
    """
    Créer un fichier d'observations de Météo-France
    à partir d'une instance Series

    Parameters
    ----------
    series : pyspc.core.series.Series
        Collection de séries de données
    filename : str
        Nom du fichier d'observations de Météo-France

    Returns
    -------
    filename : str
        Nom du fichier d'observations de Météo-France

    """
    # -------------------------------------------------------------------------
    # 0- Contrôles
    # -------------------------------------------------------------------------
    _exception.check_str(filename)
    _exception.raise_valueerror(
        not isinstance(series, Series),
        "'series' doit être une collection 'Series'"
    )
    # -------------------------------------------------------------------------
    # 1- Manipulation de Series
    # -------------------------------------------------------------------------
    # Grandeur
    try:
        varname = [k[1]
                   for k, v in EXTERNAL_VARNAMES.items()
                   if v == series.varnames[0] and k[0] == 'MF_Data']
        varname = varname[0]
        dtfmt = [s.dtfmt for s in series.values()][0]
    except IndexError as ie:
        raise ValueError('Grandeur inconnue') from ie
    # Concaténer les séries de données
    df = series.concat()
    # Retirer (POSTE, VAR) en trop dans columns
    df.columns = [x[0] for x in df.columns]
    # Inversion du pivot
    df = df.stack(0).to_frame()  # 0: POSTE
    df.columns = [varname]
    df.index.rename('DATE', level=0, inplace=True)
    df.index.rename('POSTE', level=1, inplace=True)
    # Pour trier par POSTE croissante puis par DATE croissante
    df.sort_index(level=['POSTE', 'DATE'], inplace=True)
    # Effacer l'index : ('DATE', 'POSTE') -> (0, 1, ... N)
    df = df.reset_index(level=['DATE', 'POSTE'])
    # Ré-arrangement des colonnes dans le bon ordre + ajout de la colonne Qxx
    df = df.reindex(columns=['POSTE', 'DATE', varname, 'Q{}'.format(varname)])
    # Remplir la colonne Qxxxx par 'v'
    df = df.fillna(value={'Q{}'.format(varname): 'v', varname: np.nan})
    # Formater la date
    df['DATE'] = df['DATE'].apply(lambda x: x.strftime(dtfmt))
    # -------------------------------------------------------------------------
    # 2- Ecriture du fichier
    # -------------------------------------------------------------------------
    writer = MF_Data(filename=filename)
    writer.write(df)
    return filename
