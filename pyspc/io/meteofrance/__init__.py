#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Bibliothèque pyspc du projet pyspc - IO - Météo-France
"""

from .reader import (
    read_MF_Data, read_BP, read_Sympo,
    read_MF_OpenAPI, read_MF_OpenData)
from .writer import write_MF_Data

__all__ = [
    'read_MF_Data', 'write_MF_Data',  # Publithèque
    'read_BP',  # Observation/Prévision BP
    'read_Sympo',  # Prévision Symposium
    'read_MF_OpenAPI',  # Portail API Publique
    'read_MF_OpenData',  # METEO.DATA.GOUV.FR.
]
