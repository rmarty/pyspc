#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <dbaseInfo.py>
"""

# modules Python
import sys
import argparse

# Module PySPC
from pyspc.binutils.print_args import print_args
from pyspc.metadata.refspc import RefSPC

DESCRIPTION = "Informations sur les lieux et tronçons de vigilance"
INFO = {
    "c": {
        "short": "Fichier de sortie",
        "help": "[optionnel] Fichier de sortie. Si non défini, renvoi vers "
                "la sortie standard"
    },
    "d": {
        "short": "Base de données",
        "help": "Nom du fichier de la base de données"
    },
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier d'entrée des données GRP"
    },
    "l": {
        "short": "Liste de stations",
        "help": "Fichier contenant la liste des stations Hydro. "
                "Non pris en compte si l'option -s est utilisée"
    },
    "s": {
        "short": "Station à extraire",
        "help": "Identité de la station"
    },
    "t": {
        "short": "Type de base de données",
        "help": "Type de base de données"
    },
    "v": {
        "short": "Mode bavard",
        "help": "[optionnel] Mode verbeux (defaut=False)"
    },
    "2": {
        "short": "Référentiel Hydro2",
        "help": "Référentiel Hydro2 (defaut=False)"
    },
    "3": {
        "short": "Référentiel Hydro3",
        "help": "Référentiel Hydro3 (defaut=False)"
    }
}
LIST_DATATYPES = RefSPC.get_datatypes()


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def dbaseInfo():
    """
    Validation des options et arguments de <dbaseInfo.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <dbaseInfo.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Création de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    # Référentiel des lieux : HYDRO2 (-2) ou HYDRO3 (-3)
    hydro_group = parser.add_mutually_exclusive_group(required=True)
    hydro_group.add_argument("-2", "--hydro-2",
                             action="store_true",
                             dest="hydro2",
                             default=False,
                             help=INFO["2"]["help"])
    hydro_group.add_argument("-3", "--hydro-3",
                             action="store_true",
                             dest="hydro3",
                             default=False,
                             help=INFO["3"]["help"])
    # Autres arguments
    parser.add_argument("-c", "--output-filename",
                        action="store",
                        dest="output_filename",
                        help=INFO["c"]["help"])
    parser.add_argument("-d", "--data-filename",
                        action="store",
                        dest="db_filename",
                        required=True,
                        help=INFO["d"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-t", "--data-type",
                        action="store",
                        dest="data_type",
                        choices=LIST_DATATYPES,
                        help=INFO["t"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    # Identifiants : unique ou par liste
    code_group = parser.add_mutually_exclusive_group(required=True)
    code_group.add_argument("-s", "--station-name",
                            action="store",
                            dest="station_name",
                            help=INFO["s"]["help"])
    code_group.add_argument("-l", "--station-list",
                            action="store",
                            dest="stations_list_file",
                            help=INFO["l"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
