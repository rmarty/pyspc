#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <xmlScores2png.py>
"""

# Modules Python
import sys
import argparse

from pyspc.binutils.print_args import print_args
from pyspc.convention.scores import SCORES_DTYPES  # SCORES_NAMES

DESCRIPTION = "Convertir les résultats Scores du xml au csv/png"
INFO = {
    "c": {
        "short": "[optionnel] Fichier de config de la figure",
        "help": "[optionnel] Nom du fichier de configuration de la figure"
    },
    "d": {
        "short": "Nom ou motif du fichier xml Scores",
        "help": "Nom ou motif du fichier Scores au format xml"
    },
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier des fichiers xml Scores"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier des fichiers csv et png"
    },
    "t": {
        "short": "Type de fichier XML Scores",
        "help": "Type de fichier XML Scores (sim, fcst)"
    },
    "v": {
        "short": "[optionnel] Mode verbeux",
        "help": "[optionnel] Mode verbeux (defaut=False)"
    }
}
LIST_DATATYPES = SCORES_DTYPES


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def xmlScores2png():
    """
    Validation des options et arguments de <xmlScores2png.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <xmlScores2png.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-c", "--config-filename",
                        action="store",
                        dest="cfg_filename",
                        help=INFO["c"]["help"])
    parser.add_argument("-d", "--data-filename",
                        action="store",
                        dest="xml_filename",
                        required=True,
                        help=INFO["d"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-t", "--data-type",
                        action="store",
                        dest="data_type",
                        required=True,
                        choices=LIST_DATATYPES,
                        help=INFO["t"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
