#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <phyc2plathynes.py>
"""

# modules Python
import argparse
import collections
import sys

from pyspc.binutils.print_args import print_args


METHODS = collections.OrderedDict()
METHODS['dl'] = {
    'longname': 'download',
    'short': "Télécharger les données",
    'help': "Télécharger les données",
    'param': None,
    'longparam': None
}
METHODS['cv'] = {
    'longname': 'convert',
    'short': 'Convertir les données',
    'help': 'Convertir les données',
    'param': None,
    'longparam': None
}
METHODS_STR = ''
for arg in METHODS:
    if METHODS[arg]['longname'] is not None:
        METHODS_STR += "\n+ {}/{} : ".format(arg, METHODS[arg]['longname'])
    else:
        METHODS_STR += "\n+ {} : ".format(arg)
    METHODS_STR += METHODS[arg]['help']
    if METHODS[arg]['param'] is not None:
        METHODS_STR += " (param: {})"\
                       "".format(" ".join(METHODS[arg]['param']))
METHODS_STR += "\n"


DESCRIPTION = "Télécharger / convertir les données PHYC (xml) pour PLATHYNES"
INFO = {
    "M": {
        "short": "Méthode de traitement",
        "help": "Méthodes de traitement des données Phyc pour PLATHYMES :"
                "" + METHODS_STR
    },
    "I": {
        "short": "Dossier des fichiers XML",
        "help": "Dossier des fichiers XML"
    },
    "D": {
        "short": "Fichier de config PHyC",
        "help": "Nom du fichier de configuration des paramètres"
                "de session et de scénario (au sens XML-Sandre)"
    },
    "O": {
        "short": "Répertoire du fichier projet PLATHYNES",
        "help": "Répertoire du fichier projet PLATHYNES"
    },
    "c": {
        "short": "Nom du fichier du projet PLATHYNES",
        "help": "Nom du fichier du projet PLATHYNES"
    },
    "s": {
        "short": "Station d'injection",
        "help": "Station d'injection"
    },
    "l": {
        "short": "Liste de stations d'injection",
        "help": "Fichier contenant la liste des stations d'injection"
                "Non pris en compte si l'option -s est utilisée"
    },
    "n": {
        "short": "Nom de la variable",
        "help": "Nom de la variable parmi 'P', 'Q'"
    },
    "S": {
        "short": "Liste des événements",
        "help": "[optionnel] Liste des événements. Si non défini, "
        "l'extraction est réalisée sur l'ensemble des événements "
        "du projet PLATHYNES"
    },
    "v": {
        "short": "Mode bavard",
        "help": "[optionnel] Mode verbeux (defaut=False)"
    },
    "w": {
        "short": "Mode Avertissement",
        "help": "[optionnel] Mode Avertissement (defaut=True). "
                "Si False, ne lève pas les erreurs lors de la "
                "création et lors de la manipulation des séries "
                "de données"
    }
}
LIST_VARNAMES = ['P', 'Q']


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def phyc2plathynes():
    """
    Validation des options et arguments de <phyc2plathynes.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    if args.varname is None:
        args.varname = LIST_VARNAMES
    else:
        args.varname = [args.varname]
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <phyc2plathynes.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Création de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-M", "--Method",
                        action="store",
                        dest="processing_method",
                        nargs='+',
                        required=True,
                        metavar=('Method', 'param(s)'),
                        help=INFO["M"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-D", "--config-filename",
                        action="store",
                        dest="cfg_filename",
                        help=INFO["D"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-n", "--varname",
                        action="store",
                        dest="varname",
                        choices=LIST_VARNAMES,
                        help=INFO["n"]["help"])
    parser.add_argument("-c", "--projet-filename",
                        action="store",
                        dest="projet_filename",
                        required=True,
                        help=INFO["c"]["help"])
    parser.add_argument("-S", "--selected-events",
                        action="store",
                        nargs='+',
                        dest="events",
                        help=INFO["S"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    parser.add_argument("-w", "--warning",
                        action="store_false",
                        dest="warning",
                        default=True,
                        help=INFO["w"]["help"])
    # Identifiants : unique ou par liste
    code_group = parser.add_mutually_exclusive_group()
    code_group.add_argument("-s", "--station-name",
                            action="store",
                            dest="station_name",
                            help=INFO["s"]["help"])
    code_group.add_argument("-l", "--station-list",
                            action="store",
                            dest="stations_list_file",
                            help=INFO["l"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
