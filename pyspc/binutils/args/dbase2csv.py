#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <dbase2csv.py>
"""

# modules Python
import sys
import argparse

# Module PySPC
from pyspc.binutils.print_args import print_args
from pyspc.binutils.csv import DATATYPES as FILETYPES
from pyspc.data.sacha import Sacha
from pyspc.data.prevision import Prevision14, Prevision17, Prevision19


DESCRIPTION = "Extraire les données de base de données MDB/SQLite "\
    "et conversion au format de type csv (grp16, grp18, grp20, pyspc)"
INFO = {
    "1": {
        "short": "Exporter en un seul fichier",
        "help": "Exporter en un seul fichier, valable uniquement si "
                "le format csv est de type 'pyspc'"
    },
    "C": {
        "short": "Format de fichier csv",
        "help": "Format de fichier csv"
    },
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier d'entrée de la base de donnée"
    },
    "d": {
        "short": "Base de données",
        "help": "Nom du fichier de la base de données au format .mdb/.sqlite"
    },
    "t": {
        "short": "Type des données",
        "help": "Type de la base de données .mdb/.sqlite"
    },
    "s": {
        "short": "Station à extraire",
        "help": "Identité de la station"
    },
    "l": {
        "short": "Liste de stations",
        "help": "Fichier contenant la liste des stations Hydro. "
                "Non pris en compte si l'option -s est utilisée"
    },
    "n": {
        "short": "Nom de la variable",
        "help": "Nom de la variable à extraire. Ignoré avec previ17 et previ19"
    },
    "F": {
        "short": "Date de début",
        "help": "Premier pas de temps de la série de données "
                "(aaaammjj[HH])"
    },
    "L": {
        "short": "Date de fin",
        "help": "Dernier pas de temps de la série de données "
                "(aaaammjj[HH])"
    },
    "o": {
        "short": "Compléter/corriger les données existantes ?",
        "help": "[optionnel] Ecraser les données communes existantes"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier de sortie des fichiers csv de GRP Calage"
    },
    "S": {
        "short": "Source des données pluvio",
        "help": "Source des données pluvio (radar, gauge)"
    },
    "2": {
        "short": "Référentiel Hydro2",
        "help": "Référentiel Hydro2 (defaut=False)"
    },
    "3": {
        "short": "Référentiel Hydro3",
        "help": "Référentiel Hydro3 (defaut=False)"
    },
    "v": {
        "short": "Mode bavard",
        "help": "[optionnel] Mode verbeux (defaut=False)"
    },
    "w": {
        "short": "Mode Avertissement",
        "help": "[optionnel] Mode Avertissement (defaut=True). "
                "Si False, ne lève pas les erreurs lors de la "
                "création et lors de la manipulation des séries "
                "de données"
    }
}
LIST_VARNAMES = Sacha.get_varnames()
LIST_DATATYPES = []
LIST_DATATYPES.extend(Sacha.get_datatypes())
LIST_DATATYPES.extend(Prevision14.get_datatypes())
LIST_DATATYPES.extend(Prevision17.get_datatypes())
LIST_DATATYPES.extend(Prevision19.get_datatypes())
LIST_PRCP_SRC = Sacha.get_prcp_src()
LIST_FILETYPES = FILETYPES


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def dbase2csv():
    """
    Validation des options et arguments de <dbase2csv.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <dbase2csv.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Création de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    # Référentiel des lieux : HYDRO2 (-2) ou HYDRO3 (-3)
    hydro_group = parser.add_mutually_exclusive_group(required=True)
    hydro_group.add_argument("-2", "--hydro-2",
                             action="store_true",
                             dest="hydro2",
                             default=False,
                             help=INFO["2"]["help"])
    hydro_group.add_argument("-3", "--hydro-3",
                             action="store_true",
                             dest="hydro3",
                             default=False,
                             help=INFO["3"]["help"])
    # Autres arguments
    parser.add_argument("-C", "--csv-type",
                        action="store",
                        dest="csv_type",
                        default=LIST_FILETYPES[-1],
                        choices=LIST_FILETYPES,
                        help=INFO["C"]["help"])
    parser.add_argument("-1", "--onefile-export",
                        action="store_true",
                        dest="onefile",
                        default=False,
                        help=INFO["1"]["help"])
    parser.add_argument("-d", "--data-filename",
                        action="store",
                        dest="dbase_filename",
                        required=True,
                        help=INFO["d"]["help"])
    parser.add_argument("-n", "--varname",
                        action="store",
                        dest="varname",
                        choices=LIST_VARNAMES,
                        help=INFO["n"]["help"])
    parser.add_argument("-o", "--overwrite",
                        action="store_true",
                        dest="overwrite",
                        default=False,
                        help=INFO["o"]["help"])
    parser.add_argument("-t", "--data-type",
                        action="store",
                        dest="data_type",
                        choices=LIST_DATATYPES,
                        help=INFO["t"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-S", "--src-prcp",
                        action="store",
                        dest="prcp_src",
                        choices=LIST_PRCP_SRC,
                        help=INFO["S"]["help"])
    parser.add_argument("-F", "--First-datetime",
                        action="store",
                        dest="first_dtime",
                        required=True,
                        help=INFO["F"]["help"])
    parser.add_argument("-L", "--Last-datetime",
                        action="store",
                        dest="last_dtime",
                        required=True,
                        help=INFO["L"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    parser.add_argument("-w", "--warning",
                        action="store_false",
                        dest="warning",
                        default=True,
                        help=INFO["w"]["help"])
    # Identifiants : unique ou par liste
    code_group = parser.add_mutually_exclusive_group(required=True)
    code_group.add_argument("-s", "--station-name",
                            action="store",
                            dest="station_name",
                            help=INFO["s"]["help"])
    code_group.add_argument("-l", "--station-list",
                            action="store",
                            dest="stations_list_file",
                            help=INFO["l"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
