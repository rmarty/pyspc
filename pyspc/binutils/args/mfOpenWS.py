#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <mfOpenWS.py>
"""

# modules Python
import sys
import argparse

# Module PySPC
from pyspc.binutils.print_args import print_args
from pyspc.convention.meteofrance import OPEN_DATATYPES
from pyspc.core.convention import EXTERNAL_VARNAMES
from pyspc import Parameter

DESCRIPTION = "Télécharger les données de Météo-France par webservice"
INFO = {
    "s": {
        "short": "Entité (site, station) à extraire",
        "help": "Identité de l'entité (site, station) à extraire"
    },
    "l": {
        "short": "Liste des entités",
        "help": "Fichier contenant la liste des entités. Non pris en compte "
                "si l'option -s est utilisée"
    },
    "n": {
        "short": "Nom de la variable",
        "help": "Nom de la variable à extraire"
    },
    "F": {
        "short": "Date de début",
        "help": "Premier pas de temps de la série de données "
                "(aaaammjj[HH[MM]])"
    },
    "L": {
        "short": "Date de fin",
        "help": "Dernier pas de temps de la série de données "
                "(aaaammjj[HH[MM]])"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier de sortie"
    },
    "c": {
        "short": "Fichier de config MF Open API",
        "help": "[optionnel] Nom du fichier de configuration des paramètres"
                "de session Open API"
    },
    "t": {
        "short": "Type de donnée",
        "help": "Type de donnée"
    },
    "v": {
        "short": "Mode verbeux",
        "help": "[optionnel] Mode verbeux (defaut=False)"
    }
}

LIST_DATATYPES = OPEN_DATATYPES
LIST_VARNAMES = sorted(list({Parameter(varname=v).spc_varname
                             for k, v in EXTERNAL_VARNAMES.items()
                             if k[0] in LIST_DATATYPES}))


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def mfOpenWS():
    """
    Validation des options et arguments de <mfOpenWS.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <plotGrpData.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Création de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-c", "--config-filename",
                        action="store",
                        dest="cfg_filename",
                        help=INFO["c"]["help"])
    parser.add_argument("-n", "--varname",
                        action="store",
                        dest="varname",
                        choices=LIST_VARNAMES,
                        help=INFO["n"]["help"])
    parser.add_argument("-t", "--data-type",
                        action="store",
                        dest="datatype",
                        choices=LIST_DATATYPES,
                        required=True,
                        help=INFO["t"]["help"])
    parser.add_argument("-F", "--First-datetime",
                        action="store",
                        dest="first_dtime",
                        help=INFO["F"]["help"])
    parser.add_argument("-L", "--Last-datetime",
                        action="store",
                        dest="last_dtime",
                        help=INFO["L"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        help=INFO["O"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    # Identifiants : unique ou par liste
    code_group = parser.add_mutually_exclusive_group(required=True)
    code_group.add_argument("-s", "--station-name",
                            action="store",
                            dest="station_name",
                            help=INFO["s"]["help"])
    code_group.add_argument("-l", "--station-list",
                            action="store",
                            dest="stations_list_file",
                            help=INFO["l"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
