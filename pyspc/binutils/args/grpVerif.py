#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <grpVerif.py>
"""

# modules Python
import sys
import argparse

# Module PySPC
from pyspc.binutils.print_args import print_args
from pyspc.model.grp16 import GRP_Verif as GRP16_Verif
from pyspc.model.grp18 import GRP_Verif as GRP18_Verif
from pyspc.model.grp20 import GRP_Verif as GRP20_Verif

DESCRIPTION = "Récupérer les performances des modèles GRP "\
    "(Fiches de Performance)"
INFO = {
    "c": {
        "short": "Fichier de sortie",
        "help": "[optionnel] Fichier de sortie. Si non défini, renvoi vers "
                "la sortie standard"
    },
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier d'entrée des Fiches de Performance"
    },
    "l": {
        "short": "Liste de stations",
        "help": "[optionnel] Fichier contenant la liste des stations "
                "Hydro. Non pris en compte si l'option -s est utilisée"
    },
    "s": {
        "short": "Station à considérer",
        "help": "[optionnel] Identité de la station"
    },
    "t": {
        "short": "Type de fiche de performance",
        "help": "Type de fiche de performance (cal, rtime)"
    },
    "v": {
        "short": "Mode verbeux",
        "help": "[optionnel] Mode verbeux"
    },
}
LIST_DATATYPES = []
LIST_DATATYPES.extend(['grp16_{}'.format(d)
                       for d in GRP16_Verif.get_datatypes()])
LIST_DATATYPES.extend(['grp18_{}'.format(d)
                       for d in GRP18_Verif.get_datatypes()])
LIST_DATATYPES.extend(['grp20_{}'.format(d)
                       for d in GRP20_Verif.get_datatypes()])


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def grpVerif():
    """
    Validation des options et arguments de <grpVerif.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <grpVerif.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-c", "--output-filename",
                        action="store",
                        dest="output_filename",
                        help=INFO["c"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-t", "--data-type",
                        action="store",
                        dest="data_type",
                        required=True,
                        choices=LIST_DATATYPES,
                        help=INFO["t"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    # Identifiants : unique ou par liste
    code_group = parser.add_mutually_exclusive_group(required=True)
    code_group.add_argument("-s", "--station-name",
                            action="store",
                            dest="station_name",
                            help=INFO["s"]["help"])
    code_group.add_argument("-l", "--station-list",
                            action="store",
                            dest="stations_list_file",
                            help=INFO["l"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
