#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <mf2csv.py>
"""

# Modules Python
import sys
import argparse

# Module PySPC
from pyspc.binutils.print_args import print_args
from pyspc.binutils.csv import DATATYPES as FILETYPES
from pyspc.convention.meteofrance import DATATYPES, OPEN_DATATYPES

DESCRIPTION = "Convertir les données MétéoFrance "\
    "au format de type csv (grp16, grp18, grp20, pyspc)"
INFO = {
    "1": {
        "short": "Exporter en un seul fichier",
        "help": "Exporter en un seul fichier, valable uniquement si "
                "le format csv est de type 'pyspc'"
    },
    "C": {
        "short": "Format de fichier csv",
        "help": "Format de fichier csv"
    },
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier des fichiers MF"
    },
    "d": {
        "short": "Nom ou motif du fichier MF",
        "help": "Nom ou motif du fichier de données Meteo France"
                " au format .data"
    },
    "t": {
        "short": "Type des données MF",
        "help": "Type des données MF"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier des fichiers GRP"
    },
    "f": {
        "short": "[optionnel] Données continues ",
        "help": "[optionnel] Ajouter les pas de temps manquants avec "
                "'missing_value'"
    },
    "m": {
        "short": "[optionnel] Valeur manquante",
        "help": "[optionnel] Valeurs des données manquantes "
                "(defaut=numpy.nan)"
    },
    "o": {
        "short": "[optionnel] Compléter/corriger les données existantes ?",
        "help": "[optionnel] Compléter/corriger les données existantes ?"
                "(defaut=False)"
    },
    "s": {
        "short": "[optionnel] Station à extraire",
        "help": "[optionnel] Identité de la station"
    },
    "l": {
        "short": "[optionnel] Liste de zones",
        "help": "[optionnel] Fichier contenant la liste des zones"
                "Ignoré si l'option -s est utilisée"
    },
    "v": {
        "short": "[optionnel] Mode verbeux",
        "help": "[optionnel] Mode verbeux (defaut=False)"
    },
    "w": {
        "short": "Mode Avertissement",
        "help": "[optionnel] Mode Avertissement (defaut=True). "
                "Si False, ne lève pas les erreurs lors de la "
                "création et lors de la manipulation des séries "
                "de données"
    }
}
LIST_DATATYPES = []
LIST_DATATYPES.extend(DATATYPES)
LIST_DATATYPES.extend(OPEN_DATATYPES)
LIST_FILETYPES = FILETYPES


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def mf2csv():
    """
    Validation des options et arguments de <mf2csv.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <mf2csv.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-C", "--csv-type",
                        action="store",
                        dest="csv_type",
                        default=LIST_FILETYPES[-1],
                        choices=LIST_FILETYPES,
                        help=INFO["C"]["help"])
    parser.add_argument("-1", "--onefile-export",
                        action="store_true",
                        dest="onefile",
                        default=False,
                        help=INFO["1"]["help"])
    parser.add_argument("-d", "--data-filename",
                        action="store",
                        dest="mfdata_filename",
                        required=True,
                        help=INFO["d"]["help"])
    parser.add_argument("-o", "--overwrite",
                        action="store_true",
                        dest="overwrite",
                        default=False,
                        help=INFO["o"]["help"])
    parser.add_argument("-t", "--data-type",
                        action="store",
                        dest="data_type",
                        choices=LIST_DATATYPES,
                        help=INFO["t"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    parser.add_argument("-w", "--warning",
                        action="store_false",
                        dest="warning",
                        default=True,
                        help=INFO["w"]["help"])
    # Identifiants : unique ou par liste
    code_group = parser.add_mutually_exclusive_group()
    code_group.add_argument("-s", "--station-name",
                            action="store",
                            dest="station_name",
                            help=INFO["s"]["help"])
    code_group.add_argument("-l", "--station-list",
                            action="store",
                            dest="stations_list_file",
                            help=INFO["l"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
