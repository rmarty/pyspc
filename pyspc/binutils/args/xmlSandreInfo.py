#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <xmlSandreInfo.py>
"""

# modules Python
import sys
import argparse

from pyspc.binutils.print_args import print_args
from pyspc.metadata.sandre import Sandre

DESCRIPTION = "Informations sur les lieux et tronçons de vigilance"
INFO = {
    "c": {
        "short": "Fichier de sortie",
        "help": "[optionnel] Fichier de sortie. Si non défini, renvoi vers "
                "la sortie standard"
    },
    "d": {
        "short": "Nom ou motif du fichier XML-Sandre",
        "help": "Nom ou motif du fichier XML-Sandre produit par la PHyC"
    },
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier d'entrée des données GRP"
    },
    "t": {
        "short": "Type de données XML",
        "help": "Type de données XML Sandre"
    },
    "v": {
        "short": "Mode bavard",
        "help": "[optionnel] Mode verbeux (defaut=False)"
    }
}
LIST_DATATYPES = [t for t in Sandre.get_types()
                  if t.startswith('loc') or t == 'user']


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def xmlSandreInfo():
    """
    Validation des options et arguments de <xmlSandreInfo.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <xmlSandreInfo.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Création de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-c", "--output-filename",
                        action="store",
                        dest="output_filename",
                        help=INFO["c"]["help"])
    parser.add_argument("-d", "--data-filename",
                        action="store",
                        dest="xml_filename",
                        required=True,
                        help=INFO["d"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-t", "--data-type",
                        action="store",
                        dest="data_type",
                        choices=LIST_DATATYPES,
                        required=True,
                        help=INFO["t"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
