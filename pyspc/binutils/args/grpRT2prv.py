#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <grpRT2prv.py>
"""

# modules Python
import sys
import argparse

# Module PySPC
from pyspc.binutils.print_args import print_args
from pyspc.convention.grp16 import DATATYPES as GRP16_DATATYPES
from pyspc.convention.grp18 import DATATYPES as GRP18_DATATYPES
from pyspc.convention.grp20 import DATATYPES as GRP20_DATATYPES
from pyspc.convention.prv import DATATYPES as PRV_DATATYPES
import pyspc.core.exception as _exception

DESCRIPTION = "Convertir les prévisions de GRP *Temps Réel* "\
    "au format prv Scores / prv OTAMIN"
INFO = {
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier d'entrée"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier de sortie"
    },
    "t": {
        "short": "Type de fichier grp TR et type de format prv",
        "help": "Type de fichier grp TR (*rt_fcst, *rt_fcst_diff) "
                "et type de format prv (scores_fcst, otamin*_fcst)"
    },
    "U": {
        "short": "Codes des modèles au sens POM",
        "help": "Codes des modèles au sens POM: Previ CODEPREVI, Simu CODESIMU"
    },
    "v": {
        "short": "Mode verbeux",
        "help": "[optionnel] Mode verbeux"
    },
    "w": {
        "short": "Mode Avertissement",
        "help": "[optionnel] Mode Avertissement (defaut=True). "
                "Si False, ne lève pas les erreurs lors de la "
                "création et lors de la manipulation des séries "
                "de données"
    }
}
grp_dtypes = []
grp_dtypes.extend(GRP16_DATATYPES)
grp_dtypes.extend(GRP18_DATATYPES)
grp_dtypes.extend(GRP20_DATATYPES)
LIST_DATATYPES = [(f, p) for f in grp_dtypes for p in PRV_DATATYPES
                  if p.endswith('fcst') and 'rt_fcst' in f]
LIST_UTYPES = ['Previ', 'Simu']


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def grpRT2prv():
    """
    Validation des options et arguments de <grpRT2prv.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    _exception.raise_valueerror(
        (args.data_type[0], args.data_type[1]) not in LIST_DATATYPES,
        "Type de fichier grp TR inconnu ou format prv inconnu (-t)"
    )
    if args.codepom is not None:
        for x in args.codepom:
            _exception.raise_valueerror(
                 x[0] not in LIST_UTYPES,
                 "Type de sortie incorrecte pour la codification POM (-U)"
            )
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <grpRT2prv.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-t", "--data-type",
                        action="store",
                        dest="data_type",
                        metavar=('type_GRP', 'type_PRV'),
                        nargs=2,
                        required=True,
                        help=INFO["t"]["help"])
    parser.add_argument("-U", "--user-codepom",
                        action="append",
                        dest="codepom",
                        metavar=('Type', 'Value'),
                        nargs=2,
                        help=INFO["U"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    parser.add_argument("-w", "--warning",
                        action="store_false",
                        dest="warning",
                        default=True,
                        help=INFO["w"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
