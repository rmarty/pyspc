#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <onlineReport.py>
"""

# modules Python
import argparse
from datetime import datetime as dt
import sys

# Module PySPC
from pyspc.binutils.print_args import print_args
import pyspc.core.exception as _exception
from pyspc.convention.vigicrues import VARNAMES
from pyspc.webservice.report import OnlineReport


DESCRIPTION = "Télécharger les rapports et bulletins de MF, Inrae et Vigicrues"
INFO = {
    "s": {
        "short": "Identifiant",
        "help": "[optionnel] Identifiant (Dép., Région, Station, ...)"
    },
    "l": {
        "short": "Liste des Identifiants",
        "help": "[optionnel] Fichier contenant la liste des identifiants. "
                "Non pris en compte si l'option -s est utilisée"
    },
    "r": {
        "short": "Date du rapport",
        "help": "Date du rapport (aaaammjj)"
    },
    "n": {
        "short": "Nom de la grandeur Vigicrues",
        "help": "Nom de la grandeur Vigicrues à extraire"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier de sortie"
    },
    "v": {
        "short": "Mode verbeux",
        "help": "[optionnel] Mode verbeux"
    },
    "t": {
        "short": "Type de document",
        "help": "Type de document "
                "(climatologie mensuelle, journalière, fiche station)"
    },
    "w": {
        "short": "Mode Avertissement",
        "help": "[optionnel] Mode Avertissement (defaut=True). "
                "Si False, ne lève pas les erreurs lors de la "
                "création et lors de la manipulation des séries "
                "de données"
    },
}
LIST_DATATYPES = OnlineReport.get_reporttypes()
LIST_VARNAMES = sorted(VARNAMES)


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def onlineReport():
    """
    Validation des options et arguments de <onlineReport.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    if args.runtime is not None and len(args.runtime) != 8:
        raise _exception.Error("Format de date inconnu (-r)")
    if args.data_type == "mf_dailyreport":
        if args.station_name is not None or \
                args.stations_list_file is not None:
            _exception.Warning(
                __name__,
                "les options (-s) et (-l) ne sont pas utilisées "
                "lorsque que le rapport demandé est le bulletin journalier")
        args.station_name = "FR"
        args.stations_list_file = None
    elif args.data_type == "mf_station" and args.runtime is not None:
        args.runtime = None
        _exception.Warning(
            __name__,
            "l'option (-r) n'est pas utilisée "
            "lorsque que le rapport demandé est la fiche station")
    elif args.data_type.startswith('vigicrues-1'):
        if args.runtime is not None:
            _exception.Warning(
                __name__,
                "l'option (-r) n'est pas utilisée lorsque que le rapport "
                "demandé est issue du service 1.1 de Vigicrues")
        args.runtime = dt.utcnow().strftime('%Y%m%d%H%M')
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <onlineReport.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-r", "--run-time",
                        action="store",
                        dest="runtime",
                        help=INFO["r"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-n", "--varname",
                        action="store",
                        dest="varname",
                        choices=LIST_VARNAMES,
                        help=INFO["n"]["help"])
    parser.add_argument("-t", "--data-type",
                        action="store",
                        dest="data_type",
                        choices=LIST_DATATYPES,
                        required=True,
                        help=INFO["t"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    parser.add_argument("-w", "--warning",
                        action="store_false",
                        dest="warning",
                        default=True,
                        help=INFO["w"]["help"])
    # Identifiants : unique ou par liste
    code_group = parser.add_mutually_exclusive_group()
    code_group.add_argument("-s", "--station-name",
                            action="store",
                            dest="station_name",
                            help=INFO["s"]["help"])
    code_group.add_argument("-l", "--station-list",
                            action="store",
                            dest="stations_list_file",
                            help=INFO["l"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
