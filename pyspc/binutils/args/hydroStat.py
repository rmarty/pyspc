#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <hydroStat.py>
"""

# modules Python
import sys
import argparse

# Module PySPC
from pyspc.binutils.print_args import print_args

DESCRIPTION = "Lire les statistiques exportées depuis HYDRO-2"
INFO = {
    "c": {
        "short": "Fichier de sortie",
        "help": "[optionnel] Fichier de sortie. Si non défini, renvoi vers "
                "la sortie standard"
    },
    "d": {
        "short": "Nom ou motif du fichier",
        "help": "Nom ou motif du fichier d'export statistique HYDRO-2"
    },
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier d'entrée des fichiers d'export statistique HYDRO-2"
    },
    "v": {
        "short": "Mode bavard",
        "help": "[optionnel] Mode verbeux (defaut=False)"
    }
}


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def hydroStat():
    """
    Validation des options et arguments de <hydroStat.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <hydroStat.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Création de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-c", "--output-filename",
                        action="store",
                        dest="output_filename",
                        help=INFO["c"]["help"])
    parser.add_argument("-d", "--data-filename",
                        action="store",
                        dest="filename",
                        required=True,
                        help=INFO["d"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
