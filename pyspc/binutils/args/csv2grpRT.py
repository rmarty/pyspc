#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <csv2grpRT.py>
"""

# modules Python
import sys
import argparse

# Module PySPC
from pyspc.binutils.print_args import print_args
from pyspc.binutils.csv import DATATYPES as FILETYPES
from pyspc.convention.grp16 import (
    DATATYPES as DTYPES_grp16, RT_DATA_VARNAMES as VARNAMES_grp16)
from pyspc.convention.grp18 import (
    DATATYPES as DTYPES_grp18, RT_DATA_VARNAMES as VARNAMES_grp18)
from pyspc.convention.grp20 import (
    DATATYPES as DTYPES_grp20, RT_DATA_VARNAMES as VARNAMES_grp20)
from pyspc.convention.grp22 import (
    DATATYPES as DTYPES_grp22, RT_DATA_VARNAMES as VARNAMES_grp22)
from pyspc.core.convention import EXTERNAL_VARNAMES

DESCRIPTION = "Convertir les données obs/prev du format csv "\
    "(GRP16, GRP18, pyspc) à GRP Temps-Réel"
INFO = {
    "C": {
        "short": "Format de fichier csv",
        "help": "Format de fichier csv"
    },
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier d'entrée"
    },
    "s": {
        "short": "Station à extraire",
        "help": "[optionnel] Identité de la station Hydro"
    },
    "l": {
        "short": "Liste de stations",
        "help": "[optionnel] Fichier contenant la liste des stations "
                "Hydro. Non pris en compte si l'option -s est utilisée"
    },
    "n": {
        "short": "Nom de la variable",
        "help": "Nom de la variable à extraire. L'utilisateur peut "
                "fournir une liste de noms, chacun d'entre eux étant"
                " séparé par un espace"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier de sortie"
    },
    "t": {
        "short": "Type de fichier grp TR",
        "help": "Type de fichier grp TR (data, scen_meteo)"
    },
    "v": {
        "short": "Mode verbeux",
        "help": "[optionnel] Mode verbeux"
    },
    "w": {
        "short": "Mode Avertissement",
        "help": "[optionnel] Mode Avertissement (defaut=True). "
                "Si False, ne lève pas les erreurs lors de la "
                "création et lors de la manipulation des séries "
                "de données"
    }
}
LIST_VARNAMES = {v
                 for k, v in EXTERNAL_VARNAMES.items()
                 if (k[0] == 'GRP16' and k[1] in VARNAMES_grp16)
                 or (k[0] == 'GRP18' and k[1][0] in VARNAMES_grp18)
                 or (k[0] == 'GRP20' and k[1][0] in VARNAMES_grp20)
                 or (k[0] == 'GRP22' and k[1][0] in VARNAMES_grp22)}
LIST_VARNAMES = sorted(list(LIST_VARNAMES))
LIST_DATATYPES = []
LIST_DATATYPES.extend([t for t in DTYPES_grp16
                       if t.startswith('grp16_rt') and t.endswith('data')])
LIST_DATATYPES.extend([t for t in DTYPES_grp18
                       if t.startswith('grp18_rt') and (
                           t.endswith('data') or t.endswith('metscen'))])
LIST_DATATYPES.extend([t for t in DTYPES_grp20
                       if t.startswith('grp20_rt') and (
                           t.endswith('data') or t.endswith('metscen')
                           or t.endswith('archive'))])
LIST_DATATYPES.extend([t for t in DTYPES_grp22
                       if t.startswith('grp22_rt') and (
                           t.endswith('data') or t.endswith('metscen')
                           or t.endswith('archive'))])
LIST_FILETYPES = FILETYPES


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def csv2grpRT():
    """
    Validation des options et arguments de <csv2grpRT.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <csv2grpRT.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-C", "--csv-type",
                        action="store",
                        dest="csv_type",
                        default=LIST_FILETYPES[-1],
                        choices=LIST_FILETYPES,
                        help=INFO["C"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-n", "--varname",
                        action="store",
                        dest="varname",
                        required=True,
                        nargs='+',
                        choices=LIST_VARNAMES,
                        help=INFO["n"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-t", "--data-type",
                        action="store",
                        dest="data_type",
                        required=True,
                        choices=LIST_DATATYPES,
                        help=INFO["t"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    parser.add_argument("-w", "--warning",
                        action="store_false",
                        dest="warning",
                        default=True,
                        help=INFO["w"]["help"])
    # Identifiants : unique ou par liste
    code_group = parser.add_mutually_exclusive_group(required=True)
    code_group.add_argument("-s", "--station-name",
                            action="store",
                            dest="station_name",
                            help=INFO["s"]["help"])
    code_group.add_argument("-l", "--station-list",
                            action="store",
                            dest="stations_list_file",
                            help=INFO["l"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
