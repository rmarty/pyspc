#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <duplicatePlathynesEvent.py>
"""

# modules Python
import sys
import argparse

# Module PySPC
from pyspc.binutils.print_args import print_args


DESCRIPTION = "Dupliquer un événement au sein d'un même projet PLATHYNES"
INFO = {
    "I": {
        "short": "Répertoire du fichier projet PLATHYNES",
        "help": "Répertoire du fichier projet PLATHYNES"
    },
    "d": {
        "short": "Nom du fichier du projet PLATHYNES",
        "help": "Nom du fichier du projet PLATHYNES"
    },
    "O": {
        "short": "Répertoire du nouveau projet PLATHYNES",
        "help": "Répertoire du nouveau projet PLATHYNES"
    },
    "c": {
        "short": "Nom du fichier des événements",
        "help": "Nom du fichier des événements",
    },
    "S": {
        "short": "Evénement à dupliquer",
        "help": "[optionnel] Evénement à dupliquer, "
                "si le projet en contient plusieurs"
    },
    "o": {
        "short": "[optionnel] Compléter la liste des événements ?",
        "help": "[optionnel] Compléter la liste des événements ?"
                "(defaut=False)"
    },
    "v": {
        "short": "Mode bavard",
        "help": "[optionnel] Mode verbeux (defaut=False)"
    }
}


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def duplicatePlathynesEvent():
    """
    Validation des options et arguments de <duplicatePlathynesEvent.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Définition des options et arguments de <duplicatePlathynesEvent.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Création de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    # Autres arguments
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-d", "--data-filename",
                        action="store",
                        dest="projet_filename",
                        required=True,
                        help=INFO["d"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-c", "--event-filename",
                        action="store",
                        dest="events_filename",
                        required=True,
                        help=INFO["c"]["help"])
    parser.add_argument("-S", "--selected-event",
                        action="store",
                        dest="event",
                        help=INFO["S"]["help"])
    parser.add_argument("-o", "--overwrite",
                        action="store_true",
                        dest="overwrite",
                        default=False,
                        help=INFO["o"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
