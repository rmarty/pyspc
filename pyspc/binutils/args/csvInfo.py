#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <csvInfo.py>
"""

# modules Python
import collections
import sys
import argparse

# Module PySPC
from pyspc.binutils.print_args import print_args
from pyspc.binutils.csv import DATATYPES as FILETYPES
from pyspc.core.convention import SPC_VARNAMES

METHODS = collections.OrderedDict()
METHODS['diff'] = {
    'longname': None,
    'short': 'Différence entre 2 séries',
    'help': 'Différences entre la(es) série(s) et la référence correspondant '
            'à la série de la Station dans le répertoire Dossier',
    'param': ['Station', 'Dossier'],
    'longparam': ['Station', 'Dossier']
}
METHODS['evt'] = {
    'longname': None,
    'short': "Extraction d'événements",
    'help': "Extraction d'événement",
    'param': ['Seuil', 'Algorithme', 'Ecart', 'Durée', 'Avant', 'Après'],
    'longparam': ['Seuil', 'Algorithme', 'Ecart', 'Durée', 'Avant', 'Après']
}
METHODS['max'] = {
    'longname': None,
    'short': 'Maximas annuels',
    'help': 'Maximas annuels',
    'param': None,
    'longparam': None
}
METHODS['mvl'] = {
    'longname': 'missing_value',
    'short': 'Complétude de la série',
    'help': 'Complétude de la série',
    'param': None,
    'longparam': None
}
METHODS['mvs'] = {
    'longname': 'missing_value_strip',
    'short': 'Complétude de la série',
    'help': 'Complétude de la série, en retirant les données manquantes '
            'en début et fin de série',
    'param': None,
    'longparam': None
}
METHODS['reg'] = {
    'longname': 'regime',
    'short': 'Régime hydrologique',
    'help': 'Régime hydrologique',
    'param': ['Groupe', 'Freqs', 'Dayhour', 'Strict', 'Ignore_echelle',
              'Répertoire', 'Boxplot', 'Remplir'],
    'longparam': ['Groupe (str)', "Freqences (sep: ',')", 'Dayhour (int)',
                  'Strict (bool)', 'Ignore_echelle (bool)',
                  'Répertoire (str)', 'Boxplot (bool)',
                  'Remplir figure (bool)']
}
METHODS_STR = ''
for arg in METHODS:
    if METHODS[arg]['longname'] is not None:
        METHODS_STR += "\n+ {}/{} : ".format(arg, METHODS[arg]['longname'])
    else:
        METHODS_STR += "\n+ {} : ".format(arg)
    METHODS_STR += METHODS[arg]['help']
    if METHODS[arg]['param'] is not None:
        METHODS_STR += " (param: {})"\
                       "".format(" ".join(METHODS[arg]['param']))
METHODS_STR += "\n"

DESCRIPTION = "Extraire des informations de séries au format de type csv"\
    "(grp16, grp18, grp20, pyspc), selon une méthode choisie"
INFO = {
    "C": {
        "short": "Format de fichier csv",
        "help": "Format de fichier csv"
    },
    "c": {
        "short": "Fichier de sortie",
        "help": "[optionnel] Fichier de sortie. Si non défini, renvoi vers "
                "la sortie standard"
    },
    "F": {
        "short": "Date de début",
        "help": "[optionnel] Premier pas de temps à considérer"
    },
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier d'entrée des données GRP"
    },
    "l": {
        "short": "Liste de stations",
        "help": "[optionnel] Fichier contenant la liste des stations "
                "Hydro. Non pris en compte si l'option -s est utilisée"
    },
    "L": {
        "short": "Date de fin",
        "help": "[optionnel] Dernier pas de temps à considérer"
    },
    "M": {
        "short": "Méthode de traitement",
        "help": "Informations à extraire des données : " + METHODS_STR
    },
    "n": {
        "short": "Nom de la variable",
        "help": "Nom de la variable à extraire"
    },
    "s": {
        "short": "Station à considérer",
        "help": "[optionnel] Identité de la station"
    },
    "v": {
        "short": "Mode verbeux",
        "help": "[optionnel] Mode verbeux"
    },
    "w": {
        "short": "Mode Avertissement",
        "help": "[optionnel] Mode Avertissement (defaut=True). "
                "Si False, ne lève pas les erreurs lors de la "
                "création et lors de la manipulation des séries "
                "de données"
    }
}
LIST_VARNAMES = sorted(SPC_VARNAMES.keys())
LIST_FILETYPES = FILETYPES


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def csvInfo():
    """
    Validation des options et arguments de <csvInfo.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <csvInfo.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-C", "--csv-type",
                        action="store",
                        dest="csv_type",
                        default=LIST_FILETYPES[-1],
                        choices=LIST_FILETYPES,
                        help=INFO["C"]["help"])
    parser.add_argument("-c", "--output-filename",
                        action="store",
                        dest="output_filename",
                        help=INFO["c"]["help"])
    parser.add_argument("-F", "--First-datetime",
                        action="store",
                        dest="first_dtime",
                        # required=True,
                        help=INFO["F"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        # required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-L", "--Last-datetime",
                        action="store",
                        dest="last_dtime",
                        # required=True,
                        help=INFO["L"]["help"])
    parser.add_argument("-M", "--Method",
                        action="store",
                        dest="processing_method",
                        # required=True,
                        nargs='+',
                        metavar=('Method', 'Submethod(s), ...'),
                        help=INFO["M"]["help"])
    parser.add_argument("-n", "--varname",
                        action="store",
                        dest="varname",
                        # required=True,
                        choices=LIST_VARNAMES,
                        help=INFO["n"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    parser.add_argument("-w", "--warning",
                        action="store_false",
                        dest="warning",
                        default=True,
                        help=INFO["w"]["help"])
    # Identifiants : unique ou par liste
    code_group = parser.add_mutually_exclusive_group(required=True)
    code_group.add_argument("-s", "--station-name",
                            action="store",
                            dest="station_name",
                            help=INFO["s"]["help"])
    code_group.add_argument("-l", "--station-list",
                            action="store",
                            dest="stations_list_file",
                            help=INFO["l"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
