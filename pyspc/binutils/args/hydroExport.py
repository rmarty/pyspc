#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <hydroExport.py>
"""

# Modules Python
import sys
import argparse

# Module PySPC
from pyspc.binutils.print_args import print_args
from pyspc.convention.hydro2 import DATATYPES

DESCRIPTION = "Créer les procédures d'export Hydro2"
INFO = {
    "F": {
        "short": "Date de début",
        "help": "[optionnel] Premier pas de temps à considérer"
    },
    "L": {
        "short": "Date de fin",
        "help": "[optionnel] Dernier pas de temps à considérer"
    },
    "l": {
        "short": "Liste de stations",
        "help": "[optionnel] Fichier contenant la liste des stations "
                "Hydro. Non pris en compte si l'option -s est utilisée"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier des exports Hydro"
    },
    "s": {
        "short": "Station à considérer",
        "help": "[optionnel] Identité de la station"
    },
    "t": {
        "short": "Type de procédure d'export Hydro",
        "help": "Type de procédure d'export Hydro "
    },
    "U": {
        "short": "Information utilisateur",
        "help": "Information utilisateur. [precision] Précision décimale: "
                "entre 0 et 3 chiffres après la virgule. [onefile] Nom du "
                "fichier contenant la procédure pour toutes les stations"
    },
    "v": {
        "short": "Mode verbeux",
        "help": "[optionnel] Mode verbeux (defaut=False)"
    }
}
LIST_DATATYPES = DATATYPES['export']
LIST_UTYPES = ['precision', 'onefile']


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def hydroExport():
    """
    Validation des options et arguments de <hydroExport.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    if args.first_dtime is not None:
        args.first_dtime = '{0:0<12s}'.format(args.first_dtime)
    if args.last_dtime is not None:
        args.last_dtime = '{0:0<12s}'.format(args.last_dtime)
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <hydroExport.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-F", "--First-datetime",
                        action="store",
                        dest="first_dtime",
                        # required=True,
                        help=INFO["F"]["help"])
    parser.add_argument("-L", "--Last-datetime",
                        action="store",
                        dest="last_dtime",
                        # required=True,
                        help=INFO["L"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-t", "--data-type",
                        action="store",
                        dest="data_type",
                        choices=LIST_DATATYPES,
                        required=True,
                        help=INFO["t"]["help"])
    parser.add_argument("-U", "--user-config",
                        action="append",
                        dest="user",
                        metavar=('Key', 'Value'),
                        nargs=2,
                        help=INFO["U"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    # Identifiants : unique ou par liste
    code_group = parser.add_mutually_exclusive_group(required=True)
    code_group.add_argument("-s", "--station-name",
                            action="store",
                            dest="station_name",
                            help=INFO["s"]["help"])
    code_group.add_argument("-l", "--station-list",
                            action="store",
                            dest="stations_list_file",
                            help=INFO["l"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
