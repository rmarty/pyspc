#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <bdimagexml2csv.py>
"""

# modules Python
import argparse
import sys

# Module PySPC
from pyspc.binutils.print_args import print_args
from pyspc.binutils.csv import DATATYPES as FILETYPES

DESCRIPTION = "Convertir les données BdImage du XML "\
    "au format de type csv (pyspc)"
INFO = {
    "1": {
        "short": "Exporter en un seul fichier",
        "help": "Exporter en un seul fichier, valable uniquement si "
                "le format csv est de type 'pyspc'"
    },
    "C": {
        "short": "Format de fichier csv",
        "help": "Format de fichier csv"
    },
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier des fichiers BdImage"
    },
    "d": {
        "short": "Nom ou motif du fichier BdImage XML",
        "help": "Nom ou motif du fichier de données BdImage au format .xml"
    },
    "U": {
        "short": "Seuil(s) de tolérance des ratios",
        "help": "Type de ratio (image, stats) et son seuil de tolérance."
                "Si une image ou une donnée statistique est obtenue avec "
                "un ratio distinct de 1, elle est tolérée (donc conservée) "
                "si le ratio est supérieur à son seuil de tolérance."
    },
    "o": {
        "short": "Compléter/corriger les données existantes ?",
        "help": "[optionnel] Ecraser les données communes existantes"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier des fichiers GRP"
    },
    "v": {
        "short": "Mode verbeux",
        "help": "[optionnel] Mode verbeux (defaut=False)"
    },
    "w": {
        "short": "Mode Avertissement",
        "help": "[optionnel] Mode Avertissement (defaut=True). "
                "Si False, ne lève pas les erreurs lors de la "
                "création et lors de la manipulation des séries "
                "de données"
    }
}
LIST_UTYPES = ['ratio_image', 'ratio_stats']
LIST_FILETYPES = [f for f in FILETYPES if not f.startswith('grp')]


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def bdimagexml2csv():
    """
    Validation des options et arguments de <bdimagexml2csv.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    default_ratios = {r: 1 for r in LIST_UTYPES}
    if args.ratio_level is not None:
        for level in args.ratio_level:
            default_ratios[level[0]] = float(level[1])
    args.ratio_level = default_ratios
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <bdimagexml2csv.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-C", "--csv-type",
                        action="store",
                        dest="csv_type",
                        default=LIST_FILETYPES[-1],
                        choices=LIST_FILETYPES,
                        help=INFO["C"]["help"])
    parser.add_argument("-1", "--onefile-export",
                        action="store_true",
                        dest="onefile",
                        default=False,
                        help=INFO["1"]["help"])
    parser.add_argument("-d", "--data-filename",
                        action="store",
                        dest="xml_filename",
                        required=True,
                        help=INFO["d"]["help"])
    parser.add_argument("-o", "--overwrite",
                        action="store_true",
                        dest="overwrite",
                        default=False,
                        help=INFO["o"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-U", "--user-ratio",
                        action="append",
                        dest="ratio_level",
                        metavar=('Ratio name', 'Value'),
                        nargs=2,
                        help=INFO["U"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    parser.add_argument("-w", "--warning",
                        action="store_false",
                        dest="warning",
                        default=True,
                        help=INFO["w"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
