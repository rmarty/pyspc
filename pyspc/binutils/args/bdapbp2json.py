#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <bdapbp2json.py>
"""

# modules Python
import sys
import argparse

# Module PySPC
from pyspc.binutils.print_args import print_args
from pyspc.webservice.lamedo import BdApbp

DESCRIPTION = "Extraire les données de BdApbp (LAMEDO) au format json"
INFO = {
    "S": {
        "short": "Zones",
        "help": "Identifiant des zones ApBp. "
                "Pour demander plusieurs zones, il suffit de les "
                "relier par '+'. Par ex: 41005+41004+41003 pour "
                "avoir les zones BP Source-Loire, Lignon-Dunieres "
                "et Haut-Bassin"
    },
    "c": {
        "short": "Fichier de config",
        "help": "[optionnel] Nom du fichier de configuration "
                "BdApbp et proxies"
    },
    "r": {
        "short": "Date du run APBP",
        "help": "Date du run APBP (aaaammjj[hh[mm]])"
    },
    "t": {
        "short": "Format du JSON",
        "help": "Format du JSON de la BdApbp (short, long)"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier de sortie"
    },
    "v": {
        "short": "Mode verbeux",
        "help": "[optionnel] Mode verbeux (defaut=False)"
    }
}
LIST_DATATYPES = BdApbp.get_types()


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def bdapbp2json():
    """
    Validation des options et arguments de <bdapbp2json.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <bdapbp2json.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        help=INFO["O"]["help"])
    parser.add_argument("-S", "--Spatialdomain-name",
                        action="store",
                        dest="domain_name",
                        help=INFO["S"]["help"])
    parser.add_argument("-r", "--run-time",
                        action="store",
                        dest="runtime",
                        help=INFO["r"]["help"])
    parser.add_argument("-t", "--data-type",
                        action="store",
                        dest="data_type",
                        choices=LIST_DATATYPES,
                        help=INFO["t"]["help"])
    parser.add_argument("-c", "--config-filename",
                        action="store",
                        dest="cfg_filename",
                        help=INFO["c"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
