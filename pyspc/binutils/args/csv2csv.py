#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <csv2csv.py>
"""

# modules Python
import collections
import sys
import argparse

# Module PySPC
from pyspc.binutils.print_args import print_args
from pyspc.binutils.csv import DATATYPES as FILETYPES
import pyspc.core.exception as _exception
from pyspc.core.convention import SPC_VARNAMES


METHODS = collections.OrderedDict()
METHODS['arp'] = {
    'longname': 'areaPrcp',
    'short': "Calculer la lame d'eau",
    'help': "Calculer la lame d'eau pour l'entité *Lame* selon la pondération"
            " fournie dans le fichier *listW.txt*",
    'param': ['Lame', 'listW.txt'],
    'longparam': ['Nom de la lame', 'Fichier des pondérations']
}
METHODS['ath'] = {
    'longname': 'aboveThreshold',
    'short': 'Ne conserver que les valeurs sup-seuil',
    'help': 'Ne conserver que les valeurs sup-seuil',
    'param': ['Seuil'],
    'longparam': ['Seuil']
}
METHODS['bth'] = {
    'longname': 'belowThreshold',
    'short': 'Ne conserver que les valeurs sub-seuil',
    'help': 'Ne conserver que les valeurs sub-seuil',
    'param': ['Seuil'],
    'longparam': ['Seuil']
}
METHODS['bwd'] = {
    'longname': 'betweenDates',
    'short': 'Extraire entre 2 dates',
    'help': 'Extraire entre 2 dates',
    'param': ['début', 'fin'],
    'longparam': ['Premier pas de temps à conserver',
                  'Dernier pas de temps à conserver']
}
METHODS['cat'] = {
    'longname': 'concatData',
    'short': "Concaténer les séries en une seule",
    'help': "Concaténer les séries en une seule sous l'identité *ID*",
    'param': ['ID'],
    'longparam': ['Nouvel identifiant']
}
METHODS['ccd'] = {
    'longname': 'changeCode',
    'short': "Re-définir l'identifiant de série",
    'help': "Re-définir l'id de la/des série(s) avec l'option (-s), "
            "le paramètre est le nouvel *ID* avec l'option (-l), "
            "le paramètre est le nom du fichier *ID* contenant "
            "les nouveaux ids",
    'param': ['ID'],
    'longparam': ['Nouvel identifiant']
}
METHODS['cpy'] = {
    'longname': 'copyYear',
    'short': "",
    'help': "Dupliquer le bloc annuel de la source pour compléter les données "
            "de la receveuse (-s/-l). Si l'option (-s) est utilisée, la "
            "source est une station. Si l'option (-l) est utilisée, la source "
            "est un fichier contenant la list des codes des stations src. "
            "La copie recouvre la période définie par *start* et *end*.",
    'param': ['source', 'année', 'start', 'end'],
    'longparam': ['Série source', 'Année pivot', 'Premier pas de temps',
                  'Dernier pas de temps']
}
METHODS['dsc'] = {
    'longname': 'downscale',
    'short': "Désagréger des données à un pas de temps inférieur",
    'help': "Désagréger des données à un pas de temps inférieur. "
            "Si le pas de temps final est journalier, il est nécessaire de "
            "définir l'heure de début d'une journée",
    'param': ['XX', '66/00'],
    'longparam': ["Paramètre cible de l'agrégation",
                  "Limite du jour: '66' ou '00'"]
}
METHODS['etp'] = {
    'longname': 'EvapoTransPot',
    'short': "Calculer l'ETP journalière ou horaire",
    'help': "Calculer l'évapo-transp. journalière (daily) ou horaire (hourly)"
            " à la latitude N",
    'param': ['daily/hourly', 'N'],
    'longparam': ["Pas de temps ('daily' ou 'hourly')", "Latitude"]
}
METHODS['f2s'] = {
    'longname': 'fcst2sim',
    'short': "Convertir des prévisions en séries à horizons fixes",
    'help': "Convertir des prévisions en séries à horizons fixes",
    'param': ['leadtime_1', 'leadtime_2'],
    'longparam': ['Horizon de prévision 1', 'Horizon de prévision 2']
}
METHODS['fct'] = {
    'longname': 'fillConstant',
    'short': "Remplacer les valeurs manquantes par une valeur constante",
    'help': "Remplacer les valeurs manquantes par une valeur constante *C*",
    'param': ['C'],
    'longparam': ['Constante']
}
METHODS['fli'] = {
    'longname': 'fillLinInterp',
    'short': "Remplacer les valeurs manquantes par une interpolation linéaire",
    'help': "Remplacer les valeurs manquantes par une interpolation linéaire",
    'param': None,
    'longparam': None
}
METHODS['lrg'] = {
    'longname': 'linearReg',
    'short': "Combinaison linéaire et décalage temporel",
    'help': "Transformer les données selon une régression linéaire et un "
            "décalage temporel: NEW = scale * OLD(T+timelag) +offset",
    'param': ['scale', 'offset', 'timelag'],
    'longparam': ["Facteur d'échelle", "Constante", "Décalage temporel"]
}
METHODS['nsc'] = {
    'longname': 'nearlyequalscale',
    'short': "Transformer la série à un pas de temps très proche",
    'help': "Transformer la série à un pas de temps très proche",
    'param': ['XX', 'Y,y,o,O,n,N,T,t,F,f'],
    'longparam': ["Paramètre cible de l'agrégation",
                  "Prise en compte stricte des valeurs manquantes"]
}
METHODS['pct'] = {
    'longname': 'percentile',
    'short': "Percentiles",
    'help': "Calculer les séries quantiles des séries listée par (-l). "
            "Les fichiers sont écrits en tenant compte d'un préfixe, défini "
            "à partir du nom du fichier listant les séries à considérer et "
            "des quantiles demandés (entiers entre 0 et 100)",
    'param': ['quantiles'],
    'longparam': ["quantiles (séparateur: -)"]
}
METHODS['res'] = {
    'longname': 'reservoir',
    'short': "Convertir par le Z0 et le barème d'un réservoir",
    'help': "Convertir par le Z0 et le barème d'un réservoir selon son nom "
            "*TABLENAME* et selon la correspondance entre la variable et la "
            "colonne de la table *ASSOC* : 'X:Xabcde,Y:Y12345'",
    'param': ['RESFILE', '[TABLENAME]', '[ASSOC]'],
    'longparam': ['Chemin du fichier Réservoir',
                  '[Optionnel] Nom du barème à appliquer',
                  '[Optionnel] Correspondance entre une variable et une '
                  'colonne (X:Xabcde,Y:Y12345)']
}
METHODS['rsc'] = {
    'longname': 'regularscale',
    'short': "Conversion à un pas de temps régulier",
    'help': "Conversion à un pas de temps régulier",
    'param': None,
    'longparam': None
}
METHODS['rtc'] = {
    'longname': 'ratingCurve',
    'short': "Convertir par les courbes de tarage",
    'help': "Convertir les données hydrométriques (H, Q) selon les courbes de "
            "tarage contenues soit dans une base Bareme, soit dans un fichier "
            "XML-Sandre de la PHyC, dont le chemin est CTFILE. L'utilisateur "
            "peut demander l'application d'une courbe particulière selon son "
            "identifiant CT et, dans le cas PHyC, l'application des courbes "
            "de correction contenues dans LEVFILE",
    'param': ['CTFILE', '[LEVFILE]', '[CT]'],
    'longparam': ['Chemin du fichier BAREME ou PHyC',
                  '[Optionnel] Chemin des courbes de correction PHyC',
                  '[Optionnel] Code de la courbe de tarage']
}
METHODS['s2f'] = {
    'longname': 'sim2fcst',
    'short': "Convertir des simulations en prévisions",
    'help': "Convertir des simulations en prévisions, avec écart décroissant"
            " linéairement avec l'horizon jusqu'au délai de recalage de *N1* "
            "pas de temps et sur N2 écheances de prévision. Les instants de "
            "prévision sont inclus dans la période définie par D1 et D2",
    'param': ['N1', 'N2', '[D1]', '[D2]'],
    'longparam': ['Délai de recalage', 'Horizon de prévision maximal',
                  'Premier instant', 'Dernier instant']
}
METHODS['shs'] = {
    'longname': 'subhourlyscale',
    'short': "Conversion à un pas de temps infra-horaire",
    'help': "Conversion à un pas de temps infra-horaire de *ts* secondes "
            "selon la méthode *how* et, si celle-ci est 'interpolate', "
            "selon la méthode *howinterp*",
    'param': ['ts', 'how', 'howinterp'],
    'longparam': ["Pas de temps, en secondes",
                  "[Optionnel] Méthode de ré-échantillonnage parmi "
                  "['bfill', 'ffill', 'nearest', 'interpolate']",
                  "[Optionnel] Méthode d'interpolation (si how=interpolate)"]
}
METHODS['tlg'] = {
    'longname': 'timelag',
    'short': "Décaler dans le temps",
    'help': "Décaler dans le temps les données de *N* pas de temps",
    'param': ['N'],
    'longparam': ["Décalage temporel"]
}
METHODS['usc'] = {
    'longname': 'upscale',
    'short': "Agréger les données à un pas de temps supérieur",
    'help': "Agréger les données à un pas de temps supérieur (XX), "
            "Avec prise en compte stricte des valeurs manquantes (o/n)"
            "Si le pas de temps final est journalier, il est nécessaire de "
            "définir l'heure de début d'une journée (00/66)",
    'param': ['XX', 'Y,y,o,O,n,N,T,t,F,f', '66/00'],
    'longparam': ["Paramètre cible de l'agrégation",
                  "Prise en compte stricte des valeurs manquantes",
                  "Limite du jour: '66' ou '00'"]
}
METHODS['wav'] = {
    'longname': 'weighted_average',
    'short': "Moyenne pondérée",
    'help': "Calculer la moyenne pondérée des séries listée par (-l). "
            "Les fichiers sont écrits en tenant compte d'un préfixe, défini "
            "à partir du nom du fichier listant les séries à considérer",
    'param': ['listW.txt'],
    'longparam': ['Fichier des pondérations']
}

METHODS_STR = ''
for arg in METHODS:
    if METHODS[arg]['longname'] is not None:
        METHODS_STR += "\n+ {}/{} : ".format(arg, METHODS[arg]['longname'])
    else:
        METHODS_STR += "\n+ {} : ".format(arg)
    METHODS_STR += METHODS[arg]['help']
    if METHODS[arg]['param'] is not None:
        METHODS_STR += " (param: {})"\
                       "".format(" ".join(METHODS[arg]['param']))
METHODS_STR += "\n"


DESCRIPTION = "Traiter les données format de type csv "\
    "(grp16, grp18, grp20, pyspc), selon une méthode choisie"
INFO = {
    "1": {
        "short": "Exporter en un seul fichier",
        "help": "Exporter en un seul fichier, valable uniquement si "
                "le format csv est de type 'pyspc'"
    },
    "C": {
        "short": "Format de fichier csv",
        "help": "Format de fichier csv"
    },
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier d'entrée"
    },
    "s": {
        "short": "Station à extraire",
        "help": "Identité de la station"
    },
    "l": {
        "short": "Liste de stations",
        "help": "Fichier contenant la liste des stations Hydro. "
                "Ignoré si l'option -s est utilisée"
    },
    "n": {
        "short": "Nom de la variable",
        "help": "Nom de la variable à extraire"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier de sortie"
    },
    "o": {
        "short": "[optionnel] Compléter/corriger les données existantes ?",
        "help": "[optionnel] Compléter/corriger les données existantes ?"
                "(defaut=False)"
    },
    "M": {
        "short": "Méthode de traitement",
        "help": "Méthodes de traitement des données Grp :" + METHODS_STR
    },
    "v": {
        "short": "Mode verbeux",
        "help": "[optionnel] Mode verbeux"
    },
    "w": {
        "short": "Mode Avertissement",
        "help": "[optionnel] Mode Avertissement (defaut=True). "
                "Si False, ne lève pas les erreurs lors de la "
                "création et lors de la manipulation des séries "
                "de données"
    }
}
LIST_VARNAMES = sorted(SPC_VARNAMES.keys())
# LIST_MTYPE = sorted(METHODS.keys())
LIST_FILETYPES = [' '.join([f1, f2]) for f1 in FILETYPES for f2 in FILETYPES]


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def csv2csv():
    """
    Validation des options et arguments de <csv2csv.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    if args.csv_type is None:
        args.csv_type = LIST_FILETYPES[-1].split(' ')
    else:
        _exception.raise_valueerror(
            ' '.join(args.csv_type) not in LIST_FILETYPES,
            "Définition incorrecte des types de format csv (-C)\n{} n'est pas "
            "dans {}".format(' '.join(args.csv_type), LIST_FILETYPES)
        )
    _exception.raise_valueerror(
        args.stations_list_file is None and
        args.processing_method[0] in ['arp', 'areaPrcp',
                                      'cat', 'concatData',
                                      'pct', 'percentile',
                                      'wav', 'weighted_average'],
        "l'option (-l) est nécessaire pour la méthode '{}'"
        "".format(args.processing_method[0])
    )
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <csv2csv.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(
        description=DESCRIPTION,
        formatter_class=argparse.RawTextHelpFormatter)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-C", "--csv-type",
                        action="store",
                        dest="csv_type",
                        nargs='+',
                        help=INFO["C"]["help"])
    parser.add_argument("-1", "--onefile-export",
                        action="store_true",
                        dest="onefile",
                        default=False,
                        help=INFO["1"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        help=INFO["I"]["help"])
    parser.add_argument("-n", "--varname",
                        action="store",
                        dest="varname",
                        choices=LIST_VARNAMES,
                        help=INFO["n"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        help=INFO["O"]["help"])
    parser.add_argument("-o", "--overwrite",
                        action="store_true",
                        dest="overwrite",
                        default=False,
                        help=INFO["o"]["help"])
    parser.add_argument("-M", "--Method",
                        action="store",
                        dest="processing_method",
                        nargs='+',
                        metavar=('Method', 'param(s)'),
                        help=INFO["M"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    parser.add_argument("-w", "--warning",
                        action="store_false",
                        dest="warning",
                        default=True,
                        help=INFO["w"]["help"])
    # Identifiants : unique ou par liste
    code_group = parser.add_mutually_exclusive_group(required=True)
    code_group.add_argument("-s", "--station-name",
                            action="store",
                            dest="station_name",
                            help=INFO["s"]["help"])
    code_group.add_argument("-l", "--station-list",
                            action="store",
                            dest="stations_list_file",
                            help=INFO["l"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
