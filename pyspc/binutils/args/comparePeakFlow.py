#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <comparePeakFlow.py>
"""

# Modules Python
import sys
import argparse
from pyspc.binutils.print_args import print_args
from pyspc.core.convention import SPC_VARNAMES

DESCRIPTION = "Comparer les pointes de crues entre deux sites"
INFO = {
    "c": {
        "short": "Nom du fichier du site amont",
        "help": "Nom du fichier du site amont"
    },
    "d": {
        "short": "Nom du fichier du site aval",
        "help": "Nom du fichier du site aval"
    },
    "I": {
        "short": "Dossier des fichiers événements",
        "help": "Dossier des fichiers événements"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier de sortie"
    },
    "n": {
        "short": "Nom de la grandeur",
        "help": "Nom de la grandeur"
    },
    "v": {
        "short": "Mode verbeux",
        "help": "[optionnel] Mode verbeux"
    },
}
LIST_VARNAMES = SPC_VARNAMES


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def comparePeakFlow():
    """
    Validation des options et arguments de <comparePeakFlow.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <comparePeakFlow.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-c", "--up-filename",
                        action="store",
                        dest="upfile",
                        required=True,
                        help=INFO["c"]["help"])
    parser.add_argument("-d", "--down-filename",
                        action="store",
                        dest="downfile",
                        required=True,
                        help=INFO["d"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-n", "--varname",
                        action="store",
                        dest="varname",
                        required=True,
                        choices=LIST_VARNAMES,
                        help=INFO["n"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
