#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <bdimage2xml.py>
"""

# modules Python
import sys
import argparse

# Module PySPC
from pyspc.binutils.print_args import print_args
import pyspc.core.exception as _exception
from pyspc.webservice.lamedo import BdImage
from pyspc.convention.lamedo import BDIMAGE_PREFIX
from pyspc.core.convention import SPC_VARNAMES
from pyspc import Parameter


DESCRIPTION = "Extraire les données de BdImage"
INFO = {
    "F": {
        "short": "Date de début",
        "help": "Premier pas de temps de la série de données "
                "(aaaammjj[HH[MM]])"
    },
    "L": {
        "short": "Date de fin",
        "help": "Dernier pas de temps de la série de données "
                "(aaaammjj[HH[MM]])"
    },
    "n": {
        "short": "Nom de la variable",
        "help": "Nom de la variable (convention PySPC)"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier de sortie"
    },
    "r": {
        "short": "Date du run de la prévision SYMPO",
        "help": "Date du run de la prévision SYMPO (AAAAMMJJ[HH[MM]])"
    },
    "S": {
        "short": "Zone",
        "help": "Nom du domaine spatial (Zone BDImage). "
                "Pour demander plusieurs zones, il suffit de les "
                "relier par '+'. Par ex: LO12844+LO808 pour avoir "
                "les zones correspondant aux bassins versants de "
                "la Loire à Chadrac-Pont et du Lignon du Velay à "
                "Chambon-sur-Lignon. "
                "Un pixel est libellé x,y. Il est possible d'en définir "
                "plusieurs en les reliant par '+'"
                "Un rectangle est libellé Bx1,2,3,4"
                "en renseignant dans l'ordre le préfixe 'Bx', les coordonnées"
                "X,Y du point en haut à gauche et X,Y du point en bas à droite"
                "Les pixels et coordonnées des rectangles sont à renseigner "
                "en mètre."
    },
    "t": {
        "short": "Type, sous-type et bande",
        "help": "Type, sous-type et bande de l'image BDImage"
    },
    "U": {
        "short": "Eléments à préciser (stats, precision)",
        "help": "Eléments à préciser (stats dans ['standard', 'complete'], "
                "precision dans ['standard', 'forte'])"
    },
    "v": {
        "short": "Mode verbeux",
        "help": "[optionnel] Mode verbeux (defaut=False)"
    }
}
#    "c": {
#        "short": "Fichier de config",
#        "help": "[optionnel] Nom du fichier de configuration "
#                "BdImage et Proxies"
#    },

LIST_DATATYPES = [' '.join(list(i)) for i in BdImage.get_datatypes()]
prefix = list({p for p in BDIMAGE_PREFIX.values() if isinstance(p, str)})
LIST_VARNAMES = sorted(list({Parameter(varname=v).spc_varname
                             for v in SPC_VARNAMES.keys()
                             if v[0] in prefix}))
LIST_UTYPES = ['stats', 'precision']


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def bdimage2xml():
    """
    Validation des options et arguments de <bdimage2xml.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    _exception.raise_valueerror(
        ' '.join(args.data_type) not in LIST_DATATYPES,
        "l'option (-t) est mal définie. Type de donnée '{0}' inconnu"
        "".format(args.data_type[0])
    )
    if args.update_element is not None:
        for u in args.update_element:
            _exception.raise_valueerror(
                u[0] not in LIST_UTYPES,
                "l'option (-U) est mal définie. Premier élément '{0}' "
                "inconnu".format(args.update_element[0])
            )
    if args.first_dtime is not None:
        args.first_dtime = '{0:0<12s}'.format(args.first_dtime)
    if args.last_dtime is not None:
        args.last_dtime = '{0:0<12s}'.format(args.last_dtime)
    if args.runtime is not None:
        args.runtime = '{0:0<12s}'.format(args.runtime)
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <bdimage2xml.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
#    parser.add_argument("-c", "--config-filename",
#                        action="store",
#                        dest="cfg_filename",
#                        help=INFO["c"]["help"])
    parser.add_argument("-F", "--First-datetime",
                        action="store",
                        dest="first_dtime",
                        # required=True,
                        help=INFO["F"]["help"])
    parser.add_argument("-L", "--Last-datetime",
                        action="store",
                        dest="last_dtime",
                        # required=True,
                        help=INFO["L"]["help"])
    parser.add_argument("-n", "--varname",
                        action="store",
                        dest="varname",
                        choices=LIST_VARNAMES,
                        required=True,
                        help=INFO["n"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        # required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-r", "--run-time",
                        action="store",
                        dest="runtime",
                        help=INFO["r"]["help"])
    parser.add_argument("-S", "--Spatial-domain",
                        action="store",
                        dest="spatial_domain",
                        help=INFO["S"]["help"])
    parser.add_argument("-t", "--data-type",
                        action="store",
                        dest="data_type",
                        metavar=('type', 'subtype', 'band'),
                        nargs=3,
                        help=INFO["t"]["help"])
    parser.add_argument("-U", "--user-element",
                        action="append",
                        dest="update_element",
                        metavar=('Keyword', 'Value'),
                        nargs=2,
                        help=INFO["U"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
