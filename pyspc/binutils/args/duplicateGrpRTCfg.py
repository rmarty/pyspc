#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <duplicateGrpRTCfg.py>
"""

# Modules Python
import sys
import argparse
from pyspc.binutils.print_args import print_args
from pyspc.model.grp16 import GRPRT_Cfg as GRPRT_Cfg16
from pyspc.model.grp18 import GRPRT_Cfg as GRPRT_Cfg18
from pyspc.model.grp20 import GRPRT_Cfg as GRPRT_Cfg20

DESCRIPTION = "Dupliquer la configuration GRP Temps Reel "\
    "et modifier son contenu"
INFO = {
    "c": {
        "short": "Nom du fichier Config <receiver>",
        "help": "Nom du fichier de configuration de GRP Temps Reel"
    },
    "D": {
        "short": "Nom du fichier Config <source>",
        "help": "[optionnel] Nom du fichier de configuration 'source'"
                ", si different de (-s)"
    },
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier d'entrée"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier de sortie"
    },
    "t": {
        "short": "Version de GRP",
        "help": "Version de GRP"
    },
    "v": {
        "short": "Mode verbeux",
        "help": "[optionnel] Mode verbeux"
    },
    "U": {
        "short": "Configuration: balises et nouvelles valeurs",
        "help": "Balises et valeur associees de la configuration à "
                "mettre à jour"
    }
}
LIST_DATATYPES = ['grp16', 'grp18', 'grp20']
LIST_UTYPES = []
LIST_UTYPES.extend(GRPRT_Cfg16.get_cfg_keys())
LIST_UTYPES.extend(GRPRT_Cfg18.get_cfg_keys())
LIST_UTYPES.extend(['_'.join(list(x))
                    for x in GRPRT_Cfg20.get_cfg_keys(config_run=True)])
LIST_UTYPES = sorted(list(set(LIST_UTYPES)))


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def duplicateGrpRTCfg():
    """
    Validation des options et arguments de <duplicateGrpRTCfg.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    if args.duplicata_src is None:
        args.duplicata_src = args.cfg_filename
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <duplicateGrpRTCfg.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-c", "--config-filename",
                        action="store",
                        dest="cfg_filename",
                        required=True,
                        help=INFO["c"]["help"])
    parser.add_argument("-D", "--Duplicata-source",
                        action="store",
                        dest="duplicata_src",
                        help=INFO["D"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-t", "--data-type",
                        action="store",
                        dest="data_type",
                        required=True,
                        choices=LIST_DATATYPES,
                        help=INFO["t"]["help"])
    parser.add_argument("-U", "--Update-config",
                        action="append",
                        dest="update_cfg",
                        metavar=('Keyword', 'Value'),
                        nargs='*',
                        help=INFO["U"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
