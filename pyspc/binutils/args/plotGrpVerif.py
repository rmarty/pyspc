#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <plotGrpVerif.py>
"""
import argparse
import sys
from pyspc.binutils.print_args import print_args

DESCRIPTION = "Tracer sous forme graphique les performances GRP fournies "\
    "par grpVerif.py"

LIST_UTYPES = ['SC', 'HC', 'SV', 'MODE']

INFO = {
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier du fichier de synthèse des performances GRP"
    },
    "d": {
        "short": "Nom ou motif du fichier d'entrée",
        "help": "Nom ou motif du fichier de synthèse des performances GRP"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier des images radar des performances de GRP"
    },
    "v": {
        "short": "[optionnel] Mode verbeux",
        "help": "[optionnel] Mode verbeux (defaut=False)"
    },
    "U": {
        "short": "Objet de la configuration GRP et liste de valeurs",
        "help": 'Objet de la configuration GRP parmi {} '
                'et liste des valeurs associées'.format(LIST_UTYPES)
    }
}


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def plotGrpVerif():
    """
    Validation des options et arguments de <plotGrpVerif.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <plotGrpVerif.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-d", "--data-filename",
                        action="store",
                        dest="data_filename",
                        required=True,
                        help=INFO["d"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    parser.add_argument("-U", "--user-cgrponfig",
                        action="append",
                        dest="grp_config",
                        metavar='Name Value1 Value2 ...',
                        nargs='*',
                        help=INFO["U"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
