#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <mdbInfo.py>
"""

# modules Python
import argparse
from datetime import datetime as dt
import sys
from pyspc.binutils.print_args import print_args
import pyspc.core.exception as _exception

DESCRIPTION = "Afficher l'arborescence depuis un répertoire"
INFO = {
    "I": {
        "short": "Root dirname",
        "help": "Root dirname"
    },
    "c": {
        "short": "[optionnal] Duplicated files ?",
        "help": "[optionnal] Duplicated files ?"
    },
    "d": {
        "short": "[optionnal] Tree depth",
        "help": "[optionnal] Tree depth"
    },
    "s": {
        "short": "[optionnal] Size of path content ?",
        "help": "[optionnal] Size of path content ?"
    },
    "l": {
        "short": "[optionnal] Too long pathnames warning ?",
        "help": "[optionnal] Too long pathnames warning ?"
    },
    "o": {
        "short": "[optionnal] Too old files ?",
        "help": "[optionnal] Too old files ?"
    },
    "t": {
        "short": "[optionnal] Datetime of too old files",
        "help": "[optionnal] Datetime of too old files"
    },
    "v": {
        "short": "[optionnal] Verbose mode",
        "help": "[optionnal] Verbose mode"
    }
}


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def viewTree():
    """
    Validation des options et arguments de <viewTree.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    if not isinstance(args.oldtime, dt):
        for fmt in ['%Y', '%Y%m', '%Y%m%d', '%Y%m%d%H', '%Y%m%d%H%M']:
            try:
                oldtime = dt.strptime(args.oldtime, fmt)
            except ValueError:
                oldtime = None
            else:
                args.oldtime = oldtime
                break
        else:
            _exception.Warning(
                __name__,
                "Uncorrect datetime of too old files"
            )
            args.oldfile = False
            args.oldtime = None
    if args.verbose:
        print(" -- Exécution du script "+sys.argv[0])
        print("    + Vérification des arguments")
        print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <viewTree.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Création de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-c", "--common-file",
                        action="store_true",
                        dest="duplicate",
                        default=False,
                        help=INFO["c"]["help"])
    parser.add_argument("-d", "--depth-tree",
                        action="store",
                        dest="depth",
                        default=3,
                        type=int,
                        help=INFO["d"]["help"])
    parser.add_argument("-l", "--length-warning",
                        action="store_true",
                        dest="length",
                        default=False,
                        help=INFO["l"]["help"])
    parser.add_argument("-s", "--size-info",
                        action="store_true",
                        dest="size",
                        default=False,
                        help=INFO["s"]["help"])
    parser.add_argument("-o", "--oldfile-warning",
                        action="store_true",
                        dest="oldfile",
                        default=False,
                        help=INFO["o"]["help"])
    parser.add_argument("-t", "--oldfile-time",
                        action="store",
                        dest="oldtime",
                        default=dt.now(),
                        help=INFO["o"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
