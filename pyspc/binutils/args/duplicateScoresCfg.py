#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Validation des options et arguments de <duplicateScoresCfg.py>
"""

# Modules Python
import sys
import argparse

from pyspc.binutils.print_args import print_args
from pyspc.verification.scores import Config

DESCRIPTION = "Dupliquer la configuration SCORES et modifier son contenu"
INFO = {
    "c": {
        "short": "Nom du fichier Config <receiver>",
        "help": "Nom du fichier de configuration de SCORES"
    },
    "D": {
        "short": "Nom du fichier Config <source>",
        "help": "[optionnel] Nom du fichier de configuration 'source'"
                ", si different de (-s)"
    },
    "I": {
        "short": "Dossier d'entrée",
        "help": "Dossier d'entrée"
    },
    "O": {
        "short": "Dossier de sortie",
        "help": "Dossier de sortie"
    },
    "v": {
        "short": "Mode verbeux",
        "help": "[optionnel] Mode verbeux"
    },
    "U": {
        "short": "Configuration: section, option et nouvelle valeur",
        "help": 'Section, option et valeur de la configuration '
                'à mettre à jour. Si la valeur contient des espaces, '
                'il faut la placer entre " (double quote)'
    }
}
LIST_UTYPES = ['{} {}'.format(*u) for u in Config.get_cfg_keys()]


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def duplicateScoresCfg():
    """
    Validation des options et arguments de <duplicateScoresCfg.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Définition des arguments
    # -----------------------------------------------
    parser = set_parser()
    args = parser.parse_args()
    # -----------------------------------------------
    #    ETAPE 2 : Contrôles des arguments
    # -----------------------------------------------
    if args.duplicata_src is None:
        args.duplicata_src = args.cfg_filename
    print_args(info=INFO, parser=parser, args=args, name=sys.argv[0])
    return args


def set_parser():
    """
    Validation des options et arguments de <duplicateScoresCfg.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Creation de l'instance du parser
    # -----------------------------------------------
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    # -----------------------------------------------
    #    ETAPE 2 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-c", "--config-filename",
                        action="store",
                        dest="cfg_filename",
                        required=True,
                        help=INFO["c"]["help"])
    parser.add_argument("-D", "--Duplicata-source",
                        action="store",
                        dest="duplicata_src",
                        help=INFO["D"]["help"])
    parser.add_argument("-I", "--Input-directory",
                        action="store",
                        dest="input_dir",
                        required=True,
                        help=INFO["I"]["help"])
    parser.add_argument("-O", "--Output-directory",
                        action="store",
                        dest="output_dir",
                        required=True,
                        help=INFO["O"]["help"])
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help=INFO["v"]["help"])
    parser.add_argument("-U", "--Update-config",
                        action="append",
                        dest="update_cfg",
                        metavar=('Section', 'Option', 'Value'),
                        nargs=3,
                        help=INFO["U"]["help"])
    # -----------------------------------------------
    #    ETAPE 3 : Renvoi du parser
    # -----------------------------------------------
    return parser
