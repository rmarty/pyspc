#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Exécution des opérations du binaire <onlineReport.py>
"""
from pyspc.binutils.get_stations_list import get_stations_list
import pyspc.core.exception as _exception
from pyspc.core.timeutil import str2dt
from pyspc.metadata.explore2070.carto2 import Carto2_List
from pyspc.metadata.shyreg.streamflow import Streamflow
from pyspc.webservice.report import OnlineReport


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def onlineReport(options):
    """
    Exécution des opérations du binaire <onlineReport.py>

    Parameters
    ----------
    options
        Retour de pyspc.binutils.args.onlineReport.onlineReport

    Returns
    -------
    filenames : dict
        Fichiers enregistrés
        {clé=(code, datatype), valeur = liste des fichiers associés}

    See Also
    --------
    pyspc.webservice.report.OnlineReport

    """
    # ==================================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ==================================================================
    if options.data_type == "inrae_explore2070" \
            and options.stations_list_file is not None:
        try:
            carto2 = Carto2_List(options.stations_list_file)
            df = carto2.read()
            stations_list = list(df.index.astype(str))
        except Exception:
            stations_list = []
    else:
        stations_list = get_stations_list(
            station_name=options.station_name,
            stations_list_file=options.stations_list_file)

    # ==================================================================
    #    3-- TELECHARGEMENT DES DOCUMENTS ONLINE
    # ==================================================================
    report = OnlineReport(reporttype=options.data_type)
    options.runtime = str2dt(options.runtime)
    global_filenames = []
    # ------------------------------------------------------------------
    #    3.1-- TRAITEMENT DES DONNEES SANS IDENTIFIANT
    # ------------------------------------------------------------------
    if options.data_type in ["mf_warning",
                             "vigicrues_reach", "vigicrues-1_geoinfo"]:
        _exception.Information(
            options.verbose, "    + Traitement sans identifiant")
        report.set_url(date=options.runtime)
        _exception.Information(
            options.verbose, "        + Lien vers le document : {}",
            report.url
        )
        report.set_filename(date=options.runtime, dirname=options.output_dir)
        _exception.Information(
            options.verbose, "        + Écriture du document : {}",
            report.filename
        )
        try:
            report.retrieve(engine='requests')
        except Exception as e:
            msg = "Document non téléchargé : {}".format(report.filename)
            if options.warning:
                raise FileNotFoundError(msg) from e
            _exception.Warning(msg=msg)
        global_filenames.append(report.filename)
    # ------------------------------------------------------------------
    #    3.2-- TRAITEMENT DES DONNEES PAR IDENTIFIANT
    # ------------------------------------------------------------------
    elif options.data_type in ["inrae_explore2070", "inrae_hydroclim",
                               "inrae_shyreg",
                               "inrae_shyreg_bnbv", "inrae_shyreg_hydro",
                               "mf_monthlyreport", "mf_dailyreport",
                               "mf_station", "mf_clim", "mf_climdata",
                               "vigicrues_fcst", "vigicrues_obs",
                               "vigicrues_loc", "vigicrues_sandre",
                               'vigicrues-1_info', 'vigicrues-1_domain',
                               'vigicrues-1_reach', 'vigicrues-1_loc',
                               ]:
        _exception.raise_valueerror(
            not stations_list and options.data_type != 'mf_warning',
            "aucune station à traiter"
        )
        _exception.Information(
            options.verbose, "    + Traitement par identifiant")
        for station in stations_list:
            # ----------------------------------------
            #    3.1.1 DEFINITION DU DOCUMENT CIBLE
            # ----------------------------------------
            _exception.Information(
                options.verbose, "      - Identifiant : {}", station)
            try:
                station, codetype = station.split(',')
            except ValueError:
                codetype = None
            _exception.Information(
                options.verbose and options.varname is not None,
                "      - Grandeur : {}", options.varname)
            report.set_url(code=station, varname=options.varname,
                           date=options.runtime, codetype=codetype)
            _exception.Information(
                options.verbose, "        + Lien vers le document : {}",
                report.url
            )
            report.set_filename(
                code=station, date=options.runtime, varname=options.varname,
                codetype=codetype, dirname=options.output_dir)
            # ----------------------------------------
            #    3.1.2 RECUPERATION DOCUMENT CIBLE
            # ----------------------------------------
            _exception.Information(
                options.verbose, "        + Écriture du document : {}",
                report.filename
            )
            try:
                # report.verify = False
                report.retrieve(engine='requests')
            except Exception as e:
                msg = "Document non téléchargé : {}".format(report.filename)
                if options.warning:
                    raise FileNotFoundError(msg) from e
                _exception.Warning(msg=msg)
                continue
            global_filenames.append(report.filename)
            # ----------------------------------------
            #    3.1.3 CONVERSION DES DOCUMENTS ONLINE
            # ----------------------------------------
            if options.data_type in [
                    "inrae_shyreg", "inrae_shyreg_bnbv", "inrae_shyreg_hydro"]:
                metadata = Streamflow(filename=report.filename)
                filenames = metadata.to_csv()
                global_filenames.extend(filenames)
                _exception.Information(
                    options.verbose, "        + Conversion : {}", filenames)
    # ------------------------------------------------------------------
    #    3.X-- NotImplementedError
    # ------------------------------------------------------------------
    else:
        raise NotImplementedError("Type de donnée '{}' non implémenté"
                                  "".format(options.data_type))

    # ===============================================================
    #    4-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script onlineReport")
    return global_filenames
