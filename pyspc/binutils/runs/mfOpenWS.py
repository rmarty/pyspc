#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Exécution des opérations du binaire <mfOpenWS.py>
"""
import warnings

from pyspc.binutils.get_stations_list import get_stations_list
import pyspc.core.exception as _exception
from pyspc.core.config import Config
from pyspc.core.parameter import Parameter
from pyspc.core.timeutil import str2dt
from pyspc.webservice.meteofrance import OpenAPI, OpenData


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def mfOpenWS(options):
    """
    Exécution des opérations du binaire <mfOpenWS.py>

    Parameters
    ----------
    options
        Retour de pyspc.binutils.args.mfOpenWS.mfOpenWS

    Returns
    -------
    filenames : list
        Fichiers enregistrés
        {clé=(code, datatype), valeur = liste des fichiers associés}

    See Also
    --------
    pyspc.webservice.meteofrance.OpenAPI.retrieve
    pyspc.webservice.meteofrance.OpenData.retrieve

    """
    # ===============================================================
    #    1-- TRAITEMENT DES OPTIONS
    # ===============================================================
    stations_list = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)
    _exception.raise_valueerror(not stations_list, "aucune station à traiter")
    options.first_dtime = str2dt(options.first_dtime)
    options.last_dtime = str2dt(options.last_dtime)
    param = Parameter(options.varname)
    timestep = param.timestep
    # ===============================================================
    #    2A-- RECUPERATION DES DONNEES OPEN DATA
    # ===============================================================
    if options.datatype == 'MF_OpenData':
        _exception.Information(
            options.verbose,
            "    + Appel(s) Météo-France de type {}", options.datatype)
        odata = OpenData()
        odata.login()
        filenames = odata.retrieve(
            dirname=options.output_dir,
            codes=stations_list,
            start=options.first_dtime,
            end=options.last_dtime,
            timestep=timestep,
            desc=True,
            rr_t_uv=True
        )
        odata.logout()
    # ===============================================================
    #    2B-- RECUPERATION DES DONNEES OPEN API
    # ===============================================================
    elif options.datatype == 'MF_OpenAPI':
        warnings.filterwarnings('ignore', message='Unverified HTTPS request')
        _exception.Information(
            options.verbose,
            "    + Appel(s) Météo-France de type {}", options.datatype)
        _exception.Information(
            options.verbose,
            "    + Lecture de la configuration MF Open API : {}",
            options.cfg_filename)
        api_config = Config(filename=options.cfg_filename)
        api_config.read()
        appids = {s: api_config[s].get('appid', '') for s in api_config}
        apikeys = {s: api_config[s].get('apikey', None) for s in api_config}
        oapi = OpenAPI(appid=appids.get('data_obs_meteo', None),
                       apikey=apikeys.get('data_obs_meteo', None))
        oapi.login()
        filenames = oapi.retrieve(
            dirname=options.output_dir,
            datatype='data_obs_meteo',
            codes=stations_list,
            start=options.first_dtime,
            end=options.last_dtime,
            timestep=timestep
        )
        oapi.logout()

    # ===============================================================
    #    3-- RECUPERATION DES DONNEES OPEN API
    # ===============================================================
    else:
        raise NotImplementedError

    # ===============================================================
    #    4-- FICHIERS RECUPERES
    # ===============================================================
    for f in filenames:
        _exception.Information(options.verbose,
                               "      - Ecriture du fichier : {}", f)

    # ===============================================================
    #    4-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script mfOpenWS")
    return filenames
