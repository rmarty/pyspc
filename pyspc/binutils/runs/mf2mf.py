#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Exécution des opérations du binaire <mf2mf.py>
"""
import glob
import os

import pyspc.core.exception as _exception
from pyspc.binutils.get_stations_list import get_stations_list
from pyspc import read_MF_Data, read_MF_OpenAPI, read_MF_OpenData


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def mf2mf(options):
    """
    Exécution des opérations du binaire <mf2mf.py>

    Parameters
    ----------
    options
        Retour de pyspc.binutils.args.mf2mf.mf2mf

    Returns
    -------
    filenames : list
        Fichiers enregistrés
        {clé=(code, datatype), valeur = liste des fichiers associés}

    See Also
    --------
    pyspc.webservice.meteofrance.OpenData.retrieve

    """
    # ===============================================================
    #    1-- TRAITEMENT DES OPTIONS
    # ===============================================================
    filenames = []
    _exception.Information(
        options.verbose, "    + Lister les fichiers MF")
    mf_filenames = glob.glob(os.path.join(options.input_dir,
                                          options.mfdata_filename))
    _exception.raise_valueerror(not mf_filenames,
                                "Aucun fichier MF")
    # Uniquement pour OpenData
    stations_list = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)
    if not stations_list:
        stations_list = None

    # ===============================================================
    #    2A-- TRAITEMENT DES DONNEES DATA (PUBLITHEQUE)
    # ===============================================================
    if options.datatype == 'MF_Data':
        for mf_filename in mf_filenames:
            _exception.Information(
                options.verbose, "      - Lecture du fichier {}", mf_filename)
            series = read_MF_Data(filename=mf_filename,
                                  warning=options.warning)
            filenames.append(series.to_MF_Data(
                dirname=options.output_dir,
                basename=os.path.basename(mf_filename)
            ))
    # ===============================================================
    #    2B-- TRAITEMENT DES DONNEES OPEN DATA (METEO.DATA.GOUV.FR)
    # ===============================================================
    elif options.datatype == 'MF_OpenData':
        for mf_filename in mf_filenames:
            _exception.Information(
                options.verbose, "      - Lecture du fichier {}", mf_filename)
            try:
                series = read_MF_OpenData(
                    filename=mf_filename, codes=stations_list,
                    warning=options.warning)
            except KeyError:
                _exception.Warning(msg="Problème lors de la lecture du "
                                   f"fichier {mf_filename}")
                continue
            basename = os.path.splitext(
                os.path.basename(mf_filename))[0] + '.data'
            fs = series.to_MF_Data(
                dirname=options.output_dir, basename=basename)
            if isinstance(fs, str):
                filenames.append(fs)
            else:
                filenames.extend(fs)
    # ===============================================================
    #    2C-- TRAITEMENT DES DONNEES OPEN API
    # ===============================================================
    elif options.datatype == 'MF_OpenAPI':
        for mf_filename in mf_filenames:
            _exception.Information(
                options.verbose, "      - Lecture du fichier {}", mf_filename)
            series = read_MF_OpenAPI(filename=mf_filename,
                                     warning=options.warning)
            basename = os.path.splitext(
                os.path.basename(mf_filename))[0] + '.data'
            fs = series.to_MF_Data(
                dirname=options.output_dir, basename=basename)
            if isinstance(fs, str):
                filenames.append(fs)
            else:
                filenames.extend(fs)

    # ===============================================================
    #    4-- RECUPERATION DES DONNEES OPEN API
    # ===============================================================
    else:
        raise NotImplementedError

    # ===============================================================
    #    4-- FICHIERS RECUPERES
    # ===============================================================
    for f in filenames:
        _exception.Information(options.verbose,
                               "      - Ecriture du fichier : {}", f)

    # ===============================================================
    #    4-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script mf2mf")
    return filenames
