#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Exécution des opérations du binaire <hydroportailStats.py>
"""
from pyspc.binutils.get_stations_list import get_stations_list
import pyspc.core.exception as _exception
from pyspc.core.config import Config
from pyspc.webservice.hydroportail import Hydroportail


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def hydroportailStats(options):
    """
    Exécution des opérations du binaire <hydroportailStats.py>

    Parameters
    ----------
    options
        Retour de pyspc.binutils.args.hydroportailStats.hydroportailStats

    Returns
    -------
    filenames : dict
        Fichiers enregistrés
        {clé=(code, datatype), valeur = liste des fichiers associés}

    See Also
    --------
    pyspc.webservice.hydroportail.Hydroportail.retrieve

    """
    # ===============================================================
    #    1-- TRAITEMENT DES OPTIONS
    # ===============================================================
    stations_list = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)
    _exception.raise_valueerror(not stations_list, "aucune station à traiter")

    # ===============================================================
    #    2-- LECTURE CONFIGURATION
    #        INITIALISATION DE LA SESSION HYDROPORTAIL
    # ===============================================================
    _exception.Information(
        options.verbose,
        "    + Lecture de la configuration Hydroportail : {}",
        options.cfg_filename)
    hydro_config = Config(filename=options.cfg_filename)
    hydro_config.read()
    try:
        proxies = hydro_config['proxies']
    except KeyError:
        proxies = None
    hydro_session = Hydroportail(
        hostname=hydro_config['hydroportail']['hostname'], proxies=proxies)
    # ===============================================================
    #    3-- RECUPERATION DES FLUX XML
    # ===============================================================
    _exception.Information(
        options.verbose,
        "    + Appel(s) Hydroportail de type {}", options.datatype)

    # ---------------------------------------------------------------
    #    3.3-- TELECHARGEMENT
    # ---------------------------------------------------------------
    ouahs = bool(options.datatype in ['Q3J-N', 'QM-N', 'Q-X', 'QJ-X',
                                      'QJ-annuel'])
    filenames = hydro_session.retrieve(
        dirname=options.output_dir,
        datatype=options.datatype,
        codes=stations_list,
        ouahs=ouahs)
    for case in filenames:
        _exception.Information(options.verbose,
                               "      - Ecriture du fichier : {}",
                               filenames[case])

    # ===============================================================
    #    4-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script hydroportailStats")
    return filenames
