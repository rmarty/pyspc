#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""Exécution des opérations du binaire <mf2csv.py>."""
import glob
import os
from pyspc.binutils.csv import write_csvlike
from pyspc.binutils.get_stations_list import get_stations_list
import pyspc.core.exception as _exception
from pyspc import (
    read_BP, read_MF_Data, read_Sympo, read_MF_OpenData, read_MF_OpenAPI)


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def mf2csv(options):
    """
    Exécution des opérations du binaire <mf2csv.py>.

    Parameters
    ----------
    options
        Retour de pyspc.binutils.args.mf2csv.mf2csv

    Returns
    -------
    filenames : list
        Fichiers enregistrés
        {clé=(code, datatype), valeur = liste des fichiers associés}

    """
    # ===============================================================
    #    1-- VERIFICATION DES OPTIONS/ARGUMENTS
    # ===============================================================
    stations_list = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)
    if not stations_list:
        stations_list = None

    # ===============================================================
    #    2-- TRAITEMENT DES DONNEES MF
    # ===============================================================
    _exception.Information(options.verbose, "    + Lecture des données MF")
    mf_filenames = glob.glob(os.path.join(options.input_dir,
                                          options.mfdata_filename))
    _exception.raise_valueerror(not mf_filenames,
                                "Aucun fichier MF")
    for mf_filename in mf_filenames:
        _exception.Information(
            options.verbose, "      - Lecture du fichier de type '{}' : {}",
            [options.data_type, mf_filename], unpacklist=True)
    # --------------------------------------------------------------
    #    2.1 : PUBLITHEQUE
    # --------------------------------------------------------------
        if options.data_type == "data":
            series = read_MF_Data(
                filename=mf_filename,
                codes=stations_list,
                warning=options.warning
            )
    # --------------------------------------------------------------
    #    2.2 : ARCHIVE BP
    # --------------------------------------------------------------
        elif options.data_type == "bp":
            series = read_BP(
                filename=mf_filename,
                zones=stations_list,
                warning=options.warning
            )
    # --------------------------------------------------------------
    #    2.3 : ARCHIVE SYMPO
    # --------------------------------------------------------------
        elif options.data_type == "sympo":
            series = read_Sympo(
                filename=mf_filename,
                zones=stations_list,
                warning=options.warning
            )
    # --------------------------------------------------------------
    #    2.4 : OPEN DATA ARCHIVE
    # --------------------------------------------------------------
        elif options.data_type == "MF_OpenData":
            if not stations_list:
                stations_list = None
            series = read_MF_OpenData(
                filename=mf_filename,
                codes=stations_list,
                warning=options.warning
            )
    # --------------------------------------------------------------
    #    2.5 : OPEN API
    # --------------------------------------------------------------
        elif options.data_type == "MF_OpenAPI":
            if not stations_list:
                stations_list = None
            series = read_MF_OpenAPI(
                filename=mf_filename,
                warning=options.warning
            )
    # --------------------------------------------------------------
    #    2.0 : Autre
    # --------------------------------------------------------------
        else:
            raise ValueError('Type de donnée incorrect')

    # ===============================================================
    #    3-- EXPORT DES DONNEES CSV
    # ===============================================================
        filenames = write_csvlike(
            series=series, csvtype=options.csv_type,
            dirname=options.output_dir, overwrite=options.overwrite,
            onefile=options.onefile)
        _exception.Information(options.verbose,
                               "        + Ecriture du fichier : {}", filenames)

    # ===============================================================
    #    4-- FIN DU PROGRAMME
    # ===============================================================
    _exception.Information(
        options.verbose, " -- Fin du script mf2mf")
    return filenames
