#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Exécution des opérations du binaire <csv2grpRT.py>
"""
import os
import sys

import pyspc.core.exception as _exception
from pyspc.binutils.get_stations_list import get_stations_list
from pyspc.binutils.csv import read_csvlike

import warnings
warnings.filterwarnings("ignore")


# -------------------------------------------------------------------
#      OPTIONS FUNCTIONS
# -------------------------------------------------------------------
def csv2grpRT(options):
    """
    Exécution des opérations du binaire <csv2grpRT.py>

    Parameters
    ----------
    options
        Retour de pyspc.binutils.args.csv2grpRT.csv2grpRT

    Returns
    -------
    filenames : list
        Fichiers enregistrés

    """
    # ==================================================================
    #    2-- LISTE DES STATIONS HYDRO A TRAITER
    # ==================================================================
    stations_list = get_stations_list(
        station_name=options.station_name,
        stations_list_file=options.stations_list_file)
    _exception.raise_valueerror(not stations_list, "aucune station à traiter")

    # ==================================================================
    #    3-- LECTURE DES DONNEES
    # ==================================================================
    _exception.Information(options.verbose, "    + Lecture des données")
    series = read_csvlike(
        stations=stations_list,
        varnames=options.varname,
        dirname=options.input_dir,
        csvtype=options.csv_type,
        warning=options.warning
    )
    print(stations_list)
    if options.station_name is not None:
        series.name = options.station_name
    else:
        series.name = os.path.splitext(
            os.path.basename(options.stations_list_file))[0]

    # ==================================================================
    #    4-- EXPORT DES DONNEES
    # ==================================================================
    version = options.data_type.split('_')[0]
    filenames = None
    # --------------------------------------------------------------
    #    4.1 : GRP TEMPS REEL -- FICHIER DATA
    # --------------------------------------------------------------
    if options.data_type.endswith('data'):
        filenames = series.to_GRPRT_Data(
            version=version, dirname=options.output_dir)
        _exception.Information(
            options.verbose,
            "      - Ecriture du fichier : {}", filenames)
    # --------------------------------------------------------------
    #    4.2 : GRP TEMPS REEL -- FICHIER SCEN METEO
    # --------------------------------------------------------------
    elif options.data_type.endswith('metscen'):
        filenames = series.to_GRPRT_Metscen(
            version=version, dirname=options.output_dir)
        _exception.Information(
            options.verbose,
            "      - Ecriture du fichier de scénario météo : {}", filenames)
    # --------------------------------------------------------------
    #    4.3 : GRP TEMPS REEL -- FICHIER ARCHIVE
    # --------------------------------------------------------------
    elif options.data_type.endswith('archive'):
        filenames = series.to_GRPRT_Archive(
            version=version, dirname=options.output_dir)
        _exception.Information(
            options.verbose,
            "      - Ecriture du fichier d'archive : {}", filenames)
    # --------------------------------------------------------------
    #    4.0 : AUTRE
    # --------------------------------------------------------------
    else:
        _exception.Warning(
            sys.argv[0],
            "type de fichier grpRT '{}' non traité".format(
                options.data_type)
            )

    # ==================================================================
    #    5-- FIN DU SCRIPT
    # ==================================================================
    _exception.Information(
        options.verbose, " -- Fin du script {}", sys.argv[0])

    return filenames
