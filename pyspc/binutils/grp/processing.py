#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - Traitements spécifiques GRP
"""
from pyspc.io.grp16 import read_GRP16
from pyspc.io.grp18 import read_GRP18
from pyspc.io.grp20 import read_GRP20
from pyspc.model.grp16 import GRPRT_Fcst as GRP16_Fcst
from pyspc.model.grp18 import GRPRT_Fcst as GRP18_Fcst
from pyspc.model.grp20 import GRPRT_Fcst as GRP20_Fcst


def set_grprt_reader(datatype):
    """Définir le lecteur brut selon la version de GRP"""
    if datatype.startswith('grp16'):
        return GRP16_Fcst
    if datatype.startswith('grp18'):
        return GRP18_Fcst
    if datatype.startswith('grp20'):
        return GRP20_Fcst
    raise ValueError("Type de données incorrect: "
                     "impossible de définir la version de grp")


def set_reader(datatype):
    """Définir le lecteur avancé selon la version de GRP"""
    if datatype.startswith('grp16'):
        return read_GRP16
    if datatype.startswith('grp18'):
        return read_GRP18
    if datatype.startswith('grp20'):
        return read_GRP20
    raise ValueError("Type de données incorrect: "
                     "impossible de définir la version de grp")
