#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - Documentation rst
"""
import os
from pyspc.core.config import Config


def init_examples(filename=None, test=None):
    """
    Initialiser le fichier listant les examples au format rst

    Parameters
    ----------
    filename : str
        Nom du fichier
    test : bool, None
        Ecrire le fichier ?

    """
    if test:
        with open(filename, 'w', encoding='utf-8', newline='\r\n') as f:
            # Description de l'exemple
            f.write('\n')
            f.write('Exemples\n')
            f.write('--------\n')
            f.write('\n')
            f.write('.. note:: Les exemples sont issus des tests unitaires.\n')
            f.write('\n')
            f.write('\n')


def append_examples(filename=None, cline=None, label=None, test=None,
                    config=None, figure=None, stations_list_file=None):
    """
    Ajouter un exemple dans le fichier listant les exemples au format rst

    Parameters
    ----------
    filename : str
        Nom du fichier
    cline : str, None
        Ligne de commande de l'exemple
    label : str, None
        Description de l'exemple
    test : bool, None
        Ecrire le fichier ?
    config : str, None
        Fichier de configuration de l'exemple
    figure : str, None
        Fichier image de l'exemple
    stations_list_file : str, None
        Fichier de liste de stations

    """
    if not isinstance(filename, str):
        return None
    if not isinstance(cline, str):
        return None
    if not isinstance(label, str):
        return None
    if test is None:
        return None
    # compléter fichier data/_bin/BIN/BIN_example.rst
    with open(filename, 'a', encoding='utf-8', newline='\r\n') as f:
        # Description de l'exemple
        f.write('\n')
        f.write(':blue:`')
        f.write(label.replace('\\', '/').strip())
        f.write('`')
        f.write('\n')
        f.write('\n')
        f.write('.. container:: cmdimg')
        f.write('\n')
        f.write('\n')
        f.write('   .. container:: cmdline')
        f.write('\n')
        f.write('\n')
        f.write('      {}'.format(
            cline.replace('../bin/', '').replace('\\', '/')))
        f.write('\n')
        f.write('\n')
        if config is not None:
            config = Config(filename=config)
            config.read()
            f.write('\n')
            f.write('.. code-block:: cfg\n')
            f.write('\n')
            for s in config:
                f.write('   [{}]\n'.format(s))
                for o in config[s]:
                    f.write('   {} = {}\n'.format(o, config[s][o]))
                f.write('\n')
        if stations_list_file is not None:
            f.write('\n')
            f.write('Fichier de stations : {}'.format(
                stations_list_file.replace(os.sep, '/')))
            f.write('\n')
            f.write('\n')
            f.write('.. code-block:: text\n')
            f.write('\n')
            with open(stations_list_file, 'r', encoding='utf-8') as slf:
                for line in slf.readlines():
                    f.write('   {}\n'.format(line.strip()))
            f.write('\n')
            f.write('\n')
        if figure is not None:
            if isinstance(figure, str):
                figure = [figure]
            for fig in figure:
                f.write('\n')
                f.write('.. figure:: ../_static/{}\n'.format(fig))
                f.write('   :alt: Figure\n')
                f.write('   :figclass: figurecentre\n')
                f.write('\n')
                f.write('   Figure {}\n'.format(fig))
                f.write('\n')
    return None
