#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Parcourir une arborescence de répertoires/fichiers
"""
from datetime import datetime as dt
import os
import os.path
import datetime

C_PATHNAME_LIMIT = 200


def sizeof_fmt(num):
    """
    Convertir la taille du fichier/dossier en multiple de B
    """
    for x in ['  B', ' KB', ' MB', ' GB']:
        if num < 1024.0:
            return '{0:>5.1f}'.format(num) + x
        num /= 1024.0
    return "{0:>5.1f}".format(num) + 'TB'


def get_pathsize(start_path):
    """
    Déterminer la longueur du chemin menant à un fichier/dossier
    """
    total_size = 0
    for dirpath, _, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            try:
                total_size += os.path.getsize(fp)
            except Exception:
                total_size += 0
                if len(fp) > 256:
                    try:
                        print(
                            "Avertissement: longueur du chemin trop long"
                            " ({0:d} '{1:s}'".format(len(fp), fp))
                    except ValueError:
                        pass
                else:
                    try:
                        print("Avertissement: fichier introuvable '{0}'"
                              "".format(fp))
                    except ValueError:
                        pass
    return total_size


def print_params(level, current_path, limit):
    """
    Imprimer les paramètres
    """
    if level != 0:
        return None
    print("Directory          : {0}"
          "".format(os.path.abspath(current_path)))
    print("Depth limit        : {0}"
          "".format(limit))
    print("Execution datetime : {0}"
          "".format(
              datetime.datetime.now().strftime("%Y-%m-%d %H:%M")))
    print("")
    print("{}".format("|--- " + current_path + os.sep))
    return None


def process_folders(folders, prefix_line, current_path, level, limit, size,
                    path_length, oldtime, oldfile, duplicate, basenames,
                    prefix_dot):
    """
    Traiter un dossier
    """
    sum_dir_size = 0
    for c_dir in folders:
        print("{}".format(prefix_line + c_dir + os.sep))
        c_dir_size = view_tree(
            path=os.path.join(current_path, c_dir),
            level=level+1,
            limit=limit,
            size=size,
            path_length=path_length,
            oldtime=oldtime,
            oldfile=oldfile,
            duplicate=duplicate,
            basenames=basenames
        )
        # c_dir_size = get_pathsize(current_path + os.sep + c_dir)
        if size:
            try:
                sum_dir_size += c_dir_size
                c_dir_size_txt = sizeof_fmt(int(c_dir_size))
                print("{0}{1}".format(prefix_dot, c_dir_size_txt))
            except ValueError:
                pass
        else:
            pass
    return sum_dir_size


def process_files(filenames, current_path, path_length,
                  basenames, duplicate, oldfile, oldtime, size, prefix_line):
    """
    Traiter des fichiers
    """
    sum_file_size = 0
    for c_file in filenames:
        c_pathname_size = len(current_path) + 1 + len(c_file)
        # If path_length > C_PATHNAME_LIMIT : only warnings
        if c_pathname_size > C_PATHNAME_LIMIT and path_length:
            print("Warning: PathName limit exceeded. "
                  "{0} > {1}" .format(
                      c_pathname_size, C_PATHNAME_LIMIT))
        # Else: showing metadata of current file
        else:
            # Duplicate file warning
            if c_file in basenames:
                if duplicate:
                    print("Warning: duplicated file. {}"
                          "".format(c_file))
            else:
                basenames.add(c_file)
            # Old file warning
            try:
                c_time = os.path.getmtime(  # Modification time
                    os.path.join(current_path, c_file))
#                            c_time = os.path.getctime(  # Creation time
#                            c_time = os.path.getatime(  # Access time
                c_time = dt.fromtimestamp(c_time)
                if oldfile and c_time < oldtime:
                    print("Warning: old file. {}".format(c_file))
            except ValueError:
                pass
#                c_time = None
            # Incrementation of directory size
            # Print on screen the current file
            if size:
                try:
                    c_file_size = os.lstat(
                        current_path + os.sep + c_file).st_size
                    sum_file_size += c_file_size
                    c_file_size_txt = sizeof_fmt(int(c_file_size))
                    print("{0}{1} {2}".format(
                        prefix_line, c_file_size_txt, c_file))
                except ValueError:
                    c_file_size = None
            else:
                try:
                    print("{0}{1}".format(prefix_line, c_file))
                except ValueError:
                    c_file_size = None
    return sum_file_size, basenames


def view_tree(path='.', level=0, limit=3,
              size=False, path_length=False, duplicate=False,
              oldtime=dt.now(), oldfile=False,
              basenames=None):
    """
    Show Tree of path 'path' if 'level' is lower or equal to 'limit'
    Inputs:
       - path       : (str)  current path name
       - level      : (int)  current path level
       - limit      : (int)  limit of path level to show.
                             Maximum depth of tree.
       - size       : (bool) show size of File/Dir
       - path_length: (bool) show warning on path length
                             exceeding <C_PATHNAME_LIMIT>
       - oldtime    : (dt)   comparison datetime for old file warning
       - oldfile    : (bool) show old files
       - duplicate  : (bool) show duplicated files
       - basenames  : (set)  Basename set of existing files
    Output:
       Print on screen the content of current path
    """
    if basenames is None:
        basenames = set()
    current_path = os.path.normpath(path)
    if os.path.exists(current_path):
        if level < limit:
            # Get Path content
            path_content = next(os.walk(current_path))
            # Define prefix line according to 'level' value
            prefix_line = ''
#            for _ in range(level+1):
#                prefix_line += '|    '
            prefix_line += '|    ' * level
            prefix_line += '|--- '
            prefix_dot = '|    '
#            for _ in range(level+1):
#                prefix_dot += '|    '
            prefix_dot += '|    ' * level
            prefix_dot += '|... '
            # If level == 0, display first line of output on stdout
            print_params(level, current_path, limit)
            # Display content if File
            sum_file_size = 0
            # if len(path_content[2]) > 0:
            if path_content[2]:
                sum_file_size, basenames = process_files(
                    path_content[2], current_path, path_length,
                    basenames, duplicate, oldfile, oldtime,
                    size, prefix_line)

            # Display content if Directory
            sum_dir_size = 0
            # if len(path_content[1]) > 0:
            if path_content[1]:
                process_folders(
                    path_content[1], prefix_line, current_path, level, limit,
                    size, path_length, oldtime, oldfile, duplicate, basenames,
                    prefix_dot)

        # Depth limit exceeded
        else:
            sum_file_size = 0
            if size:
                sum_dir_size = get_pathsize(current_path + os.sep)
            else:
                sum_dir_size = 0
#            if path_length:
#                print("Warning: Depth limit could be too short"
#                      " to check path length")
        # Size of current dir is the sum of size of current files
        # and of child directory
        sum_fd_size = sum_dir_size + sum_file_size
    else:
        sum_fd_size = None

    return sum_fd_size
