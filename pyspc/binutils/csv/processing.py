#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - Traitements spécifiques CSV
"""
from pyspc.io.grp16 import read_GRP16
from pyspc.io.grp18 import read_GRP18
from pyspc.io.grp20 import read_GRP20
from pyspc.io.grp22 import read_GRP22
from pyspc.io.pyspcfile import read_PyspcFile
import pyspc.core.exception as _exception

DATATYPES = ['grp16', 'grp18', 'grp20', 'grp22', 'pyspc']


def read_csvlike(csvtype=None, dirname='.', filename=None,
                 stations=None, varnames=None, simulations=None,
                 runtimes=None, models=None, scens=None, uncerts=None,
                 warning=False):
    """
    Lire les fichiers de type csv (grp16, grp18, pyspcfile)
    """
    if csvtype == 'grp16':
        return read_GRP16(
            datatype='grp16_cal_data', dirname=dirname, filename=filename,
            stations=stations, varnames=varnames, warning=warning
        )
    if csvtype == 'grp18':
        return read_GRP18(
            datatype='grp18_cal_data', dirname=dirname, filename=filename,
            stations=stations, varnames=varnames, warning=warning
        )
    if csvtype == 'grp20':
        return read_GRP20(
            datatype='grp20_cal_data', dirname=dirname, filename=filename,
            stations=stations, varnames=varnames, warning=warning
        )
    if csvtype == 'grp22':
        return read_GRP22(
            datatype='grp22_cal_data', dirname=dirname, filename=filename,
            stations=stations, varnames=varnames, warning=warning
        )
    if csvtype == 'pyspc':
        return read_PyspcFile(
            dirname=dirname, filename=filename,
            stations=stations, varnames=varnames, simulations=simulations,
            runtimes=runtimes, models=models, scens=scens, uncerts=uncerts,
            warning=warning
        )
    raise ValueError("Type de fichier csv incorrect '{}'".format(csvtype))


def write_csvlike(series=None, csvtype=None, dirname='.', overwrite=True,
                  code=None, onefile=False, explicitcolname=False):
    """
    Ecrire les fichiers de type csv (grp16, grp18, pyspcfile)
    """
    _exception.raise_valueerror(series is None, 'Collection incorrecte')
    how = 'replace'
    if overwrite:
        how = 'overwrite'
    if csvtype == 'grp16':
        return series.to_GRP_Data(dirname=dirname, version='2016', how=how)
    if csvtype == 'grp18':
        return series.to_GRP_Data(dirname=dirname, version='2018', how=how)
    if csvtype == 'grp20':
        return series.to_GRP_Data(dirname=dirname, version='2020', how=how)
    if csvtype == 'grp22':
        return series.to_GRP_Data(dirname=dirname, version='2022', how=how)
    if csvtype == 'pyspc':
        return series.to_PyspcFile(
            dirname=dirname, how=how, code=code,
            onefile=onefile, explicitcolname=explicitcolname)
    raise ValueError("Type de fichier csv incorrect '{}'".format(csvtype))
