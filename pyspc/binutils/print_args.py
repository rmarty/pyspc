#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Impression des arguments renseignés par l'utilisateur
"""
import argparse


def print_args(info=None, parser=None, args=None, name=""):
    """
    Impression des arguments renseignés par l'utilisateur

    Parameters
    ----------
    info : dict
        Dictionnaire des descriptions des arguments
    parser : argparse.ArgumentParser
        Analyseur des arguments
    args : argparse.Namespace
        Espace de nommage des arguments
    name : str
        Nom du binaire


    Entrées
    - info      = (dict)
    - parser    = ()
    - args      = ()


    """
    if not args.verbose or not isinstance(info, dict) or \
            not isinstance(parser, argparse.ArgumentParser) or \
            not isinstance(args, argparse.Namespace):
        return None
    print(" -- Exécution du script {}".format(name))
    print("    + Vérification des arguments")
    for k, v in parser.__dict__['_option_string_actions'].items():
        if not k.startswith('--') and k[1] in info:
            print("      - {} : {}".format(
                info[k[1]]['short'], args.__dict__[v.dest]))
        elif not k.startswith('--'):
            for k2 in info:
                if k2.startswith(k[1]):
                    print("      - {} : {}".format(
                        info[k2]['short'], args.__dict__[v.dest]))
    return None
