#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - Traitements spécifiques Plathynes
"""
import collections
import numpy as np
import os.path
import pandas as pnd

import pyspc.core.exception as _exception
from pyspc import read_Plathynes, Serie, Series


def process_project(dirname, basename):
    """
    Lire le projet et renvoyer
    - les informations du projet
    - les lieux du projets
    """
    prj_filename = os.path.join(dirname, basename)
    prj_cfg = read_Plathynes(filename=prj_filename, datatype='project')
    _exception.raise_valueerror(
        'events' not in prj_cfg['project'] or
        len(prj_cfg['project']['events']) == 0,
        "Aucun événement à traiter"
    )
    locations = read_Plathynes(
        filename=prj_filename, datatype='project', asloc=True)
    return prj_cfg, locations


def process_locations(locations, injections_list):
    """
    Définir les stations et injections à partir des lieux
    """
    stations = {}
    injections = {}
    suffix = {}
    stations.setdefault('P', [loc.code
                              for loc in locations.values()
                              if loc.loctype == 'point'])
    stations.setdefault('Q', [loc.code
                              for loc in locations.values()
                              if loc.loctype == 'basin'])
    _exception.raise_valueerror(
        len([y for x in stations for y in stations[x]]) == 0,
        "aucune station à traiter"
    )
    injections = {code: bool(code in injections_list)
                  for code in stations['Q']}
    counter = {True: 1, False: 1}
    for loc in locations.values():
        inj = injections.get(loc.code, None)
        if inj is None:
            continue
        suffix.setdefault(loc.name, '{}'.format(counter[inj]))
        counter[inj] += 1
    return stations, injections, suffix


def events_in_project(project, home):
    """
    Définir les événements du projet
    """
    events = collections.OrderedDict()
    for ev in project['project']['events']:
        event_filename = os.path.join(
            home, 'Ev_{}'.format(ev), '{}.evt'.format(ev))
        evt_cfg = read_Plathynes(filename=event_filename, datatype='event')
        events.setdefault(ev, {})
        events[ev]['first_dtime'] = evt_cfg[ev]['first_dt']
        events[ev]['last_dtime'] = evt_cfg[ev]['last_dt']
        events[ev]['timestep'] = evt_cfg[ev]['input_ts']
        events[ev]['filenames'] = []
        events[ev]['filenames'].extend(evt_cfg[ev]['rrobs_filenames'])
        try:
            events[ev]['filenames'].extend(evt_cfg[ev]['qinj_filenames'])
        except TypeError:
            pass
        events[ev]['filenames'].extend(evt_cfg[ev]['qobs_filenames'])
    return events


def set_nodata_series(codes, first_dt, last_dt, varname):
    """
    Définir une collection de série de données
    ne contenant que des valeurs manquantes
    """
    series = Series(datatype='obs', name='nodata')
    for c in codes:
        df = pnd.DataFrame([np.nan, np.nan], index=[first_dt, last_dt])
        serie = Serie(df, code=c, varname=varname, missing=np.nan)
        series.add(serie)
    return series
