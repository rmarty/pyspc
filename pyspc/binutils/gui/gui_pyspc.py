#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Interface graphique <pyspc>
"""
# Modules Python
import collections
from functools import partial
import os.path

# Module PyQt5 >= 5.6
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import (QAction, QFileDialog, QMainWindow, QMenu,
                             QMenuBar, QToolBar, QToolButton, QTreeWidgetItem)
# Obsolète depuis la version 5.5
# from PyQt5.QtWebKit import QWebSettings
# from PyQt5.QtWebKitWidgets import QWebPage
from PyQt5.QtWebEngineWidgets import QWebEngineSettings

# Modules pyspc
from pyspc import __version__ as lversion
from pyspc import __name__ as lname
from pyspc.binutils.gui.ui.ui_pyspc_qt56 import Ui_qt_main_window
from pyspc.binutils.gui.action import Action
from pyspc.binutils.gui.binary import Binary
from pyspc.binutils.gui.command import Command, BINARIES_DIR, ICONS_DIR
from pyspc.binutils.gui.help import Help, HELP_FILENAMES
from pyspc.binutils.gui.options import Options
from pyspc.binutils.gui.process import Process
from pyspc.binutils.gui.tree import TREE_CONTENT


class MainWindow_pySPC(QMainWindow, Ui_qt_main_window,
                       Action, Binary, Command, Help, Options, Process):
    """
    Interface graphique <pySPC>
    """
    def __init__(self, parent=None):
        """
        Initialisation de l'interface graphique <pySPC>
        """
        # ===============================================================
        #    1-- INITIALISATION INTERFACE GRAPHIQUE
        # ===============================================================
        super().__init__(parent)
        self.setupUi(self)
        self.pyqt_version = (5, 'WebEngine')
        self._fromUtf8 = str
        self._translate = QtCore.QCoreApplication.translate
        self.binary = None
        self.binary_name = None
        self.command = []
        self.command_html = ""
        self.dialog = None
        self.dialog_M = None
        self.dialog_RandU = None
        self._output = ''
        self.process = None
        # ===============================================================
        #    2-- RE-DEFINITION DE LA FENETRE PRINCIPALE
        # ===============================================================
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "icone_pyspc.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(c_icon)
        self.setWindowTitle(
            self._translate(
                "qt_main_window",
                "Module Python SPC [{lib:s}-{version:s} PyQt-{pyqt}]"
                "".format(lib=lname, version=lversion,
                          pyqt='.'.join([str(v)
                                         for v in self.pyqt_version
                                         if v is not None])),
                None))
        # ===============================================================
        #    3-- DEFINITION DE L'ARBORESCENCE DE L'EXPLORATEUR
        # ===============================================================
        self.qt_tree_items = collections.OrderedDict()
        # Entête de l'explorateur
        self.qt_explorer_tree.headerItem().setText(
            0, self._translate("qt_main_window", "Utilitaire", None))
        # Contenu de l'explorateur
        __sortingEnabled = self.qt_explorer_tree.isSortingEnabled()
        sorted_content = sorted(TREE_CONTENT)
        for key in sorted_content:
            self.qt_tree_items[key] = QTreeWidgetItem(
                self.qt_explorer_tree)
            self.qt_tree_items[key].setText(
                0, self._translate("qt_main_window", key, None))
            self._fill_TreeWidget(parent=self.qt_tree_items[key],
                                  content=TREE_CONTENT[key])
        self.qt_explorer_tree.setSortingEnabled(__sortingEnabled)
        # Action lorsque l'utilisateur double-clique sur un item
        #   en plus de l'ouverture du niveau inférieur, s'il existe
        self.qt_explorer_tree.itemDoubleClicked.connect(self.action_explorer)
        # ===============================================================
        #    4-- DEFINITION DE LA BARRE DE MENU
        # ===============================================================
        # ================================
        #       4.1 - CREATION DU MENU
        # ================================
        self.qt_menubar = QMenuBar(self)
        self.qt_menubar.setGeometry(QtCore.QRect(0, 0, 900, 21))
        self.qt_menubar.setObjectName(self._fromUtf8("qt_menubar"))
        # ================================
        #       4.2 - TITRES ET CONSTRUCTION DU MENU
        # ================================
        self.qt_menubar_items = collections.OrderedDict()
        self.qt_menubar_actions = collections.OrderedDict()
        for key in sorted_content:
            self.qt_menubar_items[key] = self.qt_menubar.addMenu(
                self._translate("qt_main_window", key, None))
            self._fill_MenuBar(parent=self.qt_menubar_items[key],
                               content=TREE_CONTENT[key])
        # ================================
        #       4.3 - AFFICHAGE
        # ================================
        self.qt_menubar_display = self.qt_menubar.addMenu(
            self._translate("qt_main_window", "Affichage", None))
        self.qt_menubar_display_explorer = QAction(self)
        self.qt_menubar_display_explorer.setText(
            self._translate("qt_main_window", "Explorateur", None))
        self.qt_menubar_display.addAction(self.qt_menubar_display_explorer)
        self.qt_menubar_display_explorer.triggered.connect(
            self.action_explorer_dock)
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "tree2.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_menubar_display_explorer.setIcon(c_icon)
        # ================================
        #       4.4 - AIDE
        # ================================
        # Menu Aide
        self.qt_menubar_help = self.qt_menubar.addMenu(
            self._translate("qt_main_window", "Aide", None))
        # Aide : ROOT
        self.qt_menubar_help_root = QAction(self)
        self.qt_menubar_help_root.setText(
            self._translate("qt_main_window", "Table des matières", None))
        self.qt_menubar_help.addAction(self.qt_menubar_help_root)
        self.qt_menubar_help_root.triggered.connect(
            partial(self.action_menu_help, HELP_FILENAMES["root"]))
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "index2.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_menubar_help_root.setIcon(c_icon)
        # Aide : DESCRIPTION
        self.qt_menubar_help_faq = QAction(self)
        self.qt_menubar_help_faq.setText(
            self._translate("qt_main_window", "Description", None))
        self.qt_menubar_help.addAction(self.qt_menubar_help_faq)
        self.qt_menubar_help_faq.triggered.connect(
            partial(self.action_menu_help, HELP_FILENAMES["description"]))
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "description.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_menubar_help_faq.setIcon(c_icon)
        # Aide : GUI
        self.qt_menubar_help_gui = QAction(self)
        self.qt_menubar_help_gui.setText(
            self._translate("qt_main_window", "Graphic User Interface", None))
        self.qt_menubar_help.addAction(self.qt_menubar_help_gui)
        self.qt_menubar_help_gui.triggered.connect(
            partial(self.action_menu_help, HELP_FILENAMES["gui"]))
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "gui.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_menubar_help_gui.setIcon(c_icon)
        # Aide : INDEX
        self.qt_menubar_help_index = QAction(self)
        self.qt_menubar_help_index.setText(
            self._translate("qt_main_window",
                            "Présentation des utilitaires", None))
        self.qt_menubar_help.addAction(self.qt_menubar_help_index)
        self.qt_menubar_help_index.triggered.connect(
            partial(self.action_menu_help, HELP_FILENAMES["bin"]))
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "cmd2.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_menubar_help_index.setIcon(c_icon)
        # Aide : API
        self.qt_menubar_help_test = QAction(self)
        self.qt_menubar_help_test.setText(
            self._translate("qt_main_window",
                            "Application Interface Programming", None))
        self.qt_menubar_help.addAction(self.qt_menubar_help_test)
        self.qt_menubar_help_test.triggered.connect(
            partial(self.action_menu_help, HELP_FILENAMES["api"]))
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "api.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_menubar_help_test.setIcon(c_icon)
        # ================================
        #       4.5 - AJOUT DU MENU DANS FENETRE PRINCIPALE
        # ================================
        self.setMenuBar(self.qt_menubar)
        # ===============================================================
        #    5-- DEFINITION DU LABEL AU DESSUS DU TABWIDGET
        # ===============================================================
        self.qt_label_binary.setAutoFillBackground(True)
        self.qt_label_binary.setStyleSheet(
            "QLabel  { background-color: rgba(150, 150, 150, 100); }")
        # ===============================================================
        #    6-- DEFINITION DU TABWIDGET
        # ===============================================================
        # Interception du changement d'onglet
        self.qt_tab_widget.currentChanged.connect(self.action_tab)
        # ================================
        #       6.1 - OPTIONS
        # ================================
        self._init_Args()
        # ================================
        #       6.2 - RUN
        # ================================
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "build.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_tab_run_cmd_pButton.setIcon(c_icon)
        self.qt_tab_run_cmd_pButton.clicked.connect(self.build_cmd)
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "run.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_tab_run_log_pButton.setIcon(c_icon)
        self.qt_tab_run_log_pButton.clicked.connect(self.run_cmd)
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "stop.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_tab_run_stop_pButton.setIcon(c_icon)
        self.qt_tab_run_stop_pButton.setEnabled(False)
        self.qt_tab_run_stop_pButton.clicked.connect(self.abort_cmd)
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "audio_off.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_tab_run_audio_pButton.setIcon(c_icon)
        self.qt_tab_run_audio_pButton.clicked.connect(self.audio_cmd)
        self.audio_status = False
        # ================================
        #       6.3 - HELP
        # ================================
        # URL de l'aide
        self.qt_tab_help_webview_url = QtCore.QUrl(None)
        self.qt_tab_help_webview_css = None
        # Bouton <Précédent>
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "back.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_tab_help_pbutton_back.setIcon(c_icon)
        self.qt_tab_help_pbutton_back.setVisible(False)
        self.qt_tab_help_pbutton_back.clicked.connect(
            self.qt_tab_help_webview.back)
        # Bouton <Navigateur>
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "webbrowser.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_tab_help_pbutton_browser.setIcon(c_icon)
        self.qt_tab_help_pbutton_browser.setVisible(False)
        self.qt_tab_help_pbutton_browser.clicked.connect(
            self.help_in_browser)
        # Bouton <Suivant>
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "forward.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_tab_help_pbutton_forward.setIcon(c_icon)
        self.qt_tab_help_pbutton_forward.setVisible(False)
        self.qt_tab_help_pbutton_forward.clicked.connect(
            self.qt_tab_help_webview.forward)
        # Bouton <Vue initiale>
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "initial.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_tab_help_pbutton_initial.setIcon(c_icon)
        self.qt_tab_help_pbutton_initial.setVisible(False)
        self.qt_tab_help_pbutton_initial.clicked.connect(
            partial(self.help_in_tab, 'initial'))
        # Bouton <Actualiser>
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "reload.png")
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qt_tab_help_pbutton_reload.setIcon(c_icon)
        self.qt_tab_help_pbutton_reload.setVisible(False)
        self.qt_tab_help_pbutton_reload.clicked.connect(
            self.qt_tab_help_webview.reload)
        # Embedded PDF
        if self.pyqt_version == (5, 'WebEngine'):
            self.qt_tab_help_webview.settings().setAttribute(
                QWebEngineSettings.PluginsEnabled, True)
        # ===============================================================
        #    7-- DEFINITION DE LA BARRE D'OUTILS
        # ===============================================================
        # Dictionnaires des barres d'outils
        self.qt_toolbars = collections.OrderedDict()
        # Dictionnaires des Actions créées dans les barres d'outils
        self.qt_toolbars_actions = collections.OrderedDict()
        # Dictionnaires des ToolButtons associées aux Actions
        self.qt_toolbars_buttons = collections.OrderedDict()
        # Dictionnaires des binaires dans les menus des ToolButtons
        self.qt_toolbars_menus = collections.OrderedDict()
        for key in sorted_content:
            self.qt_toolbars[key] = QToolBar(self)
            self.qt_toolbars[key].setObjectName((self._fromUtf8(key)))
            self.addToolBar(QtCore.Qt.TopToolBarArea, self.qt_toolbars[key])
            self._fill_ToolBar(toolbar=self.qt_toolbars[key],
                               content=TREE_CONTENT[key],
                               name=key)

    def get_path(self, arg=None):
        """
        Rechercher le nom d'un dossier, d'un fichier ou le code d'une station
        selon l'identifiant de l'argument et, si besoin, le nom du binaire
        """
        # Argument -c
        if arg == "c":
            # Dossier de recherche par défaut
            target = self.set_default_path(arg)
            # QFileDialog
            if self.binary_name.endswith("Info"):
                target = QFileDialog.getSaveFileName(
                    self, "Sélectionner le fichier", target)
            else:
                target = QFileDialog.getOpenFileName(
                    self, "Sélectionner le fichier", target)
            target = target[0]
            # Ajout dans LineEdit
            self.set_path(arg, target)

        # Argument -d
        elif arg == "d":
            # Dossier de recherche par défaut
            target = self.set_default_path(arg)
            # QDialog
            target = QFileDialog.getOpenFileName(
                self, "Sélectionner le fichier", target)
            target = target[0]
            # Ajout dans -I si parmi les arguments du binaire et si vide
            self.set_path_I(target)
            # Ajout dans LineEdit
            self.set_path(arg, target)

        # Argument -D
        elif arg == "D":
            # Dossier de recherche par défaut
            target = self.set_default_path(arg)
            # QDialog
            target = QFileDialog.getOpenFileName(
                self, "Sélectionner le fichier", target)
            target = target[0]
            # Ajout dans -I si parmi les arguments du binaire et si vide
            self.set_path_I(target)
            # Ajout dans LineEdit
            self.set_path(arg, target)

        # Argument -I
        elif arg == "I":
            # Dossier de recherche par défaut
            target = self.set_default_path(arg)
            # QDialog
            target = QFileDialog.getExistingDirectory(
                self, "Sélectionner le dossier", target)
            # Ajout dans LineEdit
            self.set_path(arg, target)

        # Argument -l
        elif arg == "l":
            # Dossier de recherche par défaut
            target = self.set_default_path(arg)
            # QDialog
            target = QFileDialog.getOpenFileName(
                self, "Sélectionner le fichier", target)
            target = target[0]
            # Ajout dans -I si parmi les arguments du binaire et si vide
            self.set_path_I(target)
            # Ajout dans LineEdit
            self.set_path(arg, target)

        # Argument -O
        elif arg == "O":
            # Dossier de recherche par défaut
            target = self.set_default_path(arg)
            # QDialog
            target = QFileDialog.getExistingDirectory(
                self, "Sélectionner le dossier", target)
            # Ajout dans LineEdit
            self.set_path(arg, target)

        # Argument -s
        elif arg == "s":
            # Dossier de recherche par défaut
            target = self.set_default_path(arg)
            # QDialog
            target = QFileDialog.getOpenFileName(
                self, "Sélectionner le fichier", target)
            target = target[0]
            # Ajout dans -I si parmi les arguments du binaire et si vide
            self.set_path_I(target)
            # Ajout dans LineEdit : Extraction du code de la station
            self.set_path(arg, target)

        # Gestion par défaut
        else:
            # Dossier de recherche par défaut
            target = self.args[arg]["line"].text()
            # QDialog
            target = QFileDialog.getOpenFileName(
                self, "Sélectionner le fichier", target)
            target = target[0]
            # Ajout dans LineEdit
            self.set_path(arg, target)

    def set_default_path(self, arg):
        """
        Définir le chemin par défaut lors d'une recherche d'un fichier
        """
        target = self.args[arg]["line"].text()
        if (self.binary_name.startswith("bdimage") or
                self.binary_name.startswith("bdlamedo") or
                self.binary_name.startswith("phyc")) and target == "":
            target = BINARIES_DIR
        elif self.args["I"]["line"].isVisible() and arg != "I" and \
                self.args["I"]["line"].text() != "" and target == "":
            target = self.args["I"]["line"].text()
        elif self.args["O"]["line"].isVisible() and arg != "O" and \
                self.args["O"]["line"].text() != "" and target == "":
            target = self.args["O"]["line"].text()
        return target

    def set_path_I(self, target):
        """
        Définir le répertoire de l'option -I
        """
        if self.args["I"]["line"].isVisible() and \
                self.args["I"]["line"].text() == "":
            self.args["I"]["line"].setText(os.path.dirname(target))

    def set_path(self, arg, target):
        """
        Définir le chemin de l'option courante
        """
        if arg == "c" and (self.binary_name.startswith("bdimage") or
                           self.binary_name.startswith("bdlamedo") or
                           self.binary_name.startswith("phyc")):
            self.args[arg]["line"].setText(target)
        elif arg == "c" and (self.binary_name.startswith("plot") or
                             self.binary_name.endswith("Info") or
                             self.binary_name.startswith("hydroStat")):
            self.args[arg]["line"].setText(target)
        elif arg == 's' or (arg == 'D' and self.binary_name == "csvInfo"):
            target = os.path.basename(target)
            target = "_".join(target.split("_")[:-1])
            self.args[arg]["line"].setText(target)
        elif arg in ['c', 'd', 'D']:
            self.args[arg]["line"].setText(os.path.basename(target))
        else:  # I, l, O
            self.args[arg]["line"].setText(target)

#        if arg == "c":
#            if self.binary_name.startswith("plot") or \
#                    self.binary_name.startswith("bdimage") or \
#                    self.binary_name.startswith("bdlamedo") or \
#                    self.binary_name.startswith("phyc") or \
#                    self.binary_name.endswith("Info") or \
#                    self.binary_name.startswith("hydroStat"):
#                self.args[arg]["line"].setText(target)
#            else:
#                self.args[arg]["line"].setText(os.path.basename(target))
#        elif arg == "d":
#            self.args[arg]["line"].setText(os.path.basename(target))
#        elif arg == "D":
#            if self.binary_name == "csvInfo":
#                target = os.path.basename(target)
#                target = "_".join(target.split("_")[:-1])
#            else:
#                target = os.path.basename(target)
#            self.args[arg]["line"].setText(target)
#        elif arg == "I":
#            self.args[arg]["line"].setText(target)
#        elif arg == "l":
#            self.args[arg]["line"].setText(target)
#        elif arg == "O":
#            self.args[arg]["line"].setText(target)
#        elif arg == "s":
#            target = os.path.basename(target)
#            target = "_".join(target.split("_")[:-1])
#            self.args[arg]["line"].setText(target)
#        else:  # I, l, O
#            self.args[arg]["line"].setText(target)

    def _fill_MenuBar(self, parent=None, content=None, level=0):
        """
        Remplir l'arborescence du menu
         - parent : Item <parent> du MenuBar
         - content : dictionnaire contenant les méta-données de l'item <parent>
                     et des items <child>
         - level : profondeur dans le menu
        """
        if parent is not None and isinstance(content, dict):
            sorted_k1 = sorted(content)
            for k1 in sorted_k1:
                v1 = content[k1]
                # DESCRIPTION DU PARENT
                if k1 == "title" and v1 is not None:
                    parent.setToolTip(
                        self._translate("qt_main_window", v1, None))
                # ICONE DU PARENT
                elif k1 == "icon" and v1 is not None and level != 0:
                    c_icon = QtGui.QIcon()
                    c_icon_filename = os.path.join(ICONS_DIR, v1)
                    c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
                    c_icon.addPixmap(
                        c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    parent.setIcon(c_icon)
                # ACTION SI BINAIRE DU MODULE <pySPC>
                elif k1.endswith(".py"):
                    # Création action
                    self.qt_menubar_actions[k1] = QAction(self)
                    self.qt_menubar_actions[k1].setText(
                        self._translate("qt_main_window", k1, None))
                    # Ajout Icone
                    if "icon" in v1 and v1["icon"] is not None:
                        c_icon = QtGui.QIcon()
                        c_icon_filename = os.path.join(ICONS_DIR, v1["icon"])
                        c_pixmap = QtGui.QPixmap(
                            self._fromUtf8(c_icon_filename))
                        c_icon.addPixmap(
                            c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
                        self.qt_menubar_actions[k1].setIcon(c_icon)
                    # Ajout Description en info bulle
#                    elif "title" in v1 and v1["title"] is not None:
                    if "title" in v1 and v1["title"] is not None:
                        self.qt_menubar_actions[k1].setToolTip(
                            self._translate(
                                "qt_main_window", v1["title"], None))
                    # Affectation au menu <parent>
                    parent.addAction(self.qt_menubar_actions[k1])
                    # Prise en compte du clic souris
                    self.qt_menubar_actions[k1].triggered.connect(
                        partial(self.action_menu, k1))

                # CONTENU DU PARENT ET DEFINITION <CHILD>
                elif isinstance(v1, dict):
                    self.qt_menubar_items[k1] = parent.addMenu(
                        self._translate("qt_main_window", k1, None))
                    self._fill_MenuBar(parent=self.qt_menubar_items[k1],
                                       content=v1,
                                       level=level+1)

    def _fill_ToolBar(self, toolbar=None, content=None, name='default'):
        """
        Remplir les barres d'outils
         - toolbar : L'objet <ToolBar> à remplir
         - content : dictionnaire contenant les méta-données
        """
        if toolbar is not None and isinstance(content, dict):
            # CONTRÖLE DU CONTENU DE <CONTENT>
            pychilds = sorted(filter(
                lambda x: (x.endswith('.py')), content.keys()))
#           pychilds = sorted([k for k in content.keys() if k.endswith('.py')])
            # SI AU NIVEAU DES BINAIRES
            if pychilds:
                # CREATION DU BOUTON
                self.qt_toolbars_actions.setdefault(name, QAction(self))
                self.qt_toolbars_actions[name].setObjectName(
                    self._fromUtf8(name))
                # CREATION INFO-BULLE
                if 'title' in content:
                    self.qt_toolbars_actions[name].setToolTip(
                         self._fromUtf8(content['title']))
                # AJOUT DE L'ICONE
                if 'icon' in content:
                    c_icon = QtGui.QIcon()
                    c_icon_filename = os.path.join(ICONS_DIR, content["icon"])
                    c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
                    c_icon.addPixmap(
                        c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    self.qt_toolbars_actions[name].setIcon(c_icon)
                # AFFECTATION DU BOUTON A LA BARRE D'OUTILS
                toolbar.addAction(self.qt_toolbars_actions[name])
                # CREATION DU MENU DES BINAIRES
                c_menu = QMenu(self)
                for child in pychilds:
                    c_content = content[child]
                    self.qt_toolbars_menus[child] = QAction(self)
                    self.qt_toolbars_menus[child].setText(
                        self._translate("qt_main_window", child, None))
                    # Ajout Icone
                    if "icon" in c_content and c_content["icon"] is not None:
                        c_icon = QtGui.QIcon()
                        c_icon_filename = os.path.join(
                            ICONS_DIR, c_content["icon"])
                        c_pixmap = QtGui.QPixmap(
                            self._fromUtf8(c_icon_filename))
                        c_icon.addPixmap(
                            c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
                        self.qt_toolbars_menus[child].setIcon(c_icon)
                    # Ajout Description en info bulle
                    if "title" in c_content and c_content["title"] is not None:
                        self.qt_toolbars_menus[child].setToolTip(
                            self._translate(
                                "qt_main_window", c_content["title"], None))
                    c_menu.addAction(self.qt_toolbars_menus[child])
                    self.qt_toolbars_menus[child].triggered.connect(
                        partial(self.action_toolbar, child))
                # RECUPERATION DU QToolButton ASSOCIE A L'ACTION
                associatedwidgets = \
                    self.qt_toolbars_actions[name].associatedWidgets()
                # AJOUT DU MENU DES BINAIRES AU QToolButton
                self.qt_toolbars_buttons[name] = associatedwidgets[-1]
                self.qt_toolbars_buttons[name].setMenu(c_menu)
                self.qt_toolbars_buttons[name].setPopupMode(
                    QToolButton.InstantPopup)
            # SINON POURSUITE DE L'ARBORESCENCE DES BINAIRES
            else:
                sorted_k1 = sorted(filter(
                    lambda x: (x not in ['title', 'icon']), content))
#                sorted_k1 = sorted([k
#                                    for k in content
#                                    if k not in ['title', 'icon']])
                for k1 in sorted_k1:
                    self._fill_ToolBar(
                        toolbar=toolbar, content=content[k1], name=k1)

    def _fill_TreeWidget(self, parent=None, content=None):
        """
        Remplir l'arborescence de l'explorateur
         - parent : Item <parent> du TreeWidget
         - content : dictionnaire contenant les méta-données de l'item <parent>
                     et des items <child>
        """
        if parent is not None and isinstance(content, dict):
            sorted_k1 = sorted(content)
            for k1 in sorted_k1:
                v1 = content[k1]
                # DESCRIPTION DU PARENT
                if k1 == "title" and v1 is not None:
                    parent.setToolTip(
                        0, self._translate("qt_main_window", v1, None))
                # ICONE DU PARENT
                elif k1 == "icon" and v1 is not None:
                    c_icon = QtGui.QIcon()
                    c_icon_filename = os.path.join(ICONS_DIR, v1)
                    c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
                    c_icon.addPixmap(
                        c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    parent.setIcon(0, c_icon)
                # CONTENU DU PARENT ET DEFINITION <CHILD>
                elif isinstance(v1, dict):
                    self.qt_tree_items[k1] = QTreeWidgetItem(parent)
                    self.qt_tree_items[k1].setText(
                        0, self._translate("qt_main_window", k1, None))
                    self._fill_TreeWidget(parent=self.qt_tree_items[k1],
                                          content=v1)

    def _init_Args(self):
        """
        Initialiser la liste des objets utilisés
        pour les arguments des binaires
        """
        # Initialisation du dictionnaire qui contiendra
        # toutes les références vers les objets PyQt
        self.args = {
            'objects': collections.OrderedDict(),
            'gBoxes': collections.OrderedDict()
        }
        # Boucles sur les objets PyQt de l'onglet <self.qt_tab_opt>
        parent_name = self.qt_tab_opt.objectName()
        items = sorted(self.__dict__)
        for item in items:
            # Considérer tous les objets sauf les Layouts et l'objet parent
            if item != parent_name and item.startswith(parent_name) and \
                    not item.endswith("Layout"):
                splitted_item = item.split("_")[3:]
                # Référencement de tous les objets
                self.args['objects'][item] = self.__dict__[item]
                # Référencement des groupBox pour les afficher/cacher
                # selon le binaire cibé par l'utilisateur
                # voir méthode : xxxxxxx
                if item.endswith("gBox"):
                    # Cas CheckBox pour Hydro2/Hydro3
                    if splitted_item[1] == '23':
                        self.args['gBoxes'][splitted_item[1]] = \
                            self.__dict__[item]
                        self.args['gBoxes'][splitted_item[1]].setStyleSheet(
                            "border:0;")
                    # Cas générique
                    else:
                        self.args['gBoxes'][splitted_item[0]] = \
                            self.__dict__[item]
                # Référencement des objets liés à un argument de binaire
                # sous la clé : nom court de l'arguement
                else:
                    if splitted_item[1] not in self.args:
                        self.args[splitted_item[1]] = {}
                    self.args[splitted_item[1]][splitted_item[2]] = \
                        self.__dict__[item]
                    # Cas CheckBox pour Hydro2/Hydro3 : gBox parent est '23'
                    if splitted_item[1] in ['2', '3']:
                        self.args[splitted_item[1]]["gBox"] = '23'
                    # Cas générique
                    else:
                        self.args[splitted_item[1]]["gBox"] = splitted_item[0]
        # Connecter le PushButton au LineEdit correspondant
        for arg in self.args:
            if "pButton" in self.args[arg] and "line" in self.args[arg]:
                self.args[arg]["pButton"].clicked.connect(
                    partial(self.get_path, arg))
        # Connecter la ComboBox de "M"
        self.args["M"]["coBox"].activated[str].connect(
            self.apply_method)
        # Connecter la CheckBox de "U"
        self.args["U"]["chBox"].stateChanged[int].connect(
            self.apply_U)
        # Connecter la CheckBox de "R"
        self.args["R"]["chBox"].stateChanged[int].connect(
            self.apply_R)
        # Rendre tout invisible
        self._setInvisible(widgets=self.args['objects'])

    def _setInvisible(self, widgets=None):
        """
        Cacher les arguments et les groupBox associés
        """
        try:
            for widget in widgets:
                widgets[widget].setVisible(False)
        except AttributeError:
            pass
