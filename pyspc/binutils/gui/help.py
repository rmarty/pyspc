#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - Interface graphique - Aide
"""
# Modules python
import os.path
from PyQt5 import QtCore
import webbrowser

# Modules pyspc
from pyspc.core.config import Config

HELP_CFG_FILENAME = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    "../../../bin/gui.ini")
HELP_CFG = Config(filename=HELP_CFG_FILENAME)
HELP_CFG.read()
if os.path.exists(HELP_CFG['doc']['local']):
    HELP_DIR = os.path.abspath(HELP_CFG['doc']['local'])
else:
    HELP_DIR = HELP_CFG['doc']['rtd']
HELP_FILENAMES = {
    "root": HELP_CFG['doc']['root'],
    "bin": HELP_CFG['doc']['bin'],
    "api": HELP_CFG['doc']['api'],
    "gui": HELP_CFG['doc']['gui'],
    "description": HELP_CFG['doc']['description'],
}


class Help(object):
    """
    Classe spécifique pour l'affichage de l'aide au format HTML
    """
    def __init__(self):
        """
        Instanciation
        """
        self.pyqt_version = None
        self.qt_tab_help_webview_css = None
        self.qt_tab_help_webview_url = None

    def action_menu_help(self, item):
        """
        Ouvrir l'aide dans un navigateur web

        item : action d'aide, activée dans le menu
        """
        webbrowser.open(os.path.join(HELP_DIR, item))

    def help_in_browser(self):
        """
        Charger la page d'aide dans le navigateur par défaut
        """
        empty_urls = [QtCore.QUrl(''), QtCore.QUrl(None)]
        if self.pyqt_version == (5, 'WebEngine'):
            self.help_in_tab_update_url(self.qt_tab_help_webview.url())
        if self.qt_tab_help_webview_url not in empty_urls:
            webbrowser.open(self.qt_tab_help_webview_url.toString())

    def help_in_tab(self, binary_name=None):
        """
        Charger la page d'aide dans le navigateur inclus dans l'onglet AIDE

        Parameters
        ----------
        binary_name : str
            Nom de l'utilitaire (nom du fichier python sans son extension)

        """
        if binary_name == 'initial':
            binary_name = self.binary_name
        if binary_name is not None:
            self.qt_tab_help_webview_url = QtCore.QUrl.fromLocalFile(
                os.path.abspath(os.path.join(HELP_DIR, 'bin',
                                             binary_name+".html")))
            self.qt_tab_help_webview_css = QtCore.QUrl.fromLocalFile(
                os.path.abspath(os.path.join(HELP_DIR, "__style.css")))
            self.qt_tab_help_webview.setProperty("url",
                                                 self.qt_tab_help_webview_url)
            self.qt_tab_help_webview.setProperty("zoomFactor", 0.80)
        else:
            self.qt_tab_help_webview_url = QtCore.QUrl(None)
            self.qt_tab_help_webview.setProperty("url",
                                                 self.qt_tab_help_webview_url)

    def help_in_tab_update_url(self, url):
        """
        Mettre à jour l'url de la page d'aide

        Parameters
        ----------
        url : str
            Url de la page d'aide

        """
        self.qt_tab_help_webview_url = url
        self.qt_tab_help_webview_css = QtCore.QUrl.fromLocalFile(
            os.path.abspath(os.path.join(HELP_DIR, "__style.css")))
        self.qt_tab_help_webview.setProperty("url",
                                             self.qt_tab_help_webview_url)
        self.qt_tab_help_webview.setProperty("zoomFactor", 0.80)
