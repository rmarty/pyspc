#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - Interface graphique - Utilitaires
"""
# Modules Python
import os.path
# Module PyQt5 >= 5.6
from PyQt5 import QtCore
from PyQt5.QtWidgets import QComboBox

# Modules pyspc
from pyspc.binutils.gui.tree import *

QLABEL_COLOR_SELECT = "QLabel  { background-color: rgba(50, 150, 250, 100); }"
QLABEL_COLOR_EMPTY = "QLabel  { background-color: rgba(150, 150, 150, 100); }"


class Binary(object):
    """
    Classe spécifique pour les processus lancés depuis l'interface
    """
    def __init__(self):
        """
        Instanciation
        """
        self.binary = None
        self.binary_name = None
        self.command = None
        self.command_html = None

    def apply_binary(self, binary=None):
        """
        Actions à réaliser lorsque l'utilisateur choisit un binaire

        binary : nom du binaire à considérer
        """
        if self.process_isrunning() or binary is None:
            return None
        self.binary = binary
        self.binary_name = os.path.splitext(binary)[0]
        # =====================================
        #    1-- LABEL AU DESSUS DES ONGLETS
        # =====================================
        binary_desc = ""
        try:
            binary_desc = self.qt_tree_items[binary].toolTip(0)
        except KeyError:
            pass
        binary_label = "<font color='blue'><b>{}</b><br>{}</font>"\
                       "".format(self.binary_name, binary_desc.upper())
        self.qt_label_binary.setText(self._fromUtf8(binary_label))
        self.qt_label_binary.setMargin(5)
        self.qt_label_binary.setAutoFillBackground(True)
        self.qt_label_binary.setStyleSheet(QLABEL_COLOR_SELECT)
        # =====================================
        #    2-- ONGLET OPTIONS
        # =====================================
        self._setInvisible(widgets=self.args['objects'])
        # Récupération du dictionnaire des arguments du binaire:
        #   libpySPC/args/<binaire>.py/INFO
        args = eval(self.binary_name + ".INFO")  # nosec
        for arg in args:
            if arg in self.args:
                for obj_name, obj_ref in self.args[arg].items():
                    # Rendre VISIBLE tous les objets
                    if obj_name == "gBox":
                        self.args["gBoxes"][obj_ref].setVisible(True)
                    elif obj_name == "par":
                        obj_ref = []
                    else:
                        self.apply_binary_all(arg, args, obj_name, obj_ref)
        # Remplir les Combobox
        if "C" in args:
            self.apply_binary_C()
        if "M" in args:
            self.apply_binary_M()
        if "n" in args:
            self.apply_binary_n()
        if "t" in args:
            self.apply_binary_t()
        # =====================================
        #    3-- ONGLET RUN
        # =====================================
        self.command = []
        self.command_html = ""
        self.qt_tab_run_cmd_text.setText(self.command_html)
        self.qt_tab_run_log_text.setHtml(
            '<html>'
            '<head>'
            '<style>'
            '  p { }'
            '  p.output {  }'
            '  p.error { color: red }'
            '  p.state { color: grey }'
            '</style>'
            '</head>'
            '<body></body>'
            '</html>')
        # =====================================
        #    4-- ONGLET AIDE
        # =====================================
        self.qt_tab_help_pbutton_back.setVisible(True)
        self.qt_tab_help_pbutton_browser.setVisible(True)
        self.qt_tab_help_pbutton_forward.setVisible(True)
        self.qt_tab_help_pbutton_initial.setVisible(True)
        self.qt_tab_help_pbutton_reload.setVisible(True)
        self.qt_tab_help_webview.setVisible(True)
        self.help_in_tab(binary_name=self.binary_name)
        return None

    def apply_binary_all(self, arg, args, obj_name, obj_ref):
        """
        Toutes les options
        """
        obj_ref.setVisible(True)
        # M-à-j des labels
        if obj_name in ["lbl", "chBox"]:
            obj_ref.setText(self._translate(
                "qt_main_window", args[arg]["short"], None))
            # Décocher la coche -U
            if arg == 'U':
                obj_ref.setChecked(False)
        # Initialisation des calendriers
        elif obj_name == "cal":
            default_dt = QtCore.QDateTime(2000, 1, 1, 0, 0)
            current_dt = obj_ref.dateTime()
            if current_dt == default_dt:
                default_dt = QtCore.QDateTime.currentDateTime()
                default_dt.setTime(QtCore.QTime.fromString("00:00", "hh:mm"))
            else:
                default_dt = current_dt
            obj_ref.setDateTime(default_dt)
        # Effacer la comboBox si déjà remplie
        elif obj_name == "coBox":
            for i in range(obj_ref.count()-1, -1, -1):
                obj_ref.removeItem(i)
        # Effacer le contenu de la ligne -U
        elif obj_name == "line" and arg == 'U':
            obj_ref.setText(self._translate("qt_main_window", "", None))

    def apply_binary_C(self):
        """
        Option -C
        """
        try:
            csvtypes = eval(self.binary_name + ".LIST_FILETYPES")  # nosec
        except AttributeError:
            csvtypes = []
        for csvtype in csvtypes:
            self.args["C"]["coBox"].addItem(
                self._translate("qt_main_window", csvtype, None))
        self.args["C"]["coBox"].setSizeAdjustPolicy(QComboBox.AdjustToContents)

    def apply_binary_M(self):
        """
        Option -M
        """
        methods = eval(self.binary_name+".METHODS")  # nosec
        self.args["M"]["par"] = []
        self.args["M"]["line"].setText(
            self._translate("qt_main_window", "", None))
        for m in methods:
            self.args["M"]["coBox"].addItem(
                self._translate("qt_main_window", m, None))

    def apply_binary_n(self):
        """
        Option -n
        """
        try:
            varnames = eval(self.binary_name + ".LIST_VARNAMES")  # nosec
        except AttributeError:
            varnames = []
        for varname in varnames:
            self.args["n"]["coBox"].addItem(
                self._translate("qt_main_window", varname, None))

    def apply_binary_t(self):
        """
        Option -t
        """
        try:
            datatypes = eval(self.binary_name + ".LIST_DATATYPES")  # nosec
        except AttributeError:
            datatypes = []
        for datatype in datatypes:
            if isinstance(datatype, (list, tuple)):
                item = ' '.join(datatype)
            else:
                item = datatype
            self.args["t"]["coBox"].addItem(
                self._translate("qt_main_window", item, None))
        self.args["t"]["coBox"].setSizeAdjustPolicy(QComboBox.AdjustToContents)

    def unapply_binary(self):
        """
        Annuler l'affichage du binaire si navigation dans l'explorateur
        """
        if self.process_isrunning():
            return
        # =====================================
        #    1-- LABEL AU DESSUS DES ONGLETS
        # =====================================
        self.qt_label_binary.clear()
        self.qt_label_binary.setStyleSheet(QLABEL_COLOR_EMPTY)
        # =====================================
        #    2-- ONGLET OPTIONS
        # =====================================
        if self.binary_name is not None:
            args = eval(self.binary_name+".INFO")  # nosec
            for arg in args:
                if arg in self.args:
                    for obj_name, _ in self.args[arg].items():
                        # Vider la comboBox si déjà remplie
                        if obj_name == "coBox":
                            for i in range(self.args[arg]["coBox"].count()-1,
                                           -1, -1):
                                self.args[arg][obj_name].removeItem(i)

                        # Vider les LineEdit
                        elif obj_name == "line":
                            self.args[arg][obj_name].setText("")

        self._setInvisible(widgets=self.args['objects'])
        # =====================================
        #    3-- ONGLET RUN
        # =====================================
        # =====================================
        #    4-- ONGLET AIDE
        # =====================================
        self.binary = None
        self.binary_name = None
        self.help_in_tab(binary_name=self.binary_name)
        self.qt_tab_help_webview.setVisible(False)
        self.qt_tab_help_pbutton_back.setVisible(False)
        self.qt_tab_help_pbutton_browser.setVisible(False)
        self.qt_tab_help_pbutton_forward.setVisible(False)
        self.qt_tab_help_pbutton_initial.setVisible(False)
        self.qt_tab_help_pbutton_reload.setVisible(False)
