#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - Interface graphique - Arborescence
"""

from pyspc.binutils.args import (
    bdapbp2json, bdapbpjson2csv, bdimage2xml, bdimagexml2csv,
    comparePeakFlow, cristal2csv, cristal2xmlSandre,
    csv2csv, csv2dbase, csv2grpRT, csv2mf, csv2prv, csvInfo,
    dbase2csv, dbase2plathynes, dbaseCreate, dbaseInfo,
    duplicateGrpRTCfg, duplicateScoresCfg, duplicatePlathynesEvent,
    grpRT2csv, grpRT2prv, grpVerif,
    hydro2csv, hydroExport, hydroStat, hydroportailStats,
    mf2csv, mf2mf, mfOpenWS,
    onlineReport,
    phyc2plathynes, phyc2xml, plathynes2csv,
    plotCsvData, plotGrpVerif, prv2csv,
    xmlSandre2csv, xmlSandreInfo, xmlScores2png
)


TREE_CONTENT = {
    "Données": {
        "title": "Observations et prévisions: "
                 "Cristal, Hydro-2, Météo-France, Vigicrues",
        "icon": "data.png",
        "Bases de données": {
            "title": "Bases locales: SACHA, BAREME, Prévision, RefSPC",
            "icon": "database.png",
            "csv2dbase.py": {"title": csv2dbase.DESCRIPTION,
                             "icon": "python.png"},
            "dbase2csv.py": {"title": dbase2csv.DESCRIPTION,
                             "icon": "python.png"},
            "dbase2plathynes.py": {"title": dbase2plathynes.DESCRIPTION,
                                   "icon": "python.png"},
            "dbaseCreate.py": {"title": dbaseCreate.DESCRIPTION,
                               "icon": "python.png"},
            "dbaseInfo.py": {"title": dbaseInfo.DESCRIPTION,
                             "icon": "python.png"}
        },
        "Documents internet": {
            "title": "Documents en ligne: Hydroclim, Shyreg, Météo-France",
            "icon": "webbrowser.png",
            "onlineReport.py": {"title": onlineReport.DESCRIPTION,
                                "icon": "python.png"},
        },
        "CRISTAL": {
            "title": "Archives Cristal",
            "icon": "cristal.png",
            "cristal2csv.py": {"title": cristal2csv.DESCRIPTION,
                               "icon": "python.png"},
            "cristal2xmlSandre.py": {"title": cristal2xmlSandre.DESCRIPTION,
                                     "icon": "python.png"},
        },
        "Hydro-2": {
            "title": "Données, exports et statistiques Hydro-2",
            "icon": "hydro2.png",
            "hydro2csv.py": {"title": hydro2csv.DESCRIPTION,
                             "icon": "python.png"},
            "hydroExport.py": {"title": hydroExport.DESCRIPTION,
                               "icon": "python.png"},
            "hydroStat.py": {"title": hydroStat.DESCRIPTION,
                             "icon": "python.png"}
        },
        "MeteoFrance": {
            "title": "Données issues de Météo-France",
            "icon": "meteo_france.png",
            "csv2mf.py": {"title": csv2mf.DESCRIPTION,
                          "icon": "python.png"},
            "mf2csv.py": {"title": mf2csv.DESCRIPTION,
                          "icon": "python.png"},
            "mf2mf.py": {"title": mf2mf.DESCRIPTION,
                         "icon": "python.png"},
            "mfOpenWS.py": {"title": mfOpenWS.DESCRIPTION,
                            "icon": "python.png"},
        },
        "Vigicrues": {
            "title": "Bases Vigicrues: "
                     "BdApbp, BdImage, PHyC, Vigicrues",
            "icon": "schapi.png",
            "BdApbp": {
                "title": "Bulletin APBP",
                "icon": "schapi_vector.png",
                "bdapbp2json.py": {"title": bdapbp2json.DESCRIPTION,
                                   "icon": "python.png"},
                "bdapbpjson2csv.py": {"title": bdapbpjson2csv.DESCRIPTION,
                                      "icon": "python.png"},
            },
            "BdImage": {
                "title": "Images Radar, Prévisions en point de grille",
                "icon": "schapi_raster.png",
                "bdimage2xml.py": {"title": bdimage2xml.DESCRIPTION,
                                   "icon": "python.png"},
                "bdimagexml2csv.py": {"title": bdimagexml2csv.DESCRIPTION,
                                      "icon": "python.png"},
            },
            "PHyC": {
                "title": "Observations, Prévisions et Informations PHyC",
                "icon": "schapi_database2.png",
                "hydroportailStats.py": {
                    "title": hydroportailStats.DESCRIPTION,
                    "icon": "python.png"},
                "phyc2xml.py": {"title": phyc2xml.DESCRIPTION,
                                "icon": "python.png"},
                "phyc2plathynes.py": {"title": phyc2plathynes.DESCRIPTION,
                                      "icon": "python.png"},
                "xmlSandre2csv.py": {"title": xmlSandre2csv.DESCRIPTION,
                                     "icon": "python.png"},
                "xmlSandreInfo.py": {"title": xmlSandreInfo.DESCRIPTION,
                                     "icon": "python.png"},
            },
        },
    },
    "Traitement de données": {
        "title": "Traitement de données",
        "icon": "data_processing.png",
        "Traitements": {
            "title": "Traitements: conversion, information",
            "icon": "data_processing2.png",
            "comparePeakFlow.py": {"title": comparePeakFlow.DESCRIPTION,
                                   "icon": "python.png"},
            "csv2csv.py": {"title": csv2csv.DESCRIPTION,
                           "icon": "python.png"},
            "csvInfo.py": {"title": csvInfo.DESCRIPTION,
                           "icon": "python.png"},
        },
        "Image": {
            "title": "Affichage graphique des données",
            "icon": "figure.png",
            "plotCsvData.py": {"title": plotCsvData.DESCRIPTION,
                               "icon": "python.png"}
        },
    },
    "Modélisation Hydrologique": {
        "title": "Modélisation (GRP, Plathynes)",
        "icon": "modhydro.png",
        "GRP": {
            "title": "Vérification des modèles GRP calés",
            "icon": "irstea_verification.png",
            "grpVerif.py": {"title": grpVerif.DESCRIPTION,
                            "icon": "python.png"},
            "plotGrpVerif.py": {"title": plotGrpVerif.DESCRIPTION,
                                "icon": "python.png"},
        },
        "Plathynes": {
            "title": "Import/Export de PLATHYNES",
            "icon": "schapi_plathynes.png",
            "dbase2plathynes.py": {"title": dbase2plathynes.DESCRIPTION,
                                   "icon": "python.png"},
            "duplicatePlathynesEvent.py": {
                "title": duplicatePlathynesEvent.DESCRIPTION,
                "icon": "python.png"},
            "phyc2plathynes.py": {"title": phyc2plathynes.DESCRIPTION,
                                  "icon": "python.png"},
            "plathynes2csv.py": {"title": plathynes2csv.DESCRIPTION,
                                 "icon": "python.png"},
        },
    },
    "Prévision Hydrologique": {
        "title": "Prévision hydrologique (GRP)",
        "icon": "irstea_rtime.png",
        "GRP": {
            "title": "Configuration et données de GRP Temps-Réel",
            "icon": "irstea_rtime_data.png",
            "duplicateGrpRTCfg.py": {"title": duplicateGrpRTCfg.DESCRIPTION,
                                     "icon": "python.png"},
            "csv2grpRT.py": {"title": csv2grpRT.DESCRIPTION,
                             "icon": "python.png"},
            "grpRT2csv.py": {"title": grpRT2csv.DESCRIPTION,
                             "icon": "python.png"},
        },
    },
    "Evaluation": {
        "title": "Évaluation de prévision (OTAMIN, SCORES)",
        "icon": "evaluation.png",
        "OTAMIN": {
            "title": "Préparation des incertitudes de modèle (OTAMIN)",
            "icon": "irstea_otamin.png",
            "csv2prv.py": {"title": csv2prv.DESCRIPTION,
                           "icon": "python.png"},
            "grpRT2prv.py": {"title": grpRT2prv.DESCRIPTION,
                             "icon": "python.png"},
            "prv2csv.py": {"title": prv2csv.DESCRIPTION,
                           "icon": "python.png"},
        },
        "SCORES": {
            "title": "Évaluation de séries hydrologiques par SCORES",
            "icon": "schapi_scores.png",
            "csv2prv.py": {"title": csv2prv.DESCRIPTION,
                           "icon": "python.png"},
            "grpRT2prv.py": {"title": grpRT2prv.DESCRIPTION,
                             "icon": "python.png"},
            "prv2csv.py": {"title": prv2csv.DESCRIPTION,
                           "icon": "python.png"},
            "duplicateScoresCfg.py": {"title": duplicateScoresCfg.DESCRIPTION,
                                      "icon": "python.png"},
            "xmlScores2png.py": {"title": xmlScores2png.DESCRIPTION,
                                 "icon": "python.png"}
        }
    },
}
