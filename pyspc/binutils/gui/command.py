#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - Interface graphique - Ligne commande
"""
# Modules Python
import os.path

# Module PyQt5 >= 5.6
from PyQt5 import QtCore, QtGui

# Modules pyspc
from pyspc.binutils.gui.tree import *

BINARIES_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                            "../../../bin")
ICONS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                         "../../../resources/icons")


class Command(object):
    """
    Classe spécifique pour les processus lancés depuis l'interface
    """
    def __init__(self):
        """
        Instanciation
        """
        self.audio_status = None
        self.process = None
        self.command = None
        self.command_html = None

    def build_cmd(self):
        """
        Construire la ligne de commande à exécuter
        """
        if self.binary is not None:
            self.command_html = "<font color='blue'><b>{}</b></font>"\
                "".format(self.binary)
            binary_dirname = os.path.join(BINARIES_DIR, self.binary)
            binary_dirname = os.path.normpath(binary_dirname)
            self.command = [binary_dirname]
            args = eval(self.binary_name+".INFO")  # nosec
#            if 's' in args and 'l' in args:
#                if self.args['s']['line'].text() != "" and \
#                        self.args['l']['line'].text() != "":
#                    QMessageBox.warning(
#                        self,
#                        "Conflit des options -s/-l",
#                        "L'option -l/--station-list n'est pas permise "
#                        "avec l'option -s/--station-name"
#                        "\n\nVeuillez redéfinir vos options"
#                    )
            for arg in args:
                if arg in self.args:
                    for obj_name, obj_ref in self.args[arg].items():
                        if obj_name == "line" and obj_ref.text() != "":
                            self.build_cmd_line(arg, obj_ref)
                        elif obj_name == "cal":
                            self.build_cmd_cal(arg, obj_ref)
                        elif obj_name == "coBox":
                            self.build_cmd_coBox(arg, obj_ref)
                        elif obj_name == "chBox" and obj_ref.isChecked():
                            self.build_cmd_chBox(arg, obj_ref)
            self.qt_tab_run_cmd_text.setText(self.command_html)

    def build_cmd_cal(self, arg, obj_ref):
        """
        Options Calendar
        """
        dt_val = obj_ref.dateTime().toPyDateTime()
        dt_fmt = "%Y%m%d%H"
        if self.binary_name in ["bdimage2xml", "bdlamedo2json"]:
            dt_fmt = "%Y%m%d%H%M"
        elif self.binary_name in ["onlineReport"]:
            dt_fmt = "%Y%m%d"
        elif self.binary_name in ["csv2mf", "csv2grpRT",
                                  "csvInfo", "plotCsvData", "dbase2csv"]:
            if self.args["n"]["coBox"].currentText().endswith("J"):
                dt_fmt = "%Y%m%d"
        self.command.append("-{}".format(arg[0]))
        self.command.append("{}".format(dt_val.strftime(dt_fmt)))
        self.command_html += " <b>-{}</b> {} ".format(
            arg[0], dt_val.strftime(dt_fmt))

    def build_cmd_chBox(self, arg, obj_ref):
        """
        Options CheckBox
        """
        if arg in ['U', 'R']:
            if self.binary_name == 'xmlScores2png':
                for par in self.args[arg]["par"]:
                    self.command.append("-{}".format(arg[0]))
                    self.command.append("{}".format(par[0]))
                    self.command_html += " <b>-{}</b> {}".format(
                        arg[0], par[0])
            else:
                for par in self.args[arg]["par"]:
                    self.command.append("-{}".format(arg[0]))
                    self.command.append("{}".format(par[0]))
                    self.command.append("{}".format(par[1]))
                    self.command_html += " <b>-{}</b> {} {}".format(
                        arg[0], par[0], par[1])
        else:
            self.command.append("-{}".format(arg[0]))
            self.command_html += " <b>-{}</b> ".format(arg[0])

    def build_cmd_coBox(self, arg, obj_ref):
        """
        Options ComboBox
        """
        self.command.append("-{}".format(arg[0]))
        if arg == 'M':
            self.command.append("{}".format(obj_ref.currentText()))
            self.command_html += " <b>-{}</b> {} ".format(
                arg[0], obj_ref.currentText())
            text = " ".join(self.args[arg]['par'])
            if text:
                self.command.extend(
                    ["{}".format(p) for p in self.args[arg]['par']])
                self.command_html += " {} ".format(text)
        elif arg == 'C':
            self.command.extend(obj_ref.currentText().split(' '))
            self.command_html += " <b>-{}</b> {} ".format(
                arg[0], obj_ref.currentText())
        elif arg == 't' and self.binary_name == "bdimage2xml":
            self.command.extend(obj_ref.currentText().split(' '))
            self.command_html += " <b>-{}</b> {} ".format(
                arg[0], obj_ref.currentText())
        else:
            self.command.append("{}".format(obj_ref.currentText()))
            self.command_html += " <b>-{}</b> {} ".format(
                arg[0], obj_ref.currentText())

    def build_cmd_line(self, arg, obj_ref):
        """
        Options LineEdit
        """
        if arg not in ["M", "U", "R"]:
            self.command.append("-{}".format(arg[0]))
            self.command.append("{}".format(obj_ref.text()))
            self.command_html += " <b>-{}</b> {} ".format(
                arg[0], obj_ref.text())

    def run_cmd(self):
        """
        Lancer le processus lié à la ligne de commande courante
        """
        self.build_cmd()
        self.process_output_clear()
        self.process = QtCore.QProcess(self)
        self.process.error.connect(self.process_error)
        self.process.finished.connect(self.process_finished)
        self.process.readyReadStandardError.connect(self.process_stderr)
        self.process.readyReadStandardOutput.connect(self.process_stdout)
        self.process.stateChanged.connect(self.process_state_changed)
        self.process.start("python", self.command, QtCore.QIODevice.ReadOnly)
        # Changement de l'état des boutons
        self.qt_tab_run_cmd_pButton.setEnabled(
            not self.qt_tab_run_cmd_pButton.isEnabled()
        )
        self.qt_tab_run_log_pButton.setEnabled(
            not self.qt_tab_run_log_pButton.isEnabled()
        )
        self.qt_tab_run_stop_pButton.setEnabled(
            not self.qt_tab_run_stop_pButton.isEnabled()
        )

    def abort_cmd(self):
        """
        Arrêter le processus courant
        """
        try:
            test = self.process.state() == QtCore.QProcess.Running
        except AttributeError:
            pass
        else:
            if test:
                self.process.kill()
                self.process_output(
                    "Arrêt du processus demandé par l'utilisateur".upper(),
                    style='abort'
                )

    def audio_cmd(self):
        """
        Signal sonore (ou non) en fin de processus ?
        """
        self.audio_status = not self.audio_status
        if self.audio_status:
            filename = "audio_on.png"
        else:
            filename = "audio_off.png"
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, filename)
        c_pixmap = QtGui.QPixmap(self._fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap,
                         QtGui.QIcon.Normal,
                         QtGui.QIcon.Off)
        self.qt_tab_run_audio_pButton.setIcon(c_icon)
