#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - Interface graphique - (Sub)process
"""
# Modules Python
import os.path
import winsound

# Module PyQt5 >= 5.6
from PyQt5 import QtCore
from PyQt5.QtWidgets import QMessageBox

AUDIO_DIRNAME = r'C:\Windows\Media'
AUDIO_FILENAMES = {
    'error': 'Windows Error.wav',  # 'Windows Critical Stop.wav'
    'default': 'Windows Notify.wav'
    # 'Windows - Exclamation.wav'  # 'Windows Print complete.wav'
}


class Process(object):
    """
    Classe spécifique pour les processus lancés depuis l'interface
    """
    def __init__(self):
        """
        Instanciation
        """
        self._output = None

    def process_isrunning(self):
        """
        Tester si un processus est en cours
        """
        try:
            test = self.process.state() == QtCore.QProcess.Running
            if test:
                QMessageBox.warning(
                    self,
                    "Processus en cours",
                    "Processus en cours : <font color='blue'><b>{}</b></font>"
                    "".format(os.path.basename(self.command[0]))
                )
            return test
        except AttributeError:
            return False

    def process_error(self, error):
        """
        Gérer les messages d'erreur du processus
        """
        msg = ''
        if error == QtCore.QProcess.FailedToStart:
            msg = ("Le processus n'a pas pu démarrer."
                   " Soit le programme appelé est absent,"
                   " ou vous pouvez avoir des autorisations insuffisantes"
                   " pour invoquer le programme.")
        elif error == QtCore.QProcess.Crashed:
            msg = ("Le processus s'est écrasé peu de temps"
                   " après le début du succès.")
        elif error == QtCore.QProcess.Timedout:
            msg = ("Le dernier appel à une fonction waitFor...() a expiré."
                   " L'état du QProcess est inchangé,"
                   " et vous pouvez essayer d'appeler waitFor ..."
                   " () à nouveau.")
        elif error == QtCore.QProcess.WriteError:
            msg = ("Une erreur s'est produite lors de la tentative "
                   "d'écriture dans le processus."
                   " Par exemple, le processus ne peut pas être "
                   "en cours d'exécution,"
                   " ou il peut avoir fermé son canal d'entrée.")
        elif error == QtCore.QProcess.ReadError:
            msg = ("Une erreur s'est produite lors de la tentative "
                   "de lecture du processus."
                   " Par exemple, le processus peut ne pas être en "
                   "cours d'exécution.")
        elif error == QtCore.QProcess.UnknownError:
            msg = ("Une erreur inconnue s'est produite."
                   " Il s'agit de la valeur de retour par défaut d'erreur ().")
        self.process_output_error(msg)

    def process_finished(self, exit_code, exit_status):
        """
        Gérer la fin du processus
        """
        if exit_status == QtCore.QProcess.NormalExit:
            self.process_output_state(
                "Le processus s'est terminé normalement"
                " avec un code retour = {}".format(exit_code))
            if self.audio_status:
                winsound.PlaySound(
                    os.path.join(
                        AUDIO_DIRNAME,
                        AUDIO_FILENAMES['default']
                    ),
                    winsound.SND_FILENAME
                )
        elif exit_status == QtCore.QProcess.CrashExit:
            self.process_output_state(
                "Le processus s'est terminé prématurément"
                " avec un code retour = {}".format(exit_code))
            if self.audio_status:
                winsound.PlaySound(
                    os.path.join(
                        AUDIO_DIRNAME,
                        AUDIO_FILENAMES['error']
                    ),
                    winsound.SND_FILENAME
                )
        # Changement de l'état des boutons
        self.qt_tab_run_cmd_pButton.setEnabled(
            not self.qt_tab_run_cmd_pButton.isEnabled()
        )
        self.qt_tab_run_log_pButton.setEnabled(
            not self.qt_tab_run_log_pButton.isEnabled()
        )
        self.qt_tab_run_stop_pButton.setEnabled(
            not self.qt_tab_run_stop_pButton.isEnabled()
        )

    def process_stderr(self):
        """
        Gérer la sortie erreur du processus
        """
        self.process_output_error(self.process.readAllStandardError())

    def process_stdout(self):
        """
        Gérer la sortie standard du processus
        """
        self.process_output(self.process.readAllStandardOutput())

    def process_state_changed(self, new_state):
        """
        Gérer le texte du statut du processus
        """
        msg = ''
        if new_state == QtCore.QProcess.Starting:
            msg = ("Le processus commence, "
                   "mais le programme n'a pas encore été invoquée.")
            return
        if new_state == QtCore.QProcess.Running:
            msg = "Le processus a démarré"
        if new_state == QtCore.QProcess.NotRunning:
            # msg = "Le processus n'est pas en cours d'exécution."
            return
        self.process_output_state(msg)

    def process_output(self, s, style=''):
        """
        Mettre en forme le texte à ajouter dans le log
        """
        if isinstance(s, QtCore.QByteArray) and self.pyqt_version[0] == 5:
            s = s.replace(b'\r\n', b'<br>')
            s = s.replace(b'\n', b'<br>')
            s = s.replace(b'\xe0', b'&agrave;')
            s = s.replace(b'\xc0', b'&Agrave;')
            s = s.replace(b'\xe9', b'&eacute;')
            s = s.replace(b'\xc9', b'&Eacute;')
            s = s.replace(b'\xe8', b'&egrave;')
            s = s.replace(b'\xc8', b'&Egrave;')
            s = s.replace(b'\xea', b'&ecirc;')
            s = s.replace(b'\xca', b'&Ecirc;')
            s = s.replace(b'\xf4', b'&ocirc;')
            s = s.replace(b'\xd4', b'&Ocirc;')
            s = s.replace(b'\xf6', b'&ouml;')
            s = s.replace(b'\xd6', b'&Ouml;')
            s = s.replace(b'\xe7', b'&ccedil;')
            s = s.replace(b'\xc7', b'&Ccedil;')
            s = s.replace(b'\xf9', b'&ugrave;')
            s = s.replace(b'\xd9', b'&Ugrave;')
            s = s.replace(b' ', b'&nbsp;')
        if style == '':
            self._output += '<b><pre>{}</pre></b>'.format(s)
        else:
            self._output += '<p class="{}">{}</p>'.format(style, s)
        self.process_output_update()

    def process_output_clear(self):
        """
        Nettoyer le texte du log
        """
        self._output = ''
        self.process_output_update()

    def process_output_error(self, s):
        """
        Renseigner le log avec une information
        sur l'erreur renvoyée par le processus
        """
        self.process_output(s, 'error')

    def process_output_state(self, s):
        """
        Renseigner le log avec une information
        sur l'état du processus
        """
        self.process_output(s, 'state')

    def process_output_update(self):
        """
        Mettre à jour le texte du log
        """
        self.qt_tab_run_log_text.setHtml(
            '<html>'
            '<head>'
            '<style>'
            '  p { }'
            '  p.output {  }'
            '  p.abort { font-weight: bold; color: red }'
            '  p.error { color: red }'
            '  p.state { color: grey }'
            '</style>'
            '</head>'
            '<body>' + self._output + '</body>'
            '</html>')
