#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Interface graphique <pySPC> : boites de dialogue optionnelles
"""
# Modules Python
from functools import partial
import os.path

# Module PyQt5 >= 5.6
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import (
    QComboBox, QDialogButtonBox, QFormLayout, QGridLayout, QHBoxLayout,
    QLabel, QLineEdit, QPushButton, QSizePolicy, QSpacerItem, QVBoxLayout)

# Modules pyspc
from pyspc.binutils.gui.command import ICONS_DIR

_fromUtf8 = str
_translate = QtCore.QCoreApplication.translate


class Dialog_M(object):
    """
    Boite de dialogue pour définir les paramètres des méthodes
    """
    def __init__(self):
        """
        Instanciation
        """
        self.title = None
        self.formLayout = None
        self.verticalLayout = None
        self.lines = None
        self.labels = None
        self.buttonBox = None
        self.pyqt_version = (5, 'WebEngine')

    def setupUi(self, Dialog, params=None, label='', item=''):
        """
        Boite de dialogue pour définir les paramètres des méthodes

        params : libellés des paramètres
        label : libellé de la boite de dialogue
        item : nom de la méthode
        """
        if params is None:
            params = []
        # Initialisation de la boite de dialogue
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(400, 300)
        Dialog.setWindowTitle(
            _translate(
                "Dialog",
                "Définition des paramètres de la méthode {}".format(item),
                None))
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "icone_pyspc.png")
        c_pixmap = QtGui.QPixmap(_fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(c_icon)
        # Layout
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        # Label de titre
        self.title = QLabel(Dialog)
        self.title.setObjectName(_fromUtf8("label"))
        self.title.setText(_translate("Dialog", label, None))
        self.verticalLayout.addWidget(self.title)
        # Form Layout
        self.formLayout = QFormLayout()
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        # Remplissage du formulaire FormLayout selon le contenu de <params>
        self.lines = []
        self.labels = []
        for k, par in enumerate(params):
            # Label
            self.labels.append(QLabel(Dialog))
            self.labels[k].setObjectName(_fromUtf8("label_{}".format(k)))
            self.labels[k].setText(_translate("Dialog", par, None))
            self.formLayout.setWidget(k, QFormLayout.LabelRole,
                                      self.labels[k])
            # LineEdit
            self.lines.append(QLineEdit(Dialog))
            self.lines[k].setObjectName(_fromUtf8("lineEdit_{}".format(k)))
            self.formLayout.setWidget(k, QFormLayout.FieldRole,
                                      self.lines[k])
        # Ajout ds le Layout
        self.verticalLayout.addLayout(self.formLayout)
        # Boutons
        self.buttonBox = QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(
            QDialogButtonBox.Cancel | QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout.addWidget(self.buttonBox)
        # Ajuster la taille de la boite de dialogue
        Dialog.adjustSize()
        # Connexions
        if self.pyqt_version[0] == 5:
            self.buttonBox.accepted.connect(Dialog.accept)
            self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)


class Dialog_RU(object):
    """
    Boite de dialogue des options spécifiques utilisateur
    """
    def __init__(self):
        """
        Instanciation
        """
        self.verticalLayout = None
        self.gridLayout = None
        self.lbl = None
        self.lines = None
        self.coBoxes = None
        self.horizontalLayout = None
        self.buttonBox = None
        self.pushButton = None
        self.pyqt_version = (5, 'WebEngine')

    def setupUi(self, Dialog, elements=None, binary_name=None):
        """
        Boite de dialogue des options spécifiques utilisateur
        """
        if elements is None:
            elements = []
        if binary_name == 'bdimagexml2csv':
            window_title = "Définition des ratios"
            element_lbl = 'Nom du ratio'
            value_lbl = 'Valeur du ratio [0.0 - 1.0]'
        elif binary_name == 'xmlScores2png':
            window_title = "Choix des scores"
            element_lbl = 'Nom du score'
            value_lbl = "Sélectionner ce score\n[oui: oui, Oui, OUI, 1,"\
                " true, True, TRUE]\n[non: autres cas]"
        else:
            window_title = "Définition des éléments à mettre à jour"
            element_lbl = "Elément à mettre à jour"
            value_lbl = "Nouvelle valeur de l'élément"

        # Initialisation de la boite de dialogue
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(400, 150)
        Dialog.setWindowTitle(_translate("Dialog", window_title, None))
        c_icon = QtGui.QIcon()
        c_icon_filename = os.path.join(ICONS_DIR, "icone_pyspc.png")
        c_pixmap = QtGui.QPixmap(_fromUtf8(c_icon_filename))
        c_icon.addPixmap(c_pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(c_icon)
        # Layout
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.gridLayout.setSpacing(2)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        # Entête des colonnes
        self.lbl = []
        self.lbl.append(QLabel(Dialog))
        self.lbl[0].setAlignment(QtCore.Qt.AlignCenter)
        self.lbl[0].setObjectName(_fromUtf8("lbl_0"))
        self.lbl[0].setText(_translate("Dialog", element_lbl, None))
        self.gridLayout.addWidget(self.lbl[0], 0, 0, 1, 1)
        self.lbl.append(QLabel(Dialog))
        self.lbl[1].setAlignment(QtCore.Qt.AlignCenter)
        self.lbl[1].setObjectName(_fromUtf8("lbl_1"))
        self.lbl[1].setText(_translate("Dialog", value_lbl, None))
        self.gridLayout.addWidget(self.lbl[1], 0, 1, 1, 1)
        # Initialisation des lignes du formulaire
        self.coBoxes = []
        self.lines = []
        # Définition de la première ligne
        self.add_element(Dialog, elements=elements)
        # Ajout de la gridLayout ds la verticalLayout
        self.verticalLayout.addLayout(self.gridLayout)
        # Spacer entre gridLayout et boutons
        spacerItem = QSpacerItem(
            20, 20, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        # Layout des boutons
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        # Bouton OK/Cancel de la boite de dialogue
        self.buttonBox = QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(
            QDialogButtonBox.Cancel | QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.horizontalLayout.addWidget(self.buttonBox)
        # Bouton pour ajouter une ligne dans la gridLayout
        self.pushButton = QPushButton(Dialog)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy)
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.setText(_translate("Dialog",
                                           "Ajouter un élément", None))
        self.horizontalLayout.addWidget(self.pushButton)
        # Ajout de la horizontalLayout ds la verticalLayout
        self.verticalLayout.addLayout(self.horizontalLayout)
        # Connexions
        self.pushButton.clicked.connect(
            partial(self.add_element, Dialog, elements))
        if self.pyqt_version[0] == 5:
            self.buttonBox.accepted.connect(Dialog.accept)
            self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def add_element(self, Dialog, elements=None):
        """
        Ajouter un élément optionnel et le spécifier
        """
        if elements is None:
            elements = []
        # ComboBox (colonne 0)
        self.coBoxes.append(QComboBox(Dialog))
        idx = len(self.coBoxes) - 1
        self.coBoxes[idx].setObjectName(_fromUtf8("comboBox_{}".format(idx)))
        for element in elements:
            self.coBoxes[idx].addItem(
                _translate("qt_main_window", element, None))
        self.gridLayout.addWidget(self.coBoxes[idx], idx+1, 0, 1, 1)
        # LineEdit (colonne 0)
        self.lines.append(QLineEdit(Dialog))
        self.lines[idx].setObjectName(_fromUtf8("lineEdit_{}".format(idx)))
        self.gridLayout.addWidget(self.lines[idx], idx+1, 1, 1, 1)
        # Ajuster la taille de la boite de dialogue
        Dialog.adjustSize()
