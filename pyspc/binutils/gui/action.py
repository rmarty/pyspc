#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - Interface graphique - Actions
"""


class Action(object):
    """
    Classe spécifique pour les actions de l'utilisateur
    """
    def __init__(self):
        """
        Instanciation
        """

    def action_explorer(self, item):
        """
        Actions à réaliser quand l'utilisateur choisit
        un binaire dans l'explorateur

        item : action activée dans l'explorateur
        """
        item_title = item.text(0)
        if item_title.endswith(".py"):
            self.apply_binary(binary=item_title)
        else:
            self.unapply_binary()

    def action_explorer_dock(self):
        """
        (Dés)Afficher le dock widget contenant l'explorateur
        """
        self.qt_explorer_dock.setVisible(not self.qt_explorer_dock.isVisible())

    def action_menu(self, item):
        """
        Actions à réaliser quand l'utilisateur choisit
        un binaire dans le menu

        item : action activée dans le menu
        """
        self.apply_binary(binary=item)

    def action_tab(self, idx):
        """
        Actions à réaliser lors d'un changement d'onglet

        idx : rang de l'onglet
        """
        if idx == 0 and self.binary is None:
            self._setInvisible(widgets=self.args['objects'])

    def action_toolbar(self, item=None):
        """
        Binaire Barre Outil => apply binary

        item : action activée dans la barre d'outils
        """
        self.apply_binary(binary=item)

#    def action_toolbar_button(self):
#        """
#        Bouton Barre Outil => afficher menu
#        """
#        toolbar_button = self.sender()
#        toolbar_menu = toolbar_button.menu()
#        toolbar_menu.setVisible(True)
#        QMessageBox.information(
#            None,
#            "1 - TOOLBAR",
#            '<b>Action </b> - <font color="blue">{}</font>'
#            ''.format(toolbar_button.objectName())
#        )
