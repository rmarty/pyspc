#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - Interface graphique - User options
"""
# Modules Python
from functools import partial

# Module PyQt5 >= 5.6
from PyQt5 import QtCore
from PyQt5.QtWidgets import QDialog

# Modules pyspc
from pyspc.binutils.gui.gui_dialogs import Dialog_M, Dialog_RU
from pyspc.binutils.gui.tree import *


class Options(object):
    """
    Classe spécifique pour les options de l'utilisateur
    """
    def __init__(self):
        """
        Instanciation
        """
        self._translate = None
        self.dialog_RandU = None
        self.dialog_M = None
        self.dialog = None

    def apply_method(self, item):
        """
        Actions à réaliser lorsque l'utilisateur choisit
        la méthode à appliquer

        item : méthode sélectionnée
        """
        args = eval(self.binary_name+".INFO")  # nosec
        methods = eval(self.binary_name+".METHODS")  # nosec
        text = self._translate(
            "qt_main_window",
            args["M"]["short"] + " : " + methods[item]['short'],
            None)
        self.args["M"]["lbl"].setText(text)
        self.args["M"]["line"].setText(
            self._translate("qt_main_window", "", None))
        self.args["M"]["par"] = []
        if methods[item]['longparam'] is not None:
            self.dialog_method(
                params=methods[item]['longparam'],
                label=methods[item]['help'],
                item=item)

    def dialog_method(self, params=None, label='', item=''):
        """
        Boite de dialogue pour définir les paramètres
        correspondant à la méthode sélectionnée

        params : libellés des paramètres
        label : libellé de la boite de dialogue
        item : nom de la méthode
        """
        if params is None:
            params = []
        self.dialog = QDialog()
        self.dialog_M = Dialog_M()
        self.dialog_M.setupUi(self.dialog, params=params,
                              label=label, item=item)
        if self.pyqt_version[0] == 5:
            self.dialog_M.buttonBox.accepted.connect(
                partial(self.param_method, self.dialog_M.lines))
            self.dialog_M.buttonBox.rejected.connect(
                partial(self.param_method, self.dialog_M.lines))
        else:
            QtCore.QObject.connect(self.dialog_M.buttonBox,
                                   QtCore.SIGNAL("accepted()"),
                                   )
            QtCore.QObject.connect(self.dialog_M.buttonBox,
                                   QtCore.SIGNAL("rejected()"),
                                   partial(self.param_method,
                                           self.dialog_M.lines))
        self.dialog.exec_()

    def param_method(self, lines):
        """
        Récupération des informations de la boite de dialogue

        lines : informations de la boite de dialogue sous forme de liste de str
        """
        self.args["M"]["par"] = [line.text() for line in lines]
        text = " ".join(self.args["M"]["par"])
        if text != "":
            self.args["M"]["line"].setText(text)

    def apply_R(self):
        """
        Actions à réaliser lorsque l'utilisateur choisit
        (dé)coche la case "R"
        """
        if self.args["R"]["chBox"].isChecked():
            elements = []
            if self.binary_name == "bdimagexml2grp":
                elements = ['image', self.args["M"]["coBox"].currentText()]
            self.args["R"]["par"] = []
            self.dialog_RU(elements=elements, arg="R")
        else:
            self.args["R"]["line"].setText(
                self._translate("qt_main_window", "", None))

    def apply_U(self):
        """
        Actions à réaliser lorsque l'utilisateur choisit
        (dé)coche la case "U"
        """
        if self.args["U"]["chBox"].isChecked():
            try:
                elements = eval(self.binary_name + ".LIST_UTYPES")  # nosec
            except NameError:
                elements = []
            self.args["U"]["par"] = []
            self.dialog_RU(elements=elements, arg="U")
        else:
            self.args["U"]["line"].setText(
                self._translate("qt_main_window", "", None))

    def dialog_RU(self, elements=None, arg="U"):
        """
        Boite de dialogue pour définir les paramètres
        à mettre à jour
        """
        if elements is None:
            elements = []
        self.dialog = QDialog()
        self.dialog_RandU = Dialog_RU()
        self.dialog_RandU.setupUi(self.dialog,
                                  elements=elements,
                                  binary_name=self.binary_name)
        if self.pyqt_version[0] == 5:
            self.dialog_RandU.buttonBox.accepted.connect(
                partial(self.param_RU,
                        arg,
                        self.dialog_RandU.coBoxes,
                        self.dialog_RandU.lines))
            self.dialog_RandU.buttonBox.rejected.connect(
                 partial(self.args[arg]["chBox"].setCheckState,
                         QtCore.Qt.Unchecked))
        else:
            QtCore.QObject.connect(self.dialog_RandU.buttonBox,
                                   QtCore.SIGNAL("accepted()"),
                                   partial(self.param_RU,
                                           arg,
                                           self.dialog_RandU.coBoxes,
                                           self.dialog_RandU.lines))
            QtCore.QObject.connect(self.dialog_RandU.buttonBox,
                                   QtCore.SIGNAL("rejected()"),
                                   partial(self.args[arg]["chBox"]
                                           .setCheckState,
                                           QtCore.Qt.Unchecked))
        self.dialog.exec_()

    def param_RU(self, arg, coBoxes=None, lines=None):
        """
        Récupération des informations de la boite de dialogue
        """
        if coBoxes is None:
            coBoxes = []
        if lines is None:
            lines = []
        if coBoxes and lines:
            self.args[arg]["par"] = [(box.currentText(), line.text())
                                     for box, line in zip(coBoxes, lines)
                                     if line.text() != ""]
            if self.args[arg]["par"]:
                if self.binary_name == 'xmlScores2png':
                    self.args[arg]["par"] = [
                        (p[0], '1')
                        for p in self.args[arg]["par"]
                        if p[1].lower() in ['oui', '1', 'true']
                    ]
                text = ";".join(["{}:{}".format(p[0], p[1])
                                 for p in self.args[arg]["par"]])
                self.args[arg]["line"].setText(
                    self._translate("qt_main_window", text, None))
            else:
                self.args[arg]["chBox"].setCheckState(QtCore.Qt.Unchecked)
        else:
            self.args[arg]["chBox"].setCheckState(QtCore.Qt.Unchecked)
