#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - Traitements - csvInfo
"""
import os.path

from pyspc.binutils.csv import read_csvlike
import pyspc.core.exception as _exception
from pyspc import Parameter


def apply_diff(series, options, export):
    """COMPARAISON SERIES AVEC UNE REFERENCE"""
    _exception.Information(
        options.verbose, "    + Différence de contenu de 2 séries")

    if export is None:
        export = 'screen'

    try:
        refloc = options.processing_method[1]
    except IndexError:
        refloc = next(iter(series.keys()))[0]
    _exception.Information(
        options.verbose, "      - Référence Identifiant : {}", refloc)

    try:
        refdir = float(options.processing_method[2])
    except (ValueError, IndexError):
        refdir = options.input_dir
    _exception.Information(
        options.verbose, "      - Référence Répertoire : {}", refdir)

    refseries = read_csvlike(
        stations=refloc,
        varnames=options.varname,
        csvtype=options.csv_type,
        dirname=refdir,
        warning=options.warning
    )
    refkey = next(iter(refseries.keys()))
    refserie = refseries[refkey]

    # Différence avec la référence
    for key in series.keys():
        if key == refkey:
            continue
        refserie.comp(series[key], media=export)


def apply_evt(series, options, export):
    """EVENEMENTS"""
    _exception.Information(
        options.verbose, "    + Evénements")

    ts = Parameter(varname=options.varname).timestep

    try:
        threshold = float(options.processing_method[1])
    except ValueError as ve:
        raise ValueError(
            "Le seuil '{}' ne peut être converti en float. "
            "Les événements ne sont pas définis."
            "".format(options.processing_method[1])
        ) from ve
    _exception.Information(
        options.verbose, "      - Seuil : {}", str(threshold))

    try:
        engine = options.processing_method[2]
    except IndexError:
        engine = 'basic'
    _exception.Information(
        options.verbose, "      - Algorithme : {}", engine)

    try:
        prominence = float(options.processing_method[3])
    except (ValueError, IndexError):
        prominence = None
    else:
        _exception.Information(
            options.verbose, "      - Scipy Prominence : {}", prominence)

    try:
        width = float(options.processing_method[4])
    except (ValueError, IndexError):
        width = None
    else:
        _exception.Information(
            options.verbose, "      - Scipy Width : {}", width)

    try:
        before = int(float(options.processing_method[5]))
    except (ValueError, IndexError):
        before = None
    else:
        before = before * ts
        _exception.Information(
            options.verbose, "      - Durée avant maxi : {}", before)

    try:
        after = int(float(options.processing_method[6]))
    except (ValueError, IndexError):
        after = None
    else:
        after = after * ts
        _exception.Information(
            options.verbose, "      - Durée après maxi : {}", before)

    if options.output_filename is not None and engine == 'scipy':
        fig_filename = os.path.splitext(options.output_filename)[0] + '.png'
        _exception.Information(
            options.verbose, "      - Fichier image : {}", fig_filename)
    else:
        fig_filename = None

    content = series.events(
        threshold=threshold,
        engine=engine,
        prominence=prominence,
        width=width,
        before=before,
        after=after,
        filename=fig_filename,
        asconfig=True,
        codefrom='serie'
    )
    return print_or_return(export, content)


def apply_max(series, options, export):
    """MAXIMAS"""
    _exception.Information(options.verbose, "    + Maximas annuels")
    content = series.find_annual_max(asconfig=True, codefrom='serie')
    return print_or_return(export, content)


def apply_mvls(series, options, export):
    """COMPLETUDE"""
    _exception.Information(options.verbose,
                           "        + Exploration des données manquantes")
    if options.processing_method[0] in ['mvs', 'missing_value_strip']:
        series = series.stripna()
    content = series.counter_missing(asconfig=True, codefrom='serie')
    return print_or_return(export, content)


def apply_reg(series, options, export):
    """REGIME HYDROLOGIQUE"""
    _exception.Information(options.verbose,
                           "        + Régime hydrologique")
    options.processing_method.pop(0)
    defaults = [
        ('groupby', None), ('freqs', None), ('dayhour', 6),
        ('strict', True), ('ignore_upscale', False),
        ('dirname', options.input_dir), ('boxplot', False), ('fill', False)]
    kwargs = {}
    for d in defaults:
        try:
            v = options.processing_method.pop(0)
            if d[0] in ['ignore_upscale', 'strict', 'boxplot', 'fill']:
                kwargs[d[0]] = bool(
                    v.lower() in ['y', 'yes', 't', 'true', 'o', 'oui'])
            elif d[0] in ['freqs']:
                kwargs[d[0]] = [float(x) for x in v.split(',')]
            elif d[0] in ['dayhour']:
                kwargs[d[0]] = int(float(v))
            else:
                kwargs[d[0]] = v
        except Exception:
            _exception.Warning(
                __file__, "Paramètre '{}' mal ou non défini".format(d[0]))
            kwargs[d[0]] = d[1]
        _exception.Information(
            options.verbose, "          - {} : {}".format(d[0], kwargs[d[0]]))
    d1, d2 = series.regime(**kwargs)
    res = []
    res.extend(d1.values())
    res.extend(d2.values())
    return res


def print_config(content):
    """Impression écran des informations Config"""
    header = content.list_unique_options()
    print(';'.join(header))
    for element in content:
        print(';'.join([str(content[element].get(h, ''))
                        for h in header]))


def print_or_return(export, content):
    """Impression ou renvoi d'une Config"""
    if export is None:
        print_config(content)
        return None
    export.update_config(config=content, strict=False)
    return export
