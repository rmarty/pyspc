#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - Traitements - comparePeakFlow
"""
import collections
from datetime import datetime as dt
import pandas as pnd

import pyspc.core.exception as _exception


def read_events(filename):
    """
    Lecture d'un fichier événement créé par csvInfo
    """
    content = pnd.read_csv(
        filename,
        sep=';',
        index_col=False,
        converters={
            'firstdt': lambda x: dt.strptime(x, '%Y-%m-%d %H:%M:%S'),
            'lastdt': lambda x: dt.strptime(x, '%Y-%m-%d %H:%M:%S'),
            'maxdt': lambda x: dt.strptime(x, '%Y-%m-%d %H:%M:%S'),
        }
    )
    sta = content.code.unique()
    _exception.raise_valueerror(
        len(sta) != 1,
        "Le fichier suivant correspond aucune/plusieurs station(s) : {}"
        "".format(filename)
    )
    content = content.drop(columns=['code', 'id'])
    content = content.set_index(keys='name', drop=True)
    return content, sta[0]


def assoc_peakflows(up, dn, options):
    """
    Associer les pointes de crue amont/aval
    """
    _exception.Information(
        options.verbose, "    + Association des événements")
    assoc = collections.OrderedDict()
    for ui in up.index:
        ups = up.loc[ui, 'firstdt']
        upe = up.loc[ui, 'lastdt']
        upt = up.loc[ui, 'maxdt'].to_pydatetime()
        upm = up.loc[ui, 'max']
        for di in dn.index:
            dns = dn.loc[di, 'firstdt']
            dne = dn.loc[di, 'lastdt']
            dnt = dn.loc[di, 'maxdt'].to_pydatetime()
            dnm = dn.loc[di, 'max']
            if upe >= dns and ups <= dne:
                assoc.setdefault((upt, dnt), (upm, dnm))
#                break
    _exception.Information(
        options.verbose,
        "      - Définition de {} périodes de crue communes", str(len(assoc)))
    x = {k[0]: [k2[1] for k2 in assoc.keys() if k2[0] == k[0]]
         for k in assoc.keys()}
    for k, v in x.items():
        _exception.Information(
            options.verbose and len(v) > 1,
            "        - Pointe amont {} associée à plusieurs pointes aval {}",
            (k, str(v))
        )
    x = {k[1]: [k2[0] for k2 in assoc.keys() if k2[1] == k[1]]
         for k in assoc.keys()}
    for k, v in x.items():
        _exception.Information(
            options.verbose and len(v) > 1,
            "        - Pointe aval {} associée à plusieurs pointes amont {}",
            (k, str(v))
        )
    return assoc
