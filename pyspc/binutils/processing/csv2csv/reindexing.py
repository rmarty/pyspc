#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - csv2csv - Indexation temporelle
"""
# Modules Python
import os

# Module PySPC
from pyspc.core.timeutil import str2dt
import pyspc.core.exception as _exception
from pyspc import Series


def apply_bwd(series, options):
    """EXTRACTION ENTRE 2 DATES"""
    _exception.Information(
        options.verbose, "      - Extraction entre 2 dates")
    return series.between_dates(
        str2dt(options.processing_method[1]),
        str2dt(options.processing_method[2]), inplace=False)


def apply_cat(series, options):
    """CONCATENATION DE SERIES"""
    # Nouveau code
    if len(options.processing_method) == 1 and \
            options.stations_list_file is not None:
        new_code = '{}'.format(os.path.splitext(
            os.path.basename(options.stations_list_file))[0])
    elif len(options.processing_method) > 1:
        new_code = options.processing_method[1]
    else:
        new_code = series.codes[0] + '_concat'
    _exception.Information(
        options.verbose,
        "      - Concatenation de série sous l'identifiant '{}'",
        new_code
    )
    # Série support
    for k, key in enumerate(series.keys()):
        cserie = series[key]
        if k == 0:
            _exception.Information(
                options.verbose, "      - Station support : {}", key[0])
            serie = cserie
            serie.code = new_code
        else:
            _exception.Information(
                options.verbose, "      - Station ajoutée : {}", key[0])
            cserie.code = new_code
            serie.update(cserie, overwrite=options.overwrite)
    new_series = Series(datatype=series.datatype)
    new_series.add(serie=serie)
    return new_series
