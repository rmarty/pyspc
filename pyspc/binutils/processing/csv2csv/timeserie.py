#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - csv2csv - Traitement temporel
"""
# Module PySPC
import pyspc.core.exception as _exception
from pyspc import Series


def apply_fct(series, options):
    """FILL CONSTANT"""
    _exception.Information(
        options.verbose, "      - Remplissage par valeur constante")
    return series.fill_constant(constant=float(options.processing_method[1]))


def apply_fli(series, options):
    """FILL LINEAR INTERPOLATION"""
    _exception.Information(
        options.verbose, "      - Remplissage par interpolation linéaire")
    return series.fill_linear_interpolation()


def apply_lrg(series, options):
    """REGRESSION LINEAIRE AVEC DECALAGE TEMPOREL"""
    # Analyse du facteur d'échelle
    scale_factor = float(options.processing_method[1])
    # Analyse du décalage
    offset = float(options.processing_method[2])
    # Analyse du décalage temporel
    timelag = int(options.processing_method[3])
    _exception.Information(
        options.verbose,
        "      - Régression Linéaire avec décalage temporel : "
        "{} x SERIE(t+{}) + {}",
        (scale_factor, timelag, offset))
    new_series = Series(datatype=series.datatype)
    for serie in series.values():
        new_serie = serie.timelag(
            timelag, inplace=False) * scale_factor + offset
        new_serie.code = serie.code
        if new_serie.spc_varname[0] in ['P', 'Q', 'E']:
            new_serie.above_threshold(threshold=0, inplace=True)
        new_series.add(serie=new_serie)
    return new_series


def apply_tlg(series, options):
    """DECALAGE TEMPOREL"""
    # Analyse de la valeur du décalage temporel
    timelag = int(float(
        options.processing_method[1].replace('"', '').replace("'", "")))
    _exception.Information(
        options.verbose,
        "      - Décalage temporel de {} pas de temps", str(timelag))
    # Application du décalage temporel
    return series.timelag(timelag, inplace=False)
