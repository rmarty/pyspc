#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - csv2csv - Echelle temporelle
"""
# Modules Python
from datetime import timedelta as td

# Module PySPC
import pyspc.core.exception as _exception


def apply_dsc(series, options):
    """DESAGREGATION"""
    _exception.Information(
        options.verbose, "      - Désagrégation des données")
    # Analyse de la valeur de l'heure de départ de la journée
    dayhour = set_dayhour(options.processing_method[2])
    # Analyse du pas de temps cible de l'agrégation
    toparam = options.processing_method[1]
    # Application
    return series.downscale(toparam=toparam, dayhour=dayhour)


def apply_nsc(series, options):
    """REMISE A UNE ECHELLE TRES PROCHE"""
    _exception.Information(
        options.verbose,
        "      - Transformer la série à un pas de temps très proche")
    # Analyse du pas de temps cible de l'agrégation
    toparam = options.processing_method[1]
    # Prise en compte stricte des valeurs manquantes
    strict = options.processing_method[2].lower() in [
        'y', 'Y', 'o', 'O', 't', 'T']
    # Application
    return series.nearlyequalscale(toparam=toparam, strict=strict)


def apply_rsc(series, options):
    """INTERPOLATION LINEAIRE INSTANTANE => HORAIRE"""
    _exception.Information(
        options.verbose,
        "      - Conversion à un pas de temps régulier")
    return series.regularscale()


def apply_shs(series, options):
    """HORAIRE => INFRA-HORAIRE"""
    _exception.Information(
        options.verbose,
        "      - Conversion à un pas de temps infra-horaire")
    # Paramètres
    try:
        ts = int(float((options.processing_method[1])))
    except ValueError as ve:
        raise ValueError(
            "le pas de temps {0} semble non convertible en 'int'"
            "".format(options.processing_method[1])) from ve
    ts = td(seconds=ts)
    try:
        meth = options.processing_method[2]
    except IndexError:
        meth = None
    try:
        methinterp = options.processing_method[3]
    except IndexError:
        methinterp = None
    _exception.Information(
        options.verbose,
        "        - Pas de temps (sec) : {}", ts)
    _exception.Information(
        options.verbose,
        "        - Méthode : {}", meth)
    _exception.Information(
        options.verbose,
        "        - Méthode interpolation : {}", methinterp)
    # Application
    return series.subhourlyscale(ts=ts, how=meth, howinterp=methinterp)


def apply_usc(series, options):
    """AGREGATION"""
    _exception.Information(
        options.verbose, "      - Agrégation à un pas de temps supérieur")
    # Analyse de la valeur de l'heure de départ de la journée
    dayhour = set_dayhour(options.processing_method[3])
    # Analyse du pas de temps cible de l'agrégation
    toparam = options.processing_method[1]
    # Prise en compte stricte des valeurs manquantes
    strict = options.processing_method[2].lower() in [
        'y', 'yes', 'o', 'oui', 't', 'true']
    # Application
    return series.upscale(toparam=toparam, dayhour=dayhour, strict=strict,
                          inplace=False)


def set_dayhour(mode):
    """Définir heure début du jour"""
    dayhour = int(float(mode[0]))  # '00' -> 0, '66' -> 6
    _exception.raise_valueerror(
        dayhour not in [0, 6],
        'Le mode journalier {} est incorrect'.format(mode))
    return dayhour
