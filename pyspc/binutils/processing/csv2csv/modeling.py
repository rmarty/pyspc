#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - csv2csv - Modélisation
"""
# Modules Python
import collections
from datetime import timedelta as td
import numpy as np
import sys

# Module PySPC
from pyspc.core.keyseries import tuple2str
from pyspc.core.timeutil import str2dt
import pyspc.core.exception as _exception
from pyspc import Reservoir, Serie, Series, read_Bareme, read_Sandre


def apply_etp(series, options):
    """EVAPO-TRANSPIRATION"""
    tss = {
        'hourly': td(hours=1),
        'daily': td(days=1),
    }
    _exception.Information(
        options.verbose, "      - Evapo-transpiration")
    # Analyse de la valeur de la latitude
    try:
        latitude = float(options.processing_method[2])
    except ValueError as ve:
        raise ValueError(
            "la latitude {0} semble non convertible en 'float'"
            "".format(options.processing_method[2])) from ve
    _exception.Information(
        options.verbose, "      - Latitude : {}", str(latitude))
    return series.etp_oudin(
        timestep=tss.get(options.processing_method[1], None),
        latitude=latitude)


def apply_f2s(series, options):
    """PREVISIONS VERS SIMULATIONS"""
    _exception.Information(
        options.verbose, "      - Conversion de prévision en simulation")
    # Echéances fixe
    leadtimes = [int(float(x)) for x in options.processing_method[1:]]
    _exception.Information(
        options.verbose, "        + Echéances fixes : {}", str(leadtimes))
    # Association des séries
    table = collections.OrderedDict()
    for k in series:
        key = (k[0], k[1], *k[2][1:])
        table.setdefault(key, [])
        table[key].append(k)
    # Nouvelle collection
    new_series = Series(datatype='obs', name='fcst2sim')
    new_dtvals = collections.OrderedDict()
    for k in table:
        _exception.Information(
            options.verbose, "        + Regroupement des séries : {}", str(k))
        keys = sorted(table[k], key=lambda x: x[2][0])
        for k2 in keys:
            serie = series[k2]
            for x in leadtimes:
                nk = '-'.join([str(x) for x in k[2:]])
                nk = (k[0], k[1], "{0}-{1:04d}".format(nk, x))
                new_dtvals.setdefault(nk, [])
                valid_dt = k2[2][0] + x * serie.timestep
                col = (serie.code, serie.spc_varname)
                try:
                    val = float(
                        serie.data_frame.loc[valid_dt][col])
                except (KeyError, IndexError):
                    val = np.nan
                new_dtvals[nk].append((valid_dt, val))
    for k in new_dtvals:
        serie = Serie(new_dtvals[k], code=tuple2str(k), varname=k[1])
        new_series.add(code=serie.code, serie=serie)
    return new_series


def apply_res(series, options):
    """CONVERSION PAR RESERVOIR"""
    # Chargement du réservoir
    res_filename = options.processing_method[1]
    _exception.Information(
        options.verbose, "      - Fichier Réservoir : {}", res_filename)
    reservoir = Reservoir()
    reservoir.load(filename=res_filename)
    # Autres paramètres
    try:
        tablename = options.processing_method[2]
    except IndexError:
        tablename = next(iter(reservoir.tables))
    _exception.Information(
        options.verbose, "      - Table Barème : {}", tablename)
    try:
        assoc = options.processing_method[3]
    except IndexError:
        assoc = None
    else:
        assoc = dict([tuple(x.split(':')) for x in assoc.split(',')])
    _exception.Information(
        options.verbose,
        "      - Correspondance variable-colonne : {}", str(assoc))
    # Séries converties
    return series.apply_Reservoir(
        reservoir=reservoir, tablename=tablename, assoc=assoc)


def apply_rtc(series, options):
    """RATING CURVE APPLICATION"""
    # -------------------------------------------------------------------------
    # 0- INITIALISATION
    # -------------------------------------------------------------------------
    rtc_filename = options.processing_method[1]
    rtc_curves = None
    # -------------------------------------------------------------------------
    # 1- BAREME
    # -------------------------------------------------------------------------
    if rtc_filename.lower().endswith('mdb'):
        rtc_curves = apply_rtc_bareme(options, rtc_filename, series)
#        try:
#            rtc_code = options.processing_method[2]
#        except IndexError:
#            rtc_code = None
#        _exception.Information(
#            options.verbose and rtc_code is None,
#            "      - Extraction des courbes de tarage de la base BAREME {}",
#            rtc_filename)
#        _exception.Information(
#            options.verbose and rtc_code is not None,
#            "      - Extraction de la courbe '{}' de la base BAREME {}",
#            (rtc_code, rtc_filename))
#        start = min([serie.firstdt for serie in series.values()])
#        end = max([serie.lastdt for serie in series.values()])
#        rtc_curves = read_Bareme(
#            filename=rtc_filename, datatype='ratingcurve', codes=series.codes,
#            code_rtc=rtc_code, first_dt=start, last_dt=end, hydro3=False,
#            warning=options.warning
#        )
    # -------------------------------------------------------------------------
    # 2- PHyC / SANDRE
    # -------------------------------------------------------------------------
    elif rtc_filename.lower().endswith('xml'):
        rtc_curves = apply_rtc_sandre(options, rtc_filename)
#        try:
#            if not options.processing_method[2].lower().endswith('xml'):
#                rtc_code = options.processing_method[2]
#            else:
#                rtc_code = options.processing_method[3]
#        except IndexError:
#            rtc_code = None
#        else:
#            raise NotImplementedError('Cas PHyC + id de la CT')
#        try:
#            if options.processing_method[2].lower().endswith('xml'):
#                lev_filename = options.processing_method[2]
#            else:
#                lev_filename = options.processing_method[3]
#        except IndexError:
#            lev_filename = None
#        _exception.Information(
#            options.verbose and rtc_code is None,
#            "      - Extraction des courbes de tarage du fichier Sandre {}",
#            rtc_filename)
#        _exception.Information(
#            options.verbose and rtc_code is not None,
#            "      - Extraction de la courbe '{}' du fichier Sandre {}",
#            (rtc_code, rtc_filename))
#        _exception.Information(
#            options.verbose and lev_filename is not None,
#            "      - Extraction des courbes de correction du fichier Sandre"
#            " {}", lev_filename)
#        rtc_curves = read_Sandre(filename=rtc_filename,
#                                 datatype='ratingcurve',
#                                 levelcor=lev_filename)
    # -------------------------------------------------------------------------
    # 3- RTC Source inconnue
    # -------------------------------------------------------------------------
    else:
        _exception.raise_valueerror(
            True, "Source des courbes de tarage inconnue", options.warning)
    # -------------------------------------------------------------------------
    # 4- Application RTC
    # -------------------------------------------------------------------------
#    print(rtc_curves)
#    for c in rtc_curves.values():
#        print(c)
    _exception.Information(
        options.verbose,
        "      - Application des courbes de tarage et de correction")
#    print(new_series)
    return series.apply_RatingCurves(curves=rtc_curves)


def apply_rtc_bareme(options, rtc_filename, series):
    """RATING CURVE APPLICATION - BAREME"""
    try:
        rtc_code = options.processing_method[2]
    except IndexError:
        rtc_code = None
    _exception.Information(
        options.verbose and rtc_code is None,
        "      - Extraction des courbes de tarage de la base BAREME {}",
        rtc_filename)
    _exception.Information(
        options.verbose and rtc_code is not None,
        "      - Extraction de la courbe '{}' de la base BAREME {}",
        (rtc_code, rtc_filename))
    return read_Bareme(
        filename=rtc_filename,
        datatype='ratingcurve',
        codes=series.codes,
        code_rtc=rtc_code,
        first_dt=min([serie.firstdt for serie in series.values()]),
        last_dt=max([serie.lastdt for serie in series.values()]),
        hydro3=False,
        warning=options.warning
    )


def apply_rtc_sandre(options, rtc_filename):
    """RATING CURVE APPLICATION - SANDRE"""
    try:
        if not options.processing_method[2].lower().endswith('xml'):
            rtc_code = options.processing_method[2]
        else:
            rtc_code = options.processing_method[3]
    except IndexError:
        rtc_code = None
    else:
        raise NotImplementedError('Cas PHyC + id de la CT')
    try:
        if options.processing_method[2].lower().endswith('xml'):
            lev_filename = options.processing_method[2]
        else:
            lev_filename = options.processing_method[3]
    except IndexError:
        lev_filename = None
    _exception.Information(
        options.verbose and rtc_code is None,
        "      - Extraction des courbes de tarage du fichier Sandre {}",
        rtc_filename)
    _exception.Information(
        options.verbose and rtc_code is not None,
        "      - Extraction de la courbe '{}' du fichier Sandre {}",
        (rtc_code, rtc_filename))
    _exception.Information(
        options.verbose and lev_filename is not None,
        "      - Extraction des courbes de correction du fichier Sandre"
        " {}", lev_filename)
    return read_Sandre(
        filename=rtc_filename,
        datatype='ratingcurve',
        levelcor=lev_filename
    )


def apply_s2f(series, options):
    """SIMULATIONS VERS PREVISION"""
    _exception.Information(
        options.verbose,
        "      - Simulation vers Prévision")
    new_series = Series(datatype='fcst', name='sim2fcst')
    # Paramètres obligatoires
    _exception.Information(
        options.verbose,
        "         + Durée de recalage : {}", options.processing_method[1])
    _exception.Information(
        options.verbose,
        "         + Horizon maximal   : {}", options.processing_method[2])
    error_depth = int(float(options.processing_method[1]))
    max_ltime = int(float(options.processing_method[2]))
    # Paramètres optionnels
    try:
        first_dtime = options.processing_method[3]
    except IndexError:
        first_dtime = None
    else:
        first_dtime = str2dt(first_dtime)
        _exception.Information(
            options.verbose,
            "         + Date du premier run : {}", str(first_dtime))
    try:
        last_dtime = options.processing_method[4]
    except IndexError:
        last_dtime = None
    else:
        last_dtime = str2dt(last_dtime)
        _exception.Information(
            options.verbose,
            "         + Date du dernier run : {}", str(last_dtime))
    # Calcul des prévisions
    for key in series:
        # Ne pas traiter les observations
        if key[2] is None:
            continue
        refkey = (key[0], key[1], None)
        # Ne pas traiter les simulations sans série observation associée
        if refkey not in series:
            _exception.Warning(
                sys.argv[0],
                "Pas de série de référence pour {}. Aucune transformation en "
                "prévision ".format(key)
            )
            continue
        _exception.Information(
            options.verbose,
            "         + Simulation={} / Référence={} / "
            "Premier instant={} / Dernier instant={}",
            (key, refkey, first_dtime, last_dtime))
        fcst = series[key].sim2fcst(
            ref=series[refkey],
            error_depth=error_depth,
            max_ltime=max_ltime,
            first_dtime=first_dtime,
            last_dtime=last_dtime
        )
        new_series.extend(fcst)
#    print(new_series)
#    for s in new_series.values():
#        print(s.code)
    return new_series
