#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Utilitaires pour les binaires de pyspc - csv2csv - Calculs stat.
"""
# Modules Python
import os

# Module PySPC
import pyspc.core.exception as _exception
from pyspc import Series


def apply_pct(series, options):
    """PERCENTILE"""
    _exception.Information(
        options.verbose,
        "      - Percentiles : {}".format(options.processing_method[1]))
    # Détermination des fréquences
    freqs = [int(q) for q in options.processing_method[1].split('-')]
    # Calcul des quantiles
    new_series = series.percentile(freqs=freqs)
    return new_series


def apply_wav(series, options, weights):
    """SOMME PONDEREE"""
    _exception.Information(
        options.verbose,
        "      - Somme pondérée : {} avec la pondération {}",
        list(weights.items()))

    serie = series.weighted_average(weights=weights)
    output_prefix = os.path.splitext(
        os.path.basename(options.stations_list_file))[0].replace('_', '-')
    if len(options.processing_method) > 1:
        output_prefix += '-{}'.format(os.path.splitext(
            os.path.basename(options.processing_method[1])
            )[0].replace('_', '-'))
    serie.code = output_prefix
    new_series = Series(datatype=series.datatype)
    new_series.add(serie=serie)
    return new_series
