#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Evaluation de simulations et prévisions - Projet SCORES - Configuration
"""
from pyspc.core.config import Config as _Config
from pyspc.convention.scores import (
    SECTIONS_OPTIONS, SECTIONS_OPTIONS_2, VERSIONS)
import pyspc.core.exception as _exception


class Config(_Config):
    """
    Classe destinée à traiter la configuration de SCORES-1.3.3

    Attributes
    ----------
    filename : str
        Nom du fichier de configuration

    """
    def __init__(self, filename=None, version=1):
        """
        Initialisation de l'instance Config de SCORES-1.3.3

        Parameters
        ----------
        filename : str
            Nom du fichier de configuration
        version : int
            Version du fichier de configuration. Défaut: 1

        """
        super().__init__(filename=filename)
        self.check_version(version=version)
        self.version = version

    def read(self, encoding='utf-8'):
        """
        Lire un fichier de configuration

        Parameters
        ----------
        encoding : str
            Encodage du fichier de configuration 'utf-8' par défaut

        """
        super().read(encoding=encoding)
        self.check_sections_options()

    def check_sections_options(self):
        """Vérifier les sections/options de Scores"""
        so_in = set(self.list_sections_options())
        so_ctl = set(self.get_cfg_keys(version=self.version))
        test = so_in.difference(so_ctl)
        if test:
            for t in test:
                _exception.Warning(
                    None,
                    "élément de configuration {} superflu".format(t))
        test = so_ctl.difference(so_in)
        _exception.raise_valueerror(
            test,
            "Configuration Scores incmplète pour la version {}. Il manque\n{}"
            "".format(self.version, test))

    def update_config(self, config=None, overwrite=None, strict=None):
        """
        Mettre à jour la configuration à partir d'un dictionnaire

        Parameters
        ----------
        config : dict, Config
            Eléments à mettre à jour. {(section, option) : valeur}
        overwrite : bool
            Forcer l'écriture si existant, par défaut: True
        strict : bool
            Ne considérer que les clés existantes, par défaut: True

        """
        super().update_config(
            config=config, overwrite=overwrite, strict=strict)
        self.check_sections_options()

    @staticmethod
    def check_version(version):
        """Vérifier la version de Scores"""
        _exception.raise_valueerror(version not in VERSIONS,
                                    "Version Scores '{}' non supportée"
                                    "".format(version))

    @classmethod
    def get_cfg_keys(cls, version=1):
        """
        Récupérer les mots clés de la configuration SCORES-1.3.3

        Parameters
        ----------
        version : int
            Version du fichier de configuration. Défaut: 1

        """
        cls.check_version(version=version)
        if version == 1:
            return SECTIONS_OPTIONS
        if version == 2:
            return SECTIONS_OPTIONS_2
        raise NotImplementedError('Cfg Scores pour version = {}'
                                  ''.format(version))
