#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Evaluation de simulations et prévisions - Lecture du csv de Series.errors
"""
import os.path
import pandas as pnd
import pyspc.core.exception as _exception


def read_csv(filenames=None):
    """
    Lire et concatener les résultats de vérification de prévisions

    Parameters
    ----------
    filenames : list
        Fichiers csv

    Returns
    -------
    df : pnd.DataFrame
        Tableau de données

    Examples
    --------
    >>> from pyspc.verification.csv import read_csv
    >>> dirname = os.path.join('data', 'verification', 'csv')
    >>> filenames = [
    ...     os.path.join(dirname, 'K0550010_valid-00.csv'),
    ...     os.path.join(dirname, 'K0550010_valid-10.csv'),
    ...     os.path.join(dirname, 'K0550010_valid-50.csv'),
    ...     os.path.join(dirname, 'K0550010_valid-90.csv'),
    ... ]
    >>> df = read_csv(filenames)
    >>> df
                   00          ...      90
                  10%     50%  ...    f-      max    mean      min      std
    leadtime                   ...
    1         -63.055 -18.556  ... 0.538  134.047   3.160  -66.949   46.077
    2        -132.097 -18.578  ... 0.429  152.621  -2.300 -141.707   66.998
    3        -130.382 -20.711  ... 0.429  225.936  -6.839 -326.609  118.396
    4        -110.882 -20.920  ... 0.357  318.344   1.889 -391.192  146.083
    5         -74.686 -19.928  ... 0.357  427.094  15.129 -421.791  169.250
    6        -145.753 -19.373  ... 0.357  522.625  20.387 -452.833  199.846
    7        -273.403  -8.851  ... 0.357  589.501  27.388 -425.850  228.690
    8        -302.395 -13.862  ... 0.357  630.222  39.963 -366.299  241.349
    9        -254.337 -10.261  ... 0.286  658.951  56.247 -404.313  248.954
    10       -179.607 -13.404  ... 0.286  683.914  66.936 -465.126  261.228
    11       -155.413 -16.380  ... 0.286  699.667  65.553 -500.101  277.983
    12       -247.261 -23.405  ... 0.286  691.893  62.319 -479.882  291.954

    See Also
    --------
    pyspc.Series.errors

    """
    _exception.check_listlike(filenames)
    for f in filenames:
        _exception.check_str(f)
    dfs = []
    for f in filenames:
        b = os.path.basename(f)
        t = b.replace('.csv', '').split('_')[1].split('-')[-1]
        d = pnd.read_csv(f, sep=';', header=0, index_col=0)
        d.columns = pnd.MultiIndex.from_product([d.columns, [t]])
        d = d.swaplevel(axis=1)
        d.sort_index(axis=0, inplace=True)
        dfs.append(d)
    try:
        df = pnd.concat(dfs, axis=1, levels=0).sort_index(axis=1)
    except ValueError:
        df = None
    return df
