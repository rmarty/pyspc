#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - Projet PREMHYCE - Données observées
"""
from datetime import datetime as dt
import os.path
import pandas as pnd
import pyspc.core.exception as _exception

DATE_FORMAT = '%Y%m%d'
"""Format des dates dans les fichiers d'observation PREMHYCE"""


def date_parser(txt):
    """Conversion de date"""
    return dt.strptime(txt, DATE_FORMAT)


class Data():
    """
    Structure de données QMJ pour PREMHYCE

    Attributes
    ----------
    filename : str
        Nom du fichier de données PREMHYCE

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe Data (projet Premhyce)

        Parameters
        ----------
        filename : str
            Nom du fichier de données PREMHYCE

        """
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance Data (projet Premhyce)
        """
        text = """
        *************************************
        ********* PREMHYCE - Data ***********
        *************************************
        *  NOM FICHIER      = {filename}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lecture d'un fichier d'observations QMJ de PREMHYCE

        Returns
        -------
        pandas.DataFrame
            Tableau des données d'observations QMJ de PREMHYCE

        Examples
        --------
        >>> import pyspc.model.premhyce as _model
        >>> f = 'data/model/premhyce/Debits.txt'
        >>> d = _model.Data(filename=f)
        >>> print(d)
        *************************************
        ********* PREMHYCE - Data ***********
        *************************************
        *  NOM FICHIER      = data/model/premhyce/Debits.txt
        *  NOM VARIABLE SPC = QJ
        *************************************

        >>> df = d.read()
        >>> df
            TYP      CODE       DATE  DEBIT(l/s)
        0   DEB  K0253030 2020-06-09        1070
        1   DEB  K0260020 2020-06-09        4340
        2   DEB  K0253030 2020-06-10         984
        3   DEB  K0260020 2020-06-10        5220
        4   DEB  K0253030 2020-06-11         919
        5   DEB  K0260020 2020-06-11        5220
        6   DEB  K0253030 2020-06-12       53500
        7   DEB  K0260020 2020-06-12      207000
        8   DEB  K0253030 2020-06-13       37800
        9   DEB  K0260020 2020-06-13      219000
        10  DEB  K0253030 2020-06-14       13300
        11  DEB  K0260020 2020-06-14       86800

        """
        return pnd.read_csv(
            self.filename,
            sep=';',
            index_col=False,
            engine='python',
            skipfooter=1,
            converters={'DATE': date_parser},
        )

    def write(self, data=None, dirname='.'):
        """
        Ecrire le fichier de données QMJ de PREMHYCE

        Parameters
        ----------
        data : pandas.DataFrame
            Tableau des données d'observations QMJ de PREMHYCE
        dirname : str
            Répertoire du fichier d'observations QMJ de PREMHYCE

        """
        _exception.raise_valueerror(
            not isinstance(data, pnd.DataFrame),
            'Les données doivent être contenues dans un pnd.DataFrame'
        )
        if self.filename is None:
            self.filename = 'Debits.txt'
        self.filename = os.path.join(dirname, self.filename)
        with open(self.filename, 'w', encoding='utf-8', newline='\r\n') as f:
            f.write('TYP;CODE;DATE;DEBIT(l/s)\n')
        data.to_csv(
            self.filename,
            mode='a',
            sep=';',
            header=False,
            index=False,
            date_format=DATE_FORMAT,
            line_terminator=';\r\n'
        )
        with open(self.filename, 'a', encoding='utf-8', newline='\r\n') as f:
            f.write('FIN;OBS;')
