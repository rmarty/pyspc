#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2016 - Prévisions
"""
from datetime import datetime as dt
import os.path
import pandas as pnd

from pyspc.convention.grp16 import DATE_FORMAT


class GRP_Fcst():
    """
    Structure de fichier de prévision produit par GRP (*Calage*)

    Attributes
    ----------
    filename : str
        Nom du fichier de données
    station : str
        Identifiant de la station
    model : str
        Nom du modèle (GRP)
    snow : str
        Avec/Sans module neige
    error : str
        Modèle d'erreur (RNA, TAN)
    ltime : int
        Echeance de prévision
    rainfall : str
        Origine des pluies (P0: plue nulle, PP: pluie parfaite)
    periods : str
        Périodes de calage et d'application

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRP_Fcst

        Parameters
        ----------
        filename : str
            Nom du fichier de données

        """
        self.filename = filename
        if self.filename is not None:
            (self.station, self.model, self.snow, self.error, self.ltime,
             self.rainfall, self.periods) = self.split_basename(self.filename)
        else:
            self.station = None
            self.model = None
            self.snow = None
            self.error = None
            self.ltime = None
            self.rainfall = None
            self.periods = None

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRP_Fcst
        """
        text = """
        *************************************
        *********** GRP 2016 - Fcst *********
        *************************************
        *  NOM FICHIER      = {filename}
        *  CODE STATION     = {station}
        *  MODEL            = {model}
        *  NEIGE            = {snow}
        *  TRAIT. ERREUR    = {error}
        *  HORIZON CALAGE   = {ltime}
        *  SOURCE PRECIP    = {rainfall}
        *  PERIODES         = {periods}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lecture du fichier de prévision produit par GRP (*Calage*)

        Returns
        -------
        pandas.DataFrame
            Tableau des données d'observation de GRP 2016

        Examples
        --------
        >>> from pyspc.model.grp16 import GRP_Fcst
        >>> f = 'data/model/grp16/cal/H_K0114030_GRP_SMN_RNA_006_PP_P1P2.TXT'
        >>> reader = GRP_Fcst(filename=filename)
        >>> df = reader.read()
        >>> df
                                   OBS000  ...        OBS012        PRV012
              DATE                         ...
        2019-11-23 06:00:00          87.5  ...          28.5       34.0956
        2019-11-23 07:00:00         109.0  ...          26.9       31.7594
        2019-11-23 08:00:00         118.0  ...          25.5       28.9885
        2019-11-23 09:00:00         118.0  ...          23.9       26.2405
        2019-11-23 10:00:00          93.7  ...          22.5       23.8233
        2019-11-23 11:00:00          77.0  ...          21.1       22.8651
        2019-11-23 12:00:00          66.7  ...          20.4       22.6630

        [7 rows x 11 columns]

        """
        df = pnd.read_csv(
            self.filename,
            sep=';',
            header=0,
            skiprows=5,
            index_col=0,
            parse_dates=True,
            date_parser=lambda x: dt.strptime(x, DATE_FORMAT)
        )
        return df

    def write(self):
        """
        Ecriture du fichier de prévision produit par GRP (*Calage*)
        """
        raise NotImplementedError

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        de prévision produit par GRP (*Calage*)

        Parameters
        ----------
        filename : str
            Fichier de prévision produit par GRP (*Calage*)

        Returns
        -------
        station : str
            Identifiant de la station
        model : str
            Nom du modèle (GRP)
        snow : str
            Avec/Sans module neige
        error : str
            Modèle d'erreur (RNA, TAN)
        ltime : int
            Echeance de prévision
        rainfall : str
            Origine des pluies (P0: plue nulle, PP: pluie parfaite)
        periods : str
            Périodes de calage et d'application


        Examples
        --------
        >>> from pyspc.model.grp16 import GRP_Fcst
        >>> f = 'data/model/grp16/cal/H_K0114030_GRP_SMN_RNA_006_PP_P1P2.TXT'
        >>> (station, model, snow, error, ltime,
        ...  rainfall, periods) = GRP_Fcst.split_basename(filename=f)
        >>> station
        K0114030
        >>> model
        GRP
        >>> snow
        SMN
        >>> error
        RNA
        >>> ltime
        6
        >>> rainfall
        PP
        >>> periods
        P1P2

        """
        if filename is None:
            return None, None
        basename = os.path.splitext(os.path.basename(filename))[0]
        try:
            [_, station, model, snow, error, ltime, rainfall, periods] = \
                basename.split('_')
        except ValueError as ve:
            raise ValueError("Le nom de fichier ne respecte pas le "
                             "nommage de GRP") from ve
        ltime = int(ltime)
        return station, model, snow, error, ltime, rainfall, periods
