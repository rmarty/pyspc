#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2016 - Observations
"""
from datetime import datetime as dt
import os.path
import pandas as pnd

import pyspc.core.exception as _exception
from pyspc.convention.grp16 import (
    DATE_FORMAT, CAL_DATA_HEADERS, CAL_DATA_VARNAMES)


class GRP_Data():
    """
    Structure de données GRP Data (GRP *Calage*)

    Attributes
    ----------
    filename : str
        Nom du fichier de données
    station : str
        Nom de la station
    varname : str
        Nom de la variable

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRP_Data

        Parameters
        ----------
        filename : str
            Nom du fichier de données

        """
        self.filename = filename
        if self.filename is not None:
            self.station, self.varname = self.split_basename(self.filename)
            self._check_varname(varname=self.varname)
        else:
            self.station = None
            self.varname = None

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRP_Data
        """
        text = """
        *************************************
        *********** GRP 2016 - Data *********
        *************************************
        *  NOM FICHIER      = {filename}
        *  CODE STATION     = {station}
        *  NOM VARIABLE     = {varname}
        *************************************
        """
        return text.format(**vars(self))

    def _check_dataframe(self, df=None, strict=False):
        """
        Contrôler le tableau de données est correctement formaté

        Parameters
        ----------
        df : pandas.DataFrame
            Tableau des données d'observation de GRP 2016
        strict : bool
            Lecture avec vérification de la variable (défaut: False)

        Raises
        ------
        ValueError
            Si la variable n'est pas reconnue par pyspc

        """
        _exception.raise_valueerror(
            df.index.name != CAL_DATA_HEADERS['index'],
            "Entête de colonne mal-formatée : {} != {}"
            "".format(df.index.name, CAL_DATA_HEADERS['index']),
            strict
        )
        _exception.raise_valueerror(
            len([c for c in df.columns if c != CAL_DATA_HEADERS[self.varname]])
            != 0,
            "Entête de colonne mal-formatée : {}"
            "".format(df.columns),
            strict
        )

    def _check_varname(self, varname=None):
        """
        Contrôler la variable

        Parameters
        ----------
        varname : str
            Nom de la variable.

        Raises
        ------
        ValueError
            Si la variable n'est pas reconnue par pyspc

        """
        if varname not in self.get_varnames():
            raise ValueError("Variable mal renseignée")

    def read(self, strict=False):
        """
        Lecture du fichier de données GRP Data (GRP *Calage*)

        Parameters
        ----------
        strict : bool
            Lecture avec vérification de la variable (défaut: False)

        Returns
        -------
        pandas.DataFrame
            Tableau des données d'observation de GRP 2016

        Examples
        --------
        >>> from pyspc.model.grp16 import GRP_Data
        >>> f = 'data/model/grp16/cal/43091005_P.txt'
        >>> reader = GRP_Data(filename=f)
        >>> df = reader.read()
        >>> df
                             P(mm)
        AAAAMMJJHH
        2017-06-13 12:00:00    0.0
        2017-06-13 13:00:00    0.0
        2017-06-13 14:00:00    0.0
        2017-06-13 15:00:00   23.8
        2017-06-13 16:00:00    2.6
        2017-06-13 17:00:00    2.2
        2017-06-13 18:00:00    1.2
        2017-06-13 19:00:00    4.9
        2017-06-13 20:00:00   34.2
        2017-06-13 21:00:00    0.2
        2017-06-13 22:00:00    0.0
        2017-06-13 23:00:00    0.0

        """
        df = pnd.read_csv(
            self.filename,
            sep=';',
            header=0,
            index_col=0,
            na_values=[-99.9, -999.999],
            keep_default_na=True,
            parse_dates=True,
            date_parser=lambda x: dt.strptime(x, DATE_FORMAT)
        )
        self._check_dataframe(df=df, strict=strict)
        return df

    def write(self, data=None, dirname='.', strict=False):
        """
        Ecrire le fichier de données GRP Data (GRP *Calage*)

        Parameters
        ----------
        data : pandas.DataFrame
            Tableau des données d'observation de GRP 2016
        dirname : str
            Répertoire local
        strict : bool
            Lecture avec vérification de la variable (défaut: False)

        """
        _exception.check_dataframe(data)
        self._check_dataframe(df=data, strict=strict)
        if self.filename is None:
            self.filename = self.join_basename(
                station=self.join_basename, varname=self.varname)
        self.filename = os.path.join(dirname, self.filename)
        return data.to_csv(
            self.filename,
            sep=';',
            float_format='%.3f',
            index=True,
            date_format=DATE_FORMAT,
            # na_rep=-99.9,
            line_terminator='\n'
        )

    @classmethod
    def get_varnames(cls):
        """
        Définir le nom de la variable
        """
        return sorted(CAL_DATA_VARNAMES)

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        de données GRP Data (GRP *Calage*)

        Parameters
        ----------
        filename : str
            Fichier de données GRP Data (GRP *Calage*)

        Returns
        -------
        station : str
            Identifiant de la station
        varname : str
            Nom de la variable

        Examples
        --------
        >>> from pyspc.model.grp16 import GRP_Data
        >>> f = 'data/model/grp16/cal/43091005_P.txt'
        >>> [station, varname] = GRP_Data.split_basename(filename=f)
        >>> station
        43091005
        >>> varname
        P

        """
        if filename is None:
            return None, None
        basename = os.path.splitext(os.path.basename(filename))[0]
        try:
            [station, varname] = basename.split('_')
        except ValueError as ve:
            raise ValueError("Le nom de fichier ne respecte pas le "
                             "nommage de GRP : {}".format(basename)) from ve
        return station, varname

    @staticmethod
    def join_basename(station=None, varname=None):
        """
        Définir le nom du fichier de données GRP Data (GRP *Calage*)
        à partir des informations

        Parameters
        ----------
        station : str
            Identifiant de la station
        varname : str
            Nom de la variable

        Returns
        -------
        filename : str
            Fichier de données GRP Data (GRP *Calage*)

        """
        if station is None or varname is None:
            raise ValueError('Définition incorrecte des arguments')
        return '{}_{}.txt'.format(station, varname)
