#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2016 - Evenement
"""
from datetime import datetime as dt
import numpy as np
import os.path
import pandas as pnd
from pyspc.convention.grp16 import CAL_EVENT_DATE_FORMAT


def date_parser(txt):
    """"Convertisseur de date"""
    return dt.strptime(txt, CAL_EVENT_DATE_FORMAT)


class GRP_Event():
    """
    Structure du fichier Evenement de GRP *Calage*

    Attributes
    ----------
    filename : str
        Nom du fichier Evenement de GRP *Calage*
    station : str
        Nom de la station
    event : str
        Identifiant de l'événement

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRP_Event

        Parameters
        ----------
        filename : str
            Nom du fichier Bassin de GRP *Calage*

        """
        super().__init__()
        self.filename = filename
        self.station, self.event = self.split_basename(self.filename)

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRP_Event
        """
        text = """
        *************************************
        *********** GRP 2016 - Event ********
        *************************************
        *  NOM FICHIER      = {filename}
        *  CODE STATION     = {station}
        *  CODE EVENEMENT   = {event}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lecture du fichier Evenement de GRP *Calage*

        Returns
        -------
        pandas.DataFrame
            Tableau des données Evenement de GRP 2016

        Examples
        --------
        >>> from pyspc.model.grp16 import GRP_Event
        >>> f = 'data/model/grp16/cal/K0114030-EV0001.DAT'
        >>> reader = GRP_Event(filename=f)
        >>> df = reader.read()
        >>> df
                          Date  Pluie(mm/h)  Debit(m3/s)
        0  2017-06-13 12:00:00        0.000         0.57
        1  2017-06-13 13:00:00        0.000         0.57
        2  2017-06-13 14:00:00        0.300         0.56
        3  2017-06-13 15:00:00        2.750         0.59
        4  2017-06-13 16:00:00        2.690         0.58
        5  2017-06-13 17:00:00        9.230         0.70
        6  2017-06-13 18:00:00       46.680        11.20
        7  2017-06-13 19:00:00       61.325       121.00
        8  2017-06-13 20:00:00       15.750       203.00
        9  2017-06-13 21:00:00        5.050       159.00
        10 2017-06-13 22:00:00        0.060        92.80
        11 2017-06-13 23:00:00        0.000        53.80
        12 2017-06-14 00:00:00        0.000        38.30
        13 2017-06-14 01:00:00        0.000        28.00
        14 2017-06-14 02:00:00        0.000        22.90
        15 2017-06-14 03:00:00        0.000        20.40
        16 2017-06-14 04:00:00        0.060        18.90
        17 2017-06-14 05:00:00        0.000        17.00
        18 2017-06-14 06:00:00        0.000        15.50
        19 2017-06-14 07:00:00        0.000        14.30
        20 2017-06-14 08:00:00        0.000        13.10
        21 2017-06-14 09:00:00        0.000        12.40
        22 2017-06-14 10:00:00        0.000        11.80
        23 2017-06-14 11:00:00        0.000        11.20
        24 2017-06-14 12:00:00        0.840        10.60

        """
        return pnd.read_fwf(
            self.filename,
            colspecs=[(0, 11), (12, 23), (24, 35)],
            converters={'Date': date_parser},
            na_values=[-99.9]
        )

    def write(self, data=None):
        """
        Ecriture du fichier Evenement de GRP *Calage*
        """
        data['Debit(m3/s)'] = data['Debit(m3/s)'].map(
            lambda x: '{0:10.4f}'.format(x)
            if not np.isnan(x) else '   -99.9000')
        data['Pluie(mm/h)'] = data['Pluie(mm/h)'].map(
            lambda x: '{0:10.4f}'.format(x)
            if not np.isnan(x) else '   -99.9000')
        data['Date       '] = data['Date'].map(
            lambda x: x.strftime(CAL_EVENT_DATE_FORMAT))
        data = data.drop(columns='Date')
        data = data.reindex(
            columns=['Date       ', 'Pluie(mm/h)', 'Debit(m3/s)'])
        with open(self.filename, 'w', encoding='utf-8', newline='\r\n') as f:
            x = '\n'.join([y.strip()
                           for y in data.to_string(
                               header=True, index=False).split('\n')])
            f.write(x)
            f.write('\n')

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        Evenement de GRP *Calage*

        Parameters
        ----------
        filename : str
            Fichier Evenement de GRP *Calage*

        Returns
        -------
        station : str
            Identifiant de la station
        event : str
            Identifiant de l'événement

        Examples
        --------
        >>> from pyspc.model.grp16 import GRP_Event
        >>> f = 'data/model/grp16/cal/K0114030-EV0001.DAT'
        >>> [station, event] = GRP_Event.split_basename(filename=f)
        >>> station
        K0114030
        >>> event
        EV0001

        """
        if filename is None:
            return None, None
        basename = os.path.splitext(os.path.basename(filename))[0]
        try:
            [station, event] = basename.split('-')
        except ValueError as ve:
            raise ValueError("Le nom de fichier ne respecte pas le "
                             "nommage de GRP") from ve
        return station, event
