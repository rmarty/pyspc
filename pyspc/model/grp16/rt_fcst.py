#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2016 - Temps-réel prévision
"""
from datetime import datetime as dt
import os.path
import pandas as pnd

import pyspc.core.exception as _exception
from pyspc.convention.grp16 import (
    DATE_FORMAT,
    RT_FCST_DTYPES, RT_FCST_FILEPREFIX, RT_FCST_LINEPREFIX
)


class GRPRT_Fcst():
    """
    Structure de données GRPRT Fcst (Prévision GRP *Temps Réel*)

    Fichiers

    - GRP_Obs.txt
    - GRP_Simu_2001.txt
    - GRP_Prev_2001.txt
    - GRP_D_Obs.txt
    - GRP_D_Simu_2001.txt
    - GRP_D_Prev_2001.txt

    Attributes
    ----------
    filename : str
        Nom du fichier de prévision
    datatype : str
        Type du fichier de prévision
    fileprefix : str
        Préfixe du nom de base du fichier
    lineprefix : str
        Préfixe des lignes de données du fichier

    """
    def __init__(self, filename=None, datatype=None):
        """
        Initialisation de l'instance de la classe GRPRT_Fcst

        Parameters
        ----------
        filename : str
            Nom du fichier de données
        datatype : str
            Type du fichier de données

        """
        self.check_datatype(datatype=datatype)
        self.datatype = datatype
        self.fileprefix = self.get_fileprefix(datatype=datatype)
        self.lineprefix = self.get_lineprefix(datatype=datatype)
        _exception.raise_valueerror(
            not os.path.basename(filename).startswith(self.fileprefix),
            'Incohérence entre le nom de fichier et le type de fichier'
        )
        self.scen = None
        if self.lineprefix in ['SIM', 'PRV']:
            self.scen = os.path.basename(filename).replace(
                self.fileprefix, '').split('.')[0]
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRPRT_Fcst
        """
        text = """
        *************************************
        *********** GRP 2016 - RT Fcst ******
        *************************************
        *  NOM FICHIER      = {filename}
        *  TYPE FICHIER     = {datatype}
        *  PREFIXE FICHIER  = {fileprefix}
        *  PREFIXE LIGNE    = {lineprefix}
        *  SCENARIO         = {scen}
        *************************************
        """
        return text.format(**vars(self))

    def check_datatype(self, datatype):
        """
        Contrôler le type de fichier
        """
        _exception.raise_valueerror(
            datatype not in self.get_types(),
            'Type de fichier incorrect'
        )

    def read(self):
        """
        Lire le fichier de prévision GRP Temps-Réel / Temps-Différé

        Returns
        -------
        pandas.DataFrame
            Tableau des prévisions de GRP Temps-Réel

        Examples
        --------
        >>> from pyspc.model.grp16 import GRPRT_Fcst
        >>> f = 'data/model/grp16/rt/GRP_D_Obs.txt'
        >>> reader = GRPRT_Fcst(filename=f, datatype='obs_diff')
        >>> df = reader.read()
        >>> df
            TYP        CODE  ...       PLUIE(mm)  Temperature(°C)
        0   OBS    K6173130  ...          2.4533              NaN
        1   OBS    K6173130  ...          2.3867              NaN
        2   OBS    K6173130  ...          1.4400              NaN
        3   OBS    K6173130  ...          0.8067              NaN
        4   OBS    K6173130  ...          1.2000              NaN
        5   OBS    K6173130  ...          0.7067              NaN
        6   OBS    K6173130  ...          0.4800              NaN
        7   OBS    K6173130  ...          0.9067              NaN
        8   OBS    K6173130  ...          0.6533              NaN
        9   OBS    K6173130  ...          0.6533              NaN

        >>> f = 'data/model/grp16/rt/GRP_D_Simu_2001.txt'
        >>> reader = GRPRT_Fcst(filename=f, datatype='sim_diff')
        >>> df = reader.read()
        >>> df
            TYP        CODE  ...       PLUIE(mm)  Temperature(°C)
        0   SIM    K6173130  ...          1.0533              NaN
        1   SIM    K6173130  ...          0.8800              NaN
        2   SIM    K6173130  ...          0.7067              NaN
        3   SIM    K6173130  ...          0.4133              NaN
        4   SIM    K6173130  ...          0.3467              NaN
        5   SIM    K6173130  ...          0.3067              NaN
        6   SIM    K6173130  ...          0.1733              NaN
        7   SIM    K6173130  ...          0.3200              NaN
        8   SIM    K6173130  ...          0.2933              NaN
        9   SIM    K6173130  ...          0.3733              NaN
        10  SIM    K6173130  ...          0.5733              NaN
        11  SIM    K6173130  ...          0.2800              NaN
        12  SIM    K6173130  ...          0.3867              NaN
        13  SIM    K6173130  ...          0.7800              NaN
        14  SIM    K6173130  ...          0.4667              NaN
        15  SIM    K6173130  ...          0.0800              NaN
        16  SIM    K6173130  ...          0.1333              NaN
        17  SIM    K6173130  ...          0.1600              NaN
        18  SIM    K6173130  ...          0.0800              NaN
        19  SIM    K6173130  ...          0.1333              NaN


        >>> f = 'data/model/grp16/rt/GRP_D_Prev_2001.txt'
        >>> reader = GRPRT_Fcst(filename=f, datatype='fcst_diff')
        >>> df = reader.read()
        >>> df
            TYP        CODE  ...       PLUIE(mm)  Temperature(°C)
        0   PRV    K6173130  ...          1.0533              NaN
        1   PRV    K6173130  ...          0.8800              NaN
        2   PRV    K6173130  ...          0.7067              NaN
        3   PRV    K6173130  ...          0.4133              NaN
        4   PRV    K6173130  ...          0.3467              NaN
        5   PRV    K6173130  ...          0.3067              NaN
        6   PRV    K6173130  ...          0.1733              NaN
        7   PRV    K6173130  ...          0.3200              NaN
        8   PRV    K6173130  ...          0.2933              NaN
        9   PRV    K6173130  ...          0.3733              NaN
        10  PRV    K6173130  ...          0.5733              NaN
        11  PRV    K6173130  ...          0.2800              NaN
        12  PRV    K6173130  ...          0.3867              NaN
        13  PRV    K6173130  ...          0.7800              NaN
        14  PRV    K6173130  ...          0.4667              NaN
        15  PRV    K6173130  ...          0.0800              NaN
        16  PRV    K6173130  ...          0.1333              NaN
        17  PRV    K6173130  ...          0.1600              NaN
        18  PRV    K6173130  ...          0.0800              NaN
        19  PRV    K6173130  ...          0.1333              NaN

        """
        return pnd.read_csv(
            self.filename,
            sep=';',
            encoding='iso-8859-1',
            header=0,
            index_col=False,
            engine='python',
            skipfooter=1,
            na_values=[-99.9, '-99.9000'],
            converters={'      CODE': str,
                        '    DATE(TU)': lambda x: dt.strptime(x.strip(),
                                                              DATE_FORMAT),
                        '    DEBIT(m3/s)': float,
                        '      PLUIE(mm)': float,
                        'Temperature(°C)': float},
        )

    def write(self, data=None):
        """
        Ecrire le fichier de prévision GRP Temps-Réel / Temps-Différé

        Parameters
        ----------
        pandas.DataFrame
            Tableau des prévisions de GRP Temps-Réel

        """
        _exception.check_dataframe(data)
        data.to_csv(
            self.filename,
            encoding='iso-8859-1',
            sep=';',
            float_format='%15.4f',
            header=True,
            index=False,
            date_format='  %Y%m%d%H',
            line_terminator='\r\n',
            na_rep='       -99.9000'
        )
        with open(self.filename, 'a', encoding='iso-8859-1',
                  newline='\r\n') as f:
            f.write('FIN;\n')

    @classmethod
    def get_fileprefix(cls, datatype=None):
        """
        Préfixe des fichiers de données

        Parameters
        ----------
        datatype : str
            Type du fichier de prévision

        Returns
        -------
        p : str
            Préfixe des fichiers de données

        Examples
        --------
        >>> from pyspc.model.grp16 import GRPRT_Fcst
        >>> d = 'obs'
        >>> p = GRPRT_Fcst.get_fileprefix(datatype=d)
        >>> p
        GRP_D_Obs

        >>> d = 'sim'
        >>> p = GRPRT_Fcst.get_fileprefix(datatype=d)
        >>> p
        GRP_D_Simu_

        >>> d = 'fcst'
        >>> p = GRPRT_Fcst.get_fileprefix(datatype=d)
        >>> p
        GRP_D_Prev_

        """
        try:
            p = RT_FCST_FILEPREFIX[datatype]
        except KeyError as ke:
            raise ValueError('Type de donnée incorrect pour la '
                             'définition du préfixe') from ke
        return p

    @classmethod
    def get_lineprefix(cls, datatype=None):
        """
        Préfixe des lignes de données

        Parameters
        ----------
        datatype : str
            Type du fichier de prévision

        Returns
        -------
        p : str
            Préfixe des lignes de données

        Examples
        --------
        >>> from pyspc.model.grp16 import GRPRT_Fcst
        >>> d = 'obs'
        >>> p = GRPRT_Fcst.get_lineprefix(datatype=d)
        >>> p
        OBS

        >>> d = 'sim'
        >>> p = GRPRT_Fcst.get_lineprefix(datatype=d)
        >>> p
        SIM

        >>> d = 'fcst'
        >>> p = GRPRT_Fcst.get_lineprefix(datatype=d)
        >>> p
        PRV

        """
        try:
            p = RT_FCST_LINEPREFIX[datatype.split('_')[0]]
        except KeyError as ke:
            raise ValueError('Type de donnée incorrect pour la '
                             'définition du préfixe') from ke
        return p

    @classmethod
    def get_types(cls):
        """
        Type de fichier de prévision GRP Temps-réel

        - obs       : observation temps-réel
        - obs_diff  : observation temps différé
        - sim       : prévision sans assimilation temps-réel
        - sim_diff  : prévision sans assimilation temps différé
        - fcst      : prévision avec assimilation temps-réel
        - fcst_diff : prévision avec assimilation temps différé

        Returns
        -------
        list
            Liste des types de fichier de prévision GRP Temps-réel

        """
        return sorted(RT_FCST_DTYPES)
