#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2016 - Temps-réel configuration
"""
import collections

import pyspc.core.exception as _exception
from pyspc.convention.grp16 import RT_CONFIG_KEYS


class GRPRT_Cfg(collections.OrderedDict):
    """
    Structure de données GRPRT Cfg (Prévision GRP *Temps Réel*)

    - Config_Prevision.txt

    Attributes
    ----------
    filename : str
        Nom du fichier Config de GRP *Temps-Réel*

    Notes
    -----
    Cette structure de données est un dictionnaire ordonné. La balise d'une
    information est la clé et la structure contient les méta-données dans ses
    valeurs

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRPRT_Cfg

        Parameters
        ----------
        filename : str
            Nom du fichier Config de GRP *Temps-Réel*

        """
        super().__init__()
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRPRT_Cfg
        """
        if self:
            strdata = ''
            for k, v in self.items():
                if k in self.get_cfg_keys():
                    strdata += '\n        *  ==> {} : {}'.format(k, v['value'])
        else:
            strdata = ''
        text = """
        *************************************
        *********** GRP 2016 - RT Cfg *******
        *************************************
        *  NOM FICHIER      = {filename}
        *  INFORMATIONS     = {strdata}
        *************************************
        """
        return text.format(filename=self.filename, strdata=strdata)

    def read(self):
        """
        Lecture du fichier de configuration de GRP Temps Réel
        """
        self.clear()
        cfg_info = {}
        with open(self.filename, 'r', encoding='iso-8859-1') as f:
            data = f.readlines()
            cfg_info["_header"] = data[:4]
            cfg_info["_sep"] = data[4]  # Ligne de separation
            for i in range(5, len(data)):
                if data[i].startswith('#'):
                    key = data[i].split('#')[1]
                    cfg_info.setdefault(key, {'doc': '', 'value': ''})
                    cfg_info[key]['doc'] = data[i].replace("\n",
                                                           "").split('#')[-1]
                    cfg_info[key]['value'] = data[i+1].replace("\n", "")
        if cfg_info:
            self.update(cfg_info)

    def update_config(self, cfg_info):
        """
        Modifier les informations de configuration GRP Temps Reel

        Parameters
        ----------
        cfg_info : dict
            Dictionnaire contenant les informations à modifier dans la
            configuration de GRP Temps Reel. Les clés correspondent aux
            balises du fichier de configuration.

        """
        # Liste des mots-clés
        keys_list = self.get_cfg_keys()
        for key, value in cfg_info.items():
            if key not in keys_list:
                continue
            try:
                self[key]['value'] = value
            except KeyError:
                _exception.Warning(
                    __name__,
                    "Clé de configuration inconnue dans la configuration "
                    "d'origine : {}".format(key))

    def write(self):
        """
        Ecrire le fichier de configuration GRP Temps Réel
        """
        # Liste des mots-clés
        keys_list = self.get_cfg_keys()
        # Ouvrir le fichier de configuration GRP Temps Réel
        with open(self.filename, 'w', encoding='iso-8859-1') as f:
            f.writelines(self['_header'])   # Entete
            f.writelines(self['_sep'])  # Premier separateur de ligne
            for key in keys_list:  # Mots-cle, intitule et valeur
                if key in self:
                    f.write('#' + key + '#' + self[key]['doc'] + '\n')
                    f.write(self[key]['value'] + '\n')
                    f.write(self['_sep'])

    @classmethod
    def get_cfg_keys(cls):
        """
        Récupérer les mots clés de la configuration GRP Temps Réel
        dans l'ordre d'apparition

        Returns
        -------
        list
            Liste des balises du fichier de configuration GRP Temps Réel
        """
        return RT_CONFIG_KEYS
