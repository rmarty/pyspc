#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2016 - Bassin
"""
import collections
from pyspc.convention.grp16 import CAL_BASIN_LINESEP, CAL_BASIN_HEADERS


class GRP_Basin(collections.OrderedDict):
    """
    Structure du fichier Bassin de GRP *Calage*

    Attributes
    ----------
    filename : str
        Nom du fichier Bassin de GRP *Calage*

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRP_Data

        Parameters
        ----------
        filename : str
            Nom du fichier Bassin de GRP *Calage*

        """
        super().__init__()
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRP_Basin
        """
        text = """
        *************************************
        *********** GRP 2016 - Basin ********
        *************************************
        *  NOM FICHIER      = {filename}
        *  INFORMATIONS     = {content}
        *************************************
        """
        content = ""
        for k, v in self.items():
            if isinstance(v, str):
                content += '\n        *      + {0} = {1}'.format(k, v)
            elif isinstance(v, (dict, collections.OrderedDict)):
                content += '\n        *      + {0}'.format(k)
                for k2, v2 in v.items():
                    content += '\n        *          + {0} = {1}'.format(k2,
                                                                         v2)
        return text.format(filename=self.filename, content=content)

    def read(self):
        """
        Lecture du fichier Bassin de GRP *Calage*
        """
        content = {}
        with open(self.filename, 'r', encoding='utf-8') as f:
            for line in f:
                # Une ligne de commentaire commence par "#" ou "!"
                if line.startswith("!") or line.startswith("#"):
                    continue
                # Balise
                tag = line[0]
                # Découpage de la ligne
                x = line.split("!", 1)[0][2:].split(" ")
                x = list(filter(None, x))
                code = x[0]  # Id Station
                w = float(x[1])  # Poids Station
                # Nom de l'entité
                try:
                    name = line.split("!")[1].strip()
                except IndexError:
                    name = ''
                # Ajout de l'entité
                content.setdefault(tag, collections.OrderedDict())
                content[tag].setdefault(code, {'n': name, 'w': w})
                # Spécificité température
                if tag == 'T':
                    content[tag][code]['z'] = float(x[2])  # Altitude Station
        self.update(content)

    def write(self):
        """
        Ecriture du fichier Bassin de GRP *Calage*
        """
        with open(self.filename, 'w', encoding='utf-8', newline="\r\n") as f:
            # Première ligne
            f.write(CAL_BASIN_LINESEP)
            # Boucle sur les étiquettes
            for tag in self.keys():
                # Entête de l'étiquette
                f.write(CAL_BASIN_HEADERS[tag])
                # Boucle sur les entités de l'étiquette
                for code in self[tag].keys():
                    # Etiquette
                    f.write('{0:1s}'.format(tag))
                    # Station
                    f.write(' {0:8s}'.format(code))
                    # Pondération
                    f.write(' {0:9.2f}'.format(self[tag][code]['w']))
                    # Altitude - Nom
                    if tag == 'T':
                        f.write(' {0:7.2f}'.format(self[tag][code]['z']))
                        f.write(' ! {0:<79s} !'.format(self[tag][code]['n']))
                    else:
                        f.write(' ! {0:<87s} !'.format(self[tag][code]['n']))
                    f.write('\n')
                f.write(CAL_BASIN_LINESEP)
