#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2016 - Temps-réel prévision
"""
import os.path
import pandas as pnd

import pyspc.core.exception as _exception
from pyspc.convention.grp16 import (
    RT_DATA_OBSFILEPREFIX, RT_DATA_LINEPREFIX,
    RT_DATA_SCENFILEPREFIX
)


class GRPRT_Data():
    """
    Structure de données GRPRT Data (GRP *Temps Réel*)

    Fichiers

    - Debit.txt
    - Pluie.txt
    - Temp.txt
    - Hauteur.txt
    - Scen_XXX_PluScenXXX.txt

    Attributes
    ----------
    filename : str
        Nom du fichier de données
    varname : str
        Nom de la variable
    scen : bool
        Observation (False) ou scénario météo (True)
    lineprefix : str
        Préfixe des lignes de données du fichier

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRPRT_Data

        Parameters
        ----------
        filename : str
            Nom du fichier de données

        """
        self.varname, self.scen = self.get_metadata(filename=filename)
        self.lineprefix = self.get_lineprefix(varname=self.varname)
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRPRT_Data
        """
        text = """
        *************************************
        *********** GRP 2016 - RT Data ******
        *************************************
        *  NOM FICHIER      = {filename}
        *  NOM VARIABLE     = {varname}
        *  SCENARIO         = {scen}
        *  PREFIXE DONNEE   = {lineprefix}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lecture d'un fichier d'observations ou de scénarios météo pour GRP RT

        Returns
        -------
        pandas.DataFrame
            Tableau des prévisions de GRP Temps-Réel

        Examples
        --------
        >>> from pyspc.model.grp16 import GRPRT_Data
        >>> f = 'data/model/grp16/rt/Debit.txt'
        >>> reader = GRPRT_Data(filename=f)
        >>> reader
        *************************************
        *********** GRP 2016 - RT Data ******
        *************************************
        *  NOM FICHIER      = data/model/grp16/rt/Debit.txt
        *  NOM VARIABLE     = Q
        *  SCENARIO         = None
        *  PREFIXE DONNEE   = CQT
        *************************************
        >>> df = reader.read()
        >>> df
           PREFIX      CODE      DATE   HOUR   VALUE  NaN
        0     CQT  K0114020  20170613  12:00    0.49  NaN
        1     CQT  K0114020  20170613  13:00    0.48  NaN
        2     CQT  K0114020  20170613  14:00    0.48  NaN
        3     CQT  K0114020  20170613  15:00    1.00  NaN
        4     CQT  K0114020  20170613  16:00    3.92  NaN
        5     CQT  K0114020  20170613  17:00    3.87  NaN
        6     CQT  K0114020  20170613  18:00    5.21  NaN
        7     CQT  K0114020  20170613  19:00   17.70  NaN
        8     CQT  K0114020  20170613  20:00   37.30  NaN
        9     CQT  K0114020  20170613  21:00   35.90  NaN
        10    CQT  K0114020  20170613  22:00   19.40  NaN
        11    CQT  K0114020  20170613  23:00   14.40  NaN
        12    CQT  K0114030  20170613  12:00    0.57  NaN
        13    CQT  K0114030  20170613  13:00    0.57  NaN
        14    CQT  K0114030  20170613  14:00    0.56  NaN
        15    CQT  K0114030  20170613  15:00    0.59  NaN
        16    CQT  K0114030  20170613  16:00    0.58  NaN
        17    CQT  K0114030  20170613  17:00    0.70  NaN
        18    CQT  K0114030  20170613  18:00   11.20  NaN
        19    CQT  K0114030  20170613  19:00  121.00  NaN
        20    CQT  K0114030  20170613  20:00  203.00  NaN
        21    CQT  K0114030  20170613  21:00  159.00  NaN
        22    CQT  K0114030  20170613  22:00   92.80  NaN
        23    CQT  K0114030  20170613  23:00   53.80  NaN

        >>> f = 'data/model/grp16/rt/Pluie.txt'
        >>> reader = GRPRT_Data(filename=f)
        >>> reader
        *************************************
        *********** GRP 2016 - RT Data ******
        *************************************
        *  NOM FICHIER      = data/model/grp16/rt/Pluie.txt
        *  NOM VARIABLE     = P
        *  SCENARIO         = None
        *  PREFIXE DONNEE   = PLU
        *************************************
        >>> df = reader.read()
        >>> df
           PREFIX      CODE      DATE   HOUR  VALUE  NaN
        0     PLU  43101002  20170613  12:00    0.0  NaN
        1     PLU  43101002  20170613  13:00    0.0  NaN
        2     PLU  43101002  20170613  14:00    0.8  NaN
        3     PLU  43101002  20170613  15:00    0.2  NaN
        4     PLU  43101002  20170613  16:00    2.6  NaN
        5     PLU  43101002  20170613  17:00   21.6  NaN
        6     PLU  43101002  20170613  18:00   49.0  NaN
        7     PLU  43101002  20170613  19:00  123.0  NaN
        8     PLU  43101002  20170613  20:00   26.8  NaN
        9     PLU  43091005  20170613  12:00    0.0  NaN
        10    PLU  43091005  20170613  13:00    0.0  NaN
        11    PLU  43091005  20170613  14:00    0.0  NaN
        12    PLU  43091005  20170613  15:00   23.8  NaN
        13    PLU  43091005  20170613  16:00    2.6  NaN
        14    PLU  43091005  20170613  17:00    2.2  NaN
        15    PLU  43091005  20170613  18:00    1.2  NaN
        16    PLU  43091005  20170613  19:00    4.9  NaN
        17    PLU  43091005  20170613  20:00   34.2  NaN

        >>> f = 'data/model/grp16/rt/Scen_006_PluMA.txt'
        >>> reader = GRPRT_Data(filename=f)
        >>> reader
        *************************************
        *********** GRP 2016 - RT Data ******
        *************************************
        *  NOM FICHIER      = data/model/grp16/rt/Scen_006_PluMA.txt
        *  NOM VARIABLE     = P
        *  SCENARIO         = 006
        *  PREFIXE DONNEE   = PLU
        *************************************
        >>> df = reader.read()
        >>> df
           PREFIX      CODE      DATE   HOUR  VALUE  NaN
        0     PLU  K0114020  20170613  12:00    0.0  NaN
        1     PLU  K0114020  20170613  13:00    0.0  NaN
        2     PLU  K0114020  20170613  14:00    2.0  NaN
        3     PLU  K0114020  20170613  15:00    2.0  NaN
        4     PLU  K0114020  20170613  16:00    2.0  NaN
        5     PLU  K0114020  20170613  17:00    5.0  NaN
        6     PLU  K0114020  20170613  18:00    5.0  NaN
        7     PLU  K0114020  20170613  19:00    5.0  NaN
        8     PLU  K0114020  20170613  20:00    0.0  NaN
        9     PLU  K0114020  20170613  21:00    0.0  NaN
        10    PLU  K0114020  20170613  22:00    0.0  NaN
        11    PLU  K0114030  20170613  12:00    0.0  NaN
        12    PLU  K0114030  20170613  13:00    0.0  NaN
        13    PLU  K0114030  20170613  14:00   10.0  NaN
        14    PLU  K0114030  20170613  15:00   10.0  NaN
        15    PLU  K0114030  20170613  16:00   10.0  NaN
        16    PLU  K0114030  20170613  17:00   15.0  NaN
        17    PLU  K0114030  20170613  18:00   15.0  NaN
        18    PLU  K0114030  20170613  19:00   15.0  NaN
        19    PLU  K0114030  20170613  20:00    0.0  NaN
        20    PLU  K0114030  20170613  21:00    0.0  NaN
        21    PLU  K0114030  20170613  22:00    0.0  NaN

        """
        return pnd.read_csv(
            self.filename,
            sep=';',
            header=None,
            index_col=False,
            skiprows=1,
            engine='python',
            skipfooter=1,
            names=['PREFIX', 'CODE', 'DATE', 'HOUR', 'VALUE', None],
        )

    def write(self, data=None):
        """
        Ecrire le fichier de données GRPRT Data

        Parameters
        ----------
        pandas.DataFrame
            Tableau des prévisions de GRP Temps-Réel

        """
        _exception.check_dataframe(data)
        with open(self.filename, 'w', encoding='utf-8', newline='\r\n') as f:
            f.write('Ligne_Entete\n')
        data.to_csv(
            self.filename,
            mode='a',
            sep=';',
            float_format='%.3f',
            header=False,
            index=False,
            line_terminator='\r\n'
        )
        with open(self.filename, 'a', encoding='utf-8', newline='\r\n') as f:
            f.write('FIN:OBS\n')

    @staticmethod
    def get_lineprefix(varname=None):
        """
        Préfixe des lignes de données

        Parameters
        ----------
        varname : str
            Nom de la variable

        Returns
        -------
        p : str
            Préfixe des lignes de données

        Examples
        --------
        >>> from pyspc.model.grp16 import GRPRT_Data
        >>> v = 'H'
        >>> p = GRPRT_Data.get_lineprefix(varname=v)
        >>> p
        HAU

        >>> v = 'P'
        >>> p = GRPRT_Data.get_lineprefix(varname=v)
        >>> p
        PLU

        >>> v = 'Q'
        >>> p = GRPRT_Data.get_lineprefix(varname=v)
        >>> p
        CQT

        >>> v = 'T'
        >>> p = GRPRT_Data.get_lineprefix(varname=v)
        >>> p
        TEM

        """
        try:
            p = RT_DATA_LINEPREFIX[varname.split('_')[0]]
        except KeyError as ke:
            raise ValueError('Type de donnée incorrect pour la '
                             'définition du préfixe') from ke
        return p

    @staticmethod
    def get_metadata(filename=None):
        """
        Extraire les méta-données du nom du fichier GRPRT Data

        Parameters
        ----------
        filename : str
            Nom du fichier GRPRT Data

        Returns
        -------
        varname : str
            Grandeur
        scen : str, None
            Identifiant du scénario

        """
        # ---------------------------------------------------------------------
        # 0- Initialisation
        # ---------------------------------------------------------------------
        basename = os.path.basename(filename)
        # ---------------------------------------------------------------------
        # 1- Cas de données d'observation (Q, P, T)
        # ---------------------------------------------------------------------
        for v, p in RT_DATA_OBSFILEPREFIX.items():
            if basename.startswith(p):
                return v, None
        # ---------------------------------------------------------------------
        # 2- Cas de scénarios météorologiques (P)
        # ---------------------------------------------------------------------
        for v, p in RT_DATA_SCENFILEPREFIX.items():
            if basename.startswith(p):
                return v, basename.split('_')[1]
        # ---------------------------------------------------------------------
        # 3- Cas inconnu
        # ---------------------------------------------------------------------
        raise ValueError("Le nommage du fichier '{}' est incorrect")
