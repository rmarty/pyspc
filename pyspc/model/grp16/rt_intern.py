#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2016 - Temps-réel interne
"""
from datetime import datetime as dt
import os.path
import pandas as pnd

from pyspc.convention.grp16 import DATE_FORMAT, RT_INTERN_DTYPES


class GRPRT_Intern():
    """
    Structure de données GRPRT Intern (Variables internes de GRP *Temps Réel*)

    Fichiers

    - PQE_1A.DAT
    - PQE_1A_D.DAT

    Attributes
    ----------
    filename : str
        Nom du fichier Archve de GRP *Temps-Réel*
    realtime : bool
        Temps-réel (True) ou Temps différé (False)

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRPRT_Intern

        Parameters
        ----------
        filename : str
            Nom du fichier Interne de GRP *Temps-Réel*

        """
        self.filename = filename
        self.realtime = self.split_basename(filename=self.filename)

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRPRT_Intern
        """
        text = """
        *************************************
        *********** GRP 2016 - RT Intern ****
        *************************************
        *  NOM FICHIER      = {filename}
        *  TEMPS-REEL       = {realtime}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lire le fichier de variables internes Temps-Réel / Temps-Différé

        Returns
        -------
        pandas.DataFrame
            Tableau des variables internes de GRP Temps-Réel

        Examples
        --------
        >>> from pyspc.model.grp16 import GRPRT_Intern
        >>> f = 'data/model/grp16/rt/intern/PV_10A.DAT'
        >>> reader = GRPRT_Intern(filename=f)
        >>> df = reader.read()
        >>> df
                             Qsim(mm)  Qobs(mm)  ...  NHU026  NHU027
        DATE (TU)                                ...
        2020-03-05 18:00:00    0.2922    0.1934  ...  0.0013     0.0
        2020-03-05 19:00:00    0.3185    0.2073  ...  0.0009     0.0
        2020-03-05 20:00:00    0.3455    0.2169  ...  0.0003     0.0
        2020-03-05 21:00:00    0.3728    0.2269  ...  0.0006     0.0
        2020-03-05 22:00:00    0.4001    0.2367  ...  0.0005     0.0
        2020-03-05 23:00:00    0.4269    0.2460  ...  0.0007     0.0
        2020-03-06 00:00:00    0.4532    0.2611  ...  0.0013     0.0
        2020-03-06 01:00:00    0.4782    0.2827  ...  0.0007     0.0
        2020-03-06 02:00:00    0.5014    0.3133  ...  0.0006     0.0
        2020-03-06 03:00:00    0.5218    0.3564  ...  0.0001     0.0
        2020-03-06 04:00:00    0.5390    0.4098  ...  0.0003     0.0
        2020-03-06 05:00:00    0.5523    0.4838  ...  0.0001     0.0
        2020-03-06 06:00:00    0.5613    0.5444  ...  0.0001     0.0
        2020-03-06 07:00:00    0.5662    0.5593  ...  0.0001     0.0
        2020-03-06 08:00:00    0.5668    0.5514  ...  0.0001     0.0
        2020-03-06 09:00:00    0.5626    0.5377  ...  0.0001     0.0
        2020-03-06 10:00:00    0.5547    0.5212  ...  0.0001     0.0
        2020-03-06 11:00:00    0.5442    0.5050  ...  0.0001     0.0
        2020-03-06 12:00:00    0.5318    0.4905  ...  0.0001     0.0
        2020-03-06 13:00:00    0.5172    0.4718  ...  0.0001     0.0
        2020-03-06 14:00:00    0.5002    0.4548  ...  0.0001     0.0
        2020-03-06 15:00:00    0.4828    0.4403  ...  0.0001     0.0
        2020-03-06 16:00:00    0.4661    0.4278  ...  0.0001     0.0
        2020-03-06 17:00:00    0.4501    0.4170  ...  0.0001     0.0
        2020-03-06 18:00:00    0.4346    0.4060  ...  0.0001     0.0

        """
        df = pnd.read_csv(
            self.filename,
            sep=';',
            header=0,
            skiprows=[0, 1, 3, 4],
            index_col=0,
            date_parser=lambda x: dt.strptime(x, DATE_FORMAT),
            na_values=[-99.9, '-99.9000',
                       -9.9900, '   -9.9900', '-9.9900'],
        )
        # Nettoyer la colonne superflue
        df.drop(columns=df.columns[-1], inplace=True)
        # Nettoyer les intitulés des colonnes
        df.columns = [c.strip() for c in df.columns]
        return df

    def write(self, data=None):
        """
        Ecrire le fichier Interne de GRP Temps-Réel / Temps-Différé
        """
        raise NotImplementedError

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        de données GRP Intern (GRP *Temps-Réel*)

        Parameters
        ----------
        filename : str
            Fichier de données GRP Archive (GRP *Temps-Réel*)

        Returns
        -------
        realtime : bool
            Temps-réel (True) ou Temps différé (False)

        Examples
        --------
        >>> from pyspc.model.grp16 import GRPRT_Intern
        >>> f = 'data/model/grp16/rt/intern/PQE_1A.DAT'
        >>> realtime = GRPRT_Intern.split_basename(filename=f)
        >>> realtime
        True
        >>> f = 'data/model/grp16/rt/intern/PQE_1A_D.DAT'
        >>> realtime = GRPRT_Intern.split_basename(filename=f)
        >>> realtime
        False

        """
        if filename is None:
            return None
        x = os.path.splitext(os.path.basename(filename))[0].split('_')
        return len(x) != 3 or x[-1] != 'D'

    def check_datatype(self, datatype):
        """
        Contrôler la cohérence entre le type et le nom du fichier

        Parameters
        ----------
        datatype : str
            Type de fichier

        Raises
        ------
        ValueError
            Si incohérence entre le type et le nom du fichier

        """
        test_file = self.split_basename(filename=self.filename)
        test_dtype = RT_INTERN_DTYPES[datatype]
        if (test_file and not test_dtype) or (not test_file and test_dtype):
            raise ValueError("Incohérence entre le type et le nom du fichier")

    @staticmethod
    def get_types():
        """
        Type de fichier de variable interne GRP Temps-réel

        - intern      : variable interne temps-réel
        - intern_diff : variable interne temps différé
        """
        return sorted(RT_INTERN_DTYPES.keys())
