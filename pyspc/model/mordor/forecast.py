#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - Projet MORDOR - Données de prévision
"""
from datetime import datetime as dt
import os.path
import pandas as pnd

DATE_FORMAT = '%d/%m/%Y'
"""Format des dates dans les fichiers de prévision MORDOR"""


def date_parser(txt):
    """Conversion de date"""
    return dt.strptime(txt, DATE_FORMAT)


class Fcst():
    """
    Structure de prévisions QMJ de MORDOR

    Attributes
    ----------
    filename : str
        Nom du fichier de données
    station : str
        Identifiant de la station
    varname : str
        Grandeur
    runtime : dt
        Instant de prévision
    model : str
        Identifiant du modèle hydro
    meteo : str
        Identifiant de l'ensemble météo
    datatype : str
        Type de prévision

    """
    def __init__(self, filename=None, meteo=None):
        """
        Initialisation de l'instance de la classe Fcst (projet MORDOR)

        Parameters
        ----------
        filename : str
            Nom du fichier de données
        meteo : str
            Identifiant de l'ensemble météo

        """
        self.filename = filename
        if self.filename is not None:
            self.datatype, self.station, self.runtime = self.split_basename(
                self.filename)
        else:
            self.datatype = None
            self.station = None
            self.runtime = None
        self.model = 'MORDOR'
        self.varname = 'Q'
        self.meteo = meteo

    def __str__(self):
        """
        Afficher les méta-données de l'instance Fcst (projet MORDOR)
        """
        text = """
        *************************************
        *********** MORDOR - Fcst ***********
        *************************************
        *  NOM FICHIER          = {filename}
        *  STATION DE PREVISION = {station}
        *  GRANDEUR             = {varname}
        *  INSTANT DE PREVISION = {runtime}
        *  MODELE DE PREVISION  = {model}
        *  ENSEMBLE METEO       = {meteo}
        *  TYPE DE PREVISION    = {datatype}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lecture d'un fichier de prévision QMJ par MORDOR

        Returns
        -------
        pandas.DataFrame
            Tableau des données de prévision QMJ de MORDOR

        Examples
        --------
        >>> from import pyspc.model.mordor import Fcst
        >>> f = 'data/model/mordor/Spaghettis_BASSIN_20220623.txt'
        >>> m = 'CEP'
        >>> d = _model.Fcst(filename=f, meteo=m)
        >>> print(d)
        *************************************
        *********** MORDOR - Fcst ***********
        *************************************
        *  NOM FICHIER          = data/model/mordor/Spaghettis_BASSIN_20220623.txt
        *  STATION DE PREVISION = BASSIN
        *  GRANDEUR             = Q
        *  INSTANT DE PREVISION = 2022-06-23 00:00:00
        *  MODELE DE PREVISION  = MORDOR
        *  ENSEMBLE METEO       = CEP
        *  TYPE DE PREVISION    = Spaghettis
        *************************************

        >>> df = d.read()
        >>> df
                     Q2018    Q2019   Q2020
        2022-06-23   55.35    55.35   55.35
        2022-06-24   48.00    47.77   49.19
        2022-06-25   67.84    66.86   72.71
        2022-06-26   76.95    76.04  104.09
        2022-06-27   76.90    75.11  137.74
        2022-06-28   76.68    73.78  144.78
        2022-06-29   78.99    74.32  226.24
        2022-06-30   80.95    72.06  320.60
        2022-07-01   75.12    68.59  283.52
        2022-07-02   68.73    64.38  287.44
        2022-07-03   65.44    60.28  232.47
        2022-07-04   60.84    56.41  200.59
        2022-07-05   56.82    53.11  171.88
        2022-07-06   57.50    50.43  156.73
        2022-07-07   56.53    53.92  133.61
        2022-07-08   54.40    46.64  125.32
        2022-07-09   50.51    43.79  119.74
        2022-07-10   49.42    43.16  110.37
        2022-07-11   45.09    41.09  104.64
        2022-07-12   45.68    41.58  103.21
        2022-07-13   45.03    38.50   96.12
        2022-07-14   40.31    36.83   90.11
        2022-07-15   39.50    35.20   92.82
        2022-07-16   37.93    33.57   87.82
        2022-07-17   36.11    32.29   86.72
        2022-07-18   33.62    31.10   83.92
        2022-07-19   32.17    29.89   82.23
        2022-07-20   31.48    28.72   78.59
        2022-07-21   30.32    27.68   73.63
        2022-07-22   28.96    26.33   69.75
        ...            ...      ...     ...
        2022-12-02  186.88   846.88   79.48
        2022-12-03  187.61   884.88   80.22
        2022-12-04  232.20   873.21  103.27
        2022-12-05  297.85   871.69  114.93
        2022-12-06  337.16   822.30  130.54
        2022-12-07  323.95   682.94  139.97
        2022-12-08  328.43   563.90  133.03
        2022-12-09  310.56   507.40  126.93
        2022-12-10  273.56   465.47  154.90
        2022-12-11  274.46   445.03  193.61
        2022-12-12  313.00   497.83  204.80
        2022-12-13  308.68   582.11  343.10
        2022-12-14  268.96   648.47  491.17
        2022-12-15  234.26   854.32  518.48
        2022-12-16  203.97  1017.43  511.70
        2022-12-17  181.10  1034.97  532.97
        2022-12-18  197.37  1033.30  522.11
        2022-12-19  218.15   901.66  519.10
        2022-12-20  210.74   824.62  563.92
        2022-12-21  215.73   773.85  527.57
        2022-12-22  229.28   729.16  554.98
        2022-12-23  220.13   772.74  550.13
        2022-12-24  307.42   986.28  587.36
        2022-12-25  321.02  1202.55  588.11
        2022-12-26  290.47  1385.89  559.49
        2022-12-27  276.24  1443.51  635.94
        2022-12-28  239.68  1246.35  687.25
        2022-12-29  216.57  1092.23  748.03
        2022-12-30  189.95  1013.56  772.25
        2022-12-31  171.33   883.63  747.17
        [192 rows x 3 columns]

        """
        content = {}
        with open(self.filename, 'r', encoding='iso-8859-1') as f:
            # -----------------------------------------------------------------
            # Entête
            # -----------------------------------------------------------------
            f.readline()
            header = f.readline().strip().split(' ')
            scens = [h for h in header if h.startswith('Q')]
            f.readline()
            f.readline()
            # -----------------------------------------------------------------
            # Contenu
            # -----------------------------------------------------------------
            for line in f.readlines():
                info = line.strip().split(' ')
                date = date_parser(info.pop(0).replace('"', ''))
                content[date] = [float(x) for x in info if x]
                content[date].pop(0)  # Echeance
                content[date].pop(0)  # Obs
        # ---------------------------------------------------------------------
        # DataFrame
        # ---------------------------------------------------------------------
        df = pnd.DataFrame(content, index=scens).transpose()
        return df

    def write(self):
        """
        Ecrire le fichier de prévision QMJ par PREMHYCE

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        de prévisions QMJ de PREMHYCE

        Parameters
        ----------
        filename : str
            Fichier de prévisions QMJ de PREMHYCE

        Returns
        -------
        datatype : str
            Type de prévision
        station : str
            Identifiant de la station
        runtime : dt
            Instant de prévision

        """
        if filename is None:
            return None, None, None
        meta = os.path.basename(filename).replace('.txt', '').split('_')
        datatype = meta.pop(0)
        runtime = dt.strptime(meta.pop(-1), '%Y%m%d')
        station = '_'.join(meta)
        return datatype, station, runtime

    @staticmethod
    def join_basename(runtime=None, station=None, model=None, meteo=None):
        """
        Définir le nom du fichier
         à partir d'informations sur la prévision prévisions QMJ de PREMHYCE

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError
