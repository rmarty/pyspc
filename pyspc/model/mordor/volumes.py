#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - Projet MORDOR - Données de volumes de soutien
"""
from datetime import datetime as dt
import os.path
import pandas as pnd

DATE_FORMAT = '%d/%m/%Y'
"""Format des dates dans les fichiers de prévision MORDOR"""


def date_parser(txt):
    """Conversion de date"""
    return dt.strptime(txt, DATE_FORMAT)


class Vol():
    """
    Structure de volumes de soutien OSE de MORDOR

    Attributes
    ----------
    filename : str
        Nom du fichier de données
    station : str
        Identifiant de la station
    varname : str
        Grandeur
    runtime : dt
        Instant de prévision
    start : dt
        Début de la période de soutien
    end : dt
        Fin de la période de soutien
    model : str
        Identifiant du modèle hydro
    meteo : str
        Identifiant de l'ensemble météo
    datatype : str
        Type de prévision

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe Vol (projet MORDOR)

        Parameters
        ----------
        filename : str
            Nom du fichier de données

        """
        self.filename = filename
        if self.filename is not None:
            [self.datatype, self.station, self.runtime, self.start, self.end,
             self.meteo] = self.split_basename(self.filename)
        else:
            self.datatype = None
            self.station = None
            self.runtime = None
            self.start = None
            self.end = None
            self.meteo = None
        self.model = 'MORDOR'
        self.varname = 'V'

    def __str__(self):
        """
        Afficher les méta-données de l'instance Vol (projet MORDOR)
        """
        text = """
        *************************************
        *********** MORDOR - Vol ************
        *************************************
        *  NOM FICHIER          = {filename}
        *  STATION DE PREVISION = {station}
        *  GRANDEUR             = {varname}
        *  INSTANT DE PREVISION = {runtime}
        *  DEBUT DE SOUTIEN     = {start}
        *  FIN DE SOUTIEN       = {end}
        *  MODELE DE PREVISION  = {model}
        *  ENSEMBLE METEO       = {meteo}
        *  TYPE DE PREVISION    = {datatype}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lecture d'un fichier de volumes de soutien MORDOR

        Returns
        -------
        pandas.DataFrame
            Tableau des volumes de soutien MORDOR

        Examples
        --------
        >>> from import pyspc.model.mordor import Vol
        >>> f = 'data/model/mordor/volumes_BASSIN_20220623-20220623-20221231_CEP.txt'
        >>> d = _model.Vol(filename=f, meteo=m)
        >>> print(d)
        *************************************
        *********** MORDOR - Vol ************
        *************************************
        *  NOM FICHIER          = data/model/mordor/volumes_BASSIN_20220623-20220623-20221231_CEP.txt
        *  STATION DE PREVISION = BASSIN
        *  GRANDEUR             = V
        *  INSTANT DE PREVISION = 2022-06-23 00:00:00
        *  DEBUT DE SOUTIEN     = 2022-06-23 00:00:00
        *  FIN DE SOUTIEN       = 2022-12-31 00:00:00
        *  MODELE DE PREVISION  = MORDOR
        *  ENSEMBLE METEO       = CEP
        *  TYPE DE PREVISION    = volumes
        *************************************

        >>> df = d.read()
        >>> df
                       50          55          60
        Q1948   11.100672   26.546400   50.848992
        Q1949  317.647900  373.294700  430.134600
        Q1950  193.684610  237.487680  284.440900
        Q1951    0.000000    0.000000    0.401760
        Q1952  121.125890  151.989700  185.298620
        Q1953   51.421820   65.514530   83.009660
        Q1954   23.479200   32.643648   42.987456
        Q1955   58.162752   96.142464  140.725728
        Q1956    1.131840    4.018464    8.662464
        Q1957  115.063200  154.489250  198.406370
        Q1958    0.000000    0.000000    0.401760
        Q1959    1.401408    5.322240   12.412224
        Q1960    0.000000    0.000000    0.401760
        Q1961    6.099840   12.064032   19.606752
        Q1962  209.941630  258.728260  311.421890
        Q1963    0.000000    0.000000    1.698624
        Q1964    8.500032   21.002112   41.545440
        Q1965    1.734048    7.033824   15.136416
        Q1966   42.158880   61.945344   85.548960
        Q1967    0.000000    0.000000    0.447552
        Q1968    0.586656    2.277504    5.679072
        Q1969    0.000000    0.021600    1.889568
        Q1970    0.320544    3.245184    8.163936
        Q1971   12.214368   23.927616   39.923712
        Q1972    6.802272   13.171680   21.924000
        Q1973   30.981312   44.841600   62.093952
        Q1974    0.247968    1.880064    7.271424
        Q1975    1.905120    4.696704    8.947584
        Q1976   77.766050   99.789410  124.098050
        Q1977    0.000000    0.000000    0.401760
        ...           ...         ...         ...
        Q1991   27.467424   40.747968   56.788128
        Q1992    5.810400   12.203136   23.258016
        Q1993  127.296580  154.205860  182.793020
        Q1994    1.069632    5.645376   12.823488
        Q1995   90.679390  114.583680  139.661280
        Q1996  142.140100  184.680000  229.042080
        Q1997    4.574016   11.364192   21.384000
        Q1998   19.140192   29.390688   42.293664
        Q1999    5.945184   13.436064   22.794912
        Q2000   21.542112   38.238912   59.278176
        Q2001    0.000000    0.654912    4.859136
        Q2002    0.000000    0.781920    3.872448
        Q2003  121.245980  155.217600  191.515970
        Q2004   16.816896   27.881280   40.468032
        Q2005   97.474750  131.994140  172.607330
        Q2006    0.000000    0.000000    0.401760
        Q2007    0.000000    0.000000    0.401760
        Q2008   35.657280   61.218720   90.307008
        Q2009   40.836096   63.834048   90.232704
        Q2010   10.269504   18.482688   29.558304
        Q2011    7.840800   14.084928   23.221728
        Q2012    8.500032   19.858176   36.990432
        Q2013    0.000000    0.640224    3.259872
        Q2014    0.000000    0.000000    0.401760
        Q2015  111.227900  141.835100  173.946530
        Q2016   91.305790  121.133660  154.221410
        Q2017    0.000000    0.167616    3.646944
        Q2018  315.722900  367.570700  421.035000
        Q2019  198.324290  241.106110  287.557340
        Q2020  120.128830  148.011840  177.210720
        [73 rows x 3 columns]

        """
        content = {}
        with open(self.filename, 'r', encoding='utf-8') as f:
            # -----------------------------------------------------------------
            # Entête
            # -----------------------------------------------------------------
            header = f.readline().strip().split(' ')
            scens = [h for h in header if h.startswith('Q')]
            f.readline()
            f.readline()
            # -----------------------------------------------------------------
            # Contenu
            # -----------------------------------------------------------------
            for line in f.readlines():
                info = line.strip().split(' ')
                threshold = int(float(
                    info.pop(0).replace('"seuil_', '').replace('"', '')
                ))
                content[threshold] = [float(x) for x in info if x]
        # ---------------------------------------------------------------------
        # DataFrame
        # ---------------------------------------------------------------------
        df = pnd.DataFrame(content, index=scens)
        return df

    def write(self):
        """
        Ecrire le fichier de prévision QMJ par PREMHYCE

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        de prévisions QMJ de PREMHYCE

        Parameters
        ----------
        filename : str
            Fichier de prévisions QMJ de PREMHYCE

        Returns
        -------
        datatype : str
            Type de prévision
        station : str
            Identifiant de la station
        runtime : dt
            Instant de prévision

        """
        if filename is None:
            return None, None, None,  None, None, None
        meta = os.path.basename(filename).replace('.txt', '').split('_')
        datatype = meta.pop(0)
        meteo = meta.pop(-1)
        dts = meta.pop(-1).split('-')
        runtime = dt.strptime(dts.pop(0), '%Y%m%d')
        start = dt.strptime(dts.pop(0), '%Y%m%d')
        end = dt.strptime(dts.pop(0), '%Y%m%d')
        station = '_'.join(meta)
        return datatype, station, runtime, start, end, meteo

    @staticmethod
    def join_basename(runtime=None, station=None, model=None, meteo=None):
        """
        Définir le nom du fichier
         à partir d'informations sur la prévision prévisions QMJ de PREMHYCE

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError
