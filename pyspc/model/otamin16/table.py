#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Incertitudes de modélisation - Projet OTAMIN v2016 - Fichier Abaque
"""
import os.path
import pandas as pnd

from pyspc.convention.otamin16 import CAL_TDELTA


class Table():
    """
    Classe permettant la manipulation du csv OTAMIN v2016 (Abaque)

    Attributes
    ----------
    filename : str
        Nom du fichier csv OTAMIN v2016 (Calage)
    station : str
        Code du lieu
    model : str
        Code du modèle selon la convention POM
    method : str
        Méthode d'estimation
    error : str
        Type d'erreur
    datatype : str
        Type de fichier
    """

    def __init__(self, filename=None):
        """
        Initialiser l'instance de la classe Table (csv, Rdata) de Otamin v2016

        Parameters
        ----------
        filename : str
            Nom du fichier prv de OTAMIN v2016

        """
        self.filename = filename
        if self.filename is not None:
            meta = self.split_basename(self.filename)
            self.station = meta[0]
            self.model = meta[1]
            self.method = meta[2]
            self.error = meta[3]
            self.datatype = meta[4]
        else:
            self.station = None
            self.model = None
            self.method = None
            self.error = None
            self.datatype = None

    def __str__(self):
        """
        Afficher des méta-données de l'instance Table (csv) de Otamin v2016
        """
        text = """
        *************************************
        ***** OTAMIN 2016 - Table ***********
        *************************************
        *  NOM FICHIER      = {filename}
        *  STATION          = {station}
        *  MODELE           = {model}
        *  METHODE          = {model}
        *  TYPE ERREUR      = {error}
        *  TYPE FICHIER     = {datatype}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lire un fichier Abaque csv Otamin

        Returns
        -------
        data : pnd.DataFrame
            Dataframe des données

        Examples
        --------
        >>> from pyspc.verification.otamin16 import Table
        >>> f = 'data/model/otamin16/K0403010_45gGRPd000_QUOIQUE_EM_TAB.csv'
        >>> d = Table(filename=f)
        >>> print(d)
        *************************************
        ***** OTAMIN 2016 - Table ***********
        *************************************
        *  NOM FICHIER      = data/model/otamin16/K0403010_45gGRPd000_QUOIQUE_EM_TAB.csv
        *  STATION          = K0403010
        *  MODELE           = 45gGRPd000
        *  METHODE          = 45gGRPd000
        *  TYPE ERREUR      = EM
        *  TYPE FICHIER     = TAB
        *************************************

        >>> df = d.read()
        >>> df
                  HorPrevi   df_val      U05  ...      U85      U90      U95
        0  0 days 12:00:00   10.141    4.670  ...   13.237   13.680   14.786
        1  0 days 12:00:00   11.359    5.142  ...   14.786   15.289   16.466
        2  0 days 12:00:00   13.272    5.905  ...   17.229   17.824   19.126
        3  0 days 12:00:00   16.353    7.148  ...   21.169   21.912   23.428
        4  0 days 12:00:00   23.790   10.213  ...   30.711   31.806   33.881
        5  0 days 12:00:00   25.404   10.707  ...   32.704   33.888   35.964
        6  0 days 12:00:00   27.260   11.490  ...   35.094   36.365   38.592
        7  0 days 12:00:00   29.701   12.519  ...   38.235   39.620   42.047
        8  0 days 12:00:00   32.246   13.591  ...   41.512   43.015   45.651
        9  0 days 12:00:00   36.502   15.385  ...   46.991   48.693   51.676
        10 0 days 12:00:00   41.721   17.585  ...   53.710   55.655   59.064
        11 0 days 12:00:00   47.707   20.108  ...   61.416   63.640   67.539
        12 0 days 12:00:00   56.642   23.874  ...   72.918   75.559   80.188
        13 0 days 12:00:00   70.745   29.818  ...   91.074   94.372  100.153
        14 0 days 12:00:00  325.899  137.363  ...  419.546  434.738  461.372
        15 1 days 00:00:00    9.746    3.579  ...   13.750   14.759   16.482
        16 1 days 00:00:00   11.045    3.934  ...   15.308   16.483   18.504
        17 1 days 00:00:00   12.980    4.479  ...   17.665   19.086   21.541
        18 1 days 00:00:00   16.452    5.495  ...   21.980   23.830   27.043
        19 1 days 00:00:00   24.706    7.978  ...   32.391   35.242   40.220
        20 1 days 00:00:00   26.171    8.161  ...   33.660   36.758   42.193
        21 1 days 00:00:00   27.890    8.697  ...   35.870   39.171   44.963
        22 1 days 00:00:00   30.254    9.434  ...   38.910   42.491   48.774
        23 1 days 00:00:00   33.949   10.586  ...   43.663   47.682   54.732
        24 1 days 00:00:00   38.268   11.933  ...   49.218   53.748   61.695
        25 1 days 00:00:00   44.059   13.739  ...   56.666   61.881   71.032
        26 1 days 00:00:00   51.186   15.961  ...   65.832   71.891   82.522
        27 1 days 00:00:00   59.604   18.586  ...   76.658   83.714   96.092
        28 1 days 00:00:00   73.324   22.864  ...   94.304  102.983  118.211
        29 1 days 00:00:00  326.951  101.950  ...  420.502  459.202  527.104
        [30 rows x 21 columns]

        """
        df = pnd.read_csv(self.filename, sep=';', index_col=False)
        df['HorPrevi'] = CAL_TDELTA * df['HorPrevi']
        return df

    def write(self, data=None):
        """
        Ecrire un fichier Abaque csv Otamin

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        csv OTAMIN v2016 (Abaque)

        Parameters
        ----------
        filename : str
            Fichier csv OTAMIN v2016 (Abaque)

        Returns
        -------
        station : str
            Code du lieu
        model : str
            Code du modèle selon la convention POM
        method : str
            Méthode d'estimation
        error : str
            Type d'erreur
        datatype : str
            Type de fichier

        """
        if filename is None:
            return None, None, None, None, None
        basename = os.path.basename(filename)
        if basename.upper().endswith('RDATA'):
            raise NotImplementedError('Fichier RData pas implémenté')
        basename = basename.replace('.csv', '')
        try:
            [station, model, method, error, datatype] = basename.split('_')
        except ValueError as ve:
            raise ValueError("Le nom de fichier ne respecte pas le "
                             "nommage de OTAMIN") from ve
        return station, model, method, error, datatype

    @staticmethod
    def join_basename(station=None, model=None, leadtime=None):
        """
        Extraire les informations depuis le nom du fichier
        csv OTAMIN v2016 (Abaque)

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError
