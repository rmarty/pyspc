#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - Projet PLATHYNES - Observations
"""
from datetime import datetime as dt, timedelta as td


def str2td(tstep=None):
    """
    Définir le pas de temps à partir de la chaine de caractères
    'nbjour h:m:s'

    Parameters
    ----------
    tstep : str
        'nbjour h:m:s'

    Returns
    -------
    tdelta : timedelta
        pas de temps

    """
    if tstep is None:
        return None
    [nbj, t] = tstep.split(' ')
    nbj = int(float(nbj))
    t = dt.strptime(t, "%H:%M:%S")
    return td(days=nbj, hours=t.hour, minutes=t.minute, seconds=t.second)


def td2str(tdelta=None):
    """
    Définir le pas de temps en une chaine de caractères 'nbjour h:m:s'

    Parameters
    ----------
    tdelta : timedelta
        pas de temps

    Returns
    -------
    tstep : str
        'nbjour h:m:s'

    """
    if not isinstance(tdelta, td):
        raise ValueError("Le pas de temps n'est pas un objet timedelta")
    j = tdelta.days
    s = tdelta.seconds
    h = s // 3600
    s = s - (h * 3600)
    m = s // 60
    s = s - (m * 60)
    return '{0} {1:02d}:{2:02d}:{3:02d}'.format(j, h, m, s)
