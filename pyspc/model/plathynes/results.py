#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - Projet PLATHYNES - Results
"""
from datetime import datetime as dt
import pandas as pnd


DATE_FORMAT = '%Y-%m-%d %H:%M:00'
"""Format des dates dans les résultats PLATHYNES"""


def date_parser(txt):
    """Conversion de date"""
    return dt.strptime(txt, DATE_FORMAT)


class Results():
    """
    Structure de données des résultats de PLATHYNES (*ResultsRaw.txt*)

    Attributes
    ----------
    filename : str
        Nom du fichier des résultats de PLATHYNES

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe Results

        Parameters
        ----------
        filename : str
            Nom du fichier des résultats de PLATHYNES

        """
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance Results
        """
        text = """
        *************************************
        ******* PLATHYNES - Results *********
        *************************************
        *  NOM FICHIER      = {filename}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lecture du fichier de données Results

        Returns
        -------
        data : pnd.DataFrame
            Dataframe des données
        meta : dict
            Méta-données des données (lieu)

        Examples
        --------
        >>> import pyspc.model.plathynes as _model
        >>> f = 'data/model/plathynes/8001_ResultsRaw.txt'
        >>> d = _model.Results(filename=f)
        >>> df, meta = d.read()
        >>> df
                 Date     Time  Rainfall [mm/h]  QSim [m3/s]  CSim [-]
        0  2019-10-20 12:00:00          5.85262      226.858       0.0
        1  2019-10-20 13:00:00          6.52331      306.928       0.0
        2  2019-10-20 14:00:00          7.72051      415.942       0.0
        3  2019-10-20 15:00:00          2.05037      548.677       0.0
        4  2019-10-20 16:00:00          4.51873      731.774       0.0
        5  2019-10-20 17:00:00          9.89279      872.356       0.0
        6  2019-10-20 18:00:00          1.68118      967.920       0.0
        7  2019-10-20 19:00:00          0.35094     1157.065       0.0
        8  2019-10-20 20:00:00          0.57987     1248.181       0.0
        9  2019-10-20 21:00:00          0.54195     1252.210       0.0
        10 2019-10-20 22:00:00          0.21910     1178.181       0.0
        11 2019-10-20 23:00:00          0.33221     1075.628       0.0
        12 2019-10-21 00:00:00          0.11985      941.526       0.0
        13 2019-10-21 01:00:00          0.05655      811.066       0.0
        14 2019-10-21 02:00:00          0.02360      822.547       0.0
        15 2019-10-21 03:00:00          0.00000      747.877       0.0
        16 2019-10-21 04:00:00          0.00000      651.639       0.0
        17 2019-10-21 05:00:00          0.00000      549.495       0.0
        18 2019-10-21 06:00:00          0.00000      457.696       0.0

        >>> meta
        {'evt': '8001', 'loc': 'LaLoireChadrac',
         'x': '723755.56', 'y': '2007188.38'}

        """
        meta = {}
        # Lecture des méta-données
        with open(self.filename, 'r', encoding='utf-8') as f:
            evt = f.readline().strip().split(' ')[-1]
            f.readline()  # ligne vide
            f.readline().strip()  # 1
            f.readline()  # StationID ....
            info = f.readline().strip().split(' ')
            meta['evt'] = evt
            meta['loc'] = info[0]
            meta['x'] = info[1]
            meta['y'] = info[2]
        # Lecture du tableau de données
        with open(self.filename, 'r', encoding='utf-8') as f:
            data = pnd.read_fwf(
                f,
                colspecs=[(0, 20), (20, 36), (36, 49), (49, 60)],
                skiprows=5,
                na_values=-1,
                keep_default_na=True,
                converters={'Date     Time': date_parser},
            )
        return data, meta

    def write(self):
        """
        Ecrire le fichier de résultats PLATHYNES

        Raises
        ------
        NotImplmentedError

        """
        raise NotImplementedError
