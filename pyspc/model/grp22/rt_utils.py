#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2022
"""
import os


def list_locs(dirname):
    """
    Lister les stations par grandeur dans une base TR de GRP.

    Parameters
    ----------
    dirname : str
        Répertoire de la base TR

    Returns
    -------
    locs : dict
        Stations par grandeur (clé)

    """
    locs = {}
    for subdir, varname in zip(['BD_Debits', 'BD_Pluies', 'BD_Temperatures'],
                               ['Q', 'P', 'T']):
        subdir = os.path.join(dirname, subdir)
        if not os.path.exists(subdir):
            continue
        locs[varname] = list(os.listdir(subdir))
    return locs
