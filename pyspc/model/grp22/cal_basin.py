#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2022 - Bassin
"""
import collections
import os.path

from pyspc.core.config import Config


class GRP_Basin(Config):
    """
    Structure du fichier Bassin de GRP *Calage*

    Attributes
    ----------
    filename : str
        Nom du fichier Bassin de GRP *Calage*

    See Also
    --------
    pyspc.core.config.Config

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRP_Data

        Parameters
        ----------
        filename : str
            Nom du fichier Bassin de GRP *Calage*

        """
        super().__init__()
        self.filename = filename
        if self.filename is not None:
            info = self.split_basename(self.filename)
            self.location = info[0]
            self.timestep = info[1]
        else:
            self.location = None
            self.timestep = None

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRP_Basin
        """
        text = """
        *************************************
        *********** GRP 2022 - Basin ********
        *************************************
        *  NOM FICHIER      = {filename}
        *  LIEU MODELE      = {location}
        *  PAS DE TEMPS     = {timestep}
        *  INFORMATIONS     = {content}
        *************************************
        """
        content = ""
        for k, v in self.items():
            if isinstance(v, str):
                content += '\n        *      + {0} = {1}'.format(k, v)
            elif isinstance(v, (dict, collections.OrderedDict)):
                content += '\n        *      + {0}'.format(k)
                for k2, v2 in v.items():
                    content += '\n        *          + {0} = {1}'.format(k2,
                                                                         v2)
        return text.format(filename=self.filename, location=self.location,
                           timestep=self.timestep, content=content)

    def read(self):
        """
        Lecture du fichier Bassin de GRP *Calage*

        Examples
        --------
        >>> from pyspc.model.grp20.cal_basin import GRP_Basin
        >>> f = 'data/model/grp20/cal/RH10585x_00J01H00M.DAT'
        >>> basin = GRP_Basin(filename=f)
        >>> basin.read()
        >>> basin
        *************************************
        *********** GRP 2022 - Basin ********
        *************************************
        *  NOM FICHIER      = data/model/grp22/cal/RH10585x_00J01H00M.ini
        *  LIEU MODELE      = RH10585x
        *  PAS DE TEMPS     = 00J01H00M
        *  INFORMATIONS     =
        *      + RH10585x
        *          + etp_pond = 1.00
        *      + 90035001
        *          + ta_pond = 0.33
        *          + ta_alti = 401.00
        *      + 90052002
        *          + rr_pond = 0.80
        *          + rr_pdt = 00J01H00M
        *          + ta_pond = 0.33
        *          + ta_alti = 473.00
        *      + 90065003
        *          + rr_pond = 0.20
        *          + rr_pdt = 00J01H00M
        *          + ta_pond = 0.33
        *          + ta_alti = 1153.00
        *      + MODELISATION
        *          + facteur_neige = 1.00
        *          + duree_lacune = 01J00H00M
        *************************************

        """
        super().read(encoding='iso-8859-15')
        conv_values = {('MODELISATION', 'facteur_neige'): float}
        for so in self.list_sections_options():
            if so[1].endswith('_alti') or so[1].endswith('_pond'):
                conv_values[so] = float
        self.convert(functions=conv_values)

    def write(self):
        """
        Ecriture du fichier Bassin de GRP *Calage*
        """
        return super().write(encoding='iso-8859-15', newline="\r\n")

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        de données GRP Basin (GRP *Calage*)

        Parameters
        ----------
        filename : str
            Fichier de données GRP Basin (GRP *Calage*)

        Returns
        -------
        location : str
            Identifiant de la station
        timestep : str
            Pas de temps du modèle

        Examples
        --------
        >>> from pyspc.model.grp22 import GRP_Basin
        >>> f = 'data/model/grp22/cal/RH10585x_00J01H00M.ini'
        >>> [location, timestep] = GRP_Basin.split_basename(filename=f)
        >>> location
        RH10585x
        >>> varname
        00J01H00M

        """
        if filename is None:
            return None, None
        basename = os.path.splitext(os.path.basename(filename))[0]
        try:
            [location, timestep] = basename.split('_')
        except ValueError as ve:
            raise ValueError("Le nom de fichier ne respecte pas le "
                             "nommage de GRP") from ve
        return (location, timestep)

    @staticmethod
    def join_basename(location=None, timestep=None):
        """
        Définir le nom du fichier de données GRP Basin (GRP *Calage*)
        à partir des informations

        Parameters
        ----------
        location : str
            Identifiant de la station
        timestep : str
            Pas de temps du modèle

        Returns
        -------
        filename : str
            Fichier de données GRP Basin (GRP *Calage*)

        """
        if location is None or timestep is None:
            raise ValueError('Définition incorrecte des arguments')
        return '{}_{}.ini'.format(location, timestep)
