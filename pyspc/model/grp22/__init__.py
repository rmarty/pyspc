#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2022
"""
from .cal_basin import GRP_Basin
from .cal_config import GRP_Cfg, GRP_Run
from .cal_data import GRP_Data
from .cal_event import GRP_Event
from .cal_fcst import GRP_Fcst
from .cal_report import report_model, report_verif
from .cal_verif import GRP_Verif

from .rt_utils import list_locs
from .rt_archive import GRPRT_Archive
from .rt_basin import GRPRT_Basin
from .rt_config import GRPRT_Cfg
from .rt_data import GRPRT_Data
from .rt_metscen import GRPRT_Metscen
from .rt_fcst import GRPRT_Fcst
from .rt_intern import GRPRT_Intern
from .rt_param import GRPRT_Param

from .tdelta import str2td, td2str

__all__ = [
    'str2td', 'td2str',
    'GRP_Basin',
    'GRP_Cfg', 'GRP_Run',
    'GRP_Data',
    'GRP_Fcst',
    'GRP_Event',
    'GRP_Verif',
    'report_model', 'report_verif',
    'GRPRT_Basin', 'GRPRT_Param', 'GRPRT_Cfg',
    'GRPRT_Data', 'GRPRT_Fcst', 'GRPRT_Metscen',
    'GRPRT_Archive', 'GRPRT_Intern',
    'list_locs'
]
