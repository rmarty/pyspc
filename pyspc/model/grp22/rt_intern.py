#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2018 - Temps-réel interne
"""
import os.path
import pandas as pnd

from pyspc.convention.grp22 import RT_INTERN_DTYPES


class GRPRT_Intern():
    """
    Structure de données GRPRT Intern (Variables internes de GRP *Temps Réel*)

    Fichiers

    - PQE_1A.DAT
    - PQE_1A_D.DAT

    Attributes
    ----------
    filename : str
        Nom du fichier Archve de GRP *Temps-Réel*
    realtime : bool
        Temps-réel (True) ou Temps différé (False)

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRPRT_Intern

        Parameters
        ----------
        filename : str
            Nom du fichier Interne de GRP *Temps-Réel*

        """
        self.filename = filename
        self.realtime = self.split_basename(filename=self.filename)

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRPRT_Intern
        """
        text = """
        *************************************
        *********** GRP 2018 - RT Intern ****
        *************************************
        *  NOM FICHIER      = {filename}
        *  TEMPS-REEL       = {realtime}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lire le fichier de variables internes Temps-Réel / Temps-Différé

        Returns
        -------
        pandas.DataFrame
            Tableau des variables internes de GRP Temps-Réel

        Examples
        --------
        >>> from pyspc.model.grp22 import GRPRT_Intern

        Cas avec des données au pas de temps horaire (mm: mm par heure)

        >>> f = 'data/model/grp22/rt/intern/PQE_1A_D.DAT'
        >>> reader = GRPRT_Intern(filename=f)
        >>> df = reader.read()
        >>> df
                             Qsim(mm)  Qobs(mm)  ETP(mm)  P(mm)  T(C)    R(mm)     S(mm)
        DATE (TU)
        2007-01-18 12:00:00    0.3649    0.3278   0.0338   0.16   NaN  25.7607  247.0715
        2007-01-18 13:00:00    0.3604    0.3221   0.0338   0.12   NaN  25.6028  247.0227
        2007-01-18 14:00:00    0.3556    0.3164   0.0319   0.20   NaN  25.4343  246.9898
        2007-01-18 15:00:00    0.3762    0.3147   0.0281   3.04   NaN  26.1560  247.5004
        2007-01-18 16:00:00    0.4350    0.3170   0.0224   3.36   NaN  28.1103  248.0608
        2007-01-18 17:00:00    0.5277    0.3363   0.0148   4.16   NaN  30.9372  248.7550
        2007-01-18 18:00:00    0.6924    0.4108   0.0054   8.48   NaN  35.3932  250.1835
        2007-01-18 19:00:00    0.8931    0.5981   0.0000   3.72   NaN  40.1447  250.7462
        2007-01-18 20:00:00    1.1170    0.9132   0.0000   7.60   NaN  44.8370  251.9237
        2007-01-18 21:00:00    1.3698    1.1836   0.0000   6.32   NaN  49.5851  252.8453
        2007-01-18 22:00:00    1.7101    1.4474   0.0000   9.44   NaN  55.3158  254.1844
        2007-01-18 23:00:00    2.0794    1.7065   0.0000   6.64   NaN  60.9018  255.0535
        2007-01-19 00:00:00    2.4511    1.9306   0.0000   7.88   NaN  66.0258  256.0480
        2007-01-19 01:00:00    2.8393    2.1194   0.0000   8.44   NaN  70.9637  257.0599
        2007-01-19 02:00:00    3.1850    2.2889   0.0000   5.32   NaN  75.0719  257.6386
        2007-01-19 03:00:00    3.4561    2.3727   0.0000   6.64   NaN  78.1338  258.3505
        2007-01-19 04:00:00    3.6906    2.5446   0.0000   5.96   NaN  80.6836  258.9535
        2007-01-19 05:00:00    3.9359    2.7203   0.0000   6.72   NaN  83.2603  259.6137
        2007-01-19 06:00:00    4.2034    2.7348   0.0000   7.24   NaN  85.9773  260.2969
        2007-01-19 07:00:00    4.5018    2.8931   0.0057   7.44   NaN  88.9021  260.9651
        2007-01-19 08:00:00    4.7756    3.4033   0.0158   6.60   NaN  91.4986  261.5183
        2007-01-19 09:00:00    4.9946    3.9310   0.0239   6.48   NaN  93.5192  262.0343
        2007-01-19 10:00:00    5.1346    4.1923   0.0300   5.36   NaN  94.7865  262.4276
        2007-01-19 11:00:00    5.1242    4.2232   0.0340   2.96   NaN  94.6928  262.5975
        2007-01-19 12:00:00    4.9236    4.0355   0.0360   1.32   NaN  92.8691  262.6214
        2007-01-19 13:00:00    4.6138    3.5419   0.0360   1.08   NaN  89.9744  262.6241
        2007-01-19 14:00:00    4.2787    3.1272   0.0340   0.48   NaN  86.7255  262.5745
        2007-01-19 15:00:00    3.9910    2.9025   0.0300   1.60   NaN  83.8279  262.6235
        2007-01-19 16:00:00    3.7529    2.7505   0.0239   1.00   NaN  81.3466  262.6202
        2007-01-19 17:00:00    3.5319    2.6294   0.0158   0.72   NaN  78.9680  262.5932
        2007-01-19 18:00:00    3.3168    2.4929   0.0057   0.80   NaN  76.5767  262.5743

        Cas avec des données au pas de temps journalier (mm: mm par jour)

        >>> f = 'data/model/grp22/rt/intern/PQE_1A_D_journalier.DAT'
        >>> reader = GRPRT_Intern(filename=f)
        >>> df = reader.read()
        >>> df
                    Qsim(mm)  Qobs(mm)  ETP(mm)  P(mm)  T(C)    R(mm)     S(mm)
        DATE (TU)
        2007-01-17    5.1060       NaN   0.2546   0.40   NaN  22.4859  244.9885
        2007-01-18    5.3363    6.0837   0.2727   7.84   NaN  22.9351  244.9837
        2007-01-19   23.3179   13.6173   0.2909  70.08   NaN  42.8322  254.6694
        2007-01-20   61.3244   68.1413   0.2818  82.96   NaN  60.9440  261.5377
        2007-01-21   49.2584   33.2340   0.2091  10.48   NaN  56.5619  260.4014
        2007-01-22   24.8283   22.5610   0.2000   9.00   NaN  43.8974  259.2471
        2007-01-23   15.1648   14.3262   0.2000   2.04   NaN  36.0085  257.4833
        2007-01-24    9.7221   10.6448   0.1818   1.12   NaN  29.8515  255.6995


        """
        with open(self.filename, 'r', encoding='iso-8859-15') as f:
            f.readline()
            f.readline()
            x = f.readline().split(';')[0]
        df = pnd.read_csv(
            self.filename,
            sep=';',
            header=0,
            skiprows=[0, 1, 3, 4],
            index_col=False,
            dtype={x: str},
            na_values=[-99.9, '-99.9000', -9.9900, '-9.9900'],
        )
        df.set_index(x, inplace=True)
        df.index.name = 'DATE (TU)'
        # Nettoyer la colonne superflue
        df.drop(columns=df.columns[-1], inplace=True)
        # Nettoyer les intitulés des colonnes
        df.columns = [c.strip() for c in df.columns]
        df.index.name = df.index.name.strip()
        lendt = list({len(i) for i in df.index})[0]
        if lendt == 12:
            fmt = '%Y%m%d%H%M'
        elif lendt == 10:
            fmt = '%Y%m%d%H'
        elif lendt == 8:
            fmt = '%Y%m%d'
        else:
            raise NotImplementedError
        df.index = pnd.to_datetime(df.index, format=fmt)
        return df

    def write(self, data=None):
        """
        Ecrire le fichier Interne de GRP Temps-Réel / Temps-Différé
        """
        raise NotImplementedError

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        de données GRP Intern (GRP *Temps-Réel*)

        Parameters
        ----------
        filename : str
            Fichier de données GRP Archive (GRP *Temps-Réel*)

        Returns
        -------
        realtime : bool
            Temps-réel (True) ou Temps différé (False)

        Examples
        --------
        >>> from pyspc.model.grp22 import GRPRT_Intern
        >>> f = 'data/model/grp22/rt/intern/PQE_1A.DAT'
        >>> realtime = GRPRT_Intern.split_basename(filename=f)
        >>> realtime
        True
        >>> f = 'data/model/grp22/rt/intern/PQE_1A_D.DAT'
        >>> realtime = GRPRT_Intern.split_basename(filename=f)
        >>> realtime
        False

        """
        if filename is None:
            return None
        x = os.path.splitext(os.path.basename(filename))[0].split('_')
        return len(x) != 3 or x[-1] != 'D'

    def check_datatype(self, datatype):
        """
        Contrôler la cohérence entre le type et le nom du fichier

        Parameters
        ----------
        datatype : str
            Type de fichier

        Raises
        ------
        ValueError
            Si incohérence entre le type et le nom du fichier

        """
        test_file = self.split_basename(filename=self.filename)
        test_dtype = RT_INTERN_DTYPES[datatype]
        if (test_file and not test_dtype) or (not test_file and test_dtype):
            raise ValueError("Incohérence entre le type et le nom du fichier")

    @staticmethod
    def get_types():
        """
        Type de fichier de variable interne GRP Temps-réel

        - intern      : variable interne temps-réel
        - intern_diff : variable interne temps différé
        """
        return sorted(RT_INTERN_DTYPES.keys())
