#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2018 - Prévisions
"""
from datetime import datetime as dt
import os.path
import pandas as pnd

from pyspc.convention.grp22 import CAL_FCST_DATEFORMAT


class GRP_Fcst():
    """
    Structure de fichier de prévision produit par GRP (*Calage*)

    Attributes
    ----------
    filename : str
        Nom du fichier de données
    station : str
        Identifiant de la station
    model : str
        Nom du modèle (GRP)
    snow : str
        Avec/Sans module neige
    error : str
        Modèle d'erreur (RNA, TAN)
    leadtime : str
        Echeance de prévision
    timestep : str
        Pas de temps de calcul
    threshold : str
        Seuil de calage
    rainfall : str
        Origine des pluies (P0: plue nulle, PP: pluie parfaite)
    periods : str
        Périodes de calage et d'application

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRP_Fcst

        Parameters
        ----------
        filename : str
            Nom du fichier de données

        """
        self.filename = filename
        if self.filename is not None:
            (self.station, self.model, self.snow, self.error, self.timestep,
             self.leadtime, self.threshold, self.rainfall,
             self.periods) = self.split_basename(self.filename)
        else:
            self.station = None
            self.model = None
            self.snow = None
            self.error = None
            self.timestep = None
            self.leadtime = None
            self.threshold = None
            self.rainfall = None
            self.periods = None

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRP_Fcst
        """
        text = """
        *************************************
        *********** GRP 2018 - Fcst *********
        *************************************
        *  NOM FICHIER      = {filename}
        *  CODE STATION     = {station}
        *  MODEL            = {model}
        *  NEIGE            = {snow}
        *  TRAIT. ERREUR    = {error}
        *  PAS DE TEMPS     = {timestep}
        *  HORIZON CALAGE   = {leadtime}
        *  SEUIL CALAGE     = {threshold}
        *  SOURCE PRECIP    = {rainfall}
        *  PERIODES         = {periods}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lecture du fichier de prévision produit par GRP (*Calage*)

        Returns
        -------
        pandas.DataFrame
            Tableau des données de prévision de GRP 2018

        Examples
        --------
        >>> from pyspc.model.grp22 import GRP_Fcst
        >>> f = 'data/model/grp22/cal/H_RH10585x_GRP_SMN_TAN_PDT_00J01H00M_HOR_00J03H00M_Scal_5d00_PP_P0P0.TXT'
        >>> reader = GRP_Fcst(filename=filename)
        >>> reader
        *************************************
        *********** GRP 2018 - Fcst *********
        *************************************
        *  NOM FICHIER      = data/model/grp22/cal/H_RH10585x_GRP_SMN_TAN_PDT_00J01H00M_HOR_00J03H00M_Scal_5d00_PP_P0P0.TXT
        *  CODE STATION     = RH10585x
        *  MODEL            = GRP
        *  NEIGE            = SMN
        *  TRAIT. ERREUR    = TAN
        *  PAS DE TEMPS     = 00J01H00M
        *  HORIZON CALAGE   = 00J03H00M
        *  SEUIL CALAGE     = 5d00
        *  SOURCE PRECIP    = PP
        *  PERIODES         = P0P0
        *************************************

        >>> df = reader.read()
        >>> df
                             OBS00J00H00M  ...  PRV05J00H00M
              DATE                         ...
        2007-01-18 12:00:00        2.8230  ...        2.7671
        2007-01-18 13:00:00        2.7738  ...        2.7381
        2007-01-18 14:00:00        2.7246  ...        2.6924
        2007-01-18 15:00:00        2.7100  ...        2.5725
        2007-01-18 16:00:00        2.7300  ...        2.4280
        2007-01-18 17:00:00        2.8962  ...        2.3638
        2007-01-18 18:00:00        3.5375  ...        2.3488
        2007-01-18 19:00:00        5.1501  ...        2.5162
        2007-01-18 20:00:00        7.8640  ...        2.6129
        2007-01-18 21:00:00       10.1920  ...        2.4551
        2007-01-18 22:00:00       12.4640  ...        2.3413
        2007-01-18 23:00:00       14.6950  ...        2.2893
        2007-01-19 00:00:00       16.6250  ...        2.2380
        2007-01-19 01:00:00       18.2500  ...        2.1963
        2007-01-19 02:00:00       19.7100  ...        2.2121
        2007-01-19 03:00:00       20.4315  ...        2.2088
        2007-01-19 04:00:00       21.9115  ...        2.2536
        2007-01-19 05:00:00       23.4250  ...        2.2863
        2007-01-19 06:00:00       23.5500  ...        2.3005
        2007-01-19 07:00:00       24.9125  ...        2.4172
        2007-01-19 08:00:00       29.3062  ...        2.5941
        2007-01-19 09:00:00       33.8499  ...        2.6126
        2007-01-19 10:00:00       36.1000  ...        2.5266
        2007-01-19 11:00:00       36.3666  ...        2.4600
        2007-01-19 12:00:00       34.7500  ...        2.4060
        2007-01-19 13:00:00       30.4996  ...        2.3009
        2007-01-19 14:00:00       26.9284  ...        2.2741
        2007-01-19 15:00:00       24.9933  ...        2.2784
        2007-01-19 16:00:00       23.6850  ...        2.2537
        2007-01-19 17:00:00       22.6416  ...        2.2274
        2007-01-19 18:00:00       21.4666  ...        2.1885

        [31 rows x 33 columns]

        """
        df = pnd.read_csv(
            self.filename,
            sep=';',
            header=0,
            skiprows=5,
            index_col=0,
            parse_dates=True,
            date_parser=lambda x: dt.strptime(x, CAL_FCST_DATEFORMAT)
        )
        return df

    def write(self):
        """
        Ecriture du fichier de prévision produit par GRP (*Calage*)
        """
        raise NotImplementedError

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        de prévision produit par GRP (*Calage*)

        Parameters
        ----------
        filename : str
            Fichier de prévision produit par GRP (*Calage*)

        Returns
        -------
        station : str
            Identifiant de la station
        model : str
            Nom du modèle (GRP)
        snow : str
            Avec/Sans module neige
        error : str
            Modèle d'erreur (RNA, TAN)
        timestep : str
            Pas de temps de calcul
        leadtime : str
            Echeance de prévision
        threshold : str
            Seuil de calage
        rainfall : str
            Origine des pluies (P0: plue nulle, PP: pluie parfaite)
        periods : str
            Périodes de calage et d'application

        Examples
        --------
        >>> from pyspc.model.grp22 import GRP_Fcst
        >>> f = 'data/model/grp22/cal/H_RH10585x_GRP_SMN_TAN_PDT_00J01H00M_HOR_00J03H00M_Scal_5d00_PP_P0P0.TXT'
        >>> (station, model, snow, error, timestep, leadtime, threshold,
        ...  rainfall, periods) = GRP_Fcst.split_basename(filename=f)
        >>> station
        RH10585x
        >>> model
        GRP
        >>> snow
        SMN
        >>> error
        TAN
        >>> timestep
        00J01H00M
        >>> leadtime
        00J03H00M
        >>> threshold
        5d00
        >>> rainfall
        PP
        >>> periods
        P0P0

        """
        if filename is None:
            return None, None
        basename = os.path.splitext(os.path.basename(filename))[0]
        try:
            [_, station, model, snow, error, _, timestep, _, leadtime,
             _, threshold, rainfall, periods] = basename.split('_')
        except ValueError as ve:
            raise ValueError("Le nom de fichier ne respecte pas le "
                             "nommage de GRP") from ve
        return (station, model, snow, error, timestep, leadtime, threshold,
                rainfall, periods)
