#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2020 - Temps-réel configuration
"""
import collections
import os.path

from pyspc.core.config import Config
from pyspc.convention.grp20 import RT_CONFIG_DATATYPES, RT_CONFIG_KEYS


class GRPRT_Cfg(Config):
    """
    Structure de données GRPRT Cfg (Prévision GRP *Temps Réel*)

    - config_prevision.ini
    - Fichiers_sortie_GRP.ini

    Attributes
    ----------
    filename : str
        Nom du fichier Config de GRP *Temps-Réel*
    datatype : str
        Type du fichier Config de GRP *Temps-Réel*

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRPRT_Cfg

        Parameters
        ----------
        filename : str
            Nom du fichier Config de GRP *Temps-Réel*

        """
        super().__init__(filename=filename)
        self.datatype = self.get_datatype(filename=filename)

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRPRT_Cfg
        """
        text = """
        *************************************
        *********** GRP 2020 - RT Cfg *******
        *************************************
        *  NOM FICHIER      = {filename}
        *  TYPE FICHIER     = {datatype}
        *  CONFIGURATION {content}
        *************************************
        """
        content = ""
        for k, v in self.items():
            if isinstance(v, str):
                content += '\n        *      + {0} = {1}'.format(k, v)
            elif isinstance(v, (dict, collections.OrderedDict)):
                content += '\n        *      + {0}'.format(k)
                for k2, v2 in v.items():
                    content += '\n        *          + {0} = {1}'.format(k2,
                                                                         v2)
        return text.format(filename=self.filename, datatype=self.datatype,
                           content=content)

    def write(self, encoding='utf-8', newline='\r\n'):
        """
        Ecrire le fichier de configuration GRP Temps Réel

        Parameters
        ----------
        encoding : str
            Encodage du fichier de configuration, 'utf-8' par défaut
        newline : str
            Charactère de nouvelle ligne, '\r\n' par défaut

        """
        return super().write(encoding=encoding, newline=newline)

    @classmethod
    def get_datatype(cls, filename=None):
        """
        Définir le type de fichier selon son nom

        Parameters
        ----------
        filename : str
            Nom du fichier Config de GRP *Temps-Réel*

        Returns
        -------
        datatype : str
            Type du fichier Config de GRP *Temps-Réel*

        See Also
        --------
        pyspc.convention.grp20.RT_CONFIG_DATATYPES

        """
        if not isinstance(filename, str):
            return None
        b = os.path.basename(filename)
        for k, v in RT_CONFIG_DATATYPES.items():
            if k in b:
                return v
        return None

    def check_cfg_keys(self):
        """
        Tester les clés du fichier de configuration

        Raise
        -----
        ValueError
            Si une clé (section, option) est incorrecte

        """
        fso = set(self.list_sections_options())
        tso = set(self.get_cfg_keys(filename=self.filename))
        if fso.difference(tso):
            raise ValueError(
                "Les clés suivantes sont incorrectes : {}"
                "".format(list(fso.difference(tso))))
        if tso.difference(fso):
            raise ValueError(
                "Les clés suivantes sont manquantes : {}"
                "".format(list(tso.difference(fso))))

    @classmethod
    def get_cfg_keys(cls, filename=None,
                     config_run=False, config_outputs=False):
        """
        Récupérer les mots clés de la configuration GRP Temps Réel
        dans l'ordre d'apparition

        Parameters
        ----------
        filename : str
            Nom du fichier Config de GRP *Temps-Réel*

        Returns
        -------
        list
            Liste des balises du fichier de configuration GRP Temps Réel

        """
        try:
            return RT_CONFIG_KEYS[cls.get_datatype(filename=filename)]
        except Exception:
            if config_run:
                return RT_CONFIG_KEYS['rt_config_run']
            if config_outputs:
                return RT_CONFIG_KEYS['rt_config_outputs']
            return None
