#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2018 - Temps-réel paramètre
"""
import collections
import copy as _copy
from pyspc.convention.grp20 import RT_PARAM_LIST


class GRPRT_Param(collections.OrderedDict):
    """
    Structure de données GRP Param (Fichier Param de GRP *Temps Réel*)

    - PARAM.DAT

    Attributes
    ----------
    filename : str
        Nom du fichier Param de GRP *Temps-Réel*

    Notes
    -----
    Cette structure de données est un dictionnaire ordonné. La balise d'une
    information est la clé et la structure contient les méta-données dans ses
    valeurs

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRPRT_Param

        Parameters
        ----------
        filename : str
            Nom du fichier Param de GRP *Temps-Réel*

        """
        super().__init__()
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRPRT_Param
        """
        text = """
        *************************************
        *********** GRP 2018 - RT Param *****
        *************************************
        *  NOM FICHIER      = {filename}
        *  INFORMATIONS     = {content}
        *************************************
        """
        info = {'content': dict(self)}
        return text.format(filename=self.filename, **info)

    def read(self):
        """
        Lecture du fichier Param de GRP *Temps *Réel*

        Examples
        --------
        >>> from pyspc.model.grp20.rt_param import GRPRT_Param
        >>> f = 'data/model/grp20/rt/PARAM.DAT'
        >>> param = GRPRT_Param(filename=f)
        >>> param.read()
        >>> param
        *************************************
        *********** GRP 2018 - RT Param *****
        *************************************
        *  NOM FICHIER      = data/model/grp20/rt/PARAM.DAT
        *  INFORMATIONS     = {'P': OrderedDict([('INTER (mm)', 1.75),
                                                 ('ROUT (mm)', 1844.567),
                                                 ('CORR (-)', 1.139),
                                                 ('TB (h)', 2.713)]),
                               'M': 'TANGARA', 'C': 0.45}
        *************************************

        >>> param['P']
        {'INTER (mm)': 1.750, 'ROUT (mm)': 1844.567, 'CORR (-)': 1.139,
         'TB (h)': 2.713}

        """
        self.clear()
        info_basin = collections.OrderedDict()
        params = _copy.deepcopy(RT_PARAM_LIST)
        with open(self.filename, 'r', encoding='iso-8859-1') as f:
            for line in f.readlines():
                # Une ligne de commentaire commence par "#"
                if line.startswith("#"):
                    continue
                # Balise
                tag = line[0]
                # Découpage de la ligne
                x = line.split("!", 1)[0][2:].strip()
                # Paramètres du modèle GRP
                if tag in ['P']:
                    info_basin.setdefault(tag, collections.OrderedDict())
                    try:
                        n = params.pop(0)
                    except IndexError:
                        continue
                    info_basin[tag].setdefault(n, float(x))
                # Correction Erreurs (0: Tangara; 1: RNA)
                elif tag in ['M']:
                    if x == '0':
                        info_basin[tag] = 'TANGARA'
                    elif x == '1':
                        info_basin[tag] = 'RNA'
                elif tag in ['C']:
                    info_basin[tag] = float(x)
        self.update(info_basin)

    def write(self):
        """
        Ecriture du fichier Param de GRP *Temps *Réel*
        """
        raise NotImplementedError
