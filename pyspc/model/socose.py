#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Méthode SOCOSE
"""
import pyspc.core.exception as _exception


def socose(df=None):
    """
    Déterminer les paramètrs D et RXD du modèle SOCOSE.
    - D : durée au-dessus du débit standardisé valant 0.5 (Q/Qp)
    - RXD : rapport du débit de pointe sur le débit moyen calculé sur
      la durée D.

    Parameters
    ----------
    df : pandas.DataFrame
        Hydrogrammes synthétiques

    Returns
    -------
    socose : dict
        Valeurs SOCOSE par colonne du tableau DataFrame
        - col: {'d': d, 'rxd': rxd}

    Examples
    --------
    >>> df
                       (K0000000, QH)
    -1 days +17:00:00        0.167761
    -1 days +18:00:00        0.185996
    -1 days +19:00:00        0.223195
    -1 days +20:00:00        0.390956
    -1 days +21:00:00        0.674690
    -1 days +22:00:00        0.870897
    -1 days +23:00:00        0.965718
    00:00:00                 1.000000
    01:00:00                 0.983953
    02:00:00                 0.945295
    03:00:00                 0.884026
    04:00:00                 0.814734
    05:00:00                 0.744712
    06:00:00                 0.683443
    07:00:00                 0.617068
    08:00:00                 0.558716
    09:00:00                 0.506929
    10:00:00                 0.466083
    11:00:00                 0.431072
    12:00:00                 0.401167
    13:00:00                 0.371991
    14:00:00                 0.350839
    15:00:00                 0.334063
    16:00:00                 0.317287
    17:00:00                 0.305616

    >>> values = socose(df)
    >>> values
    {('K0000000', 'QH'): {'d': 13, 'rxd': 1.268270287129564}}

    """
    _exception.check_dataframe(df)
    values = {}
    for c in df.columns:
        d = (df[c] >= 0.5).sum()
        rxd = df[c].max() / df[df[c] >= 0.5][c].mean()
        values.setdefault(c, {'d': d, 'rxd': rxd})
    return values
