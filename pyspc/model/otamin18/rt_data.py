#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Incertitudes de modélisation - Projet OTAMIN v2016 - Fichier prv
"""

from pyspc.model.otamin16 import RT_Data as _RT_Data_16


class RT_Data(_RT_Data_16):
    """
    Classe permettant la manipulation du prv OTAMIN v2018

    Attributes
    ----------
    filename : str
        Nom du fichier prv de OTAMIN v2018
    datatype : str
        Type du fichier de données

    """

    def __init__(self, filename=None, datatype=None):
        """
        Initialiser l'instance de la classe RT_Data (prv) de Otamin v2018

        Parameters
        ----------
        filename : str
            Nom du fichier prv de OTAMIN v2018
        datatype : str
            Type du fichier de données

        """
        super().__init__(filename=filename, datatype=datatype)

    def __str__(self):
        """
        Afficher des méta-données de l'instance Data (prv) de Otamin v2018
        """
        text = """
        *************************************
        ***** OTAMIN 2018 - RT_Data (prv) ***
        *************************************
        *  NOM FICHIER      = {filename}
        *  TYPE DE FICHIER  = {datatype}
        *************************************
        """
        return text.format(**vars(self))
