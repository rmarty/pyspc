#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Incertitudes de modélisation - Projet OTAMIN v2018 - Fichier Abaque
"""
import os.path
import pandas as pnd

from pyspc.convention.otamin18 import CAL_TDELTA


class Table():
    """
    Classe permettant la manipulation du csv OTAMIN v2018 (Abaque)

    Attributes
    ----------
    filename : str
        Nom du fichier csv OTAMIN v2018 (Calage)
    station : str
        Code du lieu
    model : str
        Code du modèle selon la convention POM
    method : str
        Méthode d'estimation
    error : str
        Type d'erreur
    datatype : str
        Type de fichier
    """

    def __init__(self, filename=None):
        """
        Initialiser l'instance de la classe Table (csv, Rdata) de Otamin v2018

        Parameters
        ----------
        filename : str
            Nom du fichier prv de OTAMIN v2018

        """
        self.filename = filename
        if self.filename is not None:
            meta = self.split_basename(self.filename)
            self.station = meta[0]
            self.model = meta[1]
            self.method = meta[2]
            self.error = meta[3]
            self.datatype = meta[4]
        else:
            self.station = None
            self.model = None
            self.method = None
            self.error = None
            self.datatype = None

    def __str__(self):
        """
        Afficher des méta-données de l'instance Data (csv) de Otamin v2018
        """
        text = """
        *************************************
        ***** OTAMIN 2018 - Table ***********
        *************************************
        *  NOM FICHIER      = {filename}
        *  STATION          = {station}
        *  MODELE           = {model}
        *  METHODE          = {model}
        *  TYPE ERREUR      = {error}
        *  TYPE FICHIER     = {datatype}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lire un fichier Abaque csv Otamin

        Returns
        -------
        data : pnd.DataFrame
            Dataframe des données

        Examples
        --------
        >>> from pyspc.verification.otamin16 import Table
        >>> f = 'data/model/otamin16/A6701210_57gGRPd000_QUOIQUE_EM_INC_TAB.csv'
        >>> d = Table(filename=f)
        >>> print(d)
        *************************************
        ***** OTAMIN 2018 - Table ***********
        *************************************
        *  NOM FICHIER      = data/model/otamin18/A6701210_57gGRPd000_QUOIQUE_EM_INC_TAB.csv
        *  STATION          = A6701210
        *  MODELE           = 57gGRPd000
        *  METHODE          = 57gGRPd000
        *  TYPE ERREUR      = EM
        *  TYPE FICHIER     = TAB
        *************************************

        >>> df = d.read()
        >>> df
                  HorPrevi   df_val     U10  ...      U70      U80      U90
        0  0 days 12:00:00   21.550  15.838  ...   23.821   25.361   27.947
        1  0 days 12:00:00   22.662  16.654  ...   25.049   26.669   29.388
        2  0 days 12:00:00   23.935  17.590  ...   26.457   28.167   31.040
        3  0 days 12:00:00   25.481  18.727  ...   28.166   29.987   33.045
        4  0 days 12:00:00   26.924  19.787  ...   29.761   31.685   34.915
        5  0 days 12:00:00   28.791  21.159  ...   31.824   33.882   37.337
        6  0 days 12:00:00   32.765  24.080  ...   36.217   38.559   42.490
        7  0 days 12:00:00   38.384  28.209  ...   42.428   45.172   49.778
        8  0 days 12:00:00   45.088  33.136  ...   49.839   53.061   58.471
        9  0 days 12:00:00  123.543  90.794  ...  136.560  145.389  160.214
        10 1 days 00:00:00   21.187  14.825  ...   23.404   25.384   29.113
        11 1 days 00:00:00   22.076  15.446  ...   24.386   26.449   30.334
        12 1 days 00:00:00   23.287  16.294  ...   25.724   27.901   31.999
        13 1 days 00:00:00   24.542  17.172  ...   27.110   29.404   33.723
        14 1 days 00:00:00   26.042  18.222  ...   28.767   31.201   35.784
        15 1 days 00:00:00   27.819  19.465  ...   30.731   33.331   38.226
        16 1 days 00:00:00   30.995  21.687  ...   34.238   37.135   42.589
        17 1 days 00:00:00   36.257  25.369  ...   40.052   43.440   49.821
        18 1 days 00:00:00   44.937  31.442  ...   49.640   53.839   61.747
        19 1 days 00:00:00  138.550  96.944  ...  153.049  165.997  190.379
        [20 rows x 11 columns]

        """
        df = pnd.read_csv(self.filename, sep=';', index_col=False)
        df['HorPrevi'] = df['HorPrevi'].apply(
            lambda x: int(float(x[:-1])) * CAL_TDELTA[x[-1]])
        return df

    def write(self, data=None):
        """
        Ecrire un fichier Abaque csv Otamin

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        csv OTAMIN v2018 (Abaque)

        Parameters
        ----------
        filename : str
            Fichier csv OTAMIN v2018 (Calage)

        Returns
        -------
        station : str
            Code du lieu
        model : str
            Code du modèle selon la convention POM
        method : str
            Méthode d'estimation
        error : str
            Type d'erreur
        datatype : str
            Type de fichier

        """
        if filename is None:
            return None, None, None, None, None
        basename = os.path.basename(filename)
        if basename.upper().endswith('RDATA'):
            raise NotImplementedError('Fichier RData pas implémenté')
        basename = basename.replace('.csv', '')
        try:
            [station, model, method, error, _, datatype] = basename.split('_')
        except ValueError as ve:
            raise ValueError("Le nom de fichier ne respecte pas le "
                             "nommage de OTAMIN") from ve
        return station, model, method, error, datatype

    @staticmethod
    def join_basename(station=None, model=None, leadtime=None):
        """
        Extraire les informations depuis le nom du fichier
        csv OTAMIN v2018 (Abaque)

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError
