#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Incertitudes de modélisation - Projet OTAMIN v2018 - Fichier Calage
"""
from datetime import datetime as dt, timedelta as td
import os.path
from pyspc.convention.otamin18 import CAL_TDELTA
from pyspc.model.otamin16 import Data as _Data_16


DATE_FORMAT = '%d-%m-%Y %H:%M'
"""Format des dates dans les fichiers csv OTAMIN v2018 (Calage)"""


def date_parser(txt):
    """"Convertisseur de date"""
    return dt.strptime(txt, DATE_FORMAT)


class Data(_Data_16):
    """
    Classe permettant la manipulation du csv OTAMIN v2018 (Calage)

    Attributes
    ----------
    filename : str
        Nom du fichier csv OTAMIN v2018 (Calage)
    station : str
        Code du lieu
    model : str
        Code du modèle selon la convention POM
    leadtime : timedelta
        Echéance de prévision
    """

    def __init__(self, filename=None):
        """
        Initialiser l'instance de la classe Data (csv) de Otamin v2018

        Parameters
        ----------
        filename : str
            Nom du fichier prv de OTAMIN v2018

        """
        super().__init__(filename=filename)
        self.filename = filename
        if self.filename is not None:
            meta = self.split_basename(self.filename)
            self.station = meta[0]
            self.model = meta[1]
            self.leadtime = meta[2]
        else:
            self.station = None
            self.model = None
            self.leadtime = None

    def __str__(self):
        """
        Afficher des méta-données de l'instance Data (csv) de Otamin v2018
        """
        text = """
        *************************************
        ***** OTAMIN 2018 - Data (csv) ******
        *************************************
        *  NOM FICHIER      = {filename}
        *  STATION          = {station}
        *  MODELE           = {model}
        *  ECHEANCE         = {leadtime}
        *************************************
        """
        return text.format(**vars(self))

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        csv OTAMIN v2018 (Calage)

        Parameters
        ----------
        filename : str
            Fichier csv OTAMIN v2018 (Calage)

        Returns
        -------
        station : str
            Code du lieu
        model : str
            Code du modèle selon la convention POM
        leadtime : timedelta
            Echéance de prévision

        """
        if filename is None:
            return None, None, None, None
        basename = os.path.basename(filename).replace('.csv', '')
        try:
            [station, model, leadtime] = basename.split('_')
        except ValueError as ve:
            raise ValueError("Le nom de fichier ne respecte pas le "
                             "nommage de OTAMIN") from ve
        try:
            ts = CAL_TDELTA[leadtime[-1]]
        except KeyError as ke:
            raise ValueError("Le pas de temps ne respecte pas le "
                             "nommage de OTAMIN") from ke
        leadtime = int(float(leadtime[:-1])) * ts
        return station, model, leadtime

    @staticmethod
    def join_basename(station=None, model=None, leadtime=None, timestep=None):
        """
        Extraire les informations depuis le nom du fichier
        csv OTAMIN v2018 (Calage)

        Parameters
        ----------
        station : str
            Code du lieu
        model : str
            Code du modèle selon la convention POM
        leadtime : timedelta
            Echéance de prévision
        timestep : timedelta
            Unité de l'échéance

        Returns
        -------
        filename : str
            Fichier csv OTAMIN v2018 (Calage)

        """
        if station is None or model is None or not isinstance(
                leadtime, td) or not isinstance(timestep, td):
            raise ValueError('Définition incorrecte des arguments')
        try:
            ts = [k for k, v in CAL_TDELTA.items() if v == timestep]
            ts = ts[0]
        except IndexError as ie:
            raise ValueError("Le pas de temps ne respecte pas le "
                             "nommage de OTAMIN") from ie
        leadtime = int(float(leadtime / CAL_TDELTA[ts]))
        return '{0}_{1}_{2:03d}{3}.csv'.format(station, model, leadtime, ts)
