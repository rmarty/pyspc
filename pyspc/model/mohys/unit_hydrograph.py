#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - MOHYS (SPC LACI) - Hydrogrammes unitaires
"""
import inspect
import math
import pandas as pnd
from scipy.stats import (binom, dgamma, dlaplace, gamma, nbinom, norm, skellam,
                         triang, uniform, weibull_min)

import pyspc.core.exception as _exception


def build(start=0, end=0, params=None, norm=True):
    """
    Construire un hydrogramme unitaire.

    Parameters
    ----------
    start : int
        Premier instant
    end : int
        Dernier instant
    params : dict
        Formulations demandées et leurs paramètres

        - clé : str
        - valeurs : list

    norm : bool
        Normaliser les hydrogrammes unitaires ?
        Par défaut: True.
        Si True, la somme des coefficients est égale à 1.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogrammes unitaires

    See Also
    --------
    pyspc.model.mohys.unit_hydrograph.plot

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> start = -11
    >>> end = 10
    >>> params = {
    ...     'FBN': [-6, 12, 4, 0.3],
    ...     'FGT': [-6, 12, 4],
    ...     'FPU': [-6, 12, 4],
    ...     'RE1': [-6, 12],
    ...     'RE2': [-6, 12, -12, 24],
    ...     'TG1': [0.75, -6, 12],
    ...     'TG2': [0.50, -6, 12, 0.75, -12, 24],
    ... }


    Cas avec normalisation (cas par défaut)

    >>> df = uh.build(start=start, end=end, params=params)
    >>> df = uh.build(start=start, end=end, params=params, norm=True)
    >>> df
              FBN       FGT       FPU       RE1       RE2       TG1       TG2
    -11  0.000000  0.000000  0.000000  0.000000  0.028571  0.000000  0.003195
    -10  0.000000  0.000000  0.000000  0.000000  0.028571  0.000000  0.006390
    -9   0.000000  0.000000  0.000000  0.000000  0.028571  0.000000  0.009585
    -8   0.000000  0.000000  0.000000  0.000000  0.028571  0.000000  0.012780
    -7   0.000000  0.000000  0.000000  0.000000  0.028571  0.000000  0.015974
    -6   0.000000  0.000000  0.000000  0.076923  0.057143  0.000000  0.019169
    -5   0.000228  0.000068  0.001502  0.076923  0.057143  0.018519  0.022364
    -4   0.005077  0.001529  0.012012  0.076923  0.057143  0.037037  0.038339
    -3   0.034221  0.010598  0.040541  0.076923  0.057143  0.055556  0.057508
    -2   0.132888  0.046628  0.096096  0.076923  0.057143  0.074074  0.076677
    -1   0.292167  0.147720  0.187688  0.076923  0.057143  0.092593  0.095847
     0   0.304650  0.293457  0.324324  0.076923  0.057143  0.111111  0.115016
     1   0.158333  0.293457  0.187688  0.076923  0.057143  0.129630  0.095847
     2   0.054031  0.147720  0.096096  0.076923  0.057143  0.148148  0.076677
     3   0.014715  0.046628  0.040541  0.076923  0.057143  0.166667  0.057508
     4   0.003210  0.010598  0.012012  0.076923  0.057143  0.111111  0.051118
     5   0.000459  0.001529  0.001502  0.076923  0.057143  0.055556  0.054313
     6   0.000020  0.000068  0.000000  0.076923  0.057143  0.000000  0.057508
     7   0.000000  0.000000  0.000000  0.000000  0.028571  0.000000  0.047923
     8   0.000000  0.000000  0.000000  0.000000  0.028571  0.000000  0.038339
     9   0.000000  0.000000  0.000000  0.000000  0.028571  0.000000  0.028754
     10  0.000000  0.000000  0.000000  0.000000  0.028571  0.000000  0.019169

    Cas sans normalisation

    >>> df = uh.build(start=start, end=end, params=params, norm=False)
    >>> df
              FBN       FGT       FPU       RE1       RE2       TG1       TG2
    -11  0.000000  0.000000  0.000000  0.000000  0.041667  0.000000  0.004630
    -10  0.000000  0.000000  0.000000  0.000000  0.041667  0.000000  0.009259
    -9   0.000000  0.000000  0.000000  0.000000  0.041667  0.000000  0.013889
    -8   0.000000  0.000000  0.000000  0.000000  0.041667  0.000000  0.018519
    -7   0.000000  0.000000  0.000000  0.000000  0.041667  0.000000  0.023148
    -6   0.000000  0.000000  0.000000  0.083333  0.083333  0.000000  0.027778
    -5   0.000228  0.000068  0.001543  0.083333  0.083333  0.018519  0.032407
    -4   0.005077  0.001529  0.012346  0.083333  0.083333  0.037037  0.055556
    -3   0.034221  0.010598  0.041667  0.083333  0.083333  0.055556  0.083333
    -2   0.132888  0.046628  0.098765  0.083333  0.083333  0.074074  0.111111
    -1   0.292167  0.147720  0.192901  0.083333  0.083333  0.092593  0.138889
     0   0.304650  0.293457  0.333333  0.083333  0.083333  0.111111  0.166667
     1   0.158333  0.293457  0.192901  0.083333  0.083333  0.129630  0.138889
     2   0.054031  0.147720  0.098765  0.083333  0.083333  0.148148  0.111111
     3   0.014715  0.046628  0.041667  0.083333  0.083333  0.166667  0.083333
     4   0.003210  0.010598  0.012346  0.083333  0.083333  0.111111  0.074074
     5   0.000459  0.001529  0.001543  0.083333  0.083333  0.055556  0.078704
     6   0.000020  0.000068  0.000000  0.083333  0.083333  0.000000  0.083333
     7   0.000000  0.000000  0.000000  0.000000  0.041667  0.000000  0.069444
     8   0.000000  0.000000  0.000000  0.000000  0.041667  0.000000  0.055556
     9   0.000000  0.000000  0.000000  0.000000  0.041667  0.000000  0.041667
     10  0.000000  0.000000  0.000000  0.000000  0.041667  0.000000  0.027778


    """
    _exception.check_numeric(start)
    _exception.check_numeric(end)
    _exception.raise_valueerror(start > end, "'start' et 'end' incorrects")
    if params is None:
        params = {n: [] for n in UH_DICT}
    _exception.check_dict(params)
    _exception.check_bool(norm)
    index = list(range(start, end+1))
    df = pnd.concat([UH_DICT[n](index, *params[n])
                     for n in params if n in UH_DICT],
                    axis=1)
    if norm:
        return df/df.sum()
    return df


def plot(df, subplots=False, filename='unit_hydrograph.png'):
    """
    Tracer un hydrogramme unitaire.

    Parameters
    ----------
    df : pandas.DataFrame
        Hydrogrammes unitaires
    subplots : bool
        1 sous figure par hydrogramme unitaire (True) ou les regrouper (False)
    filename : str
        Fichier à écrire

    Returns
    -------
    filename : str
        Fichier à écrire

    See Also
    --------
    pyspc.model.mohys.unit_hydrograph.build

    """
    figsize = (11.67, 8.27)
    if subplots:
        figsize = (8.27, 11.67)
    ax = df.plot(marker='o', subplots=subplots,
                 title="Hydrogramme Unitaire MOHYS",
                 figsize=figsize)
    if subplots:
        for sax in ax:
            sax.set_xticks(df.index)
            sax.set_xticklabels(df.index.astype(str))
            ylim = sax.get_ylim()
            sax.set_ylim((0, ylim[-1]))
            fig = sax.get_figure()
    else:
        ax.set_xticks(df.index)
        ax.set_xticklabels(df.index.astype(str))
        ylim = ax.get_ylim()
        ax.set_ylim((0, ylim[-1]))
        fig = ax.get_figure()
    fig.tight_layout()
    fig.savefig(filename, dpi=300)
    return filename


def FBN(index, loc=0, scale=1, alpha=1, kappa=1):
    """
    Hydrogramme unitaire selon Berthet (2010).

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    loc : int
        Départ. Défaut : 0.
    scale : int
        Echelle. Défaut : 1.
    alpha : float
        Forme. Défaut : 1.
    kappa : float
        Centrage. Défaut : 1.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> loc = -407.144
    >>> scale = 817.386
    >>> alpha = 117.451
    >>> kappa = 0.309
    >>> df = uh.FBN(index, loc, scale, alpha, kappa)
    >>> df
              FBN
    -11  0.002375
    -10  0.001839
    -9   0.003251
    -8   0.005726
    -7   0.010014
    -6   0.017302
    -5   0.029277
    -4   0.047839
    -3   0.073878
    -2   0.104697
    -1   0.131641
     0   0.142713
     1   0.131873
     2   0.105043
     3   0.074210
     4   0.048095
     5   0.029451
     6   0.017411
     7   0.010081
     8   0.005766
     9   0.003275
     10  0.001852

    >>> loc = -6
    >>> scale = 12
    >>> alpha = 4
    >>> kappa = 0.3
    >>> df = uh.FBN(index, loc, scale, alpha, kappa)
    >>> df
              FBN
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.000000
    -5   0.000228
    -4   0.005077
    -3   0.034221
    -2   0.132888
    -1   0.292167
     0   0.304650
     1   0.158333
     2   0.054031
     3   0.014715
     4   0.003210
     5   0.000459
     6   0.000020
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    """
    fitname = inspect.stack()[0][3]
    values = []
    for i in index:
        if i <= loc:
            values.append(0)
        elif i >= loc + scale:
            values.append(1)
        else:
            values.append(
                ((i - loc)**alpha) /
                ((i - loc)**alpha + kappa * (loc + scale - i)**alpha))
    cdf = pnd.DataFrame({fitname: values}, index=index)
    pdf = cdf.diff()
    pdf[fitname].iloc[0] = cdf[fitname].iloc[0]
    return pdf


def FEL(index, loc=0, scale=1):
    """
    Hydrogramme unitaire selon Le Moine - ELLIPTIQUE.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    loc : int
        Départ. Défaut : 0.
    scale : int
        Echelle. Défaut : 1.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> loc = -5.178
    >>> scale = 9.503
    >>> df = uh.FEL(index, loc, scale)
    >>> df
              FEL
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.000000
    -5   0.036329
    -4   0.088305
    -3   0.112629
    -2   0.126423
    -1   0.133003
     0   0.133442
     1   0.127802
     2   0.115195
     3   0.092822
     4   0.048701
     5   0.000000
     6   0.000000
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    >>> loc = -6
    >>> scale = 12
    >>> df = uh.FEL(index, loc, scale)
    >>> df
              FEL
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.000000
    -5   0.058651
    -4   0.079085
    -3   0.091888
    -2   0.100035
    -1   0.104619
     0   0.106103
     1   0.104619
     2   0.100035
     3   0.091888
     4   0.079085
     5   0.058651
     6   0.000000
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    """
    fitname = inspect.stack()[0][3]
    values = []
    for i in index:
        if i <= loc:
            values.append(0)
        elif i >= loc + scale:
            values.append(0)
        elif i <= loc + scale / 2:
            values.append(
                2 / (math.pi * scale / 2) * math.sqrt(
                    ((i - loc)/(scale / 2)) * (2 - ((i - loc)/(scale / 2))))
            )
        else:
            values.append(
                2 / (math.pi * scale / 2) * math.sqrt(
                    ((loc + scale - i)/(scale / 2))
                    * (2 - ((loc + scale - i)/(scale / 2))))
            )
    pdf = pnd.DataFrame({fitname: values}, index=index)
    return pdf


def FGT(index, loc=0, scale=1, alpha=1):
    """
    Hydrogramme unitaire selon TANGARA (2006).

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    loc : int
        Départ. Défaut : 0.
    scale : int
        Echelle. Défaut : 1.
    alpha : float
        Forme. Défaut : 1.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> loc = -3724.708
    >>> scale = 7448.433
    >>> alpha = 1070.317
    >>> df = uh.FGT(index, loc, scale, alpha)
    >>> df
              FGT
    -11  0.003802
    -10  0.001248
    -9   0.001661
    -8   0.002217
    -7   0.014601
    -6   0.022624
    -5   0.032278
    -4   0.046569
    -3   0.062500
    -2   0.098214
    -1   0.137363
     0   0.153846
     1   0.137363
     2   0.098214
     3   0.062500
     4   0.046569
     5   0.032278
     6   0.022624
     7   0.014520
     8   0.002252
     9   0.001681
     10  0.001259

    >>> loc = -6
    >>> scale = 12
    >>> alpha = 4
    >>> df = uh.FGT(index, loc, scale, alpha)
    >>> df
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.000000
    -5   0.000068
    -4   0.001529
    -3   0.010598
    -2   0.046628
    -1   0.147720
     0   0.293457
     1   0.293457
     2   0.147720
     3   0.046628
     4   0.010598
     5   0.001529
     6   0.000068
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    """
    fitname = inspect.stack()[0][3]
    values = []
    for i in index:
        if i <= loc:
            values.append(0)
        elif i >= loc + scale:
            values.append(1)
        elif i <= loc + scale / 2:
            values.append(
                (((i - loc)/(2 * scale / 2))**alpha)
                / (((i - loc)/(2 * scale / 2))**alpha
                   + (1-((i - loc)/(2 * scale / 2)))**alpha)
            )
        else:
            values.append(
                1 - (((loc + scale - i)/(2 * scale / 2))**alpha)
                / (((loc + scale - i)/(2 * scale / 2))**alpha
                   + (1-((loc + scale - i)/(2 * scale / 2)))**alpha))
    cdf = pnd.DataFrame({fitname: values}, index=index)
    pdf = cdf.diff()
    pdf[fitname].iloc[0] = cdf[fitname].iloc[0]
    return pdf


def FPA(index, loc=0, scale=1):
    """
    Hydrogramme unitaire selon Le Moine - PARABOLIQUE.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    loc : int
        Départ. Défaut : 0.
    scale : int
        Echelle. Défaut : 1.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> loc = -5.618
    >>> scale = 11.079
    >>> df = uh.FPA(index, loc, scale)
    >>> df
              FPA
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.000000
    -5   0.028524
    -4   0.067541
    -3   0.097733
    -2   0.119101
    -1   0.131645
     0   0.135364
     1   0.130259
     2   0.116330
     3   0.093577
     4   0.061999
     5   0.021597
     6   0.000000
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    >>> loc = -6
    >>> scale = 12
    >>> df = uh.FPA(index, loc, scale)
    >>> df
              FPA
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.000000
    -5   0.038194
    -4   0.069444
    -3   0.093750
    -2   0.111111
    -1   0.121528
     0   0.125000
     1   0.121528
     2   0.111111
     3   0.093750
     4   0.069444
     5   0.038194
     6   0.000000
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    """
    fitname = inspect.stack()[0][3]
    values = []
    for i in index:
        if i <= loc:
            values.append(0)
        elif i >= loc + scale:
            values.append(0)
        elif i <= loc + scale / 2:
            values.append(
                3 / (4 * scale / 2) * ((i - loc) / (scale / 2))
                * (2 - ((i - loc) / (scale / 2))))
        else:
            values.append(
                3 / (4 * scale / 2) * ((loc + scale - i) / (scale / 2))
                * (2 - ((loc + scale - i) / (scale / 2))))
    pdf = pnd.DataFrame({fitname: values}, index=index)
    return pdf


def FPU(index, loc=0, scale=1, alpha=1):
    """
    Hydrogramme unitaire selon Le Moine - PUISSANCE.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    loc : int
        Départ. Défaut : 0.
    scale : int
        Echelle. Défaut : 1.
    alpha : float
        Forme. Défaut : 1.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> loc = -239729269.709
    >>> scale = 479458539.860
    >>> alpha = 82122945.112
    >>> df = uh.FPU(index, loc, scale, alpha)
    >>> df
              FPU
    -11  0.003667
    -10  0.005165
    -9   0.007276
    -8   0.010248
    -7   0.014435
    -6   0.020332
    -5   0.028639
    -4   0.040340
    -3   0.056821
    -2   0.080036
    -1   0.112735
     0   0.158794
     1   0.131165
     2   0.093120
     3   0.066110
     4   0.046935
     5   0.033321
     6   0.023656
     7   0.016795
     8   0.011923
     9   0.008465
     10  0.006010

    >>> loc = -6
    >>> scale = 12
    >>> alpha = 4
    >>> df = uh.FPU(index, loc, scale, alpha)
    >>> df
              FPU
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.000000
    -5   0.001543
    -4   0.012346
    -3   0.041667
    -2   0.098765
    -1   0.192901
     0   0.333333
     1   0.192901
     2   0.098765
     3   0.041667
     4   0.012346
     5   0.001543
     6   0.000000
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    """
    fitname = inspect.stack()[0][3]
    values = []
    for i in index:
        if i <= loc:
            values.append(0)
        elif i >= loc + scale:
            values.append(0)
        elif i <= loc + scale / 2:
            values.append(
                0.5 * alpha / (scale / 2)
                * ((i - loc)/(scale / 2))**(alpha-1))  # PDF
#            values.append(0.5 * ((i - x[0])/(x[1] / 2))**x[2])  # CDF
        else:
            values.append(
                0.5 * alpha / (scale / 2)
                * ((loc + scale - i)/(scale / 2))**(alpha - 1))  # PDF
#            values.append(0.5 * ((x[0] + x[1] - i)/(x[1] / 2))**x[2])  # CDF
    pdf = pnd.DataFrame({fitname: values}, index=index)
    return pdf


def FSI(index, loc=0, scale=1):
    """
    Hydrogramme unitaire selon Le Moine - SINUSOIIDAL.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    loc : int
        Départ. Défaut : 0.
    scale : int
        Echelle. Défaut : 1.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> loc = -7.54
    >>> scale = 14.913
    >>> df = uh.FSI(index, loc, scale)
    >>> df
              FSI
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.001728
    -6   0.013627
    -5   0.034870
    -4   0.061743
    -3   0.089545
    -2   0.113413
    -1   0.129174
     0   0.134070
     1   0.127245
     2   0.109892
     3   0.085048
     4   0.057056
     5   0.030814
     6   0.010910
     7   0.000826
     8   0.000000
     9   0.000000
     10  0.000000

    >>> loc = -6
    >>> scale = 12
    >>> df = uh.FSI(index, loc, scale)
    >>> df
              FSI
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.000000
    -5   0.011165
    -4   0.041667
    -3   0.083333
    -2   0.125000
    -1   0.155502
     0   0.166667
     1   0.155502
     2   0.125000
     3   0.083333
     4   0.041667
     5   0.011165
     6   0.000000
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    """
    fitname = inspect.stack()[0][3]
    values = []
    for i in index:
        if i <= loc:
            values.append(0)
        elif i >= loc + scale:
            values.append(0)
        elif i <= loc + scale / 2:
            values.append(
                1 / (2 * scale / 2) * (
                    1 - math.cos(math.pi * (i - loc) / (scale / 2)))
            )  # PDF
#            values.append(
#                (i - x[0]) / (2* x[1] / 2)
#                - 1 / math.pi * math.sin(math.pi * (i - x[0]) / (x[1] / 2))
#            )  # CDF
        else:
            values.append(
                1 / (2 * scale / 2) * (
                    1 - math.cos(math.pi * (loc + scale - i) / (scale / 2)))
            )  # PDF
#            values.append(
#                (x[0] + x[1] - i) / (2* x[1] / 2)
#                - 1 / math.pi * math.sin(
#                    math.pi * (x[0] + x[1] - i) / (x[1] / 2))
#            )  # CDF
    pdf = pnd.DataFrame({fitname: values}, index=index)
    pdf[pdf < 0] = 0
    return pdf


def LBI(index, n=1, p=0, loc=0):
    """
    Hydrogramme unitaire par loi statistique BINOMIALE.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    n : int
        Paramètre de la loi Binomiale.
        Equivalent à 'scale' dans les fonctions. Défaut : 1.
    p : float
        Paramètre de la loi Binomiale. Défaut : 0.
    loc : int
        Départ. Défaut : 0.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    See Also
    --------
    scipy.stats.binom

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> n = 21
    >>> p = 0.50
    >>> loc = -11
    >>> df = uh.LBI(index, n, p, loc)
    >>> df
              LBI
    -11  0.168188
    -10  0.140157
    -9   0.097032
    -8   0.055447
    -7   0.025875
    -6   0.009703
    -5   0.002854
    -4   0.000000
    -3   0.000000
    -2   0.000000
    -1   0.000000
     0   0.000000
     1   0.000000
     2   0.000000
     3   0.000000
     4   0.000000
     5   0.000000
     6   0.000000
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    >>> n = 12
    >>> p = 0.50
    >>> loc = -6
    >>> df = uh.LBI(index, n, p, loc)
    >>> df
              LBI
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.225586
    -5   0.193359
    -4   0.120850
    -3   0.053711
    -2   0.016113
    -1   0.002930
     0   0.000000
     1   0.000000
     2   0.000000
     3   0.000000
     4   0.000000
     5   0.000000
     6   0.000000
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    """
    fitname = inspect.stack()[0][3]
    values = []
    for i in index:
        if i < loc:
            values.append(0)
        elif i > loc + n:
            values.append(0)
        else:
            values.append(binom.pmf(i - loc, n, p, loc))
    pdf = pnd.DataFrame({fitname: values}, index=index)
    pdf[pdf < 0.001] = 0
    return pdf


def LBN(index, n=1, p=1, loc=0):
    """
    Hydrogramme unitaire par loi statistique BINOMIALE NEGATIVE.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    n : int
        Paramètre de la loi Binomiale Négative.
        Equivalent à 'scale' dans les fonctions. Défaut : 1.
    p : float
        Paramètre de la loi Binomiale Négative. Défaut : 1.
    loc : int
        Départ. Défaut : 0.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    See Also
    --------
    scipy.stats.nbinom

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> n = 20.969
    >>> p = 0.50
    >>> loc = -11
    >>> df = uh.LBN(index, n, p, loc)
    >>> df
              LBN
    -11  0.019876
    -10  0.026476
    -9   0.033572
    -8   0.040729
    -7   0.047475
    -6   0.053363
    -5   0.058023
    -4   0.061197
    -3   0.062757
    -2   0.062709
    -1   0.061169
     0   0.058346
     1   0.054501
     2   0.049924
     3   0.044901
     4   0.039693
     5   0.034525
     6   0.029574
     7   0.024969
     8   0.020795
     9   0.017095
     10  0.000000

    >>> n = 12
    >>> p = 0.50
    >>> loc = -6
    >>> df = uh.LBN(index, n, p, loc)
    >>> df
              LBN
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.047211
    -5   0.060699
    -4   0.072081
    -3   0.080090
    -2   0.084094
    -1   0.084094
     0   0.080590
     1   0.074391
     2   0.066420
     3   0.057564
     4   0.048570
     5   0.039999
     6   0.032221
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    """
    fitname = inspect.stack()[0][3]
    values = []
    for i in index:
        if i < loc:
            values.append(0)
        elif i > loc + n:
            values.append(0)
        else:
            values.append(nbinom.pmf(i - loc, n, p, loc))
    pdf = pnd.DataFrame({fitname: values}, index=index)
    pdf[pdf < 0] = 0
    return pdf


def LGA(index, alpha=1, loc=0, scale=1):
    """
    Hydrogramme unitaire par loi statistique GAMMA.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    alpha : float
        Paramètre de la loi. Défaut : 1.
    loc : float
        Départ. Défaut : 0.
    scale : float
        Echelle. Défaut : 1.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    See Also
    --------
    scipy.stats.gamma

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> alpha = 6.470
    >>> loc = -3.178
    >>> scale = 1.128
    >>> df = uh.LGA(index, alpha, loc, scale)
    >>> df
              LGA
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.000000
    -5   0.000000
    -4   0.000000
    -3   0.064537
    -2   0.110755
    -1   0.141350
     0   0.148574
     1   0.136158
     2   0.112676
     3   0.086169
     4   0.061884
     5   0.042223
     6   0.027607
     7   0.017414
     8   0.010651
     9   0.006344
     10  0.003692

    >>> alpha = 4
    >>> loc = -6
    >>> scale = 12
    >>> df = uh.LGA(index, alpha, loc, scale)
    >>> df
              LGA
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.001053
    -5   0.001538
    -4   0.002113
    -3   0.002768
    -2   0.003493
    -1   0.004278
     0   0.005109
     1   0.005977
     2   0.006868
     3   0.007772
     4   0.008678
     5   0.009577
     6   0.010459
     7   0.011318
     8   0.012145
     9   0.012935
     10  0.013683

    """
    fitname = inspect.stack()[0][3]
    values = []
    for i in index:
        if i < loc:
            values.append(0)
        else:
            values.append(gamma.pdf(i - loc, alpha, loc, scale))
    pdf = pnd.DataFrame({fitname: values}, index=index)
    pdf[pdf < 0] = 0
    return pdf


def LGD(index, alpha=1, loc=0, scale=1):
    """
    Hydrogramme unitaire par loi statistique DOUBLE GAMMA.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    alpha : float
        Paramètre de la loi. Défaut : 1.
    loc : float
        Départ. Défaut : 0.
    scale : float
        Echelle. Défaut : 1.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    See Also
    --------
    scipy.stats.dgamma

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> alpha = 4
    >>> loc = -6
    >>> scale = 12
    >>> df = uh.LGD(index, alpha, loc, scale)
    >>> df
              LGD
    -11  0.000331
    -10  0.000184
    -9   0.000085
    -8   0.000027
    -7   0.000004
    -6   0.000000
    -5   0.000004
    -4   0.000027
    -3   0.000085
    -2   0.000184
    -1   0.000331
     0   0.000527
     1   0.000769
     2   0.001056
     3   0.001384
     4   0.001747
     5   0.002139
     6   0.002555
     7   0.002988
     8   0.003434
     9   0.003886
     10  0.004339

    """
    fitname = inspect.stack()[0][3]
    pdf = pnd.DataFrame({fitname: dgamma.pdf(index, alpha, loc, scale)},
                        index=index)
    return pdf


def LLD(index, alpha=1, loc=0):
    """
    Hydrogramme unitaire par loi statistique LAPLACE DISCRETE.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    alpha : float
        Paramètre de la loi. Défaut : 1.
    loc : int
        Départ. Défaut : 0.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    See Also
    --------
    scipy.stats.dlaplace

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> n = 20.969
    >>> p = 0.50
    >>> loc = -11
    >>> df = uh.LLD(index, n, p, loc)
    >>> df
              LLD
    -11  0.004441
    -10  0.006159
    -9   0.008542
    -8   0.011846
    -7   0.016428
    -6   0.022782
    -5   0.031594
    -4   0.043814
    -3   0.060762
    -2   0.084264
    -1   0.116858
     0   0.162059
     1   0.116858
     2   0.084264
     3   0.060762
     4   0.043814
     5   0.031594
     6   0.022782
     7   0.016428
     8   0.011846
     9   0.008542
     10  0.006159

    >>> n = 12
    >>> p = 0.50
    >>> loc = -6
    >>> df = uh.LLD(index, n, p, loc)
    >>> df
              LLD
    -11  0.033221
    -10  0.044843
    -9   0.060532
    -8   0.081710
    -7   0.110297
    -6   0.148885
    -5   0.110297
    -4   0.081710
    -3   0.060532
    -2   0.044843
    -1   0.033221
     0   0.024611
     1   0.018232
     2   0.013507
     3   0.010006
     4   0.007413
     5   0.005491
     6   0.004068
     7   0.003014
     8   0.002233
     9   0.001654
     10  0.001225

    """
    fitname = inspect.stack()[0][3]
    pdf = pnd.DataFrame({fitname: dlaplace.pmf(index, alpha, loc)},
                        index=index)
    pdf[pdf < 0.001] = 0
    return pdf


def LNG(index, loc=0, scale=1):
    """
    Hydrogramme unitaire par loi statistique NORMALE GAUSS.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    loc : float
        Départ. Défaut : 0.
    scale : float
        Echelle. Défaut : 1.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    See Also
    --------
    scipy.stats.norm

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> loc = -0.031
    >>> scale = 2.875
    >>> df = uh.LNG(index, loc, scale)
    >>> df
              LNG
    -11  0.000000
    -10  0.000000
    -9   0.001069
    -8   0.002978
    -7   0.007351
    -6   0.016079
    -5   0.031161
    -4   0.053509
    -3   0.081413
    -2   0.109754
    -1   0.131101
     0   0.138754
     1   0.130121
     2   0.108120
     3   0.079601
     4   0.051927
     5   0.030014
     6   0.015371
     7   0.006975
     8   0.002805
     9   0.000000
     10  0.000000

    >>> loc = -6
    >>> scale = 12
    >>> df = uh.LNG(index, loc, scale)
    >>> df
          LNG
    -11  0.030481
    -10  0.031449
    -9   0.032222
    -8   0.032787
    -7   0.033130
    -6   0.033245
    -5   0.033130
    -4   0.032787
    -3   0.032222
    -2   0.031449
    -1   0.030481
     0   0.029339
     1   0.028044
     2   0.026621
     3   0.025095
     4   0.023493
     5   0.021841
     6   0.020164
     7   0.018488
     8   0.016833
     9   0.015221
     10  0.013668

    """
    fitname = inspect.stack()[0][3]
    pdf = pnd.DataFrame({fitname: norm.pdf(index, loc, scale)}, index=index)
    pdf[pdf < 0.001] = 0
    return pdf


def LSK(index, mu1=1, mu2=1, loc=0):
    """
    Hydrogramme unitaire par loi statistique SKELLAM.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    mu1 : float
        Paramètre de forme mu1 de la loi statistique. Défaut : 1.
    mu2 : float
        Paramètre de forme mu2 de la loi statistique. Défaut : 1.
    loc : float
        Départ. Défaut : 0.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    See Also
    --------
    scipy.stats.skellam

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> mu1 = 3.597
    >>> mu2 = 4.670
    >>> loc = 1.000
    >>> df = uh.LSK(index, mu1, mu2, loc)
    >>> df
              LSK
    -11  0.000000
    -10  0.000000
    -9   0.001478
    -8   0.003597
    -7   0.008071
    -6   0.016597
    -5   0.031095
    -4   0.052734
    -3   0.080410
    -2   0.109491
    -1   0.132272
     0   0.140982
     1   0.132069
     2   0.108589
     3   0.078472
     4   0.050032
     5   0.028301
     6   0.014296
     7   0.006493
     8   0.002669
     9   0.000000
     10  0.000000

    >>> mu1 = 4
    >>> mu2 = 4
    >>> loc = -6
    >>> df = uh.LSK(index, mu1, mu2, loc)
    >>> df
              LSK
    -11  0.028694
    -10  0.050500
    -9   0.079194
    -8   0.109896
    -7   0.134142
    -6   0.143432
    -5   0.134142
    -4   0.109896
    -3   0.079194
    -2   0.050500
    -1   0.028694
     0   0.014633
     1   0.006745
     2   0.002829
     3   0.001087
     4   0.000000
     5   0.000000
     6   0.000000
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    """
    fitname = inspect.stack()[0][3]
    pdf = pnd.DataFrame({fitname: skellam.pmf(index, mu1, mu2, loc)},
                        index=index)
    pdf[pdf < 0.001] = 0
    return pdf


def LWE(index, c=1, loc=0, scale=1):
    """
    Hydrogramme unitaire par loi statistique WEIBULL.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    c : float
        Paramètre de la loi. Défaut : 1.
    loc : float
        Départ. Défaut : 0.
    scale : float
        Echelle. Défaut : 1.

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    See Also
    --------
    scipy.stats.weibull_min

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> c = 3.047
    >>> loc = -3.805
    >>> scale = 8.799
    >>> df = uh.LWE(index, c, loc, scale)
    >>> df
              LWE
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.000000
    -5   0.000000
    -4   0.000000
    -3   0.080203
    -2   0.106933
    -1   0.126906
     0   0.135311
     1   0.129911
     2   0.112119
     3   0.086629
     4   0.059587
     5   0.036245
     6   0.019353
     7   0.008999
     8   0.003614
     9   0.001243
     10  0.000363

    >>> c = 4
    >>> loc = -6
    >>> scale = 12
    >>> df = uh.LWE(index, c, loc, scale)
    >>> df
              LWE
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.000000
    -5   0.058931
    -4   0.081062
    -3   0.102482
    -2   0.119095
    -1   0.126728
     0   0.122626
     1   0.106902
     2   0.083011
     3   0.056666
     4   0.033505
     5   0.016882
     6   0.007121
     7   0.002467
     8   0.000688
     9   0.000151
     10  0.000025

    """
    fitname = inspect.stack()[0][3]
    values = []
    for i in index:
        if i <= loc:
            values.append(0)
        else:
            values.append(weibull_min.pdf(i - loc, c, loc, scale))
    pdf = pnd.DataFrame({fitname: values}, index=index)
    pdf[pdf < 0] = 0
    return pdf


def RE1(index, loc=0, scale=1):
    """
    Hydrogramme unitaire de forme géométrique RECTANGLE.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    loc : float
        Départ
    scale : float
        Echelle

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    See Also
    --------
    scipy.stats.uniform

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> loc = -12.564
    >>> scale = 17.125
    >>> df = uh.RE1(index, loc, scale)
    >>> df
              RE1
    -11  0.058394
    -10  0.058394
    -9   0.058394
    -8   0.058394
    -7   0.058394
    -6   0.058394
    -5   0.058394
    -4   0.058394
    -3   0.058394
    -2   0.058394
    -1   0.058394
     0   0.058394
     1   0.058394
     2   0.058394
     3   0.058394
     4   0.058394
     5   0.000000
     6   0.000000
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    >>> loc = -6
    >>> scale = 12
    >>> df = uh.RE1(index, loc, scale)
    >>> df
              RE1
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.083333
    -5   0.083333
    -4   0.083333
    -3   0.083333
    -2   0.083333
    -1   0.083333
     0   0.083333
     1   0.083333
     2   0.083333
     3   0.083333
     4   0.083333
     5   0.083333
     6   0.083333
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    """
    fitname = inspect.stack()[0][3]
    pdf = pnd.DataFrame({fitname: uniform.pdf(index, loc, scale)}, index=index)
    return pdf


def RE2(index, loc1=0, scale1=1, loc2=0, scale2=1):
    """
    Hydrogramme unitaire de forme géométrique DOUBLE RECTANGLE.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    loc1 : float
        Départ du rectangle 1
    scale1 : float
        Echelle du rectangle 1
    loc2 : float
        Départ du rectangle 2
    scale2 : float
        Echelle du rectangle 2

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    See Also
    --------
    scipy.stats.uniform

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> loc1 = -3.974
    >>> scale1 = 6.780
    >>> loc2 = -14.699
    >>> scale2 = 34.547
    >>> df = uh.RE2(index, loc1, scale1, loc2, scale2)
    >>> df
              RE2
    -11  0.028946
    -10  0.028946
    -9   0.028946
    -8   0.028946
    -7   0.028946
    -6   0.028946
    -5   0.028946
    -4   0.028946
    -3   0.147493
    -2   0.147493
    -1   0.147493
     0   0.147493
     1   0.147493
     2   0.147493
     3   0.028946
     4   0.028946
     5   0.028946
     6   0.028946
     7   0.028946
     8   0.028946
     9   0.028946
     10  0.028946

    >>> loc1 = -6
    >>> scale1 = 12
    >>> loc2 = -12
    >>> scale2 = 24
    >>> df = uh.RE2(index, loc1, scale1, loc2, scale2)
    >>> df
              RE2
    -11  0.041667
    -10  0.041667
    -9   0.041667
    -8   0.041667
    -7   0.041667
    -6   0.083333
    -5   0.083333
    -4   0.083333
    -3   0.083333
    -2   0.083333
    -1   0.083333
     0   0.083333
     1   0.083333
     2   0.083333
     3   0.083333
     4   0.083333
     5   0.083333
     6   0.083333
     7   0.041667
     8   0.041667
     9   0.041667
     10  0.041667

    """
    fitname = inspect.stack()[0][3]
    pdf = pnd.DataFrame(
        {fitname: [max(uniform.pdf(i, loc1, scale1),
                       uniform.pdf(i, loc2, scale2)) for i in index]},
        index=index)
    pdf[pdf < 0] = 0
    return pdf


def RT1(index, loc1=0, scale1=1, mode2=0, loc2=0, scale2=1):
    """
    Hydrogramme unitaire de forme géométrique RECTANGLE + TRIANGLE.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    loc1 : float
        Départ du rectangle 1
    scale1 : float
        Echelle du rectangle 1
    mode2 : float
        Départ du triangle 2 (paramètre c de scipy.stats.triang)
    loc2 : float
        Départ du triangle 2
    scale2 : float
        Echelle du triangle 2

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    See Also
    --------
    scipy.stats.uniform
    scipy.stats.triang

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> loc1 = -20.368
    >>> scale1 = 29.368
    >>> mode2 = 0.634
    >>> loc2 = -5.008
    >>> scale2 = 8.856
    >>> df = uh.RT1(index, loc1, scale1, mode2, loc2, scale2)
    >>> df
              RT1
    -11  0.034051
    -10  0.034051
    -9   0.034051
    -8   0.034051
    -7   0.034051
    -6   0.034051
    -5   0.034051
    -4   0.040544
    -3   0.080766
    -2   0.120988
    -1   0.161210
     0   0.201433
     1   0.198433
     2   0.128758
     3   0.059084
     4   0.034051
     5   0.034051
     6   0.034051
     7   0.034051
     8   0.034051
     9   0.034051
     10  0.000000

    >>> loc1 = -6
    >>> scale1 = 12
    >>> mode2 = 0.75
    >>> loc2 = -12
    >>> scale2 = 24
    >>> df = uh.RT1(index, loc1, scale1, mode2, loc2, scale2)
    >>> df
              RT1
    -11  0.004630
    -10  0.009259
    -9   0.013889
    -8   0.018519
    -7   0.023148
    -6   0.083333
    -5   0.083333
    -4   0.083333
    -3   0.083333
    -2   0.083333
    -1   0.083333
     0   0.083333
     1   0.083333
     2   0.083333
     3   0.083333
     4   0.083333
     5   0.083333
     6   0.083333
     7   0.069444
     8   0.055556
     9   0.041667
     10  0.027778

    """
    fitname = inspect.stack()[0][3]
    pdf = pnd.DataFrame(
        {fitname: [max(uniform.pdf(i, loc1, scale1),
                       triang.pdf(i, mode2, loc2, scale2)) for i in index]},
        index=index)
    pdf[pdf < 0] = 0
    return pdf


def TG1(index, mode=0, loc=0, scale=1):
    """
    Hydrogramme unitaire de forme géométrique TRIANGLE.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    mode : float
        Départ du triangle (paramètre c de scipy.stats.triang)
    loc : float
        Départ du triangle
    scale : float
        Echelle du triangle

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    See Also
    --------
    scipy.stats.triang

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> mode = 0.712
    >>> loc = -8.376
    >>> scale = 13.133
    >>> df = uh.TG1(index, mode, loc, scale)
    >>> df
              TG1
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.006124
    -7   0.022410
    -6   0.038696
    -5   0.054983
    -4   0.071269
    -3   0.087555
    -2   0.103841
    -1   0.120128
     0   0.136414
     1   0.151269
     2   0.111006
     3   0.070743
     4   0.030479
     5   0.000000
     6   0.000000
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    >>> mode = 0.75
    >>> loc = -6
    >>> scale = 12
    >>> df = uh.TG1(index, mode, loc, scale)
    >>> df
              TG1
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.000000
    -5   0.018519
    -4   0.037037
    -3   0.055556
    -2   0.074074
    -1   0.092593
     0   0.111111
     1   0.129630
     2   0.148148
     3   0.166667
     4   0.111111
     5   0.055556
     6   0.000000
     7   0.000000
     8   0.000000
     9   0.000000
     10  0.000000

    """
    fitname = inspect.stack()[0][3]
    pdf = pnd.DataFrame(
        {fitname: triang.pdf(index, mode, loc, scale)}, index=index)
    return pdf


def TG2(index, mode1=0, loc1=0, scale1=1, mode2=0, loc2=0, scale2=1):
    """
    Hydrogramme unitaire de forme géométrique DOUBLE TRIANGLE.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    mode1 : float
        Départ du triangle 1 (paramètre c de scipy.stats.triang)
    loc1 : float
        Départ du triangle 1
    scale1 : float
        Echelle du triangle 1
    mode2 : float
        Départ du triangle 2 (paramètre c de scipy.stats.triang)
    loc2 : float
        Départ du triangle 2
    scale2 : float
        Echelle du triangle 2

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    See Also
    --------
    scipy.stats.triang

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> mode1 = 1.000
    >>> loc1 = -5.313
    >>> scale1 = 7.313
    >>> mode2 = 0.580
    >>> loc2 = -12.575
    >>> scale2 = 22.393
    >>> df = uh.TG2(index, mode1, loc1, scale1, mode2, loc2, scale2)
    >>> df
              TG2
    -11  0.010831
    -10  0.017707
    -9   0.024584
    -8   0.031461
    -7   0.038337
    -6   0.045214
    -5   0.052091
    -4   0.058967
    -3   0.086500
    -2   0.123897
    -1   0.161294
     0   0.198691
     1   0.236088
     2   0.273486
     3   0.064746
     4   0.055250
     5   0.045753
     6   0.036257
     7   0.026761
     8   0.017264
     9   0.007768
     10  0.000000

    >>> mode1 = 0.50
    >>> loc1 = -6
    >>> scale1 = 12
    >>> mode2 = 0.75
    >>> loc2 = -12
    >>> scale2 = 24
    >>> df = uh.TG2(index, mode1, loc1, scale1, mode2, loc2, scale2)
    >>> df
              TG2
    -11  0.004630
    -10  0.009259
    -9   0.013889
    -8   0.018519
    -7   0.023148
    -6   0.027778
    -5   0.032407
    -4   0.055556
    -3   0.083333
    -2   0.111111
    -1   0.138889
     0   0.166667
     1   0.138889
     2   0.111111
     3   0.083333
     4   0.074074
     5   0.078704
     6   0.083333
     7   0.069444
     8   0.055556
     9   0.041667
     10  0.027778

    """
    fitname = inspect.stack()[0][3]
    pdf = pnd.DataFrame(
        {fitname: [max(triang.pdf(i, mode1, loc1, scale1),
                       triang.pdf(i, mode2, loc2, scale2)) for i in index]},
        index=index)
    return pdf


def TR1(index, mode1=0, loc1=0, scale1=1, loc2=0, scale2=1):
    """
    Hydrogramme unitaire de forme géométrique TRIANGLE + RECTANGLE.

    Parameters
    ----------
    index : list, pandas.Index
        Indices temporels sur lesquels il faut déterminer l'hydrogramme
    mode1 : float
        Départ du triangle 1 (paramètre c de scipy.stats.triang)
    loc1 : float
        Départ du triangle 1
    scale1 : float
        Echelle du triangle 1
    loc2 : float
        Départ du rectangle 2
    scale2 : float
        Echelle du rectangle 2

    Returns
    -------
    df : pandas.DataFrame
        Hydrogramme unitaire

    Notes
    -----

    See Also
    --------
    scipy.stats.triang
    scipy.stats.uniform

    Examples
    --------
    >>> from pyspc.model.mohys import unit_hydrograph as uh
    >>> index = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
    ...     5, 6, 7, 8, 9, 10]
    >>> mode1 = 0.656
    >>> loc1 = -6.727
    >>> scale1 = 11.363
    >>> loc2 = -3.035
    >>> scale2 = 48.770
    >>> df = uh.TR1(index, mode1, loc1, scale1, loc2, scale2)
    >>> df
              TR1
    -11  0.000000
    -10  0.000000
    -9   0.000000
    -8   0.000000
    -7   0.000000
    -6   0.017166
    -5   0.040779
    -4   0.064391
    -3   0.088003
    -2   0.111616
    -1   0.135228
     0   0.158841
     1   0.163723
     2   0.118695
     3   0.073666
     4   0.028638
     5   0.020504
     6   0.020504
     7   0.020504
     8   0.020504
     9   0.020504
     10  0.020504

    >>> mode1 = 0.50
    >>> loc1 = -6
    >>> scale1 = 12
    >>> loc2 = -12
    >>> scale2 = 24
    >>> df = uh.TR1(index, mode1, loc1, scale1, loc2, scale2)
    >>> df
              TR1
    -11  0.041667
    -10  0.041667
    -9   0.041667
    -8   0.041667
    -7   0.041667
    -6   0.041667
    -5   0.041667
    -4   0.055556
    -3   0.083333
    -2   0.111111
    -1   0.138889
     0   0.166667
     1   0.138889
     2   0.111111
     3   0.083333
     4   0.055556
     5   0.041667
     6   0.041667
     7   0.041667
     8   0.041667
     9   0.041667
     10  0.041667

    """
    fitname = inspect.stack()[0][3]
    pdf = pnd.DataFrame(
        {fitname: [max(triang.pdf(i, mode1, loc1, scale1),
                       uniform.pdf(i, loc2, scale2)) for i in index]},
        index=index)
    return pdf


UH_DICT = {
    'FBN': FBN,
    'FEL': FEL,
    'FGT': FGT,
    'FPA': FPA,
    'FPU': FPU,
    'FSI': FSI,
    'LBI': LBI,
    'LBN': LBN,
    'LGA': LGA,
    'LGD': LGD,
    'LLD': LLD,
    'LNG': LNG,
    'LSK': LSK,
    'LWE': LWE,
    'RE1': RE1,
    'RE2': RE2,
    'RT1': RT1,
    'TG1': TG1,
    'TG2': TG2,
    'TR1': TR1,
}
"""Dictionnaire des fonctions de construction des hydrogrammes unitaires"""
