#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2018 - Temps-réel prévision
"""
import os.path
import pandas as pnd

import pyspc.core.exception as _exception
from pyspc.convention.grp18 import (
    RT_DATA_OBSFILEPREFIX, RT_DATA_LINEPREFIX)


class GRPRT_Data():
    """
    Structure de données GRPRT Data (GRP *Temps Réel*)

    Fichiers

    - Debit.txt
    - Pluie.txt
    - Temp.txt
    - Hauteur.txt

    Attributes
    ----------
    filename : str
        Nom du fichier de données
    varname : str
        Nom de la variable
    timestep : str, None
        Pas de temps
    lineprefix : str
        Préfixe des lignes de données du fichier

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRPRT_Data

        Parameters
        ----------
        filename : str
            Nom du fichier de données

        """
        self.varname, self.timestep = self.split_basename(filename=filename)
        self.lineprefix = self.get_lineprefix(varname=self.varname)
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRPRT_Data
        """
        text = """
        *************************************
        *********** GRP 2018 - RT Data ******
        *************************************
        *  NOM FICHIER      = {filename}
        *  NOM VARIABLE     = {varname}
        *  PAS DE TEMPS     = {timestep}
        *  PREFIXE DONNEE   = {lineprefix}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lecture d'un fichier d'observations pour GRP RT

        Returns
        -------
        pandas.DataFrame
            Tableau des prévisions de GRP Temps-Réel

        Examples
        --------
        >>> from pyspc.model.grp18 import GRPRT_Data

        Cas de données instantanées (Débit)

        >>> f = 'data/model/grp18/rt/Debit.txt'
        >>> reader = GRPRT_Data(filename=f)
        >>> reader
        *************************************
        *********** GRP 2018 - RT Data ******
        *************************************
        *  NOM FICHIER      = data/model/grp18/rt/Debit.txt
        *  NOM VARIABLE     = Q
        *  PAS DE TEMPS     = None
        *  PREFIXE DONNEE   = CQT
        *************************************

        >>> df = reader.read()
        >>> df
           PREFIX      CODE      DATE   HOUR  VALUE  NaN
        0     CQT  RH10585x  20210530  12:00   17.1  NaN
        1     CQT  RH10585x  20210530  13:00   17.1  NaN
        2     CQT  RH10585x  20210530  14:00   17.1  NaN
        3     CQT  RH10585x  20210530  15:00   17.1  NaN
        4     CQT  RH10585x  20210530  16:00   17.1  NaN
        5     CQT  RH10585x  20210530  17:00   17.1  NaN
        6     CQT  RH10585x  20210530  18:00   17.1  NaN
        7     CQT  RH10585x  20210530  19:00   17.1  NaN
        8     CQT  RH10585x  20210530  20:00   17.1  NaN
        9     CQT  RH10585x  20210530  21:00   16.8  NaN
        10    CQT  RH10585x  20210530  22:00   16.8  NaN
        11    CQT  RH10585x  20210530  23:00   16.8  NaN
        12    CQT  RH10585x  20210531  00:00   17.2  NaN
        13    CQT  RH10585x  20210531  01:00   18.2  NaN
        14    CQT  RH10585x  20210531  02:00   19.1  NaN
        15    CQT  RH10585x  20210531  03:00   19.3  NaN
        16    CQT  RH10585x  20210531  04:00   19.3  NaN
        17    CQT  RH10585x  20210531  05:00   18.7  NaN

        Cas de données avec pas de temps (pluviométrie horaire)

        >>> f = 'data/model/grp18/rt/Pluie_00J01H00M.txt'
        >>> reader = GRPRT_Data(filename=f)
        >>> reader
        *************************************
        *********** GRP 2018 - RT Data ******
        *************************************
        *  NOM FICHIER      = data/model/grp18/rt/Pluie_00J01H00M.txt
        *  NOM VARIABLE     = P
        *  PAS DE TEMPS     = 00J01H00M
        *  PREFIXE DONNEE   = PLU
        *************************************

        >>> df = reader.read()
        >>> df
           PREFIX      CODE      DATE   HOUR  VALUE  NaN
        0     PLU  90052002  20210530  12:00    0.0  NaN
        1     PLU  90052002  20210530  13:00    0.0  NaN
        2     PLU  90052002  20210530  14:00    0.0  NaN
        3     PLU  90052002  20210530  15:00    0.0  NaN
        4     PLU  90052002  20210530  16:00    0.0  NaN
        5     PLU  90052002  20210530  17:00    0.0  NaN
        6     PLU  90052002  20210530  18:00    0.0  NaN
        7     PLU  90052002  20210530  19:00    0.0  NaN
        8     PLU  90052002  20210530  20:00    0.0  NaN
        9     PLU  90052002  20210530  21:00    0.0  NaN
        10    PLU  90052002  20210530  22:00    0.0  NaN
        11    PLU  90052002  20210530  23:00    0.0  NaN
        12    PLU  90052002  20210531  00:00    0.0  NaN
        13    PLU  90052002  20210531  01:00    0.0  NaN
        14    PLU  90052002  20210531  02:00    0.0  NaN
        15    PLU  90052002  20210531  03:00    0.0  NaN
        16    PLU  90052002  20210531  04:00    0.0  NaN
        17    PLU  90052002  20210531  05:00    0.0  NaN
        18    PLU  90065003  20210530  12:00    0.0  NaN
        19    PLU  90065003  20210530  13:00    0.0  NaN
        20    PLU  90065003  20210530  14:00    0.0  NaN
        21    PLU  90065003  20210530  15:00    0.0  NaN
        22    PLU  90065003  20210530  16:00    0.0  NaN
        23    PLU  90065003  20210530  17:00    0.0  NaN
        24    PLU  90065003  20210530  18:00    0.0  NaN
        25    PLU  90065003  20210530  19:00    0.0  NaN
        26    PLU  90065003  20210530  20:00    0.0  NaN
        27    PLU  90065003  20210530  21:00    0.0  NaN
        28    PLU  90065003  20210530  22:00    0.0  NaN
        29    PLU  90065003  20210530  23:00    0.0  NaN
        30    PLU  90065003  20210531  00:00    0.0  NaN
        31    PLU  90065003  20210531  01:00    0.0  NaN
        32    PLU  90065003  20210531  02:00    0.0  NaN
        33    PLU  90065003  20210531  03:00    0.0  NaN
        34    PLU  90065003  20210531  04:00    0.0  NaN
        35    PLU  90065003  20210531  05:00    0.0  NaN

        """
        return pnd.read_csv(
            self.filename,
            sep=';',
            header=None,
            index_col=False,
            skiprows=1,
            engine='python',
            skipfooter=1,
            names=['PREFIX', 'CODE', 'DATE', 'HOUR', 'VALUE', None],
        )

    def write(self, data=None):
        """
        Ecrire le fichier de données GRPRT Data

        Parameters
        ----------
        pandas.DataFrame
            Tableau des prévisions de GRP Temps-Réel

        """
        _exception.check_dataframe(data)
        with open(self.filename, 'w', encoding='utf-8', newline='\r\n') as f:
            f.write('Ligne_Entete\n')
        data.to_csv(
            self.filename,
            mode='a',
            sep=';',
            float_format='%.3f',
            header=False,
            index=False,
            line_terminator='\r\n'
        )
        with open(self.filename, 'a', encoding='utf-8', newline='\r\n') as f:
            f.write('FIN;OBS\n')

    @staticmethod
    def get_lineprefix(varname=None):
        """
        Préfixe des lignes de données

        Parameters
        ----------
        varname : str
            Nom de la variable

        Returns
        -------
        p : str
            Préfixe des lignes de données

        Examples
        --------
        >>> from pyspc.model.grp18 import GRPRT_Data
        >>> v = 'H'
        >>> p = GRPRT_Data.get_lineprefix(varname=v)
        >>> p
        HAU

        >>> v = 'P'
        >>> p = GRPRT_Data.get_lineprefix(varname=v)
        >>> p
        PLU

        >>> v = 'Q'
        >>> p = GRPRT_Data.get_lineprefix(varname=v)
        >>> p
        CQT

        >>> v = 'T'
        >>> p = GRPRT_Data.get_lineprefix(varname=v)
        >>> p
        TEM

        """
        try:
            p = RT_DATA_LINEPREFIX[varname.split('_')[0]]
        except KeyError as ke:
            raise ValueError('Type de donnée incorrect pour la '
                             'définition du préfixe') from ke
        return p

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les méta-données du nom du fichier GRPRT Data

        Parameters
        ----------
        filename : str
            Nom du fichier GRPRT Data

        Returns
        -------
        varname : str
            Grandeur
        timestep : str, None
            Pas de temps

        """
        # ---------------------------------------------------------------------
        # 0- Initialisation
        # ---------------------------------------------------------------------
        basename = os.path.splitext(os.path.basename(filename))[0]
        # ---------------------------------------------------------------------
        # 1- Détermination de la grandeur et du pas de temps
        # ---------------------------------------------------------------------
        infos = basename.split('_')
        prefix = infos.pop(0)
        try:
            timestep = infos.pop(0)
        except IndexError:
            timestep = None
        for v, p in RT_DATA_OBSFILEPREFIX.items():
            if prefix.startswith(p):
                return v, timestep
        # ---------------------------------------------------------------------
        # 3- Cas inconnu
        # ---------------------------------------------------------------------
        raise ValueError("Le nommage du fichier '{}' est incorrect"
                         "".format(filename))
