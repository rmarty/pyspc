#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2018 - Temps-réel archive
"""
from datetime import datetime as dt
import os.path
import pandas as pnd

from pyspc.convention.grp18 import RT_ARCHIVE_DATEFORMAT, RT_ARCHIVE_VARNAMES


class GRPRT_Archive():
    """
    Structure de données GRPRT Archive (GRP *Temps Réel*)

    - PV_10A_jjJhhHmmM.DAT
    - QV_10A.DAT

    Attributes
    ----------
    filename : str
        Nom du fichier Archve de GRP *Temps-Réel*
    varname : str
        Variable
    timestep : str, None
        Pas de temps de la variable

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRPRT_Archive

        Parameters
        ----------
        filename : str
            Nom du fichier Archve de GRP *Temps-Réel*

        """
        self.filename = filename
        infos = self.split_basename(filename=self.filename)
        self.varname = infos[0]
        self.timestep = infos[1]
        self._check_varname(varname=self.varname)

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRPRT_Archive
        """
        text = """
        *************************************
        *********** GRP 2018 - RT Archive ***
        *************************************
        *  NOM FICHIER      = {filename}
        *  NOM VARIABLE     = {varname}
        *  PAS DE TEMPS     = {timestep}
        *************************************
        """
        return text.format(**vars(self))

    def _check_varname(self, varname=None):
        """
        Contrôler la variable

        Parameters
        ----------
        varname : str
            Nom de la variable.

        Raises
        ------
        ValueError
            Si la variable n'est pas reconnue par pyspc

        """
        if varname not in self.get_varnames():
            raise ValueError("Variable mal renseignée")

    def read(self):
        """
        Lecture du fichier GRPRT_Archive

        Returns
        -------
        pandas.DataFrame
            Tableau des données d'archives de GRP Temps-Réel

        Examples
        --------
        >>> from pyspc.model.grp18 import GRPRT_Archive
        >>> f = 'data/model/grp18/rt/PV_10A_00J01H00M.DAT'
        >>> reader = GRP_Data(filename=f)
        >>> reader
        *************************************
        *********** GRP 2018 - RT Archive ***
        *************************************
        *  NOM FICHIER      = data/model/grp18/rt/PV_10A_00J01H00M.DAT
        *  NOM VARIABLE     = PV
        *  PAS DE TEMPS     = 00J01H00M
        *************************************
        >>> df = reader.read()
        >>> df
                             90065003
        Date (TU)
        2007-01-18 12:00:00       0.8
        2007-01-18 13:00:00       0.6
        2007-01-18 14:00:00       1.0
        2007-01-18 15:00:00       4.8
        2007-01-18 16:00:00       7.2
        2007-01-18 17:00:00       8.0
        2007-01-18 18:00:00      11.2
        2007-01-18 19:00:00       7.4
        2007-01-18 20:00:00       7.6
        2007-01-18 21:00:00       6.8
        2007-01-18 22:00:00       8.8
        2007-01-18 23:00:00       7.6
        2007-01-19 00:00:00       5.8
        2007-01-19 01:00:00       7.8
        2007-01-19 02:00:00       8.2
        2007-01-19 03:00:00       5.2
        2007-01-19 04:00:00       6.6
        2007-01-19 05:00:00       7.2
        2007-01-19 06:00:00       9.0
        2007-01-19 07:00:00       6.0
        2007-01-19 08:00:00       5.0
        2007-01-19 09:00:00       3.6
        2007-01-19 10:00:00       2.8
        2007-01-19 11:00:00       2.0
        2007-01-19 12:00:00       1.8

        >>> f = 'data/model/grp18/rt/QV_10A.DAT'
        >>> reader = GRP_Data(filename=f)
        >>> reader
        *************************************
        *********** GRP 2018 - RT Archive ***
        *************************************
        *  NOM FICHIER      = data/model/grp18/rt/QV_10A.DAT
        *  NOM VARIABLE     = QV
        *  PAS DE TEMPS     = None
        *************************************
        >>> df = reader.read()
        >>> df
                              RH10585x
        Date (TU)
        2007-01-18 19:00:00   6.560000
        2007-01-18 19:30:00   7.920000
        2007-01-18 22:00:00  13.600000
        2007-01-18 22:03:00  13.700000
        2007-01-18 23:00:00  15.800000
        2007-01-18 23:30:00  16.700000
        2007-01-19 00:00:00  17.300000
        2007-01-19 01:00:00  19.200000
        2007-01-19 01:30:00  19.800000
        2007-01-19 02:45:00  20.400000
        2007-01-19 03:24:00  21.500000
        2007-01-19 04:00:00  23.000000
        2007-01-19 04:30:00  23.600000
        2007-01-19 05:30:00  23.400000
        2007-01-19 06:00:00  23.600000
        2007-01-19 06:30:00  24.700000
        2007-01-19 07:30:00  28.600000
        2007-01-19 07:45:00  30.600000
        2007-01-19 08:30:00  35.100000
        2007-01-19 10:00:00  36.600000
        2007-01-19 11:30:00  35.900000
        2007-01-19 12:15:00  32.099998
        2007-01-19 12:49:00  28.600000
        2007-01-19 13:30:00  26.700000
        2007-01-19 14:15:00  25.300000
        2007-01-19 15:30:00  23.500000
        2007-01-19 16:30:00  22.800000
        2007-01-19 18:00:00  20.800000
        2007-01-19 18:30:00  20.400000
        2007-01-19 19:30:00  19.200000
        2007-01-19 20:00:00  19.300000
        2007-01-19 20:30:00  19.100000
        2007-01-19 21:00:00  18.600000
        2007-01-19 22:00:00  18.800000
        2007-01-19 23:00:00  17.900000

        """
        with open(self.filename, 'r', encoding='iso-8859-1') as f:
            f.readline()
            infos = f.readline().strip()
            station = infos.split(' ')[-1]

        # Lecture des données
        df = pnd.read_csv(
            self.filename,
            encoding='iso-8859-1',
            sep=';',
            header=0,
            skiprows=4,
            index_col=0,
            parse_dates=True,
            date_parser=lambda x: dt.strptime(x, RT_ARCHIVE_DATEFORMAT),
            converters={'Débit (l/s) ': float, 'Pluie (mm)  ': float},
            decimal=','
        )
        # Nettoyer les intitulés des colonnes
#        df.columns = [c.strip() for c in df.columns]
        df.columns = [station]
        df.index.name = df.index.name.strip()
        # Unités
        if self.varname.startswith('Q'):
            df = df.applymap(lambda x: x / 1000.)
        return df

    def write(self):
        """
        Ecriture du fichier GRPRT_Archive
        """
        raise NotImplementedError

    @classmethod
    def get_varnames(cls):
        """
        Définir le nom de la variable
        """
        return sorted(RT_ARCHIVE_VARNAMES)

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        de données GRP Archive (GRP *Temps-Réel*)

        Parameters
        ----------
        filename : str
            Fichier de données GRP Archive (GRP *Temps-Réel*)

        Returns
        -------
        varname : str
            Nom de la variable
        timestep : str, None
            Pas de temps de la variable

        Examples
        --------
        >>> from pyspc.model.grp18 import GRPRT_Archive

        Cas d'une grandeur sans pas de temps

        >>> f = 'data/model/grp18/rt/QV_10A.DAT'
        >>> varname = GRPRT_Archive.split_basename(filename=f)
        >>> varname
        QV
        >>> timestep
        None

        Cas d'une grandeur avec un pas de temps

        >>> f = 'data/model/grp18/rt/PV_10A_00J01H00M.DAT'
        >>> varname = GRPRT_Archive.split_basename(filename=f)
        >>> varname
        PV
        >>> timestep
        00J01H00

        """
        if filename is None:
            return None, None
        infos = os.path.splitext(os.path.basename(filename))[0].split('_')
        varname = infos.pop(0)
        infos.pop(0)  # 10A
        try:
            timestep = infos.pop(0)
        except IndexError:
            timestep = None
        return varname, timestep
