#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2018 - Temps-réel interne
"""
import os.path
import pandas as pnd

from pyspc.convention.grp18 import RT_INTERN_DTYPES


class GRPRT_Intern():
    """
    Structure de données GRPRT Intern (Variables internes de GRP *Temps Réel*)

    Fichiers

    - PQE_1A.DAT
    - PQE_1A_D.DAT

    Attributes
    ----------
    filename : str
        Nom du fichier Archve de GRP *Temps-Réel*
    realtime : bool
        Temps-réel (True) ou Temps différé (False)

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRPRT_Intern

        Parameters
        ----------
        filename : str
            Nom du fichier Interne de GRP *Temps-Réel*

        """
        self.filename = filename
        self.realtime = self.split_basename(filename=self.filename)

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRPRT_Intern
        """
        text = """
        *************************************
        *********** GRP 2018 - RT Intern ****
        *************************************
        *  NOM FICHIER      = {filename}
        *  TEMPS-REEL       = {realtime}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lire le fichier de variables internes Temps-Réel / Temps-Différé

        Returns
        -------
        pandas.DataFrame
            Tableau des variables internes de GRP Temps-Réel

        Examples
        --------
        >>> from pyspc.model.grp18 import GRPRT_Intern

        Cas avec des données au pas de temps horaire (mm: mm par heure)

        >>> f = 'data/model/grp18/rt/intern/PQE_1A_D.DAT'
        >>> reader = GRPRT_Intern(filename=f)
        >>> df = reader.read()
        >>> df
                             Qsim(mm)  Qobs(mm)  ETP(mm)  P(mm)  ...  NHU004
        DATE (TU)                                                ...
        2007-01-18 12:00:00    0.3650    0.3278   0.0338   0.16  ...     0.0
        2007-01-18 13:00:00    0.3604    0.3221   0.0338   0.12  ...     0.0
        2007-01-18 14:00:00    0.3555    0.3164   0.0319   0.20  ...     0.0
        2007-01-18 15:00:00    0.3766    0.3147   0.0281   3.04  ...     0.0
        2007-01-18 16:00:00    0.4365    0.3170   0.0224   3.36  ...     0.0
        2007-01-18 17:00:00    0.5306    0.3363   0.0148   4.16  ...     0.0
        2007-01-18 18:00:00    0.6982    0.4108   0.0054   8.48  ...     0.0
        2007-01-18 19:00:00    0.9022    0.5981   0.0000   3.72  ...     0.0
        2007-01-18 20:00:00    1.1287    0.9132   0.0000   7.60  ...     0.0
        2007-01-18 21:00:00    1.3858    1.1836   0.0000   6.32  ...     0.0
        2007-01-18 22:00:00    1.7311    1.4474   0.0000   9.44  ...     0.0
        2007-01-18 23:00:00    2.1057    1.7065   0.0000   6.64  ...     0.0
        2007-01-19 00:00:00    2.4809    1.9306   0.0000   7.88  ...     0.0
        2007-01-19 01:00:00    2.8736    2.1194   0.0000   8.44  ...     0.0
        2007-01-19 02:00:00    3.2212    2.2889   0.0000   5.32  ...     0.0
        2007-01-19 03:00:00    3.4918    2.3727   0.0000   6.64  ...     0.0
        2007-01-19 04:00:00    3.7265    2.5446   0.0000   5.96  ...     0.0
        2007-01-19 05:00:00    3.9712    2.7203   0.0000   6.72  ...     0.0
        2007-01-19 06:00:00    4.2390    2.7348   0.0000   7.24  ...     0.0
        2007-01-19 07:00:00    4.5375    2.8931   0.0057   7.44  ...     0.0
        2007-01-19 08:00:00    4.8103    3.4033   0.0158   6.60  ...     0.0
        2007-01-19 09:00:00    5.0269    3.9310   0.0239   6.48  ...     0.0
        2007-01-19 10:00:00    5.1634    4.1923   0.0300   5.36  ...     0.0
        2007-01-19 11:00:00    5.1467    4.2232   0.0340   2.96  ...     0.0
        2007-01-19 12:00:00    4.9376    4.0355   0.0360   1.32  ...     0.0


        Cas avec des données au pas de temps journalier (mm: mm par jour)

        >>> f = 'data/model/grp18/rt/intern/PQE_1A_D_journalier.DAT'
        >>> reader = GRPRT_Intern(filename=f)
        >>> df = reader.read()
        >>> df
                    Qsim(mm)  Qobs(mm)  ETP(mm)  P(mm)  ...  NHU003
        DATE (TU)                                       ...
        2007-01-01    2.4979    1.3206   0.2546   9.92  ...     0.0
        2007-01-02   11.8038   11.9688   0.1818  49.24  ...     0.0
        2007-01-03   17.5688   11.1614   0.2000  25.36  ...     0.0
        2007-01-04   13.2596       NaN   0.2091   3.40  ...     0.0
        2007-01-05   12.7508    9.6163   0.2091  15.04  ...     0.0
        2007-01-06   11.3613   10.6385   0.2455   2.20  ...     0.0
        2007-01-07   13.5780   11.4158   0.2818  20.40  ...     0.0
        2007-01-08   13.8564   18.8845   0.2818   7.28  ...     0.0
        2007-01-09   16.8916   15.5559   0.3091  26.36  ...     0.0
        2007-01-10   21.9469   26.3846   0.2818  25.12  ...     0.0
        2007-01-11   18.8175   17.3272   0.2727   8.44  ...     0.0
        2007-01-12   18.5854   13.7166   0.2546  22.04  ...     0.0
        2007-01-13   15.6874       NaN   0.3091   4.76  ...     0.0
        2007-01-14   10.4903       NaN   0.2455   1.60  ...     0.0
        2007-01-15    8.8068    9.9430   0.2546   6.88  ...     0.0
        2007-01-16    7.0682    8.1452   0.3000   0.00  ...     0.0
        2007-01-17    5.2633       NaN   0.2546   0.40  ...     0.0
        2007-01-18    5.6030    6.0837   0.2727   7.84  ...     0.0
        2007-01-19   25.2348   13.6173   0.2909  70.08  ...     0.0
        2007-01-20   63.2449   68.1413   0.2818  82.96  ...     0.0
        2007-01-21   47.8863   33.2340   0.2091  10.48  ...     0.0
        2007-01-22   24.7868   22.5610   0.2000   9.00  ...     0.0
        2007-01-23   14.7369   14.3262   0.2000   2.04  ...     0.0
        2007-01-24    9.2009   10.6448   0.1818   1.12  ...     0.0

        """
        df = pnd.read_csv(
            self.filename,
            sep=';',
            header=0,
            skiprows=[0, 1, 3, 4],
            index_col=False,
            dtype={'DATE (TU) ': str},
            na_values=[-99.9, '-99.9000', -9.9900, '-9.9900'],
        )
        df.set_index('DATE (TU) ', inplace=True)
        # Nettoyer la colonne superflue
        df.drop(columns=df.columns[-1], inplace=True)
        # Nettoyer les intitulés des colonnes
        df.columns = [c.strip() for c in df.columns]
        df.index.name = df.index.name.strip()
        lendt = list({len(i) for i in df.index})[0]
        if lendt == 12:
            fmt = '%Y%m%d%H%M'
        elif lendt == 10:
            fmt = '%Y%m%d%H'
        elif lendt == 8:
            fmt = '%Y%m%d'
        else:
            raise NotImplementedError
        df.index = pnd.to_datetime(df.index, format=fmt)
        return df

    def write(self, data=None):
        """
        Ecrire le fichier Interne de GRP Temps-Réel / Temps-Différé
        """
        raise NotImplementedError

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        de données GRP Intern (GRP *Temps-Réel*)

        Parameters
        ----------
        filename : str
            Fichier de données GRP Archive (GRP *Temps-Réel*)

        Returns
        -------
        realtime : bool
            Temps-réel (True) ou Temps différé (False)

        Examples
        --------
        >>> from pyspc.model.grp18 import GRPRT_Intern
        >>> f = 'data/model/grp18/rt/intern/PQE_1A.DAT'
        >>> realtime = GRPRT_Intern.split_basename(filename=f)
        >>> realtime
        True
        >>> f = 'data/model/grp18/rt/intern/PQE_1A_D.DAT'
        >>> realtime = GRPRT_Intern.split_basename(filename=f)
        >>> realtime
        False

        """
        if filename is None:
            return None
        x = os.path.splitext(os.path.basename(filename))[0].split('_')
        return len(x) != 3 or x[-1] != 'D'

    def check_datatype(self, datatype):
        """
        Contrôler la cohérence entre le type et le nom du fichier

        Parameters
        ----------
        datatype : str
            Type de fichier

        Raises
        ------
        ValueError
            Si incohérence entre le type et le nom du fichier

        """
        test_file = self.split_basename(filename=self.filename)
        test_dtype = RT_INTERN_DTYPES[datatype]
        if (test_file and not test_dtype) or (not test_file and test_dtype):
            raise ValueError("Incohérence entre le type et le nom du fichier")

    @staticmethod
    def get_types():
        """
        Type de fichier de variable interne GRP Temps-réel

        - intern      : variable interne temps-réel
        - intern_diff : variable interne temps différé
        """
        return sorted(RT_INTERN_DTYPES.keys())
