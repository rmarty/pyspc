#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2018 - Prévisions
"""
from datetime import datetime as dt
import os.path
import pandas as pnd

from pyspc.convention.grp18 import CAL_FCST_DATEFORMAT


class GRP_Fcst():
    """
    Structure de fichier de prévision produit par GRP (*Calage*)

    Attributes
    ----------
    filename : str
        Nom du fichier de données
    station : str
        Identifiant de la station
    model : str
        Nom du modèle (GRP)
    snow : str
        Avec/Sans module neige
    error : str
        Modèle d'erreur (RNA, TAN)
    ltime : str
        Echeance de prévision
    timestep : str
        Pas de temps de calcul
    rainfall : str
        Origine des pluies (P0: plue nulle, PP: pluie parfaite)
    periods : str
        Périodes de calage et d'application

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRP_Fcst

        Parameters
        ----------
        filename : str
            Nom du fichier de données

        """
        self.filename = filename
        if self.filename is not None:
            (self.station, self.model, self.snow, self.error, self.ltime,
             self.timestep, self.rainfall, self.periods) = self.split_basename(
                 self.filename)
        else:
            self.station = None
            self.model = None
            self.snow = None
            self.error = None
            self.ltime = None
            self.timestep = None
            self.rainfall = None
            self.periods = None

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRP_Fcst
        """
        text = """
        *************************************
        *********** GRP 2018 - Fcst *********
        *************************************
        *  NOM FICHIER      = {filename}
        *  CODE STATION     = {station}
        *  MODEL            = {model}
        *  NEIGE            = {snow}
        *  TRAIT. ERREUR    = {error}
        *  HORIZON CALAGE   = {ltime}
        *  PAS DE TEMPS     = {timestep}
        *  SOURCE PRECIP    = {rainfall}
        *  PERIODES         = {periods}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lecture du fichier de prévision produit par GRP (*Calage*)

        Returns
        -------
        pandas.DataFrame
            Tableau des données de prévision de GRP 2018

        Examples
        --------
        >>> from pyspc.model.grp18 import GRP_Fcst
        >>> f = 'data/model/grp18/cal/H_RH10585x_GRP_SMN_TAN_HOR_00J03H00M_PDT_00J01H00M_PP_P0P0.TXT'
        >>> reader = GRP_Fcst(filename=filename)
        >>> df = reader.read()
        >>> df
                             OBS00J00H00M  ...  PRV05J00H00M
              DATE                         ...
        2007-01-18 12:00:00         2.823  ...        2.7289
        2007-01-18 13:00:00         2.774  ...        2.7006
        2007-01-18 14:00:00         2.725  ...        2.6554
        2007-01-18 15:00:00         2.710  ...        2.5350
        2007-01-18 16:00:00         2.730  ...        2.3908
        2007-01-18 17:00:00         2.896  ...        2.3276
        2007-01-18 18:00:00         3.537  ...        2.3106
        2007-01-18 19:00:00         5.150  ...        2.4764
        2007-01-18 20:00:00         7.864  ...        2.5738
        2007-01-18 21:00:00        10.192  ...        2.4174
        2007-01-18 22:00:00        12.464  ...        2.3057
        2007-01-18 23:00:00        14.695  ...        2.2546
        2007-01-19 00:00:00        16.625  ...        2.2049
        2007-01-19 01:00:00        18.250  ...        2.1637
        2007-01-19 02:00:00        19.710  ...        2.1810
        2007-01-19 03:00:00        20.432  ...        2.1790
        2007-01-19 04:00:00        21.912  ...        2.2232
        2007-01-19 05:00:00        23.425  ...        2.2574
        2007-01-19 06:00:00        23.550  ...        2.2739
        2007-01-19 07:00:00        24.913  ...        2.3909
        2007-01-19 08:00:00        29.306  ...        2.5667
        2007-01-19 09:00:00        33.850  ...        2.5857
        2007-01-19 10:00:00        36.100  ...        2.5005
        2007-01-19 11:00:00        36.367  ...        2.4350
        2007-01-19 12:00:00        34.750  ...        2.3820

        [25 rows x 19 columns]

        """
        df = pnd.read_csv(
            self.filename,
            sep=';',
            header=0,
            skiprows=5,
            index_col=0,
            parse_dates=True,
            date_parser=lambda x: dt.strptime(x, CAL_FCST_DATEFORMAT)
        )
        return df

    def write(self):
        """
        Ecriture du fichier de prévision produit par GRP (*Calage*)
        """
        raise NotImplementedError

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        de prévision produit par GRP (*Calage*)

        Parameters
        ----------
        filename : str
            Fichier de prévision produit par GRP (*Calage*)

        Returns
        -------
        station : str
            Identifiant de la station
        model : str
            Nom du modèle (GRP)
        snow : str
            Avec/Sans module neige
        error : str
            Modèle d'erreur (RNA, TAN)
        ltime : str
            Echeance de prévision
        timestep : str
            Pas de temps de calcul
        rainfall : str
            Origine des pluies (P0: plue nulle, PP: pluie parfaite)
        periods : str
            Périodes de calage et d'application

        Examples
        --------
        >>> from pyspc.model.grp18 import GRP_Fcst
        >>> f = 'data/model/grp18/cal/H_RH10585x_GRP_SMN_TAN_HOR_00J03H00M_PDT_00J01H00M_PP_P0P0.TXT'
        >>> (station, model, snow, error, ltime, timestep,
        ...  rainfall, periods) = GRP_Fcst.split_basename(filename=f)
        >>> station
        RH10585x
        >>> model
        GRP
        >>> snow
        SMN
        >>> error
        TAN
        >>> ltime
        00J03H00M
        >>> timestep
        00J01H00M
        >>> rainfall
        PP
        >>> periods
        P0P0

        """
        if filename is None:
            return None, None
        basename = os.path.splitext(os.path.basename(filename))[0]
        try:
            [_, station, model, snow, error, _, ltime, _, timestep,
             rainfall, periods] = basename.split('_')
        except ValueError as ve:
            raise ValueError("Le nom de fichier ne respecte pas le "
                             "nommage de GRP") from ve
        return station, model, snow, error, ltime, timestep, rainfall, periods
