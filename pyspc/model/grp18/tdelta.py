#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2018 - Pas de temps
"""
from datetime import timedelta as td
import re

import pyspc.core.exception as _exception

RE_JHM = re.compile(r'^\d{2}J\d{2}H\d{2}M$')


def str2td(tstep=None):
    """
    Définir le pas de temps à partir de la chaine de caractères 'jjJhhHmmM'

    Parameters
    ----------
    tstep : str
        'jjJhhHmmM'

    Returns
    -------
    tdelta : timedelta
        pas de temps

    """
    # Renvoi None si tstep est None
    if tstep is None:
        return None
    # Contrôle de la chaine de caractères
    _exception.check_str(tstep)
    _exception.raise_valueerror(
        not RE_JHM.search(tstep),
        "La chaine de caractère '{}' est incorrecte".format(tstep)
    )
    # Découpage de la chaine de caractères
    d = int(tstep[0:2])
    h = int(tstep[3:5])
    m = int(tstep[6:8])
    t = td(days=d, hours=h, minutes=m)
    # Contrôle du pas de temps
    _exception.raise_valueerror(
        t == td(0),
        "La chaine de caractère '{}' correspond à un pas de temps nul. "
        "Veuillez utiliser None".format(tstep)
    )
    return t


def td2str(tdelta=None):
    """
    Définir le pas de temps en une chaine de caractères 'jjJhhHmmM'

    Parameters
    ----------
    tdelta : timedelta
        pas de temps

    Returns
    -------
    tstep : str
        'jjJhhHmmM'

    """
    # Renvoi None si tdelta est None
    if tdelta is None:
        return None
    # Contrôle du pas de temps
    _exception.check_td(tdelta)
    _exception.raise_valueerror(
        tdelta == td(0),
        "Le pas de temps '{}' correspond à un pas de temps nul. "
        "Veuillez utiliser None".format(tdelta)
    )
    # Conversion en str
    tstep = '{days:02d}J{hours:02d}H{minutes:02d}M'.format(
        days=tdelta.days,
        hours=int(tdelta.seconds / 3600),
        minutes=int((tdelta.seconds / 60) % 60))
    return tstep
