#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2018 - Temps-réel prévision
"""
import os.path
import pandas as pnd

import pyspc.core.exception as _exception
from pyspc.convention.grp18 import (
    RT_FCST_DTYPES, RT_FCST_FILEPREFIX, RT_FCST_LINEPREFIX
)


class GRPRT_Fcst():
    """
    Structure de données GRPRT Fcst (Prévision GRP *Temps Réel*)

    Fichiers

    - GRP_Obs.txt
    - GRP_Simu_2001.txt
    - GRP_Prev_2001.txt
    - GRP_D_Obs.txt
    - GRP_D_Simu_2001.txt
    - GRP_D_Prev_2001.txt

    Attributes
    ----------
    filename : str
        Nom du fichier de prévision
    datatype : str
        Type du fichier de prévision
    fileprefix : str
        Préfixe du nom de base du fichier
    lineprefix : str
        Préfixe des lignes de données du fichier

    """
    def __init__(self, filename=None, datatype=None):
        """
        Initialisation de l'instance de la classe GRPRT_Fcst

        Parameters
        ----------
        filename : str
            Nom du fichier de données
        datatype : str
            Type du fichier de données

        """
        self.check_datatype(datatype=datatype)
        self.datatype = datatype
        self.fileprefix = self.get_fileprefix(datatype=datatype)
        self.lineprefix = self.get_lineprefix(datatype=datatype)
        _exception.raise_valueerror(
            not os.path.basename(filename).startswith(self.fileprefix),
            'Incohérence entre le nom de fichier et le type de fichier'
        )
        self.scen = None
        if self.lineprefix in ['SIM', 'PRV']:
            self.scen = os.path.basename(filename).replace(
                self.fileprefix, '').split('.')[0]
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRPRT_Fcst
        """
        text = """
        *************************************
        *********** GRP 2018 - RT Fcst ******
        *************************************
        *  NOM FICHIER      = {filename}
        *  TYPE FICHIER     = {datatype}
        *  PREFIXE FICHIER  = {fileprefix}
        *  PREFIXE LIGNE    = {lineprefix}
        *  SCENARIO         = {scen}
        *************************************
        """
        return text.format(**vars(self))

    def check_datatype(self, datatype):
        """
        Contrôler le type de fichier
        """
        _exception.raise_valueerror(
            datatype not in self.get_types(),
            'Type de fichier incorrect'
        )

    def read(self):
        """
        Lire le fichier de prévision GRP Temps-Réel / Temps-Différé

        Returns
        -------
        pandas.DataFrame
            Tableau des prévisions de GRP Temps-Réel

        Notes
        -----
        L'analyse des fichiers fournis en exemple (La Capricieuse amont)
        a souligné que la valeur journalière correspond à la moyenne
        des valeurs sur la plage ]J-1 H=0, J H=0]. Le débit journalier est
        donc horodaté à H=0. De façon similaire, les débits horaires et
        journaliers sont horodatés à M=0.

        Examples
        --------
        >>> from pyspc.model.grp18 import GRPRT_Fcst
        >>> f = 'data/model/grp18/rt/GRP_D_Obs.txt'
        >>> reader = GRPRT_Fcst(filename=f, datatype='obs_diff')
        >>> df = reader.read()
        >>> df
            TYP       CODE         PDT            DATE(TU)      DEBIT(m3/s)        PLUIE(mm)  Temperature(°C)
        0   OBS   RH10585x   01J00H00M 2007-01-15 00:00:00           3.5675             6.88           4.4572
        1   OBS   RH10585x   01J00H00M 2007-01-16 00:00:00           2.9225             0.00           2.6503
        2   OBS   RH10585x   01J00H00M 2007-01-17 00:00:00          -9.9900             0.40           4.4628
        3   OBS   RH10585x   01J00H00M 2007-01-18 00:00:00           2.1828             7.84           6.3798
        4   OBS   RH10585x   01J00H00M 2007-01-19 00:00:00           4.8858            70.08           8.2152
        5   OBS   RH10585x   00J01H00M 2007-01-18 00:00:00           2.1717             0.24              NaN
        6   OBS   RH10585x   00J01H00M 2007-01-18 01:00:00           2.2044             1.00              NaN
        7   OBS   RH10585x   00J01H00M 2007-01-18 02:00:00           2.2372             1.36              NaN
        8   OBS   RH10585x   00J01H00M 2007-01-18 03:00:00           2.3018             2.16              NaN
        9   OBS   RH10585x   00J01H00M 2007-01-18 04:00:00           2.4748             2.84              NaN
        10  OBS   RH10585x   00J01H00M 2007-01-18 05:00:00           2.7934             0.12              NaN
        11  OBS   RH10585x   00J01H00M 2007-01-18 06:00:00           3.0027             0.04              NaN
        12  OBS   RH10585x   00J01H00M 2007-01-18 07:00:00           3.0776             0.44              NaN
        13  OBS   RH10585x   00J01H00M 2007-01-18 08:00:00           3.0836             0.04              NaN
        14  OBS   RH10585x   00J01H00M 2007-01-18 09:00:00           3.0208             0.00              NaN
        15  OBS   RH10585x   00J01H00M 2007-01-18 10:00:00           2.9493             0.68              NaN
        16  OBS   RH10585x   00J01H00M 2007-01-18 11:00:00           2.8804             0.28              NaN
        17  OBS   RH10585x   00J01H00M 2007-01-18 12:00:00           2.8227             0.16              NaN
        18  OBS   RH10585x   00J01H00M 2007-01-18 13:00:00           2.7736             0.12              NaN
        19  OBS   RH10585x   00J01H00M 2007-01-18 14:00:00           2.7246             0.20              NaN
        20  OBS   RH10585x   00J01H00M 2007-01-18 15:00:00           2.7099             3.04              NaN
        21  OBS   RH10585x   00J01H00M 2007-01-18 16:00:00           2.7297             3.36              NaN
        22  OBS   RH10585x   00J01H00M 2007-01-18 17:00:00           2.8959             4.16              NaN
        23  OBS   RH10585x   00J01H00M 2007-01-18 18:00:00           3.5374             8.48              NaN
        24  OBS   RH10585x   00J01H00M 2007-01-18 19:00:00           5.1503             3.72              NaN
        25  OBS   RH10585x   00J01H00M 2007-01-18 20:00:00           7.8637             7.60              NaN
        26  OBS   RH10585x   00J01H00M 2007-01-18 21:00:00          10.1921             6.32              NaN
        27  OBS   RH10585x   00J01H00M 2007-01-18 22:00:00          12.4637             9.44              NaN
        28  OBS   RH10585x   00J01H00M 2007-01-18 23:00:00          14.6949             6.64              NaN
        29  OBS   RH10585x   00J01H00M 2007-01-19 00:00:00          16.6246             7.88              NaN

        >>> f = 'data/model/grp18/rt/GRP_D_Simu_2001.txt'
        >>> reader = GRPRT_Fcst(filename=f, datatype='sim_diff')
        >>> df = reader.read()
        >>> df
            TYP       CODE         PDT            DATE(TU)      DEBIT(m3/s)        PLUIE(mm)  Temperature(°C)
        0   SIM   RH10585x   01J00H00M 2007-01-20 00:00:00           22.692            82.96             8.84
        1   SIM   RH10585x   01J00H00M 2007-01-21 00:00:00           17.181            10.48             7.81
        2   SIM   RH10585x   01J00H00M 2007-01-22 00:00:00            8.894             9.00             3.26
        3   SIM   RH10585x   01J00H00M 2007-01-23 00:00:00            5.288             2.04             0.41
        4   SIM   RH10585x   01J00H00M 2007-01-24 00:00:00            3.302             1.12            -2.38
        5   SIM   RH10585x   00J01H00M 2007-01-19 01:00:00           24.745             8.44              NaN
        6   SIM   RH10585x   00J01H00M 2007-01-19 02:00:00           27.738             5.32              NaN
        7   SIM   RH10585x   00J01H00M 2007-01-19 03:00:00           30.068             6.64              NaN
        8   SIM   RH10585x   00J01H00M 2007-01-19 04:00:00           32.090             5.96              NaN
        9   SIM   RH10585x   00J01H00M 2007-01-19 05:00:00           34.197             6.72              NaN
        10  SIM   RH10585x   00J01H00M 2007-01-19 06:00:00           36.502             7.24              NaN
        11  SIM   RH10585x   00J01H00M 2007-01-19 07:00:00           39.073             7.44              NaN
        12  SIM   RH10585x   00J01H00M 2007-01-19 08:00:00           41.422             6.60              NaN
        13  SIM   RH10585x   00J01H00M 2007-01-19 09:00:00           43.287             6.48              NaN
        14  SIM   RH10585x   00J01H00M 2007-01-19 10:00:00           44.463             5.36              NaN
        15  SIM   RH10585x   00J01H00M 2007-01-19 11:00:00           44.319             2.96              NaN
        16  SIM   RH10585x   00J01H00M 2007-01-19 12:00:00           42.518             1.32              NaN
        17  SIM   RH10585x   00J01H00M 2007-01-19 13:00:00           39.783             1.08              NaN
        18  SIM   RH10585x   00J01H00M 2007-01-19 14:00:00           36.843             0.48              NaN
        19  SIM   RH10585x   00J01H00M 2007-01-19 15:00:00           34.327             1.60              NaN
        20  SIM   RH10585x   00J01H00M 2007-01-19 16:00:00           32.251             1.00              NaN
        21  SIM   RH10585x   00J01H00M 2007-01-19 17:00:00           30.321             0.72              NaN
        22  SIM   RH10585x   00J01H00M 2007-01-19 18:00:00           28.449             0.80              NaN
        23  SIM   RH10585x   00J01H00M 2007-01-19 19:00:00           26.908             1.36              NaN
        24  SIM   RH10585x   00J01H00M 2007-01-19 20:00:00           25.835             1.84              NaN
        25  SIM   RH10585x   00J01H00M 2007-01-19 21:00:00           25.143             2.04              NaN
        26  SIM   RH10585x   00J01H00M 2007-01-19 22:00:00           24.338             0.72              NaN
        27  SIM   RH10585x   00J01H00M 2007-01-19 23:00:00           23.155             0.36              NaN
        28  SIM   RH10585x   00J01H00M 2007-01-20 00:00:00           21.828             0.48              NaN
        29  SIM   RH10585x   00J01H00M 2007-01-20 01:00:00           20.576             0.28              NaN
        30  SIM   RH10585x   00J01H00M 2007-01-20 02:00:00           19.414             0.32              NaN
        31  SIM   RH10585x   00J01H00M 2007-01-20 03:00:00           18.339             0.28              NaN
        32  SIM   RH10585x   00J01H00M 2007-01-20 04:00:00           17.414             0.56              NaN
        33  SIM   RH10585x   00J01H00M 2007-01-20 05:00:00           16.543             0.08              NaN
        34  SIM   RH10585x   00J01H00M 2007-01-20 06:00:00           15.663             0.08              NaN


        >>> f = 'data/model/grp18/rt/GRP_D_Prev_2001.txt'
        >>> reader = GRPRT_Fcst(filename=f, datatype='fcst_diff')
        >>> df = reader.read()
        >>> df
            TYP       CODE         PDT            DATE(TU)      DEBIT(m3/s)        PLUIE(mm)  Temperature(°C)
        0   PRV   RH10585x   01J00H00M 2007-01-20 00:00:00           19.996            82.96             8.84
        1   PRV   RH10585x   01J00H00M 2007-01-21 00:00:00           16.506            10.48             7.81
        2   PRV   RH10585x   01J00H00M 2007-01-22 00:00:00            8.722             9.00             3.26
        3   PRV   RH10585x   01J00H00M 2007-01-23 00:00:00            5.227             2.04             0.41
        4   PRV   RH10585x   01J00H00M 2007-01-24 00:00:00            3.275             1.12            -2.38
        5   PRV   RH10585x   00J01H00M 2007-01-19 01:00:00           19.335             8.44              NaN
        6   PRV   RH10585x   00J01H00M 2007-01-19 02:00:00           22.301             5.32              NaN
        7   PRV   RH10585x   00J01H00M 2007-01-19 03:00:00           24.725             6.64              NaN
        8   PRV   RH10585x   00J01H00M 2007-01-19 04:00:00           26.894             5.96              NaN
        9   PRV   RH10585x   00J01H00M 2007-01-19 05:00:00           29.154             6.72              NaN
        10  PRV   RH10585x   00J01H00M 2007-01-19 06:00:00           31.605             7.24              NaN
        11  PRV   RH10585x   00J01H00M 2007-01-19 07:00:00           34.309             7.44              NaN
        12  PRV   RH10585x   00J01H00M 2007-01-19 08:00:00           36.815             6.60              NaN
        13  PRV   RH10585x   00J01H00M 2007-01-19 09:00:00           38.870             6.48              NaN
        14  PRV   RH10585x   00J01H00M 2007-01-19 10:00:00           40.270             5.36              NaN
        15  PRV   RH10585x   00J01H00M 2007-01-19 11:00:00           40.410             2.96              NaN
        16  PRV   RH10585x   00J01H00M 2007-01-19 12:00:00           38.959             1.32              NaN
        17  PRV   RH10585x   00J01H00M 2007-01-19 13:00:00           36.589             1.08              NaN
        18  PRV   RH10585x   00J01H00M 2007-01-19 14:00:00           33.989             0.48              NaN
        19  PRV   RH10585x   00J01H00M 2007-01-19 15:00:00           31.758             1.60              NaN
        20  PRV   RH10585x   00J01H00M 2007-01-19 16:00:00           29.919             1.00              NaN
        21  PRV   RH10585x   00J01H00M 2007-01-19 17:00:00           28.198             0.72              NaN
        22  PRV   RH10585x   00J01H00M 2007-01-19 18:00:00           26.514             0.80              NaN
        23  PRV   RH10585x   00J01H00M 2007-01-19 19:00:00           25.132             1.36              NaN
        24  PRV   RH10585x   00J01H00M 2007-01-19 20:00:00           24.182             1.84              NaN
        25  PRV   RH10585x   00J01H00M 2007-01-19 21:00:00           23.587             2.04              NaN
        26  PRV   RH10585x   00J01H00M 2007-01-19 22:00:00           22.876             0.72              NaN
        27  PRV   RH10585x   00J01H00M 2007-01-19 23:00:00           21.799             0.36              NaN
        28  PRV   RH10585x   00J01H00M 2007-01-20 00:00:00           20.576             0.48              NaN
        29  PRV   RH10585x   00J01H00M 2007-01-20 01:00:00           19.419             0.28              NaN
        30  PRV   RH10585x   00J01H00M 2007-01-20 02:00:00           18.342             0.32              NaN
        31  PRV   RH10585x   00J01H00M 2007-01-20 03:00:00           17.344             0.28              NaN
        32  PRV   RH10585x   00J01H00M 2007-01-20 04:00:00           16.484             0.56              NaN
        33  PRV   RH10585x   00J01H00M 2007-01-20 05:00:00           15.675             0.08              NaN
        34  PRV   RH10585x   00J01H00M 2007-01-20 06:00:00           14.852             0.08              NaN

        """
        df = pnd.read_csv(
            self.filename,
            sep=';',
            encoding='iso-8859-1',
            header=0,
            index_col=False,
            engine='python',
            skipfooter=1,
            na_values=[-99.9, '-99.9000', -9.9900, '-9.9900'],
            converters={'      CODE': str,
                        '       PDT': str,
                        '    DATE(TU)': str,
                        '    DEBIT(m3/s)': float,
                        '      PLUIE(mm)': float,
                        'Temperature(°C)': float},
        )
        df['    DATE(TU)'] = df['    DATE(TU)'].apply(
            lambda x: '{0:0<12s}'.format(x.strip()))
        df['    DATE(TU)'] = pnd.to_datetime(
            df['    DATE(TU)'], format='%Y%m%d%H%M')
        return df

    def write(self, data=None):
        """
        Ecrire le fichier de prévision GRP Temps-Réel / Temps-Différé

        Parameters
        ----------
        pandas.DataFrame
            Tableau des prévisions de GRP Temps-Réel

        """
        # format strftime : selon valeur de row['PDT']
        raise NotImplementedError

    @classmethod
    def get_fileprefix(cls, datatype=None):
        """
        Préfixe des fichiers de données

        Parameters
        ----------
        datatype : str
            Type du fichier de prévision

        Returns
        -------
        p : str
            Préfixe des fichiers de données

        Examples
        --------
        >>> from pyspc.model.grp18 import GRPRT_Fcst
        >>> d = 'obs'
        >>> p = GRPRT_Fcst.get_fileprefix(datatype=d)
        >>> p
        GRP_D_Obs

        >>> d = 'sim'
        >>> p = GRPRT_Fcst.get_fileprefix(datatype=d)
        >>> p
        GRP_D_Simu_

        >>> d = 'fcst'
        >>> p = GRPRT_Fcst.get_fileprefix(datatype=d)
        >>> p
        GRP_D_Prev_

        """
        try:
            p = RT_FCST_FILEPREFIX[datatype]
        except KeyError as ke:
            raise ValueError('Type de donnée incorrect pour la '
                             'définition du préfixe') from ke
        return p

    @classmethod
    def get_lineprefix(cls, datatype=None):
        """
        Préfixe des lignes de données

        Parameters
        ----------
        datatype : str
            Type du fichier de prévision

        Returns
        -------
        p : str
            Préfixe des lignes de données

        Examples
        --------
        >>> from pyspc.model.grp18 import GRPRT_Fcst
        >>> d = 'obs'
        >>> p = GRPRT_Fcst.get_lineprefix(datatype=d)
        >>> p
        OBS

        >>> d = 'sim'
        >>> p = GRPRT_Fcst.get_lineprefix(datatype=d)
        >>> p
        SIM

        >>> d = 'fcst'
        >>> p = GRPRT_Fcst.get_lineprefix(datatype=d)
        >>> p
        PRV

        """
        try:
            p = RT_FCST_LINEPREFIX[datatype.split('_')[0]]
        except KeyError as ke:
            raise ValueError('Type de donnée incorrect pour la '
                             'définition du préfixe') from ke
        return p

    @classmethod
    def get_types(cls):
        """
        Type de fichier de prévision GRP Temps-réel

        - obs       : observation temps-réel
        - obs_diff  : observation temps différé
        - sim       : prévision sans assimilation temps-réel
        - sim_diff  : prévision sans assimilation temps différé
        - fcst      : prévision avec assimilation temps-réel
        - fcst_diff : prévision avec assimilation temps différé

        Returns
        -------
        list
            Liste des types de fichier de prévision GRP Temps-réel

        """
        return sorted(RT_FCST_DTYPES)
