#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2018 - Temps-réel bassin
"""
import collections


class GRPRT_Basin(collections.OrderedDict):
    """
    Structure de données GRP Basin (Fichier Bassin de GRP *Temps Réel*)

    - BASSIN.DAT

    Attributes
    ----------
    filename : str
        Nom du fichier Bassin de GRP *Temps-Réel*

    Notes
    -----
    Cette structure de données est un dictionnaire ordonné. La balise d'une
    information est la clé et la structure contient les méta-données dans ses
    valeurs

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRPRT_Basin

        Parameters
        ----------
        filename : str
            Nom du fichier Bassin de GRP *Temps-Réel*

        """
        super().__init__()
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRPRT_Basin
        """
        text = """
        *************************************
        *********** GRP 2018 - RT Basin *****
        *************************************
        *  NOM FICHIER      = {filename}
        *  INFORMATIONS     = {content}
        *************************************
        """
        info = {'content': dict(self)}
        return text.format(filename=self.filename, **info)

    def read(self):
        """
        Lecture du fichier Bassin de GRP *Temps *Réel*
        """
        self.clear()
        info_basin = collections.OrderedDict()
#        with open(self.filename, 'r', encoding='utf-8') as f:
        with open(self.filename, 'r', encoding='iso-8859-1') as f:
            for line in f.readlines():
                # Une ligne de commentaire commence par "#"
                if line.startswith("#"):
                    continue
                # Balise
                tag = line[0]
                # Découpage de la ligne
                x = line.split("!", 1)[0][2:]
                # Méta-données du bassin
                if tag in ['A', 'F', 'G', 'K', 'N', 'S']:
                    info_basin[tag] = float(x)
                elif tag in ['B', 'Q', 'L']:
                    info_basin[tag] = x.strip()
                # Stations d'entrée
                elif tag in ['E']:
                    info_basin.setdefault(tag, collections.OrderedDict())
                    x = list(filter(None, x.split(" ")))
                    c = x[0]
                    w = _convert_float(x[1], 0.0)
                    info_basin[tag].setdefault(c, {'w': w, 'n': ''})
                # Stations PLUIE
                elif tag in ['P']:
                    info_basin.setdefault(tag, collections.OrderedDict())
                    x = list(filter(None, x.split(" ")))
                    c = x[0]
                    w = _convert_float(x[1], 0.0)
                    t = x[2]
                    info_basin[tag].setdefault(c, {'w': w, 't': t, 'n': ''})
                    if len(x) > 3:
                        info_basin[tag][c]['n'] = " ".join(x[3:])
                # Stations TEMPERATURE
                elif tag in ['D']:
                    info_basin.setdefault(tag, collections.OrderedDict())
                    x = list(filter(None, x.split(" ")))
                    c = x[0]
                    w = _convert_float(x[1], 0.0)
                    z = _convert_float(x[2], -1.0)
                    info_basin[tag].setdefault(c, {'w': w, 'z': z, 'n': ''})
                # Bande ALTITUDE
                elif tag in ['C']:
                    info_basin.setdefault(tag, collections.OrderedDict())
                    x = list(filter(None, x.split(" ")))[2:]
                    c = x[0]
                    w = _convert_float(x[1], 0.0)
                    z = _convert_float(x[2], -1.0)
                    info_basin[tag].setdefault(c, {'snma': w, 'z50': z,
                                                   'n': ''})
                else:
                    raise NotImplementedError
        self.update(info_basin)

    def write(self):
        """
        Ecriture du fichier Bassin de GRP *Temps Réel*
        """
        raise NotImplementedError


def _convert_float(value=None, default=0.0):
    """"""
    try:
        x = float(value)
    except (IndexError, TypeError):
        x = default
    return x
