#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Modélisations hydrologiques - GRP version 2018 - Bassin
"""
import collections
import os.path

from pyspc.convention.grp18 import CAL_BASIN_LINESEP, CAL_BASIN_HEADERS


class GRP_Basin(collections.OrderedDict):
    """
    Structure du fichier Bassin de GRP *Calage*

    Attributes
    ----------
    filename : str
        Nom du fichier Bassin de GRP *Calage*

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe GRP_Data

        Parameters
        ----------
        filename : str
            Nom du fichier Bassin de GRP *Calage*

        """
        super().__init__()
        self.filename = filename
        if self.filename is not None:
            info = self.split_basename(self.filename)
            self.location = info[0]
            self.timestep = info[1]
        else:
            self.location = None
            self.timestep = None

    def __str__(self):
        """
        Afficher les méta-données de l'instance GRP_Basin
        """
        text = """
        *************************************
        *********** GRP 2018 - Basin ********
        *************************************
        *  NOM FICHIER      = {filename}
        *  LIEU MODELE      = {location}
        *  PAS DE TEMPS     = {timestep}
        *  INFORMATIONS     = {content}
        *************************************
        """
        content = ""
        for k, v in self.items():
            if isinstance(v, str):
                content += '\n        *      + {0} = {1}'.format(k, v)
            elif isinstance(v, (dict, collections.OrderedDict)):
                content += '\n        *      + {0}'.format(k)
                for k2, v2 in v.items():
                    content += '\n        *          + {0} = {1}'.format(k2,
                                                                         v2)
        return text.format(filename=self.filename, location=self.location,
                           timestep=self.timestep, content=content)

    def read(self):
        """
        Lecture du fichier Bassin de GRP *Calage*
        """
        content = {}
        with open(self.filename, 'r', encoding='utf-8') as f:
            for line in f:
                # Une ligne de commentaire commence par "#" ou "!"
                if line.startswith("!") or line.startswith("#"):
                    continue
                # Balise
                tag = line[0]
                # Ajout de la balise
                content.setdefault(tag, collections.OrderedDict())
                # Découpage de la ligne
                x = line.split("!", 1)[0][2:].split(" ")
                x = list(filter(None, x))
                code = x.pop(0)  # Id Station
                # Nom de l'entité
                try:
                    name = line.split("!")[1].strip()
                except IndexError:
                    name = ''
                # Spécificités N/L
                if tag in ['N']:
                    content[tag] = float(code)
                    continue
                if tag in ['L']:
                    content[tag] = {'t': code, 'n': name}
                    continue
                # Ajout de l'entité
                content[tag].setdefault(code, {})
                content[tag][code]['n'] = name
                # Pondération de l'entité
                try:
                    w = float(x.pop(0))  # Poids Station
                except IndexError:
                    pass
                else:
                    content[tag][code]['w'] = w
                # 3e élément : z (si T), pdt (si P)
                try:
                    t = x.pop(0)
                except IndexError:
                    pass
                else:
                    if tag == 'P':
                        content[tag][code]['t'] = t
                    elif tag == 'T':
                        content[tag][code]['z'] = float(t)  # Altitude
        self.update(content)

    def write(self):
        """
        Ecriture du fichier Bassin de GRP *Calage*
        """
        with open(self.filename, 'w', encoding='utf-8', newline="\r\n") as f:
            # Première ligne
            f.write(CAL_BASIN_LINESEP)
            # Boucle sur les étiquettes
            for tag in self.keys():
                # Entête de l'étiquette
                f.write(CAL_BASIN_HEADERS[tag])
                if tag in ['N']:
                    f.write('{0:1s}'.format(tag))
                    f.write(' {0:9.2f}'.format(self[tag]))
                    f.write(' {0:<98s} !'.format(""))
                    f.write('\n')
                    f.write(CAL_BASIN_LINESEP)
                    continue
                if tag in ['L']:
                    f.write('{0:1s}'.format(tag))
                    f.write(' {0:9s}'.format(self[tag]['t']))
                    f.write(' {0:<15s}'.format(""))
                    f.write('! {0:<81s} !'.format(self[tag]['n']))
                    f.write('\n')
                    f.write(CAL_BASIN_LINESEP)
                    continue
                # Boucle sur les entités de l'étiquette
                for code in self[tag].keys():
                    # Etiquette
                    f.write('{0:1s}'.format(tag))
                    # Station
                    f.write(' {0:8s}'.format(code))
                    # Pondération
                    f.write(' {0:9.2f}'.format(self[tag][code]['w']))
                    # Altitude - Nom
                    if tag == 'T':
                        f.write(' {0:7.2f}'.format(self[tag][code]['z']))
                        f.write(' ! {0:<79s} !'.format(self[tag][code]['n']))
                    elif tag == 'P':
                        f.write(' {0:9s}'.format(self[tag][code]['t']))
                        f.write(' ! {0:<77s} !'.format(self[tag][code]['n']))
                    else:
                        f.write(' ! {0:<87s} !'.format(self[tag][code]['n']))
                    f.write('\n')
                f.write(CAL_BASIN_LINESEP)

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier
        de données GRP Basin (GRP *Calage*)

        Parameters
        ----------
        filename : str
            Fichier de données GRP Basin (GRP *Calage*)

        Returns
        -------
        location : str
            Identifiant de la station
        timestep : str
            Pas de temps du modèle

        Examples
        --------
        >>> from pyspc.model.grp18 import GRP_Basin
        >>> f = 'data/model/grp18/cal/RH10585x_00J01H00M.DAT'
        >>> [location, timestep] = GRP_Basin.split_basename(filename=f)
        >>> location
        RH10585x
        >>> varname
        00J01H00M

        """
        if filename is None:
            return None, None
        basename = os.path.splitext(os.path.basename(filename))[0]
        try:
            [location, timestep] = basename.split('_')
        except ValueError as ve:
            raise ValueError("Le nom de fichier ne respecte pas le "
                             "nommage de GRP") from ve
        return (location, timestep)

    @staticmethod
    def join_basename(location=None, timestep=None):
        """
        Définir le nom du fichier de données GRP Basin (GRP *Calage*)
        à partir des informations

        Parameters
        ----------
        location : str
            Identifiant de la station
        timestep : str
            Pas de temps du modèle

        Returns
        -------
        filename : str
            Fichier de données GRP Basin (GRP *Calage*)

        """
        if location is None or timestep is None:
            raise ValueError('Définition incorrecte des arguments')
        return '{}_{}.DAT'.format(location, timestep)
