#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Serie - Méthodes spécifiques - Combiner, mettre à jour
"""
from datetime import datetime as dt, timedelta as td
import difflib
import io
import numpy as np
import pandas as pnd

import pyspc.core.exception as _exception


class Combining():
    """
    Classe spécifique pour les mélanges à partir de pyspc.core.serie.Serie
    """
    def __init__(self):
        """
        Mélanges, mises à jour
        """
        self.data_frame = None
        self._firstdt = None
        self._lastdt = None
        self._length = None

    def comp(self, other, fromfile=None, tofile=None, media='screen'):
        """
        Comparer 2 instances Serie

        Parameters
        ----------
        other : pyspc.core.serie.Serie
            Séries de données de référence
        fromfile : str
            Nom de la référence. Si non défini, prend la valeur other.code
        tofile : str
            Nom de la série courante. Si non défini, prend la valeur self.code
        media : str, io.IOBase, None
            - 'screen' pour impression écran (défaut)
            - si io.IOBase : Objet fichier, export de la comparaison.
            - si None : ni impression ni export, la comparaison est retournée

        Returns
        -------
        lines : list
            Liste des valeurs comparées, si media = None

        """
        test = _exception.raise_valueerror(
            not isinstance(other, type(self)),
            "pas de mise-à-jour car other n'est pas une instance Serie",
            self.warning
        )
        if test:
            return None
        # Définition des identifiants
        # de la série (self, tofile)
        # et de la référence (other, fromfile)
        if tofile is None:
            tofile = self.code
        if fromfile is None:
            fromfile = other.code
        # Traitement de la série <other>
        s1 = [dt.strftime(kt, other.dtfmt) +
              "\t" + "{0:.3f}".format(val[(other.code, other.spc_varname)])
              for kt, val in other.data_frame.iterrows()]
        # Traitement de la série <self>
        s2 = [dt.strftime(kt, self.dtfmt) +
              "\t" + "{0:.3f}".format(val[(self.code, self.spc_varname)])
              for kt, val in self.data_frame.iterrows()]
        # Application de 'difflib'
        # for line in difflib.context_diff(
        #     s1, s2, fromfile=fromfile, tofile=tofile):
        lines = []
        for line in difflib.unified_diff(
                s1, s2, fromfile=fromfile, tofile=tofile):
            if isinstance(media, io.IOBase):
                media.write(line)
                media.write('\n')
            elif media == 'screen':
                print(line)
            else:
                pass
            lines.append(line)
        return lines

    def copy_year(self, src=None, year=None, start=None, end=None):
        """
        Dupliquer le bloc annuel year de la source src
        pour compléter les données de la receveuse self
        entre les dates start et end

        Parameters
        ----------
        src : pyspc.core.serie.Serie
            Séries de données de référence
        year : int
            Année à dupliquer
        start : datetime
            Premier pas de temps ciblé
        end : datetime
            Dernier pas de temps ciblé

        """
        # =====================================================================
        # Contrôles des paramètres
        _exception.raise_valueerror(
            not isinstance(src, type(self)),
            "La source n'est pas une instance Serie. "
            "La copie annuelle ne s'applique pas"
        )
        _exception.raise_valueerror(
            self.spc_varname.endswith('I'),
            "La donnée est instantanée. La copie annuelle ne s'applique pas"
        )
        _exception.raise_valueerror(
            self.spc_varname != src.spc_varname,
            "Les paramètres ne sont pas identiques. "
            "La copie annuelle ne s'applique pas"
        )
        _exception.check_int(year)
        _exception.check_dt(start)
        _exception.check_dt(end)
        # =====================================================================
        # Sélection des données de l'année <year>
        if self.timestep < td(hours=1):
            first_dt = dt(year, 1, 1, 0, 0)
            last_dt = dt(year, 12, 31, 23, 59)
        elif self.timestep < td(days=1):
            first_dt = dt(year, 1, 1, 0)
            last_dt = dt(year, 12, 31, 23)
        else:
            first_dt = dt(year, 1, 1)
            last_dt = dt(year, 12, 31)
        _exception.raise_valueerror(
            last_dt < src.firstdt or first_dt > src.lastdt,
            "La donnée <src> ne contient pas l'année à dupliquer",
        )
        src = src.between_dates(first_dt, last_dt, inplace=False)
        _exception.raise_valueerror(
            src is None,
            "La donnée <src> ne contient pas l'année à dupliquer",
        )
        # =====================================================================
        # Construction du data_frame qui contiendra
        # les données dupliquées
        to_dates = pnd.date_range(start=start, end=end, freq=self.timestep)
        to_data = pnd.DataFrame([np.nan]*len(to_dates), index=to_dates)
        to_data.columns = ['values']  # [(self.code, self.spc_varname)]
        # Remplissage du data_frame qui contiendra
        # les données dupliquées
        for idx in to_data.index:
            try:
                idx_year = idx.replace(year=year)
            except ValueError:
                if self.warning:
                    _exception.Warning(
                        __name__,
                        "le jour {} n'existe pas dans l'année {}"
                        "".format(idx.strftime('%d/%m'), year))
            else:
                try:
                    val = src.data_frame[
                        (src.code, src.spc_varname)].loc[idx_year]
                except KeyError:
                    pass
                else:
                    to_data['values'].loc[idx] = val
        # Instance SPC Data des données dupliquées
        to_spcdata = self.__class__(
              to_data,
              code=self.code,
              varname=self.varname,
              missing=self.missing
        )
        # Mise à jour de l'instance <self> avec les données dupliquées
        self.update(to_spcdata, overwrite=True)

    def update(self, other=None, overwrite=False, strict=False):
        """
        Mettre à jour de l'instance Serie
        par le contenu d'une autre instance Serie

        Parameters
        ----------
        other : pyspc.core.serie.Serie
            Séries de données de référence
        overwrite : bool
            Ecraser les valeurs de self par celles d'other ? Par défaut: False,
            ce qui signifie que seules les valeurs manquantes sont remplacées
        strict : bool
            Forcer le respect de la concordance des codes des deux Serie ?
            Par défaut: False

        """
        # =====================================================================
        # Contrôles des paramètres
        _exception.check_bool(overwrite)
        _exception.check_bool(strict)
        tests = []
        test = _exception.raise_valueerror(
            not isinstance(other, type(self)),
            "pas de mise-à-jour car other n'est pas une instance Serie",
            self.warning
        )
        tests.append(test)
        test = _exception.raise_valueerror(
            not isinstance(self.data_frame, pnd.DataFrame)
            or not isinstance(other.data_frame, pnd.DataFrame),
            "pas de mise-à-jour car les dataframes n'existent pas",
            self.warning
        )
        tests.append(test)
        test = _exception.raise_valueerror(
            self.spc_varname != other.spc_varname,
            "pas de mise-à-jour car noms de variable distincts",
            self.warning
        )
        tests.append(test)
        # Contrôles spécifiques si strict = True
        if strict:
            test = _exception.raise_valueerror(
                self.code != other.code,
                "pas de mise-à-jour car identifiants de station distincts",
                self.warning
            )
            tests.append(test)
        # Bilan des contrôles
        if any(tests):
            return None
        # =====================================================================
        if overwrite:
            df = other.data_frame.combine_first(self.data_frame)
        else:
            df = self.data_frame.combine_first(other.data_frame)
        if df is None:
            return None
        # =====================================================================
        self.data_frame = df
        if self.fill and not self.spc_varname.endswith('I'):
            self.reindex()
        # Si pas de reindex, tri chronologique
        else:
            self.data_frame.sort_index(inplace=True)
        self._length = len(self.data_frame)
        self._firstdt = self.data_frame.index[0].to_pydatetime()
        self._lastdt = self.data_frame.index[-1].to_pydatetime()
        return None
