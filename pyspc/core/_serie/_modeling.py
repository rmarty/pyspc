#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Serie - Méthodes spécifiques - Modélisation
"""
from datetime import timedelta as td
import functools
import numpy as np
import pandas as pnd

import pyspc.model.oudin as _oudin
import pyspc.model.sauquet as _sauquet
import pyspc.model.socose as _socose
import pyspc.core.exception as _exception
from pyspc.core.keyseries import str2tuple, tuple2str
from pyspc.core.parameter import Parameter
from pyspc.core.ratingcurve import RatingCurves
from pyspc.core.reservoir import Reservoir, Table


def _build_sim2fcst(serie, ref, r, error_ratio, leadtimes):
    """
    Construire une prévision à partir d'une simulation
    et d'une durée de recalage

    Parameters
    ----------
    serie : Serie
        Série de données de la simulation
    ref : Serie
        Série de référence (souvent: les observations)
    r : datetime
        Instant de prévision (début de la prévision, i.e. DtDerObs)
    error_ratio : list
        Liste des ratios de décroissance de l'erreur
    leadtimes : list
        Liste des échéances (int)

    Returns
    -------
    dtvals : list
        Prévision sous forme d'une liste de (date, valeur)

    """
    colserie = (serie.code, serie.spc_varname)
    colref = (ref.code, ref.spc_varname)
    try:
        valsim = float(serie.data_frame[colserie].loc[r])
        valref = float(ref.data_frame[colref].loc[r])
#    except ZeroDivisionError:
    except (KeyError, IndexError):
        return None
    dtvals = []
    valerr = valref - valsim
    valerrs = [valerr * ratio for ratio in error_ratio]
    for x, e in zip(leadtimes, valerrs):
        v = r + x * serie.timestep
        try:
            val = float(serie.data_frame[colserie].loc[v])
        except (KeyError, IndexError):
            val = np.nan
        else:
            val += e
        finally:
            dtvals.append((v, val))
    return dtvals


def _convert_res_table(value, table=None, col1=None, col2=None):
    """
    Convertir à partir d'une table d'un réservoir
    """
    return table.convert(value=value, col1=col1, col2=col2)


class Modeling():
    """
    Classe spécifique pour la modélisation à partir de pyspc.core.serie.Serie
    """
    def __init__(self):
        """
        Modélisation
        """
        self._code = None
        self._spc_varname = None
        self._timestep = None

    def apply_RatingCurves(self, curves=None, tocode=None,
                           extrapolation=False):
        """
        Appliquer les courbes de tarage à la série

        Parameters
        ----------
        curves : pyspc.core.ratingcurve.RatingCurves
            Courbes de tarage et de correction
        tocode : str
            Code de la série finale
                - si série de hauteurs, contient le code de la station
                - si série de débits, contient le code du site
        extrapolation : bool
            Autoriser les conversions hors plage de validité
            des courbes de tarage. Défaut: False


        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.raise_valueerror(
            not isinstance(curves, RatingCurves),
            'Les courbes de tarage sont incorrectement définies'
        )
        if tocode is None:
            tocode = self.code
        _exception.check_str(tocode)
        _exception.check_bool(extrapolation)
        rtcpar = self.parameter.apply_RatingCurves()
        # ---------------------------------------------------------------------
        # 1- Détermination des CT disponibles
        # ---------------------------------------------------------------------
        curves = curves.select(
            codes=[self.code, tocode], start=self.firstdt, end=self.lastdt)
        _exception.raise_valueerror(
            len(curves) == 0,
            "Aucune courbe de tarage disponible pour les identifiants {} "
            "et la période du {} au {}".format(
                [self.code, tocode], self.firstdt, self.lastdt),
            self.warning
        )
        for c in set(curves.codes):
            try:
                curves.check_overlapping(code=c)
#                curves.check_overlapping(code=self.code)
#                curves.check_overlapping(code=tocode)
            except ValueError:
                _exception.raise_valueerror(
                    True,
                    "Non unicité temporelle des courbes de tarage pour la "
                    "série {}".format(self.code), self.warning
                )
        # ---------------------------------------------------------------------
        # 2- Application COURBE TARAGE
        # ---------------------------------------------------------------------
        convframe = pnd.DataFrame(
            {(tocode, rtcpar.spc_varname): [np.nan] * self.length},
            index=self.data_frame.index)
        for curve in curves.values():
            # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            # 2.1 - Sélection des données communes
            #       avec la période de validité de la courbe de tarage
            # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            start = max(self.firstdt, curve.valid_dt[0])
            end = min(self.lastdt, curve.valid_dt[1])
            subserie = self.between_dates(start, end, inplace=False)
            # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            # 2.2 - Courbe de correction interpolée
            # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            levelcor = None
            if curve.levelcor is not None:
                levelcor = curve.interpolate_levelcor(
                    subserie.data_frame.index)
            # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            # 2.2 - Application de la courbe de correction si H -> Q
            #       H' = H + levelcor
            # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            if curve.levelcor is not None and self.spc_varname.startswith('H'):
                subserie.data_frame[(self.code, self.spc_varname)] = \
                    subserie.data_frame[(self.code, self.spc_varname)] + \
                    levelcor['h']
            values = subserie.data_frame[(self.code, self.spc_varname)].values
            # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            # 2.3 - Conversion par courbe de tarage
            # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            conv = curve.convert(values=values,
                                 col=self.spc_varname[0].lower(),
                                 extrapolation=extrapolation)
            conv = pnd.DataFrame(
                {(tocode, rtcpar.spc_varname): conv},
                index=subserie.data_frame.index)
            # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            # 2.4 - Application de la courbe de correction si Q -> H
            #       H' = H - levelcor
            # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            if curve.levelcor is not None and self.spc_varname.startswith('Q'):
                conv[(tocode, rtcpar.spc_varname)] = \
                    conv[(tocode, rtcpar.spc_varname)] - levelcor['h']
            # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            # 2.5 - Mise à jour du pnd.DataFrame final
            # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            convframe.update(conv, overwrite=False)
        # ---------------------------------------------------------------------
        # 3- Création de la série convertie
        # ---------------------------------------------------------------------
        convserie = self.__class__(
            convframe, code=tocode, varname=rtcpar.spc_varname)
        # ---------------------------------------------------------------------
        # 4- Messages d'avertissement
        # ---------------------------------------------------------------------
        if self.warning and \
                convserie.counter_missing() > self.counter_missing():
            _exception.Warning(
                __name__, "Certaines valeurs n'ont pu être converties")
        # ---------------------------------------------------------------------
        # 5- Retour de la série convertie
        # ---------------------------------------------------------------------
        return convserie

    def apply_Reservoir(self, reservoir=None, tablename=None, assoc=None,
                        sim=False):
        """
        Appliquer le Z0 et un bareme de réservoir
        et créer une collections Series à partir de la série courante

        Parameters
        ----------
        reservoir : pyspc.core.reservoir.Reservoir
            Instance de reservoir.Reservoir
        tablename : str
            Nom du bareme
        assoc : dict
            Correspondance des colonnes de la table
        sim : bool
            Définir les séries calculées comme étant des simulations.
            Défaut: False. Si True, le nom de la simulation est donné par
            reservoir.name

        Returns
        -------
        pyspc.core.series.Series
            Collection de séries converties par le réservoir et son barème

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.raise_valueerror(
            not isinstance(reservoir, Reservoir),
            "Réservoir incorrect"
        )
        _exception.raise_valueerror(
            not self.code.startswith(reservoir.code)
            and not reservoir.code.startswith(self.code),
            'Incohérence entre les codes du réservoir et de la série'
        )
        _exception.raise_valueerror(
            tablename not in reservoir.tables,
            'Bareme de réservoir inconnu'
        )
        table = reservoir.tables[tablename]
        _exception.raise_valueerror(
            not isinstance(table, Table),
            'Bareme de réservoir incorrect'
        )
        # ---------------------------------------------------------------------
        # 1- Initialisation
        # ---------------------------------------------------------------------
        series = self._constructor_expanddim(datatype='obs', name='Reservoir')
        meta = None
        if sim:
            meta = reservoir.name
        # ---------------------------------------------------------------------
        # 2- Conversion H <-> Z par Z0 : 1e tentative à partir de self
        # ---------------------------------------------------------------------
        try:
            s0 = self.apply_ReservoirZ0(reservoir=reservoir)
        except ValueError:
            test_Z0 = False
        else:
            test_Z0 = True
            series.add(code=s0.code, serie=s0, meta=meta)
        # ---------------------------------------------------------------------
        # 3- Conversion par le bareme
        # ---------------------------------------------------------------------
        ss = self.apply_ReservoirTable(table=table, assoc=assoc)
        if len(ss) == 0:
            ss = s0.apply_ReservoirTable(table=table, assoc=assoc)
        series.extend(series=ss)
        # ---------------------------------------------------------------------
        # 4- Conversion H <-> Z par Z0 : 2e tentative à partir de self
        # ---------------------------------------------------------------------
        if not test_Z0:
            for s in ss:
                try:
                    s0 = ss[s].apply_ReservoirZ0(reservoir=reservoir)
                except ValueError:
                    pass
                else:
                    series.add(code=s0.code, serie=s0, meta=meta)
        return series

    def apply_ReservoirTable(self, table=None, assoc=None, sim=False):
        """
        Appliquer un bareme de réservoir et créer une collections Series
        à partir de la série courante

        Parameters
        ----------
        tablename : pyspc.core.reservoir.Table
            Bareme du réservoir
        assoc : dict
            Correspondance des colonnes de la table
        sim : bool
            Définir les séries calculées comme étant des simulations.
            Défaut: False. Si True, le nom de la simulation est donné par
            table.name

        Returns
        -------
        pyspc.core.series.Series
            Collection de séries converties par le barème

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.raise_valueerror(
            not isinstance(table, Table),
            'Bareme de réservoir incorrect'
        )
        meta = None
        if sim:
            meta = table.name
        # ---------------------------------------------------------------------
        # 1- Table de correspondance
        # ---------------------------------------------------------------------
        if assoc is None:
            assoc = {}
        assoc = {**table.assoc, **assoc}
        # ---------------------------------------------------------------------
        # 2- Collection de séries
        # ---------------------------------------------------------------------
        series = self._constructor_expanddim(datatype='obs', name='Table')
        for a in assoc:
            try:
                aa = assoc[a]
                v = self.parameter.apply_ReservoirTable(col=aa)
                if v.spc_varname == self.spc_varname:
                    continue
                df = self.data_frame[(self.code, self.spc_varname)].map(
                    functools.partial(
                        _convert_res_table, table=table,
                        col1=assoc[self.spc_varname[0]], col2=assoc[a]))
                df = df.to_frame()
                df.columns = [(self.code, v.spc_varname)]
            except (AttributeError, ValueError, KeyError):
                continue
            serie = self.__class__(
                df,
                code=self.code,
                provider=self.provider,
                varname=v.spc_varname
            )
            series.add(code=serie.code, serie=serie, meta=meta)
        return series

    def apply_ReservoirZ0(self, reservoir=None):
        """
        Appliquer le Z0 de réservoir

        Parameters
        ----------
        reservoir : pyspc.core.reservoir.Reservoir
            Instance de reservoir.Reservoir

        Returns
        -------
        pyspc.core.series.Serie
            Série de données converties par l'application du Z0 du réservoir

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.raise_valueerror(
            not isinstance(reservoir, Reservoir),
            "Réservoir incorrect"
        )
        # ---------------------------------------------------------------------
        # 1- Conversion
        # ---------------------------------------------------------------------
        v = self.parameter.apply_ReservoirZ0()
        ratio = 1
        if self.spc_varname.startswith('Z'):
            ratio = -1
        df = self.data_frame + ratio * reservoir.Z0
        df.columns = [(self.code, v.spc_varname)]
        return self.__class__(
            df,
            code=self.code,
            provider=self.provider,
            varname=v.spc_varname
        )

    def etp_oudin(self, latitude=None, timestep=None):
        """
        Modélisation de l'ETP par Oudin à partir de série de température
        et d'une valeur de latitude

        Parameters
        ----------
        latitude : float
            Latitude de la série
        timestep : timedelta
            Pas de temps de la série d'ETP calculée (horaire ou journalier).
            Si non défini, la série d'ETP sera au même pas de temps que la
            série de température, si celui-ci est horaire ou journalier

        Returns
        -------
        pyspc.core.serie.Serie
            Série des valeurs d'ETP

        Raises
        ------
        ValueError
            si la série n'est pas une série de température

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.raise_valueerror(
            not self.spc_varname.startswith('T'),
            "le type de données '{0}' est incorrect pour le calcul de l'ETP "
            "par Oudin".format(self.spc_varname)
        )
        _exception.check_numeric(latitude)
        if timestep is None:
            timestep = self.timestep
        _exception.raise_valueerror(
            timestep not in [td(days=1), td(hours=1)],
            "le pas de temps final '{0}' est incorrect pour le calcul de "
            "l'ETP par Oudin".format(timestep)
        )
        # ---------------------------------------------------------------------
        # 1- Changement de pas de temps
        #    TI -> TH
        #    TH -> TJ
        # ---------------------------------------------------------------------
        if self.timestep is None:
            serie_h = self.regularscale()
        else:
            serie_h = self.copy()
        if serie_h.timestep == td(hours=1):
            serie_j = serie_h.upscale(
                toparam='TJ',
                dayhour=6,
                strict=True
            )
        else:
            serie_j = serie_h.copy()
        # ---------------------------------------------------------------------
        # 2- Application de la formulation d'Oudin
        # ---------------------------------------------------------------------
        df_etp_j = _oudin.daily_ETP(
            data_tj=serie_j.data_frame, latitude=latitude)
        # ---------------------------------------------------------------------
        # 3- Retour de la série d'ETP
        # ---------------------------------------------------------------------
        p = Parameter.find(prefix='E', timedelta=timestep)
        if p.timestep == td(hours=1):
            df_etp_h = _oudin.daily2hourly(df_etp_j)
            return self.__class__(
                df_etp_h,
                code=self.code,
                varname=p.spc_varname
            )
        return self.__class__(
            df_etp_j,
            code=self.code,
            varname=p.spc_varname
        )

    def regime_sauquet(self):
        """
        Définir la similarité du régime hydro avec les références Sauquet.

        Returns
        -------
        norm : pandas.DataFrame
            Valeurs mensuelles normalisées
        regimes : pandas.DataFrame
            Identifiants des colonnes de df, des régimes et critère de
            similarité

        See Also
        --------
        pyspc.model.sauquet.REGIMES
        pyspc.model.sauquet.compute_distance

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.raise_valueerror(
            not self.spc_varname.startswith('Q'),
            "La série doit être une chronique de débit."
        )
        # ---------------------------------------------------------------------
        # 1- Calcul du régime hydrologique mensuel
        # ---------------------------------------------------------------------
        try:
            regime = self.regime(groupby='month')
            key = (self.code, 'QM', 'mean')
            df = regime[key].to_frame()
            df.columns = [self.code]
        except ValueError:
            return None, None
        # ---------------------------------------------------------------------
        # 2- Application des références Sauquet
        # ---------------------------------------------------------------------
        return _sauquet.compute_distance(df)

    def sim2fcst(self, ref=None, first_dtime=None, last_dtime=None,
                 error_depth=0, max_ltime=120):
        """
        Convertir une simulation en prévision
        avec prise en compte de l'erreur à l'instant de prévision
        qui décroit linéairement avec l'échéance de prévision

        Parameters
        ----------
        ref : Serie
            Série de référence
        error_depth : int
            Durée de prise en compte de l'erreur, en pas de temps. Défaut: 0
        first_dt : datetime
            Premier instant de prévision
        last_dt : datetime
            Dernier instant de prévision
        max_ltime : int
            Horizon de prévision maximal, en pas de temps. Défaut: 120

        Returns
        -------
        series : Series
            Collection de prévisions
        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.raise_valueerror(
            not isinstance(ref, type(self)),
            "La série de référence n'est pas une Serie"
        )
        _exception.raise_valueerror(
            ref.spc_varname != self.spc_varname,
            "La série de référence ne correspond pas à la même grandeur"
        )
        if first_dtime is None:
            first_dtime = self.firstdt
        if last_dtime is None:
            last_dtime = self.lastdt
        _exception.check_dt(first_dtime)
        _exception.check_dt(last_dtime)
        _exception.raise_valueerror(
            last_dtime < self.firstdt or first_dtime > self.lastdt,
            "Incohérence entre les instants de prévision et les dates de la "
            "simulation"
        )
        _exception.check_int(error_depth)
        _exception.check_int(max_ltime)
        # ---------------------------------------------------------------------
        # 1- Initialisation
        # ---------------------------------------------------------------------
        series = self._constructor_expanddim(datatype='fcst', name='sim2fcst')
        leadtimes = list(range(max_ltime+1))
        if error_depth == 0:
            error_ratio = [0]*len(leadtimes)
            error_scen = '000'
        else:
            error_ratio = [1. - float(t)/float(error_depth)
                           if t <= error_depth else 0
                           for t in leadtimes]
            error_scen = '{0:03d}'.format(error_depth)
        runtimes = [i.to_pydatetime()
                    for i in self.data_frame.index
                    if i >= first_dtime
                    if i <= last_dtime
                    if i in ref.data_frame.index]
        # ---------------------------------------------------------------------
        # 2- Récupération du modèle
        # ---------------------------------------------------------------------
        info = str2tuple(
            tuple2str((self.code, self.spc_varname)), forcesim=True)
        model = info[-1]
        if model is None:
            model = 'model'
        # ---------------------------------------------------------------------
        # 3- Création des prévisions
        # ---------------------------------------------------------------------
        for r in runtimes:
            serie = _build_sim2fcst(self, ref, r, error_ratio, leadtimes)
            if serie is None:
                continue
            keyt = (info[0], info[1], (r, model, error_scen, None))
            keys = tuple2str(keyt)
            code = '_'.join(keys.split('_')[:-1])
            serie = self.__class__(
                serie,
                code=code,
                varname=self.spc_varname,
                warning=self.warning
            )
            series.add(code=info[0], serie=serie, meta=keyt[2])
        return series

    def socose(self):
        """
        Déterminer les paramètrs D et RXD du modèle SOCOSE.
        - D : durée au-dessus du débit standardisé valant 0.5 (Q/Qp)
        - RXD : rapport du débit de pointe sur le débit moyen sur D

        Parameters
        ----------

        Returns
        -------
        socose : dict
            Valeurs SOCOSE par colonne du tableau DataFrame
            - col: {'d': d, 'rxd': rxd}

        See Also
        --------
        pyspc.model.socose.socose

        Examples
        --------
        >>> serie
        *************************************
        *********** SERIE *******************
        *************************************
        *  NOM VARIABLE SPC     = QH
        *  INTITULE VARIABLE    = Débit horaire
        *  IDENTIFIANT          = K0000000
        *  FOURNISSEUR          = Provider(name='SPC')
        *  NOM VARIABLE         = QH
        *  UNITE                = m3/s
        *  VALEUR MANQUANTE     =
        *  SERIE CONTINUE       = True
        *  PAS DE TEMPS         = 1:00:00
        *  UNITE DE TEMPS       = hour
        *  FUSEAU HORAIRE       = UTC
        *  PROFONDEUR SERIE     = 25
        *  PREMIER PAS DE TEMPS = 2024-03-30 06:00:00
        *  DERNIER PAS DE TEMPS = 2024-03-31 06:00:00
        *************************************

        >>> serie.df
                             (K0000000, QH)
        2024-03-30 06:00:00            23.0
        2024-03-30 07:00:00            25.5
        2024-03-30 08:00:00            30.6
        2024-03-30 09:00:00            53.6
        2024-03-30 10:00:00            92.5
        2024-03-30 11:00:00           119.4
        2024-03-30 12:00:00           132.4
        2024-03-30 13:00:00           137.1
        2024-03-30 14:00:00           134.9
        2024-03-30 15:00:00           129.6
        2024-03-30 16:00:00           121.2
        2024-03-30 17:00:00           111.7
        2024-03-30 18:00:00           102.1
        2024-03-30 19:00:00            93.7
        2024-03-30 20:00:00            84.6
        2024-03-30 21:00:00            76.6
        2024-03-30 22:00:00            69.5
        2024-03-30 23:00:00            63.9
        2024-03-31 00:00:00            59.1
        2024-03-31 01:00:00            55.0
        2024-03-31 02:00:00            51.0
        2024-03-31 03:00:00            48.1
        2024-03-31 04:00:00            45.8
        2024-03-31 05:00:00            43.5
        2024-03-31 06:00:00            41.9

        >>> values = serie.socose()
        >>> values
        {('K0000000', 'QH'): {'d': 13, 'rxd': 1.2682701202590196}}

        """
        return _socose.socose(self.standardize())
