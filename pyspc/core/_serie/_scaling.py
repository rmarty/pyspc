#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Serie - Méthodes spécifiques - Echelle temporelle
"""
from datetime import datetime as dt, timedelta as td
import numpy as np
import pandas as pnd


from pyspc.core.parameter import Parameter
import pyspc.core.exception as _exception


def _build_upscale_groups(index=None, timedelta=None, dayhour=6):
    """
    Définir les groupes des valeurs à agréger

    Parameters
    ----------
    index : list, DatetimeIndex
        Liste des dates
    timedelta : timedelta
        Pas de temps
    dayhour : int
        Heure définissant la limite de la journée

    Returns
    -------
    groups : list

    """
    _exception.check_td(timedelta)
    _exception.check_int(dayhour)
    if timedelta == td(days=1):
        return [dt(i.year, i.month, i.day) if i.hour > dayhour
                else dt(i.year, i.month, i.day) - td(days=1) for i in index]
    if timedelta == td(hours=3):
        return [i + td(hours=i.hour % 3 if i.hour % 3 == 0 else 3 - i.hour % 3)
                for i in index]
    if timedelta == td(hours=1):
        return [i + td(minutes=i.minute % 60 if i.minute % 60 == 0
                else 60 - i.minute % 60) for i in index]
    if timedelta in [td(days=31), td(days=30)]:
        return [dt(i.year, i.month, 1) for i in index]
    raise ValueError('Pas de temps final incorrect')


class Scaling():
    """
    Classe spécifique pour l'échelle temporelle de pyspc.core.serie.Serie
    """
    def __init__(self):
        """
        Echelle temporelle
        """
        self.data_frame = None
        self.code = None
        self.parameter = None
        self.spc_varname = None
        self.timestep = None
        self.warning = None

    def downscale(self, toparam=None, dayhour=6):
        """
        Désagréger la série à un pas de temps inférieur

        Parameters
        ----------
        toparam : Parameter
            Paramètre correspond au pas de temps ciblé
        dayhour : int
            Heure du début de la journée. Nécessaire si la source est un pas de
            temps journalier. Par défaut: 6. Valeurs admises: [0, 6]

        Returns
        -------
        pyspc.core.serie.Serie
            Série des valeurs désagrégées

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        if isinstance(toparam, str):
            toparam = Parameter(varname=toparam)
        _exception.raise_valueerror(
            not isinstance(toparam, Parameter),
            'Le paramètre cible est incorrect'
        )
        _exception.raise_valueerror(
            self.timestep == td(days=1) and dayhour not in [0, 6],
            "La valeur 'dayhour' est incorrecte"
        )
        _exception.raise_valueerror(
            not self.parameter.isDownscalable(toparam),
            'Le paramètre {} ne peut être désagrégé pour devenir le paramètre '
            '{}'.format(self.parameter.spc_varname, toparam.spc_varname)
        )
        # ---------------------------------------------------------------------
        # 1- Création du pandas.DataFrame de la série désagrégée
        # ---------------------------------------------------------------------
        ntimestep = int(self.timestep / toparam.timestep)
        dates = []
        values = []
        for i, v in self.data_frame.iterrows():
            if self.timestep == td(days=1):
                i = i.replace(hour=dayhour)
            for k in range(ntimestep):
                k = ntimestep - k - 1
                dates.append(i - k * toparam.timestep)
                if self.parameter.cumulable:
                    values.append(v[(self.code, self.spc_varname)] / ntimestep)
                else:
                    values.append(v[(self.code, self.spc_varname)])

        dframe = pnd.DataFrame(values, index=dates)
        # ---------------------------------------------------------------------
        # 2- Renvoie d'une instance Serie
        # ---------------------------------------------------------------------
        return self.__class__(
            dframe,
            code=self.code,
            varname=toparam.spc_varname
        )

    def nearlyequalscale(self, toparam=None, strict=True):
        """
        Ré-échantilloner une série à un pas de temps proche

        Parameters
        ----------
        toparam : Parameter
            Paramètre correspond au pas de temps ciblé
        strict : bool
            Calcul strict, imposant np.nan dans la valeur agrégée si au moins
            un pas de temps initial vaut np.nan. Retire de la série les pas
            de temps où les données d'origine ne sont pas complètes
            Défaut: True

        Returns
        -------
        pyspc.core.serie.Serie
            Série des valeurs agrégées/moyennées

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        if isinstance(toparam, str):
            toparam = Parameter(varname=toparam)
        _exception.raise_valueerror(
            not isinstance(toparam, Parameter),
            'Le paramètre cible est incorrect'
        )
        _exception.raise_valueerror(
            not self.parameter.isNearlyequalscalable(toparam),
            'Le paramètre {} ne peut être ré-échantillonner pour devenir le '
            'paramètre {}'.format(self.parameter.spc_varname,
                                  toparam.spc_varname)
        )
        # ---------------------------------------------------------------------
        # 1- Désagrégation au pas de temps 'minute'
        # ---------------------------------------------------------------------
        tmptimestep = td(minutes=1)
        ntimestep = int(self.timestep / tmptimestep)
        dates = []
        values = []
        for i, v in self.data_frame.iterrows():
            for k in range(ntimestep):
                k = ntimestep - k - 1
                dates.append(i - k * tmptimestep)
                if self.parameter.cumulable:
                    values.append(v[(self.code, self.spc_varname)] / ntimestep)
                else:
                    values.append(v[(self.code, self.spc_varname)])
        dframe = pnd.DataFrame(values, index=dates)
        # ---------------------------------------------------------------------
        # 2- Agrégation au pas de temps final
        # ---------------------------------------------------------------------
        tmpmodulo = int(toparam.timestep / tmptimestep)
        groups = [i + td(minutes=i.minute % tmpmodulo
                         if i.minute % tmpmodulo == 0
                         else tmpmodulo - i.minute % tmpmodulo)
                  for i in dframe.index]
        if strict:
            dframe = dframe.replace(np.nan, np.inf)
        dframe.columns = [toparam.spc_varname]
        dframe = dframe.groupby(groups)
        if self.parameter.cumulable:
            dframe = dframe.sum()
        else:
            dframe = dframe.mean()
        dframe[dframe == np.inf] = np.nan
        if strict:
            upcounter = int(toparam.timestep / tmptimestep)
            firstgroup = groups.count(groups[0])
            lastgroup = groups.count(groups[-1])
            if firstgroup != upcounter:
                dframe = dframe[1:]
            if lastgroup != upcounter:
                dframe = dframe[:-1]
        # ---------------------------------------------------------------------
        # 3- Renvoyer la série agrégée
        # ---------------------------------------------------------------------
        return self.__class__(
            dframe,
            code=self.code,
            varname=toparam.spc_varname
        )

    def regularscale(self):
        """
        Interpoler une série d'un pas de temps irrégulier
        à un pas de temps régulier

        Returns
        -------
        pyspc.core.serie.Serie
            Série à pas de temps régulier

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        regpar = self.parameter.to_regularscale()
        # ---------------------------------------------------------------------
        # 1- Détermination de la période d'interpolation
        # ---------------------------------------------------------------------
        dt_min = min(self.data_frame.index).replace(minute=0)
        dt_max = max(self.data_frame.index).replace(minute=0) + regpar.timestep
        dates = [dt_min + x * regpar.timestep
                 for x in range(int((dt_max - dt_min) / regpar.timestep) + 1)]
        dates_s = [x - dt_min for x in dates]
        dates_s = [x.total_seconds() for x in dates_s]
        raw_s = [x - dt_min for x in self.data_frame.index]
        raw_s = [x.total_seconds() for x in raw_s]
        # ---------------------------------------------------------------------
        # 2- Interpolation à pas de temps régulier
        # ---------------------------------------------------------------------
        interp = np.interp(dates_s,
                           raw_s,
                           self.data_frame.values[:, 0],
                           left=np.nan,
                           right=np.nan)
        dframe = pnd.DataFrame(interp, index=dates)
        # ---------------------------------------------------------------------
        # 3- Renvoi de la nouvelle série
        # ---------------------------------------------------------------------
        return self.__class__(
            dframe,
            code=self.code,
            varname=regpar.spc_varname
        )

    def standardize(self):
        """
        Standardiser la chronique par normalisation selon la valeur max.

        L'index est un timedelta depuis la date du maxi.
        Cette chronique standardisée est utilisée par la méthode socose


        Returns
        -------
        df : pandas.DataFrame
            Tableau des données standardisées

        See Also
        --------
        pyspc.core.serie.Serie.socose

        Examples
        --------
        >>> serie
        *************************************
        *********** SERIE *******************
        *************************************
        *  NOM VARIABLE SPC     = QH
        *  INTITULE VARIABLE    = Débit horaire
        *  IDENTIFIANT          = K0000000
        *  FOURNISSEUR          = Provider(name='SPC')
        *  NOM VARIABLE         = QH
        *  UNITE                = m3/s
        *  VALEUR MANQUANTE     =
        *  SERIE CONTINUE       = True
        *  PAS DE TEMPS         = 1:00:00
        *  UNITE DE TEMPS       = hour
        *  FUSEAU HORAIRE       = UTC
        *  PROFONDEUR SERIE     = 25
        *  PREMIER PAS DE TEMPS = 2024-03-30 06:00:00
        *  DERNIER PAS DE TEMPS = 2024-03-31 06:00:00
        *************************************

        >>> serie.df
                             (K0000000, QH)
        2024-03-30 06:00:00            23.0
        2024-03-30 07:00:00            25.5
        2024-03-30 08:00:00            30.6
        2024-03-30 09:00:00            53.6
        2024-03-30 10:00:00            92.5
        2024-03-30 11:00:00           119.4
        2024-03-30 12:00:00           132.4
        2024-03-30 13:00:00           137.1
        2024-03-30 14:00:00           134.9
        2024-03-30 15:00:00           129.6
        2024-03-30 16:00:00           121.2
        2024-03-30 17:00:00           111.7
        2024-03-30 18:00:00           102.1
        2024-03-30 19:00:00            93.7
        2024-03-30 20:00:00            84.6
        2024-03-30 21:00:00            76.6
        2024-03-30 22:00:00            69.5
        2024-03-30 23:00:00            63.9
        2024-03-31 00:00:00            59.1
        2024-03-31 01:00:00            55.0
        2024-03-31 02:00:00            51.0
        2024-03-31 03:00:00            48.1
        2024-03-31 04:00:00            45.8
        2024-03-31 05:00:00            43.5
        2024-03-31 06:00:00            41.9

        >>> df = serie.standardize()
        >>> df
                           (K0000000, QH)
        -1 days +17:00:00        0.167761
        -1 days +18:00:00        0.185996
        -1 days +19:00:00        0.223195
        -1 days +20:00:00        0.390956
        -1 days +21:00:00        0.674690
        -1 days +22:00:00        0.870897
        -1 days +23:00:00        0.965718
        00:00:00                 1.000000
        01:00:00                 0.983953
        02:00:00                 0.945295
        03:00:00                 0.884026
        04:00:00                 0.814734
        05:00:00                 0.744712
        06:00:00                 0.683443
        07:00:00                 0.617068
        08:00:00                 0.558716
        09:00:00                 0.506929
        10:00:00                 0.466083
        11:00:00                 0.431072
        12:00:00                 0.401167
        13:00:00                 0.371991
        14:00:00                 0.350839
        15:00:00                 0.334063
        16:00:00                 0.317287
        17:00:00                 0.305616

        """
        valmax, dtmax = self.max()
        df = self.df / valmax
        df.index = [i - dtmax for i in df.index]
        return df

    def subhourlyscale(self, ts=None, how=None, howinterp=None):
        """
        Interpoler une série d'un pas de temps horaire
        à un pas de temps infra-horaire

        Parameters
        ----------
        ts : timedelta
            Pas de temps infra-horaire entre 1 min et 1h. Par défaut: 5 minutes
        how : str
            Méthode de ré-échantillonnage parmi:
            ['bfill', 'ffill', 'nearest', 'interpolate']
        howinterp : str
            Méthode d'interpolation. Par défaut: 'linear'

        Returns
        -------
        pyspc.core.serie.Serie
            Série à pas de temps infra-horaire

        .. seealso::
            `pandas.DataFrame.resample <https://pandas.pydata.org/pandas-docs
            /stable/reference/api/pandas.DataFrame.resample.html>`_.

        """
#            + limit = (str) Nombre maximal de valeurs successives à remplir
#            + limit_direction = (str) Sens de l'interpolation / extrapolation
#            + limit_area = (str) Portée de l'interpolation / extrapolation
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        shpar = self.parameter.to_subhourlyscale()
        if ts is None:
            ts = td(seconds=60*5)
        _exception.raise_valueerror(
            not isinstance(ts, td),
            "Le pas de temps n'est pas un timedelta",
            self.warning
        )
        _exception.raise_valueerror(
            ts < td(seconds=60) or ts > td(hours=1),
            "Le pas de temps est incorrect",
            self.warning
        )
        if how is None:
            how = 'interpolate'
        _exception.raise_valueerror(
            how not in ['bfill', 'ffill', 'nearest', 'interpolate'],
            "Méthode de ré-échantillonnage incorrecte",
            self.warning
        )
        if howinterp is None:
            howinterp = 'index'  # 'linear'
        # ---------------------------------------------------------------------
        # 1- Ré-échantillonnage
        # ---------------------------------------------------------------------
        df = self.data_frame
        if how == 'bfill':
            df = df.resample(ts).bfill()
        elif how == 'ffill':
            df = df.resample(ts).ffill()
        elif how == 'nearest':
            df = df.resample(ts).nearest()
        elif how == 'interpolate':
            # Eviter d'interpoler les nan d'origine
            df = df.replace(np.nan, np.inf)
            # Interpoler au pas de temps infra-horaire
            df = df.resample(ts).interpolate(
                method=howinterp, limit_area='inside')
            # Remetre les nan d'origine
            df = df.replace(np.inf, np.nan)
        # ---------------------------------------------------------------------
        # 2- Renvoi de la nouvelle série
        # ---------------------------------------------------------------------
        return self.__class__(
            df,
            code=self.code,
            varname=shpar.spc_varname
        )

    def upscale(self, toparam=None, dayhour=6, strict=True):
        """
        Agréger/moyenner la série à un pas de temps supérieur

        Parameters
        ----------
        toparam : Parameter
            Paramètre correspond au pas de temps ciblé
        dayhour : int
            Heure du début de la journée Nécessaire si la cible est un pas de
            temps journalier. Par défaut: 6. Valeurs admises: [0, 6]
        strict : bool
            Calcul strict, imposant np.nan dans la valeur agrégée si au moins
            un pas de temps initial vaut np.nan. Retire de la série les pas
            de temps où les données d'origine ne sont pas complètes
            Défaut: True

        Returns
        -------
        pyspc.core.serie.Serie
            Série des valeurs agrégées/moyennées

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        if isinstance(toparam, str):
            toparam = Parameter(varname=toparam)
        _exception.raise_valueerror(
            not isinstance(toparam, Parameter),
            'Le paramètre cible est incorrect'
        )
        _exception.raise_valueerror(
            self.timestep == td(days=1) and dayhour not in [0, 6],
            "La valeur 'dayhour' est incorrecte"
        )
        _exception.raise_valueerror(
            not isinstance(strict, bool),
            "Le paramètre 'strict' est incorrect"
        )
        _exception.raise_valueerror(
            not self.parameter.isUpscalable(toparam),
            'Le paramètre {} ne peut être agrégé pour devenir le paramètre '
            '{}'.format(self.parameter.spc_varname, toparam.spc_varname)
        )
        # ---------------------------------------------------------------------
        # 1- Détermination des regroupements
        # ---------------------------------------------------------------------
        groups = _build_upscale_groups(
            index=self.data_frame.index,
            timedelta=toparam.timestep,
            dayhour=dayhour
        )
        # ---------------------------------------------------------------------
        # 2- Définition de la nouvelle série
        # ---------------------------------------------------------------------
        if strict:
            dframe = self.data_frame.replace(np.nan, np.inf)
        else:
            dframe = self.data_frame
        dframe.columns = [toparam.spc_varname]
        dframe = dframe.groupby(groups)
        if self.parameter.cumulable:
            dframe = dframe.sum()
        else:
            dframe = dframe.mean()
        # ---------------------------------------------------------------------
        # 3- Remplacement des valeurs infinies par np.nan
        # ---------------------------------------------------------------------
        dframe[dframe == np.inf] = np.nan
        # ---------------------------------------------------------------------
        # 4- Censurer les pas de temps incomplets (si strict)
        # ---------------------------------------------------------------------
        if strict:
            upcounter = int(toparam.timestep / self.timestep)
            firstgroup = groups.count(groups[0])
            lastgroup = groups.count(groups[-1])
            if firstgroup != upcounter:
                dframe = dframe[1:]
            if lastgroup != upcounter:
                dframe = dframe[:-1]
        # ---------------------------------------------------------------------
        # 5- Renvoyer la série agrégée
        # ---------------------------------------------------------------------
        return self.__class__(
            dframe,
            code=self.code,
            varname=toparam.spc_varname
        )
