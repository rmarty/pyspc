#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Serie - Méthodes spécifiques - Traitement temporel
"""
from datetime import timedelta as td
import pytz

import pyspc.core.exception as _exception


class TimeSerie():
    """
    Classe spécifique pour le Traitement temporel de pyspc.core.serie.Serie
    """
    def __init__(self):
        """
        Traitement temporel
        """
        self.data_frame = None
        self._timezone = None
        self._firstdt = None
        self._lastdt = None

    def fill_constant(self, constant=None, inplace=False):
        """
        Remplacer les valeurs manquantes self.missing
        par une valeur constante constant
        Si l'option inplace est définie à False,
        alors cette méthode crée une nouvelle instance Serie

        Parameters
        ----------
        constant : int, float
            Valeur constante
        inplace : bool
            Traiter en place (True) ou renvoyer une nouvelle instance (False)
            défaut: False

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données, si inplace=False

        """
        _exception.check_numeric(constant)
        if not inplace:
            return self.__class__(
                self.data_frame.fillna(constant),
                code="{0}_fct{1}".format(self.code, constant),
                varname=self.spc_varname,
                missing=self.missing)
        self.data_frame.fillna(constant, inplace=True)
        return None

    def fill_linear_interpolation(self, inplace=False):
        """
        Remplacer les valeurs manquantes self.missing
        par une interpolation linéaire
        Si l'option inplace est définie à False,
        alors cette méthode crée une nouvelle instance Serie

        Parameters
        ----------
        inplace : bool
            Traiter en place (True) ou renvoyer une nouvelle instance (False)
            défaut: False

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données, si inplace=False

        """
        if not inplace:
            return self.__class__(
                self.data_frame.interpolate(method='index',
                                            limit_area='inside',
                                            inplace=inplace),
                code="{0}_fli".format(self.code),
                varname=self.spc_varname,
                missing=self.missing
            )
        self.data_frame.interpolate(
            method='index',
            limit_area='inside',
            inplace=inplace
        )
        return None

    def first_valid_index(self):
        """
        Renvoyer la date de la première valeur valide (i.e. != np.nan)

        Returns
        -------
        timestamp, datetime
            Date de la première valeur valide

        """
        return self.data_frame.first_valid_index()

    def last_valid_index(self):
        """
        Renvoyer la date de la dernière valeur valide (i.e. != np.nan)

        Returns
        -------
        timestamp, datetime
            Date de la dernière valeur valide

        """
        return self.data_frame.last_valid_index()

    def set_timezone(self, timezone=pytz.utc):
        """
        Appliquer un nouveau fuseau horaire au contenu de l'instance
        de la classe Serie

        Parameters
        ----------
        timezone : pytz
            Date de la dernière valeur valide

        """
        self.data_frame.index = \
            self.data_frame.index.tz_localize(self.timezone)
        self.data_frame.index = self.data_frame.index.tz_convert(timezone)
        self._timezone = timezone
        self._firstdt = self.data_frame.index[0].to_pydatetime()
        self._lastdt = self.data_frame.index[-1].to_pydatetime()

    def shift(self, periods=None, freq=None, inplace=True):
        """
        Appliquer la méthode de pnd.DataFrame.shift à l'instance Serie

        Parameters
        ----------
        periods : timedelta
            Période
        freq : timedelta
            Fréquence
        inplace : bool
            Traiter en place (True) ou renvoyer une nouvelle instance (False)
            défaut: True

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données, si inplace=False

        .. seealso:: pandas.DataFrame.index.shift

        """
        code = "{0}_shifted".format(self.code)
        if inplace:
            self.data_frame.index.shift(periods=periods, freq=freq)
            self._firstdt = self.data_frame.index[0].to_pydatetime()
            self._lastdt = self.data_frame.index[-1].to_pydatetime()
        else:
            df = self.data_frame.shift(periods=periods, freq=freq)
            return self.__class__(
                df,
                code=code,
                varname=self.spc_varname,
                missing=self.missing)
        return None

    def timelag(self, tlag=None, inplace=True):
        """
        Décaler la série de données de l'instance Serie :
        - tlag                  Si tlag est une instance de td,
        - tlag*self.timestep    Sinon
        Si l'option inplace est définie à False,
        alors cette méthode crée une nouvelle instance Serie

        Parameters
        ----------
        tlag : int, float, timedelta
            Période
        inplace : bool
            Traiter en place (True) ou renvoyer une nouvelle instance (False)
            défaut: True

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données, si inplace=False

        """
        _exception.raise_valueerror(
            not isinstance(tlag, (int, float, td)),
            "Argument tlag n'est ni un timedelta, "
            "ni un entier, ni un flottant "
        )
        if isinstance(tlag, td):
            freq = tlag
            code = "{0}_tlag{1:d}".format(self.code, int(tlag/self.timestep))
        elif self.spc_varname.endswith('I'):
            freq = tlag * td(seconds=60)
            code = "{0}_tlag{1:d}".format(self.code, int(tlag))
            if self.warning:
                _exception.Warning(
                    __name__,
                    "Le décalage demandé des données instantanées est "
                    "supposé être défini en minutes")
        else:
            freq = tlag * self.timestep
            code = "{0}_tlag{1:d}".format(self.code, int(tlag))
        if inplace:
            self.data_frame.index += freq
            self._firstdt = self.data_frame.index[0].to_pydatetime()
            self._lastdt = self.data_frame.index[-1].to_pydatetime()
        else:
            df = self.data_frame.copy(deep=True)
            df.index += freq
            return self.__class__(
                df,
                code=code,
                varname=self.varname,
                missing=self.missing)
        return None
