#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Serie - Méthodes spécifiques - Opérateurs binaires
"""


class BinaryOperator():
    """
    Classe spécifique pour les opérateurs binaires de pyspc.core.serie.Serie
    """
    def __init__(self):
        """
        Opérateurs binaires
        """
        self._code = None
        self._data_frame = None
        self._missing = None
        self._spc_varname = None

    def __add__(self, other):
        """
        Additionner l'instance Serie avec scalaire
        ou une autre instance Serie

        Parameters
        ----------
        other : int, float, pyspc.core.serie.Serie
            Objet à ajouter

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        if isinstance(other, (int, float)):
            return self.__class__(
                self.data_frame + other,
                code="({0})a({1})".format(self.code, other),
                varname=self.spc_varname,
                missing=self.missing)
        if isinstance(other, type(self)) and \
                self.spc_varname == other.spc_varname:
            return self.__class__(
                self.data_frame + other.data_frame,
                code="({0})a({1})".format(self.code, other.code),
                varname=self.spc_varname,
                missing=self.missing)
        if self.warning:
            raise ValueError(
                "Pas d'addition possible. "
                "Aucune instance Serie créée.")
        return None

    def __radd__(self, other):
        """
        Additionner un scalaire avec une instance Serie

        Parameters
        ----------
        other : int, float, pyspc.core.serie.Serie
            Objet à ajouter

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        return self.__add__(other)

    def __sub__(self, other):
        """
        Soustraire l'instance Serie avec scalaire
        ou une autre instance Serie

        Parameters
        ----------
        other : int, float, pyspc.core.serie.Serie
            Objet à soustraire

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        if isinstance(other, (int, float)):
            return self.__class__(
                self.data_frame - other,
                code="({0})s({1})".format(self.code, other),
                varname=self.spc_varname,
                missing=self.missing)
        if isinstance(other, type(self)) and \
                self.spc_varname == other.spc_varname:
            return self.__class__(
                self.data_frame - other.data_frame,
                code="({0})s({1})".format(self.code, other.code),
                varname=self.spc_varname,
                missing=self.missing)
        if self.warning:
            raise ValueError(
                "Pas de soustraction possible. "
                "Aucune instance Serie créée.")
        return None

    def __rsub__(self, other):
        """
        Soustraire un scalaire avec une instance Serie

        Parameters
        ----------
        other : int, float, pyspc.core.serie.Serie
            Objet à soustraire

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        s = other + -1 * self
        s.code = "({0})s({1})".format(other, self.code)
        s.data_frame.columns = [(s.code, self.spc_varname)]
        return s

    def __mul__(self, other):
        """
        Multiplier l'instance Serie avec scalaire
        ou une autre instance Serie

        Parameters
        ----------
        other : int, float, pyspc.core.serie.Serie
            Objet à multiplier

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        if isinstance(other, (int, float)):
            return self.__class__(
                self.data_frame * other,
                code="({0})m({1})".format(self.code, other),
                varname=self.spc_varname,
                missing=self.missing)
        if isinstance(other, type(self)) and \
                self.spc_varname == other.spc_varname:
            return self.__class__(
                self.data_frame * other.data_frame,
                code="({0})m({1})".format(self.code, other.code),
                varname=self.spc_varname,
                missing=self.missing)
        if self.warning:
            raise ValueError(
                "Pas de multiplication possible. "
                "Aucune instance Serie créée.")
        return None

    def __rmul__(self, other):
        """
        Multiplier un scalaire avec une instance Serie

        Parameters
        ----------
        other : int, float, pyspc.core.serie.Serie
            Objet à multiplier

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        return self.__mul__(other)

    def __truediv__(self, other):
        """
        Diviser l'instance Serie avec scalaire
        ou une autre instance Serie
        Division Python3
        Renvoie une nouvelle instance <Serie

        Parameters
        ----------
        other : int, float, pyspc.core.serie.Serie
            Objet à diviser

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        if isinstance(other, (int, float)):
            return self.__class__(
                self.data_frame / other,
                code="({0})d({1})".format(self.code, other),
                varname=self.spc_varname,
                missing=self.missing)
        if isinstance(other, type(self)) and \
                self.spc_varname == other.spc_varname:
            return self.__class__(
                self.data_frame / other.data_frame,
                code="({0})d({1})".format(self.code, other.code),
                varname=self.spc_varname,
                missing=self.missing)
        if self.warning:
            raise ValueError(
                "Pas de division possible. "
                "Aucune instance Serie créée.")
        return None

    def __rtruediv__(self, other):
        """
        Diviser un scalaire avec une instance Serie

        Parameters
        ----------
        other : int, float, pyspc.core.serie.Serie
            Objet à diviser

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        s = other * self.__pow__(-1)
        s.code = "({0})d({1})".format(other, self.code)
        s.data_frame.columns = [(s.code, self.spc_varname)]
        return s

    def __pow__(self, other):
        """
        Mettre l'instance Serie à la puissance.
        La puissance peut être un scalire ou une autre instance Serie

        Parameters
        ----------
        other : int, float, pyspc.core.serie.Serie
            Puissance à appliquer

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        if isinstance(other, (int, float)):
            return self.__class__(
                self.data_frame ** other,
                code="({0})p({1})".format(self.code, other),
                varname=self.spc_varname,
                missing=self.missing)
        if isinstance(other, type(self)) and \
                self.spc_varname == other.spc_varname:
            return self.__class__(
                self.data_frame ** other.data_frame,
                code="({0})p({1})".format(self.code, other.code),
                varname=self.spc_varname,
                missing=self.missing)
        if self.warning:
            raise ValueError(
                "Pas de puissance possible. "
                "Aucune instance Serie créée.")
        return None

    def __abs__(self):
        """
        Serie mise en valeurs absolues

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        return self.__class__(
            self.data_frame.abs(),
            code="abs({0})".format(self.code),
            varname=self.spc_varname,
            missing=self.missing)

    def __mod__(self, other):
        """
        Calculer l'instance Serie modulo other.
        other peut être un scalaire ou une autre instance Serie

        Parameters
        ----------
        other : int, float, pyspc.core.serie.Serie
            Modulo à appliquer

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        if isinstance(other, (int, float)):
            return self.__class__(
                self.data_frame % other,
                code="({0})mod({1})".format(self.code, other),
                varname=self.spc_varname,
                missing=self.missing)
        if isinstance(other, type(self)) and \
                self.spc_varname == other.spc_varname:
            return self.__class__(
                self.data_frame % other.data_frame,
                code="({0})mod({1})".format(self.code, other.code),
                varname=self.spc_varname,
                missing=self.missing)
        if self.warning:
            raise ValueError(
                "Pas de modulo possible. "
                "Aucune instance Serie créée.")
        return None

    def __lt__(self, other):
        """
        Serie inférieure strictement à other.
        other peut être un scalaire ou une autre instance Serie

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        if isinstance(other, (int, float)):
            return self.__class__(
                self.data_frame < other,
                code="({0})lt({1})".format(self.code, other),
                varname=self.spc_varname,
                missing=self.missing)
        if isinstance(other, type(self)) and \
                self.spc_varname == other.spc_varname:
            return self.__class__(
                self.data_frame < other.data_frame,
                code="({0})lt({1})".format(self.code, other.code),
                varname=self.spc_varname,
                missing=self.missing)
        if self.warning:
            raise ValueError(
                "Pas d'opération < possible. "
                "Aucune instance Serie créée.")
        return None

    def __le__(self, other):
        """
        Serie inférieure ou égale à other.
        other peut être un scalaire ou une autre instance Serie

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        if isinstance(other, (int, float)):
            return self.__class__(
                self.data_frame <= other,
                code="({0})le({1})".format(self.code, other),
                varname=self.spc_varname,
                missing=self.missing)
        if isinstance(other, type(self)) and \
                self.spc_varname == other.spc_varname:
            return self.__class__(
                self.data_frame <= other.data_frame,
                code="({0})le({1})".format(self.code, other.code),
                varname=self.spc_varname,
                missing=self.missing)
        if self.warning:
            raise ValueError(
                "Pas d'opération <= possible. "
                "Aucune instance Serie créée.")
        return None

    def __eq__(self, other):
        """
        Serie égal strictement à other.
        other peut être un scalaire ou une autre instance Serie

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        if isinstance(other, (int, float)):
            return self.__class__(
                self.data_frame == other,
                code="({0})eq({1})".format(self.code, other),
                varname=self.spc_varname,
                missing=self.missing)
        if isinstance(other, type(self)) and \
                self.spc_varname == other.spc_varname:
            return self.__class__(
                self.data_frame == other.data_frame,
                code="({0})eq({1})".format(self.code, other.code),
                varname=self.spc_varname,
                missing=self.missing)
        if self.warning:
            raise ValueError(
                "Pas d'opération == possible. "
                "Aucune instance Serie créée.")
        return None

    def __ne__(self, other):
        """
        Serie non égal strictement à other.
        other peut être un scalaire ou une autre instance Serie

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        if isinstance(other, (int, float)):
            return self.__class__(
                self.data_frame != other,
                code="({0})ne({1})".format(self.code, other),
                varname=self.spc_varname,
                missing=self.missing)
        if isinstance(other, type(self)) and \
                self.spc_varname == other.spc_varname:
            return self.__class__(
                self.data_frame != other.data_frame,
                code="({0})ne({1})".format(self.code, other.code),
                varname=self.spc_varname,
                missing=self.missing)
        if self.warning:
            raise ValueError(
                "Pas d'opération != possible. "
                "Aucune instance Serie créée.")
        return None

    def __ge__(self, other):
        """
        Serie supérieure ou égale à other.
        other peut être un scalaire ou une autre instance Serie

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        if isinstance(other, (int, float)):
            return self.__class__(
                self.data_frame >= other,
                code="({0})ge({1})".format(self.code, other),
                varname=self.spc_varname,
                missing=self.missing)
        if isinstance(other, type(self)) and \
                self.spc_varname == other.spc_varname:
            return self.__class__(
                self.data_frame >= other.data_frame,
                code="({0})ge({1})".format(self.code, other.code),
                varname=self.spc_varname,
                missing=self.missing)
        if self.warning:
            raise ValueError(
                "Pas d'opération >= possible. "
                "Aucune instance Serie créée.")
        return None

    def __gt__(self, other):
        """
        Serie supérieure strictement à other.
        other peut être un scalaire ou une autre instance Serie

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        if isinstance(other, (int, float)):
            return self.__class__(
                self.data_frame > other,
                code="({0})gt({1})".format(self.code, other),
                varname=self.spc_varname,
                missing=self.missing)
        if isinstance(other, type(self)) and \
                self.spc_varname == other.spc_varname:
            return self.__class__(
                self.data_frame > other.data_frame,
                code="({0})gt({1})".format(self.code, other.code),
                varname=self.spc_varname,
                missing=self.missing)
        if self.warning:
            raise ValueError(
                "Pas d'opération > possible. "
                "Aucune instance Serie créée.")
        return None
