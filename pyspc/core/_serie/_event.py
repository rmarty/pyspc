#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Serie - Méthodes spécifiques - Evénements
"""
import collections
from datetime import timedelta as td
import numpy as np
import pandas as pnd

import pyspc.core.exception as _exception


class Event():
    """
    Classe spécifique pour la détermination d'événements
    de pyspc.core.serie.Serie
    """
    def __init__(self):
        """
        Evénements
        """
        self.code = None
        self.dtfmt = None
        self.missing = None
        self.parameter = None
        self.spc_varname = None

    def events(self, threshold=None, engine='basic',
               prominence=None, width=None,
               before=None, after=None, filename=None):
        """
        Sélectionner les événements définis par un seuil

        Parameters
        ----------
        engine : str
            Algorithme à utiliser

            - 'basic' : événement défini par un seuil uniquement. Pas de
              prise en compte explicite des montées/décrues.
              Valeur par défaut pour une raison de compatibilité
              avec les versions antérieures

            - 'scipy' : application de scipy.signal.find_peaks

        threshold : float
            Valeur minimale d'une pointe

        Other Parameters
        ----------------
        prominence : float
            Ecart d'amplitude minimal entre deux pointes
        width : float
            Ecart temporel minimal entre deux pointes. En pas de temps,
            i.e. indice dans la liste des valeurs de la série
        before : timedelta
            Durée maximale avant la pointe
        after : timedelta
            Durée maximale après la pointe
        filename : str
            Nom du fichier image. Si défini, imprime la série
            et ses événements dans une image

        Returns
        -------
        events : dict
            Dictionnaire des événements
                - clé : datetime (jour du maxi)

            valeur : dict
                Informations sur l'événement

                - 'start' : date de début
                - 'end'   : date de fin
                - 'dtmax' : date de la valeur maximale
                - 'max'   : valeur maximale
                - 'name'  : nom de l'événement (jour du max en str)
                - None sinon

        """
        if engine == 'basic':
            return self.events_basic(threshold=threshold, filename=filename)
        if engine == 'scipy':
            return self.events_scipy(
                threshold=threshold,
                prominence=prominence,
                width=width,
                before=before,
                after=after,
                filename=filename
            )
        raise ValueError('Algorithme {} incorrecte'.format(engine))

    def events_basic(self, threshold=None, filename=None):
        """
        Sélectionner les événements définis par un seuil - Version Basic

        Parameters
        ----------
        threshold : float
            Valeur minimale d'une pointe

        Other Parameters
        ----------------
        filename : str
            Nom du fichier image. Si défini, imprime la série
            et ses événements dans une image

        Returns
        -------
        events : dict
            Dictionnaire des événements
                - clé : datetime (jour du maxi)
            valeur : dict
                Informations sur l'événement
                - 'start' : date de début
                - 'end'   : date de fin
                - 'dtmax' : date de la valeur maximale
                - 'max'   : valeur maximale
                - 'name'  : nom de l'événement (jour du max en str)
                - None sinon

        """
        # ---------------------------------------------------------------------
        # 0- Contrôle
        # ---------------------------------------------------------------------
        _exception.check_numeric(threshold)
        # ---------------------------------------------------------------------
        # 1- Initialisation
        # ---------------------------------------------------------------------
        events = collections.OrderedDict()
        # ---------------------------------------------------------------------
        # 2- Calcul
        # ---------------------------------------------------------------------
        dataset = self.above_threshold(threshold=threshold, inplace=False)
        dataset.code = self.code
        dataset.data_frame.columns = [(dataset.code, dataset.spc_varname)]
        # ---------------------------------------------------------------------
        # 3- Archivage dans un dictionnaire
        # source :
        #   https://stackoverflow.com/questions/60455507/
        #   split-dataframe-into-multiple-dataframe-based-on-nan-value
        # evolution possible : qd passage à pandas > 0.25.0
        #   https://stackoverflow.com/questions/21402384/
        #   how-to-split-a-pandas-time-series-by-nan-values
        # ---------------------------------------------------------------------
        df = dataset.data_frame
        # create groups by comparing to null
        df['group'] = df.isnull().all(axis=1).cumsum()
        # Use dictionary comprehension together with loc
        # to select the relevant group
        d = {i: df.loc[df.group == i, [(dataset.code, dataset.spc_varname)]]
             for i in range(1, df.group.iat[-1])}
        # Nettoyage des groupes de valeurs
        d = {i: d[i] for i in d if len(d[i].dropna()) > 0}
        # Définition des événements
        for e in d.values():
            serie = self.__class__(
                e, code=self.code, varname=self.spc_varname,
                missing=self.missing)
            serie = serie.stripna()
            (max_value, max_dt) = serie.max()
            key = max_dt.strftime(self.dtfmt)
            events.setdefault(
                key,
                {
                    'name': key,
                    'max': max_value,
                    'maxdt': max_dt,
                    'massdt': serie.timecentroid(),
                    'firstdt': serie.firstdt,
                    'lastdt': serie.lastdt,
                    'length': serie.length,
                    'serie': serie
                }
            )
        # ---------------------------------------------------------------------
        # 4- Création de la figure
        # ---------------------------------------------------------------------
        if filename is not None:
            plot_events(serie=self, events=events, filename=filename)
        # ---------------------------------------------------------------------
        # 5- Retour
        # ---------------------------------------------------------------------
        return events

    def events_scipy(self, threshold=None, prominence=None, width=None,
                     before=None, after=None, filename=None):
        """
        Sélectionner les événements définis par un seuil - Version scipy

        Parameters
        ----------
        threshold : float
            Valeur minimale d'une pointe
        prominence : float
            Ecart d'amplitude minimal entre deux pointes
        width : float
            Ecart temporel minimal entre deux pointes. En pas de temps,
            i.e. indice dans la liste des valeurs de la série

        Other Parameters
        ----------------
        before : timedelta
            Durée maximale avant la pointe
        after : timedelta
            Durée maximale après la pointe
        filename : str
            Nom du fichier image. Si défini, imprime la série
            et ses événements dans une image

        Returns
        -------
        events : dict
            Dictionnaire des événements
                - clé : datetime (jour du maxi)
            valeur : dict
                Informations sur l'événement
                - 'start' : date de début
                - 'end'   : date de fin
                - 'dtmax' : date de la valeur maximale
                - 'max'   : valeur maximale
                - 'name'  : nom de l'événement (jour du max en str)
                - None sinon

        .. warning:: Cette méthode nécessite l'import de la
        bibliothèque tierce scipy.signal

        """
        # ---------------------------------------------------------------------
        # 1- Application alogirthme scipy
        # ---------------------------------------------------------------------
        values = self.data_frame.values.flatten()
        values[np.isnan(values)] = self.parameter.missing[1]
        indices, tags = apply_findpeaks(
            values=values,
            threshold=threshold,
            prominence=prominence,
            width=width,
        )
        # ---------------------------------------------------------------------
        # 2- Archivage dans un dictionnaire
        # ---------------------------------------------------------------------
        events = archive_findpeaks(
            self, values, indices, tags, before=before, after=after)
        # ---------------------------------------------------------------------
        # 3- Création de la figure
        # ---------------------------------------------------------------------
        if filename is not None:
            plot_events(serie=self, events=events, filename=filename)
        # ---------------------------------------------------------------------
        # 4- Retour
        # ---------------------------------------------------------------------
        return events


def apply_findpeaks(values=None, threshold=None, prominence=None, width=None):
    """
    Appliquer l'algorithme find_peaks de scipy

    Parameters
    ----------
    values : list
        Liste des valeurs dans l'ordre chronologique
    threshold : float
        Valeur minimale d'une pointe
    prominence : float
        Ecart d'amplitude minimal entre deux pointes
    width : float
        Ecart temporel minimal entre deux pointes. En pas de temps, i.e.
        indice dans la liste des valeurs de la série

    Returns
    -------
    indices : list
        Liste des indices temporels des pointes.
        Ces pointes sont triées par ordre croissant
    tags : list
        Liste des étiquettes des événements:
        - 0 : instant dans aucun événement
        - i>0 : instant dans l'événement n°i en phase de montée
        - i<0 : instant dans l'événement n°i en phase de descente

    .. warning:: Cette méthode nécessite l'import de la
                 bibliothèque tierce scipy.signal

    """
    # imports spécifiques
    from scipy.signal import find_peaks  # peak_prominences, peak_widths
    # initialisation
    df = pnd.DataFrame({'tags': [np.nan]*len(values)})
    # définir les pointes
    indices, properties = find_peaks(
        values, height=threshold, threshold=(None, None),
        prominence=(prominence, None), width=(width, None), rel_height=1)
    # tri par valeurs croissantes de pointes
    props = list(zip(properties['peak_heights'], indices,
                     properties['left_bases'], properties['right_bases']))
    props.sort()
    indices = [p[1] for p in props]
    # définir les catégories des valeurs
    for kp, p in enumerate(props):
        up = pnd.DataFrame({'tags': kp+1}, index=range(p[2], p[1]+1))
        df.update(up, overwrite=False)
        dn = pnd.DataFrame({'tags': -1 * (kp+1)}, index=range(p[1]+1, p[3]+1))
        df.update(dn, overwrite=False)
    df = df.fillna(0).astype('int64')
    return indices, df['tags'].tolist()


def archive_findpeaks(serie, values, indices, tags, before=None, after=None):
    """
    Archiver les informations des événements dans un dictionnaire

    Parameters
    ----------
    serie : pyspc.core.serie.Serie
        Série de données d'origine
    values : list
        Liste des valeurs dans l'ordre chronologique
    indices : list
        Liste des indices temporels des pointes.
        Ces pointes sont triées par ordre croissant
    tags : list
        Liste des étiquettes des événements:
        - 0 : instant dans aucun événement
        - i>0 : instant dans l'événement n°i en phase de montée
        - i<0 : instant dans l'événement n°i en phase de descente

    Other Parameters
    ----------------
    before : timedelta
        Durée maximale avant la pointe
    after : timedelta
        Durée maximale après la pointe

    Returns
    -------
    events : dict
        Dictionnaire des événements
            - clé : datetime (jour du maxi)
        valeur : dict
            Informations sur l'événement
            - 'start' : date de début
            - 'end'   : date de fin
            - 'maxdt' : date de la valeur maximale
            - 'max'   : valeur maximale
            - 'massdt': date du centre de masse de l'événement
            - 'name'  : nom de l'événement (jour du max en str)
            - None sinon

    """
    _exception.raise_valueerror(
        before is not None and not isinstance(before, td),
        'Durée avant la pointe incorrecte'
    )
    _exception.raise_valueerror(
        after is not None and not isinstance(after, td),
        'Durée après la pointe incorrecte'
    )
    events = collections.OrderedDict()
    for k, i in enumerate(indices):
        k2 = k + 1
        max_value = float('{0:.3f}'.format(values[i]))
        max_dt = serie.data_frame.index[i].to_pydatetime()
        key = max_dt.strftime(serie.dtfmt)
        kf = tags.index(k2)
        kl = len(tags) - tags[::-1].index(-k2)
        firstdt = serie.data_frame.index[kf]
        if isinstance(before, td):
            firstdt = max(firstdt, max_dt + before)
        lastdt = serie.data_frame.index[kl]
        if isinstance(after, td):
            lastdt = min(lastdt, max_dt + after)
        evserie = serie.between_dates(
            first_dtime=firstdt, last_dtime=lastdt, inplace=False)
        key = max_dt.strftime(serie.dtfmt)
        events.setdefault(
            key,
            {
                'name': key,
                'max': max_value,
                'maxdt': max_dt,
                'massdt': evserie.timecentroid(),
                'firstdt': evserie.firstdt,
                'lastdt': evserie.lastdt,
                'length': evserie.length,
                'serie': evserie
            }
        )
    return events


def plot_events(serie=None, events=None, filename=None):
    """
    Tracer la série d'origine et ses événements

    Parameters
    ----------
    serie : pyspc.core.serie.Serie
        Série de données d'origine
    events : dict
        Dictionnaire des événements
            - clé : datetime (jour du maxi)
            - valeur :  Informations sur l'événement
                - 'start' : date de début
                - 'end'   : date de fin
                - 'maxdt' : date de la valeur maximale
                - 'max'   : valeur maximale
                - 'massdt': date du centre de masse de l'événement
                - 'name'  : nom de l'événement (jour du max en str)
                - None sinon
    filename : str
        Nom du fichier image. Si défini, imprime la série
        et ses événements dans une image

    """
    import matplotlib.pyplot as mplt
    import pylab
    # ---------------------------------------------------------------------
    # 1- Création de la figure
    # ---------------------------------------------------------------------
    fig = mplt.figure(dpi=150)
    axes = fig.add_axes((0.10, 0.10, 0.85, 0.80))
    # ---------------------------------------------------------------------
    # 2- Série originelle
    # ---------------------------------------------------------------------
    axes.plot_date(serie.data_frame.index, serie.data_frame.values,
                   color='tab:gray', linestyle='-', linewidth=2, marker='')
    # ---------------------------------------------------------------------
    # 3- Evenements
    # ---------------------------------------------------------------------
    for event in events:
        mn = events[event]['serie'].min()[0]
        mx = events[event]['max']
        dx = events[event]['maxdt']
        dc = events[event]['massdt']
        ds = events[event]['firstdt']
        de = events[event]['lastdt']
        df = events[event]['serie'].data_frame
        # Série de l'événement
        axes.plot_date(df.index, df.values,
                       color='blue', linestyle='-', linewidth=2, marker='')
        # Prominence : écart entre le min et le max
        axes.plot_date([dx, dx], [mn, mx],
                       linestyle='-', linewidth=1, marker='', color='k')
        # Durée : période entre le début et la fin
        axes.plot_date([ds, de], [mn, mn],
                       linestyle='-', linewidth=1, marker='', color='k')
        # Pointe de crue
        axes.plot_date(dx, mx, marker='x', color='tab:orange')
        # Centre de masse
        axes.plot_date(dc, mn, marker='x', color='tab:pink')

    # ---------------------------------------------------------------------
    # 4- Enregistrement du fichier
    # ---------------------------------------------------------------------
    mplt.title(
        '{} [{}] - du {} au {}\n{} événements'.format(
            serie.code, serie.spc_varname, serie.firstdt.strftime('%d/%m/%Y'),
            serie.lastdt.strftime('%d/%m/%Y'), len(events)),
        fontsize=12)
    pylab.savefig(filename, dpi=150)
    mplt.close()
