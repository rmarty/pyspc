#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Serie - Méthodes spécifiques - Figures
"""
import os.path


class Plotting():
    """
    Classe spécifique pour les mélanges à partir de pyspc.core.serie.Serie
    """
    def __init__(self):
        """
        Mélanges, mises à jour
        """
        self.code = None
        self.data_frame = None
        self.firstdt = None
        self.lastdt = None
        self.length = None
        self.spc_varname = None

    def plot(self, dirname='.', fmt='png'):
        """
        Imprimer la série de données contenue dans <self.data-frame>
        dans un fichier image

        Parameters
        ----------
        dirname : str
            Dossier où écrire l'image. Défaut: '.'
        fmt : str
            Format du fichier image. Défault: png

        Returns
        -------
        str
            Chemin complet du fichier image

        """
        filename = "_".join([
            self.code,
            self.firstdt.strftime(self.dtfmt),
            self.lastdt.strftime(self.dtfmt),
            self.spc_varname
        ])
        filename += '.' + fmt
        filename = os.path.join(dirname, filename)
        title = "{} [{}] - du {} au {}".format(
            self.code,
            self.spc_varname,
            self.firstdt.strftime('%d/%m/%Y'),
            self.lastdt.strftime('%d/%m/%Y')
        )
        ax = self.data_frame.plot(
            figsize=(11.69, 8.27),
            title=title
        )
        fig = ax.get_figure()
        fig.savefig(filename, dpi=150)
        return filename
