#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Serie - Méthodes spécifiques - Reindexation temporelle
"""
from datetime import datetime as dt
import math
import numpy as np
import pandas as pnd

import pyspc.core.exception as _exception
from pyspc.core.keyseries import str2tuple, tuple2str


class Reindexing():
    """
    Classe spécifique pour la reindexation temporelle de pyspc.core.serie.Serie
    """
    def __init__(self):
        """
        Reindexation temporelle
        """
        self.data_frame = None
        self._firstdt = None
        self._lastdt = None
        self._length = None

    def between_dates(self, first_dtime=None, last_dtime=None, inplace=False):
        """
        Sous-echantillonner <self>
        entre les dates <first_dtime> et <last_dtime>.
        Si l'option <inplace> est définie à False,
        alors cette méthode crée une nouvelle instance <Serie>

        Parameters
        ----------
        first_dtime : datetime
            Première date à conserver
        last_dtime : datetime
            Dernière date à conserver
        inplace : bool
            Traiter en place (True) ou renvoyer une nouvelle instance (False)
            défaut: False

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données, si inplace=False

        .. seealso:: pandas.DataFrame.index.shift

        """
        if isinstance(first_dtime, dt):
            subsample_start = first_dtime
        else:
            subsample_start = dt.strptime(first_dtime, self.dtfmt)
        if isinstance(last_dtime, dt):
            subsample_stop = last_dtime
        else:
            subsample_stop = dt.strptime(last_dtime, self.dtfmt)
        if subsample_stop < self.firstdt or subsample_start > self.lastdt:
            if self.warning:
                _exception.Warning(
                    __name__,
                    "pas de données entre {0} et {1}. "
                    "Aucune instance Serie créée.".format(
                        first_dtime, last_dtime))
            return None
        subsample_index = [i
                           for i in self.data_frame.index
                           if subsample_stop >= i >= subsample_start]
        reindex = self.data_frame.reindex(subsample_index)
        if reindex.empty:
            if self.warning:
                _exception.Warning(
                    __name__,
                    "pas de données entre {0} et {1}. "
                    "Aucune instance Serie créée.".format(
                        first_dtime, last_dtime))
            return None
        if not inplace:
            return self.__class__(reindex,
                                  code=self.code,
                                  varname=self.spc_varname,
                                  missing=self.missing)
        self.data_frame = reindex
        self._length = len(self.data_frame)
        self._firstdt = self.data_frame.index[0].to_pydatetime()
        self._lastdt = self.data_frame.index[-1].to_pydatetime()
        return None

    def reindex(self):
        """
        Re-indexer pour remplir les pas de temps manquants
        """
        if self.spc_varname.endswith('I'):
            return None
        if self.spc_varname in ['QM', 'PM']:
            freq = 'MS'
        else:
            freq = self.timestep
        dates = pnd.date_range(
            min(self.data_frame.index),
            max(self.data_frame.index),
            freq=freq
        )
        self.data_frame = self.data_frame.reindex(dates)
        self._length = len(self.data_frame)
        self._firstdt = self.data_frame.index[0].to_pydatetime()
        self._lastdt = self.data_frame.index[-1].to_pydatetime()
        return None

    def split(self, value=None, method=None):
        """
        Découper la série en une collection de séries.

        Parameters
        ----------
        value : int, float, list
            Valeur d'application de la méthodologie
        method : str
            Méthodologie de découpage de la série (N=value)

            - 'chunks' : N blocs en cherchant une taille homogène
            - 'chunk_size' : blocs de taille identique N
              sans garantir de taille homogène

            - 'days' : fréquence de N jours (N=value)
            - 'groups' : regroupements fournis sous la forme d'un DataFrame
              de même dimension que la série

            - 'dates' : découpage selon une liste de dates fournies sous
              la forme de datetime.datetime

        Returns
        -------
        pyspc.core.series.Series
            Collection de séries

        Notes
        -----

        Le nommage des nouvelles séries suit la procédure suivante:

        - s'il s'agit d'une série d'observation : définition d'une simulation
          'split-X', avec X l'identifiant du bloc

        - s'il s'agit d'une série de simulation : ajout de 'split-X' dans le
          nom de la simulation (en suffixe), avec X l'identifiant du bloc


        Examples
        --------
        >>> import pandas as pnd
        >>> from random import Random
        >>> from pyspc.core.serie import Serie

        >>> random = Random('pyspc')
        >>> dates = pnd.date_range(dt(2020, 1, 1), dt(2023, 7, 1), freq='H')
        >>> df = pnd.DataFrame([random.normalvariate(15, 10) for d in dates],
        ...                    index=dates)
        >>> obs = Serie(df, code='MASTATION', varname='TH')
        >>> sim = Serie(df, code='MASTATION_MASIMU', varname='TH')

        CAS PAR N BLOCS DE TAILLE HOMOGENE

        >>> series = obs.split(value=4, method='chunks')
        >>> series
        *************************************
        ********** SERIES *******************
        *************************************
        *  NOM DE LA COLLECTION = Split
        *  TYPE DE COLLECTION   = obs
        *  NOMBRE DE SERIES     = 4
        *  ----------------------------------
        *  SERIE #1
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-0
        *  ----------------------------------
        *  SERIE #2
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-1
        *  ----------------------------------
        *  SERIE #3
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-2
        *  ----------------------------------
        *  SERIE #4
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-3
        *************************************
        >>> for k, s in series.items():
        ...     print(k, s.length)
        ('MASTATION', 'TH', 'split-0') 7663
        ('MASTATION', 'TH', 'split-1') 7663
        ('MASTATION', 'TH', 'split-2') 7663
        ('MASTATION', 'TH', 'split-3') 7660

        CAS PAR BLOC DE TAILLE N - OBSERVATION

        >>> series = obs.split(value=10000, method='chunk_size')
        >>> series
        *************************************
        ********** SERIES *******************
        *************************************
        *  NOM DE LA COLLECTION = Split
        *  TYPE DE COLLECTION   = obs
        *  NOMBRE DE SERIES     = 4
        *  ----------------------------------
        *  SERIE #1
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-0
        *  ----------------------------------
        *  SERIE #2
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-1
        *  ----------------------------------
        *  SERIE #3
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-2
        *  ----------------------------------
        *  SERIE #4
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-3
        *************************************
        >>> for k, s in series.items():
        ...     print(k, s.length)
        ('MASTATION', 'TH', 'split-0') 10000
        ('MASTATION', 'TH', 'split-1') 10000
        ('MASTATION', 'TH', 'split-2') 10000
        ('MASTATION', 'TH', 'split-3') 649

        CAS PAR BLOC DE TAILLE N - SIMULATION

        >>> series = sim.split(value=10000, method='chunk_size')
        >>> series
        *************************************
        ********** SERIES *******************
        *************************************
        *  NOM DE LA COLLECTION = Split
        *  TYPE DE COLLECTION   = obs
        *  NOMBRE DE SERIES     = 4
        *  ----------------------------------
        *  SERIE #1
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = MASIMU-split-0
        *  ----------------------------------
        *  SERIE #2
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = MASIMU-split-1
        *  ----------------------------------
        *  SERIE #3
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = MASIMU-split-2
        *  ----------------------------------
        *  SERIE #4
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = MASIMU-split-3
        *************************************
        >>> for k, s in series.items():
        ...     print(k, s.length)
        ('MASTATION', 'TH', 'MASIMU-split-0') 10000
        ('MASTATION', 'TH', 'MASIMU-split-1') 10000
        ('MASTATION', 'TH', 'MASIMU-split-2') 10000
        ('MASTATION', 'TH', 'MASIMU-split-3') 649


        CAS PAR BLOC DE N JOURS

        >>> series = obs.split(value=365, method='days')
        >>> series
        *************************************
        ********** SERIES *******************
        *************************************
        *  NOM DE LA COLLECTION = Split
        *  TYPE DE COLLECTION   = obs
        *  NOMBRE DE SERIES     = 5
        *  ----------------------------------
        *  SERIE #1
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-0
        *  ----------------------------------
        *  SERIE #2
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-1
        *  ----------------------------------
        *  SERIE #3
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-2
        *  ----------------------------------
        *  SERIE #4
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-3
        *  ----------------------------------
        *  SERIE #5
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-4
        *************************************
        >>> for k, s in series.items():
        ...     print(k, s.length)
        ('MASTATION', 'TH', 'split-0') 7200
        ('MASTATION', 'TH', 'split-1') 7200
        ('MASTATION', 'TH', 'split-2') 7200
        ('MASTATION', 'TH', 'split-3') 7200
        ('MASTATION', 'TH', 'split-4') 1849


        CAS PAR LISTE DE DATES

        >>> split_dates = [dt(1999, 1, 1), dt(2020, 1, 1), dt(2021, 1, 1),
        ...                dt(2022, 1, 1), dt(2023, 1, 1), dt(2024, 1, 1)]
        >>> series = obs.split(value=split_dates, method='dates')
        >>> series
        *************************************
        ********** SERIES *******************
        *************************************
        *  NOM DE LA COLLECTION = Split
        *  TYPE DE COLLECTION   = obs
        *  NOMBRE DE SERIES     = 4
        *  ----------------------------------
        *  SERIE #1
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-0
        *  ----------------------------------
        *  SERIE #2
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-1
        *  ----------------------------------
        *  SERIE #3
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-2
        *  ----------------------------------
        *  SERIE #4
        *      - CODE    = MASTATION
        *      - VARNAME = TH
        *      - META    = split-3
        *************************************
        >>> for k, s in series.items():
        ...     print(k, s.firstdt, s.lastdt, s.length)

        ('MASTATION', 'TH', 'split-0') 2020-01-01 00:00 2020-12-31 23:00 8784
        ('MASTATION', 'TH', 'split-1') 2021-01-01 00:00 2021-12-31 23:00 8760
        ('MASTATION', 'TH', 'split-2') 2022-01-01 00:00 2022-12-31 23:00 8760
        ('MASTATION', 'TH', 'split-3') 2023-01-01 00:00 2023-07-01 00:00 4345

        """
        # -------------------------------------------------------------------
        # 0- Contrôle général
        # -------------------------------------------------------------------
        _methods = ['chunks', 'chunk_size', 'days', 'groups', 'dates',
                    'hydro_year']
        _exception.raise_valueerror(
            method not in _methods,
            f'Méthode incorrecte. A choisir parmi {_methods}')
        df_split = {}
        keyserie = str2tuple(tuple2str((self.code, self.spc_varname)))
        _exception.raise_valueerror(isinstance(keyserie[2], tuple),
                                    "Non applicable sur une prévision")
        # -------------------------------------------------------------------
        # 1.1- Découpage en N blocs
        # -------------------------------------------------------------------
        if method == 'chunks':
            _exception.check_numeric(value)
            return self.split(
                value=list(np.arange(len(self.df.index))
                           // math.ceil(len(self.df.index) / value)),
                method='groups')
        # -------------------------------------------------------------------
        # 1.2- Découpage en X blocs de taille N (homogénéité non garantie)
        # -------------------------------------------------------------------
        if method == 'chunk_size':
            _exception.check_numeric(value)
#            df_split = np.array_split(self.df, value) -> 30 secondes
            return self.split(
                value=list(np.arange(len(self.df.index)) // value),
                method='groups')
        # -------------------------------------------------------------------
        # 2.1- Découpage en X blocs de N jours
        # -------------------------------------------------------------------
        if method == 'days':
            _exception.check_int(value)
            g = 0
            for _, df in self.df.groupby(pnd.Grouper(freq=f"{value}D")):
                df_split[f"split-{g}"] = df
                g += 1
        # -------------------------------------------------------------------
        # 2.2- Découpage en X blocs selon des dates fournies par l'utilisateur
        # -------------------------------------------------------------------
        elif method == 'dates':
            _exception.check_listlike(value)
            groups = ['-'.join([str(x >= y) for y in value])
                      for x in self.df.index]
            g = 0
            for _, df in self.df.groupby(groups):
                df_split[f"split-{g}"] = df
                g += 1
        # -------------------------------------------------------------------
        # 2.3- Découpage par année hydrologique
        # -------------------------------------------------------------------
        elif method == 'hydro_year':
            _exception.check_int(value)
            offset = int(bool(value > 6))
            groups = [x.year + offset if x >= dt(x.year, value, 1)
                      else x.year + offset - 1
                      for x in self.df.index]
            for y, df in self.df.groupby(groups):
                df_split[f"hydro-year-{y}"] = df
        # -------------------------------------------------------------------
        # 2.X- Découpage par groupe
        # -------------------------------------------------------------------
        elif method == 'groups':
            _exception.check_listlike(value)
            _exception.raise_valueerror(
                len(value) != len(self.df.index),
                "Taille des groupes incohérente avec la série")
            df_split = {f"split-{g}": df for g, df in self.df.groupby(value)}
        # -------------------------------------------------------------------
        # 3- Retour sous forme de Series
        # -------------------------------------------------------------------
        series = self._constructor_expanddim(
            datatype='obs', name=f'split_{method}')
        for k, df in df_split.items():
            serie = self.__class__(
                df,
                code=keyserie[0], provider=self.provider, varname=keyserie[1])
            series.add(
                code=serie.code, serie=serie,
                meta=k if keyserie[2] is None else '-'.join([keyserie[2], k]))
        return series

    def stripna(self):
        """
        Renvoyer une nouvelle instance
        sans les np.nan en début et fin de série

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données

        """
        return self.between_dates(
            first_dtime=self.first_valid_index(),
            last_dtime=self.last_valid_index(),
            inplace=False
        )
