#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Serie - Méthodes spécifiques - Calculs
"""
import calendar
import collections
from datetime import datetime as dt, timedelta as td
import numpy as np
import pandas as pnd

import pyspc.core.exception as _exception


class Computation():
    """
    Classe spécifique pour les calculs à partir de pyspc.core.serie.Serie
    """
    def __init__(self):
        """
        Calculs
        """
        self.code = None
        self.data_frame = None
        self.firstdt = None
        self.lastdt = None
        self.length = None
        self.missing = None
        self.spc_varname = None
        self.timestep = None

    def above_threshold(self, threshold=None, inplace=False):
        """
        Déclarer les valeurs inférieures au seuil comme étant manquantes

        Parameters
        ----------
        threshold : int, float
            Seuil à appliquer
        inplace : bool
            Traiter en place (True) ou renvoyer une nouvelle instance (False)
            défaut: False

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données, si inplace=False

        """
        _exception.raise_valueerror(
            not isinstance(threshold, (int, float)),
            'Seuil incorrect'
        )
        if inplace:
            self.data_frame[self.data_frame < threshold] = np.nan
        else:
            data = self.data_frame.copy(deep=True)
            data[data < threshold] = np.nan
            return self.__class__(
                data,
                code="{0}_ath{1}".format(self.code, threshold),
                varname=self.spc_varname,
                missing=self.missing)
        return None

    def below_threshold(self, threshold=None, inplace=False):
        """
        Déclarer les valeurs supérieures au seuil comme étant manquantes

        Parameters
        ----------
        threshold : int, float
            Seuil à appliquer
        inplace : bool
            Traiter en place (True) ou renvoyer une nouvelle instance (False)
            défaut: False

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données, si inplace=False

        """
        _exception.raise_valueerror(
            not isinstance(threshold, (int, float)),
            'Seuil incorrect'
        )
        if inplace:
            self.data_frame[self.data_frame > threshold] = np.nan
        else:
            data = self.data_frame.copy(deep=True)
            data[data > threshold] = np.nan
            return self.__class__(
                data,
                code="{0}_bth{1}".format(self.code, threshold),
                varname=self.spc_varname,
                missing=self.missing)
        return None

    def counter_missing(self):
        """
        Déterminer le nombre de valeurs manquantes dans la série

        Returns
        -------
        int
            Nombre de valeurs manquantes dans la série de données

        """
        return self.length - self.data_frame.count(
            numeric_only=True)[(self.code, self.spc_varname)]

    def cumsum(self, skipna=True):
        """
        Série des sommes cumulées

        Parameters
        ----------
        skipna : bool
            Ignorer les valeurs manquantes

        Returns
        -------
        pyspc.core.serie.Serie
            Série des valeurs cumulées

        .. seealso:: pandas.DataFrame.cumsum

        """
        return self.__class__(
            self.data_frame.cumsum(skipna=skipna),
            code="{0}_csum".format(self.code),
            varname=self.spc_varname,
            missing=self.missing)

    def describe(self, freqs=None):
        """
        Déterminer les statistiques descriptives de la série

        Parameters
        ----------
        freqs : list
            Liste des fréquences, de 0 à 100

        Returns
        -------
        pnd.DataFrame
            Statistiques descriptives

        .. seealso:: pandas.DataFrame.describe

        """
        if freqs is None:
            freqs = [10, 50, 90]
        freqs = [f / 100. if f > 1 else f for f in freqs]
        return self.data_frame.describe(percentiles=freqs)

    def find_annual_max(self):
        """
        Lister les maximas annuels
        Année hydrologique : 01/09/aaaa-1 - 31/08/aaaa

        Returns
        -------
        dict
            Dictionnaire des maximas annuels.
            - clé : année
            - valeur : {'max': float, 'maxdt': datetime, 'pmv': float}
            avec
            - max   : valeur du max
            - maxdt : instant du max
            - pmv   : %age de valeurs manquantes dans l'année

        """
        annual_max = collections.OrderedDict()
        # Recherche des années hydrologiques AAAA
        # du 01/09/(AAAA-1) au 31/08/AAAA
        start = self.firstdt
        if start.month > 8:
            start = start.replace(year=start.year+1)
        end = self.lastdt
        if end.month > 8:
            end = end.replace(year=end.year+1)
        years = list(range(start.year, end.year+1))
        # Recherche des max
        for year in years:
            annual_max.setdefault(year, {'max': None,
                                         'maxdt': None,
                                         'pmv': None})
            start = dt(year-1, 9, 1)
            end = dt(year, 8, 31)
            mx = self.data_frame[
                (self.code, self.spc_varname)].loc[start:end].max()
            dx = self.data_frame[
                (self.code, self.spc_varname)].loc[start:end].idxmax()
            cmv = self.data_frame[
                (self.code, self.spc_varname)].loc[start:end].count()
            ctot = float(dt(year, 12, 31).timetuple().tm_yday)
            try:
                ctot *= int(td(days=1) / self.timestep)
            except TypeError:
                ctot *= int(td(days=1) / td(minutes=1))
            annual_max[year]['pmv'] = 1 - (cmv / ctot)
            annual_max[year]['max'] = mx
            annual_max[year]['maxdt'] = dx
        return annual_max

    def find_annual_min(self):
        """
        Lister les minimas annuels
        Année civile : 01/01/aaaa - 31/12/aaaa

        Returns
        -------
        dict
            Dictionnaire des minimas annuels.
            - clé : année
            - valeur : {'min': float, 'mindt': datetime, 'pmv': float}
            avec
            - min   : valeur du min
            - mindt : instant du min
            - pmv   : %age de valeurs manquantes dans l'année

        """
        annual_min = collections.OrderedDict()
        # Recherche des années civile AAAA
        # du 01/01/(AAAA) au 31/12/AAAA
        years = list(range(self.firstdt.year, self.lastdt.year + 1))
        # Recherche des max
        for year in years:
            annual_min.setdefault(year, {'min': None,
                                         'mindt': None,
                                         'pmv': None})
            start = dt(year, 1, 1)
            end = dt(year, 12, 31)
            mn = self.data_frame[
                (self.code, self.spc_varname)].loc[start:end].min()
            dn = self.data_frame[
                (self.code, self.spc_varname)].loc[start:end].idxmin()
            cmv = self.data_frame[
                (self.code, self.spc_varname)].loc[start:end].count()
            if self.timestep == td(days=31):
                ctot = float(dt(year, 12, 31).timetuple().tm_mon)
            elif self.timestep == td(days=1):
                ctot = float(dt(year, 12, 31).timetuple().tm_yday)
            elif self.timestep == td(hours=1):
                ctot = float(dt(year, 12, 31).timetuple().tm_yday) * 24
            else:
                raise NotImplementedError
            annual_min[year]['pmv'] = 1 - (cmv / ctot)
            annual_min[year]['min'] = mn
            annual_min[year]['mindt'] = dn
        return annual_min

    def max(self):
        """
        Renvoyer le maximum de la série et la date associée

        Returns
        -------
        float
            Valeur du maximum
        datetime
            Date du maximum

        .. seealso:: pandas.DataFrame.max et  pandas.DataFrame.idxmax

        """
        max_value = self.data_frame.max(skipna=True).values[0]
        max_dt = self.data_frame.idxmax(skipna=True).values[0]
        max_dt = pnd.Timestamp(max_dt).to_pydatetime()
        return max_value, max_dt

    def min(self):
        """
        Renvoyer le minimum de la série et la date associée

        Returns
        -------
        float
            Valeur du minimum
        datetime
            Date du minimum

        .. seealso:: pandas.DataFrame.min et  pandas.DataFrame.idxmin

        """
        min_value = self.data_frame.min(skipna=True).values[0]
        min_dt = self.data_frame.idxmin(skipna=True).values[0]
        min_dt = pnd.Timestamp(min_dt).to_pydatetime()
        return min_value, min_dt

    def regime(self, groupby=None, freqs=None, dayhour=6, strict=True,
               ignore_upscale=False):
        """
        Calculer le régime de la chronique.

        Parameters
        ----------
        groupby : str
            Regroupement des données

            - 'dayofyear' : par jour de l'année (défaut)
            - 'month' : par mois

        freqs : list
            Fréquences des quantiles, entre 0 et 1.
            Par défaut: [0.10, 0.25, 0.50, 0.75, 0.90]
        dayhour : int
            Heure du début de la journée Nécessaire si la cible est un pas de
            temps journalier. Par défaut: 6. Valeurs admises: [0, 6]
        strict : bool
            Calcul strict, imposant np.nan dans la valeur agrégée si au moins
            un pas de temps initial vaut np.nan. Retire de la série les pas
            de temps où les données d'origine ne sont pas complètes
            Défaut: True
        ignore_upscale : bool
            Ignorer la mise à l'échelle. Défaut: False.
            Si le paramètre est cumulable, ignore_upscale est mis à False


        Returns
        -------
        pandas.DataFrame
            Tableau du régime hydrologique contenant les minimas, maximas,
            moyennes et les quantiles associées aux fréquences spécifiées

        Examples
        --------
        >>> from datetime import datetime as dt
        >>> import numpy as np
        >>> import pandas as pnd
        >>> from random import Random
        >>> from pyspc.core.serie import Serie

        >>> random = Random('pyspc')
        >>> dates = pnd.date_range(dt(2000, 1, 1), dt(2023, 7, 1), freq='D')
        >>> df = pnd.DataFrame(
        ...     {'sigma': [5 + 4 * np.sin(
        ...                2 * np.pi * min(365, d.timetuple().tm_yday/365.0))
        ...                for d in dates],
        ...      'mu': [10 * np.cos(
        ...             2 * np.pi * min(365, d.timetuple().tm_yday/365.0))
        ...             for d in dates]},
        ...     index=dates
        ... )
        >>> df['normalvariate'] = df.apply(
        ...     lambda row: random.normalvariate(row['mu'], row['sigma']),
        ...                                      axis=1)
        >>> serie = Serie(df['normalvariate'], code='MASTATION', varname='TJ')

        CAS DAYOFYEAR

        >>> regime = serie.regime(groupby='dayofyear')
        >>> regime
                  MASTATION
                         TJ
                       amin       mean       amax       q10        q25        q50        q75        q90
        dayofyear
        1          1.944009  10.624312  20.113833  5.263542   6.774031  10.585418  14.503644  15.079707
        2         -0.259760   9.405203  19.817392  2.379518   5.376413   9.916178  13.573836  15.616009
        3         -0.181454  10.297588  21.049035  2.696734   6.207425   9.238487  14.893574  17.331033
        4         -5.145176   8.492487  18.598175  1.284406   5.552731   8.107479  12.133227  16.312690
        5          0.207759  10.119969  24.490016  4.522729   6.148460  10.294487  12.758538  18.530463
        6          2.459365  10.934372  20.392852  4.335647   8.223262  11.605376  13.718347  15.589172
        7          6.183924  11.048712  18.620448  6.527771   8.329515  10.557980  13.471992  15.162192
        8         -2.837258  10.011024  21.114053  4.385651   6.142117  10.629248  13.173047  16.101604
        9         -1.759126   9.194610  15.619534  3.082263   7.326997  10.416776  11.832072  14.088313
        10        -0.831915   8.491538  18.695751  1.792905   5.164604   9.183864  12.001302  13.811218
        11        -2.224917  12.474504  23.189785  3.526222   6.234537  13.716551  18.169241  20.929219
        12         0.192211   8.947970  16.653351  2.155900   5.366654   9.397785  12.749126  14.162431
        13        -1.863916   9.156694  23.647803  1.567216   5.279567   9.559719  12.201265  15.886308
        14        -1.283101   9.916968  19.012489  3.674430   6.081189  10.318217  13.123059  17.412076
        15        -0.132395  13.393634  20.754137  5.292921   9.852966  14.900619  18.073066  20.210763
        16        -9.843188   7.577008  23.533791  0.467720   4.078625   7.237520  12.271942  14.753191
        17        -0.945028   8.201018  19.217643  1.306905   4.509631   7.660138  11.837876  14.352006
        18        -0.162434   7.865895  19.764133  0.637975   4.182544   7.683877  11.582366  13.857101
        19        -1.631207  10.868952  28.835518  2.890020   5.641252  11.899187  13.981045  19.641085
        20         2.877473  10.354723  20.742810  4.655265   6.319709  10.345086  12.576178  17.543802
        21        -0.359789   9.753262  21.061425  3.589661   6.108222   8.882443  12.867481  17.005235
        22        -1.851937   9.712771  30.366130  2.020791   3.142092   6.993661  14.838456  20.981410
        23        -6.003920  12.254727  25.806680  5.528492   8.385381  11.543759  17.803213  20.503745
        24        -1.366565  12.479229  28.552720  4.677895   8.569749  13.075048  17.188437  20.053550
        25         1.376093   8.544323  19.961965  3.124412   4.563643   7.823728  12.067380  14.478125
        26        -4.134279   7.779648  22.400089 -0.935813   3.443725   7.990922  11.244633  17.413172
        27        -1.513403   9.824618  23.918041  1.980819   4.413007  10.064812  14.228396  20.450119
        28        -5.114598  10.350452  23.809753  2.831210   6.193378   8.552981  14.725332  20.465081
        29        -7.761090   9.869827  23.886531  2.467829   6.123588  10.901072  13.652352  18.006390
        30        -4.903314   7.137828  18.869998 -0.707865   3.447457   6.974199  10.312168  15.000295
        ...             ...        ...        ...       ...        ...        ...        ...        ...
        336        4.351649   8.410195  14.244957  5.452110   6.248769   7.916380  10.172580  13.205556
        337        4.545814   8.176946  13.055793  4.855146   5.632999   7.991481  10.304988  11.471382
        338        2.023751   8.394688  13.671743  4.861224   6.886953   8.498807  10.088508  12.182250
        339        5.878882   9.739374  13.343329  6.520828   7.900646  10.341276  11.430028  11.996803
        340       -0.935858   9.180920  15.083883  5.137096   6.875063   9.955873  11.356597  13.055717
        341        3.821531   9.702031  14.464514  7.489006   8.394184   8.883279  11.940618  13.151455
        342        4.135648   9.933368  22.267054  5.407166   6.692677   8.666765  12.087093  15.001430
        343        5.480372   9.526010  12.765188  7.955415   8.691168   9.864494  10.706000  11.037573
        344        0.055580   8.348085  15.573621  4.893742   6.194555   8.548581  10.075738  12.029745
        345        0.987658  10.016465  16.134114  5.214590   8.553301  10.828631  12.205413  13.509378
        346        4.291916   9.585788  15.770062  4.808465   7.238564  10.133367  11.882017  13.698296
        347        2.365325   9.447996  17.309514  5.161123   6.998339   9.650121  11.271402  13.393767
        348        1.815191   9.294719  16.110000  2.701645   6.778497   9.313064  12.013235  15.267646
        349        0.335351   9.172088  16.775585  5.303601   6.366535   9.946066  12.446337  14.094746
        350        4.418837  11.034226  20.066203  6.836929   8.993011  11.122376  12.471647  14.533944
        351        0.992976  10.118205  19.865972  3.931755   5.878853   9.863075  13.672705  17.438937
        352        2.360168   9.587696  16.356217  5.135191   7.616783   9.692119  11.805785  13.738285
        353        1.879207   9.721086  21.528831  5.128223   7.665339   9.636685  11.137671  14.015609
        354        0.005776   9.610247  21.223430  5.016252   6.975288   9.316436  12.683450  14.666547
        355        4.183093  10.805621  17.953885  6.658991   8.907132  11.562818  12.712600  13.687104
        356       -2.465142   9.719868  17.231451  5.316063   6.960975   8.504196  12.946158  15.516718
        357        2.207875   9.721184  20.190657  4.962292   7.182469   9.244547  11.818021  13.484418
        358        4.174932  10.007208  15.627777  5.014417   7.745050   9.366240  13.280770  15.255052
        359       -1.837906   9.957878  21.965943  0.363735   6.755967  10.363652  12.969316  17.755847
        360        0.682689  10.486417  21.506449  4.921221   6.599487   9.890103  15.009039  17.229929
        361        1.713487  12.005706  20.285289  6.604912   8.955794  12.263369  14.694296  18.709962
        362        3.215175  11.295978  16.617168  6.464909  10.327176  12.133073  13.649460  14.916064
        363       -0.747765   8.296246  16.288661  3.409350   5.990357   9.017231  10.270798  11.308030
        364        2.919868  11.530928  19.750778  6.676048   8.894218  10.682790  15.097649  17.149895
        365        3.755801  11.390088  17.965603  7.596842   8.426334  10.643652  15.149170  16.246636
        [365 rows x 8 columns]

        CAS MONTH

        >>> regime = serie.regime(groupby='month')
        >>> regime
               MASTATION
                      TJ
                    amin      mean       amax        q10        q25       q50        q75        q90
        month
        1     -10.987349  9.775277  30.366130   2.175369   5.483506  9.881925  13.756946  17.887767
        2     -20.927502  7.647808  31.096103  -2.161437   2.162670  7.702300  13.467442  17.929788
        3     -28.532033  2.447466  29.276437  -9.260649  -3.558794  2.684536   8.463805  14.078995
        4     -28.076151 -2.284140  22.403386 -13.540367  -8.314738 -2.430664   3.609519   9.348240
        5     -36.541390 -7.150944  18.317250 -17.218375 -12.848633 -7.027587  -1.690938   3.763987
        6     -29.483204 -9.808500   8.399533 -17.139951 -13.776696 -9.963173  -5.798924  -2.059937
        7     -24.401203 -9.630296   5.242768 -15.005319 -12.217287 -9.512707  -7.089044  -4.420848
        8     -16.569580 -6.969922   0.025375 -10.138419  -8.672136 -6.874644  -5.273049  -3.783082
        9      -8.038793 -2.551329   2.903838  -4.974818  -3.823146 -2.513706  -1.179915  -0.131121
        10     -2.843159  2.679328   8.982568   0.252474   1.242906  2.467920   3.998118   5.349232
        11      0.709528  6.937878  15.738727   3.912094   5.228819  6.599372   8.564590  10.420266
        12     -2.465142  9.774600  22.267054   5.040022   7.199803  9.765345  12.172330  14.699299

        """
        # -------------------------------------------------
        # 0- Contrôles
        # -------------------------------------------------
        if groupby is None:
            groupby = 'dayofyear'
        if groupby == 'dayofyear':
            ts = td(days=1)
        else:
            ts = td(days=31)
        _exception.check_in(groupby, ['dayofyear', 'month'])
        if freqs is None:
            freqs = [0.10, 0.25, 0.50, 0.75, 0.90]
        _exception.check_listlike(freqs)
        _exception.raise_valueerror(
            not isinstance(ignore_upscale, bool),
            "Le paramètre 'ignore_upscale' est incorrect"
        )
        if self.parameter.cumulable and ignore_upscale:
            _exception.Warning(
                None,
                "Le paramètre 'ignore_upscale' est forcé à False "
                "dans le cas du paramètre cumulable {}"
                "".format(self.spc_varname))
            ignore_upscale = False
        # -------------------------------------------------
        # CAS 1: Ignorer la mise à l'échelle
        # -------------------------------------------------
        if ignore_upscale:
            return _apply_regime_groupby(self, groupby, freqs)
        # -------------------------------------------------
        # CAS 2: Aggrégation au pas de temps "idéal" selon groupby
        # -------------------------------------------------
        if self.timestep is None:
            src = self.regularscale()
        else:
            src = self
        # Paramètre cible
        try:
            target = src.parameter.find(
                prefix=src.spc_varname[0], timedelta=ts)
        except ValueError:
            _exception.Warning(
                None,
                "Application du régime sans mise au pas de temps adapté, "
                "à partir de {}, imposé par groupby={}"
                "".format(self.spc_varname, groupby))
            return _apply_regime_groupby(self, groupby, freqs)
        # Test si 'Même grandeur que Upscalable'
        if src.parameter == target:
            return _apply_regime_groupby(self, groupby, freqs)
        # Test si 'Upscalable'
        _exception.raise_valueerror(
            not src.parameter.isUpscalable(target),
            "Le calcul du régime nécessite que la série soit agrégée "
            "au pas de temps {}".format(ts)
        )
        # Série agrégée
        up = src.upscale(toparam=target, dayhour=dayhour, strict=strict)
        return _apply_regime_groupby(up, groupby, freqs)

    def timecentroid(self):
        """
        Déterminer le centre de masse temporel de la série

        Returns
        -------
        datetime
            Centre de masse temporel

        """
        c = self.data_frame.columns[0]
        self.data_frame["epoch"] = self.data_frame.index.astype('int64')//1e9
        mean_date = (
            self.data_frame["epoch"] * self.data_frame[c]
        ).sum(skipna=True) / self.data_frame[c].sum(skipna=True)
        self.data_frame = self.data_frame.drop('epoch', axis=1)
        mean_date = dt.fromtimestamp(mean_date)
        mean_loc = self.data_frame.index.get_loc(mean_date, method='nearest')
        return self.data_frame.index[mean_loc].to_pydatetime()


# =============================================================================
# --- FONCTION DE CALCUL DE PERCENTILE POUR DF.agg
def percentile(n):
    """Fonction percentile"""
    def percentile_(x):
        """Fonction percentile interne"""
        return x.quantile(n)
    percentile_.__name__ = 'q{0:02.0f}'.format(n*100)
    return percentile_


def _groupby_doy(index):
    """
    Regroupement par jour de l'année,
    en retirant l'effet des années bissextiles
    """
    groups = []
    for x in index:
        if calendar.isleap(x.year) and x.month >= 3:
            groups.append(x.dayofyear - 1)
        else:
            groups.append(x.dayofyear)
    return groups


def _apply_regime_groupby(src, groupby, freqs):
    """
    Appliquer le calcul du régime hydrologique
    """
    df = src.data_frame.copy(deep=True)
    col = df.columns[0]
    if groupby == 'dayofyear':
        df['group'] = _groupby_doy(df.index)
    if groupby == 'month':
        df['group'] = df.index.month
    fcts = [np.min, np.mean, np.max]
    fcts.extend([percentile(f) for f in freqs if 0 < f < 1])
    aggdf = df.groupby('group').agg({col: fcts})
    aggdf.index.name = groupby
    return aggdf
# =============================================================================
