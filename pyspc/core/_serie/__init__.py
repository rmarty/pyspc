#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs et convention de pyspc - Serie - Méthodes spécifiques
"""

from ._binaryoperator import BinaryOperator
from ._combining import Combining
from ._computation import Computation
from ._copy import Copy
from ._event import Event
from ._modeling import Modeling
from ._plotting import Plotting
from ._reindexing import Reindexing
from ._scaling import Scaling
from ._timeserie import TimeSerie

__all__ = [
    'BinaryOperator',  # méthodes des opérateurs binaires
    'Combining',  # méthodes de mélanges, mises-à-jour
    'Computation',  # méthodes de calculs
    'Copy',  # méthodes de copie
    'Event',  # méthodes de détermination d'événements
    'Modeling',  # méthodes de modélisation
    'Plotting',  # méthodes de figures
    'Reindexing',  # méthodes de re-indexation temporelle
    'Scaling',  # méthodes de changement d'échelle temporelle
    'TimeSerie',  # méthodes de traitement temporel
]
