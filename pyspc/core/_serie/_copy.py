#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Serie - Méthodes spécifiques - Copies
"""
import copy as _cp


class Copy():
    """
    Classe spécifique pour la copie de pyspc.core.serie.Serie
    """
    def __init__(self):
        """
        Copies
        """

    def copy(self):
        """
        Copier 'superficiellement' l'instance <Serie>.
        Une modification dans la copie modifiera aussi l'originale!
        Si vous voulez une copie sans interaction, il faut utiliser
        self.deepcopy()

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données, si inplace=False

        """
        return _cp.copy(self)

    def deepcopy(self):
        """
        Copier 'profondément' l'instance <Serie>.
        Une modification dans la copie ne modifiera pas l'originale!

        Returns
        -------
        pyspc.core.serie.Serie
            Nouvelle série de données, si inplace=False

        """
        return _cp.deepcopy(self)
