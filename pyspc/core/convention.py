#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs et convention de pyspc - Convention interne à pyspc
"""
from collections import namedtuple
from datetime import timedelta as td

# -----------------------------------------------------------------------------
# --- FOURNISSEURS EXTERNES : (fournisseur, grandeur) -> grandeur
# -----------------------------------------------------------------------------
EXTERNAL_VARNAMES = {

    # ---------------------------
    # --- Bareme
    # ---------------------------
    ('Bareme', 'jcote'): 'HI',
    ('Bareme', 'jdebit'): 'QI',
    ('Bareme', 'valeur'): 'HI',

    # ---------------------------
    # --- Cristal
    # ---------------------------
    ('Cristal', ('KMHEAU', 'VALUE_MES')): 'HI',
    ('Cristal', ('KMHEAU', 'VALUE_CONV')): 'QI',
    ('Cristal', ('KMLBAR', 'VALUE_MES')): 'ZI',
    ('Cristal', ('KMLBAR', 'VALUE_CONV')): 'QI',
    ('Cristal', ('KMTEMP', 'VALUE_MES')): 'TI',  # Température de l'eau ?
    ('Cristal', ('KMTEMP', 'VALUE_CONV')): 'TI',  # Température de l'air ?
    # ('KMTEMP', 'VALUE_MES'): 'TI',  # Température de l'eau ?
    # ('KMPLUI', 'VALUE_MES'): 'RR',  # Pluvio: directement spc_varname

    # ---------------------------
    # --- GRP16
    # ---------------------------
    ('GRP16', 'P'): 'PH',
    ('GRP16', 'T'): 'TH',
    ('GRP16', 'EH'): 'EH',
    ('GRP16', 'EJ'): 'EJ',
    ('GRP16', 'Q'): 'QH',
    ('GRP16', 'H'): 'HH',

    # ---------------------------
    # --- GRP18
    # ---------------------------
    ('GRP18', ('Q', None)): 'QI',
    ('GRP18', ('Q', td(hours=1))): 'QH',
    ('GRP18', ('Q', td(days=1))): 'QJ',
    ('GRP18', ('P', td(minutes=5))): 'P5m',
    ('GRP18', ('P', td(minutes=6))): 'P6m',
    ('GRP18', ('P', td(minutes=15))): 'P15m',
    ('GRP18', ('P', td(hours=1))): 'PH',
    ('GRP18', ('P', td(hours=3))): 'P3H',
    ('GRP18', ('P', td(days=1))): 'PJ',
    ('GRP18', ('T', None)): 'TI',
    ('GRP18', ('T', td(hours=1))): 'TH',
    ('GRP18', ('T', td(days=1))): 'TJ',
    ('GRP18', ('E', td(hours=1))): 'EH',
    ('GRP18', ('E', td(days=1))): 'EJ',

    # ---------------------------
    # --- GRP20
    # ---------------------------
    ('GRP20', ('Q', None)): 'QI',
    ('GRP20', ('Q', td(hours=1))): 'QH',
    ('GRP20', ('Q', td(days=1))): 'QJ',
    ('GRP20', ('P', td(minutes=5))): 'P5m',
    ('GRP20', ('P', td(minutes=6))): 'P6m',
    ('GRP20', ('P', td(minutes=15))): 'P15m',
    ('GRP20', ('P', td(hours=1))): 'PH',
    ('GRP20', ('P', td(hours=3))): 'P3H',
    ('GRP20', ('P', td(days=1))): 'PJ',
    ('GRP20', ('T', None)): 'TI',
    ('GRP20', ('T', td(hours=1))): 'TH',
    ('GRP20', ('T', td(days=1))): 'TJ',
    ('GRP20', ('E', td(hours=1))): 'EH',
    ('GRP20', ('E', td(days=1))): 'EJ',

    # ---------------------------
    # --- GRP22
    # ---------------------------
    ('GRP22', ('Q', None)): 'QI',
    ('GRP22', ('Q', td(hours=1))): 'QH',
    ('GRP22', ('Q', td(days=1))): 'QJ',
    ('GRP22', ('P', td(minutes=5))): 'P5m',
    ('GRP22', ('P', td(minutes=6))): 'P6m',
    ('GRP22', ('P', td(minutes=15))): 'P15m',
    ('GRP22', ('P', td(hours=1))): 'PH',
    ('GRP22', ('P', td(hours=3))): 'P3H',
    ('GRP22', ('P', td(days=1))): 'PJ',
    ('GRP22', ('T', None)): 'TI',
    ('GRP22', ('T', td(hours=1))): 'TH',
    ('GRP22', ('T', td(days=1))): 'TJ',
    ('GRP22', ('E', td(hours=1))): 'EH',
    ('GRP22', ('E', td(days=1))): 'EJ',

    # ---------------------------
    # --- Hydro2
    # ---------------------------
    ('Hydro2', 'H-TEMPS'): 'HI',
    ('Hydro2', 'QTVAR'): 'QI',
    ('Hydro2', 'QTFIX'): 'QH',
    ('Hydro2', 'QJM'): 'QJ',
    ('Hydro2', 'TOUSMOIS'): 'QM',

    # ---------------------------
    # --- Lamedo
    # ---------------------------
    ('BdApbp', 'short'): 'PJ',
    ('BdApbp', 'long'): 'PJ',

    # ---------------------------
    # --- Météo-France
    # ---------------------------
    # Météo-France - Publithèque
    ('MF_Data', 'RR5'): 'P5m',
    ('MF_Data', 'RR6'): 'P6m',
    ('MF_Data', 'RR1'): 'PH',
    ('MF_Data', 'RR3'): 'P3H',
    ('MF_Data', 'RR'): 'PJ',
    ('MF_Data', 'RR24'): 'PJ',
    ('MF_Data', 'T'): 'TH',
    ('MF_Data', 'TNTXM'): 'TJ',
    ('MF_Data', 'ETPMON'): 'EJ',
    # Météo-France - Prévision Symposium
    ('Sympo', 'RR3'): 'P3H',
    # Météo-France - OpenData
    ('MF_OpenData', 'RR', 'MN'): 'P6m',
    ('MF_OpenData', 'RR1', 'H'): 'PH',
    ('MF_OpenData', 'RR', 'Q'): 'PJ',
    ('MF_OpenData', 'T', 'H'): 'TH',
    ('MF_OpenData', 'TNTXM', 'Q'): 'TJ',
    # Météo-France - OpenAPI
    ('MF_OpenAPI', 'RR6', 'MN'): 'P6m',
    ('MF_OpenAPI', 'RR1', 'H'): 'PH',
    ('MF_OpenAPI', 'RR', 'Q'): 'PJ',
    ('MF_OpenAPI', 'T', 'H'): 'TH',
    ('MF_OpenAPI', 'TNTXM', 'Q'): 'TJ',

    # ---------------------------
    # --- MORDOR
    # ---------------------------
    ('Mordor', 'Q'): 'QJ',
    ('Mordor', 'V'): 'VI',

    # ---------------------------
    # --- PLATHYNES
    # ---------------------------
    # directement dans le reader

    # ---------------------------
    # --- PRV (Otamin, Scores)
    # ---------------------------
    ('prv', ('Q', None)): 'QI',
    ('prv', ('Q', td(hours=1))): 'QH',
    ('prv', ('H', None)): 'HI',
    ('prv', ('H', td(hours=1))): 'HH',
    ('prv', ('RR', td(hours=1))): 'PH',
    ('prv', ('TA', None)): 'TI',
    ('prv', ('TA', td(hours=1))): 'TH',

    # ---------------------------
    # --- Premhyce
    # ---------------------------
    ('Premhyce', 'Data'): 'QJ',
    ('Premhyce', 'Fcst'): 'QJ',

    # ---------------------------
    # --- Sandre
    # ---------------------------
    # directement dans le reader

    # ---------------------------
    # --- Vigicrues
    # ---------------------------
    # directement dans le reader

}


# -----------------------------------------------------------------------------
# --- namedtuple SPC_VARNAME
# -----------------------------------------------------------------------------
_SPC_VARNAME = namedtuple(
    'SPC_VARNAME',
    ['spc_varname', 'desc', 'timedelta', 'units'],
    defaults=['spc_varname', None, None, None]
)

SPC_VARNAMES = {
    'P5m': _SPC_VARNAME(
        'P5m', 'Précipitation 5-minutes', td(seconds=300), 'mm',
    ),
    'P6m': _SPC_VARNAME(
        'P6m', 'Précipitation 6-minutes', td(seconds=360), 'mm',
    ),
    'P15m': _SPC_VARNAME(
        'P15m', 'Précipitation 15-minutes', td(seconds=900), 'mm',
    ),
    'PH': _SPC_VARNAME(
        'PH', 'Précipitation horaire', td(hours=1), 'mm',
    ),
    'P3H': _SPC_VARNAME(
        'P3H', 'Précipitation tri-horaire', td(hours=3), 'mm',
    ),
    'PJ': _SPC_VARNAME(
        'PJ', 'Précipitation journalière', td(days=1), 'mm',
    ),
    'PM': _SPC_VARNAME(
        'PM', 'Précipitation mensuelle', td(days=31), 'mm',
    ),
    'TI': _SPC_VARNAME(
        'TI', 'Température instantanée', None, 'celsius_degrees',
    ),
    'TH': _SPC_VARNAME(
        'TH', 'Température horaire', td(hours=1), 'celsius_degrees',
    ),
    'TJ': _SPC_VARNAME(
        'TJ', 'Température moyenne journalière', td(days=1), 'celsius_degrees',
    ),
#    'TM': _SPC_VARNAME(
#        'TM', 'Température moyenne mensuelle', td(days=31), 'celsius_degrees',
#    ),
    'EH': _SPC_VARNAME(
        'EH', 'Evapo-transpiration horaire', td(hours=1), 'mm',
    ),
    'EJ': _SPC_VARNAME(
        'EJ', 'Evapo-transpiration journalière', td(days=1), 'mm',
    ),
#    'EM': _SPC_VARNAME(
#        'EM', 'Evapo-transpiration mensuelle', td(days=31), 'mm',
#    ),
    'QI': _SPC_VARNAME(
        'QI', 'Débit instantané', None, 'm3/s',
    ),
    'QH': _SPC_VARNAME(
        'QH', 'Débit horaire', td(hours=1), 'm3/s',
    ),
    'QJ': _SPC_VARNAME(
        'QJ', 'Débit moyen journalier', td(days=1), 'm3/s',
    ),
    'QM': _SPC_VARNAME(
        'QM', 'Débit moyen mensuel', td(days=31), 'm3/s',
    ),
    'HI': _SPC_VARNAME(
        'HI', 'Hauteur instantanée', None, 'm',
    ),
    'HH': _SPC_VARNAME(
        'HH', 'Hauteur horaire', td(hours=1), 'm',
    ),
    'ZI': _SPC_VARNAME(
        'ZI', 'Cote instantanée', None, 'mNGF',
    ),
    'ZH': _SPC_VARNAME(
        'ZH', 'Cote horaire', td(hours=1), 'mNGF',
    ),
    'VI': _SPC_VARNAME(
        'VI', 'Volume retenue instantanée', None, 'm3',
    ),
    'VH': _SPC_VARNAME(
        'VH', 'Volume retenue horaire', td(hours=1), 'm3',
    ),
    'HU2J': _SPC_VARNAME(
        'HU2J', 'Humidité du sol moyenne journalière', td(days=1), '%',
#        'HU2J', 'Humidité du sol moyenne journalière', None, '%',
    ),
}
"""Dictionnaire des grandeurs de la convention suivie par pyspc"""

# -----------------------------------------------------------------------------
# --- namedtuple SPC_TIME
# -----------------------------------------------------------------------------
_SPC_TIME = namedtuple(
    'SPC_TIME',
    ['timedelta', 'timeunits', 'dtfmt'],
    defaults=['spc_time', None, None]
)

SPC_TIMES = {
    None: _SPC_TIME(None, None, '%Y%m%d%H%M'),
    td(seconds=300): _SPC_TIME(td(seconds=300), '5-min', '%Y%m%d%H%M'),
    td(seconds=360): _SPC_TIME(td(seconds=360), '6-min', '%Y%m%d%H%M'),
    td(seconds=900): _SPC_TIME(td(seconds=900), '15-min', '%Y%m%d%H%M'),
    td(hours=1): _SPC_TIME(td(hours=1), 'hour', '%Y%m%d%H'),
    td(hours=3): _SPC_TIME(td(hours=3), '3-hour', '%Y%m%d%H'),
    td(days=1): _SPC_TIME(td(days=1), 'days', '%Y%m%d'),
    td(days=31): _SPC_TIME(td(days=31), 'month', '%Y%m'),
}
"""Dictionnaire des durées de la convention suivie par pyspc"""

# -----------------------------------------------------------------------------
# --- LIEUX
# -----------------------------------------------------------------------------
LOCTYPE = [
    'point',  # ponctuel
    'basin',  # bassin versant
]
"""Type de lieu"""
