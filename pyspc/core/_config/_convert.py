#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs et convention de pyspc - Config - Convertisseurs
"""
from datetime import datetime as dt
import os


class CONVERT():
    """
    Classe spécifique pour les convertisseurs de pyspc.core.config.Config
    """
    def __init__(self):
        """
        Convertisseurs
        """

    @staticmethod
    def from_listofint(l, sep=None, fmt=None):
        """
        Convertir liste d'entiers en une chaîne de caractères

        Parameters
        ----------
        text : str
            Texte à convertir
        sep : str
            Séparateur. Par défaut: ';'
        fmt : str
            Format. Par défaut: '{}'

        Returns
        -------
        Chaîne de caractères

        """
        if not isinstance(l, list):
            return ''
        if sep is None:
            sep = ";"
        return sep.join([CONVERT.from_int(x, fmt=fmt) for x in l])

    @staticmethod
    def from_listofintorstr(l, sep=None, fmt=None):
        """
        Convertir liste d'entiers ou de texte en une chaîne de caractères

        Parameters
        ----------
        text : str
            Texte à convertir
        sep : str
            Séparateur. Par défaut: ';'
        fmt : str
            Format. Par défaut: '{}'

        Returns
        -------
        Chaîne de caractères

        """
        if not isinstance(l, list):
            return ''
        if sep is None:
            sep = ";"
        return sep.join([CONVERT.from_int(x, fmt=fmt)
                         if isinstance(x, int) else x
                         for x in l])

    @staticmethod
    def from_listoffloat(l, sep=None, fmt=None):
        """
        Convertir liste de réels en une chaîne de caractères

        Parameters
        ----------
        text : str
            Texte à convertir
        sep : str
            Séparateur. Par défaut: ';'
        fmt : str
            Format. Par défaut: '{}'

        Returns
        -------
        Chaîne de caractères

        """
        if not isinstance(l, list):
            return ''
        if sep is None:
            sep = ";"
        return sep.join([CONVERT.from_float(x, fmt=fmt) for x in l])

    @staticmethod
    def from_listofstr(l, sep=None):
        """
        Convertir liste de chaînes de caractères en une chaîne de caractères

        Parameters
        ----------
        l : str
            Texte à convertir
        sep : str
            Séparateur. Par défaut: ';'

        Returns
        -------
        Chaîne de caractères

        """
        if not isinstance(l, list):
            return ''
        if sep is None:
            sep = ";"
        return sep.join(l)

    @staticmethod
    def from_dictofint(d, sep1=None, sep2=None, fmt=None):
        """
        Convertir dictionnaire d'entiers en une chaîne de caractères

        Parameters
        ----------
        d : dict
            Dictionnaire à convertir
        sep1 : str
            Séparateur définissant les groupes
            'clévaleurSEP...'. Par défaut: ';'
        sep2 : str
            Séparateur définissant les clés et valeurs
            'cléSEPvaleur'. Par défaut: ','

        Returns
        -------
        Chaîne de caractères

        """
        if not isinstance(d, dict):
            return ''
        if sep1 is None:
            sep1 = ";"
        if sep2 is None:
            sep2 = ","
        return sep1.join([sep2.join([k, CONVERT.from_int(v, fmt)])
                          for k, v in d.items()])

    @staticmethod
    def from_dictoffloat(d, sep1=None, sep2=None, fmt=None):
        """
        Convertir dictionnaire de réels en une chaîne de caractères

        Parameters
        ----------
        d : dict
            Dictionnaire à convertir
        sep1 : str
            Séparateur définissant les groupes
            'clévaleurSEP...'. Par défaut: ';'
        sep2 : str
            Séparateur définissant les clés et valeurs
            'cléSEPvaleur'. Par défaut: ','

        Returns
        -------
        Chaîne de caractères

        """
        if not isinstance(d, dict):
            return ''
        if sep1 is None:
            sep1 = ";"
        if sep2 is None:
            sep2 = ","
        return sep1.join([sep2.join([k, CONVERT.from_float(v, fmt)])
                          for k, v in d.items()])

    @staticmethod
    def from_dictofstr(d, sep1=None, sep2=None):
        """
        Convertir dictionnaire de str en une chaîne de caractères

        Parameters
        ----------
        d : dict
            Dictionnaire à convertir
        sep1 : str
            Séparateur définissant les groupes
            'clévaleurSEP...'. Par défaut: ';'
        sep2 : str
            Séparateur définissant les clés et valeurs
            'cléSEPvaleur'. Par défaut: ','

        Returns
        -------
        Chaîne de caractères

        """
        if not isinstance(d, dict):
            return ''
        if sep1 is None:
            sep1 = ";"
        if sep2 is None:
            sep2 = ","
        return sep1.join([sep2.join([k, v]) for k, v in d.items()])

    @staticmethod
    def from_bool(n):
        """
        Convertir un booléen en une chaîne de caractères

        Parameters
        ----------
        n : bool
            Valeur à convertir

        Returns
        -------
        Chaîne de caractères

        """
        if not isinstance(n, bool):
            return ''
        return str(n)

    @staticmethod
    def from_datetime(n, fmt=None):
        """
        Convertir un datetime en une chaîne de caractères

        Parameters
        ----------
        n : datetime
            datetime à convertir
        fmt : str
            Format. Par défaut: '%Y%m%d%H'

        Returns
        -------
        Chaîne de caractères

        """
        if not isinstance(n, dt):
            return ''
        if fmt is None or not isinstance(fmt, str):
            fmt = '%Y%m%d%H'
        return n.strftime(fmt)

    @staticmethod
    def from_float(n, fmt=None):
        """
        Convertir un réel en une chaîne de caractères

        Parameters
        ----------
        n : float
            Valeur à convertir
        fmt : str
            Format. Par défaut: '{}'

        Returns
        -------
        Chaîne de caractères

        """
        if not isinstance(n, float):
            return ''
        if fmt is None or not isinstance(fmt, str):
            fmt = '{}'
        return fmt.format(n)

    @staticmethod
    def from_int(n, fmt=None):
        """
        Convertir un entier en une chaîne de caractères

        Parameters
        ----------
        n : :obj:`int`
            Valeur à convertir
        fmt : str
            Format. Par défaut: '{}'

        Returns
        -------
        Chaîne de caractères

        """
        if not isinstance(n, int):
            return ''
        if fmt is None or not isinstance(fmt, str):
            fmt = '{}'
        return fmt.format(n)

    @staticmethod
    def to_listofint(text, sep=None):
        """
        Convertir une chaîne de caractères en liste d'entiers ou de texte

        Parameters
        ----------
        text : str
            Texte à convertir
        sep : str
            Séparateur. Par défaut: ';'

        Returns
        -------
        Liste d'entiers

        """
        if text is None or text == '':
            return None
        if not isinstance(text, str):
            return text
        if sep is None:
            sep = ";"
        return [int(float(x)) for x in text.split(sep)]

    @staticmethod
    def to_listofintorstr(text, sep=None):
        """
        Convertir une chaîne de caractères en liste d'entiers ou de texte

        Parameters
        ----------
        text : str
            Texte à convertir
        sep : str
            Séparateur. Par défaut: ';'

        Returns
        -------
        Liste d'entiers ou de texte

        """
        if text is None or text == '':
            return None
        if not isinstance(text, str):
            return text
        if sep is None:
            sep = ";"
        return [int(float(x)) if x.isdigit() else x
                for x in text.split(sep)]

    @staticmethod
    def to_listoffloat(text, sep=None):
        """
        Convertir une chaîne de caractères en liste de réels

        Parameters
        ----------
        text : str
            Texte à convertir
        sep : str
            Séparateur. Par défaut: ';'

        Returns
        -------
        Liste de réels

        """
        if text is None or text == '':
            return None
        if not isinstance(text, str):
            return text
        if sep is None:
            sep = ";"
        return [float(x) for x in text.split(sep)]

    @staticmethod
    def to_listofstr(text, sep=None):
        """
        Convertir une chaîne de caractères en liste de chaînes de caractères

        Parameters
        ----------
        text : str
            Texte à convertir
        sep : str
            Séparateur. Par défaut: ';'

        Returns
        -------
        Liste de chaînes de caractères

        """
        if text is None or text == '':
            return None
        if not isinstance(text, str):
            return text
        if sep is None:
            sep = ";"
        return text.split(sep)

    @staticmethod
    def to_listofdatetime(text, sep=None, fmt='%Y%m%d%H'):
        """
        Convertir une chaîne de caractères en liste de datetime

        Parameters
        ----------
        text : str
            Texte à convertir
        sep : str
            Séparateur. Par défaut: ';'
        fmt : str
            Format de la date

        Returns
        -------
        Liste de chaînes de caractères

        """
        if text is None or text == '':
            return None
        if not isinstance(text, str):
            return text
        if sep is None:
            sep = ";"
        return [dt.strptime(x, fmt) for x in text.split(sep)]

    @staticmethod
    def to_dictofint(text, sep1=None, sep2=None):
        """
        Convertir une chaîne de caractères en dictionnaire d'entiers

        Parameters
        ----------
        text : str
            Texte à convertir
        sep1 : str
            Séparateur définissant les groupes
            'clévaleurSEP...'. Par défaut: ';'
        sep2 : str
            Séparateur définissant les clés et valeurs
            'cléSEPvaleur'. Par défaut: ','

        Returns
        -------
        Dictionnaire d'entiers

        """
        if text is None or text == '':
            return None
        if not isinstance(text, str):
            return text
        if sep1 is None:
            sep1 = ";"
        if sep2 is None:
            sep2 = ","
        d = {}
        for k in text.split(sep1):
            [c, a] = k.split(sep2)
            d[c] = int(float(a))
        return d

    @staticmethod
    def to_dictoffloat(text, sep1=None, sep2=None):
        """
        Convertir une chaîne de caractères en dictionnaire de réels

        Parameters
        ----------
        text : str
            Texte à convertir
        sep1 : str
            Séparateur définissant les groupes
            'clévaleurSEP...'. Par défaut: ';'
        sep2 : str
            Séparateur définissant les clés et valeurs
            'cléSEPvaleur'. Par défaut: ','

        Returns
        -------
        Dictionnaire de réels

        """
        if text is None or text == '':
            return None
        if not isinstance(text, str):
            return text
        if sep1 is None:
            sep1 = ";"
        if sep2 is None:
            sep2 = ","
        d = {}
        for k in text.split(sep1):
            [c, a] = k.split(sep2)
            d[c] = float(a)
        return d

    @staticmethod
    def to_dictofstr(text, sep1=None, sep2=None):
        """
        Convertir une chaîne de caractères en dictionnaire de str

        Parameters
        ----------
        text : str
            Texte à convertir
        sep1 : str
            Séparateur définissant les groupes
            'clévaleurSEP...'. Par défaut: ';'
        sep2 : str
            Séparateur définissant les clés et valeurs
            'cléSEPvaleur'. Par défaut: ','

        Returns
        -------
        Dictionnaire de str

        """
        if text is None or text == '':
            return None
        if not isinstance(text, str):
            return text
        if sep1 is None:
            sep1 = ";"
        if sep2 is None:
            sep2 = ","
        d = {}
        for k in text.split(sep1):
            x = k.split(sep2)
            d[x[0]] = sep2.join(x[1:])
        return d

    @staticmethod
    def to_bool(text):
        """
        Convertir une chaîne de caractères en un booléen

        Parameters
        ----------
        text : str
            Texte à convertir

        Returns
        -------
        Booléen

        """
        if text is None or text == '':
            return None
        if not isinstance(text, str):
            return text
        return text.lower() in ['true', 'yes', 'oui']

    @staticmethod
    def to_datetime(fmt, text):
        """
        Convertir une chaîne de caractères en datetime

        Parameters
        ----------
        fmt : str
            Format de la date
        text : str
            Texte à convertir

        Returns
        -------
        datetime

        """
        if text is None or text == '':
            return None
        if not isinstance(text, str):
            return text
        return dt.strptime(text, fmt)

    @staticmethod
    def to_datetimeformat(text):
        """
        Convertir une chaîne de caractères en format pour datetime

        Parameters
        ----------
        text : str
            Texte à convertir

        Returns
        -------
        Format pour datetime

        """
        if text is None or text == '':
            return None
        if not isinstance(text, str):
            return text
        return text.replace("Y", "%Y").replace("m", "%m").replace("d", "%d")\
            .replace("H", "%H").replace("M", "%M").replace("n", "\n")\
            .replace("%%", "%")

    @staticmethod
    def to_float(text):
        """
        Convertir une chaîne de caractères en un réel

        Parameters
        ----------
        text : str
            Texte à convertir

        Returns
        -------
        float

        """
        if text is None or text == '':
            return None
        if not isinstance(text, str):
            return text
        return float(text)

    @staticmethod
    def to_int(text):
        """
        Convertir une chaîne de caractères en un entier

        Parameters
        ----------
        text : str
            Texte à convertir

        Returns
        -------
        int

        """
        if text is None or text == '':
            return None
        if not isinstance(text, str):
            return text
        return int(float(text))

    @staticmethod
    def to_path(dirname, text):
        """
        Convertir une chaîne de caractères en chemin de fichier

        Parameters
        ----------
        dirname : str
            Répertoire du fichier
        text : str
            Texte à convertir

        Returns
        -------
        Chemin de fichier

        """
        if text is None or text == '':
            return None
        return os.path.join(dirname, text)
