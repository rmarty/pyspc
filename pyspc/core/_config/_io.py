#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs et convention de pyspc - Config - Import/Export
"""
import collections
import os.path
import pandas as pnd

import pyspc.core.exception as _exception


class IO():
    """
    Classe spécifique pour les Imports/Exports de pyspc.core.config.Config
    """
    def __init__(self):
        """
        Import/Export
        """

    @classmethod
    def from_csv(cls, filename=None, encoding='utf-8',
                 sectionname=None, sep=None):
        """
        Lire la configuration au format CSV

        Parameters
        ----------
        filename : str
            Fichier de configuration csv
        encoding : str
            Encodage du fichier de configuration 'utf-8' par défaut
        sectionname : str
            Nom de la colonne utilisée pour 'section'
        sep : str
            Séparateur, ';' par défaut

        Returns
        -------
        config : Config
            Instance Config

        """
        if filename is None or sectionname is None or \
                not os.path.exists(filename):
            return None
        if sep is None:
            sep = ';'
        config = cls(filename=filename)
        with open(filename, 'r', encoding=encoding) as f:
            header = f.readline().strip().split(sep)
            if sectionname not in header:
                return config
            idxsec = header.index(sectionname)
            for line in f.readlines():
                info = line.strip().split(sep)
                valsec = info[idxsec]
                config.setdefault(
                    valsec,
                    collections.OrderedDict()
                )
                for k, h in enumerate(header):
                    config[valsec].setdefault(h, info[k])
        return config

    def to_csv(self, filename=None, encoding='utf-8', sectionname=None,
               sep=None, date_format=None, float_format=None, sort_index=None):
        """
        Écrire la configuration au format CSV

        Parameters
        ----------
        filename : str
            Fichier de configuration csv
        encoding : str
            Encodage du fichier de configuration 'utf-8' par défaut
        sectionname : str
            Nom de la colonne utilisée par 'section'
            et à ajouter dans l'export. Par défaut: 'section'
        sep : str
            Séparateur, ';' par défaut
        date_format : str
            Format des dates. Par défaut: '%Y%m%d%H%M'
        float_format : str
            Format des réels
        sort_index : bool
            Trier l'index par ordre croissant. Défaut: False

        Returns
        -------
        filename : str
            Fichier de configuration csv

        See Also
        --------
        pandas.DataFrame.to_csv

        .. warning:: Ne préserve pas l'ordre des colonnes (options)

        """
        if sep is None:
            sep = ';'
        if date_format is None:
            date_format = '%Y%m%d%H%M'
        if sort_index is None:
            sort_index = False
        # Conversion en DataFrame
        df = pnd.DataFrame(self).transpose()
        df.index = df.index.to_flat_index()
        _exception.raise_valueerror(
            df.empty,
            "La configuration est vide. Elle ne peut donc pas être exportée "
            "en csv")
        if sort_index:
            df = df.sort_index()
        # Colonnes avec maintien de l'ordre
        columns = self.list_ordered_options()
        # Export
        if isinstance(sectionname, str):
            df.to_csv(
                filename, encoding=encoding, columns=columns, sep=sep,
                index=True, index_label=sectionname, date_format=date_format,
                float_format=float_format, line_terminator='\n')
        else:
            df.to_csv(
                filename, encoding=encoding, columns=columns, sep=sep,
                index=False, date_format=date_format,
                float_format=float_format, line_terminator='\n')
        return filename

    @classmethod
    def from_multitxt(cls, filenames=None, encoding='utf-8', sep=None):
        """
        Lire la configuration depuis plusieurs fichiers TXT

        Parameters
        ----------
        filenames : str, list, dict
            Fichier(s) de configuration texte
        encoding : str
            Encodage du fichier de configuration 'utf-8' par défaut
        sep : str
            Séparateur, '

        Returns
        -------
        config : Config
            Instance Config

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        if filenames is None:
            return None
        if sep is None:
            sep = '='
        if isinstance(filenames, str):
            filenames = [filenames]
        if isinstance(filenames, list):
            filenames = {os.path.splitext(os.path.basename(f))[0]: f
                         for f in filenames}
        if not isinstance(filenames, dict):
            return None
        # ---------------------------------------------------------------------
        # 1- Lecture
        # ---------------------------------------------------------------------
        config = cls(filename=filenames)
        for section, filename in filenames.items():
            config.setdefault(section, collections.OrderedDict())
            with open(filename, 'r', encoding=encoding) as f:
                for line in f.readlines():
                    if line.startswith('#'):
                        continue
                    line = line.strip().split(sep)
                    option = line[0]
                    val = sep.join(line[1:])
                    config[section].setdefault(option, val)
        return config

    def to_multitxt(self, dirname=None, encoding='utf-8', sep=None):
        """
        Ecrire la configuration dans plusieurs fichiers TXT

        Parameters
        ----------
        dirname : str
            Répertoire des fichier
        encoding : str
            Encodage du fichier de configuration 'utf-8' par défaut
        sep : str
            Séparateur, par défaut: '='

        Returns
        -------
        filenames : list
            Liste de fichiers de configuration texte

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        if dirname is None:
            return None
        if sep is None:
            sep = '='
        # ---------------------------------------------------------------------
        # 1- Ecriture
        # ---------------------------------------------------------------------
        filenames = []
        for section in self.keys():
            filename = os.path.join(dirname, '{}.txt'.format(section))
            with open(filename, 'w', encoding=encoding, newline='\n') as f:
                for option, value in self[section].items():
                    f.write(option)
                    f.write(sep)
                    f.write(value)
                    f.write('\n')
            filenames.append(filename)
        return filenames
