#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Fonctions spécifiques pour pyspc.core._series._plotting
"""
# from datetime import timedelta as td
import matplotlib.pyplot as mplt
import matplotlib.dates as mdates
import numpy as np

import pyspc.core.exception as _exception


def _create_figure(series=None, plottype=None, dpi=150):
    """
    Créer l'objet figure et les zones graphiques, par grandeur

    Parameters
    ----------
    series : pyspc.core.series.Series
        Collection de séries de données
    plottype : str
        Type de figure

    Other Parameters
    ----------------
    dpi : int
        Résolution de l'image

    Returns
    -------
    fig : matplotlib.figure.Figure
        Objet figure
    axes : dict
        Dictionnaire reliant les grandeurs (clés)
        aux zones graphiques (matplotlib.axes._axes.Axes)

    """
    # Création de la figure
    _exception.check_int(dpi)
    fig = mplt.figure(dpi=dpi)
    tests = {v: [x.startswith(v) for x in series.varnames]
             for v in ['H', 'T', 'Z']}
    # Création des axes
    axes = {}
    if plottype == 'event':
        axes['Q'] = fig.add_axes([0.10, 0.15, 0.80, 0.50])
        axes['P'] = fig.add_axes([0.10, 0.70, 0.80, 0.20])
        if any(tests['T']):
            axes['T'] = axes['P'].twinx()
    elif plottype == 'hydro_limni':
        axes['Q'] = fig.add_axes([0.11, 0.15, 0.78, 0.75])
        if any(tests['H']):
            axes['H'] = axes['Q'].twinx()
    elif plottype == 'reservoir':
        axes['Q'] = fig.add_axes([0.15, 0.15, 0.75, 0.35])
        level = None
        if any(tests['Z']):
            level = 'Z'
        elif any(tests['H']):
            level = 'H'
        if level is not None:
            axes[level] = fig.add_axes([0.15, 0.55, 0.75, 0.35])
    elif plottype == 'subreach':
        axes['H'] = fig.add_axes([0.07, 0.02, 0.90, 0.30])
        axes['Q'] = fig.add_axes([0.07, 0.34, 0.90, 0.30])
        axes['P'] = fig.add_axes([0.07, 0.66, 0.90, 0.30])
        fig.set_size_inches(8.27, 11.69)  # A4
    else:
        _exception.raise_valueerror(
            len({s.units for s in series.values()}) > 1,
            "La figure de type '{}' ne traite qu'une grandeur unique"
            "".format(plottype)
        )
        axes[series.varnames[0][0]] = fig.add_axes([0.10, 0.15, 0.75, 0.75])
    axes = {v: axes[v[0]] for v in series.varnames if v[0] in axes}
    return fig, axes


def _plot_serie(plottype=None, ax=None, serie=None, config=None,
                fill_threshold=None, fill_reverse=None, uncert=None):
    """
    Tracer une courbe/un histogramme

    Parameters
    ----------
    plottype : str
        Type de figure
    ax : matplotlib.axes._axes.Axes
        Zone graphique
    serie : pyspc.core.serie.Serie
        Série de données
    config : dict
        Configuration de la courbe

    Other Parameters
    ----------------
    fill_threshold : tuple
        Tuples (seuil, couleur)
    fill_reverse : bool
        Affichage du volume au-dessus (False) ou en-dessous du seuil.
        Par défaut: False
    uncert : Series
        Collection de séries représentant les incertitudes de la série

    Returns
    -------
    Line2D
        Courbe tracée

    """
    x = serie.data_frame.index
    y = serie.data_frame[(serie.code, serie.spc_varname)]
    if plottype == 'hyeto':
        _exception.raise_valueerror(
            serie.timestep is None,
            "La figure de type '{}' est incompatible avec des données "
            "instantanées".format(plottype)
        )
#        dbar = int(serie.timestep/td(days=1))
        dbar = 1
        obj = mplt.bar(x, y, -1 * dbar)
        _apply_setp_bar(obj, config)
    else:
        obj = ax.plot_date(x, y)
        _apply_setp_plot_date(obj, config)
        # AJOUT DES INTERVALLES D'INCERTITUDE
        try:
            c = obj[0].get_color()
            ukeys = list(uncert.keys())
            ucases = [(k, len(ukeys)-k-1)
                      for k, x in enumerate(ukeys)
                      if k < len(ukeys)-k-1]
            x = len(ucases)
            for k, uc in enumerate(ucases):
                a = float(k+1) / float(x+1)
                sinf = uncert[ukeys[uc[0]]]
                ssup = uncert[ukeys[uc[1]]]
                ax.fill_between(
                    sinf.data_frame.index,
                    sinf.data_frame[(sinf.code, sinf.spc_varname)],
                    ssup.data_frame[(ssup.code, ssup.spc_varname)],
                    color=c,
                    alpha=a,
                    linewidth=0.5
                )
        except AttributeError:
            pass
        # AJOUT DES ZONES SUP-SEUIL / SUB-SEUIL
        try:
            for tc in sorted(fill_threshold, reverse=fill_reverse):
                t = tc[0]
                c = tc[-1]
                if not fill_reverse:
                    ax.fill_between(x, y, [t] * len(x), where=y >= t, color=c)
                else:
                    ax.fill_between(x, y, [t] * len(x), where=y <= t, color=c)
        except (TypeError, AttributeError):
            pass
    return obj


def _apply_setp_bar(obj, config):
    """Appliquer matplotlib.setp pour bar"""
    mplt.setp(obj, 'color', [1.0, 1.0, 1.0, 0.0])
    for k, v in config.items():
        try:
            if k in ['marker', 'markersize', 'linestyle']:
                pass
            elif k.endswith('color'):
                if 'face' in k:
                    mplt.setp(obj, 'color', v)
                else:
                    mplt.setp(obj, 'edgecolor', v)
            else:
                mplt.setp(obj, k, v)
        except AttributeError:
            pass
        except TypeError:
            pass


def _apply_setp_plot_date(obj, config):
    """Appliquer matplotlib.setp pour plot_date"""
    for k, v in config.items():
        try:
            mplt.setp(obj, k, v)
        except AttributeError:
            pass
        except TypeError:
            pass


def _set_legend(axes=None, plottype=None, config=None):
    """
    Tracer et Configurer la légende - 1 légende par zone graphique

    Parameters
    ----------
    axes : dict
        Dictionnaire reliant les grandeurs (clés)
        aux zones graphiques (matplotlib.axes._axes.Axes)
    plottype : str
        Type de figure
    config : dict
        Configuration de la figure

    """
    loc = config['legend']
    ncol = config['legendcol']
    lfontsize = config['lfontsize']
    tmp_axes = {k[0].upper(): v for k, v in axes.items()}
    if plottype == 'event':
        lines, labels = tmp_axes['P'].get_legend_handles_labels()
        try:
            lines2, labels2 = tmp_axes['T'].get_legend_handles_labels()
        except KeyError:
            tmp_axes['P'].legend(lines, labels,
                                 loc=loc, ncol=ncol, fontsize=lfontsize)
        else:
            tmp_axes['P'].legend(lines + lines2, labels + labels2,
                                 loc=loc, ncol=ncol, fontsize=lfontsize)
        tmp_axes['Q'].legend(loc=loc, ncol=ncol, fontsize=lfontsize)
    elif plottype == 'hydro_limni':
        k = ['Q', 'H']
        try:
            lines, labels = tmp_axes['Q'].get_legend_handles_labels()
        except KeyError:
            lines, labels = [], []
            k.remove('Q')
        try:
            lines2, labels2 = tmp_axes['H'].get_legend_handles_labels()
        except KeyError:
            lines2, labels2 = [], []
            k.remove('H')
        try:
            tmp_axes[k[0]].legend(lines + lines2, labels + labels2,
                                  loc=loc, ncol=ncol, fontsize=lfontsize)
        except IndexError:
            pass
    elif plottype == 'hyeto':
        v = next(iter(tmp_axes.keys()))
        handles, labels = tmp_axes[v].get_legend_handles_labels()
        lh = {l: h for h, l in zip(handles, labels)}
        tmp_axes[v].legend(
            lh.values(), lh.keys(),
            loc=loc, ncol=ncol, fontsize=lfontsize)
    else:
        for ax in tmp_axes.values():
            ax.legend(loc=loc, ncol=ncol, fontsize=lfontsize)


def _set_legend_bottom(axes=None, plottype=None, config=None):
    """
    Tracer et Configurer la légende - 1 légende par image (en bas)

    Parameters
    ----------
    axes : dict
        Dictionnaire reliant les grandeurs (clés)
        aux zones graphiques (matplotlib.axes._axes.Axes)
    plottype : str
        Type de figure
    config : dict
        Configuration de la figure

    """
    dy = config['legendbottom']
    bbox = config['legendbox']
    loc = config['legend']
    ncol = config['legendcol']
    lfontsize = config['lfontsize']
    tmp_axes = {k[0].upper(): v for k, v in axes.items()}
    lines = [x for ax in tmp_axes.values()
             for x in ax.get_legend_handles_labels()[0]]
    labels = [x for ax in tmp_axes.values()
              for x in ax.get_legend_handles_labels()[1]]
    if plottype == 'event':
        box_p = tmp_axes['P'].get_position()
        box_q = tmp_axes['Q'].get_position()
        dy_pq = box_p.y0 - box_q.y1
        tmp_axes['P'].set_position([box_p.x0, box_p.y0 + box_p.height * dy,
                                    box_p.width, box_p.height * (1-dy)])
        box_p = tmp_axes['P'].get_position()
        tmp_axes['Q'].set_position(
            [box_q.x0, box_p.y0-dy_pq-box_q.height * (1-dy),
             box_q.width, box_q.height * (1-dy)])
        tmp_axes['Q'].legend(
            lines, labels,
            bbox_to_anchor=bbox,
            loc=loc, ncol=ncol, fontsize=lfontsize)
        return None
    if plottype == 'hydro':
        v = next(iter(tmp_axes.keys()))
        box = tmp_axes[v].get_position()
        tmp_axes[v].set_position(
            [box.x0, box.y0 + box.height * dy,
             box.width, box.height * (1-dy)])
        tmp_axes[v].legend(
            bbox_to_anchor=bbox, loc=loc, ncol=ncol, fontsize=lfontsize)
        return None
    if plottype == 'hydro_limni':
        box = tmp_axes['Q'].get_position()
        tmp_axes['H'].set_position([box.x0, box.y0 + box.height * dy,
                                    box.width, box.height * (1-dy)])
        # Put a legend below current axis
        tmp_axes['H'].legend(
            lines, labels,
            bbox_to_anchor=bbox,
            loc=loc, ncol=ncol, fontsize=lfontsize)
        return None
    if plottype == 'hyeto':
        v = next(iter(tmp_axes.keys()))
        handles, labels = tmp_axes[v].get_legend_handles_labels()
        lh = {l: h for h, l in zip(handles, labels)}
        box = tmp_axes[v].get_position()
        tmp_axes[v].set_position(
            [box.x0, box.y0 + box.height * dy,
             box.width, box.height * (1-dy)])
        tmp_axes[v].legend(
            lh.values(), lh.keys(),
            bbox_to_anchor=bbox, loc=loc, ncol=ncol, fontsize=lfontsize)
        return None
    if plottype == 'reservoir':
        if 'Z' in tmp_axes:
            level = 'Z'
        elif 'H' in tmp_axes:
            level = 'H'
        else:
            _exception.Warning(
                None, msg='Impossible de définir la légende sous le graphe')
            return _set_legend(axes=axes, plottype=plottype, config=config)
        box_z = tmp_axes[level].get_position()
        box_q = tmp_axes['Q'].get_position()
        dy_zq = box_z.y0 - box_q.y1
        tmp_axes[level].set_position(
            [box_z.x0, box_z.y0 + box_z.height * dy,
             box_z.width, box_z.height * (1-dy)])
        box_z = tmp_axes[level].get_position()
        tmp_axes['Q'].set_position(
            [box_q.x0, box_z.y0 - dy_zq - box_q.height * (1-dy),
             box_q.width, box_q.height * (1-dy)])
        tmp_axes['Q'].legend(
            lines, labels,
            bbox_to_anchor=bbox,
            loc=loc, ncol=ncol, fontsize=lfontsize)
        return None
    raise NotImplementedError


def _set_xaxis(axes=None, axb=None, config=None):
    """
    Configurer les y-axis

    Parameters
    ----------
    axes : dict
        Dictionnaire reliant les grandeurs (clés)
        aux zones graphiques (matplotlib.axes._axes.Axes)
    axb : matplotlib.axes._axes.Axes
        Zone graphique inférieure
    config : dict
        Configuration de la figure

    """
    # limites
    try:
        dt1 = config['xlim'][0]
        dt2 = config['xlim'][-1]
    except (KeyError, IndexError):
        xlim = None
    else:
        _exception.check_dt(dt1)
        _exception.check_dt(dt2)
        xlim = (mdates.date2num(dt1), mdates.date2num(dt2))
    # titre
    axb.set_xlabel(config['xtitle'], fontsize=config['xfontsize']+2)
    axb.tick_params(axis='x', labelsize=config['xfontsize'])
    for ax in axes.values():
        # limites
        if xlim is not None:
            ax.set_xlim(xlim)
        # pour définir les marques majeures/mineures et le format des dates
        _set_xaxis_locator(ax=ax, xminor=config['xminor'], xfmt=config['xfmt'])
        # pour enlever les labels entre les 2 zones
#        if ax != axb:  # ne fonctionne pas dans le cas hydro_limni
        if ax.get_position().y0 != axb.get_position().y0:
            ax.set_xticklabels('')


def _set_xaxis_locator(ax=None, xminor=None, xfmt=None):
    """
    Définir les dates de l'axe X

    Parameters
    ----------
    ax : matplotlib.axes._axes.Axes
        Zone graphique
    xminor : tuple
        Position des marques mineures (début, fin, pas)
    xfmt : str
        Format d'affichage des dates

    """
    # Positionnement des marques majeures : 8 maximum
    ax.xaxis.set_major_locator(mdates.DayLocator())
    kdl = 0
    while True:
        kdl += 1
        ax.xaxis.set_major_locator(mdates.DayLocator(interval=kdl))
        if len(ax.xaxis.get_major_ticks()) <= 8:
            break
    # Positionnement des marques mineures
    ax.xaxis.set_minor_locator(
        mdates.HourLocator(
            np.arange(int(xminor[0]), int(xminor[1]), int(xminor[2]))))
    # Format des dates
    ax.xaxis.set_major_formatter(mdates.DateFormatter(xfmt))


def _set_yaxis(axes=None, plottype=None, config=None):
    """
    Configurer les y-axis

    Parameters
    ----------
    axes : dict
        Dictionnaire reliant les grandeurs (clés)
        aux zones graphiques (matplotlib.axes._axes.Axes)
    plottype : str
        Type de figure
    config : dict
        Configuration de la figure

    """
    do_invert = False
    for v, ax in axes.items():
        # titre
        ky = 'ytitle' + v[0].lower()
        ax.set_ylabel(config.get(ky, ''), fontsize=config['yfontsize'])
        ax.tick_params(axis='y', labelsize=config['yfontsize'])
        # limites
        ky = 'ylim' + v[0].lower()
        try:
            ylim = (config[ky][0], config[ky][-1])
        except (KeyError, IndexError):
            pass
        else:
            ax.set_ylim(ylim)
        # Configuration spécifique : 'event' P -> axe inversé
        if plottype == 'event' and v[0].lower() == 'p' and not do_invert:
            ax.invert_yaxis()
            do_invert = True
