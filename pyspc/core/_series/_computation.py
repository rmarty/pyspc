#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Series - Méthodes spécifiques - Calculs
"""
import collections
import os.path
import matplotlib.pyplot as mplt
import pandas as pnd

import pyspc.core.config as _config
import pyspc.core.exception as _exception
from pyspc.core.keyseries import tuple2str
# from pyspc.plotting.regime import plot_regime
import pyspc.plotting.regime as pr


class Computation():
    """
    Classe spécifique pour les calculs à partir de pyspc.core.series.Series
    """
    def __init__(self):
        """
        Calculs
        """
        self.datatype = None
        self.name = None

    def above_threshold(self, threshold=None, inplace=False):
        """
        Pour chacune des séries:
        Remplacer des valeurs inférieures au seuil <threshold>
        par la valeur manquante
        Si l'option <inplace> est définie à False,
        alors cette méthode crée une nouvelle instance <Series>

        Parameters
        ----------
        threshold : int, float, dict
            Seuil à appliquer
        inplace : bool
            Traiter en place (True) ou renvoyer une nouvelle instance (False)
            défaut: False

        Returns
        -------
        series : Series
            Nouvele collection de séries (si inplace:False)

        """
        threshold = self._arg2dict(threshold)
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in self.keys():
            s = self[k].above_threshold(threshold[k], inplace=inplace)
            if s is None:
                continue
            try:
                series.add(code=k[0], serie=s, meta=k[2])
            except AttributeError:
                pass
        return series

    def below_threshold(self, threshold=None, inplace=False):
        """
        Pour chacune des séries:
        Remplacer les valeurs supérieures au seuil <threshold>
        par la valeur manquante
        Si l'option <inplace> est définie à False,
        alors cette méthode crée une nouvelle instance <Series>

        Parameters
        ----------
        threshold : int, float
            Seuil à appliquer
        inplace : bool
            Traiter en place (True) ou renvoyer une nouvelle instance (False)
            défaut: False

        Returns
        -------
        series : Series
            Nouvele collection de séries (si inplace:False)

        """
        threshold = self._arg2dict(threshold)
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in self.keys():
            s = self[k].below_threshold(threshold[k], inplace=inplace)
            if s is None:
                continue
            try:
                series.add(code=k[0], serie=s, meta=k[2])
            except AttributeError:
                pass
        return series

    def counter_missing(self, asconfig=False, codefrom='series'):
        """
        Pour chacune des séries:
        Déterminer le nombre de valeurs manquantes

        Parameters
        ----------
        asconfig : bool
            Renvoyer une instance Config
        codefrom : str
            Codification depuis
            - 'serie'  : code défini par l'attribut code de l'objet Serie
            - 'series' : code défini par la clé dans la collections Series
            Valeur par défaut.

        Returns
        -------
        pyspc.core.config.Config, dict
            Informations sur les valeurs manquantes

        """
        if asconfig:
            cfg = _config.Config()
            for k in self.keys():
                counter = self[k].counter_missing()
                if codefrom == 'serie':
                    c = self[k].code
                else:
                    c = k
                cfg.setdefault(c, collections.OrderedDict())
                cfg[c]['code'] = c
                cfg[c]['firstdt'] = self[k].firstdt
                cfg[c]['lastdt'] = self[k].lastdt
                cfg[c]['timestep'] = self[k].timestep
                cfg[c]['length'] = self[k].length
                cfg[c]['length valid'] = self[k].length - counter
                cfg[c]['missing'] = counter
                cfg[c]['length valid (%)'] = int(
                    100. * cfg[c]['length valid'] / cfg[c]['length'])
                cfg[c]['missing (%)'] = int(
                    100. * cfg[c]['missing'] / cfg[c]['length'])
            return cfg
        if codefrom == 'series':
            return {k: self[k].counter_missing() for k in self.keys()}
        if codefrom == 'serie':
            return {self[k].code: self[k].counter_missing()
                    for k in self.keys()}
        return None

    def cumsum(self, skipna=True, inplace=False):
        """
        Pour chacune des séries:
        Série des sommes cumulées

        Parameters
        ----------
        skipna : bool
            Ignorer les valeurs manquantes? défaut: True
        inplace : bool
            Traiter en place (True) ou renvoyer une nouvelle instance (False)
            défaut: False

        Returns
        -------
        series : Series
            Nouvele collection de séries (si inplace:False)

        """
        skipna = self._arg2dict(skipna)
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in self.keys():
            s = self[k].cumsum(skipna=skipna[k])
            try:
                series.add(code=k[0], serie=s, meta=k[2])
            except AttributeError:
                self.add(code=k[0], serie=s, meta=k[2])
        return series

    def events(self, threshold=None, engine='basic',
               prominence=None, width=None,
               before=None, after=None, filename=None,
               asconfig=False, codefrom='series'):
        """
        Pour chacune des séries:
        Sélectionner les événements définis par un seuil


        Parameters
        ----------
        threshold : float or dict
            Valeur du seuil
        engine : str or dict
            Méthode de calcul
            parmi ['basic', 'scipy']
            Par défaut: 'basic'
            Si 'scipy': utilise scipy.find_peaks()
        prominence : float or dict
            Paramètre pour scipy
        width : float or dict
            Paramètre pour scipy
        before : float or dict
            Paramètre pour scipy
        after : float or dict
            Paramètre pour scipy
        filename : str or dict
            Paramètre pour scipy
        asconfig : bool
            Renvoyer une instance Config
        codefrom : str
            Codification depuis
            - 'serie'  : code défini par l'attribut code de l'objet Serie
            - 'series' : code défini par la clé dans la collections Series
            Valeur par défaut.

        Returns
        -------
        pyspc.core.config.Config, dict
            - Dictionnaire des événements
        clé : datetime
            jour du max
        valeur : dict
            Informations sur l'événement
            - 'start' : date de début
            - 'end'   : date de fin
            - 'dtmax' : date de la valeur maximale
            - 'max'   : valeur maximale
            - 'name'  : nom de l'événement (jour du max en str)
            - None sinon

        .. seealso:: Voir la documentation de Serie.events

        """
        threshold = self._arg2dict(threshold)
        engine = self._arg2dict(engine)
        prominence = self._arg2dict(prominence)
        width = self._arg2dict(width)
        before = self._arg2dict(before)
        after = self._arg2dict(after)
        filename = self._arg2dict(filename)
        if asconfig:
            cfg = _config.Config()
            for k in self.keys():
                if codefrom == 'serie':
                    c = self[k].code
                else:
                    c = k
                events = self[k].events(
                    threshold=threshold[k],
                    engine=engine[k],
                    prominence=prominence[k],
                    width=width[k],
                    before=before[k],
                    after=after[k],
                    filename=filename[k]
                )
                for e in events:
                    key = (c, e)
                    cfg.setdefault(key, {})
                    cfg[key]['code'] = c
                    cfg[key]['name'] = events[e]['name']
                    cfg[key]['firstdt'] = events[e]['firstdt']
                    cfg[key]['lastdt'] = events[e]['lastdt']
                    cfg[key]['maxdt'] = events[e]['maxdt']
                    cfg[key]['max'] = events[e]['max']
                    cfg[key]['massdt'] = events[e]['massdt']
            return cfg
        if codefrom == 'series':
            return {k: self[k].events(
                        threshold=threshold[k],
                        engine=engine[k],
                        prominence=prominence[k],
                        width=width[k],
                        before=before[k],
                        after=after[k],
                        filename=filename[k]
                    )
                    for k in self.keys()}
        if codefrom == 'serie':
            return {self[k].code: self[k].events(
                        threshold=threshold[k],
                        engine=engine[k],
                        prominence=prominence[k],
                        width=width[k],
                        before=before[k],
                        after=after[k],
                        filename=filename[k]
                    )
                    for k in self.keys()}
        return None

    def find_annual_max(self, asconfig=False, codefrom='series'):
        """
        Pour chacune des séries:
        Lister les maximas annuels

        Année *hydrologique* : 01/09/aaaa-1 - 31/08/aaaa

        Parameters
        ----------
        asconfig : bool
            Renvoyer une instance Config
        codefrom : str
            Codification depuis
            - 'serie'  : code défini par l'attribut code de l'objet Serie
            - 'series' : code défini par la clé dans la collections Series
            Valeur par défaut.

        Returns
        -------
        pyspc.core.config.Config, dict
            - clé       : année
            - valeur    : dictionnaire
                - max   : valeur du max
                - maxdt : instant du max
                - pmv   : %age de valeurs manquantes dans l'année

        """
        if asconfig:
            cfg = _config.Config()
            for k in self.keys():
                maxis = self[k].find_annual_max()
                if codefrom == 'serie':
                    c = self[k].code
                else:
                    c = k
                cfg.setdefault(c, collections.OrderedDict())
                cfg[c]['code'] = c
                cfg[c]['firstdt'] = self[k].firstdt
                cfg[c]['lastdt'] = self[k].lastdt
                cfg[c]['timestep'] = self[k].timestep
                cfg[c]['length'] = self[k].length
                for m, v in maxis.items():
                    for kv, vv in v.items():
                        km = '{}_{}'.format(m, kv)
                        cfg[c][km] = vv
            return cfg
        if codefrom == 'series':
            return {k: self[k].find_annual_max() for k in self.keys()}
        if codefrom == 'serie':
            return {self[k].code: self[k].find_annual_max()
                    for k in self.keys()}
        return None

    def find_annual_min(self, asconfig=False, codefrom='series'):
        """
        Pour chacune des séries:
        Lister les minimas annuels

        Année *civile* : 01/01/aaaa - 31/12/aaaa

        Parameters
        ----------
        asconfig : bool
            Renvoyer une instance Config
        codefrom : str
            Codification depuis
            - 'serie'  : code défini par l'attribut code de l'objet Serie
            - 'series' : code défini par la clé dans la collections Series
            Valeur par défaut.

        Returns
        -------
        pyspc.core.config.Config, dict
            - clé       : année
            - valeur    : dictionnaire
                - min   : valeur du min
                - mindt : instant du min
                - pmv   : %age de valeurs manquantes dans l'année

        """
        if asconfig:
            cfg = _config.Config()
            for k in self.keys():
                maxis = self[k].find_annual_min()
                if codefrom == 'serie':
                    c = self[k].code
                else:
                    c = k
                cfg.setdefault(c, collections.OrderedDict())
                cfg[c]['code'] = c
                cfg[c]['firstdt'] = self[k].firstdt
                cfg[c]['lastdt'] = self[k].lastdt
                cfg[c]['timestep'] = self[k].timestep
                cfg[c]['length'] = self[k].length
                for m, v in maxis.items():
                    for kv, vv in v.items():
                        km = '{}_{}'.format(m, kv)
                        cfg[c][km] = vv
            return cfg
        if codefrom == 'series':
            return {k: self[k].find_annual_min() for k in self.keys()}
        if codefrom == 'serie':
            return {self[k].code: self[k].find_annual_min()
                    for k in self.keys()}
        return None

    def max(self, asconfig=False, codefrom='series'):
        """
        Pour chacune des séries:
        Renvoyer le maximum de la série et la date associée

        Parameters
        ----------
        asconfig : bool
            Renvoyer une instance Config
        codefrom : str
            Codification depuis
            - 'serie'  : code défini par l'attribut code de l'objet Serie
            - 'series' : code défini par la clé dans la collections Series
            Valeur par défaut.

        Returns
        -------
        pyspc.core.config.Config, dict
            + clé       : clé de la série dans la collection
            + valeur    : (maximum de la série, date associée)

        """
        if asconfig:
            cfg = _config.Config()
            for k in self.keys():
                if codefrom == 'serie':
                    c = self[k].code
                else:
                    c = k
                cfg.setdefault(c, collections.OrderedDict())
                cfg[c]['code'] = c
                cfg[c]['firstdt'] = self[k].firstdt
                cfg[c]['lastdt'] = self[k].lastdt
                cfg[c]['timestep'] = self[k].timestep
                cfg[c]['length'] = self[k].length
                cfg[c]['max'], cfg[c]['maxdt'] = self[k].max()
            return cfg
        if codefrom == 'series':
            return {k: self[k].max() for k in self.keys()}
        if codefrom == 'serie':
            return {self[k].code: self[k].max() for k in self.keys()}
        return None

    def min(self, asconfig=False, codefrom='series'):
        """
        Pour chacune des séries:
        Renvoyer le minimum de la série et la date associée

        Parameters
        ----------
        asconfig : bool
            Renvoyer une instance Config
        codefrom : str
            Codification depuis
            - 'serie'  : code défini par l'attribut code de l'objet Serie
            - 'series' : code défini par la clé dans la collections Series
            Valeur par défaut.

        Returns
        -------
        pyspc.core.config.Config, dict
            + clé       : clé de la série dans la collection
            + valeur    : (minimum de la série, date associée)

        """
        if asconfig:
            cfg = _config.Config()
            for k in self.keys():
                if codefrom == 'serie':
                    c = self[k].code
                else:
                    c = k
                cfg.setdefault(c, collections.OrderedDict())
                cfg[c]['code'] = c
                cfg[c]['firstdt'] = self[k].firstdt
                cfg[c]['lastdt'] = self[k].lastdt
                cfg[c]['timestep'] = self[k].timestep
                cfg[c]['length'] = self[k].length
                cfg[c]['min'], cfg[c]['mindt'] = self[k].min()
            return cfg
        if codefrom == 'series':
            return {k: self[k].min() for k in self.keys()}
        if codefrom == 'serie':
            return {self[k].code: self[k].min() for k in self.keys()}
        return None

    def regime(self, groupby=None, freqs=None, dayhour=6, strict=True,
               ignore_upscale=False, dirname=None, boxplot=False, fill=False):
        """
        Pour chacune des séries:
        Calculer le régime de la chronique

        Parameters
        ----------
        groupby : str
            Regroupement des données
            - 'dayofyear' : par jour de l'année (défaut)
            - 'month' : par mois
        freqs : list
            Fréquences des quantiles, entre 0 et 1.
            Par défaut: [0.10, 0.25, 0.50, 0.75, 0.90]
        dayhour : int
            Heure du début de la journée Nécessaire si la cible est un pas de
            temps journalier. Par défaut: 6. Valeurs admises: [0, 6]
        strict : bool
            Calcul strict, imposant np.nan dans la valeur agrégée si au moins
            un pas de temps initial vaut np.nan. Retire de la série les pas
            de temps où les données d'origine ne sont pas complètes
            Défaut: True
        ignore_upscale : bool
            Ignorer la mise à l'échelle. Défaut: False.
            Si le paramètre est cumulable, ignore_upscale est mis à False

        Other Parameters
        ----------------
        dirname : str, None
            Répertoire des exports. Si non défini, les régimes hydrologiques
            sont directement renvoyés sous la forme de tuples
            - dictionnaire de pandas.DataFrame,
            - dictionnaire de matplotlib.pyplot.figure
        boxplot : bool
            Tracer les quantiles sous forme de boxplot (True)
            ou d'enveloppes quantiliques (False).
            Par défaut: False.
        fill : bool
            Remplir les boxplots/enveloppes quantiliques.
            Par défaut: False

        Returns
        -------
        - si dirname n'est pas défini
            regimes : dict
                Tableaux du régime hydrologique contenant les minimas, maximas,
                moyennes et les quantiles associées aux fréquences spécifiées
            figs : dict, None
                Images matplotlib des régimes hydrologiques

        - si dirname est défini
            csv_filenames : dict
                Fichiers csv exportés, dont le nom générique est
                keyserie_regime-groupby.csv
            png_filenames : dict
                Fichiers png exportés, dont le nom générique est
                keyserie_regime-groupby.png

        Notes
        -----
        Les clés des dictionnaires renvoyés correspondent
        aux clés des séries de la collection

        See Also
        --------
        pyspc.core.serie.Serie.regime

        """
        # -------------------------------------------------------------------
        # 1- Calcul des régimes hydrologiques
        # -------------------------------------------------------------------
        regimes = {k: v.regime(groupby=groupby, freqs=freqs, dayhour=dayhour,
                               strict=strict, ignore_upscale=ignore_upscale)
                   for k, v in self.items()}
        figs = {k: pr.plot_regime(data=v, boxplot=boxplot, fill=fill)
                for k, v in regimes.items()}
        if dirname is None:
            return regimes, figs
        # -------------------------------------------------------------------
        # X- Exports CSV
        # -------------------------------------------------------------------
        _exception.check_str(dirname)
        csv_filenames = {}
        png_filenames = {}
        for k, v in regimes.items():
            b = os.path.join(
                dirname, "{0}_regime-{1}".format(tuple2str(k), groupby))
            fcsv = b + '.csv'
            fpng = b + '.png'
            if isinstance(v, pnd.DataFrame):
                v.to_csv(fcsv, sep=';', float_format='%.3f')
                csv_filenames[k] = fcsv
                fig = figs[k]
                mplt.figure(fig.number)  # set current fig
                mplt.savefig(fpng, dpi=300)
                mplt.close(fig)
                png_filenames[k] = fpng
        return csv_filenames, png_filenames

    def timecentroid(self, asconfig=False, codefrom='series'):
        """
        Pour chacune des séries:
        Déterminer le centre de masse temporel de la série

        Parameters
        ----------
        asconfig : bool
            Renvoyer une instance Config
        codefrom : str
            Codification depuis
            - 'serie'  : code défini par l'attribut code de l'objet Serie
            - 'series' : code défini par la clé dans la collections Series
            Valeur par défaut.

        Returns
        -------
        pyspc.core.config.Config, dict
            + clé       : clé de la série dans la collection
            + valeur    : date du centre de masse

        See Also
        --------
        pyspc.core.serie.Serie.timecentroid

        """
        if asconfig:
            cfg = _config.Config()
            for k in self.keys():
                if codefrom == 'serie':
                    c = self[k].code
                else:
                    c = k
                cfg.setdefault(c, collections.OrderedDict())
                cfg[c]['code'] = c
                cfg[c]['firstdt'] = self[k].firstdt
                cfg[c]['lastdt'] = self[k].lastdt
                cfg[c]['timestep'] = self[k].timestep
                cfg[c]['length'] = self[k].length
                cfg[c]['massdt'] = self[k].timecentroid()
            return cfg
        if codefrom == 'series':
            return {k: self[k].timecentroid() for k in self.keys()}
        if codefrom == 'serie':
            return {self[k].code: self[k].timecentroid() for k in self.keys()}
        return None
