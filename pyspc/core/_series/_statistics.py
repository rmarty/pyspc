#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Series - Méthodes spécifiques - Calculs stat.
"""
import collections
import numpy as np
import pandas as pnd

import pyspc.core.config as _config
from pyspc.core.keyseries import tuple2str, str2tuple
import pyspc.core.exception as _exception


class Statistics():
    """
    Classe spécifique pour les stats à partir de pyspc.core.series.Series
    """
    def __init__(self):
        """
        Calculs
        """
        self.datatype = None
        self.name = None

    def describe(self, freqs=None, asconfig=False, bydate=False,
                 codefrom='series'):
        """
        Déterminer les statistiques descriptives de chaque série

        Parameters
        ----------
        freqs : list
            Liste des fréquences, de 0 à 100
        asconfig : bool
            Renvoyer une instance Config
            Ignoré si byrow est True
        bydate : bool
            Réaliser date par date
            Par défaut: False.
            Si True, les séries sont concaténées
            et les statistiques sont renvoyées
            en tant qu'instance Series
        codefrom : str
            Codification. Ignoré si byrow est True
            - 'serie'  : code défini par l'attribut code de l'objet Serie
            - 'series' : code défini par la clé dans la collections Series
            Valeur par défaut.

        Returns
        -------
        pyspc.core.config.Config, dict
            - si bydate:False et asconfig:False : dictionnaire des stats
            - si bydate:False et asconfig:True : instance Config des stats
            - si bydate:True : instance Séries

        """
        # -----------------------------------------------
        #    ETAPE 0 : Contrôles
        # -----------------------------------------------
        self.refresh()
        if freqs is None:
            freqs = [10, 50, 90]
        # -----------------------------------------------
        #    CAS 1 : Statistiques par date
        # -----------------------------------------------
        if bydate:
            return self.describe_bydate(freqs=freqs)
        # -----------------------------------------------
        #    CAS 2 : Statistiques par série - Config
        # -----------------------------------------------
        if asconfig:
            return self.describe_asconfig(freqs=freqs, codefrom=codefrom)
        # -----------------------------------------------
        #    CAS 3 : Statistiques par série - Dictionnaire
        # -----------------------------------------------
        if codefrom == 'series':
            return {k: self[k].describe(freqs=freqs)
                    for k in self.keys()}
        if codefrom == 'serie':
            return {self[k].code: self[k].describe(freqs=freqs)
                    for k in self.keys()}
        return None

    def describe_asconfig(self, freqs=None, codefrom='series'):
        """
        Déterminer les statistiques descriptives de chaque série

        Parameters
        ----------
        freqs : list
            Liste des fréquences, de 0 à 100
        codefrom : str
            Codification. Ignoré si byrow est True
            - 'serie'  : code défini par l'attribut code de l'objet Serie
            - 'series' : code défini par la clé dans la collections Series
            Valeur par défaut.

        Returns
        -------
        pyspc.core.config.Config
            Statistiques descriptives

        """
        # -----------------------------------------------
        #    ETAPE 0 : Contrôles
        # -----------------------------------------------
        self.refresh()
        if freqs is None:
            freqs = [10, 50, 90]
        # -----------------------------------------------
        #    CAS 2 : Statistiques par série - Config
        # -----------------------------------------------
        cfg = _config.Config()
        for k in self.keys():
            dsc = self[k].describe(freqs=freqs)
            if codefrom == 'serie':
                c = self[k].code
            else:
                c = k
            cfg.setdefault(c, collections.OrderedDict())
            for i in dsc.index:
                cfg[c].setdefault(
                    i, dsc[(self[k].code, self[k].spc_varname)].loc[i])
        return cfg

    def describe_bydate(self, freqs=None):
        """
        Déterminer les statistiques descriptives de chaque série

        Parameters
        ----------
        freqs : list
            Liste des fréquences, de 0 à 100

        Returns
        -------
        pyspc.core.series.Series
            Statistiques descriptives

        """
        # -----------------------------------------------
        #    ETAPE 0 : Contrôles
        # -----------------------------------------------
        self.refresh()
        if freqs is None:
            freqs = [10, 50, 90]
        # -----------------------------------------------
        #    CAS 1 : Statistiques par date
        # -----------------------------------------------
        _exception.raise_valueerror(
            not self.check_series(),
            "Au moins une série n'est pas une instance Serie"
        )
        _exception.raise_valueerror(
            not self.check_unique_varname(),
            'La collection contient plusieurs variables. '
            'Aucun calcul réalisé.'
        )
        _exception.check_listlike(freqs)
        _exception.raise_valueerror(
            self.datatype == 'fcst' and not self.check_unique_runtime(),
            'La collection contient plusieurs instants de prévision. '
            'Aucun calcul réalisé.'
        )
        _exception.raise_valueerror(
            self.datatype == 'fcst' and not self.check_unique_code(),
            'La collection correspond à plusieurs codes. '
            'Aucun calcul réalisé.'
        )
        # -----------------------------------------------
        #    CAS 1.1 : Statistiques
        # -----------------------------------------------
        freqs = [f/100 if f > 1 else f for f in freqs]
        df = self.concat()
        dsc = df.apply(pnd.DataFrame.describe, axis=1, percentiles=freqs)
        # -----------------------------------------------
        #    CAS 1.2 : Instance Series
        # -----------------------------------------------
        series = self.__class__(
            datatype=self.datatype, name=self.name + '_describe')
        for c in dsc.columns:
            code = '{}_{}'.format(self.name, c)
            serie = self._constructor_expanddim(
                dsc[c].to_frame(),
                code=code,
                varname=self.varnames[0]
            )
            if self.datatype == 'obs':
                series.add(
                    code=code,
                    serie=serie
                )
            elif self.datatype == 'fcst':
                runtime = [k[2][0] for k in self.keys()][0]
                serie.code = self.codes[0]
                series.add(
                    code=self.codes[0],
                    serie=serie,
                    meta=(runtime, code, None)
                )
        return series

    def percentile(self, keys=None, freqs=None, skipna=True):
        """
        Déterminer les percentiles

        Parameters
        ----------
        keys : list
            Clés des séries à considérer
            Si non renseigné, la méthode s'applique
            à toutes les séries de la collection
        freqs : list
            Liste des fréquences, de 0 à 100
        skipna : bool
            Ignorer les NaN ?
            - False : utiliser numpy.npercentile
            - True  : utiliser numpy.nanpercentile

        Returns
        -------
        series : Series
            Nouvele collection de séries

        Notes
        -----
        Le nommage des séries dépend de la nature de la collection

        - obs : ('NomDeLaCollection-freq', varname, None)
        - fcst, avec modèle unique : (station, varname, (runtime, model, 'percentile', freq))
        - fcst, avec multi-modèle : (station, varname, (runtime, 'NomDeLaCollection', 'percentile', freq))

        """
        # -----------------------------------------------
        #    ETAPE 0 : Contrôles
        # -----------------------------------------------
        self.refresh()
        freqs = _exception.set_default(freqs, default=[10, 50, 90])
        _exception.raise_valueerror(
            not self.check_series,
            "Au moins une série n'est pas une instance Serie"
        )
        _exception.raise_valueerror(
            not self.check_unique_varname(),
            'La collection contient plusieurs variables. Aucun calcul réalisé.'
        )
        _exception.check_listlike(freqs)
        _exception.raise_valueerror(
            self.datatype == 'fcst' and not self.check_unique_runtime(),
            'La collection contient plusieurs instants de prévision. '
            'Aucun calcul réalisé.'
        )
        _exception.raise_valueerror(
            self.datatype == 'fcst' and not self.check_unique_code(),
            'La collection correspond à plusieurs codes. '
            'Aucun calcul réalisé.'
        )
        # -----------------------------------------------
        #    ETAPE 1 : Initialisation
        # -----------------------------------------------
        keys = _exception.set_default(keys, default=list(self.keys()))
        keys = list(set(keys).intersection(set(self.keys())))
        _exception.raise_valueerror(
            not keys,
            'La sélection des séries est vide. Aucun calcul réalisé.'
        )
        # Détermination de la période de calcul
        start = [self[k].firstdt for k in keys]
        end = [self[k].lastdt for k in keys]
        start = min(start)
        end = max(end)
        timestep = [self[k].timestep for k in keys][0]
        spc_varname = [self[k].spc_varname for k in keys][0]
        dates = pnd.date_range(start=start, end=end, freq=timestep)
        # -----------------------------------------------
        #    ETAPE 2 : Calcul des quantiles
        # -----------------------------------------------
        concat = self.concat(keys)
        if not skipna:
            concat = concat.dropna()
        quant = concat.quantile(
            q=[f/100 if f > 1 else f for f in freqs], axis=1).transpose()
        quant.columns = freqs
        quantiles = quant.reindex(dates)
        quantiles = {c: quantiles.xs(c, axis=1).to_frame()
                     for c in quantiles.columns}
        # -----------------------------------------------
        #    ETAPE 3 : Renvoi l'instance <Series>
        #              des quantiles
        # -----------------------------------------------
        series = self.__class__(
            datatype=self.datatype, name=self.name + '_percentile')
        for f in freqs:
            fstr = '{0:03d}'.format(int(f))
            spc_data = self._constructor_expanddim(
                quantiles[f], code=fstr, varname=spc_varname)
            if self.datatype == 'obs':
                spc_data.code = '{}-q{}'.format(self.name, fstr)
                series.add(serie=spc_data)
            elif self.datatype == 'fcst':
                runtimes = list({k[0] for k in self.meta})
                models = {k[1] for k in self.meta}
                if len(models) == 1:
                    meta = (runtimes[0], list(models)[0], 'percentile', fstr)
                else:
                    meta = (runtimes[0], self.name, 'percentile', fstr)
                key = (self.codes[0], self.varnames[0], meta)
                spc_data.code = str2tuple(tuple2str(key), forceobs=True)[0]
                series.add(code=key[0], serie=spc_data, meta=meta)
        return series

    def weighted_average(self, keys=None, weights=None, skipna=True):
        """
        Calculer la moyenne pondérée

        Parameters
        ----------
        keys : list
            Clés des séries à considérer
            Si non renseigné, la méthode s'applique
            à toutes les séries de la collection
        weights : int, float, dict
            - Pondération uniforme (si int, float)
            - Dictionnaire des poids à appliquer {ident série : poids}
        skipna : bool
            Ignorer les NaN ?
            - False : utiliser numpy.average
            - True  : utiliser numpy.ma.average


        Returns
        -------
        series : Series
            Nouvele collection de séries

        """
        # -----------------------------------------------
        #    ETAPE 0 : Contrôles
        # -----------------------------------------------
        self.refresh()
        _exception.raise_valueerror(
            not self.check_series(),
            "Au moins une série n'est pas une instance Serie"
        )
        _exception.raise_valueerror(
            not self.check_unique_varname(),
            'La collection contient plusieurs variables. '
            'Aucun calcul réalisé.'
        )
        _exception.raise_valueerror(
            self.datatype == 'fcst' and not self.check_unique_runtime(),
            'La collection contient plusieurs instants '
            'de prévision. Aucun calcul réalisé.'
        )
        _exception.raise_valueerror(
            self.datatype == 'fcst' and not self.check_unique_code(),
            'La collection correspond à plusieurs codes. '
            'Aucun calcul réalisé.'
        )
        # -----------------------------------------------
        #    ETAPE 1 : Initialisation
        # -----------------------------------------------
        if keys is None:
            keys = list(self.keys())
        else:
            keys = list(set(keys).intersection(set(self.keys())))
#            keys = [k for k in keys if k in self.keys()]
        _exception.raise_valueerror(
            not keys,
            'La sélection des séries est vide. '
            'Aucun calcul réalisé.'
        )
        # Pondération
        code_new = self.name + '_waverage'
        try:
            weights = [weights.get(k, 0) for k in keys]
        except AttributeError:
            weights = [weights for k in keys]
        # Détermination de la période de calcul
        start = [self[k].firstdt for k in keys]
        end = [self[k].lastdt for k in keys]
        start = min(start)
        end = max(end)
        timestep = [self[k].timestep for k in keys][0]
        spc_varname = [self[k].spc_varname for k in keys][0]
        dates = pnd.date_range(start=start, end=end, freq=timestep)
        # -----------------------------------------------
        #    ETAPE 2 : Calcul de la moyenne pondérée
        # -----------------------------------------------
        # Concaténer les séries retenues
        concat = self.concat(keys)
        # Retirer les lignes avec nan si skipna=False
        if not skipna:
            concat = concat.dropna()
        # Appliquer la pondération
        for k, c in enumerate(concat):
            concat[c] = weights[k] * concat[c]
        # Calculer la somme pondérée
        wnum = concat.sum(axis=1).reindex(dates)
        # Normaliser par les poids des valeurs != nan
        concat[~concat.isna()] = 1
        concat[concat.isna()] = 0
        for k, c in enumerate(concat):
            concat[c] = weights[k] * concat[c]
        wdenom = concat.sum(axis=1).reindex(dates)
        # Calculer la somme pondérée finale
        wa_data = wnum.div(wdenom)
        wa_data.columns = [spc_varname]
        # -----------------------------------------------
        #    ETAPE 3 : Renvoi l'instance <Serie>
        #              de la moyenne pondérée
        # -----------------------------------------------
        return self._constructor_expanddim(
            wa_data,
            code=code_new,
            varname=spc_varname
        )

    def errors(self, ref=None, absolute=None, relative=None,
               relative_fromref=None, freqs=None, sample_ratio=None):
        """
        Calculer les erreurs selon la série de référence

        Parameters
        ----------
        ref : Serie
            Série de référence
        absolute : bool
            Erreur absolue ? Défaut: False
        relative : bool
            Erreur relative ? Défaut: False
        relative_fromref : bool
            Dénominateur depuis série de référence. Défaut: True
        freqs : list
            Liste des fréquences, de 0 à 1
        sample_ratio : float
            % mini de la taille des échantillons
            défaut: 0.9

        Returns
        -------
        samples : pnd.DataFrame
            erreurs par prévision et par échéance
        stats : pnd.DataFrame
            statistiques des erreurs par échéance

        """
        # -----------------------------------------------
        #    ETAPE 0 : Contrôles
        # -----------------------------------------------
        self.refresh()
        _exception.raise_valueerror(
            self.datatype != 'fcst',
            'Le calcul est implémenté uniquement pour des prévisions'
        )
        Serie = self._constructor_expanddim
        _exception.raise_valueerror(
            not isinstance(ref, Serie),
            "La série de référence n'est pas un objet Serie. "
            "Aucun calcul réalisé."
        )
        _exception.raise_valueerror(
            not self.check_series(),
            "Au moins une série de prévision n'est pas une instance Serie"
        )
        _exception.raise_valueerror(
            not self.check_unique_varname(),
            'La collection contient plusieurs variables. Aucun calcul réalisé.'
        )
        _exception.raise_valueerror(
            not self.check_unique_code(),
            'La collection correspond à plusieurs lieux. Aucun calcul réalisé.'
        )
        _exception.raise_valueerror(
            self.codes[0] != ref.code,
            'La collection ne correspond pas au code de la série de '
            'référence. Aucun calcul réalisé.'
        )
        _exception.raise_valueerror(
            self.varnames[0] != ref.spc_varname,
            'La collection ne correspond pas à la grandeur de la série de '
            'référence. Aucun calcul réalisé.'
        )
        absolute = bool(absolute)
        relative = bool(relative)
        if relative_fromref is None:
            relative_fromref = True
        _exception.check_bool(relative_fromref)
        if freqs is None:
            freqs = [0.10, 0.50, 0.90]
        _exception.check_listlike(freqs)
        if sample_ratio is None:
            sample_ratio = 0.9
        _exception.check_numeric(sample_ratio)
        # -----------------------------------------------
        #    ETAPE 1 : Calcul des erreurs
        # -----------------------------------------------
        sample = _errors_fcst_compute(
            series=self, ref=ref, absolute=absolute, relative=relative,
            relative_fromref=relative_fromref)
        # -----------------------------------------------
        #    ETAPE 2 : Calcul des statistiques d'erreur
        # -----------------------------------------------
        sample_threshold = int(sample_ratio * len(self))
        stats = _errors_fcst_statistics(
            sample=sample, freqs=freqs, sample_threshold=sample_threshold)
        return sample, stats


def _errors_fcst_compute(series=None, ref=None, absolute=None, relative=None,
                         relative_fromref=None):
    """
    Calculer la matrice des erreurs
    """
    sample = []
    for key, serie in series.items():
        dtrun = key[2][0]
        try:
            e = serie - ref
            if absolute:
                e = abs(e)
            if relative and relative_fromref:
                e.code = ref.code
                e = e / ref
            elif relative:
                e.code = serie.code
                e = e / serie
            e = e.stripna()
        # Passer à la série suivante
        # si la série courante n'a pas de valid_index
        except TypeError:
            continue
        e.data_frame.index = (e.data_frame.index - dtrun) / ref.timestep
        e.data_frame.index = e.data_frame.index.astype(int)
        e.data_frame.columns = [tuple2str(key)]
        e.data_frame.index.name = 'leadtime'
        sample.append(e.data_frame)
    return pnd.concat(sample, axis=1)


def _errors_fcst_statistics(sample=None, freqs=None, sample_threshold=None):
    """
    Calculer les statistiques des erreurs
    """
    # Calculer les statistiques : NaN ignoré par pnd.DataFrame.describe
    stats = sample.apply(pnd.DataFrame.describe, axis=1, percentiles=freqs)
    # Conserver les échéances avec un échantillon suffisant
    stats = stats[stats['count'] >= sample_threshold]
    stats['count'] = stats['count'].astype(int)
    # Compteur des valeurs positives/négatives
    stats['count+'] = sample.apply(lambda x: np.count_nonzero(x > 0), axis=1)
    stats['count-'] = sample.apply(lambda x: np.count_nonzero(x < 0), axis=1)
    stats['count+'] = stats['count+'].astype(int)
    stats['count-'] = stats['count-'].astype(int)
    # Fréquence des valeurs positives/négatives
    stats['f+'] = stats['count+'] / stats['count']
    stats['f-'] = stats['count-'] / stats['count']
    # Renvoi des valeurs statistiques avec les colonnes triées par nom
    return stats.reindex(sorted(stats.columns), axis=1)
