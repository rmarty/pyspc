#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Series - Méthodes spécifiques - Traitement temporel
"""
import pytz


class TimeSerie():
    """
    Classe spécifique pour le Traitement temporel de pyspc.core.series.Series
    """
    def __init__(self):
        """
        Traitement temporel
        """
        self.datatype = None
        self.name = None

    def fill_constant(self, constant=None, inplace=False):
        """
        Pour chacune des séries:
        Remplacer les valeurs manquantes
        par une valeur constante <constant>
        Si l'option <inplace> est définie à False,
        alors cette méthode crée une nouvelle instance <Series>

        Parameters
        ----------
        constant : int, float
            Valeur constante
        inplace : bool
            Traitement en place (True)  ou renvoyer une nouvelle collection
            (False). Par défaut: False

        Returns
        -------
        pyspc.core.series.Series
            Nouvelle collection si inplace=False

        See Also
        --------
        pyspc.core.serie.Serie.fill_constant

        """
        constant = self._arg2dict(constant)
        if inplace:
            for k in self.keys():
                self[k].fill_constant(constant[k], inplace=True)
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
            for k in self.keys():
                s = self[k].fill_constant(constant[k], inplace=False)
                series.add(
                    code=k[0],
                    serie=s,
                    meta=k[2]
                )
            return series
        return None

    def fill_linear_interpolation(self, inplace=False):
        """
        Pour chacune des séries:
        Remplacer les valeurs manquantes
        par une interpolation linéaire
        Si l'option <inplace> est définie à False,
        alors cette méthode crée une nouvelle instance <Series>

        Parameters
        ----------
        inplace : bool
            Traitement en place (True)  ou renvoyer une nouvelle collection
            (False). Par défaut: False

        Returns
        -------
        pyspc.core.series.Series
            Nouvelle collection si inplace=False

        See Also
        --------
        pyspc.core.serie.Serie.fill_linear_interpolation

        """
        if inplace:
            for k in self.keys():
                self[k] = self[k].fill_linear_interpolation()
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
            for k in self.keys():
                s = self[k].fill_linear_interpolation()
                series.add(
                    code=k[0],
                    serie=s,
                    meta=k[2]
                )
            return series
        return None

    def set_timezone(self, timezone=pytz.utc, inplace=True):
        """
        Appliquer un nouveau fuseau horaire au contenu de l'instance
        de la classe Serie

        Parameters
        ----------
        timezone : pytz
            Date de la dernière valeur valide
        inplace : bool
            Traitement en place (True)  ou renvoyer une nouvelle collection
            (False). Par défaut: True

        Returns
        -------
        pyspc.core.series.Series
            Nouvelle collection si inplace=False

        See Also
        --------
        pyspc.core.serie.Serie.set_timezone

        """
        timezone = self._arg2dict(timezone)
        if inplace:
            for k in self.keys():
                self[k].set_timezone(timezone=timezone[k], inplace=True)
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
            for k in self.keys():
                s = self[k].set_timezone(timezone=timezone[k], inplace=False)
                series.add(
                    code=k[0],
                    serie=s,
                    meta=k[2]
                )
            return series
        return None

    def shift(self, periods=None, freq=None, inplace=True):
        """
        Appliquer la méthode de pnd.DataFrame.shift à l'instance <Series>

        Parameters
        ----------
        periods : timedelta
            Période
        freq : timedelta
            Fréquence
        inplace : bool
            Traitement en place (True)  ou renvoyer une nouvelle collection
            (False). Par défaut: True

        Returns
        -------
        pyspc.core.series.Series
            Nouvelle collection si inplace=False


        See Also
        --------
        pyspc.core.serie.Serie.shift

        """
        periods = self._arg2dict(periods)
        freq = self._arg2dict(freq)
        if inplace:
            for k in self.keys():
                self[k].shift(periods=periods[k], freq=freq[k], inplace=True)
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
            for k in self.keys():
                s = self[k].shift(
                    periods=periods[k], freq=freq[k], inplace=False)
                series.add(
                    code=k[0],
                    serie=s,
                    meta=k[2]
                )
            return series
        return None

    def timelag(self, tlag=None, inplace=True):
        """
        Pour chacune des séries:
        Décaler la série de données :
        - <tlag>  Si <tlag> est une instance de <td>,
        - <tlag>*self.timestep    Sinon
        Si l'option <inplace> est définie à False,
        alors cette méthode crée une nouvelle instance <Series>

        Parameters
        ----------
        tlag : int, float, timedelta
            Période
        inplace : bool
            Traitement en place (True)  ou renvoyer une nouvelle collection
            (False). Par défaut: True

        Returns
        -------
        pyspc.core.series.Series
            Nouvelle collection si inplace=False

        See Also
        --------
        pyspc.core.serie.Serie.timelag

        """
        tlag = self._arg2dict(tlag)
        if inplace:
            for k in self.keys():
                self[k].timelag(tlag[k], inplace=True)
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
            for k in self.keys():
                s = self[k].timelag(tlag[k], inplace=False)
                series.add(
                    code=k[0],
                    serie=s,
                    meta=k[2]
                )
            return series
        return None
