#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Series - Méthodes spécifiques - Modélisation
"""


class Modeling():
    """
    Classe spécifique pour la modélisation à partir de pyspc.core.series.Series
    """
    def __init__(self):
        """
        Modélisation
        """
        self.datatype = None
        self.name = None

    def apply_RatingCurves(self, curves=None, tocodes=None,
                           extrapolation=False, inplace=False):
        """
        Appliquer les courbes de tarage à la série

        Parameters
        ----------
        curves : pyspc.core.ratingcurve.RatingCurves
            Courbes de tarage et de correction
        tocodes : str, dict
            Codes des séries finales
                - si série de hauteurs, contient le code de la station
                - si série de débits, contient le code du site
        extrapolation : bool, dict
            Autoriser les conversions hors plage de validité
            des courbes de tarage. Défaut: False
        inplace : bool
            Nouvelle collection (False) ou ajout dans la collection (True)

        Returns
        -------
        series : pyspc.core.series.Series
            Collection de Serie converties par courbes de tarage

        """
        tocodes = self._arg2dict(tocodes)
        extrapolation = self._arg2dict(extrapolation)
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in self.keys():
            try:
                conv = self[k].apply_RatingCurves(
                    curves=curves, tocode=tocodes[k],
                    extrapolation=extrapolation[k])
            except AttributeError:
                continue
#            except ValueError:
#                continue
            meta = curves.name
            if len(curves) == 1:
                meta += '-{}'.format(next(iter(curves))[1])
            try:
                series.add(serie=conv, meta=meta)
            except AttributeError:
                self.add(serie=conv, meta=meta)
        return series

    def apply_Reservoir(self, reservoir=None, tablename=None, assoc=None,
                        sim=False, inplace=False):
        """
        Pour chacune des séries:
        Appliquer le Z0 et un bareme de réservoir
        et créer une collections Series à partir de la série courante

        Parameters
        ----------
        reservoir : Reservoir
            Instance de reservoir.Reservoir
        tablename : str
            Nom du bareme
        assoc : dict
            Correspondance des colonnes de la table
        sim : bool
            Définir les séries calculées comme étant des simulations.
            Défaut: False. Si True, le nom de la simulation est donné par
            reservoir.name
        inplace : bool
            Nouvelle collection (False) ou ajout dans la collection (True)

        Returns
        -------
        series : pyspc.core.series.Series
            Collection de Serie converties par le bareme

        """
        reservoir = self._arg2dict(reservoir)
        tablename = self._arg2dict(tablename)
        assoc = self._arg2dict(assoc)
        if self.datatype == 'fcst':
            sim = False
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in self.keys():
            try:
                conv = self[k].apply_Reservoir(
                    reservoir=reservoir[k], tablename=tablename[k],
                    assoc=assoc[k], sim=sim)
            except ValueError:
                continue
            for ks, s in conv.items():
                try:
                    series.add(code=ks[0], serie=s, meta=ks[2])
                except AttributeError:
                    self.add(code=ks[0], serie=s, meta=ks[2])
        return series

    def apply_ReservoirTable(self, table=None, assoc=None,
                             sim=False, inplace=False):
        """
        Appliquer un bareme de réservoir

        Parameters
        ----------
        tablename : pyspc.core.reservoir.Table
            Bareme du réservoir
        assoc : dict
            Correspondance des colonnes de la table
        sim : bool
            Définir les séries calculées comme étant des simulations.
            Défaut: False. Si True, le nom de la simulation est donné par
            table.name
        inplace : bool
            Nouvelle collection (False) ou ajout dans la collection (True)

        Returns
        -------
        pyspc.core.series.Series
            Collection de séries converties par le barème

        """
        table = self._arg2dict(table)
        assoc = self._arg2dict(assoc)
        if self.datatype == 'fcst':
            sim = False
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in self.keys():
            try:
                conv = self[k].apply_ReservoirTable(
                    table=table[k], assoc=assoc[k], sim=sim)
            except ValueError:
                continue
            for ks, s in conv.items():
                try:
                    series.add(code=ks[0], serie=s, meta=ks[2])
                except AttributeError:
                    self.add(code=ks[0], serie=s, meta=ks[2])
        return series

    def apply_ReservoirZ0(self, reservoir=None, inplace=False):
        """
        Appliquer le Z0 de réservoir

        Parameters
        ----------
        reservoir : pyspc.core.reservoir.Reservoir
            Instance de reservoir.Reservoir
        inplace : bool
            Nouvelle collection (False) ou ajout dans la collection (True)

        Returns
        -------
        pyspc.core.series.Serie
            Série de données converties par l'application du Z0 du réservoir

        """
        reservoir = self._arg2dict(reservoir)
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in self.keys():
            try:
                conv = self[k].apply_ReservoirZ0(reservoir=reservoir[k])
            except ValueError:
                continue
            for ks, s in conv.items():
                try:
                    series.add(code=ks[0], serie=s, meta=ks[2])
                except AttributeError:
                    self.add(code=ks[0], serie=s, meta=ks[2])
        return series

    def etp_oudin(self, latitude=None, timestep=None, inplace=False):
        """
        Modélisation de l'ETP par Oudin à partir de série de température
        et d'une valeur de latitude

        Parameters
        ----------
        latitude : float, dict de float
            Latitudes des séries
        timestep : timedelta, dict de timedelta
            Pas de temps de la série d'ETP calculée (horaire ou journalier).
            Si non défini, la série d'ETP sera au même pas de temps que la
            série de température, si celui-ci est horaire ou journalier
        inplace : bool
            Si True: Ajouter les séries agrégées
            dans la collection courante. Si False, créer
            une nouvelle collection de séries

        Returns
        -------
        series : Series
            Collections de séries des valeurs d'ETP

        """
        latitude = self._arg2dict(latitude)
        timestep = self._arg2dict(timestep)
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in self.keys():
            try:
                s = self[k].etp_oudin(latitude=latitude[k],
                                      timestep=timestep[k])
            except ValueError:
                continue
            try:
                series.add(code=k[0], serie=s, meta=k[2])
            except AttributeError:
                self.add(code=k[0], serie=s, meta=k[2])
        return series

    def regime_sauquet(self):
        """
        Définir la similarité du régime hydro avec les références Sauquet.

        Returns
        -------
        regimes : dict
            Dictionnaire des régimes normalisés et des régimes Sauquet
            clé: clé de la série dans la collection
            valeur : {'norm': régime normalisé, 'regimes': régimes Sauquet}

        See Also
        --------
        pyspc.core.serie.Serie.regime_sauquet

        """
        regimes = {}
        for k, s in self.items():
            try:
                n, r = s.regime_sauquet()
            except ValueError:
                continue
            if n is None or r is None:
                continue
            regimes.setdefault(k, {'norm': n, 'regimes': r})
        return regimes

    def sim2fcst(self, ref=None, first_dtime=None, last_dtime=None,
                 error_depth=0, max_ltime=120):
        """
        Convertir une simulation en prévision
        avec prise en compte de l'erreur à l'instant de prévision
        qui décroit linéairement avec l'échéance de prévision

        Parameters
        ----------
        ref : Serie
            Série de référence
        error_depth : int
            Durée de prise en compte de l'erreur, en pas de temps. Défaut: 0
        first_dt : datetime
            Premier instant de prévision
        last_dt : datetime
            Dernier instant de prévision
        max_ltime : int
            Horizon de prévision maximal, en pas de temps. Défaut: 120

        Returns
        -------
        series : Series
            Collection de prévisions

        """
        ref = self._arg2dict(ref)
        first_dtime = self._arg2dict(first_dtime)
        last_dtime = self._arg2dict(last_dtime)
        error_depth = self._arg2dict(error_depth)
        max_ltime = self._arg2dict(max_ltime)
        series = self.__class__(datatype='fcst', name='sim2fcst')
        for k in self.keys():
            s = self[k].sim2fcst(
                ref=ref[k],
                first_dtime=first_dtime[k],
                last_dtime=last_dtime[k],
                error_depth=error_depth[k],
                max_ltime=max_ltime[k],
            )
            series.extend(series=s)
        return series

    def socose(self):
        """
        Déterminer les paramètrs D et RXD du modèle SOCOSE.
        - D : durée au-dessus du débit standardisé valant 0.5 (Q/Qp)
        - RXD : rapport du débit de pointe sur le débit moyen sur D

        Parameters
        ----------

        Returns
        -------
        socose : dict
            Valeurs SOCOSE par clé de la collection
            - col: {'d': d, 'rxd': rxd}

        See Also
        --------
        pyspc.core.serie.Serie.socose
        pyspc.model.socose.socose

        """
        values = {}
        for k, s in self.items():
            v = s.socose()
            for v2 in v.values():
                values.setdefault(k, v2)
        return values
