#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Series - Méthodes spécifiques - Figures
"""
import matplotlib.pyplot as mplt
import os.path
import pylab

from pyspc.core.keyseries import tuple2str
import pyspc.core.exception as _exception
from pyspc.plotting.config import Config

from ._plotting_lib import (
    _create_figure, _plot_serie,
    _set_legend, _set_legend_bottom,
    _set_xaxis,  _set_yaxis
)

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


DEFAULT_FIGURE = {
    'dirname': '.',
    'dpi': 75,
    'format': 'png',
    'legend': 0,
    'legendcol': 1,
    'legendbottom': 0,
    'legendbox': (0.5, -0.20),
    'lfontsize': 12,
    'verbose': False,
    'tfontsize': 12,
    'xfmt': "%d/%m/%Y\n%H:%M",
    'xfontsize': 12,
    'xminor': [0, 25, 6],
    'xtitle': 'Date',
    'yfontsize': 12,
}
"""Dictionnaire des éléments de configuration de figure par défaut"""
# LEGENDE
# 0=best, 1=upperright, 2=upperleft,
# 3=lowerleft, 4=lowerright, 5=right,
# 6=centerleft, 7=centerright,
# 8=lowercenter, 9=uppercenter, 10=center

DEFAULT_LINE2D = {
    'linewidth': 2,
    'linestyle': '-',
    'marker': '',
    'markersize': 4,
    # 'color': [0.0, 0.0, 0.0],
    # 'markeredgecolor': [0.0, 0.0, 0.0],
    # 'markerfacecolor': [1.0, 1.0, 1.0],
    'label': ''
}
"""Dictionnaire des éléments de configuration de courbe par défaut"""

PLOTTITLES = {
    'hyeto': {
        'title': 'Hyétogramme',
        'filename': 'hyetogramme',
        'ytitle': 'Précipitations (mm)',
    },
    'hydro': {
        'title': 'Hydrogramme',
        'filename': 'hydrogramme',
        'ytitle': 'Débit ($m^3/s$)',
    },
    'event': {
        'title': 'Evenement',
        'filename': 'evenement',
        'ytitleq': 'Débit ($m^3/s$)',
        'ytitlep': 'Précip. (mm)',
        'ytitlet': 'Temp. (C)',
    },
    'hydro_limni': {
        'title': 'Hydrogramme / Limnigramme',
        'filename': 'hydro_limni',
        'ytitle1': 'Débit ($m^3/s$)',
        'ytitle2': 'Hauteur (m)',
        'ytitleq': 'Débit ($m^3/s$)',
        'ytitleh': 'Hauteur (m)',
        'ytitlep': 'Précip. (mm)',
        'ytitlet': 'Temp. (C)',
    },
    'reservoir': {
        'title': 'Reservoir',
        'filename': 'reservoir',
        'ytitleq': 'Débit ($m^3/s$)',
        'ytitlez': 'Cote (mNGF)',
    },
    'subreach': {
        'title': 'Synthèse',
        'filename': 'subreach',
        'ytitleq': 'Débit ($m^3/s$)',
        'ytitleh': 'Hauteur (m)',
        'ytitlep': 'Précip. (mm)',
    },
}
"""Dictionnaire des titres et nom de fichier par type de figure"""

PLOTTYPES = [
    'hydro',  # Hydrogrammes, limnigrammes
    'hydro_limni',  # Hydrogrammes ET Limnigrammes
    'hyeto',  # Hyétogrammes
    'event',  # Événement
    'reservoir',  # Reservoir
    'subreach'  # Reservoir
]
"""Liste des types de figure autotisés pour des collections de Serie"""


class Plotting():
    """
    Classe spécifique pour les figures à partir de pyspc.core.series.Series
    """
    def __init__(self):
        """
        Mélanges, mises à jour
        """
        self.datatype = None
        self.name = None

    def plot(self, plottype=None, filename=None, config=None,
             fill_threshold=None, fill_reverse=None, uncert=None):
        """
        Tracer une figure temporelle

        Parameters
        ----------
        plottype : str
            Type de figure. Si non défini, la figure est tracée par pandas

        Other Parameters
        ----------------
        filename : str
            Nom du fichier de l'image, si impression par défaut (pandas)
        config : Config, dict
            Configuration de la figure et des coubes.
            Les clés des options des courbes correspondent au keyseries.
            Voir aussi pyspc.core.keyseries
        fill_threshold : dict
            Dictionnaire des séries utilisées pour tracer les incertitudes
            Les clés des incertitudes correspondent au keyseries.
            Les valeurs sont des tuples (seuil, couleur)
            Voir aussi pyspc.core.keyseries
        fill_reverse : bool
            Affichage du volume au-dessus (False) ou en-dessous du seuil.
            Par défaut: False
        uncert : dict
            Dictionnaire des séries utilisées pour tracer les incertitudes
            Les clés des incertitudes correspondent au keyseries.
            Voir aussi pyspc.core.keyseries

        Returns
        -------
        filename : str
            Nom du fichier image créé

        Notes
        -----
        Les options config et uncert sont utilisées uniquement si plottype
        est parmi Series.get_plottypes()

        """
        if plottype in self.get_plottypes():
            return self.plot_series(
                plottype=plottype, config=config,
                fill_threshold=fill_threshold, fill_reverse=fill_reverse,
                uncert=uncert)
        return self.plot_bypandas(filename=filename)

    def plot_bypandas(self, filename=None):
        """
        Imprimer les série de données dans un fichier image

        Parameters
        ----------
        filename : str
            Nom du fichier de l'image

        Returns
        -------
        filename : str
            Nom du fichier de l'image

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.raise_valueerror(
            not self.check_series(),
            "Au moins une série n'est pas une instance Serie"
        )
        _exception.raise_valueerror(
            len({s.units for s in self.values()}) > 1,
            "La figure par défaut ne traite qu'une grandeur unique"
        )
        # ---------------------------------------------------------------------
        # 1- Fichier et format de l'image
        # ---------------------------------------------------------------------
        if filename is None:
            filename = str(self.name) + '.png'
        info = os.path.splitext(os.path.basename(filename))
        title = info[0].replace("_", " ")
        # ---------------------------------------------------------------------
        # 2- Courbes
        # ---------------------------------------------------------------------
        ax = None
        fig = None
        for serie in self.values():
            df = serie.data_frame
            if fig is None:
                ax = df.plot(figsize=(11.69, 8.27), title=title, marker='o')
                fig = ax.get_figure()
            else:
                df.plot(ax=ax)
        # ---------------------------------------------------------------------
        # 3- Titres
        # ---------------------------------------------------------------------
        units = list({s.units for s in self.values()})[0]
        ax.set_ylabel(units)
        ax.legend()
        fig.tight_layout(rect=(0, 0, 1, 0.95))
#        fig.suptitle(title)
        # ---------------------------------------------------------------------
        # 4- Enregistrement
        # ---------------------------------------------------------------------
        fig.savefig(filename, dpi=150)
        mplt.close(fig)
        fig = None
        # ---------------------------------------------------------------------
        # 5- Retour
        # ---------------------------------------------------------------------
        return filename

    def plot_series(self, plottype=None, config=None,
                    fill_threshold=None, fill_reverse=None, uncert=None):
        """
        Tracer une figure temporelle

        Parameters
        ----------
        plottype : str
            Type de figure
        config : Config, dict, filename
            Configuration de la figure et des coubes.
            Les clés des options des courbes correspondent au keyseries.
            Voir aussi pyspc.core.keyseries

        Other Parameters
        ----------------
        fill_threshold : dict
            Dictionnaire des séries utilisées pour tracer les incertitudes
            Les clés des incertitudes correspondent au keyseries.
            Les valeurs sont des tuples (seuil, couleur)
            Voir aussi pyspc.core.keyseries
        fill_reverse : bool
            Affichage du volume au-dessus (False) ou en-dessous du seuil.
            Par défaut: False
        uncert : dict
            Dictionnaire des séries utilisées pour tracer les incertitudes
            Les clés des incertitudes correspondent au keyseries.
            Voir aussi pyspc.core.keyseries

        Returns
        -------
        filename : str
            Nom du fichier image créé

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        if isinstance(config, str):
            config = Config(filename=config)
            config.load()
        _exception.check_dict(config)
        if plottype is None:
            try:
                plottype = config['figure']['plottype']
            except KeyError:
                pass
        self.check_plottype(plottype=plottype)
        if fill_threshold is None:
            fill_threshold = {}
        _exception.check_dict(fill_threshold)
        if fill_reverse is None:
            fill_reverse = False
        _exception.check_bool(fill_reverse)
        if uncert is None:
            uncert = {}
        _exception.check_dict(uncert)
        # ---------------------------------------------------------------------
        # 1- Chargement de la configuration par défaut
        # ---------------------------------------------------------------------
        config['figure'] = {
            **DEFAULT_FIGURE,  # Configuration par défaut
            **PLOTTITLES[plottype],  # Titres et labels par défaut
            **config.get('figure', {})  # Configuration de l'utilisateur
        }
        # ---------------------------------------------------------------------
        # 2- Création de la figure et des zones graphiques
        # ---------------------------------------------------------------------
        dpi = config['figure']['dpi']
        fig, axes = _create_figure(series=self, plottype=plottype, dpi=dpi)
        yn = min([ax.get_position().y0 for ax in fig.get_axes()])
        axb = [ax for ax in fig.get_axes() if ax.get_position().y0 == yn][0]
#        yx = max([ax.get_position().y0 for ax in fig.get_axes()])
#        axt = [ax for ax in fig.get_axes() if ax.get_position().y0 == yx][0]
        # ---------------------------------------------------------------------
        # 3- Création des courbes
        # ---------------------------------------------------------------------
        for key in self.keys():
            try:
                ax = axes[key[1]]
            except KeyError:
                continue
            kstr = tuple2str(key)
            kcfg = {
                **DEFAULT_LINE2D,  # Configuration par défaut
                **{'label': kstr},  # Label par défaut
                **config.get(kstr, {})  # Configuration de l'utilisateur
            }
            _plot_serie(plottype=plottype, ax=ax, serie=self[key],
                        config=kcfg,
                        fill_threshold=fill_threshold.get(kstr, None),
                        fill_reverse=fill_reverse,
                        uncert=uncert.get(kstr, None)
                        )
        # ---------------------------------------------------------------------
        # 4- Axe X
        # ---------------------------------------------------------------------
        _set_xaxis(axes=axes, axb=axb, config=config['figure'])
        # ---------------------------------------------------------------------
        # 5- Axes Y
        # ---------------------------------------------------------------------
        _set_yaxis(axes=axes, plottype=plottype, config=config['figure'])
        # ---------------------------------------------------------------------
        # 6- Titre
        # ---------------------------------------------------------------------
        fig.suptitle(config['figure']['title'],
                     fontsize=config['figure'].get('tfontsize', 16))
        # ---------------------------------------------------------------------
        # 7- Légende
        # ---------------------------------------------------------------------
        if config['figure']['legendbottom']:
            _set_legend_bottom(
                axes=axes, plottype=plottype, config=config['figure'])
        else:
            _set_legend(axes=axes, plottype=plottype, config=config['figure'])
        # ---------------------------------------------------------------------
        # 8- Enregistrement de la figure
        # ---------------------------------------------------------------------
        fmt = config['figure']['format']
        filename = os.path.join(config['figure']['dirname'],
                                config['figure']['filename'] + '.' + fmt)
        pylab.savefig(filename, dpi=dpi)
        mplt.close(fig)
        fig = None
        return filename

    def check_plottype(self, plottype=None):
        """
        Contrôler le type de figure

        Parameters
        ----------
        plottype : str
            Type de figure

        Raises
        ------
        ValueError
            Si lLe type de figure est incorrect

        See Also
        --------
        get_plottypes

        """
        _exception.raise_valueerror(
            plottype not in self.get_plottypes(),
            "Le type de figure '{}' est incorrect".format(plottype)
        )

    @classmethod
    def get_plottypes(cls):
        """
        Liste des types de figures

        Returns
        -------
        list
            Types de figures

        """
        return sorted(PLOTTYPES)
