#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Series - Méthodes spécifiques - Indexation temporelle
"""
import pandas as pnd
import pyspc.core.exception as _exception


class Reindexing():
    """
    Classe spécifique pour l'indexation temporelle de pyspc.core.series.Series
    """
    def __init__(self):
        """
        Reindexation temporelle
        """
        self.datatype = None
        self.name = None

    def between_dates(self, first_dtime=None, last_dtime=None, inplace=False):
        """
        Pour chacune des séries:
        Sous-echantillonner entre les dates <first_dtime> et <last_dtime>.
        Si l'option <inplace> est définie à False,
        alors cette méthode crée une nouvelle instance <Series>

        Parameters
        ----------
        first_dtime : datetime
            Première date à conserver
        last_dtime : datetime
            Dernière date à conserver
        inplace : bool
            Traiter en place (True) ou renvoyer une nouvelle instance (False)
            défaut: False

        Returns
        -------
        series : Series
            Collection de séries agrégées/moyennées
            (si inplace:False)

        See Also
        --------
        pyspc.core.serie.Serie.between_dates

        """
        first_dtime = self._arg2dict(first_dtime)
        last_dtime = self._arg2dict(last_dtime)
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in self.keys():
            s = self[k].between_dates(
                first_dtime[k], last_dtime[k], inplace=inplace)
            if s is None:
                continue
            try:
                series.add(code=k[0], serie=s, meta=k[2])
            except (ValueError, AttributeError):
                pass
        return series

    def concat(self, keys=None):
        """
        Concaténer la collection en un seul DataFrame

        Parameters
        ----------
        keys : list
            Clés des séries à concaténer
            Si non renseigné, la concaténation s'applique
            à toutes les séries de la collection

        Returns
        -------
        dframe : pnd.DataFrame
            DataFrame des séries concaténées

        """
        if keys is None:
            keys = list(self.keys())
        else:
            keys = [k for k in keys if k in self.keys()]
        dfs = [self[k].data_frame for k in keys]
        df = pnd.concat(dfs, axis=1)
        df.columns = keys
        return df

    def split(self, value=None, method=None):
        """
        Pour chacune des séries: découper une collection de séries.

        Parameters
        ----------
        value : int, float, list, dict
            Valeur d'application de la méthodologie
        method : str, dict
            Méthodologie de découpage de la série (N=value)
            - 'chunks' : N blocs en cherchant une taille homogène
            - 'chunk_size' : blocs de taille identique N
            - 'days' : fréquence de N jours (N=value)
            - 'groups' : regroupements fournis sous la forme d'un DataFrame
              de même dimension que la série
            - 'dates' : découpage selon une liste de dates fournies sous
              la forme de datetime.datetime

        Returns
        -------
        pyspc.core.series.Series
            Collection de séries

        Notes
        -----
        Les paramètres sont à fournir soit sous la forme attendue par
        Serie.split (voir plus bas), soit sous la forme d'un dictionnaire
        où la clé est la clé associée à la série dans la collection courante.

        L'homogénéité des tailles des blocs par 'chunksize' n'est pas garantie.

        See Also
        --------
        pyspc.core.serie.Serie.split

        Examples
        --------
        >>> series
        *************************************
        ********** SERIES *******************
        *************************************
        *  NOM DE LA COLLECTION = series
        *  TYPE DE COLLECTION   = obs
        *  NOMBRE DE SERIES     = 4
        *  ----------------------------------
        *  SERIE #1
        *      - CODE    = K0000000
        *      - VARNAME = QH
        *      - META    = None
        *  ----------------------------------
        *  SERIE #2
        *      - CODE    = K9999999
        *      - VARNAME = QH
        *      - META    = None
        *  ----------------------------------
        *  SERIE #3
        *      - CODE    = K3333333
        *      - VARNAME = QH
        *      - META    = None
        *  ----------------------------------
        *  SERIE #4
        *      - CODE    = K4444444
        *      - VARNAME = QH
        *      - META    = None
        *************************************

        >>> splits = series.split(value=3, method='chunks')
        >>> splits
        *************************************
        ********** SERIES *******************
        *************************************
        *  NOM DE LA COLLECTION = series_split
        *  TYPE DE COLLECTION   = obs
        *  NOMBRE DE SERIES     = 12
        *  ----------------------------------
        *  SERIE #1
        *      - CODE    = K0000000
        *      - VARNAME = QH
        *      - META    = split-0
        *  ----------------------------------
        *  SERIE #2
        *      - CODE    = K0000000
        *      - VARNAME = QH
        *      - META    = split-1
        *  ----------------------------------
        *  SERIE #3
        *      - CODE    = K0000000
        *      - VARNAME = QH
        *      - META    = split-2
        *  ----------------------------------
        *  SERIE #4
        *      - CODE    = K9999999
        *      - VARNAME = QH
        *      - META    = split-0
        *  ----------------------------------
        *  SERIE #5
        *      - CODE    = K9999999
        *      - VARNAME = QH
        *      - META    = split-1
        *  ----------------------------------
        *  SERIE #6
        *      - CODE    = K9999999
        *      - VARNAME = QH
        *      - META    = split-2
        *  ----------------------------------
        *  SERIE #7
        *      - CODE    = K3333333
        *      - VARNAME = QH
        *      - META    = split-0
        *  ----------------------------------
        *  SERIE #8
        *      - CODE    = K3333333
        *      - VARNAME = QH
        *      - META    = split-1
        *  ----------------------------------
        *  SERIE #9
        *      - CODE    = K3333333
        *      - VARNAME = QH
        *      - META    = split-2
        *  ----------------------------------
        *  SERIE #10
        *      - CODE    = K4444444
        *      - VARNAME = QH
        *      - META    = split-0
        *  ----------------------------------
        *  SERIE #11
        *      - CODE    = K4444444
        *      - VARNAME = QH
        *      - META    = split-1
        *  ----------------------------------
        *  SERIE #12
        *      - CODE    = K4444444
        *      - VARNAME = QH
        *      - META    = split-2
        *************************************

        """
        value = self._arg2dict(value)
        method = self._arg2dict(method)
        _exception.raise_valueerror(
            self.datatype == 'fcst',
            "Fonctionnalité non permise pour des prévisions")
        series = self.__class__(datatype=self.datatype,
                                name=self.name + '_split')
        for k in self.keys():
            try:
                split = self[k].split(value=value[k], method=method[k])
            except ValueError:
                continue
            series.extend(split)
        return series

    def stripna(self, inplace=False):
        """
        Pour chacune des séries:
        Renvoyer de nouvelles séries
        sans les np.nan en début et fin de série
        Si l'option <inplace> est définie à False,
        alors cette méthode crée une nouvelle instance <Series>

        Parameters
        ----------
        inplace : bool
            Traiter en place (True) ou renvoyer une nouvelle instance (False)
            défaut: False

        Returns
        -------
        series : Series
            Collection de séries agrégées/moyennées
            (si inplace:False)

        See Also
        --------
        pyspc.core.serie.Serie.stripna

        """
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in self.keys():
            s = self[k].stripna()
            if s is None:
                continue
            try:
                series.add(code=k[0], serie=s, meta=k[2])
            except AttributeError:
                self[k] = s
        return series
