#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Series - Méthodes spécifiques - Exports
"""
import os.path

import pyspc.core.exception as _exception


class Exporting():
    """
    Classe spécifique pour les exports à partir de pyspc.core.series.Series
    """
    def __init__(self):
        """
        Exports
        """
        self.datatype = None

    def to_BdApbp(self, datatype=None, dirname='.'):
        """
        Créer un fichier de données MF à partir d'une instance Series

        Parameters
        ----------
        datatype : str
            Type de fichier json
        dirname : str
            Répertoire où écrire le fichier

        Returns
        -------
        filenames : list
            Liste des noms de fichiers créés

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        self.refresh()
        _exception.raise_valueerror(
            not self.check_notempty(),
            'La collection est vide. Aucun export réalisé.'
        )
        _exception.raise_valueerror(
            self.datatype != 'fcst',
            "La collection doit être de type 'fcst'"
        )
        _exception.raise_valueerror(
            not self.check_unique_varname(),
            "La collection ne doit contenir qu'une grandeur"
        )
        _exception.raise_valueerror(
            self.varnames[0] != 'PJ',
            "La collection doit contenir des pluies journalières"
        )
        # ---------------------------------------------------------------------
        # 1- Export
        # ---------------------------------------------------------------------
        from pyspc.io.lamedo import write_BdApbp
        return write_BdApbp(series=self, datatype=datatype, dirname=dirname)

    def to_csv(self, dirname='.', filename=None, keys=None):
        """
        Export vers csv

        Parameters
        ----------
        dirname : str
            Répertoire du fichier csv

        Other Parameters
        ----------------
        filename : str
            Fichier csv à écrire. Si non défini, le nom du fichier repose sur
            le nom de la collection
        keys : list
            Clés des séries à concaténer. Si non renseigné, la concaténation
            s'applique à toutes les séries de la collection

        Returns
        -------
        filename : str
            Fichier csv créé

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.check_str(dirname)
        self.refresh()
        _exception.raise_valueerror(
            not self.check_notempty(),
            'La collection est vide. Aucun export réalisé.'
        )
        if filename is None:
            filename = '{}.csv'.format(self.name)
        filename = os.path.join(dirname, filename)
        # ---------------------------------------------------------------------
        # 1- Export
        # ---------------------------------------------------------------------
        from pyspc.io.csv import write_csv
        return write_csv(series=self, filename=filename, keys=keys)

    def to_GRP_Data(self, dirname='.', version='2016', how='replace'):
        """
        Créer un fichier de données GRP Data à partir d'une instance Series

        Parameters
        ----------
        dirname : str
            Répertoire où écrire le fichier
        version : str
            Version de GRP parmi ['grp16', '2016', 'grp18', '2018',
            'grp20', '2020']. Par défaut: '2016'

        Other Parameters
        ----------------
        how : str
            Option d'écriture
                - replace : écraser si un fichier existe déjà (défaut)
                - fillna : mettre à jour le fichier existant
                  (seulement les NaN et les valeurs aux instants non existants)
                - overwrite :  mettre à jour le fichier existant
                  (toutes les valeurs, y compris les non-NaN)

        Returns
        -------
        filename : list of str
            Liste des noms des fichiers créés

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.raise_valueerror(
            not self.check_notempty(),
            'La collection est vide. Aucun export réalisé.'
        )
        _exception.raise_valueerror(
            self.datatype != 'obs',
            "La collection doit être de type 'obs'"
        )
        # ---------------------------------------------------------------------
        # 1- Export 2016
        # ---------------------------------------------------------------------
        if version in ['grp16', '2016']:
            from pyspc.io.grp16 import write_GRP16
            return write_GRP16(
                series=self, dirname=dirname, datatype='grp16_cal_data',
                how=how)
        # ---------------------------------------------------------------------
        # 2- Export 2018
        # ---------------------------------------------------------------------
        if version in ['grp18', '2018']:
            from pyspc.io.grp18 import write_GRP18
            return write_GRP18(
                series=self, dirname=dirname, datatype='grp18_cal_data',
                how=how)
        # ---------------------------------------------------------------------
        # 3- Export 2020
        # ---------------------------------------------------------------------
        if version in ['grp20', '2020']:
            from pyspc.io.grp20 import write_GRP20
            return write_GRP20(
                series=self, dirname=dirname, datatype='grp20_cal_data',
                how=how)
        # ---------------------------------------------------------------------
        # 0- Version inconnue
        # ---------------------------------------------------------------------
        raise ValueError(
            "La version '{}' est incorrecte. Elle doit être parmi {}".format(
                version, ['grp16', '2016', 'grp18', '2018', 'grp20', '2020']))

    def to_GRPRT_Archive(self, version='2016', dirname='.'):
        """
        Créer un fichier de données GRP RT Archive
        à partir d'une instance Series

        Parameters
        ----------
        dirname : str
            Répertoire où écrire le fichier
        version : str
            Version de GRP parmi ['grp16', '2016', 'grp18', '2018',
            'grp20', '2020']. Par défaut: '2016'

        Returns
        -------
        filename : list of str
            Liste des noms des fichiers créés

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        self.refresh()
        _exception.raise_valueerror(
            not self.check_notempty(),
            'La collection est vide. Aucun export réalisé.'
        )
        _exception.raise_valueerror(
            self.datatype != 'obs',
            'Le type de collection est incorrect. Aucun export réalisé.'
        )
        # ---------------------------------------------------------------------
        # 1- Export 2016
        # ---------------------------------------------------------------------
        if version in ['grp16', '2016']:
            from pyspc.io.grp16 import write_GRP16
            return write_GRP16(
                series=self, dirname=dirname, datatype='grp16_rt_archive')
        # ---------------------------------------------------------------------
        # 2- Export 2018
        # ---------------------------------------------------------------------
        if version in ['grp18', '2018']:
            raise NotImplementedError('grp18')
        # ---------------------------------------------------------------------
        # 3- Export 2020
        # ---------------------------------------------------------------------
        if version in ['grp20', '2020']:
            from pyspc.io.grp20 import write_GRP20
            return write_GRP20(
                series=self, dirname=dirname, datatype='grp20_rt_archive')
        # ---------------------------------------------------------------------
        # 3- Export 2022
        # ---------------------------------------------------------------------
        if version in ['grp22', '2022']:
            from pyspc.io.grp22 import write_GRP22
            return write_GRP22(
                series=self, dirname=dirname, datatype='grp22_rt_archive')
        # ---------------------------------------------------------------------
        # 0- Version inconnue
        # ---------------------------------------------------------------------
        raise ValueError(
            "La version '{}' est incorrecte. Elle doit être parmi {}".format(
                version, ['grp16', '2016', 'grp18', '2018', 'grp20', '2020']))

    def to_GRPRT_Data(self, version='2016', dirname='.'):
        """
        Créer un fichier de données GRP RT Data à partir d'une instance Series

        Parameters
        ----------
        dirname : str
            Répertoire où écrire le fichier
        version : str
            Version de GRP parmi ['grp16', '2016', 'grp18', '2018',
            'grp20', '2020', 'grp22', '2022']. Par défaut: '2016'

        Returns
        -------
        filename : list of str
            Liste des noms des fichiers créés

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        self.refresh()
        _exception.raise_valueerror(
            not self.check_notempty(),
            'La collection est vide. Aucun export réalisé.'
        )
        # ---------------------------------------------------------------------
        # 1- Export 2016 : obs et scénario météo
        # ---------------------------------------------------------------------
        if version in ['grp16', '2016']:
            from pyspc.io.grp16 import write_GRP16
            return write_GRP16(
                series=self, dirname=dirname, datatype='grp16_rt_data')
        # ---------------------------------------------------------------------
        # 2- Export 2018 : obs seulement
        # ---------------------------------------------------------------------
        if version in ['grp18', '2018']:
            _exception.raise_valueerror(
                self.datatype != 'obs',
                "La collection doit être de type 'obs'"
            )
            from pyspc.io.grp18 import write_GRP18
            return write_GRP18(
                series=self, dirname=dirname, datatype='grp18_rt_data')
        # ---------------------------------------------------------------------
        # 3- Export 2020 : obs seulement
        # ---------------------------------------------------------------------
        if version in ['grp20', '2020']:
            _exception.raise_valueerror(
                self.datatype != 'obs',
                "La collection doit être de type 'obs'"
            )
            from pyspc.io.grp20 import write_GRP20
            return write_GRP20(
                series=self, dirname=dirname, datatype='grp20_rt_data')
        # ---------------------------------------------------------------------
        # 3- Export 2020 : obs seulement
        # ---------------------------------------------------------------------
        if version in ['grp22', '2022']:
            _exception.raise_valueerror(
                self.datatype != 'obs',
                "La collection doit être de type 'obs'"
            )
            from pyspc.io.grp22 import write_GRP22
            return write_GRP22(
                series=self, dirname=dirname, datatype='grp22_rt_data')
        # ---------------------------------------------------------------------
        # 0- Version inconnue
        # ---------------------------------------------------------------------
        raise ValueError(
            "La version '{}' est incorrecte. Elle doit être parmi {}".format(
                version, ['grp16', '2016', 'grp18', '2018', 'grp20', '2020'
                          'grp22', '2022']))

    def to_GRPRT_Metscen(self, version='2016', dirname='.'):
        """
        Créer un fichier de données GRP RT Data à partir d'une instance Series

        Parameters
        ----------
        dirname : str
            Répertoire où écrire le fichier
        version : str
            Version de GRP parmi ['grp16', '2016', 'grp18', '2018',
            'grp20', '2020']. Par défaut: '2016'

        Returns
        -------
        filename : list of str
            Liste des noms des fichiers créés

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        self.refresh()
        _exception.raise_valueerror(
            not self.check_notempty(),
            'La collection est vide. Aucun export réalisé.'
        )
        # ---------------------------------------------------------------------
        # 1- Export 2016 : obs et scénario météo
        # ---------------------------------------------------------------------
        if version in ['grp16', '2016']:
            return self.to_GRPRT_Data(version=version, dirname=dirname)
        # ---------------------------------------------------------------------
        # 2- Export 2018 : scénario météo seulement
        # ---------------------------------------------------------------------
        if version in ['grp18', '2018']:
            _exception.raise_valueerror(
                self.datatype != 'fcst',
                "La collection doit être de type 'fcst'"
            )
            from pyspc.io.grp18 import write_GRP18
            return write_GRP18(
                series=self, dirname=dirname, datatype='grp18_rt_metscen')
        # ---------------------------------------------------------------------
        # 3- Export 2020 : scénario météo seulement
        # ---------------------------------------------------------------------
        if version in ['grp20', '2020']:
            _exception.raise_valueerror(
                self.datatype != 'fcst',
                "La collection doit être de type 'fcst'"
            )
            from pyspc.io.grp20 import write_GRP20
            return write_GRP20(
                series=self, dirname=dirname, datatype='grp20_rt_metscen')
        # ---------------------------------------------------------------------
        # 4- Export 2022 : scénario météo seulement
        # ---------------------------------------------------------------------
        if version in ['grp22', '2022']:
            _exception.raise_valueerror(
                self.datatype != 'fcst',
                "La collection doit être de type 'fcst'"
            )
            from pyspc.io.grp22 import write_GRP22
            return write_GRP22(
                series=self, dirname=dirname, datatype='grp22_rt_metscen')
        # ---------------------------------------------------------------------
        # 0- Version inconnue
        # ---------------------------------------------------------------------
        raise ValueError(
            "La version '{}' est incorrecte. Elle doit être parmi {}".format(
                version, ['grp16', '2016', 'grp18', '2018', 'grp20', '2020',
                          'grp22', '2022']))

    def to_MF_Data(self, dirname='.', basename=None, asobs=True):
        """
        Créer un fichier de données MF à partir d'une instance Series

        Parameters
        ----------
        dirname : str
            Répertoire où écrire le fichier

        Other Parameters
        ----------------
        basename : str
            Nom du fichier.
            Si non défini, le 'name' de la collection sera utilisé
        asobs : bool
            Si False, les identifiants proviennent du 1er élément de la clé
            Si True, les identifiants proviennent des 1er et dernier éléments
            Cette option est utile s'il s'agit d'une collection de prévisions
            Défaut: True

        Returns
        -------
        filename : list
            Noms des fichiers créés

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        self.refresh()
        _exception.raise_valueerror(
            not self.check_notempty(),
            'La collection est vide. Aucun export réalisé.'
        )
        # ---------------------------------------------------------------------
        # 1.1- Export - Multi-variable
        # ---------------------------------------------------------------------
#        _exception.raise_valueerror(
#            not self.check_unique_varname(),
#            'La collection contient plusieurs variables. Aucun export réalisé'
#        )
        if not self.check_unique_varname():
            filenames = []
            for v in sorted(set(self.varnames)):
                b = ''
                if basename is not None:
                    b = os.path.splitext(basename)[0] + f"_{v}" \
                        + os.path.splitext(basename)[1]
                subseries = self.__class__(datatype=self.datatype,
                                           name=f"{self.name}_{v}")
                for ks, s in self.items():
                    if ks[1] == v:
                        subseries.add(code=ks[0], serie=s, meta=ks[2])
                filenames.append(subseries.to_MF_Data(
                    dirname=dirname, basename=b, asobs=asobs))
            return filenames
        # ---------------------------------------------------------------------
        # 1.2- Export Mono-variable
        # ---------------------------------------------------------------------
        from pyspc.io.meteofrance import write_MF_Data
        if basename is None:
            basename = '{}.data'.format(self.name)
        filename = os.path.join(dirname, basename)
        if asobs:
            return write_MF_Data(series=self.asobs(), filename=filename)
        return write_MF_Data(series=self, filename=filename)

    def to_PLATHYNES_Data(self, dirname='.', suffix=None, event=None,
                          injections=None):
        """
        Ecrire des fichiers d'observation (.mgr, .mqo, .mqi) pour PLATHYNES

        Parameters
        ----------
        dirname : str
            Répertoire de destination
        suffix : dict
            Association entre les stations hydrométriques (grandeur: Q)
            et les suffixes des noms de fichier

        Other Parameters
        ----------------
        event : str
            Identifiant de l'événement.
            Si non défini, reprend le nom de la collection
        injections : dict
            Indiquer si une série (clé=code de la série)
            est une injection pour PLATHYNES (valeur: True/False)

        Returns
        -------
        filenames : list
            Fichiers créés

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        self.refresh()
        _exception.raise_valueerror(
            not self.check_notempty(),
            'La collection est vide. Aucun export réalisé.'
        )
        _exception.raise_valueerror(
            self.datatype != 'obs',
            "La collection doit être de type 'obs'"
        )
        if event is None:
            event = self.name
        if suffix is None:
            suffix = {}
        # ---------------------------------------------------------------------
        # 1- Export
        # ---------------------------------------------------------------------
        from pyspc.io.plathynes import write_Plathynes
        return write_Plathynes(series=self, dirname=dirname, suffix=suffix,
                               event=event, injections=injections)

    def to_Prevision19(self, filename=None, valid=False):
        """
        Créer un fichier prv (Otamin, Scores) à partir d'une instance Series

        Parameters
        ----------
        filename : str
            Chemin de la base de données de type Prevision19
        valid : bool
            Seulement les prévisions validées (True)
            ou toutes les prévisions produites (False)
            Défaut: False

        Returns
        -------

        See Also
        --------
        pyspc.data.prevision.Prevision19
        pyspc.data.prevision.Prevision19.insert_fcst

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        self.refresh()
        _exception.raise_valueerror(
            not self.check_notempty(),
            'La collection est vide. Aucun export réalisé.'
        )
        _exception.raise_valueerror(
            self.datatype != 'fcst',
            "La collection doit être de type 'fcst'"
        )
        _exception.check_str(filename)
        # ---------------------------------------------------------------------
        # 1- Export
        # ---------------------------------------------------------------------
        from pyspc.io.prevision import write_Prevision19
        return write_Prevision19(series=self, filename=filename, valid=valid)

    def to_prv(self, datatype=None, dirname='.', basename=None):
        """
        Créer un fichier prv (Otamin, Scores) à partir d'une instance Series

        Parameters
        ----------
        datatype : str
            Type de fichier json
        dirname : str
            Répertoire où écrire le fichier

        Other Parameters
        ----------------
        basename : str
            Nom du fichier.
            Si non défini, le 'name' de la collection sera utilisé

        Returns
        -------
        filename : str
           Noms du fichiers créé

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        self.refresh()
        _exception.raise_valueerror(
            not self.check_notempty(),
            'La collection est vide. Aucun export réalisé.'
        )
        if self.datatype == 'obs':
            _exception.raise_valueerror(
                not datatype.endswith('obs') and not datatype.endswith('sim'),
                "Incohérence entre le type de collection '{}' "
                "et le type d'export '{}'".format(self.datatype, datatype)
            )
        elif self.datatype == 'fcst':
            _exception.raise_valueerror(
                not datatype.endswith('fcst')
                and not datatype.endswith('trend'),
                "Incohérence entre le type de collection '{}' "
                "et le type d'export '{}'".format(self.datatype, datatype)
            )
            _exception.raise_valueerror(
                datatype.endswith('trend')
                and {m[3] for m in self.meta} == set([None]),
                "Incohérence entre la présence de probabilité '{}'"
                "et le type d'export '{}'".format({m[3] for m in self.meta},
                                                  datatype)
            )
        # ---------------------------------------------------------------------
        # 1- Export
        # ---------------------------------------------------------------------
        from pyspc.io.prv import write_prv
        if basename is None:
            basename = '{}.prv'.format(self.name)
        filename = os.path.join(dirname, basename)
        return write_prv(series=self, datatype=datatype, filename=filename)

    def to_PyspcFile(self, dirname='.', code=None, how='replace',
                     onefile=False, explicitcolname=False):
        """
        Créer un fichier csv de type 'PyspcFile' à partir d'une instance Series

        Parameters
        ----------
        dirname : str
            Répertoire où écrire les fichiers

        Other Parameters
        ----------------
        code : str
            Identifiant de la collection, utilisé si onefile = True
            et si la collection contient plusieurs stations
            Si non défini, le 'name' de la collection sera utilisé
        onefile : bool
            Enregister la collection dans un unique fichier. Défaut: False
            Si la collection ne contient qu'une série,
            alors cette option est ignorée
        explicitcolname : bool
            Forcer le nommage explicite des colonnes. Défaut: False
        how : str
            Option d'écriture, uniquement si onefile = False
            - replace : écraser si un fichier existe déjà (défaut)
            - fillna : mettre à jour le fichier existant
              (seulement les NaN et les valeurs aux instants non existants)
            - overwrite :  mettre à jour le fichier existant
              (toutes les valeurs, y compris les non-NaN)

            Si onefile = True, le fichier existant est écrasé.

        Returns
        -------
        filenames : list
            Liste des noms de fichiers créés

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        self.refresh()
        _exception.raise_valueerror(
            not self.check_notempty(),
            'La collection est vide. Aucun export réalisé.'
        )
        # ---------------------------------------------------------------------
        # 1- Export
        # ---------------------------------------------------------------------
        from pyspc.io.pyspcfile import write_PyspcFile
        return write_PyspcFile(
            series=self, dirname=dirname, code=code, how=how,
            onefile=onefile, explicitcolname=explicitcolname)

    def to_Sandre(self, datatype=None, dirname='.', basename=None,
                  runtime=None, sender=None, user=None, target=None):
        """
        Créer un fichier xml Sandre à partir d'une instance Series

        Parameters
        ----------
        datatype : str
            Type de fichier json
        dirname : str
            Répertoire où écrire le fichier
        runtime : datetime
            Date de production de la donnée/prévision.
            Si non défini, la date est dt.utcnow()
        sender : str
            Identifiant de l'emetteur
        user : str
            Identifiant du contact
        target : str
            Identifiant du destinataire

        Other Parameters
        ----------------
        basename : str
            Nom du fichier.
            Si non défini, le 'name' de la collection sera utilisé

        Returns
        -------
        filenames : list
            Liste des noms de fichiers créés

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        from pyspc.data.sandre import Sandre
        self.refresh()
        _exception.raise_valueerror(
            not self.check_notempty(),
            'La collection est vide. Aucun export réalisé.'
        )
        Sandre.check_dtype(datatype)
        _exception.raise_valueerror(
            not self.check_unique_varname(),
            "La collection ne doit contenir qu'une grandeur"
        )
        _exception.raise_valueerror(
            self.datatype == 'fcst' and not datatype.startswith('data_fcst'),
            "Incohérence entre le type de collection '{}' "
            "et le type d'export '{}'".format(self.datatype, datatype)
        )
        _exception.raise_valueerror(
            self.datatype == 'obs' and not datatype.startswith('data_obs'),
            "Incohérence entre le type de collection '{}' "
            "et le type d'export '{}'".format(self.datatype, datatype)
        )
        _exception.raise_valueerror(
            not self.check_unique_varname(),
            "La collection ne doit contenir qu'une grandeur"
        )
        _exception.raise_valueerror(
            not self.check_unique_runtime(),
            "La collection ne doit contenir qu'une date de production"
        )
        # ---------------------------------------------------------------------
        # 1- Export
        # ---------------------------------------------------------------------
        from pyspc.io.sandre import write_Sandre
        if basename is None:
            basename = '{}.xml'.format(self.name)
        filename = os.path.join(dirname, basename)
        return write_Sandre(
            series=self, datatype=datatype, filename=filename,
            runtime=runtime, sender=sender, user=user, target=target)

    def to_xls(self, dirname='.', filename=None, sheetname=None,  keys=None,
               overwrite=True):
        """
        Export vers xls

        Parameters
        ----------
        dirname : str
            Répertoire du fichier xls
        sheet_name : str
            Feuille à écrire. Si non défini, le nom de la feuille repose sur
            le nom de la collection

        Other Parameters
        ----------------
        filename : str
            Fichier xls. Si non défini, le nom du fichier repose sur le nom
            de la collection
        keys : list
            Clés des séries à concaténer. Si non renseigné, la concaténation
            s'applique à toutes les séries de la collection
        overwrite : bool
            Ecraser le fichier ? Si False, un fichier existant est complété

        Returns
        -------
        filename : str
            Fichier csv créé

        .. warning:: Cette méthode nécessite l'import de la
        bibliothèque tierce pandas.ExcelWriter

        .. warning:: Cette méthode nécessite l'import de la
        bibliothèque tierce xlwt

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.check_str(dirname)
        _exception.check_bool(overwrite)
        self.refresh()
        _exception.raise_valueerror(
            not self.check_notempty(),
            'La collection est vide. Aucun export réalisé.'
        )
        if sheetname is None:
            sheetname = self.name
        if filename is None:
            filename = '{}.xls'.format(self.name)
        filename = os.path.join(dirname, filename)
        # ---------------------------------------------------------------------
        # 1- Export
        # ---------------------------------------------------------------------
        from pyspc.io.csv import write_xls
        return write_xls(series=self, filename=filename, sheetname=sheetname,
                         keys=keys, overwrite=overwrite)
