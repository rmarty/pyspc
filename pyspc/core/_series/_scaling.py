#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Objets natifs de pyspc - Series - Méthodes spécifiques - Echelle temporelle
"""
import pandas as pnd


class Scaling():
    """
    Classe spécifique pour l'échelle temporelle de pyspc.core.series.Series
    """
    def __init__(self):
        """
        Echelle temporelle
        """
        self.datatype = None
        self.name = None

    def downscale(self, toparam=None, dayhour=6, inplace=False):
        """
        Désagréger à un pas de temps inférieur les séries de la collection,

        Parameters
        ----------
        toparam : Parameter, dict de Parameter
            Paramètre correspond au pas de temps ciblé
        dayhour : int, dict de int
            Heure du début de la journée
            Nécessaire si la cible est un pas de
            temps journalier.
            Par défaut: 6. Valeurs admises: [0, 6]
        inplace : bool
            Si True: Ajouter les séries agrégées
            dans la collection courante. Si False, créer
            une nouvelle collection de séries

        Returns
        -------
        series : Series
            Collection de séries agrégées/moyennées
            (si inplace:False)

        """
        toparam = self._arg2dict(toparam)
        dayhour = self._arg2dict(dayhour)
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in self.keys():
            try:
                s = self[k].downscale(toparam=toparam[k], dayhour=dayhour[k])
            except ValueError:
                continue
            try:
                series.add(code=k[0], serie=s, meta=k[2])
            except AttributeError:
                self.add(code=k[0], serie=s, meta=k[2])
        return series

    def nearlyequalscale(self, toparam=None, strict=True, inplace=False):
        """
        Ré-échantilloner une série à un pas de temps proche

        Parameters
        ----------
        toparam : Parameter, dict de Parameter
            Paramètre correspond au pas de temps ciblé
        strict : bool, dict de bool
            Calcul strict, imposant np.nan dans la valeur
            agrégée si au moins un pas de temps initial vaut
            np.nan. Retire de la série les pas de temps
            où les données d'origine ne sont pas complètes
            Défaut: True
        inplace : bool
            Si True: Ajouter les séries agrégées
            dans la collection courante. Si False, créer
            une nouvelle collection de séries

        Returns
        -------
        series : SerieS
            Collection de séries ré-échantillonnées
            (si inplace:False)

        """
        toparam = self._arg2dict(toparam)
        strict = self._arg2dict(strict)
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in self.keys():
            try:
                s = self[k].nearlyequalscale(
                    toparam=toparam[k], strict=strict[k])
            except ValueError:
                continue
            try:
                series.add(code=k[0], serie=s, meta=k[2])
            except AttributeError:
                self.add(code=k[0], serie=s, meta=k[2])
        return series

    def regularscale(self, inplace=False):
        """
        Interpoler les séries de la collection
        d'un pas de temps irrégulier à un pas de temps régulier

        Parameters
        ----------
        inplace : bool
            Si True: Ajouter les séries agrégées
            dans la collection courante. Si False, créer
            une nouvelle collection de séries

        Returns
        -------
        series : Series
            Collection de séries à un pas de temps régulier
            (si inplace:False)

        """
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in self.keys():
            try:
                s = self[k].regularscale()
            except ValueError:
                continue
            try:
                series.add(code=k[0], serie=s, meta=k[2])
            except AttributeError:
                self.add(code=k[0], serie=s, meta=k[2])
        return series

    def standardize(self):
        """
        Standardiser la chronique par normalisation selon la valeur max.

        L'index est un timedelta depuis la date du maxi.
        Cette chronique standardisée est utilisée par la méthode socose

        Returns
        -------
        df : pandas.DataFrame
            Tableau des données standardisées

        See Also
        --------
        pyspc.core.serie.Serie.standardize
        pyspc.core.serie.Series.socose

        """
        dfs = {k: s.standardize() for k, s in self.items()}
        if not dfs:
            return None
        df = pnd.concat(dfs, axis=1).droplevel(-1, axis=1)
        return df

    def subhourlyscale(self, ts=None, how=None, howinterp=None, inplace=False):
        """
        Interpoler les séries de la collection
        d'un pas de temps horaire à un pas de temps infra-horaire

        Parameters
        ----------
        ts : timedelta, dict de timedelta
            Pas de temps infra-horaire entre 1 min et 1h
            Par défaut: 5 minutes
        how : str, dict de str
            Méthode de ré-échantillonnage parmi:
            ['bfill', 'ffill', 'nearest', 'interpolate']
        howinterp : str, dict de str
            Méthode d'interpolation
            Par défaut: 'linear'
        inplace : bool
            Si True: Ajouter les séries agrégées
            dans la collection courante. Si False, créer
            une nouvelle collection de séries

        Returns
        -------
        series : Series
            Collection de séries infra-horaires
            (si inplace:False)

        """
        ts = self._arg2dict(ts)
        how = self._arg2dict(how)
        howinterp = self._arg2dict(howinterp)
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in self.keys():
            try:
                s = self[k].subhourlyscale(
                    ts=ts[k], how=how[k], howinterp=howinterp[k])
            except ValueError:
                continue
            try:
                series.add(code=k[0], serie=s, meta=k[2])
            except AttributeError:
                self.add(code=k[0], serie=s, meta=k[2])
        return series

    def upscale(self, toparam=None, dayhour=6, strict=True, inplace=False):
        """
        Agréger/moyenner à un pas de temps supérieur
        les séries de la collection,

        Parameters
        ----------
        toparam : Parameter, dict de Parameter
            Paramètre correspond au pas de temps ciblé
        dayhour : int, dict de int
            Heure du début de la journée
            Nécessaire si la cible est un pas de
            temps journalier.
            Par défaut: 6. Valeurs admises: [0, 6]
        strict : bool, dict de bool
            Calcul strict, imposant np.nan dans la valeur
            agrégée si au moins un pas de temps initial vaut
            np.nan. Retire de la série les pas de temps
            où les données d'origine ne sont pas complètes
            Défaut: True
        inplace : bool
            Si True: Ajouter les séries agrégées
            dans la collection courante. Si False, créer
            une nouvelle collection de séries

        Returns
        -------
        series : Series
            Collection de séries agrégées/moyennées
            (si inplace:False)

        """
        toparam = self._arg2dict(toparam)
        dayhour = self._arg2dict(dayhour)
        strict = self._arg2dict(strict)
        if inplace:
            series = None
        else:
            series = self.__class__(datatype=self.datatype, name=self.name)
        for k in sorted(self.keys()):
            try:
                s = self[k].upscale(
                    toparam=toparam[k], dayhour=dayhour[k], strict=strict[k])
            except ValueError:
                continue
            try:
                series.add(code=k[0], serie=s, meta=k[2])
            except AttributeError:
                self.add(code=k[0], serie=s, meta=k[2])
        return series
