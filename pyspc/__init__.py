#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Bibliothèque pyspc du projet pyspc
"""

__author__ = "Renaud Marty"
__email__ = "renaud.marty@developpement-durable.gouv.fr"
__date__ = "2025-02-12"
__version__ = "3.0.4"

# Import des objets natifs
from pyspc.core.config import Config
from pyspc.core.location import Location, Locations
from pyspc.core.parameter import Parameter
from pyspc.core.provider import Provider
from pyspc.core.ratingcurve import RatingCurve, RatingCurves
from pyspc.core.reach import Reach, Reaches
from pyspc.core.reservoir import Reservoir, Table
from pyspc.core.serie import Serie
from pyspc.core.series import Series

# Import des lecteurs -> Series
from pyspc.io.bareme import read_Bareme
from pyspc.io.cristal import read_Cristal
from pyspc.io.csv import read_csv, read_xls
from pyspc.io.grp16 import read_GRP16
from pyspc.io.grp18 import read_GRP18
from pyspc.io.grp20 import read_GRP20
from pyspc.io.hydro2 import read_Hydro2
from pyspc.io.lamedo import read_BdApbp, read_BdImage
from pyspc.io.meteofrance import (
    read_MF_Data, read_MF_OpenData, read_MF_OpenAPI,
    read_BP, read_Sympo)
from pyspc.io.plathynes import read_Plathynes
from pyspc.io.premhyce import read_Premhyce
from pyspc.io.prevision import (
    read_Prevision14, read_Prevision17, read_Prevision19)
from pyspc.io.prv import read_prv
from pyspc.io.pyspcfile import read_PyspcFile
from pyspc.io.refspc import read_RefSPC
from pyspc.io.sacha import read_Sacha
from pyspc.io.sandre import read_Sandre
from pyspc.io.vigicrues import read_Vigicrues
