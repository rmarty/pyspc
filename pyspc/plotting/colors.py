#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Images - Couleurs
"""
COLORS = [
    'dodgerblue',
    'red',
    'darkolivegreen',
    'darkgoldenrod',
    'violet',
    'purple',
    'brown',
    'orange',
    'teal',
    'blueviolet',
    'indigo',
    'seagreen',
    'skyblue',
    'crimson',
    'dimgrey',
    'tomato',
    'moccasin',
    'gold',
    'wheat',
    'darkviolet',
]
"""Liste de noms de couleur"""

TABCOLORS = [
    'tab:blue',
    'tab:orange',
    'tab:green',
    'tab:red',
    'tab:purple',
    'tab:brown',
    'tab:pink',
    'tab:olive',
    'tab:cyan',
]
"""Liste de noms de couleur (tab)"""

RICCOLORS = {
    'VJ': [0.8, 1.0, 0.2],
    'J': [1.0, 1.0, 0.0],
    'JO': [1.0, 0.8, 0.4],
    'O': [1.0, 0.6, 0.2],
    'OR': [1.0, 0.2, 0.0],
    'R': [0.8, 0.0, 0.0]
}
"""Dictionnaire des couleurs des zones de transition"""
