#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Images - Configuration
"""
import collections
from functools import partial

from pyspc.core.config import Config as _Config


CONVERTERS = {
        'xlim': partial(_Config.to_listofdatetime, sep=',', fmt='%Y%m%d%H'),
        'ylim': partial(_Config.to_listoffloat, sep=','),
        'ylimh': partial(_Config.to_listoffloat, sep=','),
        'ylimp': partial(_Config.to_listoffloat, sep=','),
        'ylimq': partial(_Config.to_listoffloat, sep=','),
        'ylimt': partial(_Config.to_listoffloat, sep=','),
        'ylimz': partial(_Config.to_listoffloat, sep=','),
        'ylim1': partial(_Config.to_listoffloat, sep=','),
        'ylim2': partial(_Config.to_listoffloat, sep=','),
        'xminor': partial(_Config.to_listofint, sep=','),
        'yminor': partial(_Config.to_listofint, sep=','),
        'dpi': int,
        'legend': int,
        'legendbottom': _Config.to_float,  # _Config.to_bool
        'legendcol': int,
        'lfontsize': int,
        'xfontsize': int,
        'yfontsize': int,
        'tfontsize': int,
        'xfmt': _Config.to_datetimeformat,
        'linewidth': int,
        'markersize': int,
}


class Config(collections.OrderedDict):
    """
    Classe destinée à traiter la configuration des images

    Attributes
    ----------
    filename : str
        Nom du fichier de configuration

    """

    # Ré-utilisations des méthodes de la classe native Config
    __str__ = _Config.__str__
    convert = _Config.convert
    read = _Config.read
    update_config = _Config.update_config
    list_sections_options = _Config.list_sections_options

    def __init__(self, filename=None):
        """
        Initialisation de l'instance Config des images

        Parameters
        ----------
        filename : str
            Nom du fichier de configuration

        """
        super().__init__()
        self.filename = filename

    def load(self):
        """
        Lire le fichier de configuration et convertir les éléments
        """
        # ---------------------------------------------------------------------
        # 1- Lecture
        # ---------------------------------------------------------------------
        self.read()
        # ---------------------------------------------------------------------
        # 2- Compléter par les valeurs par défaut
        # ---------------------------------------------------------------------
        keys = [x for x in self.keys()
                if x not in ['figure', 'defaut', 'default']]
        for key in keys:
            self[key] = {
                **self.get('defaut', {}),  # Configuration defaut
                **self.get('default', {}),  # Configuration default
                **self[key]  # Configuration de l'utilisateur
            }
        # ---------------------------------------------------------------------
        # 3- Définir les convertisseurs
        # ---------------------------------------------------------------------
        dict_conv = {}
        for section in self:
            for option in self[section]:
                if 'color' in option:
                    if isinstance(self[section][option], str) and \
                            ',' in self[section][option]:
                        dict_conv[(section, option)] = \
                            partial(_Config.to_listoffloat, sep=',')
                else:
                    dict_conv[(section, option)] = CONVERTERS.get(option, str)
        # ---------------------------------------------------------------------
        # 4- Conversion
        # ---------------------------------------------------------------------
        self.convert(functions=dict_conv)
