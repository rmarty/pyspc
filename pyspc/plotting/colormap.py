#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Images - Palette de couleur
"""
import matplotlib.pyplot as mplt
import numpy as np

import pyspc.core.exception as _exception


def build_colormap(cmapname='gist_rainbow', cmapsize=256, reverse=False,
                   partial=None):
    """
    Définir le tableau contenant les couleurs _rgb

    Parameters
    ----------
    cmapname : str
        Nom de la palette de couleur
    cmapsize : int
        Taille de la palette
    reverse : bool
        Renverser la palette. Défaut: False
    partial : float
        Fraction de la palette à retourner. Défaut: 1 (palette complete)
        Si 'partial' est une fraction

    Returns
    -------
    cmap_rgb : numpy.ndarray
        Tableau des quadruplets (red, green, blue, alpha) de couleur
        Dimension : (cmapsize, 4)

    Notes
    -----
    https://matplotlib.org/stable/users/explain/colors/colormaps.html

    """
    _exception.check_int(cmapsize)
    _exception.raise_valueerror(
        cmapsize < 1,
        'Taille de la palette trop petite'
    )
    _exception.raise_valueerror(
        cmapsize > 256,
        'Taille de la palette trop grande'
    )
    if isinstance(partial, (int, float)):
        _exception.raise_valueerror(
            partial < 0 or partial > 1,
            'Fraction incorrectement définie'
        )
    else:
        partial = 1
    cmap = mplt.get_cmap(cmapname)
    if reverse:
        cmap_rgb = cmap(np.linspace(255, 255 - int(255 * partial),
                                    num=cmapsize, dtype=int))
    else:
        cmap_rgb = cmap(np.linspace(0, int(255 * partial),
                                    num=cmapsize, dtype=int))
    return cmap_rgb
