#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Données (observations, prévisions) - Vigicrues - Observations
"""
from datetime import datetime as dt, timedelta as td, timezone as tz
import json
import pandas as pnd

from pyspc.convention.vigicrues import VARNAMES


class Vigicrues_Data():
    """
    Structure liée aux stations Vigicrues
    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe Vigicrues_Data

        Parameters
        ----------
        filename : str
            Fichier json

        """
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance <Vigicrues_Data>
        """
        text = """
        *************************************
        *********** VIGICRUES - Data ********
        *************************************
        *  FICHIER          = {filename}
        *************************************
        """
        return text.format(**vars(self))

    def _check_varname(self, varname=None):
        """
        Contrôler la variable

        Parameters
        ----------
        varname : str
            Nom de la variable.

        Raises
        ------
        ValueError
            Si la variable n'est pas reconnue par pyspc

        """
        if varname not in self.get_varnames():
            raise ValueError("Variable mal renseignée")

    def read(self, content=None):
        """
        Lire le résultat du flux json de Vigicrues
        - si Vigicrues_Location.filename est défini, alors le contenu est lu
          directement depuis ce fichier
        - sinon le contenu est celui défini par le paramètre content

        Parameters
        ----------
        content : str
            - Si cela correspond à un fichier, celui-ci est lu
              i.e, correspond au retour de urlretrieve
            - Sinon, il est considéré comme étant le résultés de urlopen

        Returns
        -------
        data : dict
            Contenu du flux json de Vigicrues

        Examples
        --------
        >>> import pyspc.metadata.vigicrues as _vigicrues
        >>> f = 'data/data/vigicrues/K118001010_observation.json'
        >>> vigi = _vigicrues.Vigicrues_Data(filename=f)
        >>> df = vigi.read()
        >>> df
                            K118001010
                                     Q
        2022-03-29 10:00:00       19.2
        2022-03-29 10:05:00       19.1
        2022-03-29 10:20:00       19.1
        2022-03-29 10:25:00       19.0
        2022-03-29 10:30:00       19.3
        2022-03-29 10:35:00       19.0
        2022-03-29 10:50:00       19.4
        2022-03-29 10:55:00       19.6
        2022-03-29 11:00:00       19.7
        2022-03-29 11:05:00       19.4
        2022-03-29 11:10:00       19.4
        2022-03-29 11:15:00       19.6
        2022-03-29 11:20:00       19.7
        2022-03-29 11:25:00       19.6
        2022-03-29 11:30:00       19.7
        2022-03-29 11:35:00       19.7
        2022-03-29 11:40:00       19.6
        2022-03-29 11:45:00       19.6
        2022-03-29 11:50:00       19.6
        2022-03-29 11:55:00       19.3
        2022-03-29 12:00:00       19.4
        2022-03-29 12:05:00       19.4
        2022-03-29 12:10:00       19.4
        2022-03-29 12:15:00       19.4
        2022-03-29 12:20:00       19.4
        2022-03-29 12:25:00       19.7
        2022-03-29 12:30:00       19.6
        2022-03-29 12:35:00       19.3
        2022-03-29 12:40:00       19.3
        2022-03-29 12:45:00       19.6
        2022-03-29 12:50:00       19.6
        2022-03-29 12:55:00       19.6
        2022-03-29 13:00:00       19.7
        2022-03-29 13:05:00       19.7
        2022-03-29 13:10:00       19.4
        2022-03-29 13:20:00       19.4
        2022-03-29 13:25:00       19.7
        2022-03-29 13:30:00       19.7
        2022-03-29 13:35:00       19.6
        2022-03-29 13:40:00       19.6
        2022-03-29 13:45:00       19.6
        2022-03-29 13:50:00       19.7
        2022-03-29 13:55:00       19.7
        2022-03-29 14:00:00       19.6
        2022-03-29 14:05:00       19.8
        2022-03-29 14:10:00       19.6
        2022-03-29 14:15:00       19.7
        2022-03-29 14:20:00       19.6
        2022-03-29 14:25:00       19.7
        2022-03-29 14:30:00       19.6

        Notes
        -----
        L'horodatage est forcé à UTC.
        L'information relative au fuseau horaire est retirée.

        """
        if self.filename is not None:
            with open(self.filename, 'r', encoding='utf-8') as j:
                data = json.load(j)
        else:
            data = json.loads(content)
        self._check_varname(varname=data['Serie']['GrdSerie'])
        key = (data['Serie']['CdStationHydro'], data['Serie']['GrdSerie'])
        values = [float(x['ResObsHydro']) for x in data['Serie']['ObssHydro']]
        # astimezone(tz(td(0))) : pour passer de UTC + Xh à UTC
        # replace(tzinfo=None)  : pour supprimer tzinfo
        index = [dt.fromisoformat(
                    x['DtObsHydro']).astimezone(tz(td(0))).replace(tzinfo=None)
                 for x in data['Serie']['ObssHydro']]
        return pnd.DataFrame({key: values}, index=index)

    @classmethod
    def get_varnames(cls):
        """
        Définir le nom de la variable
        """
        return sorted(VARNAMES)
