#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Données d'observation et de prévision - Projet LAMEDO - BdImage
"""


class BdImage():
    """
    Structure liée aux données XML BdImage (projet Lamedo)
    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe XML BdImage (projet Lamedo)

        Parameters
        ----------
        filename : str
            Fichier local XML

        """
        self.filename = filename

    def __str__(self):
        """
        Afficher les données de l'instance XML BdImage (projet Lamedo)
        """
        text = """
        *************************************
        ********* DATA - BdImage ************
        *************************************
        *  FICHIER          = {filename}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lire le fichier XML de BdImage (projet Lamedo)

        Examples
        --------
        >>> from pyspc.data.lamedo import BdImage
        >>> f = 'data/data/lamedo/SourceLoire-bbox_antilope-j1-rr'\
        ...     '_202105090600-202105120600_010000.xml'
        >>> bdimage = BdImage(filename=f)
        >>> bdimage
        *************************************
        ********* DATA - BdImage ************
        *************************************
        *  FICHIER          = data/data/lamedo/SourceLoire-bbox_antilope-j1-rr_202105090600-202105120600_010000.xml
        *************************************

        >>> content = bdimage.read()
        >>> content
        Message(Report(
            date: 2021-05-18 13:21:00, depth: 0:00:00.082000, status: 0,
            message: async job 402d6648-39f7-4fcf-a1d4-c802bfd3ebe6,
            request: {'sousTypeImage': 'j1', 'epsg': '2154', 'duree': '010000',
                      'pdt': '010000', 'request': 'getObsValuesByBBoxAsync',
                      'ul': '793000.000000000000,6414000.000000000000',
                      'version': '2016', 'dateFin': '202105120600',
                      'lr': '795000.000000000000,6412000.000000000000',
                      'dateDeb': '202105090600', 'bandes': 'rr',
                      'typeImage': 'antilope'},
            uri: None), 4 observation(s), 0 prevision(s))

        See Also
        --------
        Repose sur la libbdimage :
        https://gitlab.com/vigicrues/lamedo/bdimage3

        """
        from libbdimage.bdixml import r2016 as _xml
        try:
            return _xml.Message.fromfile(self.filename)
        except OSError as oe:
            raise ValueError("Impossible de lire le fichier {}"
                             "".format(self.filename)) from oe
