#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Données d'observation et de prévision - Prévisions du SPC LCI - Version 2014
"""
import collections
from datetime import datetime as dt, timedelta as td
import numpy as np
import pandas as pnd

import pyspc.core.exception as _exception
from pyspc.io.dbase.mdb import Mdb
from pyspc.convention.prevision import (
    P14_DATATYPES,
    P14_SQL_META,  P14_SQL_DATA,
    P14_TABLES_META, P14_ATTS_META,
    P14_TABLES_DATA, P14_ATTS_DATA
)


class Prevision14(Mdb):
    """
    Classe destinée à traiter la base Prévision du SPC LCI, période 2014-2016

    Attributes
    ----------
    filename : str
        Chemin de la base de données
    sql : str
        Requête courante au format SQL

    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance Prevision14

        Parameters
        ----------
        filename : str
            Chemin de la base de données

        """
        super().__init__(filename=filename)

    def read_fcst(self, codes=None, valid=False,
                  hydro_version='hydro3', first_dt=None, last_dt=None,
                  warning=True):
        """
        Récupération des prévisions de la base Prevision14

        Parameters
        ----------
        codes : list
            Codes Hydro2/Hydro3 des stations
        valid : bool
            Seulement les prévisions validées (True)
            ou toutes les prévisions produites (False)
            Défaut: False
        hydro_version : str
            Version du référentiel: 'hydro2' ou 'hydro3'. Défaut: 'hydro3'
        first_dt : datetime
            Premier instant de prévision
        last_dt : datetime
            Dernier instant de prévision
        warning : bool
            Affiche les avertissement si True. Défaut: True

        Returns
        -------
        X

        Raises
        ------
        ValueError
            si hydro_version n'est pas parmi ['hydro2', 'hydro3']

        See Also
        --------
        Prevision14.get_fcst_hydro2
        Prevision14.get_fcst_hydro3

        """
        if hydro_version == 'hydro2':
            return self.read_fcst_hydro2(codes=codes, valid=valid,
                                         first_dt=first_dt, last_dt=last_dt,
                                         warning=warning)
        if hydro_version == 'hydro3':
            return self.read_fcst_hydro3(codes=codes, valid=valid,
                                         first_dt=first_dt, last_dt=last_dt,
                                         warning=warning)
        raise ValueError('Version hydro incorrecte')

    def read_fcst_hydro2(self, codes=None, valid=False,
                         first_dt=None, last_dt=None, warning=True):
        """
        Récupération des prévisions de la base Prevision14

        Parameters
        ----------
        codes : list
            Codes Hydro2/Hydro3 des stations
        valid : bool
            Seulement les prévisions validées (True)
            ou toutes les prévisions produites (False)
            Défaut: False
        first_dt : datetime
            Premier instant de prévision
        last_dt : datetime
            Dernier instant de prévision
        warning : bool
            Affiche les avertissement si True. Défaut: True

        Returns
        -------
        pandas.DataFrame
            Tableau des données prévues

        Notes
        -----
        Les colonnes vides sont ignorées.

        Examples
        --------
        >>> from datetime import datetime as dt
        >>> from pyspc.data.prevision import Prevision14
        >>> f = 'data/io/dbase/prevision_2014.mdb'
        >>> reader = Prevision14(filename=f)

        Exemple de prévision validée - Cas Hydro2

        >>> df = reader.read_fcst_hydro2(
        ...     codes=['K1251810'],
        ...     first_dt=dt(2013, 5, 3, 12),
        ...     last_dt=dt(2013, 5, 3, 23),
        ...     valid=True
        ... )
        >>> df
                                       K1251810
                            2013-05-03 23:00:00
                                           2006
                                        DebiVal HautVal
        DateVal
        2013-05-04 00:00:00          204.199997    3.47
        2013-05-04 01:00:00          202.100006    4.79
        2013-05-04 02:00:00          200.199997    4.78
        2013-05-04 03:00:00          198.399994    4.77
        2013-05-04 04:00:00          196.300003    4.76
        2013-05-04 05:00:00          193.600006    4.75
        2013-05-04 06:00:00          190.399994    4.73
        2013-05-04 07:00:00          186.699997    4.71
        2013-05-04 08:00:00          182.500000    4.69
        2013-05-04 09:00:00          177.899994    4.66
        2013-05-04 10:00:00          173.100006    4.63
        2013-05-04 11:00:00          168.199997    4.61
        2013-05-04 12:00:00          163.300003    4.58
        2013-05-04 13:00:00          158.399994    4.55
        2013-05-04 14:00:00          153.500000    4.53
        2013-05-04 15:00:00          148.800003    4.50
        2013-05-04 16:00:00          144.300003    4.48
        2013-05-04 17:00:00          139.899994    4.45
        2013-05-04 18:00:00          135.699997    4.43
        2013-05-04 19:00:00          131.699997    4.41
        2013-05-04 20:00:00          127.699997    4.38
        2013-05-04 21:00:00          123.699997    4.36
        2013-05-04 22:00:00          119.800003    4.34
        2013-05-04 23:00:00          115.800003    4.32
        2013-05-05 00:00:00          111.599998    4.29
        2013-05-05 01:00:00          107.300003    4.27
        2013-05-05 02:00:00          103.000000    4.24
        2013-05-05 03:00:00           98.900002    4.22
        2013-05-05 04:00:00           95.099998    4.20
        2013-05-05 05:00:00           91.500000    4.18
        ...                                 ...     ...
        2013-05-05 18:00:00           59.099998    3.97
        2013-05-05 19:00:00           57.400002    3.96
        2013-05-05 20:00:00           55.799999    3.94
        2013-05-05 21:00:00           54.200001    3.93
        2013-05-05 22:00:00           52.700001    3.92
        2013-05-05 23:00:00           51.299999    3.90
        2013-05-06 00:00:00           49.900002    3.89
        2013-05-06 01:00:00           48.700001    3.88
        2013-05-06 02:00:00           47.400002    3.86
        2013-05-06 03:00:00           46.200001    3.84
        2013-05-06 04:00:00           45.099998    3.83
        2013-05-06 05:00:00           44.000000    3.81
        2013-05-06 06:00:00           43.000000    3.80
        2013-05-06 07:00:00           42.000000    3.78
        2013-05-06 08:00:00           41.000000    3.77
        2013-05-06 09:00:00           40.099998    3.75
        2013-05-06 10:00:00           39.200001    3.73
        2013-05-06 11:00:00           38.400002    3.71
        2013-05-06 12:00:00           37.500000    3.69
        2013-05-06 13:00:00           36.700001    3.67
        2013-05-06 14:00:00           36.000000    3.65
        2013-05-06 15:00:00           35.200001    3.63
        2013-05-06 16:00:00           34.500000    3.61
        2013-05-06 17:00:00           33.799999    3.59
        2013-05-06 18:00:00           33.200001    3.57
        2013-05-06 19:00:00           32.500000    3.55
        2013-05-06 20:00:00           31.900000    3.53
        2013-05-06 21:00:00           31.299999    3.51
        2013-05-06 22:00:00           30.700001    3.49
        2013-05-06 23:00:00           30.100000    3.47

        [72 rows x 2 columns]

        Exemple de prévision brute - Cas Hydro2

        >>> df = reader.read_fcst_hydro2(
        ...     codes=['K1251810'],
        ...     first_dt=dt(2013, 5, 3, 12),
        ...     last_dt=dt(2013, 5, 3, 23),
        ...     valid=False
        ... )
        >>> df
                                       K1251810  ...
                            2013-05-03 23:00:00  ...
                                           2001  ...
                                        PluiVal  ...    DebiVal HautVal
        DateVal                                  ...
        2013-05-04 00:00:00                 0.1  ... 204.199997    3.47
        2013-05-04 01:00:00                 1.6  ... 202.100006    4.79
        2013-05-04 02:00:00                 2.2  ... 200.199997    4.78
        2013-05-04 03:00:00                 1.1  ... 198.399994    4.77
        2013-05-04 04:00:00                 0.2  ... 196.300003    4.76
        2013-05-04 05:00:00                 0.1  ... 193.600006    4.75
        2013-05-04 06:00:00                 0.1  ... 190.399994    4.73
        2013-05-04 07:00:00                 0.0  ... 186.699997    4.71
        2013-05-04 08:00:00                 0.2  ... 182.500000    4.69
        2013-05-04 09:00:00                 0.0  ... 177.899994    4.66
        2013-05-04 10:00:00                 0.0  ... 173.100006    4.63
        2013-05-04 11:00:00                 0.0  ... 168.199997    4.61
        2013-05-04 12:00:00                 0.0  ... 163.300003    4.58
        2013-05-04 13:00:00                 0.0  ... 158.399994    4.55
        2013-05-04 14:00:00                 0.0  ... 153.500000    4.53
        2013-05-04 15:00:00                 0.0  ... 148.800003    4.50
        2013-05-04 16:00:00                 0.0  ... 144.300003    4.48
        2013-05-04 17:00:00                 0.0  ... 139.899994    4.45
        2013-05-04 18:00:00                 0.0  ... 135.699997    4.43
        2013-05-04 19:00:00                 0.0  ... 131.699997    4.41
        2013-05-04 20:00:00                 0.0  ... 127.699997    4.38
        2013-05-04 21:00:00                 0.0  ... 123.699997    4.36
        2013-05-04 22:00:00                 0.0  ... 119.800003    4.34
        2013-05-04 23:00:00                 0.0  ... 115.800003    4.32
        2013-05-05 00:00:00                 0.0  ... 111.599998    4.29
        2013-05-05 01:00:00                 0.0  ... 107.300003    4.27
        2013-05-05 02:00:00                 0.0  ... 103.000000    4.24
        2013-05-05 03:00:00                 0.0  ...  98.900002    4.22
        2013-05-05 04:00:00                 0.0  ...  95.099998    4.20
        2013-05-05 05:00:00                 0.0  ...  91.500000    4.18
        ...                                 ...  ...        ...     ...
        2013-05-05 18:00:00                 0.0  ...  59.099998    3.97
        2013-05-05 19:00:00                 0.0  ...  57.400002    3.96
        2013-05-05 20:00:00                 0.0  ...  55.799999    3.94
        2013-05-05 21:00:00                 0.0  ...  54.200001    3.93
        2013-05-05 22:00:00                 0.0  ...  52.700001    3.92
        2013-05-05 23:00:00                 0.0  ...  51.299999    3.90
        2013-05-06 00:00:00                 0.1  ...  49.900002    3.89
        2013-05-06 01:00:00                 0.0  ...  48.700001    3.88
        2013-05-06 02:00:00                 0.0  ...  47.400002    3.86
        2013-05-06 03:00:00                 0.0  ...  46.200001    3.84
        2013-05-06 04:00:00                 0.0  ...  45.099998    3.83
        2013-05-06 05:00:00                 0.0  ...  44.000000    3.81
        2013-05-06 06:00:00                 0.0  ...  43.000000    3.80
        2013-05-06 07:00:00                 0.0  ...  42.000000    3.78
        2013-05-06 08:00:00                 0.0  ...  41.000000    3.77
        2013-05-06 09:00:00                 0.0  ...  40.099998    3.75
        2013-05-06 10:00:00                 0.0  ...  39.200001    3.73
        2013-05-06 11:00:00                 0.0  ...  38.400002    3.71
        2013-05-06 12:00:00                 0.0  ...  37.500000    3.69
        2013-05-06 13:00:00                 0.0  ...  36.700001    3.67
        2013-05-06 14:00:00                 0.0  ...  36.000000    3.65
        2013-05-06 15:00:00                 0.2  ...  35.200001    3.63
        2013-05-06 16:00:00                 0.0  ...  34.500000    3.61
        2013-05-06 17:00:00                 0.0  ...  33.799999    3.59
        2013-05-06 18:00:00                 0.0  ...  33.200001    3.57
        2013-05-06 19:00:00                 0.0  ...  32.500000    3.55
        2013-05-06 20:00:00                 0.0  ...  31.900000    3.53
        2013-05-06 21:00:00                 0.0  ...  31.299999    3.51
        2013-05-06 22:00:00                 0.0  ...  30.700001    3.49
        2013-05-06 23:00:00                 0.0  ...  30.100000    3.47

        [72 rows x 27 columns]

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.check_listlike(codes)
        # ---------------------------------------------------------------------
        # 1- Recherche des séries
        # ---------------------------------------------------------------------
        series = self.read_serie_hydro2(
            codes=codes, valid=valid, first_dt=first_dt, last_dt=last_dt,
            warning=warning)
        if self.check_sql_return(content=series, warning=warning) is None:
            return None
        # ---------------------------------------------------------------------
        # 2- Recherche des valeurs
        # ---------------------------------------------------------------------
        # ---------------------------------------------------------------------
        # 2.1- Connexion base Access
        # ---------------------------------------------------------------------
        self.connect()
        # ---------------------------------------------------------------------
        # 2.2- Rechercher les prévisions
        # ---------------------------------------------------------------------
        dfs = collections.OrderedDict()
        for meta, nserie in series.items():
            with_date = True
            self._set_sql_fcst_hydro2(
                nserie=nserie, valid=valid, with_date=with_date)
            content = self.execute(warning=warning)
            if content is None:
                with_date = False
                self._set_sql_fcst_hydro2(
                    nserie=nserie, valid=valid, with_date=with_date)
                content = self.execute(warning=warning)
            if self.check_sql_return(content=content, warning=warning) is None:
                continue
        # ---------------------------------------------------------------------
        # 2.3- Tableau de données
        # ---------------------------------------------------------------------
            # Créer le pnd.DataFrame
            atts = P14_ATTS_DATA[('hydro2', valid)]
            if not with_date:  # Version SANS les dates dans la requête SQL
                atts.pop(1)
            df = {c: [x[k] for x in content] for k, c in enumerate(atts)}
            df = pnd.DataFrame(df)
            # Supprimier les colonnes 'None', np.nan
            df = df.fillna(value=np.nan)
            df = df.dropna(axis=1, how='all')
            # Retirer la colonne NSerie
            df = df.drop(atts[0], axis=1)
            # Définir la date
            c = P14_ATTS_DATA[('hydro2', valid)][1]
            if not with_date:
                df[c] = [meta[1] + i*td(hours=1) for i in df.index]
            # Définir l'index par la colonnes DateVal
            df = df.set_index(keys=c, drop=True)
            # Stockage dans le dictionnaire
            dfs[meta[:3]] = df
        # ---------------------------------------------------------------------
        # 2.4- Déconnexion base Access
        # ---------------------------------------------------------------------
        self.close()
        # ---------------------------------------------------------------------
        # 3- Fusion des données et des méta-données
        # ---------------------------------------------------------------------
        df = pnd.concat(dfs, axis=1)
        # ---------------------------------------------------------------------
        # 4- Retour
        # ---------------------------------------------------------------------
        return df

    def _set_sql_fcst_hydro2(self, nserie=None, valid=False, with_date=True):
        """
        Définir de la requête SQL destinée à récupérer
        les chroniques de prévision
            + nserie = clé primaire de la série
            + valid = (boolean)     Seulement les prévisions validées (True)
                                    ou toutes les prévisions produites (False)
            + with_date = (bool) Avec ou sans la date dans la requête SQL
        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.check_int(nserie)
        # ---------------------------------------------------------------------
        # 1- Définition de la requête
        # ---------------------------------------------------------------------
        # définition des éléments de la requête SQL
        atts = P14_ATTS_DATA[('hydro2', valid)]
        if not with_date:  # Version SANS les dates dans la requête SQL
            atts.pop(1)
        sql_atts = ",".join(['"' + att + '"' for att in atts])
        # Séries validées/expertisées
        table = P14_TABLES_DATA[valid]
        # définition de la requête SQL
        self.sql = P14_SQL_DATA[('hydro2', valid)].format(
            table, sql_atts, 'NSerie', nserie)

    def read_serie_hydro2(self, codes=None, valid=None,
                          first_dt=None, last_dt=None, warning=True):
        """
        Récupération des séries de la base Prevision14

        Parameters
        ----------
        codes : list
            Codes Hydro2/Hydro3 des stations
        valid : bool
            Seulement les prévisions validées (True)
            ou toutes les prévisions produites (False)
            Défaut: False
        first_dt : datetime
            Premier instant de prévision
        last_dt : datetime
            Dernier instant de prévision
        warning : bool
            Affiche les avertissement si True. Défaut: True

        Returns
        -------
        dict
            Dictionnaire des méta-données des séries {méta: nserie}

        Examples
        --------
        >>> from datetime import datetime as dt
        >>> from pyspc.data.prevision import Prevision14
        >>> f = 'data/io/dbase/prevision_2014.mdb'
        >>> reader = Prevision14(filename=f)

        Exemple de prévision validée - Cas Hydro2

        >>> content = reader.read_serie_hydro2(
        ...     codes=['K1251810', 'K1363010'],
        ...     first_dt=dt(2013, 5, 3, 12),
        ...     last_dt=dt(2013, 5, 3, 23),
        ...     valid=True
        ... )
        >>> content
        {('K1251810', datetime.datetime(2013, 5, 3, 23, 0), 2006,
          None, None, None, None, None, None, None): 96,
         (K1363010', datetime.datetime(2013, 5, 3, 23, 0), 2006,
          None, None, None, None, None, None, None): 136}

        Exemple de prévision brute - Cas Hydro2

        >>> content = reader.read_serie_hydro2(
        ...     codes=['K1251810', 'K1363010'],
        ...     first_dt=dt(2013, 5, 3, 12),
        ...     last_dt=dt(2013, 5, 3, 23),
        ...     valid=False
        ... )
        >>> content
        {('K1251810', datetime.datetime(2013, 5, 3, 23, 0), 2001,
          True, True, True): 91,
         ('K1251810', datetime.datetime(2013, 5, 3, 23, 0), 2002,
          True, True, True): 92,
         ('K1251810', datetime.datetime(2013, 5, 3, 23, 0), 2003,
          True, True, True): 93,
         ('K1251810', datetime.datetime(2013, 5, 3, 23, 0), 2004,
          True, True, True): 94,
         ('K1251810', datetime.datetime(2013, 5, 3, 23, 0), 2005,
          True, True, True): 95,
         ('K1251810', datetime.datetime(2013, 5, 3, 23, 0), 2006,
          True, True, True): 96,
         ('K1251810', datetime.datetime(2013, 5, 3, 23, 0), 2007,
          True, True, True): 100,
         ('K1251810', datetime.datetime(2013, 5, 3, 23, 0), 2008,
          True, True, True): 98,
         ('K1251810', datetime.datetime(2013, 5, 3, 23, 0), 2009,
          True, True, True): 99,
         ('K1363010', datetime.datetime(2013, 5, 3, 23, 0), 2001,
          True, True, True): 131,
         ('K1363010', datetime.datetime(2013, 5, 3, 23, 0), 2002,
          True, True, True): 132,
         ('K1363010', datetime.datetime(2013, 5, 3, 23, 0), 2003,
          True, True, True): 133,
         ('K1363010', datetime.datetime(2013, 5, 3, 23, 0), 2004,
          True, True, True): 134,
         ('K1363010', datetime.datetime(2013, 5, 3, 23, 0), 2005,
          True, True, True): 135,
         ('K1363010', datetime.datetime(2013, 5, 3, 23, 0), 2006,
          True, True, True): 136,
         ('K1363010', datetime.datetime(2013, 5, 3, 23, 0), 2007,
          True, True, True): 140,
         ('K1363010', datetime.datetime(2013, 5, 3, 23, 0), 2008,
          True, True, True): 138,
         ('K1363010', datetime.datetime(2013, 5, 3, 23, 0), 2009,
          True, True, True): 139}

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.check_listlike(codes)
        # ---------------------------------------------------------------------
        # 1- Requête SQL
        # ---------------------------------------------------------------------
        # définition des éléments de la requête SQL
        atts = P14_ATTS_META[('hydro2', valid)]
        sql_atts = ",".join(['"' + att + '"' for att in atts])
        sql_codes = ",".join(["'" + code + "'" for code in codes])
        # Séries validées/expertisées
        table = P14_TABLES_META[valid]
        # définition de la requête SQL
        self.sql = P14_SQL_META[('hydro2', valid)].format(
            table, sql_atts, atts[1], sql_codes)
        if isinstance(first_dt, dt):
            self.sql += " AND ({0}.{1} >= {2})".format(
                table, atts[2], self.from_datetime(first_dt, "%Y%m%d%H", -60))
        if isinstance(last_dt, dt):
            self.sql += " AND ({0}.{1} <= {2})".format(
                table, atts[2], self.from_datetime(last_dt, "%Y%m%d%H", +60))
        # ---------------------------------------------------------------------
        # 2- Appliquer la requête SQL SERIE
        # ---------------------------------------------------------------------
        self.connect()
        series = self.execute(warning=warning)
        self.close()
        # ---------------------------------------------------------------------
        # 3- Traitement du résultat de la requête SQL
        # ---------------------------------------------------------------------
        if self.check_sql_return(content=series, warning=warning) is None:
            return None
        data = collections.OrderedDict()
        for serie in series:
            meta = tuple(serie[1:])
            data[meta] = serie[0]
        # ---------------------------------------------------------------------
        # 4- Retour
        # ---------------------------------------------------------------------
        return data

    def read_fcst_hydro3(self, codes=None, valid=False, first_dt=None,
                         last_dt=None, warning=True):
        """
        Récupération des prévisions de la base Prevision14

        Parameters
        ----------
        codes : list
            Codes Hydro2/Hydro3 des stations
        valid : bool
            Seulement les prévisions validées (True)
            ou toutes les prévisions produites (False)
            Défaut: False
        first_dt : datetime
            Premier instant de prévision
        last_dt : datetime
            Dernier instant de prévision
        warning : bool
            Affiche les avertissement si True. Défaut: True

        Returns
        -------
        pandas.DataFrame
            Tableau des données prévues

        Examples
        --------



        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.check_listlike(codes)
        # ---------------------------------------------------------------------
        # 1- Appliquer la requête SQL SERIE
        # ---------------------------------------------------------------------
        series = self.read_serie_hydro3(
            codes=codes, first_dt=first_dt, last_dt=last_dt, valid=valid,
            warning=warning)
        if self.check_sql_return(content=series, warning=warning) is None:
            return None
        # ---------------------------------------------------------------------
        # 2- Appliquer la requête SQL DATA
        # ---------------------------------------------------------------------
        # ---------------------------------------------------------------------
        # 2.1- Connexion base Access
        # ---------------------------------------------------------------------
        self.connect()
        # ---------------------------------------------------------------------
        # 2.2- Rechercher les prévisions
        # ---------------------------------------------------------------------
        dfs = collections.OrderedDict()
        atts = P14_ATTS_DATA['hydro3']
        sql_atts = ",".join(['"' + att + '"' for att in atts])
        table = P14_TABLES_DATA[valid]
        for meta, nserie in series.items():
            self.sql = P14_SQL_DATA['hydro3'].format(
                table, sql_atts, atts[0], nserie)
            content = self.execute(warning=warning)
            if self.check_sql_return(content=content, warning=warning) is None:
                continue
        # ---------------------------------------------------------------------
        # 2.3- Tableau de données
        # ---------------------------------------------------------------------
            df = {c: [x[k] for x in content] for k, c in enumerate(atts)}
            df = pnd.DataFrame(df)
            # Retirer la colonne NSerie
            df = df.drop(atts[0], axis=1)
            # Définir la date
            c = atts[1]
            df[c] = [meta[1] + i*td(hours=1) for i in df[c]]
            # Définir l'index par la colonnes DateVal
            df = df.set_index(keys=c, drop=True)
            # Stockage dans le dictionnaire
            dfs[meta[:3]] = df
        # ---------------------------------------------------------------------
        # 2.4- Déconnexion base Access
        # ---------------------------------------------------------------------
        self.close()
        # ---------------------------------------------------------------------
        # 4- Retour
        # ---------------------------------------------------------------------
        return dfs

    def read_serie_hydro3(self, codes=None, valid=None,
                          first_dt=None, last_dt=None, warning=True):
        """
        Récupération des séries de la base Prevision14

        Parameters
        ----------
        codes : list
            Codes Hydro2/Hydro3 des stations
        valid : bool
            Seulement les prévisions validées (True)
            ou toutes les prévisions produites (False)
            Défaut: False
        first_dt : datetime
            Premier instant de prévision
        last_dt : datetime
            Dernier instant de prévision
        warning : bool
            Affiche les avertissement si True. Défaut: True

        Returns
        -------
        dict
            Dictionnaire des méta-données des séries {méta: nserie}

        Examples
        --------
        >>> from datetime import datetime as dt
        >>> from pyspc.data.prevision import Prevision14
        >>> f = 'data/io/dbase/prevision_2014.mdb'
        >>> reader = Prevision14(filename=f)

        Exemple de prévision validée - Cas Hydro3

        >>> content = reader.read_serie_hydro3(
        ...     codes=['K6373020', 'K6402520', 'K6453010'],
        ...     first_dt=dt(2016, 5, 31, 12),
        ...     last_dt=dt(2016, 5, 31, 18),
        ...     valid=True
        ... )
        >>> content
        {
            ('K6373020', datetime.datetime(2016, 5, 31, 13, 0), 2001): 10262,
            ('K6402520', datetime.datetime(2016, 5, 31, 12, 0), 3002): 10267,
            ('K6453010', datetime.datetime(2016, 5, 31, 15, 0), 2001): 10391,
            ('K6373020', datetime.datetime(2016, 5, 31, 16, 0), 2001): 10426,
            ('K6402520', datetime.datetime(2016, 5, 31, 16, 0), 3002): 10430,
            ('K6402520', datetime.datetime(2016, 5, 31, 18, 0), 3001): 10477,
        }

        Exemple de prévision brute - Cas Hydro3

        >>> content = reader.read_serie_hydro3(
        ...     codes=['K6373020', 'K6402520', 'K6453010'],
        ...     first_dt=dt(2016, 5, 31, 12),
        ...     last_dt=dt(2016, 5, 31, 18),
        ...     valid=False
        ... )
        >>> content
        {
            ('K6373020', datetime.datetime(2016, 5, 31, 13, 0), 2001): 10262,
            ('K6402520', datetime.datetime(2016, 5, 31, 12, 0), 3002): 10267,
            ('K6453010', datetime.datetime(2016, 5, 31, 15, 0), 2001): 10391,
            ('K6373020', datetime.datetime(2016, 5, 31, 16, 0), 2001): 10426,
            ('K6402520', datetime.datetime(2016, 5, 31, 16, 0), 3002): 10430,
            ('K6402520', datetime.datetime(2016, 5, 31, 18, 0), 3001): 10477,
        }

        """
        # ---------------------------------------------------------------------
        # 0- Contrôles
        # ---------------------------------------------------------------------
        _exception.check_listlike(codes)
        # ---------------------------------------------------------------------
        # 1- Requête SQL
        # ---------------------------------------------------------------------
        # définition des éléments de la requête SQL
        atts = P14_ATTS_META['hydro3']
        sql_atts = ",".join(['"' + att + '"' for att in atts])
        sql_codes = ",".join(["'" + code + "'" for code in codes])
        # Séries validées/expertisées
        table = P14_TABLES_META[valid]
        # définition de la requête SQL
        self.sql = P14_SQL_META['hydro3'].format(
            table, sql_atts, atts[1], sql_codes)
        if isinstance(first_dt, dt):
            self.sql += " AND ({0}.{1} >= {2})".format(
                table, atts[2], self.from_datetime(first_dt, "%Y%m%d%H", -60))
        if isinstance(last_dt, dt):
            self.sql += " AND ({0}.{1} <= {2})".format(
                table, atts[2], self.from_datetime(last_dt, "%Y%m%d%H", +60))
        # ---------------------------------------------------------------------
        # 2- Appliquer la requête SQL SERIE
        # ---------------------------------------------------------------------
        self.connect()
        series = self.execute(warning=warning)
        self.close()
        # ---------------------------------------------------------------------
        # 3- Traitement du résultat de la requête SQL
        # ---------------------------------------------------------------------
        if self.check_sql_return(content=series, warning=warning) is None:
            return None
        data = collections.OrderedDict()
        for serie in series:
            meta = tuple(serie[1:])
            data[meta] = serie[0]
        # ---------------------------------------------------------------------
        # 4- Retour
        # ---------------------------------------------------------------------
        return data

    @classmethod
    def get_datatypes(cls):
        """
        Type de données des bases Prevision 2014

        Returns
        -------
        list
            Type de données des bases Prevision 2014

        """
        return sorted(P14_DATATYPES)
