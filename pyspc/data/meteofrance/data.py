#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Données d'observation et de prévision - Météo-France - Données observées
"""
import pandas as pnd

import pyspc.core.exception as _exception


class MF_Data():
    """
    Structure de données d'observation de Météo-France

    Attributes
    ----------
    filename : str
        Nom du fichier

    """

    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe MF_Data

        Parameters
        ----------
        filename : str
            Nom du fichier

        """
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance MF_Data
        """
        text = """
        *************************************
        *********** MF - DATA ***************
        *************************************
        *  NOM FICHIER      = {filename}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lecture du fichier de données MF

        Returns
        -------
        pandas.DataFrame
            Tableau des données

        Examples
        --------
        >>> from pyspc.data.meteofrance import MF_Data
        >>> f = 'data/data/mf/RR6.data'
        >>> reader = MF_Data(filename=f)
        >>> content = reader.read()
        >>> content
               POSTE          DATE   RR6 QRR6
        0   43111002  201706131700   0.0    v
        1   43111002  201706131706   1.0    v
        2   43111002  201706131712   0.2    v
        3   43111002  201706131718   0.4    v
        4   43111002  201706131724   0.4    v
        5   43111002  201706131730   0.4    v
        6   43111002  201706131736   2.2    v
        7   43111002  201706131742  11.7    v
        8   43111002  201706131748  13.3    v
        9   43111002  201706131754   8.2    v
        10  43111002  201706131800   6.3    v
        11  43111002  201706131806  13.4    v
        12  43111002  201706131812  14.2    v
        13  43111002  201706131818  12.9    v
        14  43111002  201706131824  21.5    v
        15  43111002  201706131830  14.1    v
        16  43111002  201706131836  17.0    v
        17  43111002  201706131842  17.0    v
        18  43111002  201706131848   8.0    v
        19  43111002  201706131854   2.8    v
        20  43111002  201706131900   1.8    v
        21  43111002  201706131906   6.1    v
        22  43111002  201706131912  11.2    v
        23  43111002  201706131918   6.6    v
        24  43111002  201706131924   3.5    v
        25  43111002  201706131930   1.6    v
        26  43111002  201706131936   0.4    v
        27  43111002  201706131942   0.6    v
        28  43111002  201706131948   5.0    v
        29  43111002  201706131954   1.8    v

        """
        return pnd.read_csv(
            self.filename,
            sep=';',
            header=0,
            index_col=False,
            converters={'POSTE': str, 'DATE': str},
            decimal=','
        )

    def write(self, data=None):
        """
        Ecrire le fichier de données MeteoFrance
        à partir d'un dictionnaire

        Parameters
        ----------
        data : pandas.DataFrame
            Tableau des données

        """
        _exception.check_dataframe(data)
        data.to_csv(
            self.filename,
            sep=';',
            float_format='%.1f',  # '%.1f' # %g
            header=True,
            index=False,
            line_terminator='\r\n',
            decimal=',',
        )
