#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Données d'observation et de prévision - Météo-France - OPEN DATA
"""
from datetime import datetime as dt
import os.path
import pandas as pnd

from pyspc.convention.meteofrance import API_PREFIX


class MF_OpenAPI():
    """
    Lecteur des données de l'API de Météo-France.

    Attributes
    ----------
    filename : str
        Fichier de données.
    prefix : str
        Préfixe du fichier de données.
    timestep : datetime.timedelta
        Pas de temps des données.
    code : str
        Identifiant de la station de mesure.
    start : str
        Premier instant.
    end : str
        Dernier instant.

    """
    def __init__(self, filename=None):
        """
        Instanciation du lecteur du fichier METEO.DATA.GOUV.FR

        Parameters
        ----------
        filename : str
            Fichier de données

        Examples
        --------
        >>> from pyspc.data.meteofrance import MF_OpenAPI
        >>> f = '43091005_Q_202410150000_202410190000.csv'
        >>> reader = MF_OpenAPI(filename=f)
        >>> reader
        *******************************************
        *********** MF - OPEN API *****************
        *******************************************
        *  NOM FICHIER      = 43091005_Q_202410150000_202410190000.csv
        *  PREFIXE          = Q
        *  PAS DE TEMPS     = 1 day, 0:00:00
        *  STATION          = 43091005
        *  DEBUT            = 2024-10-15 00:00:00
        *  FIN              = 2024-10-19 00:00:00
        *************************************

        """
        self.filename = filename
        info = self.split_basename(self.filename)
        self.code = info[0]
        self.prefix = info[1]
        self.timestep = info[2]
        self.start = info[3]
        self.end = info[4]

    def __str__(self):
        """
        Afficher les méta-données de l'instance.
        """
        text = """
        *******************************************
        *********** MF - OPEN API *****************
        *******************************************
        *  NOM FICHIER      = {filename}
        *  PREFIXE          = {prefix}
        *  PAS DE TEMPS     = {timestep}
        *  STATION          = {code}
        *  DEBUT            = {start}
        *  FIN              = {end}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lecture du fichier de données.

        Returns
        -------
        pandas.DataFrame
            Tableau des données

        Examples
        --------
        >>> from pyspc.data.meteofrance import MF_OpenAPI
        >>> f = '43091005_Q_202410150000_202410190000.csv'
        >>> reader = MF_OpenAPI(filename=f)
        >>> content = reader.read()
        >>> content
              POSTE       DATE     RR    TN   HTN    TX   HTX    TM   TMNX  DG TAMPLI TNTXM ETPGRILLE
        0  43091005 2024-10-15   38,6  12,4  1855  18,0  1156  14,1  15,20   0    5,6  15,2       1,9
        1  43091005 2024-10-16  189,0  11,5   217  13,3  1411  12,4  12,40   0    1,8  12,4       0,1
        2  43091005 2024-10-17  130,4   9,8   757  13,0  1302  11,5  11,40   0    3,2  11,4       0,1
        3  43091005 2024-10-18    2,4   5,3   644  10,7  1207   8,2   8,00   0    5,4   8,0       0,4
        4  43091005 2024-10-19    0,0   7,2   618  13,6  1416   9,0  10,40   0    6,4  10,4       1,1

        """
        converters = {'POSTE': lambda x: str(x).strip(), 'DATE': str}
        date_parser = str
        if self.prefix == 'MN':
            date_parser = _date_parser_MN
        elif self.prefix == 'H':
            date_parser = _date_parser_H
        elif self.prefix == 'Q':
            date_parser = _date_parser_Q
        df = pnd.read_csv(self.filename, sep=';', header=0, index_col=0,
                          decimal=',', converters=converters)
        df = df.dropna(axis=1, how='all')
        df['DATE'] = df['DATE'].apply(date_parser)
        return df

    @staticmethod
    def split_basename(filename=None):
        """
        Extraire les informations depuis le nom du fichier.

        Parameters
        ----------
        filename : str
            Fichier de données.

        Returns
        -------
        prefix : str
            Préfixe du fichier de données.
        timestep : datetime.timedelta
            Pas de temps des données.
        code : str
            Identifiant de la station de mesure.
        start : str
            Premier instant.
        end : str
            Dernier instant.

        Examples
        --------
        >>> from pyspc.data.meteofrance import MF_OpenAPI
        >>> f = '43091005_Q_202410150000_202410190000.csv'
        >>> reader = MF_OpenAPI.split_basename(filename=f)
        >>> [code, prefix, timestep, start, end] = \
        ... MF_OpenData.split_basename(filename=f)

        >>> code
        43091005

        >>> prefix
        Q

        >>> timestep
        datetime.timedelta(days=1)

        >>> start
        datetime.datetime(2024, 10, 15)

        >>> end
        datetime.datetime(2024, 10, 19)

        """
        inv_api_prefix = {v: k for k, v in API_PREFIX.items()}
        if filename is None:
            return None, None, None, None, None
        basename = os.path.splitext(os.path.basename(filename))[0]
        try:
            info = basename.split('_')
            code = info.pop(0)
            prefix = info.pop(0)
            start = dt.strptime(info.pop(0), "%Y%m%d%H%M")
            end = dt.strptime(info.pop(0), "%Y%m%d%H%M")
            timestep = inv_api_prefix[prefix]
        except (IndexError, ValueError) as ive:
            raise ValueError("Nommage incorrect.") from ive
        except KeyError as ke:
            raise ValueError(f"Le préfixe '{prefix}' est inconnu") from ke
        return code, prefix, timestep, start, end


def _date_parser_MN(x):
    """"""
    try:
        return dt.strptime(x, '%Y%m%d%H%M')
    except ValueError:
        return x


def _date_parser_H(x):
    """"""
    try:
        return dt.strptime(x, '%Y%m%d%H')
    except ValueError:
        return x


def _date_parser_Q(x):
    """"""
    try:
        return dt.strptime(x, '%Y%m%d')
    except ValueError:
        return x
