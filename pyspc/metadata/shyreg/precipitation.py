#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Méta-données (lieux, tronçons, statistiques) - SHYREG - Pluie
"""
import os.path
import pandas as pnd


class Precipitation():
    """
    Structure liée aux fiches SHYREG - Pluie
    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe Precipitation

        Parameters
        ----------
        filename : str
            Fichier local du csv SHYREG Pluie

        """
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance <Precipitation>
        """
        text = """
        *************************************
        ********* SHYREG - Precipitation ****
        *************************************
        *  FICHIER          = {filename}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lire le rapport 'shyreg'

        Returns
        -------
        stats : pnd.DataFrame
            Tableau des valeurs statistiques

        Examples
        --------
        >>> from pyspc.metadata.shyreg.precipitation import Precipitation
        >>> f = 'data/metadata/shyreg/Export_SHYREG.csv'
        >>> shyreg = Precipitation(filename=f)
        >>> df = shyreg.read()
        >>> df
                X        Y  PM01.1000  PM01.0500  ...  PM72.0005  PM72.0002
        1  836000  6475000       84.4       76.8  ...      105.6       86.2
        2  837000  6475000       85.3       77.8  ...      105.7       86.3
        3  838000  6475000       85.2       77.6  ...      104.9       85.6
        4  835000  6474000       84.7       77.3  ...      107.1       87.6
        5  836000  6474000       84.8       77.3  ...      107.0       87.5

        [5 rows x 74 columns]

        """
        if not os.path.exists(self.filename):
            return None
        return pnd.read_csv(self.filename, sep=";", index_col=0, decimal=',')
