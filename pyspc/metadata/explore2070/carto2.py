#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Méta-données (lieux, tronçons, statistiques) - Explore2070
"""
import os.path
import pandas as pnd


class Carto2_List():
    """
    Structure liée aux fiches Explore2070
    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe Carto2_List

        Parameters
        ----------
        filename : str
            Fichier local de la liste des fiches Explore2070

        """
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance <Carto2_List>
        """
        text = """
        *************************************
        ****** Explore2070 - Carto2_List ****
        *************************************
        *  FICHIER          = {filename}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lire le rapport 'shyreg'

        Returns
        -------
        stats : pnd.DataFrame
            Tableau des valeurs statistiques

        Examples
        --------
        >>> from pyspc.metadata.explore2070.carto2 import Carto2_List
        >>> f = 'data/_bin/onlineReport/in/liste_explore2070.txt'
        >>> carto2 = Carto2_List(filename=f)
        >>> df = carto2.read()
        >>> df
                Lien Fiche        Nom commun SourceGeo Num fiche
        Explore ID
        653           Lien            COUBON Sim       653  (COUBON )
        654           Lien  BRIVES-CHARENSAC  Gr       654  (BRIVES-CHARENSAC )
        655           Lien        LE MONTEIL  Gr       655  (LE MONTEIL )

        """
        if not os.path.exists(self.filename):
            return None
        return pnd.read_csv(self.filename, sep="\t", index_col=0, skiprows=2)
