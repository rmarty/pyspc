#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Méta-données (lieux, contact, courbe tarage) - XML Sandre
"""
from pyspc.convention.sandre import DATATYPES

DATATYPES = sorted([d for d in DATATYPES
                    if d.startswith('loc')
                    or d.startswith('ratingcurve')
                    or d.startswith('user')])


class Sandre():
    """
    Structure liée aux méta-données XML Sandre
    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe XML Sandre

        Parameters
        ----------
        filename : str
            Fichier local du flux GeoJSON de Vigicrues

        """
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance XML Sandre
        """
        text = """
        *************************************
        *********** XML Sandre - Metadata ***
        *************************************
        *  FICHIER          = {filename}
        *************************************
        """
        return text.format(**vars(self))

    def read(self):
        """
        Lire le fichier XML du Sandre

        Examples
        --------
        >>> import pyspc.metadata.sandre as _sandre

        Exemple de fichier de sites hydro

        >>> f = 'data/metadata/sandre/SiteHydro.xml'
        >>> sandre = _sandre.Sandre(filename=f)
        >>> content = sandre.read()
        >>> content
        Message du 2017-12-11 15:54:06 de version 1.1
         Emetteur: Intervenant SANDRE 1537::<sans nom> [0 contact] [None]
        Destinataire: Intervenant SIRET 13000930100016::<sans nom>
        [1 contact] [Contact 398::<sans civilite> <sans nom> <sans prenom>
        [intervenant 13000930100016]]
        Contenu:
            0 intervenants
            2 siteshydro
            0 sitesmeteo
            0 seuilshydro
            0 seuilsmeteo
            0 modelesprevision
            0 evenements
            0 courbestarage
            0 jaugeages
            0 courbescorrection
            0 serieshydro
            0 seriesmeteo
            0 seriesobselab
            0 simulations

        >>> for s in content.siteshydro:
        ...     print(s)
        Site STANDARD K0550010::La Loire à Bas-en-Basset [0 station]
        Site STANDARD K0260010::La Loire à Chadrac [0 station]

        Exemple de fichier de stations hydro

        >>> f = 'data/metadata/sandre/StationHydro.xml'
        >>> sandre = _sandre.Sandre(filename=f)
        >>> content = sandre.read()
        >>> content
        Message du 2017-12-11 15:54:06 de version 1.1
         Emetteur: Intervenant SANDRE 1537::<sans nom> [0 contact] [None]
        Destinataire: Intervenant SIRET 13000930100016::<sans nom>
        [1 contact] [Contact 398::<sans civilite> <sans nom> <sans prenom>
        [intervenant 13000930100016]]
        Contenu:
            0 intervenants
            1 siteshydro
            0 sitesmeteo
            0 seuilshydro
            0 seuilsmeteo
            0 modelesprevision
            0 evenements
            0 courbestarage
            0 jaugeages
            0 courbescorrection
            0 serieshydro
            0 seriesmeteo
            0 seriesobselab
            0 simulations
        >>> for s in content.siteshydro:
        ...     print(s)
        ...     for s2 in s.stations:
        ...         print(s2)
        Site STANDARD K4350010::<sans libelle> [2 stations]
        Station STD K435001020::La Loire à Orléans - Quai du Roi [0 capteur]
        Station STD K435001010::La Loire à Orléans - Pont Royals [0 capteur]

        Exemple de fichier de sites météo

        >>> f = 'data/metadata/sandre/SiteMeteo.xml'
        >>> sandre = _sandre.Sandre(filename=f)
        >>> content = sandre.read()
        >>> content
        Message du 2018-09-13 09:19:21 de version 1.1
         Emetteur: Intervenant SANDRE 1537::<sans nom> [0 contact] [None]
        Destinataire: Intervenant SANDRE 0::<sans nom> [0 contact] [None]
        Contenu:
            0 intervenants
            0 siteshydro
            3 sitesmeteo
            0 seuilshydro
            0 seuilsmeteo
            0 modelesprevision
            0 evenements
            0 courbestarage
            0 jaugeages
            0 courbescorrection
            0 serieshydro
            0 seriesmeteo
            0 seriesobselab
            0 simulations

        >>> for s in content.siteshydro:
        ...     print(s)
        Sitemeteo 043130002::MAZET-VOLAMONT [2 grandeurs]
        Sitemeteo 043091005::LES ESTABLES_SAPC [2 grandeurs]
        Sitemeteo 043051003::LE-CHAMBON-SUR-LIGNON-SPC [1 grandeur]

        Exemple de fichier de courbes de tarage

        >>> f = 'data/metadata/sandre/RatingCurves.xml'
        >>> sandre = _sandre.Sandre(filename=f)
        >>> content = sandre.read()
        >>> content
        Message du 2018-09-26 17:22:46 de version 1.1
         Emetteur: Intervenant SANDRE 1537::<sans nom> [0 contact] [None]
        Destinataire: Intervenant SIRET 13000930100016::<sans nom>
        [1 contact] [Contact 398::<sans civilite> <sans nom> <sans prenom>
        [intervenant 13000930100016]]
        Contenu:
            0 intervenants
            0 siteshydro
            0 sitesmeteo
            0 seuilshydro
            0 seuilsmeteo
            0 modelesprevision
            0 evenements
            2 courbestarage
            0 jaugeages
            0 courbescorrection
            0 serieshydro
            0 seriesmeteo
            0 seriesobselab
            0 simulations
        >>> for s in content.courbestarage:
        ...     print(s)
        Courbe de tarage de type fonction puissance
        10016::H201416 [8 points pivot]
        Courbe de tarage de type fonction puissance
        10017::H201620 [7 points pivot]

        Exemple de fichier 'user_admin'

        >>> f = 'data/metadata/sandre/SiteHydro.xml'
        >>> sandre = _sandre.Sandre(filename=f)
        >>> content = sandre.read()
        >>> content
        Message du 2019-08-06 09:07:44 de version 1.1
         Emetteur: Intervenant SANDRE 0000::<sans nom> [0 contact] [None]
        Destinataire: Intervenant SANDRE 1111::<sans nom> [1 contact]
        [Contact 404::<sans civilite> <sans nom> <sans prenom>
        [intervenant 1111]]
        Contenu:
            1 intervenants
            0 siteshydro
            0 sitesmeteo
            0 seuilshydro
            0 seuilsmeteo
            0 modelesprevision
            0 evenements
            0 courbestarage
            0 jaugeages
            0 courbescorrection
            0 serieshydro
            0 seriesmeteo
            0 seriesobselab
            0 simulations

        >>> for i in content.intervenants:
        ...     print(i)
        Intervenant SANDRE 1111::Service de Prévision des Crues (SPC) X-Y-Z
        [1 contact]


        Exemple de fichier 'user_loc_meteo'

        >>> f = 'data/metadata/sandre/SiteHydro.xml'
        >>> sandre = _sandre.Sandre(filename=f)
        >>> content = sandre.read()
        >>> content
        Message du 2019-08-06 09:07:44 de version 1.1
         Emetteur: Intervenant SANDRE 0000::<sans nom> [0 contact] [None]
        Destinataire: Intervenant SANDRE 1111::<sans nom> [1 contact]
        [Contact 404::<sans civilite> <sans nom> <sans prenom>
        [intervenant 1111]]
        Contenu:
            0 intervenants
            0 siteshydro
            5 sitesmeteo
            0 seuilshydro
            0 seuilsmeteo
            0 modelesprevision
            0 evenements
            0 courbestarage
            0 jaugeages
            0 courbescorrection
            0 serieshydro
            0 seriesmeteo
            0 seriesobselab
            0 simulations

        Exemple de fichier 'user_site_hydro'

        >>> f = 'data/metadata/sandre/SiteHydro.xml'
        >>> sandre = _sandre.Sandre(filename=f)
        >>> content = sandre.read()
        >>> content
        Message du 2019-08-06 09:07:44 de version 1.1
         Emetteur: Intervenant SANDRE 0000::<sans nom> [0 contact] [None]
        Destinataire: Intervenant SANDRE 1111::<sans nom> [1 contact]
        [Contact 404::<sans civilite> <sans nom> <sans prenom>
        [intervenant 1111]]
        Contenu:
            0 intervenants
            6 siteshydro
            0 sitesmeteo
            0 seuilshydro
            0 seuilsmeteo
            0 modelesprevision
            0 evenements
            0 courbestarage
            0 jaugeages
            0 courbescorrection
            0 serieshydro
            0 seriesmeteo
            0 seriesobselab
            0 simulations

        Exemple de fichier 'user_station_hydro'

        >>> f = 'data/metadata/sandre/SiteHydro.xml'
        >>> sandre = _sandre.Sandre(filename=f)
        >>> content = sandre.read()
        >>> content
        Message du 2019-08-06 09:07:44 de version 1.1
         Emetteur: Intervenant SANDRE 0000::<sans nom> [0 contact] [None]
        Destinataire: Intervenant SANDRE 1111::<sans nom> [1 contact]
        [Contact 404::<sans civilite> <sans nom> <sans prenom>
        [intervenant 1111]]
        Contenu:
            0 intervenants
            2 siteshydro
            0 sitesmeteo
            0 seuilshydro
            0 seuilsmeteo
            0 modelesprevision
            0 evenements
            0 courbestarage
            0 jaugeages
            0 courbescorrection
            0 serieshydro
            0 seriesmeteo
            0 seriesobselab
            0 simulations

        See Also
        --------
        Repose sur la libhydro :
        https://gitlab.com/vigicrues/hydro3/libhydro

        """
        from libhydro.conv import xml as _xml
        return _xml.Message.from_file(self.filename)

    def check_dtype(self, dtype):
        """
        Contrôler s'il s'agit bien d'une méta-donnée autorisée

        Parameters
        ----------
        dtype : str
            Type de méta-donnée à contrôler

        """
        try:
            DATATYPES.index(dtype)
        except ValueError as ve:
            raise ValueError("Export incorrect") from ve

    @classmethod
    def get_types(cls):
        """
        Renvoyer la liste des méta-données du XML Sandre

        Returns
        -------
        list
            Types de méta-données de la XML Sandre

        """
        return sorted(DATATYPES)
