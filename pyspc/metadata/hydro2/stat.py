#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Méta-données (statistiques, synthèse) - Hydro2
"""
from pyspc.convention.hydro2 import (
    DATATYPES, TR,
    LINECONTENTS, SEP, LINESTOSKIP, PAIRVALUES
)


def _process_946(x):
    """Traiter la ligne 946"""
    d = {}
    values = x[9:]
    if (len(values) - 1 - 4) % 6 != 0:
        return d
    itr = (len(values) - 1 - 4) // 6
    subtr = TR[:itr]
    c = 9
    for v in ['QJ', 'QIX']:
        # Gumbel
        for s in ['x0', 'gradex']:
            k = 'QC_{}_{}'.format(v, s)
            d[k] = x[c]
            c += 1
        # Temps de retour
        for t in TR:
            for s in ['', 'inf', 'sup']:
                k = 'QC_{}_{}{}'.format(v, t, s)
                try:
                    subtr.index(t)
                except ValueError:
                    d[k] = ''
                else:
                    d[k] = x[c]
                    c += 1
    return d


class Hydro2():
    """
    Structure de statistiques HYDRO-2

    Attributes
    ----------
    filename : str
        Nom du fichier Hydro2

    """

    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe Hydro2

        Parameters
        ----------
        filename : str
            Nom du fichier Hydro2

        """
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance Hydro2
        """
        text = """
        *************************************
        ******** HYDRO2 - Statistics ********
        *************************************
        *  NOM FICHIER      = {filename}
        *************************************
        """
        return text.format(filename=self.filename)

    def read(self):
        """
        Lecture du fichier d'export statistiques HYDRO-2

        Returns
        -------
        info : dict
            Dictionnaire des informations par station:
            - clé : station
            - valeur : dictionnaire des statistiques de la station

        Examples
        --------
        >>> from pyspc.metadata.hydro2 import Hydro2

        Cas avec un fichier DEBCLA

        >>> f = 'data/metadata/hydro2/debcla1.txt'
        >>> d = 'DEBCLA'
        >>> reader = Hydro2_Stat(filename=f, datatype=d)
        >>> content = reader.read()
        >>> content
        {
            'K1251810': {
                'area': '776.000',
                'end': '31-08-2020',
                'name': "L'Arroux à Dracy-Saint-Loup [Surmoulin]",
                'provider': 'DREAL Bourgogne',
                'start': '01-09-1948',
                'station': 'K1251810',
                'z0': '291.000',
                '0.0000': '0.020',
                '0.0010': '0.045',
                ''*****': '*****',
                '0.9980': '88.300',
                '0.9990': '100.000',
                '1.0000': '156.000',
            },
            'K1321810': {
                'area': '1800.000',
                'end': '31-08-2020',
                'name': "L'Arroux à Étang-sur-Arroux [Pont du Tacot]",
                'provider': 'DREAL Centre',
                'start': '01-09-1948',
                'station': 'K1321810',
                'z0': '273.000',
                '0.0000': '0.147',
                '0.0010': '0.211',
                ''*****': '*****',
                '0.9980': '233.000',
                '0.9990': '262.000',
                '1.0000': '408.000',
            },
            'K1341810': {
                'area': '2280.000',
                'end': '31-08-2020',
                'name': "L'Arroux à Rigny-sur-Arroux",
                'provider': 'DREAL Centre',
                'start': '01-09-1948',
                'station': 'K1341810',
                'z0': '237.000',
                '0.0000': '0.100',
                '0.0010': '0.286',
                ''*****': '*****',
                '0.9980': '264.000',
                '0.9990': '296.000',
                '1.0000': '391.000',
            }
        }

        Cas avec un fichier CRUCAL

        >>> f = 'data/metadata/hydro2/crucal1.txt'
        >>> d = 'DEBCLA'
        >>> reader = Hydro2_Stat(filename=f, datatype=d)
        >>> content = reader.read()
        >>> content
        {
            'K1251810': {
                'area': '776.000',
                'end': '31-082020',
                'law': 'GUMBEL',
                'name': "L'Arroux à Dracy-Saint-Loup [Surmoulin]",
                'provider': 'DREAL Bourgogne',
                'start': '01-091948',
                'station': 'K1251810',
                'units': 'm3/s',
                'var': 'QIX',
                'z0': '291.000',
                '2': '110.000-98.000-120.000',
                '5': '150.000-140.000-180.000',
                '10': '190.000-170.000-220.000',
                '20': '210.000-190.000-260.000',
                '50': '250.000-220.000-310.000',
                '19850123': '104.000',
                ''*****': '*****',
                '20200306': '122.000',
            },
            'K1321810': {
                'area': '1800.000',
                'end': '31-082020',
                'law': 'GUMBEL',
                'name': "L'Arroux à Étang-sur-Arroux [Pont du Tacot]",
                'provider': 'DREAL Centre',
                'start': '01-091948',
                'station': 'K1321810',
                'units': 'm3/s',
                'var': 'QIX',
                'z0': '273.000',
                '2': '230.000-210.000-250.000',
                '5': '320.000-290.000-360.000',
                '10': '380.000-340.000-440.000',
                '20': '430.000-390.000-510.000',
                '50': '500.000-450.000-600.000',
                '19730214': '108.000',
                ''*****': '*****',
                '20200306': '255.000',
            },
            'K1341810': {
                'area': '2280.000',
                'end': '31-082020',
                'law': 'GUMBEL',
                'name': "L'Arroux à Rigny-sur-Arroux",
                'provider': 'DREAL Centre',
                'start': '01-091948',
                'station': 'K1341810',
                'units': 'm3/s',
                'var': 'QIX',
                'z0': '237.000',
                '2': '230.000-220.000-250.000',
                '10': '370.000-340.000-410.000',
                '20': '420.000-380.000-480.000',
                '5': '320.000-290.000-350.000',
                '50': '490.000-440.000-560.000',
                '19681201': '253.000',
                ''*****': '*****',
                '20200307': '244.000',
            }
        }

        Cas avec un fichier SYNTHESE

        >>> f = 'data/metadata/hydro2/synth1.txt'
        >>> d = 'SYNTHESE'
        >>> reader = Hydro2_Stat(filename=f, datatype=d)
        >>> content = reader.read()
        >>> content
        {
            'K1251810': {
                'DC_01': '0.101',
                ''*****': '*****',
                'QC_QIX_10': '190.000',
                'QC_QIX_100': '',
                'QC_QIX_100inf': '',
                'QC_QIX_100sup': '',
                'QC_QIX_10inf': '170.000',
                'QC_QIX_10sup': '220.000',
                'QC_QIX_2': '110.000',
                'QC_QIX_20': '210.000',
                'QC_QIX_20inf': '190.000',
                'QC_QIX_20sup': '260.000',
                'QC_QIX_2inf': '98.000',
                'QC_QIX_2sup': '120.000',
                'QC_QIX_5': '150.000',
                'QC_QIX_50': '250.000',
                'QC_QIX_50inf': '220.000',
                'QC_QIX_50sup': '310.000',
                'QC_QIX_5inf': '140.000',
                'QC_QIX_5sup': '180.000',
                'QC_QIX_gradex': '40.800',
                'QC_QIX_x0': '93.400',
                ''*****': '*****',
                'QspMM_nb': '37',
                'area': '776.000',
                'name': "L'Arroux à Dracy-Saint-Loup [Surmoulin]",
                'provider': 'DREAL Bourgogne',
                'station': 'K1251810',
                'z0': '291.000'
            },
            'K1321810': {
                'DC_01': '0.630',
                ''*****': '*****',
                'QC_QIX_10': '380.000',
                'QC_QIX_100': '',
                'QC_QIX_100inf': '',
                'QC_QIX_100sup': '',
                'QC_QIX_10inf': '340.000',
                'QC_QIX_10sup': '440.000',
                'QC_QIX_2': '230.000',
                'QC_QIX_20': '430.000',
                'QC_QIX_20inf': '390.000',
                'QC_QIX_20sup': '510.000',
                'QC_QIX_2inf': '210.000',
                'QC_QIX_2sup': '250.000',
                'QC_QIX_5': '320.000',
                'QC_QIX_50': '500.000',
                'QC_QIX_50inf': '450.000',
                'QC_QIX_50sup': '600.000',
                'QC_QIX_5inf': '290.000',
                'QC_QIX_5sup': '360.000',
                'QC_QIX_gradex': '77.600',
                'QC_QIX_x0': '201.000',
                ''*****': '*****',
                'QspMM_AA': '12.3',
                'QspMM_nb': '50',
                'area': '1800.000',
                'name': "L'Arroux à Étang-sur-Arroux [Pont du Tacot]",
                'provider': 'DREAL Centre',
                'station': 'K1321810',
                'z0': '273.000'
            },
            'K1341810': {
                'DC_01': '0.602',
                ''*****': '*****',
                'QC_QIX_10': '370.000',
                'QC_QIX_100': '',
                'QC_QIX_100inf': '',
                'QC_QIX_100sup': '',
                'QC_QIX_10inf': '340.000',
                'QC_QIX_10sup': '410.000',
                'QC_QIX_2': '230.000',
                'QC_QIX_20': '420.000',
                'QC_QIX_20inf': '380.000',
                'QC_QIX_20sup': '480.000',
                'QC_QIX_2inf': '220.000',
                'QC_QIX_2sup': '250.000',
                'QC_QIX_5': '320.000',
                'QC_QIX_50': '490.000',
                'QC_QIX_50inf': '440.000',
                'QC_QIX_50sup': '560.000',
                'QC_QIX_5inf': '290.000',
                'QC_QIX_5sup': '350.000',
                'QC_QIX_gradex': '71.100',
                'QC_QIX_x0': '208.000',
                ''*****': '*****',
                'QspMM_AA': '11.9',
                'QspMM_nb': '54',
                'area': '2280.000',
                'name': "L'Arroux à Rigny-sur-Arroux",
                'provider': 'DREAL Centre',
                'station': 'K1341810',
                'z0': '237.000'
            }
        }

        """
        info = {}
        with open(self.filename, 'r', encoding='cp1252') as f:
            for line in f.readlines():
                x = line.split(';')
                # Lignes à ignorer
                if x[0] in LINESTOSKIP:
                    continue
                # Décomposition de la ligne sous forme de dictionnaire
                try:
                    meta = {k: SEP[x[0]].join([x[i] for i in v])
                            for k, v in LINECONTENTS[x[0]].items()}
                except KeyError:
                    continue
                # Décomposition de la ligne sous forme de paires
                # clé: valeur
                try:
                    ik = SEP[x[0]].join([x[i] for i in PAIRVALUES[x[0]][0]])
                    iv = SEP[x[0]].join([x[i] for i in PAIRVALUES[x[0]][1]])
                    meta2 = {ik: iv}
                except KeyError:
                    pass
                else:
                    meta.update(meta2)
                # Spécificité de la ligne 925 (SYNTHESE)
                if x[0] == '925' and len(x) != 21:
                    meta['MX_HIX'] = ''
                    meta['MX_HIXdt'] = ''
                # Spécificité de la ligne 946 (SYNTHESE)
                elif x[0] == '946':
                    meta.update(_process_946(x))
                # Mise-à-jour des statistiques de la station
                s = meta['station']
                info.setdefault(s, {})
                info[s].update(meta)
        return info

    def write(self):
        """
        Ecrire le fichier d'export statistiques HYDRO-2

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @classmethod
    def get_datatypes(cls):
        """
        Définir le type de procédure d'export statistique Hydro-2

        Returns
        -------
        list
             Liste des procédures d'export statistique Hydro-2

        """
        return sorted(DATATYPES['metadata'])
