#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Méta-données (lieux, tronçons, statistiques) - Vigicrues - Tronçon
"""
from datetime import datetime as dt
import json


class Vigicrues_Reach():
    """
    Structure liée aux tronçons Vigicrues
    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe Vigicrues_Reach

        Parameters
        ----------
        filename : str
            Fichier local du flux GeoJSON de Vigicrues

        """
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance <Vigicrues>
        """
        text = """
        *************************************
        *********** VIGICRUES - Reach *******
        *************************************
        *  FICHIER          = {filename}
        *************************************
        """
        return text.format(**vars(self))

    def read(self, content=None):
        """
        Lire le résultat GeoJson des tronçons de Vigicrues
        - si Vigicrues_Reach.filename est défini, alors le contenu est lu
          directement depuis ce fichier
        - sinon le contenu est celui défini par le paramètre content

        Parameters
        ----------
        content : str
            - Si cela correspond à un fichier, celui-ci est lu
              i.e, correspond au retour de urlretrieve
            - Sinon, il est considéré comme étant le résultés de urlopen

        Returns
        -------
        data : dict
            Contenu du flux GeoJSON

        Examples
        --------
        >>> import pyspc.metadata.vigicrues as _vigicrues
        >>> f = 'data/metadata/vigicrues/troncons.json'
        >>> vigi = _vigicrues.Vigicrues_Reach(filename=f)
        >>> content = vigi.read()

        Dans le véritable geojson, l'élément 'coordinates'
        correspond à la liste des points x,y

        >>> content
        {'type': 'FeatureCollection',
         'crs': {'type': 'name',
                 'properties': {'name': 'urn:ogc:def:crs:EPSG::2154'}},
         'VersionFlux': 1,
         'CdIntervenant': 'id.eaufrance.fr/int/1537',
         'UriScenarioSandre': 'id.eaufrance.fr/scn/pcru_geojson/1',
         'RefInfoVigiCru': '22092018_10',
         'DtHrInfoVigiCru': datetime.datetime(2018, 9, 22, 7, 50),
         'features': [{'type': 'Feature',
           'properties': {'gid': 117,
            'CdEntVigiCru': 'LC110',
            'NomEntVigiCru': 'Haut bassin de la Loire',
            'CdTCC': 10,
            'NivSituVigiCruEnt': 1},
           'geometry': {'type': 'MultiLineString', 'coordinates': []},
           'id': 117},
          {'type': 'Feature',
           'properties': {'gid': 118,
            'CdEntVigiCru': 'LC120',
            'NomEntVigiCru': 'Loire forézienne',
            'CdTCC': 10,
            'NivSituVigiCruEnt': 1},
           'geometry': {'type': 'MultiLineString', 'coordinates': []},
           'id': 118}]}

        """
        if self.filename is not None:
            with open(self.filename, 'r', encoding='utf-8') as j:
                data = json.load(j)
        else:
            data = json.loads(content)
        data['DtHrInfoVigiCru'] = dt.strptime(
            data['DtHrInfoVigiCru'].replace('+00:00', ''),
            '%Y-%m-%dT%H:%M:%S'
        )
        return data
