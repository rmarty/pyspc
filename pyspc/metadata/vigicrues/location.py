#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Méta-données (lieux, tronçons, statistiques) - Vigicrues - Lieu, Station
"""
import json


class Vigicrues_Location():
    """
    Structure liée aux stations Vigicrues
    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe Vigicrues_Location

        Parameters
        ----------
        filename : str
            Fichier local du flux json de Vigicrues

        """
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance <Vigicrues>
        """
        text = """
        *************************************
        *********** VIGICRUES - Location ****
        *************************************
        *  FICHIER          = {filename}
        *************************************
        """
        return text.format(**vars(self))

    def read(self, content=None):
        """
        Lire le résultat du flux json de Vigicrues
        - si Vigicrues_Location.filename est défini, alors le contenu est lu
          directement depuis ce fichier
        - sinon le contenu est celui défini par le paramètre content

        Parameters
        ----------
        content : str
            - Si cela correspond à un fichier, celui-ci est lu
              i.e, correspond au retour de urlretrieve
            - Sinon, il est considéré comme étant le résultés de urlopen

        Returns
        -------
        data : dict
            Contenu du flux json de Vigicrues

        Examples
        --------
        >>> import pyspc.metadata.vigicrues as _vigicrues
        >>> f = 'data/metadata/vigicrues/K118001010_station.json'
        >>> vigi = _vigicrues.Vigicrues_Location(filename=f)
        >>> content = vigi.read()
        >>> content
        {
            'VersionFlux': 'Beta 0.3k',
            'CdStationHydro': 'K118001010',
            'LbStationHydro': 'Digoin [Pont canal]',
            'LbCoursEau': 'Loire',
            'CdStationHydroAncienRef': 'K1180010',
            'CoordStationHydro': {'CoordXStationHydro': '775417', 'CoordYStationHydro': '6598045'},
            'CdCommune': '71176',
            'Evenement': [],
            'VigilanceCrues': {
                'StationPrevision': 't', 'Photo': 't', 'ZoomInitial': '3',
                'PereBoitEntVigiCru': {
                    'CdEntVigiCru': '10',
                    'Link': 'https://www.vigicrues.gouv.fr/services/bulletin.json?CdEntVigiCru=10'},
                'CruesHistoriques': [
                    {'LbUsuel': 'Crue du 03/11/2008', 'ValHauteur': '4.95', 'ValDebit': '1850.00'},
                    {'LbUsuel': 'Crue du 04/12/2003', 'ValHauteur': '4.88', 'ValDebit': '1800.00'},
                    {'LbUsuel': 'Crue du 20/05/2013', 'ValHauteur': '3.14', 'ValDebit': '675.00'}],
                'StationsBassin': [
                    {'CdStationHydro': 'K117321001', 'LbStationHydro': "Montceaux-l'Étoile", 'LbCoursEau': 'Arconce', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K117321001'},
                    {'CdStationHydro': 'K113000201', 'LbStationHydro': 'Mornay [Villorbaine]', 'LbCoursEau': 'Arconce', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K113000201'},
                    {'CdStationHydro': 'K091003010', 'LbStationHydro': 'Commelle-Vernay [Barrage de Villerest]', 'LbCoursEau': 'Loire', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K091003010'},
                    {'CdStationHydro': 'K091001010', 'LbStationHydro': 'Villerest [Aval]', 'LbCoursEau': 'Loire', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K091001010'},
                    {'CdStationHydro': 'K091001011', 'LbStationHydro': 'Villerest [Pont de Villerest]', 'LbCoursEau': 'Loire', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K091001011'},
                    {'CdStationHydro': 'K094301001', 'LbStationHydro': 'Amplepuis', 'LbCoursEau': 'Rhins', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K094301001'},
                    {'CdStationHydro': 'K098301101', 'LbStationHydro': 'St-Vincent-de-Boisset [Pont Maréchal]', 'LbCoursEau': 'Rhins', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K098301101'},
                    {'CdStationHydro': 'K106301002', 'LbStationHydro': 'Pouilly-sous-Charlieu', 'LbCoursEau': 'Sornin', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K106301002'},
                    {'CdStationHydro': 'K108402001', 'LbStationHydro': 'Bénisson-Dieu', 'LbCoursEau': 'Teyssonne', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K108402001'}],
                'FluxDonnees': {
                    'Observations': {
                        'Hauteurs': 'https://www.vigicrues.gouv.fr/services/observations.json?CdStationHydro=K118001010&FormatDate=iso',
                        'Debits': 'https://www.vigicrues.gouv.fr/services/observations.json?CdStationHydro=K118001010&FormatDate=iso&GrdSerie=Q'},
                    'Previsions': {
                        'Hauteurs': 'https://www.vigicrues.gouv.fr/services/previsions.json?CdStationHydro=K118001010&FormatDate=iso',
                        'Debits': 'https://www.vigicrues.gouv.fr/services/previsions.json?CdStationHydro=K118001010&FormatDate=iso&GrdSimul=Q'}
                }
            }
        }
        """
        if self.filename is not None:
            with open(self.filename, 'r', encoding='utf-8') as j:
                data = json.load(j)
        else:
            data = json.loads(content)
        return data
