#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Méta-données (lieux, tronçons, statistiques) - Vigicrues - Service v1.1
"""
import json


class Service():
    """
    Structure liée au flux (geo)json du service 1.1
    """
    def __init__(self, filename=None):
        """
        Initialisation de l'instance de la classe Service

        Parameters
        ----------
        filename : str
            Fichier local du flux (geo)json de Vigicrues 1.1

        """
        self.filename = filename

    def __str__(self):
        """
        Afficher les méta-données de l'instance <Service>
        """
        text = """
        *************************************
        ******** VIGICRUES - Service 1.1 ****
        *************************************
        *  FICHIER          = {filename}
        *************************************
        """
        return text.format(**vars(self))

    def read(self, content=None):
        """
        Lire le résultat des flux (geo)json de Vigicrues (service 1.1)
        - si Service.filename est défini, alors le contenu est lu
          directement depuis ce fichier
        - sinon le contenu est celui défini par le paramètre content

        Parameters
        ----------
        content : str
            - Si cela correspond à un fichier, celui-ci est lu
              i.e, correspond au retour de urlretrieve
            - Sinon, il est considéré comme étant le résultat de urlopen

        Returns
        -------
        data : dict
            Contenu du flux (geo)json de Vigicrues (service 1.1)

        Examples
        --------
        >>> import pyspc.metadata.vigicrues as _vigicrues
        >>> f = 'data/metadata/vigicrues/vigicrues-1_info_30-5_202402140927.json'
        >>> vigi = _vigicrues.Service(filename=f)
        >>> content = vigi.read()
        >>> content
        {'@context': {'mat': 'http://id.eaufrance.fr/ddd/mat/3.1/',
                      'int': 'http://id.eaufrance.fr/ddd/int/2/',
                      'vic': 'http://id.eaufrance.fr/ddd/vic/1.1/',
                      'com': 'http://id.eaufrance.fr/ddd/com/4/',
                      'hyd': 'http://id.eaufrance.fr/ddd/hyd/2.3/',
                      'eth': 'http://id.eaufrance.fr/ddd/eth/2/',
                      'zon': 'http://id.eaufrance.fr/ddd/zon/2.2/',
                      'mdo': 'http://id.eaufrance.fr/ddd/mdo/1.4/'},
         'mat:Scenario': {'Flux': {'Version': 'béta',
                                   'DateRevision': '2019-09-16T11:00:00'},
                          'mat:CodeScenario': 'VICGeo',
                          'mat:VersionScenario': '1beta',
                          'mat:NomScenario': 'Vigilance crues aux formats géo',
                          'DateHeureCreationFichier': '2024-02-14T09:27:12+00:00',
                          'Emetteur': {'@id': 'http://id.eaufrance.fr/int/1537',
                                       'int:CdIntervenant': '1537'}},
         'vic:InfoVigiCru': {'vic:RefInfoVigiCru': '14022024_10',
                             'vic:TypInfoVigiCru': '1',
                             'vic:DtHrInfoVigiCru': '2024-02-14T08:17:00+0100',
                             'vic:DtHrSuivInfoVigiCru': '2024-02-14T16:00:00+0100',
                             'vic:NivInfoVigiCru': '1',
                             'vic:EstNivCalInfoVigiCru': True,
                             'vic:StInfoVigiCru': '4',
                             'vic:SituActuInfoVigiCru': 'Pas de vigilance particulière requise.',
                             'vic:ConsInfoVigiCru': '',
                             'vic:ConseqInfoVigiCru': '',
                             'vic:porteSurLEntite': {'@id': 'http://id.eaufrance.fr/TerEntVigiCru/30',
                                                     'vic:CdEntVigiCru': '30', 'vic:TypEntVigiCru': '5'},
                             'vic:aEteProduitePar': {'@id': 'http://id.eaufrance.fr/int/ARenseigner',
                                                     'vic:CdIntervenant': 'ARenseigner',
                                                     'vic:LbIntervenant': 'Service de Prévision des Crues Loire-Allier-Cher-Indre'}}}

        """
        if self.filename is not None:
            with open(self.filename, 'r', encoding='utf-8') as j:
                data = json.load(j)
        else:
            data = json.loads(content)
        return data
