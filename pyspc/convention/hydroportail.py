#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - Hydroportail
"""
DATATYPES = {
    'Q3J-N': 'vcn3/statistique/resultat',
    'QM-N': 'qmna/statistique/resultat',
    'Q-X': 'crucal/statistique/resultat',
    'QJ-X': 'crucal_j/statistique/resultat',
    'QJ-annuel': 'qj_annual/statistique/resultat',
    'DEBCLA': 'debits-classes/statistique/reference',
}
"""Type des pages Hydroportail récupérables"""

OUAHS_COLS = [
    'Exclue',
    'Début de saison',
    'Fin de saison',
    'Valeur (en m³/s)',
    'Date',
    'Date de la mesure du min/max',
    'Qualification',
    'Nb de points',
    'Données continues & bonnes',
    'Données continues & douteuses',
    'Données continues & non qualifiées',
    'Données discontinues',
    'Données de discontinuités faibles',
    'Données de discontinuités neutres',
    'Données de discontinuités fortes',
]
"""Colonnes du ficher d'échantillon pour OUAHS"""
OUAHS_NO_TRAILING_ZEROS = [
    'Valeur (en m³/s)',
    'Données continues & bonnes',
    'Données continues & douteuses',
    'Données continues & non qualifiées',
    'Données discontinues',
    'Données de discontinuités faibles',
    'Données de discontinuités neutres',
    'Données de discontinuités fortes',
]
"""Colonnes OUAHS où l'on doit retirer les .0 superflus"""
OUAHS_COLS_FROM_SEASONS = {
    'Date': 'observedAt',
    'Date de la mesure du min/max': 'measuredAt',
    'Qualification': 'qualification',
    'Nb de points': 'numObs',
    'Données continues & bonnes': {'qualification20': 1,
                                   'continuity0': 0.01},
    'Données continues & douteuses': {'qualification12': 1,
                                      'continuity0': 0.01},
    'Données continues & non qualifiées': {'qualification16': 1,
                                           'continuity0': 0.01},
    'Données discontinues': 'continuity1',
    'Données de discontinuités faibles': 'continuity4',
    'Données de discontinuités neutres': 'continuity6',
    'Données de discontinuités fortes': 'continuity8',
}
"""Associations des colonnes de l'export seasons d'Hydroportail et de OUAHS"""
