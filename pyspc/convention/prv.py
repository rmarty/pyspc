#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - prv (Otamin, Scores)
"""

DATATYPES = [
    'otamin16_fcst',
    'otamin16_trend',
    'otamin18_fcst',
    'otamin18_trend',
    'scores_obs',
    'scores_sim',
    'scores_fcst'
]
"""Type de fichiers prv (Otamin, Scores)"""

DATANAMES = {
    'otamin16_fcst': 'Fichier prv OTAMINv2016 contenant des prévisions',
    'otamin16_trend': 'Fichier prv OTAMINv2016 contenant des tendances',
    'otamin18_fcst': 'Fichier prv OTAMINv2018 contenant des prévisions',
    'otamin18_trend': 'Fichier prv OTAMINv2018 contenant des tendances',
    'scores_obs': 'Fichier prv Scores contenant des observations',
    'scores_sim': 'Fichier prv Scores contenant des simulations',
    'scores_fcst': 'Fichier prv Scores contenant des prévisions'
}
"""Description de fichiers prv (Otamin, Scores)"""
