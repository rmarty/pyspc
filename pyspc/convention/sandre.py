#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - PHyC/Sandre
"""
VARNAMES = ['Q', 'H', 'RR', 'TA']
"""Grandeurs autorisées par la PHyC"""
ELABVARNAMES = {
    "GRAD": 'Gradient de hauteur',
    "dQmM": 'Delta du débit moyen mensuel '
            'pour la reconstitution du débit naturel',
    "HINJ": 'Hauteur instantanée minimale journalière',
    "HINM": 'Hauteur instantanée minimale mensuelle',
    "HIXJ": 'Hauteur instantanée maximal journalière',
    "HIXM": 'Hauteur instantanée maximale mensuelle',
    "QINJ": 'Débit instantané minimal journalier',
    "QINM": 'Débit instantané minimal mensuel',
    "QIXJ": 'Débit instantané maximal journalier',
    "QIXM": 'Débit instantané maximal mensuel',
    "QmJ": 'Débit moyen journalier',
    "QmM": 'Débit moyen mensuel',
}
"""Grandeurs élaborées autorisées par la PHyC"""

# PATCH pour respecter la nomenclature [513] de libhydro != requête PHyC v1
# libhydro.conv.xml._from_xml > _serieobselab_from_element
# Conversion Sandre V1.1 to V2 (QmJ ->QmnJ)
for e in sorted(ELABVARNAMES.keys()):
    if e in ['QmJ', 'QIXJ', 'QINJ', 'HIXJ', 'HINJ']:
        e2 = '{}n{}'.format(e[0:-1], e[-1])
        ELABVARNAMES[e2] = ELABVARNAMES[e]


ASSOC = {'RR': 'P', 'TA': 'T', 'Q': 'Q', 'H': 'H'}
"""Association entre la grandeur Sandre et le préfixe de variables de pyspc"""

DATATYPES = [
    'data_fcst_hydro',
    'data_obs_hydro',
    'data_obs_meteo',
    'flowmes',
    'levelcor',
    'ratingcurve',
    'loc_hydro',
    'loc_hydro_child',
    'loc_meteo',
    'user'
]
"""Type des exports PHyC"""

DATANAMES = {
    'data_fcst_hydro': 'Prévision hydrologique',
    'data_obs_hydro': 'Observation hydrologique',
    'data_obs_meteo': 'Observation météorologique',
    'flowmes': 'Jaugeage',
    'levelcor': 'Courbe de correction (CorTh)',
    'ratingcurve': 'Courbe de tarage',
    'loc_hydro': 'Lieu de mesure hydrométrique',
    'loc_hydro_child': "Lieu 'fils' de mesure hydrométrique",
    'loc_meteo': 'Lieu de mesure météorologique',
    'user': 'Utilisateur, contact'
}
"""Description des exports PHyC"""

LOC_HYDRO_TYPES = {4: 'ZoneHydro', 8: 'SiteHydro', 10: 'StationHydro',
                   12: 'CapteurHydro'}
"""Type de lieu hydro selon la longueur de l'identifiant"""

RATIO_UNITS = {
    'H': 0.001,  # Convertir les données : mm => m
    'Q': 0.001,  # Conserver les données : l/s => m3/s
    'RR': 0.10,  # Convertir les données : 1/10mm => mm
    'TA': 1.00,  # Convertir les données : 1/10 oC en oC
}
"""Ratio de conversion d'unités"""

TRENDS = {'min': '10', 'moy': '50', 'max': '90'}
"""Probabilités associées aux tendances"""

OBS_COLS = ['Location', 'Varname']
"""Liste des méta-données des prévisions"""

FCST_COLS = ['Location', 'Varname', 'Runtime', 'Model', 'Scenario', 'Prob']
"""Liste des méta-données des prévisions"""
