#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - Bareme
"""

DATATYPES = [
    'flowmes',
    'levelcor',
    'ratingcurve',
]
"""Type de données Bareme"""

DATANAMES = {
    'flowmes': 'Jaugeage',
    'levelcor': 'Courbe de correction (CorTh)',
    'ratingcurve': 'Courbe de tarage',
}
"""Description de données Bareme"""

RATIOS = {
    'jcote': 0.001,
    'valeur': 0.001,
    'jdebit': 1
}
"""Ratios de conversion des unités"""
ATTS = {
    'flowmes': ['jdatedeb', 'jcote', 'jdebit'],
    'levelcor': ['ladate', 'valeur'],
    'ratingcurve': ['cdatedeb', 'cdatefin', 'cdatemaj', 'noct', 'ctype',
                    'cnum', 'cmin', 'cmax'],
    'rtc_point': ['nopt', 'h', 'q'],
    'rtc_power': ['nopt', 'a', 'b', 'h0', 'hsup'],
    ('station', False): 'codehydro',
    ('station', True): 'codehydro3',
}
"""Attributs de base Bareme"""
SQL = {
   'flowmes': """
   SELECT {1}
   FROM {0} INNER JOIN {2} ON {2}.nosta={0}.nosta
   WHERE ({0}.{3}='{4}')
   """,
   'levelcor': """
   SELECT {1}
   FROM {0} INNER JOIN {2} ON {2}.nosta={0}.nosta
   WHERE ({0}.{3}='{4}')
   """,
   'ratingcurve': """
   SELECT {1}
   FROM {0} INNER JOIN {2} ON {2}.nosta={0}.nosta
   WHERE ({0}.{3}='{4}')
   """,
   'rtc_point': """
   SELECT {1}
   FROM {0}
   WHERE ({0}.noct={2});
   """,
   'rtc_power': """
   SELECT {1}
   FROM {0}
   WHERE ({0}.noct={2});
   """,
}
TABLES = {
    'flowmes': 'jaugeage',
    'levelcor': 'courbecorrection',
    'ratingcurve': 'entetecourbe',
    'rtc_point': 'pointcourbe',
    'rtc_power': 'troncon',
    'station': 'station',
}
"""Tables de base Bareme"""
