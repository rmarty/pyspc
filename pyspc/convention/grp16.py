#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - GRP version 2016
"""

# -----------------------------------------------------------------------------
# --- COMMUN
# -----------------------------------------------------------------------------
DATE_FORMAT = '%Y%m%d%H'
"""Format des dates dans les fichiers de GRP 2016"""

DATATYPES = [
    "grp16_cal_data", "grp16_cal_event", "grp16_cal_fcst",
    "grp16_rt_data",
    "grp16_rt_obs", "grp16_rt_obs_diff",
    "grp16_rt_sim", "grp16_rt_sim_diff",
    "grp16_rt_fcst", "grp16_rt_fcst_diff",
    "grp16_rt_archive",
    "grp16_rt_intern", "grp16_rt_intern_diff",
]
"""Types de données de GRP version 2016"""

DATANAMES = {
    "grp16_cal_data": "Fichier de données observées (BDD_var / *)",
    "grp16_cal_event": "Fichier événement",
    "grp16_cal_fcst": "Fichier de prévision (Resultats / *_PP_P0P0.TXT)",
    "grp16_rt_data": "Fichier de données observées (Entrées / *.txt)",
    "grp16_rt_obs": "Fichier de sortie, observation temps-réel",
    "grp16_rt_obs_diff":  "Fichier de sortie, observation temps-différé",
    "grp16_rt_sim":
        "Fichier de sortie, prévision sans assimilation temps-réel",
    "grp16_rt_sim_diff":
        "Fichier de sortie, prévision sans assimilation temps-différé",
    "grp16_rt_fcst":
        "Fichier de sortie, prévision avec assimilation temps-réel",
    "grp16_rt_fcst_diff":
        "Fichier de sortie, prévision avec assimilation temps-différé",
    "grp16_rt_archive": "Fichier de données archivées (BD_var / SITE / *.DAT)",
    "grp16_rt_intern": "Fichier de données internes temps-réel (PQE*.DAT)",
    "grp16_rt_intern_diff":
        "Fichier de données internes temps-différé (PQE*.DAT)",
}
"""Description des données de GRP version 2016"""

# -----------------------------------------------------------------------------
# --- CAL - BASSIN
# -----------------------------------------------------------------------------
CAL_BASIN_LINESEP = "!---------------------------------------------"\
                    "-----------------------------------------------"\
                    "------------------!\n"
CAL_BASIN_HEADERS = {
    'E': "! AAAAAAAA NNNNNN.NN ! Format ecriture code et "
         "ponderation station ETP (1 ligne par station, "
         "commencant par E) !\n",
    'P': "! AAAAAAAA NNNNNN.NN ! Format ecriture code et "
         "ponderation pluvio (1 ligne par poste, "
         "commencant par P)        !\n",
    'T': "! AAAAAAAA NNNNNN.NN NNNN.NN ! Station T "
         "(1 ligne par station, commencant par T)  "
         "                             !\n",
}

# -----------------------------------------------------------------------------
# --- CAL - CONFIG
# -----------------------------------------------------------------------------
CAL_CONFIG_HEADER = """######################################################  README UTILISATEURS  ######################################################
# N = Numero du bassin ; Code = Identifiant du bassin ; Nom Station = Libelle bassin                                              #
# S = Sans module neige  --   A =  Avec Cemaneige                                                                                 #
# T = Correction de Tangara  --   R = Correction des sorties par reseau de neurones                                               #
# Superficie = Superficie du bassin en km2 ; RT = Referentiel Temporel (TU, HL ou HH)                                             #
# HOR = Horizon de calage ; Seuil_C = Seuil de calage en m3/s                                                                     #
# Date debut, Date fin = Dates de debut et de fin sur lesquelles on veut travailler                                               #
# Seuil_V = Seuil utilise pour l'analyse des resultat, en m3/s                                                                    #
# NJ = Nombre de jours presents sur la fenetre temporelle de chaque evenement pour le trace des hydrogrammes prevus               #
#      Doit etre divisible par 2.                                                                                                 #
# HC = Horizon des cheveux que l'on veut voir apparaitre sur le trace des hydrogrammes prevus.                                    #
#      Doit etre divisible par 3. Indiquer '00' pour ne pas les tracer.                                                           #
# EC = Ecart en heure entre les cheveux des previsions (de 01 a 72 heures)                                                        #
# Ecart = Ecart (en m3/s) entre prevision et observation au dela duquel on veut connaitre les pas de temps concernes              #
# Inc = 0 ou 1, pour activer ou desactiver la generation d'abaques d'incertitudes                                                 #
# TGR = 0 ou 1, pour tracer les eventuels resultats de TGR dans les fiches performances et dans le trace des hydrogrammes prevus  #
# N : Chiffre entre 0 et 9   ;   S : caractere alphanumerique                                                                     #
###################################################################################################################################
!----------------------              CALAGE DU MODELE           -----------------------------------!##!   ANALYSE DES RESULTATS   !
!--!--------!------------------------------!-!-!-!-!---------!--!---!--------!----------!----------!##!--------!--!--!--!-----!I!T!
!--!--------!------------------------------!S!S!A!A!---------!--!---!--------!----------!----------!##!--------!--!--!--!-----!n!G!
!N°!    Code!Nom Station                   !T!R!T!R!  Superf.!RT!HOR! Seuil_C!Date debut!  Date fin!##! Seuil_V!NJ!HC!EC!Ecart!c!R!
!--!--------!------------------------------!-!-!-!-!---------!--!---!--------!----------!----------!##!--------!--!--!--!-----!-!-!
!NN!SSSSSSSS!SSSSSSSSSSSSSSSSSSSSSSSSSSSSSS!N!N!N!N!NNNNNN.NN!SS!NNN!NNNNN.NN!NN/NN/NNNN!NN/NN/NNNN!##!NNNNN.NN!NN!NN!NN!NNNNN!N!N!
!--!--------!------------------------------!-!-!-!-!---------!--!---!--------!----------!----------!##!--------!--!--!--!-----!-!-!
"""
CAL_CONFIG_FORMAT = '!{NB:2d}!{CODE:8s}!{NOM:<30s}!{ST:1d}!{SR:1d}!{AT:1d}'\
        '!{AR:1d}!{SURFACE:9.2f}!{RT:2s}!{HOR:3d}!{SC:8.2f}!{DEB:10s}'\
        '!{FIN:10s}!##!{SV:8.2f}!{NJ:2d}!{HC:2d}!{EC:2d}!{ECART:5d}!{INC:1d}'\
        '!{TGR:1d}!\n'
CAL_CONFIG_NAMES = ['NB', 'CODE', 'NOM',
                    'ST', 'SR', 'AT', 'AR',
                    'SURFACE', 'RT', 'HOR', 'SC', 'DEB', 'FIN',
                    'SV', 'NJ', 'HC', 'EC', 'ECART', 'INC', 'TGR']
CAL_CONFIG_CONVERTERS = [int, str, str,
                         int, int, int, int,
                         float, str, int, float, str, str,
                         float, int, int, int, int, int, int]
CAL_CONFIG_CALKEYS = ['NB', 'CODE', 'NOM',
                      'ST', 'SR', 'AT', 'AR',
                      'SURFACE', 'RT', 'HOR', 'SC', 'DEB', 'FIN']

# -----------------------------------------------------------------------------
# --- CAL - DATA
# -----------------------------------------------------------------------------
CAL_DATA_HEADERS = {
    'index': 'AAAAMMJJHH',
    'Q': 'Q(m3/s)',
    'P': 'P(mm)',
    'T': 'T(°C)',
    'EH': 'ETP(mm)',
}
"""Dictionnaire des entêtes de fichier"""
CAL_DATA_VARNAMES = ['EH', 'P', 'Q', 'T']
"""Liste des variables autorisées"""

# -----------------------------------------------------------------------------
# --- CAL - EVENT
# -----------------------------------------------------------------------------
CAL_EVENT_DATE_FORMAT = "%Y%m%d %H"
"""Format de la colonne Date des fichiers Evenements"""
CAL_EVENT_HEADERS = {
    'index': 'Date',
    'Q': 'Debit(m3/s)',
    'P': 'Pluie(mm/h)',
}
"""Dictionnaire des entêtes de fichier"""

# -----------------------------------------------------------------------------
# --- CAL - VERIF
# -----------------------------------------------------------------------------
CAL_VERIF_DTYPES = ['cal', 'rtime']
CAL_VERIF_NAMES = {'cal': 'Fiche en calage-contrôle',
                   'rtime': 'Fiche en calage complet'}

# -----------------------------------------------------------------------------
# --- TR - ARCHIVE
# -----------------------------------------------------------------------------
RT_ARCHIVE_BASENAMES = {
    'PV': 'PV_10A.DAT',
    'QV': 'QV_10A.DAT',
    'TV': 'TV_10A.DAT',
}
"""Noms de base des fichiers de type Archive de GRP Temps-Réel"""
RT_ARCHIVE_DATEFORMAT = '%Y%m%d%H00'
RT_ARCHIVE_FLOATFORMAT = {
    'PV': '%12.4f',
    'QV': '%12.2f',
    'TV': '%12.4f',
}
"""Dictionnaire des formats des réels"""
RT_ARCHIVE_HEADERS = {
    None: '#----------------------------------',
    'loc': 'Code    ',
    'index': 'Date (TU)   ',
    ('QV', 'desc'): 'AAAAAAAA;NNNNMMJJHHNN;FFFFFFFFF.FF;',
    ('PV', 'desc'): 'AAAAAAAA;NNNNMMJJHH00;FFFFFFF.FFFF;',
    ('TV', 'desc'): 'AAAAAAAA;NNNNMMJJHH00;FFFFFFF.FFFF;',

    ('QV', 'short'): ' Debit (l/s)',
    ('PV', 'short'): ' Pluie(mm/h)',
    ('TV', 'short'): '    Temp(°C)',

    ('QV', 'long'): 'Fichier de donnees de debit pas de temps variable'
                    ' (debit instantane)',
    ('PV', 'long'): 'Fichier de donnees de pluie horaire',
    ('TV', 'long'): 'Fichier de donnees de Temperature horaire',
}
"""Dictionnaire des entêtes de fichier"""
RT_ARCHIVE_VARNAMES = ['PV', 'QV', 'TV']
"""Variables associées aux fichiers de type Archive de GRP Temps-Réel"""

# -----------------------------------------------------------------------------
# --- TR - BASSIN
# -----------------------------------------------------------------------------
RT_BASIN_LINESEP = "#--------------------------------------------------------"\
    "-------------\n"
RT_BASIN_HEADERS = {
    'A': "#          FFFFFF.FF ! Format ecriture superficie du bassin  "
         "(1 ligne commencant par S)\n",
    'B': "# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA ! Format ecriture nom station "
         "(1 ligne commencant par B)\n",
    'E': "# AAAAAAAA FFFFFF.FF ! Format ecriture code et ponderation station "
         "ETP (1 ligne par station, commencant par E)\n",
    'F': "# AAAAAAAA FFFFFF.FF  AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA "
         "! Format ecriture code, nom et ponderation "
         "pluvio (1 ligne par poste, commencant par P)\n",
    'G': "# FFFFF.FF           ! Format ecriture Qvig "
         "(1 ligne commencant par G)\n",
#    'P': "",  # A laisser commenté car entête par 'F'
    'Q': "# AAAAAAAA           ! Format ecriture code Hydro "
         "(1 ligne commencant par Q)\n",
    'S': "#          FFFFFF.FF ! Format ecriture superficie du bassin  "
         "(1 ligne commencant par S)\n",
    'T': "#                 II ! Format ecriture decalage temporel des series "
         "de pluie et debit par rapport au UTC (2 lignes commencant par T)\n",
}
RT_BASIN_FORMATS = {
    'A': "A          {0:9.2f} ! Altitude mediane du bassin (m)\n",
    'B': "B {0:<30s} ! Nom station\n",
    'E': "E {0:8s} {1:9.2f} ! Code et ponderation station ETP {2:>4d}\n",
    'F': "F          {0:9.2f}  Pluie annuelle moyenne (mm/an)  \n",
    'G': "G {0:8.2f}           ! Seuil de vigilance (m3/s)\n",
    'P': "P {0:8s} {1:9.2f}  {2:30s}           ! Code, ponderation et "
         "nom poste pluvio {3:>3d}\n",
    'Q': "Q {0:8s}           ! Code Hydro\n",
    'S': "S          {0:9.2f} ! Superficie du bassin (km2)\n",
    'T': "T                 {0:>2d} ! Decalage temporel de la serie de {1} "
         "par rapport au UTC\n",
}

# -----------------------------------------------------------------------------
# --- TR - CONFIG
# -----------------------------------------------------------------------------
RT_CONFIG_KEYS = [
    'MODFON', 'INSTPR', 'OBSCHE', 'OBSTYP',
    'SCECHE', 'SCETYP', 'SCEREF', 'SCENBR',
    'PRVCHE', 'PRVTYP', 'PRVREF', 'PRVUNI',
    'HORMAX', 'CODMOD', 'CONFIR', 'CHOIXH', 'CODMPP', 'CHEMIN',
    'SIMULA',  # ajout 3.1
]
"""Balises du fichier de configuration de GRP Temps-Réel"""
#    'SCENBT'   # ajout 3.1. Retiré dans 16r.. !

# -----------------------------------------------------------------------------
# --- TR - DATA
# -----------------------------------------------------------------------------
RT_DATA_OBSFILEPREFIX = {
    'H': 'Hauteur',
    'P': 'Pluie',
    'Q': 'Debit',
    'T': 'Temp',
}
"""Préfixe des noms de base des fichiers d'entrée de GRP Temps-Réel"""
RT_DATA_LINEPREFIX = {
    'H': 'HAU',
    'P': 'PLU',
    'Q': 'CQT',
    'T': 'TEM',
}
"""Préfixe des lignes de données des fichiers d'entrée de GRP Temps-Réel"""
RT_DATA_SCENFILEPREFIX = {
    'P': 'Scen_',
}
"""Préfixe des noms de fichier des scénarios météo"""
RT_DATA_SCENFORMAT = {
    'P': "Scen_{0}_Plu{1}.txt",
}
"""Nom de base d'un fichier de scénario météo"""
RT_DATA_VARNAMES = ['H', 'P', 'Q', 'T']
"""Liste des variables autorisées"""
RT_DATA_SCENVARNAMES = ['P']
"""Liste des variables autorisées pour les scénarios météo"""

# -----------------------------------------------------------------------------
# --- TR - FCST
# -----------------------------------------------------------------------------
RT_FCST_DTYPES = ["obs", "obs_diff", "sim", "sim_diff", "fcst", "fcst_diff"]
"""Types de fichiers de prévision de GRP Temps-Réel"""
RT_FCST_FILEPREFIX = {
    'obs': 'GRP_Obs',
    'obs_diff': 'GRP_D_Obs',
    'sim': 'GRP_Simu_',
    'sim_diff': 'GRP_D_Simu_',
    'fcst': 'GRP_Prev_',
    'fcst_diff': 'GRP_D_Prev_',
}
"""Préfixe des noms de fichier de prévision de GRP Temps-Réel"""
RT_FCST_LINEPREFIX = {
    'obs': 'OBS',
    'sim': 'SIM',
    'fcst': 'PRV',
}
"""Préfixe des lignes de données des fichiers de prévision de GRP Temps-Réel"""
RT_FCST_VARNAMES = {
    'DEBIT': 'Q',
    'PLUIE': 'P',
    'Temperature': 'T',
}
"""Association entre les colonnes et les grandeurs de GRP Temps-Réel"""

# -----------------------------------------------------------------------------
# --- TR - INTERN
# -----------------------------------------------------------------------------
RT_INTERN_BASENAMES = {
    'intern': 'PQE_1A.DAT',
    'intern_diff': 'PQE_1A_D.DAT',
}
"""Noms de base des fichiers de type Intern de GRP Temps-Réel"""
RT_INTERN_DTYPES = {
    'intern': True,
    'intern_diff': False,
}
"""Types de fichier Intern de GRP Temps-Réel"""

# -----------------------------------------------------------------------------
# --- TR - PARAM
# -----------------------------------------------------------------------------
RT_PARAM_LIST = ['ROUT (mm)', 'CORR (-)', 'TB (h)']
"""Balises du fichier de paramètres de GRP Temps-Réel"""
