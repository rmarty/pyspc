#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - Météo-France
"""
from datetime import datetime as dt, timedelta as td

# =============================================================================
# --- COMMUN
# =============================================================================
DATATYPES = ['data', 'bp', 'sympo']
"""Liste des types de données MF"""

DATANAMES = {
    'data': 'Archive publithèque',
    'bp': 'Archive BP (xml)',
    'sympo': 'Archive sympo/rr3 (txt)'}
"""Description des types de données MF"""

OPEN_DATATYPES = ['MF_OpenData', 'MF_OpenAPI']
"""Liste des types de données MF accessibles par webservices"""

# =============================================================================
# --- METEO.DATA.GOUV.FR
# =============================================================================
MDG_HOSTANME = 'https://object.files.data.gouv.fr/meteofrance/data/'\
    'synchro_ftp/BASE'
"""URL racine des données METEO.DATA.GOUV.FR"""
MDG_TIMESTEP = {
    td(days=1): 'QUOT',  # QUOTIDIEN
    td(hours=1): 'HOR',  # HORAIRE
    td(minutes=6): 'MIN',  # 6-MINUTES
}
"""Pas de temps des données METEO.DATA.GOUV.FR"""
MDG_DSC = {
    ('QUOT', True): 'Q_descriptif_champs_RR-T-Vent.csv',
    ('QUOT', False): 'Q_descriptif_champs_autres-parametres.csv',
    ('HOR', True): 'H_descriptif_champs.csv',
    ('HOR', False): 'H_descriptif_champs.csv',
    ('MIN', True): 'MN_descriptif_champs.csv',
    ('MIN', False): 'MN_descriptif_champs.csv',
}
"""Fichiers de description des paramètres METEO.DATA.GOUV.FR"""
MDG_PREFIX = {
    'Q': td(days=1),  # QUOTIDIEN
    'H': td(hours=1),  # HORAIRE
    'MN': td(minutes=6),  # 6-MINUTES
}
"""Préfixes des fichiers de données METEO.DATA.GOUV.FR"""
MDG_DATE_COLNAME = {
    'Q': 'AAAAMMJJ',  # QUOTIDIEN
    'H': 'AAAAMMJJHH',  # HORAIRE
    'MN': 'AAAAMMJJHHMN',  # 6-MINUTES
}
"""Colonne des dates des fichiers de données METEO.DATA.GOUV.FR"""

# =============================================================================
# --- MF API
# =============================================================================
API_TOKEN_URL = "https://portail-api.meteofrance.fr/token"
"""URL de récupération d'un jeton Oauth2"""
API_HOSTNAME = "https://public-api.meteofrance.fr/public"
"""URL racine des données de l'API publique de MF"""
API_DATATYPES = ['loc_meteo', 'loc_meteo_meta', 'data_obs_meteo']
"""Types de données récupérables par API."""
API_WEBSERVICES = {
    ('data', td(minutes=6)):
        '/DPClim/v1/commande-station/infrahoraire-6m'
        '?id-station={loc}&date-deb-periode={start}&date-fin-periode={end}',
    ('data', td(hours=1)):
        '/DPClim/v1/commande-station/horaire'
        '?id-station={loc}&date-deb-periode={start}&date-fin-periode={end}',
    ('data', td(days=1)):
        '/DPClim/v1/commande-station/quotidienne'
        '?id-station={loc}&date-deb-periode={start}&date-fin-periode={end}',
    ('data', 'file'): "/DPClim/v1/commande/fichier?id-cmde={fileid}",
    ('loc', td(minutes=6)):
        '/DPClim/v1/liste-stations/infrahoraire-6m?id-departement={loc}',
    ('loc', td(hours=1)):
        '/DPClim/v1/liste-stations/horaire?id-departement={loc}',
    ('loc', td(days=1)):
        '/DPClim/v1/liste-stations/quotidienne?id-departement={loc}',
    ('loc', 'meta'): "/DPClim/v1/information-station?id-station={loc}",
}
"""Chemins des web-services par type de requête. Voir API_HOSTNAME."""
API_PREFIX = {
    td(days=1): 'Q',  # QUOTIDIEN
    td(hours=1): 'H',  # HORAIRE
    td(minutes=6): 'MN',  # 6-MINUTES
}
"""Préfixes des fichiers de données METEO.DATA.GOUV.FR"""


# =============================================================================
# --- PUBLITHEQUE
# =============================================================================
VARNAMES = [
    'RR6',  # PRECIPITATIONS 6-MINUTES (P6M)
    'RR1',  # PRECIPITATIONS HORAIRES (PH)
    'RR3',  # PRECIPITATIONS TRI-HORAIRES (P3H)
    'RR',  # PRECIPITATIONS JOURNALIERES (PJ)
    'RR24',  # PRECIPITATIONS JOURNALIERES (PJ)
    'T',  # TEMPERATURE SURFACE HORAIRE (TH)
    'TNTXM',  # TEMPERATURE SURFACE JOURNALIERE (TJ)
    'ETPMON',  # EVAPO-TRANSIPIRATION (EJ)
]
"""Liste des grandeurs de la Publithèque"""
DATE_FORMATS = {
    'RR6': '%Y%m%d%H%M',
    'RR1': '%Y%m%d%H',
    'RR3': '%Y%m%d%H',
    'RR': '%Y%m%d',
    'RR24': '%Y%m%d',
    'T': '%Y%m%d%H',
    'TNTXM': '%Y%m%d',
    'ETPMON': '%Y%m%d',
}
"""Format de la date dans les fichiers Météo-France selon la grandeur"""

# =============================================================================
# --- BP
# =============================================================================
BP_NAMESPACE_AP = \
    "{urn:meteofrance:specification:schema-xsd:production:soprano-AP}"
"""Espace de nommage du XML BP - bloc AP"""


BP_NAMESPACE = \
    "{urn:meteofrance:specification:schema-xsd:production:soprano-donnees}"
"""Espace de nommage du XML BP"""

BP_PARAMS = {
    'MAXRR': 'Maximum de précipitations',
    'AVGRR': 'Moyenne de précipitations',
}
"""Grandeurs contenues dans les BP, et leur libellé"""

BP_ZONES = {
    "43001": {'name': 'Borne Ance-du-Nord',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "43002": {'name': 'Source Loire',
              'AP_PJ': 60,
              'provider': 'DIRCE'},
    "43003": {'name': 'Lignon-Velay Dunières',
              'AP_PJ': 60,
              'provider': 'DIRCE'},
    "43004": {'name': 'Furan Semène Coise',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "43005": {'name': 'Lignon-Forez Mare',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "43006": {'name': 'Rhins Sornin',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "43007": {'name': 'Besbre amont',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "43008": {'name': 'Allier Aval',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "43009": {'name': 'Sioule aval',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "43010": {'name': 'Sioule amont',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "43011": {'name': 'Allier intermediaire',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "43012": {'name': 'Alagnon',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "43013": {'name': 'Allier Brivadois',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "43014": {'name': 'Dore',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "43015": {'name': 'Haut Allier',
              'AP_PJ': 30,
              'provider': 'DIRCE'},

    "23001": {'name': 'Arconce-Bourbince',
              'AP_PJ': 30,
              'provider': 'DIRNE'},
    "23002": {'name': 'Arroux amont',
              'AP_PJ': 30,
              'provider': 'DIRNE'},
    "23003": {'name': 'Aron-Nièvre',
              'AP_PJ': 30,
              'provider': 'DIRNE'},
    "23004": {'name': 'Vallée Loire bourbonnaise',
              'AP_PJ': 30,
              'provider': 'DIRNE'},

    "33001": {'name': 'Tardes Cher-amont',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "33002": {'name': 'Aumance Marmande',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "33003": {'name': 'Indre-amont Arnon-amont',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "33004": {'name': 'Arnon-aval Theols',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "33005": {'name': 'Indrois Fouzon',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "33006": {'name': 'Cher-aval Indre-aval',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "33007": {'name': 'Loire Tourangelle',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "33008": {'name': 'Cosson Beuvron',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "33009": {'name': 'Loire Giennoise',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "33010": {'name': 'Yèvre',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "33011": {'name': 'Sauldre',
              'AP_PJ': 30,
              'provider': 'DIRO'},

    "41003": {'name': 'Borne Ance du Nord',  # 'Haut Bassin Loire'
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "41004": {'name': 'Lignon Velay Dunières',
              'AP_PJ': 60,
              'provider': 'DIRCE'},
    "41005": {'name': 'Source Loire',
              'AP_PJ': 60,
              'provider': 'DIRCE'},
    "41006": {'name': 'Furan Semène Coise',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "41007": {'name': 'Lignon Forez Mare',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "41008": {'name': 'Furan Semène Coise',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "41009": {'name': 'Besbre amont',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "41011": {'name': 'Besbre',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "41012": {'name': 'Loire Bourbonnaise',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "41014": {'name': 'Rhins Sornin',
              'AP_PJ': 30,
              'provider': 'DIRCE'},

    "41101": {'name': 'Allier aval',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "41103": {'name': 'Dore',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "41105": {'name': 'Allier intermediaire',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "41106": {'name': 'Alagnon',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "41107": {'name': 'Allier Brivadois',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "41108": {'name': 'Haut Allier',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "41109": {'name': 'Sioule amont',
              'AP_PJ': 30,
              'provider': 'DIRCE'},
    "41110": {'name': 'Sioule aval',
              'AP_PJ': 30,
              'provider': 'DIRCE'},

    "21010": {'name': 'Sornin Arconce',
              'AP_PJ': 30,
              'provider': 'DIRNE'},
    "21013": {'name': 'Arroux Bourbince',
              'AP_PJ': 30,
              'provider': 'DIRNE'},
    "21014": {'name': 'Vallée Loire Nivernaise',
              'AP_PJ': 30,
              'provider': 'DIRNE'},
    "21015": {'name': 'Aron',
              'AP_PJ': 30,
              'provider': 'DIRNE'},
    "21016": {'name': 'Arconce-Bourbince',
              'AP_PJ': 30,
              'provider': 'DIRNE'},
    "21018": {'name': 'Arroux amont',
              'AP_PJ': 30,
              'provider': 'DIRNE'},
    "21019": {'name': 'Aron-Nièvre',
              'AP_PJ': 30,
              'provider': 'DIRNE'},
    "21020": {'name': 'Vallée Loire bourbonnaise',
              'AP_PJ': 30,
              'provider': 'DIRNE'},

    "31003": {'name': 'Tardes Cher amont',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "31004": {'name': 'Indre amont Arnon amont',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "31005": {'name': 'Aumance Marmande',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "31008": {'name': 'Indrois Fouzon',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "31009": {'name': 'Cher aval Indre aval',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "31010": {'name': 'Cosson Beuvron',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "31011": {'name': 'Loire Tourangelle',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "31012": {'name': 'Yevre',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "31013": {'name': 'Arnon aval Theols',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "31014": {'name': 'Loire Giennoise',
              'AP_PJ': 30,
              'provider': 'DIRO'},
    "31015": {'name': 'Sauldre',
              'AP_PJ': 30,
              'provider': 'DIRO'},

    "51001": {'name': 'Cher Amont',
              'AP_PJ': 30,
              'provider': 'DIRSO'},
    "71002": {'name': 'Cher Aval Indre Aval',
              'AP_PJ': 30,
              'provider': 'DIRIC'},
    "71003": {'name': 'Loire Tourangelle',
              'AP_PJ': 30,
              'provider': 'DIRIC'},
    "71004": {'name': 'Loire Giennoise',
              'AP_PJ': 30,
              'provider': 'DIRIC'},
    "71005": {'name': 'Sauldre',
              'AP_PJ': 30,
              'provider': 'DIRIC'},
    "71006": {'name': 'Yèvre Cher Moyen',
              'AP_PJ': 30,
              'provider': 'DIRIC'},
    "71007": {'name': 'Indre Amont Arnon Théols',
              'AP_PJ': 30,
              'provider': 'DIRIC'}
}
"""Méta-données des zones AP"""

BP_HISTORY = {
    (dt(2008, 6, 12, 0), dt(2012, 3, 15, 12)): {
        "provider": {'ic': "DIRIC", 'ly': "DIRCE", 'so': "DIRSO"},
        "zones": {
            "730": ['71002'],
            "510": ['51001'],
            "731": ['71003'],
            "732": ['71004'],
            "733": ['71005'],
            "734": ['71006'],
            "735": ['71007'],
            "401": ['41011', '21013', '21014', '21015'],
            "402": ['41006', '41007', '41012', '21010'],
            "403": ['41005', '41004', '41003'],
            "410": ['41101']
        }
    },
    (dt(2012, 3, 15, 12), dt(2012, 4, 24, 12)): {
        "provider": {'ic': "DIRIC", 'ly': "DIRCE", 'so': "DIRSO"},
        "zones": {
            '71002': ['71002'],
            '71003': ['71003'],
            '71004': ['71004'],
            '71005': ['71005'],
            '71006': ['71006'],
            '71007': ['71007'],
            '41005': ['41005'],
            '41003': ['41003'],
            '41004': ['41004'],
            '41006': ['41006'],
            '41007': ['41007'],
            '41012': ['41012'],
            '41010': ['21010'],
            '41011': ['41011'],
            '41014': ['21014'],
            '41013': ['21013'],
            '41015': ['21015'],
            '41101': ['41101'],
            '51001': ['51001']
        }
    },
    (dt(2012, 4, 24, 12), dt(2014, 7, 8, 12)): {
        "provider": {
            'ic': "DIRIC", 'ly': "DIRCE", 'ne': "DIRNE", 'so': "DIRSO"},
        "zones": {
            '71002': ['71002'],
            '71003': ['71003'],
            '71004': ['71004'],
            '71005': ['71005'],
            '71006': ['71006'],
            '71007': ['71007'],
            '41005': ['41005'],
            '41003': ['41003'],
            '41004': ['41004'],
            '41006': ['41006'],
            '41007': ['41007'],
            '41012': ['41012'],
            '41010': ['21010'],
            '41011': ['41011'],
            '41014': ['21014'],
            '41013': ['21013'],
            '41015': ['21015'],
            '41101': ['41101'],
            '51001': ['51001']
        }
    },
    (dt(2014, 7, 8, 12), dt(2019, 10, 1)): {
        "provider": {
            'ic': "DIRIC", 'ly': "DIRCE", 'ne': "DIRNE", 'so': "DIRSO"},
        "zones": {
            '71002': ['71002'],
            '71003': ['71003'],
            '71004': ['71004'],
            '71005': ['71005'],
            '71006': ['71006'],
            '71007': ['71007'],
            '41005': ['41005'],
            '41003': ['41003'],
            '41004': ['41004'],
            '41006': ['41006'],
            '41007': ['41007'],
            '41012': ['41012'],
            '21010': ['21010'],
            '41011': ['41011'],
            '21014': ['21014'],
            '21013': ['21013'],
            '21015': ['21015'],
            '41101': ['41101'],
            '51001': ['51001']
        }
    },
    (dt(2019, 10, 1), dt(2023, 12, 5, 12)): {
        "provider": {'ly': "DIRCE", 'ne': "DIRNE", 'rn': "DIRO"},
        "zones": {
            '31003': ['31003'],
            '31004': ['31004'],
            '31005': ['31005'],
            '31008': ['31008'],
            '31009': ['31009'],
            '31010': ['31010'],
            '31011': ['31011'],
            '31012': ['31012'],
            '31013': ['31013'],
            '31014': ['31014'],
            '31015': ['31015'],
            '41003': ['41003'],
            '41004': ['41004'],
            '41005': ['41005'],
            '41007': ['41007'],
            '41008': ['41008'],
            '41009': ['41009'],
            '41014': ['41014'],
            '21016': ['21016'],
            '21018': ['21018'],
            '21019': ['21019'],
            '21020': ['21020']
        }
    },
    (dt(2023, 12, 5, 12), dt(2050, 1, 1)): {
        "provider": {'ly': "DIRCE", 'ne': "DIRNE", 'rn': "DIRO"},
        "zones": {
            '23001': ['23001'],
            '23002': ['23002'],
            '23003': ['23003'],
            '23004': ['23004'],
            '33001': ['33001'],
            '33003': ['33003'],
            '33002': ['33002'],
            '33005': ['33005'],
            '33006': ['33006'],
            '33008': ['33008'],
            '33007': ['33007'],
            '33010': ['33010'],
            '33004': ['33004'],
            '33009': ['33009'],
            '33011': ['33011'],
            '43001': ['43001'],
            '43003': ['43003'],
            '43002': ['43002'],
            '43005': ['43005'],
            '43004': ['43004'],
            '43007': ['43007'],
            '43006': ['43006'],
            '43008': ['43008'],
            '43014': ['43014'],
            '43011': ['43011'],
            '43012': ['43012'],
            '43013': ['43013'],
            '43015': ['43015'],
            '43010': ['43010'],
            '43009': ['43009'],
        }
    }
}
"""Historique des zones AP du SPC LACI"""


# =============================================================================
# --- SYMPO
# =============================================================================
SYMPO_DATE_FORMAT = '%Y%m%d%H%M%S'
"""Format des dates des fichiers Sympo"""

SYMPO_VERSIONS = ['rr3', 'sympo']
"""Versions des fichiers Sympo"""

SYMPO_ZONES = {
    '0301': "Val de Cher",
    '0302': "Bocage Bourbonnais",
    '0303': "Sologone Bourbonnaise",
    '0304': "Ouest Combraille",
    '0305': "Est Combraille",
    '0307': "Val d'Allier",
    '0308': "Mtg Bourbonnaise",
    '0309': "Smt Mtg Bourbonnaise",
    '0708': "Cévennes ardéchoises",
    '0709': "Plateau Ardéchois",
    '1801': "Sologne vierzonnaise",
    '1802': "Pays fort et Sancerrois",
    '1803': "Val de Loire",
    '1804': "Champagne ouest",
    '1805': "Champagne est",
    '1806': "Vallée de Germigny",
    '1807': "Boischaut",
    '1808': "Marche berrichonne",
    '2104': "Hautes-Côtes-Ouche",
    '2105': "Auxois Sud",
    '2106': "Morvan Est",
    '2301': "Basse marche Creusoise",
    '2302': "Bas-Berry Bourbonnais",
    '2303': "Combraille",
    '2304': "Marche",
    '2307': "Val-de-Creuse",
    '3601': "Boischaut nord",
    '3602': "Champagne berrichonne",
    '3603': "Brenne",
    '3605': "Boischaut sud",
    '3701': "Gatine ouest",
    '3702': "Gatine est",
    '3703': "Chinon-Val de Loire",
    '3704': "Tours-Val de Loire",
    '3705': "Champeigne Ste Maure",
    '3706': "Gatines Loches Montrésor",
    '3707': "Richelais",
    '4102': "Vallée du Loir",
    '4103': "Beauce",
    '4104': "Vallée de Loire",
    '4105': "Touraine méridionale",
    '4106': "Sologne",
    '4201': "Monts du Forez",
    '4202': "Roannais",
    '4203': "Plaine du Forez",
    '4204': "Monts du Lyonnais",
    '4205': "Zone urbaine St Etienne",
    '4206': "Le Pilat",
    '4303': "Plateaux Fix  Chaise-Dieu",
    '4304': "Le Puy-Vallée Loire",
    '4305': "Plateaux Loudes-Blavozy",
    '4306': "Plateaux du Devès",
    '4307': "Yssingeaux-Vivarais",
    '4501': "Beauce",
    '4503': "Orléanais",
    '4504': "Val de Loire",
    '4505': "Sologne",
    '4506': "Puisaye",
    '5801': "Vallée de la Loire",
    '5802': "Colline du Nivernais",
    '5803': "Haute Vallée de l'Yonne",
    '5804': "Bazois",
    '5805': "Morvan Ouest",
    '6301': "Nord Limagne",
    '6305': "Mont du Forez",
    '6306': "Les Bois Noirs",
    '6307': "Plaine de la Dore",
    '6308': "Combrailles",
    '6902': "Monts du Beaujolais",
    '6903': "Mont du Lyonnais",
    '7101': "Autunois",
    '7105': "Charollais-Brionnais",
    '7106': "Val de Loire",
    '7107': "Morvan",
    '8905': "Puisaye",
}
"""Zones symposium"""
