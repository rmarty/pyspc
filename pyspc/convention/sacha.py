#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - Bases Sacha
"""

# =============================================================================
# --- COMMUN
# =============================================================================
DATATYPES = [
    'sacha',  # Bd Sacha Historique
    'sacha_TR',  # Bd Sacha Temps-Réel
]
"""Origine des données SACHA (historique ou temps-réel)"""

DATANAMES = {
    'sacha': 'Sacha Historique',
    'sacha_TR': 'Bd Sacha Temps-Réel',
}
"""Description des données SACHA (historique ou temps-réel)"""

PRCPTYPES = ['gauge', 'radar']
"""Origine des données pluviométriques (station ou radar)"""

HYDROTYPES = ['hydro2', 'hydro3']
"""Origine du référentiel des stations"""

VARNAMES = ['HH', 'QH', 'PH', 'TH']
"""Grandeurs disponibles"""

# =============================================================================
# --- STATIONS
# =============================================================================
CODES_STATION = {
    ('hydro2', 'HH'): 'codeh',
    ('hydro2', 'PH'): 'codep',
    ('hydro2', 'PH'): 'codep',
    ('hydro2', 'QH'): 'codeq',
    ('hydro2', 'TH'): 'codet',

    ('hydro3', 'HH'): 'valeur',
    ('hydro3', 'PH'): 'valeur',
    ('hydro3', 'PH'): 'valeur',
    ('hydro3', 'QH'): 'valeur',
    ('hydro3', 'TH'): 'valeur',
}
"""Attibut des identifiants des stations"""

TABLE_STATION = {
    'hydro2': 'station',
    'hydro3': 'phyc',
}
"""Table des stations"""

SQL_STATION = {
    'hydro2': """
SELECT {0}.nosta, {0}.{1}
FROM {0}
WHERE {0}.{1} IN ({2})
""",
    'hydro3': """
SELECT {0}.nosta, {0}.{1}, {0}.tr
FROM {0}
WHERE (({2}) AND {0}.tr = 1)
""",
}
"""Requêter une sélection de stations"""

ATTS_ALL_STATIONS = {
    'hydro2': ['nosta', 'nom', 'courdo', 'bv',
               'codeh', 'codeq', 'codep', 'codet', 'code_bareme',
               'altitude', 'xlambert', 'ylambert', 'comment'],
    'hydro3': ['nosta', 'nature', 'valeur', 'tr', 'ordre']
}
"""Attributs des tables des stations"""

SQL_ALL_STATIONS = {
    'hydro2': """
SELECT station.nosta, station.nom, station.courdo, station.bv, station.codeh,
       station.codeq, station.codep, station.codet, station.code_bareme,
       station.altitude, station.xlambert, station.ylambert, station.comment
FROM station
ORDER BY station.nosta
""",
    'hydro3': """
SELECT phyc.nosta, phyc.nature, phyc.valeur, phyc.tr, phyc.ordre
FROM phyc
ORDER BY phyc.nosta
""",
}
"""Requêter toutes les stations"""

ATTS_INSERT_STATIONS = {
    'hydro2': ['nosta'],
    'hydro3': ['nosta', 'nature', 'valeur', 'tr', 'ordre']
}
ATTS_UPDATE_STATIONS = {
    'hydro2': ['nom',  # 'nosta'
               'courdo', 'arteau', 'codeh', 'codeq', 'codep',
               'numpreh', 'numpreq', 'numprep', 'caliph', 'calipq', 'calipp',
               'comment', 'bv', 'h_jaune', 'h_orange', 'h_rouge', 'q_jaune',
               'q_orange', 'q_rouge', 'code_bareme', 'h_autre1', 'h_autre2',
               'q_autre1', 'q_autre2', 'altitude', 'xlambert', 'ylambert',
               'numpret', 'calipt', 'codet', 'h_jaunemin', 'h_orangemin',
               'h_rougemin', 'q_jaunemin', 'q_orangemin', 'q_rougemin',
               'delaipertinenceetiage', 'delaipertinencecrue', 'module'],
    'hydro3': []
}
SQL_INSERT_STATIONS = """
INSERT INTO {0} ({1})
VALUES ({2})
"""
# """Requêter pour ajouter des stations"""
SQL_UPDATE_STATIONS = """
UPDATE {0}
SET {1}
WHERE {0}.{2} = {3}
"""
# """Requêter pour mettre-à-jour des stations"""


# =============================================================================
# --- DONNEES
# =============================================================================
NATURES = {
    ('HH', None): 0,
    ('PH', 'gauge'): 2,
    ('PH', 'radar'): 3,
    ('QH', None): 1,
    ('TH', None): 4,
}
"""Nature des données"""

RATIO_UNITS = {
    'HH': 0.01,  # Convertir les données : cm => m
    'QH': 1.00,  # Conserver en m3/s
    'PH': 0.10,  # Convertir les données : 1/10mm => mm
    'TH': 0.10,  # Convertir les données : 1/10 oC en oC
}
"""Ratio de conversion d'unités"""

TABLE_DATA = {
    True: 'donnees_treel',
    False: 'donnees'
}
"""Table des données"""

SQL_DATA = """
SELECT {0}.ladate, {0}.valeur
FROM {0}
WHERE ({0}.nosta = {1}) AND ({0}.nature = {2})
"""
"""Requêter une sélection de données"""

SQL_DATACOVERAGE = """
SELECT COUNT({0}.valeur), MIN({0}.valeur), MAX({0}.valeur),
       MIN({0}.ladate), MAX({0}.ladate)
FROM {0}
WHERE ({0}.nosta = {1}) AND ({0}.nature = {2})
"""
"""Requêter une sélection de données"""
