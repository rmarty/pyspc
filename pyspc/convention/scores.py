#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - Projet SCORES
"""

# -----------------------------------------------------------------------------
# --- GENERAL
# -----------------------------------------------------------------------------
VERSIONS = [1, 2]
"""Versions de Scores"""
# -----------------------------------------------------------------------------
# --- CONFIGURATION
# -----------------------------------------------------------------------------
SECTIONS_OPTIONS = [
    ('GENERAL', 'FONCTIONNEMENT'),
    ('GENERAL', 'MODE'),
    ('GENERAL', 'ENTITE'),
    ('GENERAL', 'GRANDEUR'),
    ('GENERAL', 'TAUX_PAS_DE_TEMPS'),
    ('CSV', 'CSV_DELIMITEUR'),
    ('OBSERVATION', 'DONNEES_OBSERVATION'),
    ('OBSERVATION', 'LACUNE_EVENEMENT'),
    ('OBSERVATION', 'DETECTION_EVENEMENT'),
    ('OBSERVATION', 'LISTE_EVENEMENT'),
    ('PREVISION', 'DONNEES_PREVISION'),
    ('PREVISION', 'LISTE_MODELE'),
    ('PREVISION', 'LISTE_EXECUTION'),
    ('PREVISION', 'LISTE_ECHEANCE'),
    ('INTERPOLATION', 'METHODE_INTERPOLATION'),
    ('EXPORT', 'EXPORT_PREVISION_ECHEANCE_FIXE'),
    ('EXPORT', 'DOSSIER_EXPORT_PREVISION_ECHEANCE_FIXE'),
    ('CALCUL', 'FICHIER_RESULTAT'),
    ('CALCUL', 'LISTE_GROUPES_SCORES'),
    ('CALCUL', 'ERREURS_ABSOLUES'),
    ('CALCUL', 'DELAI_PREVISION'),
    ('CALCUL', 'RAPPORT_DE_POINTE'),
    ('CALCUL', 'RAPPORT_ECHELLE'),
    ('CALCUL', 'DISTRIBUTION_ERREUR'),
    ('CALCUL', 'METHODE_CALCUL_FRANCHISSEMENT_SEUIL'),
    ('CALCUL', 'LISTE_SEUIL'),
    ('CALCUL', 'SEUIL_MINIMAL'),
    ('CALCUL', 'TAUX_VALEURS_IGNOREES'),
    ('CALCUL', 'FENETRE_TEMPORELLE'),
    ('CALCUL', 'HYSTERESIS'),
    ('CALCUL', 'CALCUL_PAR_CLASSE'),
    ('CALCUL', 'NATURE_CLASSE'),
    ('CALCUL', 'PORTEE_CLASSE'),
    ('CALCUL', 'NOMBRE_CLASSE'),
    ('CALCUL', 'LISTE_CLASSE'),
    ('CALCUL', 'CALCUL_AVEC_REPHASAGE'),
    ('CALCUL', 'LISTE_REPHASAGE'),
    ('CALCUL', 'PHASE_DE_MONTEE'),
    ('CALCUL', 'DELTA_TEMPS_ENTREE_PHASE_MONTANTE'),
    ('CALCUL', 'DELTA_TEMPS_SORTIE_PHASE_MONTANTE')
]
"""Liste des (sections, options) de configuration de Scores-1.3.3"""
SECTIONS_OPTIONS_2 = [
    ('GENERAL', 'FONCTIONNEMENT'),
    ('GENERAL', 'MODE'),
    ('GENERAL', 'ENTITE'),
    ('GENERAL', 'GRANDEUR'),
    ('GENERAL', 'TAUX_PAS_DE_TEMPS'),
    ('CSV', 'CSV_DELIMITEUR'),
    ('OBSERVATION', 'DONNEES_OBSERVATION'),
    ('OBSERVATION', 'LACUNE_EVENEMENT'),
    ('OBSERVATION', 'DETECTION_EVENEMENT'),
    ('OBSERVATION', 'LISTE_EVENEMENT'),
    ('PREVISION', 'DONNEES_PREVISION'),
    ('PREVISION', 'LISTE_MODELE'),
    ('PREVISION', 'LISTE_EXECUTION'),
    ('PREVISION', 'LISTE_ECHEANCE'),
    ('PREVISION', 'QUANTILES_PROBA'),
    ('INTERPOLATION', 'METHODE_INTERPOLATION'),
    ('EXPORT', 'EXPORT_PREVISION_ECHEANCE_FIXE'),
    ('EXPORT', 'DOSSIER_EXPORT_PREVISION_ECHEANCE_FIXE'),
    ('CALCUL', 'FICHIER_RESULTAT'),
    ('CALCUL', 'DOSSIER_RESULTAT'),
    ('CALCUL', 'LISTE_GROUPES_SCORES'),
    ('CALCUL', 'ERREURS_ABSOLUES'),
    ('CALCUL', 'UNITE_TEMPS'),
    ('CALCUL', 'DELAI_PREVISION'),
    ('CALCUL', 'RAPPORT_DE_POINTE'),
    ('CALCUL', 'RAPPORT_ECHELLE'),
    ('CALCUL', 'DISTRIBUTION_ERREUR'),
    ('CALCUL', 'METHODE_CALCUL_FRANCHISSEMENT_SEUIL'),
    ('CALCUL', 'LISTE_SEUIL'),
    ('CALCUL', 'SEUIL_MINIMAL'),
    ('CALCUL', 'SEUIL_BRIER'),
    ('CALCUL', 'TAUX_VALEURS_IGNOREES'),
    ('CALCUL', 'FENETRE_TEMPORELLE'),
    ('CALCUL', 'HYSTERESIS'),
    ('CALCUL', 'CALCUL_PAR_CLASSE'),
    ('CALCUL', 'NATURE_CLASSE'),
    ('CALCUL', 'PORTEE_CLASSE'),
    ('CALCUL', 'NOMBRE_CLASSE'),
    ('CALCUL', 'LISTE_CLASSE'),
    ('CALCUL', 'CALCUL_AVEC_REPHASAGE'),
    ('CALCUL', 'LISTE_REPHASAGE'),
    ('CALCUL', 'PHASE_DE_MONTEE'),
    ('CALCUL', 'DELTA_TEMPS_ENTREE_PHASE_MONTANTE'),
    ('CALCUL', 'DELTA_TEMPS_SORTIE_PHASE_MONTANTE'),
    ('FICHIER', 'PREFIXE'),
    ('FICHIER', 'FORMAT_GRAPH'),
    ('FICHIER', 'RESOLUTION_GRAPH'),
    ('FICHIER', 'RESOLUTION_PERSO'),
    ('FICHIER', 'TAILLE_GRAPH_TYPE'),
    ('FICHIER', 'TAILLE_GRAPH_VAL_L'),
    ('FICHIER', 'TAILLE_GRAPH_VAL_H'),
]
"""Liste des (sections, options) de configuration de Scores-2.0"""

# -----------------------------------------------------------------------------
# --- PRV
# -----------------------------------------------------------------------------
EXPORT_DTYPES = {
    'obs': 'OBSERVATION',
    'sim': 'SIMULATION',
    'fcst': 'PREVISION',
}
"""Type de format prv de Scores"""

EXPORT_UNITS = {
    'Q': 'm3/s',
    'H': 'm',
    'RR': 'mm',
    'TA': 'Celsius'
}
"""Unités des grandeurs pour le format prv de Scores"""

EXPORT_COLS = ['Stations', 'Grandeurs', 'IdSeries']
"""Liste des entêtes du format prv de Scores"""

EXPORT_FCST_COLS = ['# Modeles', '# Scenarios', '# DtDerObs', '# Probas']
"""Liste des entêtes supplémentaires pour les prévisions"""

# -----------------------------------------------------------------------------
# --- XML
# -----------------------------------------------------------------------------
SCORES_DTYPES = ['sim', 'fcst']
"""Modes de fonctionnement de Scores"""

SCORES_ASSOC = {
    'ME': {'erreur_absolue': 'NON', 'nom': 'Erreur moyenne', 'numero': '1'},
    'MAE': {'erreur_absolue': 'OUI', 'nom': 'Erreur moyenne', 'numero': '1'},
    'MRE': {'erreur_absolue': 'NON', 'nom': 'Erreur relative moyenne',
            'numero': '2'},
    'MARE': {'erreur_absolue': 'OUI', 'nom': 'Erreur relative moyenne',
             'numero': '2'},
    'STDERR': {'nom': 'Ecart-type des erreurs', 'numero': '3'},
    'RMSE': {'nom': 'Erreur quadratique moyenne', 'numero': '4'},
    'BIAS': {'nom': 'Biais', 'numero': '5'},
    'PREC': {'nom': 'Précision', 'numero': '6'},
    'ME_CDF': {'erreur_absolue': 'NON', 'nom': 'Distribution des erreurs',
               'numero': '9'},
    'MAE_CDF': {'erreur_absolue': 'OUI', 'nom': 'Distribution des erreurs',
                'numero': '9'},
    'PERS': {'nom': 'PERSISTANCE', 'numero': '7'},
    'C2MP': {'nom': 'C2MP', 'numero': '7'},
    'NASH': {'nom': 'Critère de Nash', 'numero': '8'},
    'VOL': {'nom': 'Erreur sur les volumes de crues', 'numero': '10'},
    'MAX_ME': {'erreur_absolue': 'NON', 'nom': 'Erreur commise sur la pointe',
               'numero': '11'},
    'MAX_MAE': {'erreur_absolue': 'OUI', 'nom': 'Erreur commise sur la pointe',
                'numero': '11'},
    'TMAX_ME': {'erreur_absolue': 'NON',
                'nom': 'Décalage temporel sur la pointe', 'numero': '12'},
    'TMAX_MAE': {'erreur_absolue': 'OUI',
                 'nom': 'Décalage temporel sur la pointe', 'numero': '12'},
    'FB': {'nom': 'Biais en fréquence', 'numero': '14'},
    'SR': {'nom': 'Taux de succès', 'numero': '15'},
    'POD': {'nom': 'Taux de détection', 'numero': '16'},
    'POND': {'nom': 'Taux de non détection', 'numero': '17'},
    'FAR': {'nom': 'Taux de fausse alarme', 'numero': '18'},
    'CSI': {'nom': 'Indice de succès critique', 'numero': '19'},
}
"""Informations sur les critères numériques implémentés dans Scores-1.3.3"""

SCORES_NAMES = list(SCORES_ASSOC.keys())
"""Liste des critères numériques implémentés dans Scores-1.3.3"""
