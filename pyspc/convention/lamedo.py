#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - LAMEDO
"""
import os.path
from pyspc.core.config import Config

# =============================================================================
# --- COMMUN
# =============================================================================
DEFAULT_CFG_FILENAME = os.path.join(os.path.dirname(__file__), 'lamedo.txt')
"""Fichier complémentaire pour les webservices LAMEDO"""

HOSTNAMES = {}
if os.path.exists(DEFAULT_CFG_FILENAME):
    config = Config(filename=DEFAULT_CFG_FILENAME)
    config.read()
    HOSTNAMES.update({s: config[s].get('hostname', '') for s in config})

# =============================================================================
# --- BD APBP
# =============================================================================
BDAPBP_DATATYPES = ['short', 'long']
"""Formats des données ApBp au format JSON"""
BDAPBP_DATANAMES = {'short': 'Format court du BP', 'long': 'Format long du BP'}
"""Description des formats de données ApBp au format JSON"""

BDAPBP_COLUMNS = {
    'short': ['CODE', 'DATE', 'MOY', 'LOC'],
    'long': ['CODE', 'NAME', 'DATE', 'MOY', 'DELTA', 'LOC', 'LOCDELTA',
             'DTPROD']
}
BDAPBP_DATE_FORMAT = '%Y%m%d%H%M'

BDAPBP_SCENS = ['MoyInf', 'MoySup', 'LocInf', 'LocSup']
BDAPBP_RAWSCENS = ['MOY', 'DELTA', 'LOC', 'LOCDELTA']

# =============================================================================
# --- BD IMAGE
# =============================================================================
# Copie de QGIS\profil\python\plugins\lamedo\core\bdimage.py > BANDS
# (sans bdibase.Specie)
BDIMAGE_BANDS = {
    'panthere_france': ['rr'],
    'panthere_france-iq': ['rr', 'qualite'],
    'antilope_temps-reel': ['rr'],
    'antilope_v1': ['rr'],
    'antilope_j1': ['rr'],
    'antilope_france-tr-60mn': ['rr', 'qualite'],
    'antilope_france-td-60mn': ['rr', 'qualite'],
    'antilope_france-tr-15mn': ['rr', 'qualite'],
    'antilope_france-td-15mn': ['rr', 'qualite'],
    'comephore_france': ['rr'],
    # RETRAIT AVEC LIBBDIMAGE 1.5.8
    #    'sim-v1_hu2-brut': ['brut'],
    #    'sim-v1_hu2-ref-annuel': ['ref-annuel'],
    #    'sim-v1_hu2-ref-mensuel': ['ref-mensuel'],
    #    'sim-v1_neige-gradient': ['rr'],
    #    'sim-v1_neige-hauteur': ['hauteur'],
    #    'sim-v1_neige-reservoir': ['rr'],
    #    'sim-v1_neige': ['rr'],
    #    'sim-v1_pluie': ['rr'],
    'sim_vent': ['vitesse'],
    'sim_neige': ['rr', 'hauteur'],
    'sim_t': ['t'],
    'sim_hra': ['humidite'],
    'sim_hu': ['brut', 'wg', 'ref-clim', 'ref-jour'],
    'sim_ray': ['solaire', 'infrarouge'],
    'sim_rr': ['total', 'solide', 'liquide', 'fonte', 'ecoulement'],
    'sim_evap': ['rr'],
    'modis-snow_albedo': ['albedo'],
    'modis-snow_fsc': ['fsc'],
    'sympo_rr': ['rr'],
    'sympo_iso0': ['altitude'],
    'sympo_lpn': ['altitude'],
    'sympo_t': ['t'],
    # AJOUT LIBBDIMAGE 1.5.8
    'arpege_rr': ['total', 'liquide', 'solide'],
    'arpege_iso0': ['altitude'],
    'arpege_lpn': ['altitude'],
    'arpege_t': ['t'],
    'arome_rr': ['total', 'liquide', 'neige', 'graupel'],
    'arome_iso0': ['altitude'],
    'arome_lpn': ['altitude'],
    'arome_t': ['t'],
    'arome-ifs_rr': ['total', 'liquide', 'neige', 'graupel'],
    'arome-ifs_iso0': ['altitude'],
    'arome-ifs_lpn': ['altitude'],
    'arome-ifs_t': ['t'],
    # AJOUT LIBBDIMAGE 1.5.11
    'arome-pi_rr': ['total'],
    'piaf_france-antilope-15mn': ['rr'],
}
"""Bandes des images"""

BDIMAGE_FORECASTS = ['sympo',
                     'arpege', 'arome', 'arome-ifs',  # ajout 1.5.8
                     'arome-pi', 'piaf'  # ajout 1.5.11
                     ]
"""Liste des images correspondant à des modèles de prévision"""

# Copie de QGIS\profil\python\plugins\lamedo\core\bdimage.py > UNITS
BDIMAGE_RATIOS = {
    'panthere_france_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    'panthere_france-iq_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    'panthere_france-iq_qualite': (1, ''),  # retour ws en classes
    'antilope_temps-reel_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    'antilope_v1_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    'antilope_j1_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    'antilope_france-tr-60mn_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    'antilope_france-tr-60mn_qualite': (1, ''),  # retour ws en classes
    'antilope_france-td-60mn_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    'antilope_france-td-60mn_qualite': (1, ''),  # retour ws en classes
    'antilope_france-tr-15mn_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    'antilope_france-tr-15mn_qualite': (1, ''),  # retour ws en classes
    'antilope_france-td-15mn_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    'antilope_france-td-15mn_qualite': (1, ''),  # retour ws en classes
    'comephore_france_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    # RETRAIT AVEC LIBBDIMAGE 1.5.8
    #    'sim-v1_hu2-brut_brut': (1, '%'),  # retour ws en percentage
    #    'sim-v1_hu2-ref-annuel_ref-annuel': (1, ''),  # retour ws en classes
    #    'sim-v1_hu2-ref-mensuel_ref-mensuel': (1, ''),  # retour ws en classes
    #    'sim-v1_neige-gradient_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    #    'sim-v1_neige-hauteur_hauteur': (1, 'cm'),  # retour ws en cm
    #    'sim-v1_neige-reservoir_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    #    'sim-v1_neige_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    #    'sim-v1_pluie_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    'sim_vent_vitesse': (1, 'm/s'),  # retour ws en m/s
    'sim_neige_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    'sim_neige_hauteur': (100, 'cm'),  # retour ws en m
    'sim_t_t': (1, '°C'),  # retour ws en c
    'sim_hra_humidite': (1, '%'),  # retour ws en percentage
    'sim_hu_brut': (1, '%'),  # retour ws en percentage
    'sim_hu_wg': (1, '1/100m3/m3'),  # retour ws en 1/100m3/m3
    'sim_hu_ref-clim': (1, ''),  # retour ws en classes
    'sim_hu_ref-jour': (1, ''),  # retour ws en classes
    'sim_ray_solaire': (1, 'w/m2'),  # retour ws en w/m2
    'sim_ray_infrarouge': (1, 'w/m2'),  # retour ws en w/m2
    'sim_rr_total': (0.1, 'mm'),  # retour ws en 1/10mm
    'sim_rr_solide': (0.1, 'mm'),  # retour ws en 1/10mm
    'sim_rr_liquide': (0.1, 'mm'),  # retour ws en 1/10mm
    'sim_rr_fonte': (0.1, 'mm'),  # retour ws en 1/10mm
    'sim_rr_ecoulement': (0.1, 'mm'),  # retour ws en 1/10mm
    'sim_evap_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    'modis-snow_albedo_albedo': (1, '%'),  # retour ws en percentage
    'modis-snow_fsc_fsc': (1, '%'),  # retour ws en percentage
    'sympo_rr_rr': (0.1, 'mm'),  # retour ws en 1/10mm
    'sympo_iso0_altitude': (1, 'm'),  # retour ws en m
    'sympo_lpn_altitude': (1, 'm'),  # retour ws en m
    'sympo_t_t': (1, '°C'),  # retour ws en c
    # AJOUT LIBBDIMAGE 1.5.8
    'arpege_rr_total': (0.1, 'mm'),  # retour ws en 1/10mm
    'arpege_rr_liquide': (0.1, 'mm'),  # retour ws en 1/10mm
    'arpege_rr_solide': (0.1, 'mm'),  # retour ws en 1/10mm
    'arpege_iso0_altitude': (1, 'm'),  # retour ws en m
    'arpege_lpn_altitude': (1, 'm'),  # retour ws en m
    'arpege_t_t': (1, '°C'),  # retour ws en c
    'arome_rr_total': (0.1, 'mm'),  # retour ws en 1/10mm
    'arome_rr_liquide': (0.1, 'mm'),  # retour ws en 1/10mm
    'arome_rr_neige': (0.1, 'mm'),  # retour ws en 1/10mm
    'arome_rr_graupel': (0.1, 'mm'),  # retour ws en 1/10mm
    'arome_iso0_altitude': (1, 'm'),  # retour ws en m
    'arome_lpn_altitude': (1, 'm'),  # retour ws en m
    'arome_t_t': (1, '°C'),  # retour ws en c
    'arome-ifs_rr_total': (0.1, 'mm'),  # retour ws en 1/10mm
    'arome-ifs_rr_liquide': (0.1, 'mm'),  # retour ws en 1/10mm
    'arome-ifs_rr_neige': (0.1, 'mm'),  # retour ws en 1/10mm
    'arome-ifs_rr_graupel': (0.1, 'mm'),  # retour ws en 1/10mm
    'arome-ifs_iso0_altitude': (1, 'm'),  # retour ws en m
    'arome-ifs_lpn_altitude': (1, 'm'),  # retour ws en m
    'arome-ifs_t_t': (1, '°C'),  # retour ws en c
    # AJOUT LIBBDIMAGE 1.5.11
    'arome-pi_rr_total': (0.1, 'mm'),  # retour ws en 1/10mm
    'piaf_france-antilope-15mn_rr': (0.1, 'mm'),  # retour ws en 1/10mm
}
"""Ratios des images"""

BDIMAGE_PREFIX = {
    'panthere_france_rr': 'P',
    'panthere_france-iq_rr': 'P',
    'panthere_france-iq_qualite': None,
    'antilope_temps-reel_rr': 'P',
    'antilope_v1_rr': 'P',
    'antilope_j1_rr': 'P',
    'antilope_france-tr-60mn_rr': 'P',
    'antilope_france-tr-60mn_qualite': None,
    'antilope_france-td-60mn_rr': 'P',
    'antilope_france-td-60mn_qualite': None,
    'antilope_france-tr-15mn_rr': 'P',
    'antilope_france-tr-15mn_qualite': None,
    'antilope_france-td-15mn_rr': 'P',
    'antilope_france-td-15mn_qualite': None,
    'comephore_france_rr': 'P',
    # RETRAIT AVEC LIBBDIMAGE 1.5.8
    #    'sim-v1_hu2-brut_brut': None,
    #    'sim-v1_hu2-ref-annuel_ref-annuel': None,
    #    'sim-v1_hu2-ref-mensuel_ref-mensuel': None,
    #    'sim-v1_neige-gradient_rr': 'P',
    #    'sim-v1_neige-hauteur_hauteur': 'H',
    #    'sim-v1_neige-reservoir_rr': 'P',
    #    'sim-v1_neige_rr': 'P',
    #    'sim-v1_pluie_rr': 'P',
    'sim_vent_vitesse': None,
    'sim_neige_rr': 'P',
    'sim_neige_hauteur': 'H',
    'sim_t_t': 'T',
    'sim_hra_humidite': None,
    'sim_hu_brut': 'H',
    'sim_hu_wg': 'H',
    'sim_hu_ref-clim': 'H',
    'sim_hu_ref-jour': 'H',
    'sim_ray_solaire': None,
    'sim_ray_infrarouge': None,
    'sim_rr_total': 'P',
    'sim_rr_solide': 'P',
    'sim_rr_liquide': 'P',
    'sim_rr_fonte': 'P',
    'sim_rr_ecoulement': 'P',
    'sim_evap_rr': 'E',
    'modis-snow_albedo_albedo': None,
    'modis-snow_fsc_fsc': None,
    'sympo_rr_rr': 'P',
    'sympo_iso0_altitude': 'H',
    'sympo_lpn_altitude': 'H',
    'sympo_t_t': 'T',
    # AJOUT LIBBDIMAGE 1.5.8
    'arpege_rr_total': 'P',
    'arpege_rr_liquide': 'P',
    'arpege_rr_solide': 'P',
    'arpege_iso0_altitude': 'H',
    'arpege_lpn_altitude': 'H',
    'arpege_t_t': 'T',
    'arome_rr_total': 'P',
    'arome_rr_liquide': 'P',
    'arome_rr_neige': 'P',
    'arome_rr_graupel': 'P',
    'arome_iso0_altitude': 'H',
    'arome_lpn_altitude': 'H',
    'arome_t_t': 'T',
    'arome-ifs_rr_total': 'P',
    'arome-ifs_rr_liquide': 'P',
    'arome-ifs_rr_neige': 'P',
    'arome-ifs_rr_graupel': 'P',
    'arome-ifs_iso0_altitude': 'H',
    'arome-ifs_lpn_altitude': 'H',
    'arome-ifs_t_t': 'T',
    # AJOUT LIBBDIMAGE 1.5.11
    'arome-pi_rr_total': 'P',
    'piaf_france-antilope-15mn_rr': 'P',
}
"""Préfixes des images pour correspondance avec les grandeurs de pyspc"""

BDIMAGE_PRECISION = ['standard', 'forte']
"""Precision des requêtes Zones"""

BDIMAGE_STATS = ['standard', 'complete']
"""Precision des requêtes Zones"""
