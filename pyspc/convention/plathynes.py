#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - Projet PLATHYNES
"""

# -----------------------------------------------------------------------------
# --- COMMUN
# -----------------------------------------------------------------------------
DATATYPES = [
    'export',
    'results',
    'data',
    'project',
    'event',
    'model',
    'simul',
]
"""Type de fichiers PLATHYNES"""

DATANAMES = {
    'export': 'Fichier export',
    'results': 'Fichier résultat',
    'data': 'Fichier de données observées (mgr, mqi, mqo)',
    'project': 'Fichier projet (prj)',
    'event': 'Fichier événement (evt)',
    'model': 'Fichier modèle (mwc)',
    'simul': 'Fichier simulation (sim)',
}
"""Description de fichiers PLATHYNES"""

PREFIX_VARNAMES = {
    ('export', 'RR'): 'P',
    ('export', 'Qobs'): 'Q',
    ('export', 'Qsim'): 'Q',
    ('results', 'Rainfall [mm/h]'): 'P',
    ('results', 'QSim [m3/s]'): 'Q',
}
"""Association des grandeurs PLATHYNES et des préfixes des grandeurs PYSPC"""

# -----------------------------------------------------------------------------
# --- CONFIGURATION
# -----------------------------------------------------------------------------

CONFIG_DTYPES = {
    'project': '.prj',  # Projet
    'event': '.evt',    # Evenement
    'model': '.mwc',    # model
    'simul': '.sim',    # simul
}
"""Types et extensions des fichiers de configuration PLATHYNES"""

NUMBER_OF = {
    'event': {
        ('Parameters', 'evenementiels'): 'Nombre de parametres ',
        ('Forcing settings', 'pluies'): 'Nombre de sources de ',
        ('Forcing settings', 'debits'): 'Nombre de sources de ',
        ('Observations', 'observations'): "Nombre de fichiers d'",
        ('Structure height files', 'hauteurs de barrage'):
            "Nombre de fichiers de ",
    },
    'model': {
        ('Stations', 'stations'): 'Number of ',
        ('Unites modele', 'surface units'): 'Number of ',
        ('Unites modele', 'soil units'): 'Number of ',
        ('Unites modele', 'node units'): 'Number of ',
        ('Regression functions', 'regression functions'): 'Number of ',
    },
    'project': {
        ('Tables set', 'tables'): 'Nombre de ',
        ('Charts set', 'graphs'): 'Nombre de ',
        ('Structures set', 'structures'): 'Nombre de ',
        ('Stations', 'pluvio'): 'Nombre de stations ',
        ('Stations', 'hydro'): 'Nombre de stations ',
        ('Configurations', 'configurations'): 'Nombre de ',
        ('Events', 'events'): 'Number of ',
        ('Simulations', 'simulations'): 'Nombre de '
    },
    'simul': {
        ('Events', 'events'): 'Number of ',
        ('Optimisation settings', 'attempts'): 'Number of ',
        ('Cost functions', 'functions'): 'Number of cost ',
    },
}
"""Eléments textuels 'Nombre de' ou 'Number of' selon le type de config."""

# -----------------------------------------------------------------------------
# --- DATA
# -----------------------------------------------------------------------------
DATA_EXT = {
    '.mgr': ('P', False),
    '.mqo': ('Q', False),
    '.mqi': ('Q', True),
    '.mho': ('H', False),  # nouveauté Plathynes 1.10 (octobre 2024)
    '.mhi': ('H', True),  # nouveauté Plathynes 1.10 (octobre 2024)
}
"""Dictionnaire des extensions de fichier de PLATHYNES Data"""

RATIOS = {
    'P': 0.1,  # 1/10 mm -> mm
    'Q': 1,
    'H': 1
}
"""Ratios des unités"""
