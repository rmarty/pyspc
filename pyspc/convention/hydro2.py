#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention -  Hydro2
"""
from datetime import timedelta as td

# -----------------------------------------------------------------------------
# --- COMMUN
# -----------------------------------------------------------------------------
DATATYPES = {
    'export': ['H-TEMPS', 'QJM', 'QTFIX', 'QTVAR',
               'DEBCLA', 'CRUCAL', 'TOUSMOIS', 'SYNTHESE'],
    'data': ['H-TEMPS', 'QJM', 'QTFIX', 'QTVAR', 'TOUSMOIS'],
    'metadata': ['DEBCLA', 'CRUCAL', 'SYNTHESE']
}
"""Procédures d'export Hydro-2"""

DTFMT_CONTENT = {
    'QTFIX': "%Y%m%d %H:%M",
    'QTVAR': "%Y%m%d %H:%M",
    'QJM': "%Y%m%d",
    'H-TEMPS': "%Y%m%d %H:%M",
    'DEBCLA': None,
    'CRUCAL': None,
    'TOUSMOIS': "%Y_%m",
    'SYNTHESE': None
}
"""Format de la date dans les fichiers HYDRO-2 selon la procédure d'export"""

RATIOS_CONTENT = {
    'H-TEMPS': 0.001,
    'QJM': 0.001,
}
"""Ratios pour convertir dans les unités de pyspc"""

LINECONTENTS = {
    # TOUSMOIS
    '907': {
        'station': [3],
        'datatype': [5],
        'subtype': [6],
        'start': [7],
        'end': [8],
        'start_month': [9],
        'QMM_AA': [10],
        'units': [23],
        'precision': [24],
    },
    '908': {
        'station': [2],
    },
    '911': {
        'station': [3],
        'var': [6],
        'start': [7, 10],
        'end': [8, 11],
        'law': [9],
        'units': [17],
    },
    '912': {
        'station': [2],
    },
    '915': {
        'station': [3],
        'start': [6, 8],
        'end': [7, 9]
    },
    '916': {
        'station': [2],
    },
    # QTVAR
    '919': {
        'station': [3],
        'datatype': [5],
        'start': [6, 7],
        'end': [8, 9],
        'units': [11],
    },
    '920': {
        'station': [2],
    },
    # QTFIX
    '921': {
        'station': [3],
        'datatype': [5],
        'start': [6, 7],
        'end': [8, 9],
        'units': [11],
    },
    '922': {
        'station': [2],
    },
    '925': {
        'station': [3],
        'MX_nb': [6],
        'MX_start': [7],
        'MX_QIX': [10],
        'MX_QIXdt': [8, 9],
        'MX_HIX': [14],
        'MX_HIXdt': [12, 13],
        'MX_QJX': [-4],
        'MX_QJdt': [-5],
    },
    '933': {
        'station': [2],
    },
    '941': {
        'station': [3],
        'QMM_nb': [6],
        'QMM_01': [7],
        'QMM_02': [9],
        'QMM_03': [11],
        'QMM_04': [13],
        'QMM_05': [15],
        'QMM_06': [17],
        'QMM_07': [19],
        'QMM_08': [21],
        'QMM_09': [23],
        'QMM_10': [25],
        'QMM_11': [27],
        'QMM_12': [29],
        'QMM_AA': [31],
    },
    '942': {
        'station': [3],
        'QspMM_nb': [6],
        'QspMM_01': [7],
        'QspMM_02': [9],
        'QspMM_03': [11],
        'QspMM_04': [13],
        'QspMM_05': [15],
        'QspMM_06': [17],
        'QspMM_07': [19],
        'QspMM_08': [21],
        'QspMM_09': [23],
        'QspMM_10': [25],
        'QspMM_11': [27],
        'QspMM_12': [29],
        'QspMM_AA': [31],
    },
    '943': {
        'station': [3],
        'LEMM_nb': [6],
        'LEMM_01': [7],
        'LEMM_02': [9],
        'LEMM_03': [11],
        'LEMM_04': [13],
        'LEMM_05': [15],
        'LEMM_06': [17],
        'LEMM_07': [19],
        'LEMM_08': [21],
        'LEMM_09': [23],
        'LEMM_10': [25],
        'LEMM_11': [27],
        'LEMM_12': [29],
        'LEMM_AA': [31],
    },
    '944': {
        'station': [3],
        'QA_law': [7],
        'QA_nb': [8],
        'QA_low5': [9],
        'QA_low5_inf': [10],
        'QA_low5_sup': [11],
        'QA_med': [12],
        'QA_med_inf': [13],
        'QA_med_sup': [14],
        'QA_high5': [15],
        'QA_high5_inf': [16],
        'QA_high5_sup': [17],
        'QA_ave': [18],
        'QA_ave_inf': [19],
        'QA_ave_sup': [20],
    },
    '945': {
        'station': [3],
        'QE_law': [7],
        'QE_nb': [8],
        'QE_VCN3_2': [9],
        'QE_VCN3_2inf': [10],
        'QE_VCN3_2sup': [11],
        'QE_VCN3_5': [12],
        'QE_VCN3_5inf': [13],
        'QE_VCN3_5sup': [14],
        'QE_VCN3_mean': [15],
        'QE_VCN3_std': [16],
        'QE_VCN10_2': [17],
        'QE_VCN10_2inf': [18],
        'QE_VCN10_2sup': [19],
        'QE_VCN10_5': [20],
        'QE_VCN10_5inf': [21],
        'QE_VCN10_5sup': [22],
        'QE_VCN10_mean': [23],
        'QE_VCN10_std': [24],
        'QE_QMNA_2': [25],
        'QE_QMNA_2inf': [26],
        'QE_QMNA_2sup': [27],
        'QE_QMNA_5': [28],
        'QE_QMNA_5inf': [29],
        'QE_QMNA_5sup': [30],
        'QE_QMNA_mean': [31],
        'QE_QMNA_std': [32],
    },
    '946': {
        'station': [3],
        'QC_law': [7],
        'QC_nb': [8],
    },
    '947': {
        'station': [3],
        'DC_nb': [6],
        'DC_99': [7],
        'DC_98': [8],
        'DC_95': [9],
        'DC_90': [10],
        'DC_80': [11],
        'DC_70': [12],
        'DC_60': [13],
        'DC_50': [14],
        'DC_40': [15],
        'DC_30': [16],
        'DC_20': [17],
        'DC_10': [18],
        'DC_05': [19],
        'DC_02': [20],
        'DC_01': [21],
    },
    '950': {
        'station': [1],
        'name': [2],
        'z0': [3],
        'area': [4],
        'provider': [7],
    },
    # H-TEMPS
    'CTH': {
        'station': [1],
    },
    # QJM
    'QJO': {
        'station': [1],
    },
}
"""Informations à extraire selon la ligne"""

PAIRVALUES = {
    '912': ([3], [6]),
    '916': ([3], [4]),
    '920': ([3, 4], [5]),  # QTVAR
    '922': ([3, 4], [5]),  # QTFIX
    '933': ([4], [5, 6, 7]),
    'CTH': ([2, 3], [4]),  # H-TEMPS
    'QJO': ([2], [3]),  # QJM
}
"""Lignes à répartir sous forme de paires (clé, valeur)"""

SEP = {
    '907': '',
    '908': '',
    '911': '',
    '912': '',
    '915': '-',
    '916': '',
    '919': ' ',
    '920': ' ',
    '921': ' ',
    '922': ' ',
    '925': ' ',
    '933': '-',
    '941': '',
    '942': '',
    '943': '',
    '944': '',
    '945': '',
    '946': '',
    '947': '',
    '950': '',
    'CTH': ' ',
    'QJO': '',
}
"""Séparateur à utiliser pour joindre 2 informations"""

LINESTOSKIP = ['CHT', 'DEB', 'DEC', 'FIN', 'VST']
"""Lignes à ignorer"""

# -----------------------------------------------------------------------------
# --- DATA
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# --- EXPORT
# -----------------------------------------------------------------------------
MAX_PERIOD_EXPORT = {
    'QTFIX': td(days=330),
    'QTVAR': td(days=330),
    'QJM': td(days=1000),
    'H-TEMPS': td(days=2000),
    'DEBCLA': None,
    'CRUCAL': None,
    'TOUSMOIS': None,
    'SYNTHESE': None
}
"""Durée maximale d'une période d'export"""

DTFMT_EXPORT = {
    'QTFIX': "%d/%m/%Y %H:%M",
    'QTVAR': "%d/%m/%Y %H:%M",
    'QJM': "%d/%m/%Y",
    'H-TEMPS': "%d/%m/%Y",
    'DEBCLA': ["%d/%m", "%Y"],
    'CRUCAL': ["%d/%m", "%Y"],
    'TOUSMOIS': ["%d/%m", "%Y"],
    'SYNTHESE': None
}
"""Format de la date selon la procédure d'export"""

# -----------------------------------------------------------------------------
# --- STATISTIQUES
# -----------------------------------------------------------------------------
TR = [2, 5, 10, 20, 50, 100]
"""Liste des temps de retour"""
