#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - Référentiel Dreal CVL
"""

# -----------------------------------------------------------------------------
# --- DATATYPES
# -----------------------------------------------------------------------------
REFSPC_DATATYPES = [
    'loc_hydro',  # Lieu de mesure hydro
    'loc_meteo',  # Lieu de mesure météo
    'reach',  # Tronçon réglementaire
    'stat_hydro',  # Statistiques hydrauliques
    'stat_meteo'  # Statistiques pluviométriques
]
"""Types de données du référentiel Dreal CVL"""

REFSPC_DATANAMES = {
    'loc_hydro': 'Lieu de mesure hydro du référentiel Dreal CVL',
    'loc_meteo': 'Lieu de mesure météo du référentiel Dreal CVL',
    'reach': 'Tronçon réglementaire du référentiel Dreal CVL',
    'stat_hydro': 'Statistiques hydrauliques du référentiel Dreal CVL',
    'stat_meteo': ' Statistiques pluviométriques du référentiel Dreal CVL',
}
"""Description des données du référentiel Dreal CVL"""

# -----------------------------------------------------------------------------
# --- COL NAMES
# -----------------------------------------------------------------------------
REFSPC_COLNAMES = {
    False: 'code_hydro2',
    8: 'code_site',
    10: 'code_station',
    12: 'code_capteur',
    'reach': 'code_troncon',
    'mf': 'code_mf',
}

# -----------------------------------------------------------------------------
# --- SQL / LOCATIONS HYDRO
# -----------------------------------------------------------------------------
SQL_L_h2 = """
    SELECT {0}
    FROM CAPTEUR_HYDRO
    INNER JOIN STATION_HYDRO ON STATION_HYDRO.id_station = CAPTEUR_HYDRO.id_station
    WHERE "{2}" IN ({1})
    ORDER BY "{2}"
"""
ATT_L_h2 = ["id_capteur_hydro", "code_hydro2", "code_capteur",
            "nom_capteur_phyc", "type", "departement",
            "x_l93", "y_l93"]
SQL_L_h3 = {
    8: """
    SELECT SITE_HYDRO.{0}
    FROM SITE_HYDRO
    INNER JOIN STATION_HYDRO ON STATION_HYDRO.id_site_hydro = SITE_HYDRO.id_site_hydro
    WHERE "{2}" IN ({1})
    ORDER BY "{2}"
""",
    10: """
    SELECT {0}
    FROM STATION_HYDRO
    WHERE "{2}" IN ({1})
    ORDER BY "{2}"
""",
    12: """
    SELECT {0}
    FROM CAPTEUR_HYDRO
    INNER JOIN STATION_HYDRO ON STATION_HYDRO.id_station = CAPTEUR_HYDRO.id_station
    WHERE "{2}" IN ({1})
    ORDER BY "{2}"
"""
}
ATT_L_h3 = {
    8: ["id_site_hydro", "code_site", "nom_site_hydro",
        "nom_riviere", "type", "departement",
        "id_bnbv", "taille_bv", "x_l93", "y_l93"],
    10: ["id_station", "code_station", "nom_station_hydro", "type",
         "departement", "x_l93", "y_l93", "delai_vigicrues"],  # "delai_vigicrues_2018"
    12: ["id_capteur_hydro", "code_hydro2", "code_capteur",
         "nom_capteur_hydro", "type", "departement",
         "x_l93", "y_l93"]
}

# -----------------------------------------------------------------------------
# --- SQL / LOCATIONS FROM REACHES
# -----------------------------------------------------------------------------
SQL_LfR_h3 = """
    SELECT {0}
    FROM "STATION_TRONCON"
    INNER JOIN STATION_HYDRO ON STATION_HYDRO.id_station = STATION_TRONCON.id_station
    INNER JOIN TRONCON ON TRONCON.id_troncon = STATION_TRONCON.id_troncon
    WHERE "code_troncon" IN ({1}) AND {2} IS NOT NULL
    ORDER BY "{2}"
"""
ATT_LfR_h3 = ["code_station", "code_troncon"]
SQL_LfR_h2 = """
    SELECT {0}
    FROM "STATION_TRONCON"
    INNER JOIN STATION_HYDRO ON STATION_HYDRO.id_station = STATION_TRONCON.id_station
    INNER JOIN TRONCON ON TRONCON.id_troncon = STATION_TRONCON.id_troncon
    INNER JOIN CAPTEUR_HYDRO ON STATION_HYDRO.id_station = CAPTEUR_HYDRO.id_station
    WHERE "code_troncon" IN ({1}) AND {2} IS NOT NULL
    ORDER BY "{2}"
"""
ATT_LfR_h2 = ["code_hydro2", "code_troncon"]

# -----------------------------------------------------------------------------
# --- SQL / LOCATIONS METEO
# SELECT SUBSTR(CAST(100000000 + "code_mf" AS VARCHAR), 2, 8) as "code_mf"
# FROM "SITE_METEO"
# WHERE SUBSTR(CAST(100000000 + "code_mf" AS VARCHAR), 2, 8) LIKE '07%'

# -----------------------------------------------------------------------------
#SQL_LfD_m = """
#    SELECT "code_mf"
#    FROM "SITE_METEO"
#    WHERE {}
#"""
SQL_LfD_m = """
    SELECT SUBSTR(CAST(100000000 + "code_mf" AS VARCHAR), 2, 8) as "code_mf"
    FROM "SITE_METEO"
    WHERE {}
"""
SQL_L_m = """
    SELECT {0}
    FROM "SITE_METEO"
    WHERE "{2}" IN ({1})
    ORDER BY "{2}"
"""
ATT_L_m = ["id_site_meteo", "code_mf", "nom_mf", "nom_site_meteo",
           "x_l93", "y_l93", "z"]

# -----------------------------------------------------------------------------
# --- SQL / REACHES
# -----------------------------------------------------------------------------
SQL_R = """
    SELECT {0}
    FROM "TRONCON"
    WHERE "{2}" IN ({1})
    ORDER BY "{2}"
"""
ATT_R = ["id_troncon", "code_troncon", "nom_troncon"]

# -----------------------------------------------------------------------------
# --- SQL / REACHES FROM LOCATIONS
# -----------------------------------------------------------------------------
SQL_RfL = """
    SELECT {0}
    FROM "STATION_TRONCON"
    INNER JOIN STATION_HYDRO ON STATION_HYDRO.id_station = STATION_TRONCON.id_station
    INNER JOIN TRONCON ON TRONCON.id_troncon = STATION_TRONCON.id_troncon
    WHERE "code_station" IN ({1})
    ORDER BY "{2}"
"""
ATT_RfL = ["code_troncon", "code_station"]

# -----------------------------------------------------------------------------
# --- SQL / STATS HYDRO
# -----------------------------------------------------------------------------
SQL_S_h3 = """
    SELECT {0}
    FROM STATS_HYDRO
    INNER JOIN SITE_HYDRO ON SITE_HYDRO.id_site_hydro = STATS_HYDRO.id_site_hydro
    WHERE "{2}" IN ({1})
    ORDER BY "{2}"
"""
ATT_S_h3 = ["id_stats_hydro", "code_site",
            "t2", "t5", "t10", "t20", "t50", "t100",
            "methode", "type_loi", "taille_echantillon",
            "periode_echantillon", "date_calcul", "stat_ref_spc"]

# -----------------------------------------------------------------------------
# --- SQL / STATS METEO
# -----------------------------------------------------------------------------
SQL_S_m = """
    SELECT {0}
    FROM STATS_PLUVIO
    INNER JOIN SITE_METEO ON SITE_METEO.id_site_meteo = STATS_PLUVIO.id_site_meteo
    WHERE SITE_METEO."{2}" IN ({1})
    ORDER BY SITE_METEO."{2}"
"""
ATT_S_m = ["id_stats_pluvio", "code_mf", "duree",
           "t2", "t5", "t10", "t20", "t50", "t100",
           "methode", "taille_echantillon", "periode_echantillon",
           "date_calcul", "source"]
