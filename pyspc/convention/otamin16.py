#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - Projet OTAMIN v2016
"""
from datetime import timedelta as td

# -----------------------------------------------------------------------------
# --- CSV
# -----------------------------------------------------------------------------
CAL_COLS = ['Stations', 'Grandeurs', 'Modeles']
"""Liste des entêtes du format csv de Otamin v2016"""
CAL_TDELTA = td(hours=1)
"""Pas de temps des échéances du format csv de Otamin v2016"""


# -----------------------------------------------------------------------------
# --- PRV
# -----------------------------------------------------------------------------
EXPORT_DTYPES = {
    'fcst': 'PREVISION',
    'trend': 'PREVISION + OTAMIN',
}
"""Type de format prv de Otamin v2016"""

EXPORT_UNITS = {
    'Q': 'm3/s',
    'H': 'm',
    'RR': 'mm',
    'TA': 'Celsius'
}
"""Unités des grandeurs pour le format prv de Otamin v2016"""

EXPORT_COLS = ['Stations', 'Grandeurs', 'IdSeries',
               '# Modeles', '# Scenarios', '# DtDerObs']
"""Liste des entêtes du format prv de Otamin v2016"""

EXPORT_TREND_COLS = ['# Probas']
"""Liste des entêtes supplémentaires pour les tendances"""
