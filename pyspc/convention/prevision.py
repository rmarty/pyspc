#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - Prévisions du SPC LCI
"""

# =============================================================================
# --- COMMUN
# =============================================================================

# =============================================================================
# --- PREVISION - 2014
# =============================================================================
P14_DATATYPES = [
    'previ14',  # Bd Prevision SPC LCI 2014-2016 : prv brutes
    'previ14_val',  # Bd Prevision SPC LCI 2014-2016 : prv valid
]
"""Types de prévision issues de Prevision14"""

P14_DATANAMES = {
    'previ14': 'Bd Prevision SPC LCI 2014-2016 : prv brutes',
    'previ14_val': 'Bd Prevision SPC LCI 2014-2016 : prv validées',
}
"""Description des prévision issues de Prevision14"""

P14_VARNAMES = {
    'PluiVal': 'PH',
    'DebiVal': 'QH',
    'HautVal': 'HH',
}
"""Table d'association des grandeurs de Prevision14"""

P14_ATTS_META = {
    ('hydro2', True): [
        'NSerie', 'CodeStation', 'DateSerie', 'CodeModele', 'DateIncer',
        'Qcen', 'Qinf', 'Qsup', 'Hcen', 'Hinf', 'Hsup'],
    ('hydro2', False): [
        'NSerie', 'CodeStation', 'DateSerie', 'CodeModele',
        'Hauteur', 'Debit', 'Pluie'],
    'hydro3': ['NSerie', "CodeStation", "DateDerObs", "CodeModele"]
}
"""Attributs des méta-données des séries"""
P14_SQL_META = {
    ('hydro2', True): """
    SELECT {1}
    FROM {0}
    WHERE ({0}.{2} IN ({3}))
    """,
    ('hydro2', False): """
    SELECT {1}
    FROM {0}
    WHERE ({0}.{2} IN ({3}))
    """,
    'hydro3': """
    SELECT {1}
    FROM {0}
    WHERE ({0}.{2} IN ({3}))
    """,
}
P14_TABLES_META = {True: 'Serie', False: 'Serie_B'}
"""Table des méta-données des séries"""

P14_ATTS_DATA = {
    ('hydro2', True): [
        'NSerie', 'DateVal', 'PluiVal', 'DebiVal', 'DebiInfVal', 'DebiSupVal',
        'HautVal', 'HautInfVal', 'HautSupVal'],
    ('hydro2', False): [
        'NSerie', 'DateVal', 'PluiVal', 'DebiVal', 'HautVal'],
    'hydro3': ['NSerie', "DateVal", "Val", "Val10", "Val50", "Val90"]
}
"""Attributs des données des séries"""
P14_SQL_DATA = {
    ('hydro2', True): """
    SELECT {1}
    FROM {0}
    WHERE ({0}.{2} = {3})
    """,
    ('hydro2', False): """
    SELECT {1}
    FROM {0}
    WHERE ({0}.{2} = {3})
    """,
    'hydro3': """
    SELECT {1}
    FROM {0}
    WHERE ({0}.{2} = {3})
    """,
}
P14_TABLES_DATA = {True: 'Prevues', False: 'Prevues_B'}
"""Table des données des séries"""


# =============================================================================
# --- PREVISION - 2017
# =============================================================================
P17_DATATYPES = [
    'previ17',  # Bd Prevision SPC LCI 2017-2019 : prv brutes
    'previ17_val',  # Bd Prevision SPC LCI 2017-2019 : prv valid
    'previ17_diff',  # Bd Prevision SPC LCI 2017-2019 : prv valid diff
]
"""Types de prévision issues de Prevision17"""

P17_DATANAMES = {
    'previ17': 'Bd Prevision SPC LCI 2017-2019 : prv brutes',
    'previ17_val': 'Bd Prevision SPC LCI 2017-2019 : prv validées',
    'previ17_diff': 'Bd Prevision SPC LCI 2017-2019 : prv validées diffusées',
}
"""Description des prévision issues de Prevision14"""

P17_VARNAMES = {
    (8, True): 'HH',
    (8, False): 'QH',
    (10, True): 'QH',
    (10, False): 'HH',

}
"""Table d'association des grandeurs de Prevision17"""

P17_ATTS_META = {
    True: ["CodeStation", "CodeModele", "DateDerObs", "Source", "Diffuse"],
    False: ["CodeStation", "CodeModele", "DateDerObs"],  # , "Source"
}
"""Attributs des méta-données des séries"""
P17_SQL_META = {
    (True, True): """
    SELECT {0}.NSerie, {0}.{1}, {0}.{3}, {0}.{2}, {0}.{4}, {0}.{5}
    FROM {0}
    WHERE ({0}.{1} IN ({6})) AND ({0}.{5} = 1)
    """,
    (True, False): """
    SELECT {0}.NSerie, {0}.{1}, {0}.{3}, {0}.{2}, {0}.{4}, {0}.{5}
    FROM {0}
    WHERE ({0}.{1} IN ({6}))
    """,
    (False, False): """
    SELECT {0}.NSerie, {0}.{1}, {0}.{3}, {0}.{2}
    FROM {0}
    WHERE ({0}.{1} IN ({4}))
    """  # , {0}.{4} // WHERE ({0}.{1} IN ({5}))
}
P17_TABLES_META = {True: 'Serie', False: 'Serie_B'}
"""Table des méta-données des séries"""

P17_ATTS_DATA = {
    True: ["DateVal", "Val", "Val10", "Val50", "Val90",
           "Valconv", "Val10conv", "Val50conv", "Val90conv"],
    False: ["DateVal", "Val", "Val10", "Val50", "Val90",
            "Valconv", "Val10conv", "Val50conv", "Val90conv"],
}
"""Attributs des données des séries"""
P17_SQL_DATA = """
    SELECT {0}.NSerie, {0}.{1}, {0}.{3}, {0}.{2}, {0}.{4}, {0}.{5}, {0}.{6}, {0}.{7}, {0}.{8}, {0}.{9}
    FROM {0}
    WHERE {0}.NSerie = {10}
"""
P17_TABLES_DATA = {True: 'Prevues', False: 'Prevues_B'}
"""Table des données des séries"""

P17_ATTS_MODEL = ['CodeModele', 'Nom', 'CodePOM']
"""Attributs des méta-données des modèles"""
P17_TABLES_MODEL = 'Modeles'
"""Table des méta-données des modèles"""

# =============================================================================
# --- PREVISION - 2019
# =============================================================================
P19_DATATYPES = [
    'previ19',  # Bd Prevision SPC LCI 2019-.... : prv brutes
    'previ19_val',  # Bd Prevision SPC LCI 2019-.... : prv valid
    'previ19_diff',  # Bd Prevision SPC LCI 2019-.... : prv valid diff
]
"""Types de prévision issues de Prevision19"""

P19_DATANAMES = {
    'previ19': 'Bd Prevision SPC LCI 2019-.... : prv brutes',
    'previ19_val': 'Bd Prevision SPC LCI 2019-.... : prv validées',
    'previ19_diff': 'Bd Prevision SPC LCI 2019-.... : prv validées diffusées',
}
"""Description des prévision issues de Prevision14"""

P19_VARNAMES = {
    (8, True): 'HH',
    (8, False): 'QH',
    (10, True): 'QH',
    (10, False): 'HH',

}
"""Table d'association des grandeurs de Prevision19"""
P19_RATIOS = {
    ('HH', '.mdb'): 0.001,
    ('QH', '.mdb'): 0.001,
}
"""Ratios d'unités des grandeurs de Prevision19"""

P19_ATTS_MODEL = {
    '.mdb': ["CodeModele", "Nom", "CodePOM"],
    '.sqlite': ["id_modeles", "CodeModele", "Nom", "CodePOM"],
}
"""Attributs des méta-données des modèles"""
P19_SQL_MODEL = """
    SELECT {0}
    FROM {1}
    ORDER BY CodeModele
"""
P19_SQL_INSERTMODEL = """
    INSERT INTO {0} ({1})
    VALUES ({2})
"""
P19_TABLES_MODEL = {'.mdb': 'Modeles', '.sqlite': 'Modeles'}
"""Table des méta-données des modèles"""


P19_ATTS_META = {
    True: ["NSerie", "CodeStation", "CodeModele", "DateDerObs",
           "Source", "Diffuse"],
    False: ["NSerie", "CodeStation", "CodeModele", "DateDerObs"],  # , "Source"
}
"""Attributs des méta-données des séries"""
P19_SQL_META = """
    SELECT {0}
    FROM {3}
    WHERE {1}
    ORDER BY {2}
"""
P19_SQL_UNIQUEMETA = """
    SELECT {0}
    FROM {3}
    WHERE {2} IN ({1})
    ORDER BY {2} DESC
"""
P19_SQL_INSERTMETA = """
    INSERT INTO {0} ({1})
    VALUES ({2})
"""
P19_STATUS_META = {'auto': 1, 'pilote': 2, 'expresso': 3}
"""Statut des séries validées"""
P19_TABLES_META = {True: 'Serie', False: 'Serie_B'}
"""Table des méta-données des séries"""

P19_ATTS_DATA = {
    True: ["NSerie", "DateVal",
           "Val", "Val10", "Val50", "Val90",
           "Valconv", "Val10Conv", "Val50conv", "Val90conv"],
    False: ["NSerie", "DateVal",
            "Val", "Val10", "Val50", "Val90",
            "Valconv", "Val10Conv", "Val50conv", "Val90conv"],
}
"""Attributs des données des séries"""
P19_SQL_DATA = """
    SELECT {0}
    FROM {4}
    WHERE {2} IN ({1})
    ORDER BY {2}, {3} ASC
"""
P19_SQL_INSERTDATA = """
    INSERT INTO {0} ({1})
    VALUES ({2})
"""
P19_TABLES_DATA = {True: 'Prevues', False: 'Prevues_B'}
"""Table des données des séries"""
