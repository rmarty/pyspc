#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - GRP version 2020
"""

# -----------------------------------------------------------------------------
# --- COMMUN
# -----------------------------------------------------------------------------
DATATYPES = [
    "grp20_cal_data", "grp20_cal_event", "grp20_cal_fcst",
    "grp20_rt_data", "grp20_rt_metscen",
    "grp20_rt_obs", "grp20_rt_obs_diff",
    "grp20_rt_sim", "grp20_rt_sim_diff",
    "grp20_rt_fcst", "grp20_rt_fcst_diff",
    "grp20_rt_archive", "grp20_rt_intern", "grp20_rt_intern_diff",
]
"""Types de données de GRP version 2020"""

DATANAMES = {
    "grp20_cal_data": "Fichier de données observées (BDD_var / *)",
    "grp20_cal_event": "Fichier événement",
    "grp20_cal_fcst": "Fichier de prévision (Resultats / *_PP_P0P0.TXT)",
    "grp20_rt_data": "Fichier de données observées (Entrées / *.txt)",
    "grp20_rt_metscen":
        "Fichier de scénarios météorologiques (Entrées / Scen*)",
    "grp20_rt_obs": "Fichier de sortie, observation temps-réel",
    "grp20_rt_obs_diff":  "Fichier de sortie, observation temps-différé",
    "grp20_rt_sim":
        "Fichier de sortie, prévision sans assimilation temps-réel",
    "grp20_rt_sim_diff":
        "Fichier de sortie, prévision sans assimilation temps-différé",
    "grp20_rt_fcst":
        "Fichier de sortie, prévision avec assimilation temps-réel",
    "grp20_rt_fcst_diff":
        "Fichier de sortie, prévision avec assimilation temps-différé",
    "grp20_rt_archive": "Fichier de données archivées (BD_var / SITE / *.DAT)",
    "grp20_rt_intern": "Fichier de données internes temps-réel (PQE*.DAT)",
    "grp20_rt_intern_diff":
        "Fichier de données internes temps-différé (PQE*.DAT)",
}
"""Description des données de GRP version 2020"""

# -----------------------------------------------------------------------------
# --- CAL - BASSIN
# -----------------------------------------------------------------------------
CAL_BASIN_LINESEP = "!---------------------------------------------"\
                    "-----------------------------------------------"\
                    "------------------!\n"
CAL_BASIN_HEADERS = {
    'E': "! AAAAAAAA NNNNNN.NN ! Format ecriture code et "
         "ponderation station ETP (1 ligne par station, "
         "commencant par E) !\n",
    'P': "! AAAAAAAA NNNNNN.NN nnJnnHnnM ! Format ecriture code, pond et pdt "
         "pluvio (1 ligne par poste, commencant par P)!\n",
    'T': "! AAAAAAAA NNNNNN.NN NNNN.NN ! Format ecriture code et ponderation "
         "temperature (1 ligne par station, commencant par T) !\n",
    'N': "! NNNNNN.NN ! Facteur correctif de la sous-captation de la neige "
         "                                              !\n",
    'L': "! nnJnnHnnM ! Duree entre 2 donnees a partir de laquelle on "
         "considere une periode comme lacunaire (1 ligne commencant par L) !\n"
}

# -----------------------------------------------------------------------------
# --- CAL - CONFIG
# -----------------------------------------------------------------------------
CAL_CONFIG_HEADER = """################################################################################  README UTILISATEURS  ##################################################################################################
# --- CARACTERISTIQUES DU BASSIN                                                                                                                                                                        #
# Code : Identifiant du bassin                                                                                                                                                                          #
# PDT : Pas de temps du modèle - format : xxJxxHxxM (exemple : 00J01H00M pour le pas de temps 1h)                                                                                                       #
# Nom Station : Libellé du bassin                                                                                                                                                                       #
# Superf. : Superficie du bassin (en km2)                                                                                                                                                               #
# RT : Référentiel temporel (TU, HL ou HH)                                                                                                                                                              #
# Date début, Date fin : Dates de début et de fin de la chronique étudiée - format : JJ/MM/AAAA HH:mm                                                                                                   #
#                                                                                                                                                                                                       #
# --- CALAGE DU MODELE                                                                                                                                                                                  #
# S : Configuration sans module neige CemaNeige - A : Configuration avec module neige CemaNeige - 0 = NON, 1 = OUI                                                                                      #
# T : Correction des sorties avec la méthode Tangara - R : Correction des sorties par réseau de neurones artificiels - 0 = NON, 1 = OUI                                                                 #
# HOR1, HOR2 : Horizons de calage à tester - format : xxJxxHxxM. Pour ignorer HOR2, mettre 00J00H00M                                                                                                    #
# SeuilC1, SeuilC2 : Seuils de calage en m3/s. Pour ignorer SeuilC2, mettre -99.                                                                                                                        #
#                                                                                                                                                                                                       #
# --- ANALYSE DES RESULTATS                                                                                                                                                                             #
# SeuilV1, SeuilV2, SeuilV3 : Seuils de vigilance utilisés pour l'analyse des résultats (en m3/s)                                                                                                       #
# NJ : Nombre de jours présents sur la fenêtre temporelle de chaque évènement pour le tracé des hydrogrammes prévus.                                                                                    #
# HC : Horizon des cheveux sur le tracé des hydrogrammes prévus - format : xxJxxHxxM. Pour ne pas les tracer, mettre '00J00H00M'                                                                        #
# EC : Ecart entre les cheveux des prévisions (en pas de temps)                                                                                                                                         #
# Ecart : Ecart de débit entre prévision et observation au-delà duquel les dates concernées seront référencées (en m3/s)                                                                                #
# Inc : Génération des incertitudes - 0 = NON, 1 = OUI                                                                                                                                                  #
#                                                                                                                                                                                                       #
# --- FORMATS                                                                                                                                                                                           #
# N : Chiffre entre 0 et 9                                                                                                                                                                              #
# S : Caractère alphanumerique                                                                                                                                                                          #
#########################################################################################################################################################################################################
!--- CARACTERISTIQUES DU BASSIN                                                                  !##!--- CALAGE DU MODELE                         !##!--- ANALYSE DES RESULTATS                         !
!--------!---------!------------------------------!---------!--!----------------!----------------!##!-!-!-!-!---------!---------!--------!--------!##!--------!--------!--------!--!---------!--!-----!I!
!--------!---------!------------------------------!---------!--!----------------!----------------!##!S!S!A!A!---------!---------!--------!--------!##!--------!--------!--------!--!---------!--!-----!n!
!    Code!PDT      !Nom Station                   !  Superf.!RT!Date debut      !Date fin        !##!T!R!T!R!HOR1     !HOR2     ! SeuilC1! SeuilC2!##! SeuilV1! SeuilV2! SeuilV3!NJ!HC       !EC!Ecart!c!
!--------!---------!------------------------------!---------!--!----------------!----------------!##!-!-!-!-!---------!---------!--------!--------!##!--------!--------!--------!--!---------!--!-----!-!
!SSSSSSSS!NNJNNHNNM!SSSSSSSSSSSSSSSSSSSSSSSSSSSSSS!NNNNNN.NN!SS!NN/NN/NNNN NN:NN!NN/NN/NNNN NN:NN!##!N!N!N!N!NNJNNHNNM!NNJNNHNNM!NNNNN.NN!NNNNN.NN!##!NNNNN.NN!NNNNN.NN!NNNNN.NN!NN!NNJNNHNNM!NN!NNNNN!N!
!--------!---------!------------------------------!---------!--!----------------!----------------!##!-!-!-!-!---------!---------!--------!--------!##!--------!--------!--------!--!---------!--!-----!-!
"""
CAL_CONFIG_FORMAT = \
    '!{CODE:8s}!{PDT:9s}!{NOM:<30s}!{SURFACE:9.2f}!{RT:2s}!{DEB:16s}'\
    '!{FIN:16s}!##!{ST:1d}!{SR:1d}!{AT:1d}!{AR:1d}!{HOR1:9s}!{HOR2:9s}'\
    '!{SC1:8.2f}!{SC2:8.2f}!##!{SV1:8.2f}!{SV2:8.2f}!{SV3:8.2f}!{NJ:2d}'\
    '!{HC:9s}!{EC:2d}!{ECART:5d}!{INC:1d}!\n'
CAL_CONFIG_NAMES = [
    'CODE', 'PDT', 'NOM', 'SURFACE', 'RT', 'DEB', 'FIN',
    'ST', 'SR', 'AT', 'AR', 'HOR1', 'HOR2', 'SC1', 'SC2',
    'SV1', 'SV2', 'SV3', 'NJ', 'HC', 'EC', 'ECART', 'INC']
"""Colonnes du fichier de configuration de GRP Calage"""
CAL_CONFIG_COMPNAMES = ['HOR', 'SC', 'SV']
"""Colonnes permettant la comparaison des modèles"""
CAL_CONFIG_COMPMISS = {'HOR': '00J00H00M', 'SC': -99.0, 'SV': -99.0}
CAL_CONFIG_CONVERTERS = [
    str, str, str, float, str, str, str,
    int, int, int, int, str, str, float, float,
    float, float, float, int, str, int, int, int]
CAL_CONFIG_BVKEYS = CAL_CONFIG_NAMES[:7]
"""Colonnes relatives aux caractéristiques du bassin"""
CAL_CONFIG_MODKEYS = CAL_CONFIG_NAMES[7:15]
"""Colonnes relatives aux caractéristiques du modèle"""
CAL_CONFIG_CALKEYS = [
    'CODE', 'PDT', 'DEB', 'FIN',
    'ST', 'SR', 'AT', 'AR', 'HOR1', 'HOR2', 'SC1', 'SC2']
"""Colonnes relatives aux caractéristiques de la calibration"""
CAL_CONFIG_CALNAMES = {'SR': 'SMN_RNA', 'ST': 'SMN_TAN',
                       'AR': 'AMN_RNA', 'AT': 'AMN_TAN'}
"""Noms longs des paramètres de calibration"""

# -----------------------------------------------------------------------------
# --- CAL - DATA
# -----------------------------------------------------------------------------
CAL_DATA_HEADERS = {'Q': 'Q(m3/s)', 'P': 'P(mm)', 'T': 'T(°C)', 'E': 'ETP(mm)'}
"""Dictionnaire des entêtes de fichier"""
CAL_DATA_VARNAMES = list(CAL_DATA_HEADERS.keys())
"""Liste des variables autorisées"""

# -----------------------------------------------------------------------------
# --- CAL - EVENT
# -----------------------------------------------------------------------------
CAL_EVENT_HEADERS = {'index': 'Date', 'Q': 'Debit(m3/s)', 'P': 'Pluie(mm/h)'}
"""Dictionnaire des entêtes de fichier"""

# -----------------------------------------------------------------------------
# --- CAL - FCST
# -----------------------------------------------------------------------------
CAL_FCST_DATEFORMAT = '%Y%m%d%H'
"""Format des dates dans les fichiers de prévisions de GRP 2020"""

# -----------------------------------------------------------------------------
# --- CAL - VERIF
# -----------------------------------------------------------------------------
CAL_VERIF_DTYPES = {'TESTS': 'cal', 'CALAG': 'rtime'}
CAL_VERIF_NAMES = {
    'cal': 'Fiche en calage-contrôle', 'rtime': 'Fiche en calage complet'}
CAL_VERIF_SCORES = ['Eff_Cal', 'Eff_Val', 'POD', 'FAR', 'CSI']

# -----------------------------------------------------------------------------
# --- TR - ARCHIVE
# -----------------------------------------------------------------------------
RT_ARCHIVE_DATEFORMAT = '%Y%m%d%H%M'
RT_ARCHIVE_DATESHORTFORMAT = '%m%d%H%M'
RT_ARCHIVE_VARNAMES = ['PV', 'QV', 'TV']
"""Variables associées aux fichiers de type Archive de GRP Temps-Réel"""
RT_ARCHIVE_HEADERS = {
    'PV': 'Pluie (mm)',
    'QV': 'Débit (L/s)',
    'TV': 'Temp. (°C)',
}

# -----------------------------------------------------------------------------
# --- TR - BASSIN
# -----------------------------------------------------------------------------
#RT_BASIN_LINESEP = "#--------------------------------------------------------"\
#    "-------------\n"
#RT_BASIN_HEADERS = {
#    'A': "#          FFFFFF.FF ! Format ecriture superficie du bassin  "
#         "(1 ligne commencant par S)\n",
#    'B': "# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA ! Format ecriture nom station "
#         "(1 ligne commencant par B)\n",
#    'E': "# AAAAAAAA FFFFFF.FF ! Format ecriture code et ponderation station "
#         "ETP (1 ligne par station, commencant par E)\n",
#    'F': "# AAAAAAAA FFFFFF.FF  AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA "
#         "! Format ecriture code, nom et ponderation "
#         "pluvio (1 ligne par poste, commencant par P)\n",
#    'G': "# FFFFF.FF           ! Format ecriture Qvig "
#         "(1 ligne commencant par G)\n",
##    'P': "",  # A laisser commenté car entête par 'F'
#    'Q': "# AAAAAAAA           ! Format ecriture code Hydro "
#         "(1 ligne commencant par Q)\n",
#    'S': "#          FFFFFF.FF ! Format ecriture superficie du bassin  "
#         "(1 ligne commencant par S)\n",
#    'T': "#                 II ! Format ecriture decalage temporel des series "
#         "de pluie et debit par rapport au UTC (2 lignes commencant par T)\n",
#}
#RT_BASIN_FORMATS = {
#    'A': "A          {0:9.2f} ! Altitude mediane du bassin (m)\n",
#    'B': "B {0:<30s} ! Nom station\n",
#    'E': "E {0:8s} {1:9.2f} ! Code et ponderation station ETP {2:>4d}\n",
#    'F': "F          {0:9.2f}  Pluie annuelle moyenne (mm/an)  \n",
#    'G': "G {0:8.2f}           ! Seuil de vigilance (m3/s)\n",
#    'P': "P {0:8s} {1:9.2f}  {2:30s}           ! Code, ponderation et "
#         "nom poste pluvio {3:>3d}\n",
#    'Q': "Q {0:8s}           ! Code Hydro\n",
#    'S': "S          {0:9.2f} ! Superficie du bassin (km2)\n",
#    'T': "T                 {0:>2d} ! Decalage temporel de la serie de {1} "
#         "par rapport au UTC\n",
#}

# -----------------------------------------------------------------------------
# --- TR - CONFIG
# -----------------------------------------------------------------------------
RT_CONFIG_DATATYPES = {
    'config_prevision': 'rt_config_run',
    'Fichiers_sortie_GRP': 'rt_config_outputs',
}
"""Type de fichier de configuration de GRP Temps-Réel selon son préfixe"""
RT_CONFIG_KEYS = {
    'rt_config_run': [
        ('GENERAL', 'MODFON'), ('GENERAL', 'INSTPR'), ('GENERAL', 'CONFIRM'),
        ('CHEMINS', 'BDD'), ('CHEMINS', 'OBS'), ('CHEMINS', 'SCE'),
        ('CHEMINS', 'PRV'), ('CHEMINS', 'R'),
        ('OBSERVATIONS', 'TYPE'),
        ('SCENARIOS', 'TYPE'), ('SCENARIOS', 'REF'), ('SCENARIOS', 'EXTRA'),
        ('SORTIES', 'TYPE'), ('SORTIES', 'REF'), ('SORTIES', 'UNITE'),
        ('SORTIES', 'CODMPP'), ('SORTIES', 'AFFOBS'), ('SORTIES', 'SIMULA')],
    'rt_config_outputs': [
        ('SORTIES', 'DEPASS'), ('SORTIES', 'PREVI'),
        ('ARCHIVES', 'HISTO_PRV_2HOR'), ('ARCHIVES', 'HISTO_PRV'),
        ('DERNIER_EXPORT', 'GRP_EXP'),
        ('FICHES_CONTROLE', 'FCH'), ('FICHES_CONTROLE', 'FCC'),
        ('FICHES_CONTROLE', 'FCN'), ('FICHES_CONTROLE', 'FCS'),
        ('INCERTITUDES', 'GRP_PRV_INC'),
        ('RAPPORTS', 'LIST_PB')],
}
"""Balises des fichiers de configuration de GRP Temps-Réel"""

# -----------------------------------------------------------------------------
# --- TR - DATA
# -----------------------------------------------------------------------------
RT_DATA_OBSFILEPREFIX = {
    'H': 'Hauteur', 'P': 'Pluie', 'Q': 'Debit', 'T': 'Temp'}
"""Préfixe des noms de base des fichiers d'entrée de GRP Temps-Réel"""
RT_DATA_LINEPREFIX = {
    'H': 'HAU', 'P': 'PLU', 'Q': 'DEB', 'T': 'TEM'}
"""Préfixe des lignes de données des fichiers d'entrée de GRP Temps-Réel"""
RT_DATA_SCENFILEPREFIX = {'P': 'Scen_', 'T': 'ScenT_'}
"""Préfixe des noms de fichier des scénarios météo"""
RT_DATA_SCENFORMAT = {
    'P': "Scen_{0}_Plu{1}_{2}.txt", 'T': "ScenT_{0}_Tem{1}_{2}.txt"}
"""Nom de base d'un fichier de scénario météo"""
RT_DATA_SCENHEADER = {'P': 'TYP;CODE_BAS;AAAAMMJJhhmm;P(mm)',
                      'T': 'TYP;CODE_BAS;AAAAMMJJhhmm;Temp(°C)'}
"""Entête des données de scénarios météorologiques"""
RT_DATA_VARNAMES = ['H', 'P', 'Q', 'T']
"""Liste des variables autorisées"""
RT_DATA_SCENVARNAMES = ['P', 'T']
"""Liste des variables autorisées pour les scénarios météo"""

# -----------------------------------------------------------------------------
# --- TR - FCST
# -----------------------------------------------------------------------------
RT_FCST_DTYPES = ["obs", "obs_diff", "sim", "sim_diff", "fcst", "fcst_diff"]
"""Types de fichiers de prévision de GRP Temps-Réel"""
RT_FCST_FILEPREFIX = {
    'obs': 'GRP_Obs', 'obs_diff': 'GRP_D_Obs',
    'sim': 'GRP_Simu_', 'sim_diff': 'GRP_D_Simu_',
    'fcst': 'GRP_Prev_', 'fcst_diff': 'GRP_D_Prev_',
}
"""Préfixe des noms de fichier de prévision de GRP Temps-Réel"""
RT_FCST_LINEPREFIX = {'obs': 'OBS', 'sim': 'SIM', 'fcst': 'PRV'}
"""Préfixe des lignes de données des fichiers de prévision de GRP Temps-Réel"""
RT_FCST_VARNAMES = {'DEBIT': 'Q', 'PLUIE': 'P', 'Temperature': 'T'}
"""Association entre les colonnes et les grandeurs de GRP Temps-Réel"""

# -----------------------------------------------------------------------------
# --- TR - INTERN
# -----------------------------------------------------------------------------
RT_INTERN_BASENAMES = {'intern': 'PQE_1A.DAT', 'intern_diff': 'PQE_1A_D.DAT'}
"""Noms de base des fichiers de type Intern de GRP Temps-Réel"""
RT_INTERN_DTYPES = {'intern': True, 'intern_diff': False}
"""Nature du temps réel (True) ou différé (False) par type de fichier Intern"""

# -----------------------------------------------------------------------------
# --- TR - PARAM
# -----------------------------------------------------------------------------
RT_PARAM_LIST = ['INTER (mm)', 'ROUT (mm)', 'CORR (-)', 'TB (h)']
"""Balises du fichier de paramètres de GRP Temps-Réel"""
