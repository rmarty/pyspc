#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - Site Web Vigicrues
"""

CODETYPES = {
    'vigicrues-1': {
        'dept': '4',
        'spc': '5',
        'station': '7',
        'troncon': '8',
    }
}
"""Codification des types d'entité pour Vigicrues"""

DATATYPES = ['vigicrues_fcst', 'vigicrues_loc', 'vigicrues_obs',
             'vigicrues_reach', 'vigicrues_sandre']
"""Types de données de Vigicrues"""

RATIO_UNITS = {
    'H': 1.0,  # Convertir les données : m
    'Q': 1.0,  # Conserver les données : m3/s
}
"""Ratio de conversion d'unités"""

TRENDS = {
    'ResMinPrev': '10', 'ResMoyPrev': '50', 'ResMaxPrev': '90',
}
"""Dictionnaire de correspondance des tendances de prévision"""

VARNAMES = ['H', 'Q']
"""Grandeurs Vigicrues"""
