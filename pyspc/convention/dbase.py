#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - DataBase
"""
from pyspc.convention.prevision import (
    P14_DATATYPES, P17_DATATYPES, P19_DATATYPES,
    P14_DATANAMES, P17_DATANAMES, P19_DATANAMES)
from pyspc.convention.refspc import REFSPC_DATATYPES, REFSPC_DATANAMES
from pyspc.convention.sacha import (
    DATATYPES as SACHA_DATATYPES, DATANAMES as SACHA_DATANAMES)

MDB_DATATYPES = sorted([*P14_DATATYPES, *P17_DATATYPES, *P19_DATATYPES,
                        *SACHA_DATATYPES])
"""Types de donnée issues des bases .mdb"""

SQLITE_DATATYPES = sorted([*REFSPC_DATATYPES, *P19_DATATYPES])
"""Types de donnée issues des bases .sqlite"""

DATATYPES = sorted(list(set([*MDB_DATATYPES, *SQLITE_DATATYPES])))
"""Types de donnée issues des bases"""


MDB_DATANAMES = {**P14_DATANAMES, **P17_DATANAMES, **P19_DATANAMES,
                 **SACHA_DATANAMES}
"""Description des types de donnée issues des bases .mdb"""

SQLITE_DATANAMES = {**REFSPC_DATANAMES, **P19_DATANAMES}
"""Description des type de donnée issues des bases .sqlite"""

DATANAMES = {**MDB_DATANAMES, **SQLITE_DATANAMES}
"""Description des type de donnée issues des bases"""
