#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convention - GRP version 2018
"""

# -----------------------------------------------------------------------------
# --- COMMUN
# -----------------------------------------------------------------------------
DATATYPES = [
    "grp18_cal_data", "grp18_cal_event", "grp18_cal_fcst",
    "grp18_rt_data", "grp18_rt_metscen",
    "grp18_rt_obs", "grp18_rt_obs_diff",
    "grp18_rt_sim", "grp18_rt_sim_diff",
    "grp18_rt_fcst", "grp18_rt_fcst_diff",
    "grp18_rt_archive",
    "grp18_rt_intern", "grp18_rt_intern_diff",
]
"""Types de données de GRP version 2018"""

DATANAMES = {
    "grp18_cal_data": "Fichier de données observées (BDD_var / *)",
    "grp18_cal_event": "Fichier événement",
    "grp18_cal_fcst": "Fichier de prévision (Resultats / *_PP_P0P0.TXT)",
    "grp18_rt_data": "Fichier de données observées (Entrées / *.txt)",
    "grp18_rt_metscen":
        "Fichier de scénarios météorologiques (Entrées / Scen*)",
    "grp18_rt_obs": "Fichier de sortie, observation temps-réel",
    "grp18_rt_obs_diff":  "Fichier de sortie, observation temps-différé",
    "grp18_rt_sim":
        "Fichier de sortie, prévision sans assimilation temps-réel",
    "grp18_rt_sim_diff":
        "Fichier de sortie, prévision sans assimilation temps-différé",
    "grp18_rt_fcst":
        "Fichier de sortie, prévision avec assimilation temps-réel",
    "grp18_rt_fcst_diff":
        "Fichier de sortie, prévision avec assimilation temps-différé",
    "grp18_rt_archive": "Fichier de données archivées (BD_var / SITE / *.DAT)",
    "grp18_rt_intern": "Fichier de données internes temps-réel (PQE*.DAT)",
    "grp18_rt_intern_diff":
        "Fichier de données internes temps-différé (PQE*.DAT)",
}
"""Description des données de GRP version 2018"""

# -----------------------------------------------------------------------------
# --- CAL - BASSIN
# -----------------------------------------------------------------------------
CAL_BASIN_LINESEP = "!---------------------------------------------"\
                    "-----------------------------------------------"\
                    "------------------!\n"
CAL_BASIN_HEADERS = {
    'E': "! AAAAAAAA NNNNNN.NN ! Format ecriture code et "
         "ponderation station ETP (1 ligne par station, "
         "commencant par E) !\n",
    'P': "! AAAAAAAA NNNNNN.NN nnJnnHnnM ! Format ecriture code, pond et pdt "
         "pluvio (1 ligne par poste, commencant par P)!\n",
    'T': "! AAAAAAAA NNNNNN.NN NNNN.NN ! Format ecriture code et ponderation "
         "temperature (1 ligne par station, commencant par T) !\n",
    'N': "! NNNNNN.NN ! Facteur correctif de la sous-captation de la neige "
         "                                              !\n",
    'L': "! nnJnnHnnM ! Durée entre 2 données à partir de laquelle on "
         "considère une période comme lacunaire (1 ligne commencant par L) !\n"
}

# -----------------------------------------------------------------------------
# --- CAL - CONFIG
# -----------------------------------------------------------------------------
CAL_CONFIG_HEADER = """######################################################  README UTILISATEURS  ####################################################################################
# Code : Identifiant du bassin                                                                                                                                  #
# PDT : Pas de temps choisi - format : xxJxxHxxM (exemple : 00J01H00M pour le pas de temps 1h)                                                                  #
# Nom Station : Libellé bassin                                                                                                                                  #
# S : Sans module neige - A : Avec Cemaneige                                                                                                                    #
# T : Correction de Tangara - R : Correction des sorties par réseau de neurones                                                                                 #
# Superf. : Superficie du bassin en km2                                                                                                                         #
# RT : Référentiel Temporel (TU, HL ou HH)                                                                                                                      #
# HOR : Horizon de calage - format : xxJxxHxxM                                                                                                                  #
# Seuil_C : Seuil de calage en m3/s                                                                                                                             #
# Date début, Date fin : Dates de début et de fin sur lesquelles on veut travailler                                                                             #
# Seuil_V : Seuil utilisé pour l'analyse des résultats, en m3/s                                                                                                 #
# NJ : Nombre de jours présents sur la fenêtre temporelle de chaque évènement pour le tracé des hydrogrammes prévus                                             #
# HC : Horizon des cheveux que l'on veut voir apparaitre sur le tracé des hydrogrammes prévus - format : xxJxxHxxM                                              #
#      Doit être compris dans la liste des horizons disponibles. Indiquer '00J00H00M' pour ne pas les tracer.                                                   #
# EC : Ecart en pas de temps entre les cheveux des prévisions                                                                                                   #
# Ecart : Ecart (en m3/s) entre prévision et observation au-delà duquel on veut connaitre les pas de temps concernés                                            #
# Inc : 0 ou 1 pour activer ou désactiver la génération d'abaques d'incertitudes                                                                                #
# N : Chiffre entre 0 et 9   ;   S : caractère alphanumérique                                                                                                   #
#################################################################################################################################################################
!----------------------------------------              CALAGE DU MODELE              ---------------------------------------!##!      ANALYSE DES RESULTATS     !
!--------!---------!------------------------------!-!-!-!-!---------!--!---------!--------!----------------!----------------!##!--------!--!---------!--!-----!I!
!--------!---------!------------------------------!S!S!A!A!---------!--!---------!--------!----------------!----------------!##!--------!--!---------!--!-----!n!
!    Code!PDT      !Nom Station                   !T!R!T!R!  Superf.!RT!HOR      ! Seuil_C!Date debut      !Date fin        !##! Seuil_V!NJ!HC       !EC!Ecart!c!
!--------!---------!------------------------------!-!-!-!-!---------!--!---------!--------!----------------!----------------!##!--------!--!---------!--!-----!-!
!SSSSSSSS!NNJNNHNNM!SSSSSSSSSSSSSSSSSSSSSSSSSSSSSS!N!N!N!N!NNNNNN.NN!SS!NNJNNHNNM!NNNNN.NN!NN/NN/NNNN NN:NN!NN/NN/NNNN NN:NN!##!NNNNN.NN!NN!NNJNNHNNM!NN!NNNNN!N!
!--------!---------!------------------------------!-!-!-!-!---------!--!---------!--------!----------------!----------------!##!--------!--!---------!--!-----!-!
"""
CAL_CONFIG_FORMAT = '!{CODE:8s}!{PDT:9s}!{NOM:<30s}!{ST:1d}!{SR:1d}!{AT:1d}!'\
        '{AR:1d}!{SURFACE:9.2f}!{RT:2s}!{HOR:9s}!{SC:8.2f}!{DEB:16s}!'\
        '{FIN:16s}!##!{SV:8.2f}!{NJ:2d}!{HC:9s}!{EC:2d}!{ECART:5d}!{INC:1d}!\n'
CAL_CONFIG_NAMES = ['CODE', 'PDT', 'NOM', 'ST', 'SR', 'AT', 'AR',
                    'SURFACE', 'RT', 'HOR', 'SC', 'DEB', 'FIN',
                    'SV', 'NJ', 'HC', 'EC', 'ECART', 'INC']
CAL_CONFIG_CONVERTERS = [str, str, str, int, int, int, int,
                         float, str, str, float, str, str,
                         float, int, str, int, int, int]
CAL_CONFIG_CALKEYS = ['CODE', 'PDT', 'NOM', 'ST', 'SR', 'AT', 'AR',
                      'SURFACE', 'RT', 'HOR', 'SC', 'DEB', 'FIN']

# -----------------------------------------------------------------------------
# --- CAL - DATA
# -----------------------------------------------------------------------------
CAL_DATA_HEADERS = {
    'Q': 'Q(m3/s)',
    'P': 'P(mm)',
    'T': 'T(°C)',
    'E': 'ETP(mm)',
}
"""Dictionnaire des entêtes de fichier"""
CAL_DATA_VARNAMES = list(CAL_DATA_HEADERS.keys())
"""Liste des variables autorisées"""

# -----------------------------------------------------------------------------
# --- CAL - EVENT
# -----------------------------------------------------------------------------
CAL_EVENT_HEADERS = {
    'index': 'Date',
    'Q': 'Debit(m3/s)',
    'P': 'Pluie(mm/h)',
}
"""Dictionnaire des entêtes de fichier"""

# -----------------------------------------------------------------------------
# --- CAL - FCST
# -----------------------------------------------------------------------------
CAL_FCST_DATEFORMAT = '%Y%m%d%H'
"""Format des dates dans les fichiers de prévisions de GRP 2018"""

# -----------------------------------------------------------------------------
# --- CAL - VERIF
# -----------------------------------------------------------------------------
CAL_VERIF_DTYPES = {
    'TESTS': 'cal',
    'CALAG': 'rtime'
}
CAL_VERIF_NAMES = {'TESTS': 'Fiche en calage-contrôle',
                   'CALAG': 'Fiche en calage complet'}

# -----------------------------------------------------------------------------
# --- TR - ARCHIVE
# -----------------------------------------------------------------------------
RT_ARCHIVE_DATEFORMAT = '%Y%m%d%H%M'
RT_ARCHIVE_VARNAMES = ['PV', 'QV']
"""Variables associées aux fichiers de type Archive de GRP Temps-Réel"""

# -----------------------------------------------------------------------------
# --- TR - BASSIN
# -----------------------------------------------------------------------------
#RT_BASIN_LINESEP = "#--------------------------------------------------------"\
#    "-------------\n"
#RT_BASIN_HEADERS = {
#    'A': "#          FFFFFF.FF ! Format ecriture superficie du bassin  "
#         "(1 ligne commencant par S)\n",
#    'B': "# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA ! Format ecriture nom station "
#         "(1 ligne commencant par B)\n",
#    'E': "# AAAAAAAA FFFFFF.FF ! Format ecriture code et ponderation station "
#         "ETP (1 ligne par station, commencant par E)\n",
#    'F': "# AAAAAAAA FFFFFF.FF  AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA "
#         "! Format ecriture code, nom et ponderation "
#         "pluvio (1 ligne par poste, commencant par P)\n",
#    'G': "# FFFFF.FF           ! Format ecriture Qvig "
#         "(1 ligne commencant par G)\n",
##    'P': "",  # A laisser commenté car entête par 'F'
#    'Q': "# AAAAAAAA           ! Format ecriture code Hydro "
#         "(1 ligne commencant par Q)\n",
#    'S': "#          FFFFFF.FF ! Format ecriture superficie du bassin  "
#         "(1 ligne commencant par S)\n",
#    'T': "#                 II ! Format ecriture decalage temporel des series "
#         "de pluie et debit par rapport au UTC (2 lignes commencant par T)\n",
#}
#RT_BASIN_FORMATS = {
#    'A': "A          {0:9.2f} ! Altitude mediane du bassin (m)\n",
#    'B': "B {0:<30s} ! Nom station\n",
#    'E': "E {0:8s} {1:9.2f} ! Code et ponderation station ETP {2:>4d}\n",
#    'F': "F          {0:9.2f}  Pluie annuelle moyenne (mm/an)  \n",
#    'G': "G {0:8.2f}           ! Seuil de vigilance (m3/s)\n",
#    'P': "P {0:8s} {1:9.2f}  {2:30s}           ! Code, ponderation et "
#         "nom poste pluvio {3:>3d}\n",
#    'Q': "Q {0:8s}           ! Code Hydro\n",
#    'S': "S          {0:9.2f} ! Superficie du bassin (km2)\n",
#    'T': "T                 {0:>2d} ! Decalage temporel de la serie de {1} "
#         "par rapport au UTC\n",
#}

# -----------------------------------------------------------------------------
# --- TR - CONFIG
# -----------------------------------------------------------------------------
RT_CONFIG_KEYS = [
    'MODFON', 'INSTPR', 'OBSCHE', 'OBSTYP',
    'SCECHE', 'SCETYP', 'SCEREF', 'SCENBR',
    'PRVCHE', 'PRVTYP', 'PRVREF', 'PRVUNI',
    'CODMOD', 'CONFIR', 'CODMPP', 'CHEMIN',
    'SIMULA',  # ajout 3.1
    'BDDTRCHE',  # ajout v2018
]
"""Balises du fichier de configuration de GRP Temps-Réel"""
#    'HORMAX',  # retrait v2018
#    'CHOIXH',  # retrait v2018

# -----------------------------------------------------------------------------
# --- TR - DATA
# -----------------------------------------------------------------------------
RT_DATA_OBSFILEPREFIX = {
    'H': 'Hauteur',
    'P': 'Pluie',
    'Q': 'Debit',
    'T': 'Temp',
}
"""Préfixe des noms de base des fichiers d'entrée de GRP Temps-Réel"""
RT_DATA_LINEPREFIX = {
    'H': 'HAU',
    'P': 'PLU',
    'Q': 'CQT',
    'T': 'TEM',
}
"""Préfixe des lignes de données des fichiers d'entrée de GRP Temps-Réel"""
RT_DATA_SCENFILEPREFIX = {
    'P': 'Scen_',
    'T': 'ScenT_',
}
"""Préfixe des noms de fichier des scénarios météo"""
RT_DATA_SCENFORMAT = {
    'P': "Scen_{0}_Plu{1}_{2}.txt",
    'T': "ScenT_{0}_Tem{1}_{2}.txt",
}
"""Nom de base d'un fichier de scénario météo"""
RT_DATA_VARNAMES = ['H', 'P', 'Q', 'T']
"""Liste des variables autorisées"""
RT_DATA_SCENVARNAMES = ['P', 'T']
"""Liste des variables autorisées pour les scénarios météo"""

# -----------------------------------------------------------------------------
# --- TR - FCST
# -----------------------------------------------------------------------------
RT_FCST_DTYPES = ["obs", "obs_diff", "sim", "sim_diff", "fcst", "fcst_diff"]
"""Types de fichiers de prévision de GRP Temps-Réel"""
RT_FCST_FILEPREFIX = {
    'obs': 'GRP_Obs',
    'obs_diff': 'GRP_D_Obs',
    'sim': 'GRP_Simu_',
    'sim_diff': 'GRP_D_Simu_',
    'fcst': 'GRP_Prev_',
    'fcst_diff': 'GRP_D_Prev_',
}
"""Préfixe des noms de fichier de prévision de GRP Temps-Réel"""
RT_FCST_LINEPREFIX = {
    'obs': 'OBS',
    'sim': 'SIM',
    'fcst': 'PRV',
}
"""Préfixe des lignes de données des fichiers de prévision de GRP Temps-Réel"""
RT_FCST_VARNAMES = {
    'DEBIT': 'Q',
    'PLUIE': 'P',
    'Temperature': 'T',
}
"""Association entre les colonnes et les grandeurs de GRP Temps-Réel"""

# -----------------------------------------------------------------------------
# --- TR - INTERN
# -----------------------------------------------------------------------------
RT_INTERN_BASENAMES = {
    'intern': 'PQE_1A.DAT',
    'intern_diff': 'PQE_1A_D.DAT',
}
"""Noms de base des fichiers de type Intern de GRP Temps-Réel"""
RT_INTERN_DTYPES = {
    'intern': True,
    'intern_diff': False,
}
"""Types de fichier Intern de GRP Temps-Réel"""

# -----------------------------------------------------------------------------
# --- TR - PARAM
# -----------------------------------------------------------------------------
RT_PARAM_LIST = ['INTER (mm)', 'ROUT (mm)', 'CORR (-)', 'TB (h)']
"""Balises du fichier de paramètres de GRP Temps-Réel"""
