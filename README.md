pySPC ![icon](https://bitbucket.org/rmarty/pyspc/src/master/resources/icons/icone_pyspc.png)
===============================================================================

Description
-------------------------------------------------------------------------------

Le paquet **pySPC** est destiné à traiter, convertir, afficher, analyser les séries de données pluviométriques, hydrométriques, statistiques, de modélisation à partir de bases de données, de fichier de type *csv* similaire au format des fichiers GRP, à savoir *AAAAMM[JJ[HH[mm]]];Valeur*. Il permet notamment de:

### Hydro-météorologie

*  récupérer les données hydrométéorologiques (*[lames d'eau radar](http://wikhydro.developpement-durable.gouv.fr/index.php/B.04_-_Estimation_d%27une_pluie_de_bassin_par_observation_RADAR)*, *[Bulletins Précipitations](http://wikhydro.developpement-durable.gouv.fr/index.php/B.13_-_Pr%C3%A9visions_num%C3%A9riques_de_pr%C3%A9cipitation#.C3.80_l.27.C3.A9chelle_des_SPC.C2.A0:_les_bulletins_de_pr.C3.A9vision_.28AP.2FBP.29)*, *[prévisions Symposium](https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=135&id_rubrique=50)*) issues de la base de données **[LAMEDO](https://gitlab.com/vigicrues/lamedo/bdimage3)** du Schapi ;
*  convertir les données de **Météo-France** (*[publithèque](https://publitheque.meteo.fr)*, *[Bulletins Précipitations](http://wikhydro.developpement-durable.gouv.fr/index.php/B.13_-_Pr%C3%A9visions_num%C3%A9riques_de_pr%C3%A9cipitation#.C3.80_l.27.C3.A9chelle_des_SPC.C2.A0:_les_bulletins_de_pr.C3.A9vision_.28AP.2FBP.29)*, *[prévisions Symposium](https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=135&id_rubrique=50)*) vers/depuis le format *csv* de GRP ;
*  récupérer [les bulletins climatologiques](https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=129&id_rubrique=52) et [les fiches stations](https://donneespubliques.meteofrance.fr/?fond=contenu&id_contenu=37) de **Météo-France** ;
*  tracer les figures des *[Bulletins Précipitations](http://wikhydro.developpement-durable.gouv.fr/index.php/B.13_-_Pr%C3%A9visions_num%C3%A9riques_de_pr%C3%A9cipitation#.C3.80_l.27.C3.A9chelle_des_SPC.C2.A0:_les_bulletins_de_pr.C3.A9vision_.28AP.2FBP.29)* et des observations radar/sol ;

### Observation hydrologique

*  appliquer des courbes de tarage depuis **Bareme** ou la **[PHyC](https://www.hydroportail.developpement-durable.gouv.fr/)** ;
*  lire des bases de données Access: **SACHA**, **Bareme** ;
*  convertir les données issues des archives **[CRISTAL](http://www.centre.developpement-durable.gouv.fr/le-reseau-cristal-a86.html)** au format de la **[Banque HYDRO-2](https://www.ecologique-solidaire.gouv.fr/banque-hydro)** (*H-TEMPS*, *QTVAR*) ;
*  créer les procédures d'export de la **[Banque HYDRO-2](http://www.hydro.eaufrance.fr/)** (*H-TEMPS*, *QTVAR*, *QTFIX*, *TOUSMOIS*, *DEBCLA*, *CRUCAL*)
*  convertir les données de la **[Banque HYDRO-2](http://www.hydro.eaufrance.fr/)** (*H-TEMPS*, *QTVAR*, *QTFIX*) vers/depuis le format *csv* de GRP ;
*  récupérer les statistiques des exports de la **[Banque HYDRO-2](http://www.hydro.eaufrance.fr/)** (*DEBCLA*, *CRUCAL*)
*  convertir les **observations** issues de la **[PHyC](https://www.hydroportail.developpement-durable.gouv.fr/)** du format **[XML-Sandre](http://www.sandre.eaufrance.fr/notice-doc/donn%C3%A9es-hydrom%C3%A9triques-1)** au format *csv* de GRP ;
*  récupérer les *informations hydrométriques* (courbe de tarage, courbe de correction, jaugeage) issues de la **[PHyC](https://www.hydroportail.developpement-durable.gouv.fr/)** du format **[XML-Sandre](http://www.sandre.eaufrance.fr/notice-doc/donn%C3%A9es-hydrom%C3%A9triques-1)** ;
*  convertir les données exportées depuis **Graphyte** vers les formats *csv* de GRP et **[Banque HYDRO-2](https://www.ecologique-solidaire.gouv.fr/banque-hydro)**;
*  convertir les données selon la cote de déversement (Z0) et le barème d'un **[Réservoir](https://fr.wikipedia.org/wiki/Barrage)**;

### Traitement de données hydrologiques

*  tracer des figures (hydrogramme, limnigrammme, hyétogramme, événement, réservoir) ;
*  réaliser différentes opérations mathématiques sur des séries de données (addition, soustraction, régression linéaire, décalage dans le temps, synthèse statistique, quantiles, désagrégation, agrégation...) ;
*  extraire des événements à partir d'une série, selon l'algorithme **[scipy.signal.find_peaks](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.find_peaks.html)** ou plus simplement par un seuil ;
*  convertir une collection de séries aux/depuis plusieurs formats classiques: **[csv](https://fr.wikipedia.org/wiki/Comma-separated_values)**, **[xls](https://fr.wikipedia.org/wiki/XLS)** ou **[xlsx](https://fr.wikipedia.org/wiki/XLSX)**;
*  récupérer [**les fiches des bassins versants**](https://webgr.inrae.fr/activites/base-de-donnees/) et [**les fiches SHYREG**](https://shyreg.inrae.fr/) de l'**Inrae** (*Irstea*)  (*shyreg*: nécessite pdfminer dans sa version python3);
*  réaliser une analyse des pointes de crue entre 1 station amont et 1 station aval ;

### Modélisation hydrologique

*  préparer les données pour **[GRP](https://webgr.inrae.fr/modeles/modele-de-prevision-grp/)** en mode Calage et en mode Temps-Réel ;
*  caler en *masse* des modèles **GRP-v2016r, GRP-v2018, GRP-v2020** ;
*  récupérer les scores de performance des modèles GRP calés depuis les fiches de performance (nécessite **pdfminer** dans sa version [python3](https://pypi.python.org/pypi/pdfminer3k)) ;
*  tracer sous forme graphique (radar) les valeurs des scores POD, FAR et CSI selon les seuils de calage, les horizons de prévisions et les seuils de vigilance
*  rejouer en *masse* des événements passés par **GRP-v2016r, GRP-v2018, GRP-v2020** ;
*  lire et traiter les **fichiers d'observations** (.mqo, .mqi, .mgr) de la plate-forme de modélisation hydrologique **[PLATHYNES](https://gitlab.com/vigicrues/plathynes/plathynes)**
*  lire et traiter les **simulations** de la plate-forme de modélisation hydrologique **[PLATHYNES](https://gitlab.com/vigicrues/plathynes/plathynes)**
*  lire et traiter les **exports** de la plate-forme de modélisation hydrologique **[PLATHYNES](https://gitlab.com/vigicrues/plathynes/plathynes)**
*  lire et traiter les **fichiers de configuration** (projet, événement...) de la plate-forme de modélisation hydrologique **[PLATHYNES](https://gitlab.com/vigicrues/plathynes/plathynes)**
*  créer les **fichiers d'observations** (.mqo, .mqi, .mgr) de la plate-forme de modélisation hydrologique **[PLATHYNES](https://gitlab.com/vigicrues/plathynes/plathynes)** à partir de la **[PHyC](https://www.hydroportail.developpement-durable.gouv.fr/)** au format **[XML-Sandre](http://www.sandre.eaufrance.fr/notice-doc/donn%C3%A9es-hydrom%C3%A9triques-1)** ou à partir de bases de données Access (**SACHA**)
*  convertir les données *prv* des applications **SCORES** et **[OTAMIN-Temps Réel](https://webgr.inrae.fr/modeles/otamin/)** vers/depuis le format *csv* de GRP ;
*  extraire et tracer les scores calculés par l'application **SCORES**

### Prévision et vigilance hydrologique

*  extraire les prévisions brutes, validées et expértisées depuis la base **Prévision** du SPC LCI dans sa version 2014-2016 ; 
*  extraire les prévisions brutes, validées/expértisées et diffusées depuis la base **Prévision** du SPC LCI dans sa version 2017-2019 ; 
*  extraire les prévisions brutes, validées/expértisées et diffusées depuis la base **Prévision** du SPC LCI dans sa version 2019-.... ; 
*  convertir les prévisions brutes, validées et/ou expértisées issues de la **[PHyC](https://www.hydroportail.developpement-durable.gouv.fr/)** ou de la **[POM](https://www.ecologique-solidaire.gouv.fr/sites/default/files/Schapi_BILAN_2019_SR-WEB-planche.pdf)** du format **[XML-Sandre](http://www.sandre.eaufrance.fr/notice-doc/donn%C3%A9es-hydrom%C3%A9triques-1)** au format *csv* de GRP ;
*  récupérer et convertir les données relatives aux tronçons de vigilance et aux observations de **[Vigicrues](https://www.vigicrues.gouv.fr/)** ;
*  préparer les données d'observation, télécharger et synthétiser les prévisions du projet **[PREMHYCE](https://webgr.inrae.fr/projets/projets-en-cours/onema-premhyce/)**


Installation
-------------------------------------------------------------------------------

Se reporter à la documentation pour l'installation. Celle-ci est désormais disponible sur **[readthedocs.io](https://pyspc-doc.readthedocs.io/fr/latest/install.html)**

Tests des utilitaires
-------------------------------------------------------------------------------

Les cas d'application mentionnés comme exemples dans les pages de la documentation sont accessibles dans le répertoire test.

License
-------------------------------------------------------------------------------

Se reporter au fichier COPYING.txt.

Contact
-------------------------------------------------------------------------------

Renaud Marty <renaud.marty@developpement-durable.gouv.fr>

[![version](https://img.shields.io/badge/version-3.0.0-blue)](https://bitbucket.org/rmarty/pyspc/get/v3.0.0.zip)
[![python](https://img.shields.io/badge/python-3.7-blue)](https://www.python.org/)
[![issues](https://img.shields.io/bitbucket/issues/rmarty/pyspc)](https://bitbucket.org/rmarty/pyspc/issues?status=new&status=open)
[![readthedocs](https://readthedocs.org/projects/pyspc-doc/badge/?version=latest)](https://pyspc-doc.readthedocs.io/fr/latest)
[![coverage](https://img.shields.io/badge/coverage-82%25-green)](https://coverage.readthedocs.io)
[![codefactor](https://www.codefactor.io/repository/bitbucket/rmarty/pyspc/badge)](https://www.codefactor.io/repository/bitbucket/rmarty/pyspc)
[![License](https://img.shields.io/badge/license-GPLv3-blue)](https://www.gnu.org/licenses/quick-guide-gplv3.fr.html)


