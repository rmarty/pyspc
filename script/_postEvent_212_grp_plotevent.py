#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_212_grp_plotevent:

.. role:: blue

.. role:: boldblue

Tracer la simulation événementielle et les prévisions (_postEvent_212_grp_plotevent.py)
---------------------------------------------------------------------------------------

Description
+++++++++++

Tracer la simulation événementielle et les prévisions

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration de GRP

:boldblue:`GRP_DIRNAME` : :blue:`Répertoire de GRP`

:boldblue:`GRP_SCEN` : :blue:`Code scénario (seulement prv)`

.. rubric:: Configuration des fichiers csv

:boldblue:`CSV_DIRNAME` : :blue:`Répertoire des données au format csv`

:boldblue:`SPC_VARNAME` : :blue:`Variable`

.. rubric:: Configuration des figures

:boldblue:`INCLUDE_FORECAST` : :blue:`Inclure les prévisions ? (T/F)`

:boldblue:`MAXLTIME_FORECAST` : :blue:`Echéance maximal. Si None, reprend la valeur EVENT`

:boldblue:`COLORS_FORECAST` : :blue:`Liste de couleurs pour les prévisions`

"""

# *****************************************************************************
#               PARAMETRES
# *****************************************************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt, timedelta as td
import glob
import itertools
import matplotlib.pyplot as mplt
import os.path
from pyspc import Config, Series, read_GRP16, read_PyspcFile
from pyspc.plotting.config import Config as PlotConfig

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# ------------------------------------
# PARAMETRES : CONFIGURATION DE GRP
#   GRP_DIRNAME         : Répertoire de GRP
#   GRP_SCEN            : Code scénario (seulement prv)
# ------------------------------------
GRP_DIRNAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\20211012_GRP\Temps_Reel'
GRP_SCEN = '2001'
# -----------------------------------------------------------------------------
# PARAMETRES : ARCHIVAGE CSV
#   CSV_DIRNAME         : Répertoire des données au format csv
#   SPC_VARNAME         : Variable
# -----------------------------------------------------------------------------
CSV_DIRNAME = os.path.join('fcst', 'grp')
SPC_VARNAME = 'QH'
# ------------------------------------
# PARAMETRES : CONFIGURATION DE LA FIGURE
#   INCLUDE_FORECAST    : Inclure les prévisions ? (T/F)
#   MAXLTIME_FORECAST   : Echéance maximal. Si None, reprend la valeur EVENT
#   COLORS_FORECAST     : Liste de couleurs pour les prévisions
# ------------------------------------
INCLUDE_FORECAST = True
MAXLTIME_FORECAST = 24
COLORS_FORECAST = [
    mplt.cm.tab20c(4), mplt.cm.tab20c(5), mplt.cm.tab20c(6), mplt.cm.tab20c(7)]
# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
COLORS_FORECAST = itertools.cycle(COLORS_FORECAST)
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
})
if MAXLTIME_FORECAST is None:
    MAXLTIME_FORECAST = EVENT['event']['maxltime']
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
# ------------------------------------
# --- LECTURE DES DONNEES
# ------------------------------------
fcst_filenames = glob.glob(
    os.path.join(
        HOME, CSV_DIRNAME,
        '*', '*', '*',  # année / mois / jour
        'grp16rtfcstdiff_*_{}.txt'.format(SPC_VARNAME)
    )
)
FCST_SERIES = Series(datatype='fcst', name='grp16')
if INCLUDE_FORECAST:
    for f in fcst_filenames:
        print(os.path.relpath(f, HOME))
        tmp_series = read_PyspcFile(filename=f)
        FCST_SERIES.extend(tmp_series)
#        break

datatype = "intern_diff"
for loc in LOCS_HYDRO:
    filename = os.path.join(GRP_DIRNAME, 'BD_Bassins', loc, 'PQE_1A_D.DAT')
    if not os.path.exists(filename):
        continue
    SERIES = read_GRP16(filename=filename, datatype='grp16_rt_intern_diff')
    SERIES.between_dates(
        first_dtime=EVENT['event']['firstdt'],
        last_dtime=EVENT['event']['lastdt']
        + td(hours=EVENT['event']['maxltime']),
        inplace=True
    )
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION IMAGE
# ------------------------------------
    CFGIMG_FILENAME = os.path.join(
        HOME, 'config', os.path.basename(__file__).replace('.py', '.txt'))
    CFGIMG = PlotConfig(filename=CFGIMG_FILENAME)
    CFGIMG.load()
# ------------------------------------
# --- MAJ DE LA CONFIGURATION IMAGE
# ------------------------------------
    CFGIMG['figure']['dirname'] = os.path.join(HOME, 'fig', 'grp')
    if not os.path.exists(CFGIMG['figure']['dirname']):
        os.makedirs(CFGIMG['figure']['dirname'])
    if INCLUDE_FORECAST:
        CFGIMG['figure']['filename'] = '{}_fcst-{}'.format(
            loc, MAXLTIME_FORECAST)
    else:
        CFGIMG['figure']['filename'] = loc
    CFGIMG['figure']['title'] = '{}\n{} ({}, {})'.format(
        EVENT['event']['title'], LOCS_HYDRO[loc]['name'],
        LOCS_HYDRO[loc]['river'], loc)
    src_s = [s for s in CFGIMG if s.startswith('SITE')]
    for s in src_s:
        dst_s = s.replace('SITE', loc)
        CFGIMG.setdefault(dst_s, {})
        for o in CFGIMG[s]:
            try:
                CFGIMG[dst_s][o] = CFGIMG[s][o].replace('SITE', loc)\
                    .replace('NAME', LOCS_HYDRO[loc]['name'])
            except AttributeError:
                CFGIMG[dst_s][o] = CFGIMG[s][o]
# ------------------------------------
# --- CONFIGURATION DES COURBES DES PREVISIONS
# ------------------------------------
    if INCLUDE_FORECAST:
        kf = 0
        for k, s in FCST_SERIES.items():
            if (k[0] != loc or k[1] != SPC_VARNAME
                    or not isinstance(k[2], tuple)):
                continue
            runtime = k[2][0]
            if (runtime < EVENT['event']['firstdt']
                    or runtime > EVENT['event']['lastdt']):
                continue
            s.between_dates(first_dtime=runtime,
                            last_dtime=runtime + td(hours=MAXLTIME_FORECAST),
                            inplace=True)
            SERIES.add(s)
            cfg_key = '{}_{}'.format(s.code, s.spc_varname)
            CFGIMG.setdefault(cfg_key, {})
            CFGIMG[cfg_key]['code'] = s.code
            CFGIMG[cfg_key]['varname'] = SPC_VARNAME
            if kf == 0:
                CFGIMG[cfg_key]['color'] = \
                    CFGIMG['FCST_{}'.format(SPC_VARNAME)]['color']
                CFGIMG[cfg_key]['label'] = \
                    CFGIMG['FCST_{}'.format(SPC_VARNAME)]['label']
            else:
                CFGIMG[cfg_key]['label'] = ''
                CFGIMG[cfg_key]['color'] = next(COLORS_FORECAST)
            for o in CFGIMG['FCST_{}'.format(SPC_VARNAME)]:
                CFGIMG[cfg_key].setdefault(
                    o, CFGIMG['FCST_{}'.format(SPC_VARNAME)][o])
            kf += 1
        # Focer les séries sim et obs à être en avant-plan
        # Forcer les séries prv à être en arrière-plan
        ss = [s for s in SERIES if len(s[0].split('_')) <= 2]
        for s in ss:
            SERIES.move_to_end(s, last=True)
# ------------------------------------
# --- MAJ DE LA CONFIGURATION IMAGE AVEC VALEURS PAR DEFAUT
# ------------------------------------
    for s in CFGIMG:
        if s in ['figure', 'defaut']:
            continue
        for o in CFGIMG['defaut']:
            CFGIMG[s].setdefault(o, CFGIMG['defaut'][o])
# ------------------------------------
# --- CREATION DE L'IMAGE
# ------------------------------------
    filename = SERIES.plot_series(plottype='event', config=CFGIMG)
    print('Création de la figure {}'
          ''.format(os.path.relpath(filename, start=HOME)))

#    break
