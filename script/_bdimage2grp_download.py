#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_bdimage_download:

.. role:: blue

.. role:: boldblue

Télécharger les données BdImages (_bdimage_download.py)
-------------------------------------------------------

Description
+++++++++++

Télécharger les données (valeurs, statistique) d'images BdImages

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES IMAGES

:boldblue:`BDI_TYPE` : :blue:`Type d'image`

:boldblue:`BDI_STYPE` : :blue:`Sous-type d'image`

:boldblue:`BDI_BAND` : :blue:`Bande de l'image`

:boldblue:`VARNAME` : :blue:`Variable`

:boldblue:`ZONES` : :blue:`Zones`

:boldblue:`STATS` : :blue:`'standard', 'complete' ou <None>`

:boldblue:`PRECISION` : :blue:`'standard', 'forte' ou <None>`

.. rubric:: CONFIGURATION DES DONNEES LOCALES

:boldblue:`OUTPUT_DIRNAME` : :blue:`Répertoire de stockage`

"""

# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt, timedelta as td
import os
import os.path
import subprocess
import time

from pyspc import Parameter

# ------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   OUTPUT_DIRNAME  : Répertoire de stockage
#   BDI_TYPE        : Type d'image
#   BDI_STYPE       : Sous-type d'image
#   BDI_BAND        : Bande de l'image
#   VARNAME         : Variable
#   ZONES           : Zones
#   STATS           : 'standard', 'complete' ou <None>
#   PRECISION       : 'standard', 'forte' ou <None>
# ------------------------------------
OUTPUT_DIRNAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20\__CIC_ANALOGUES\DATA\20221202_complemente_LE_LoireAllier\xml'
BDI_TYPE = 'sim'
BDI_STYPE = 'hu'
BDI_BAND = 'brut'
VARNAME = 'HU2J'  # 'PH'
ZONES = 'LO27301'
STATS = 'standard'
PRECISION = None

# ------------------------------------
# PARAMETRES : CONFIGURATION DE LA PERIODE DE CALCUL
#   RUN_DT   : Instant de prévision (SYMPO)
#   FIRST_DT : Premier pas de temps à considérer
#   LAST_DT  : Dernier pas de temps à considérer
#   DELTA_DT : Intervalle entre 2 requêtes
# ------------------------------------
RUN_DT = dt.utcnow()
FIRST_DT = dt(2013, 1, 1)
LAST_DT = dt(2016, 21, 31)
DELTA_DT = td(days=1500)

# ------------------------------------
# DEPENDANCES
# ------------------------------------
PARAMETER = Parameter(varname=VARNAME)
STEP_DT = PARAMETER.timestep
LEN_DT = int((LAST_DT - FIRST_DT) / DELTA_DT) + 1
if not os.path.exists(OUTPUT_DIRNAME):
    os.makedirs(OUTPUT_DIRNAME)

# *****************************************
#               SCRIPT
# *****************************************
# ------------------------------------
# LECTURE DE LA LISTE DES ZONES
# ------------------------------------
# Période demandée dans la requête courante
# -----------------------------------------
print('IMAGE {} : {} / {} / {}'.format(VARNAME, BDI_TYPE, BDI_STYPE, BDI_BAND))
processLog = os.path.join(OUTPUT_DIRNAME, 'bdimage_download.log')
with open(processLog, 'w', encoding='utf-8', newline="\n") as flog:
    for kt in range(LEN_DT):
        t0 = FIRST_DT + kt * DELTA_DT
        tN = FIRST_DT + (kt+1) * DELTA_DT - STEP_DT
        tN = min(tN, LAST_DT)
        print("-> Requête du {0} au {1}".format(
            t0.strftime("%Y-%m-%d %H:%M UTC"),
            tN.strftime("%Y-%m-%d %H:%M UTC")))
    # Construction de la commande <bdimage2xml>
    # -----------------------------------------
        processArgs = [
            'python',
            os.path.join(os.environ['PYSPC_BIN'], 'bdimage2xml.py'),
            '-c', os.path.join(os.environ['PYSPC_BIN'], 'bdimage2xml_RM.txt'),
            '-F', t0.strftime("%Y%m%d%H%M"),
            '-L', tN.strftime("%Y%m%d%H%M"),
            '-n', VARNAME,
            '-O', OUTPUT_DIRNAME,
            '-r', RUN_DT.strftime("%Y%m%d%H%M"),
            '-S', ZONES,
            '-t', BDI_TYPE, BDI_STYPE, BDI_BAND,
            '-v'
        ]
        if STATS is not None:
            processArgs.extend(['-U', 'stats', STATS])
        if PRECISION is not None:
            processArgs.extend(['-U', 'precision', PRECISION])
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=flog, stderr=subprocess.STDOUT)  # nosec
        processRun.wait()
        # je mets 5 sec. d'attente entre 2 processus
        time.sleep(5)
