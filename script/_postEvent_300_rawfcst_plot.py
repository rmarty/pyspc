#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_300_rawfcst_plot:

.. role:: blue

.. role:: boldblue

Tracer les prévisions opérationnelles par lieu/runtime (_postEvent_300_rawfcst_plot.py)
---------------------------------------------------------------------------------------

Description
+++++++++++

Tracer les prévisions opérationnelles par lieu/runtime

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des figures

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`TREND` : :blue:`Répertoire de MOHYS`

:boldblue:`FIRST_DTIME` : :blue:`Première date des runtimes. None si définie par event`

:boldblue:`LAST_DTIME` : :blue:`Dernière date des runtimes. None si définie par event`

"""

# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import collections
from datetime import datetime as dt, timedelta as td
import glob
import itertools
import os.path
from pyspc import Config, read_PyspcFile, read_Prevision19, Series
from pyspc.core.keyseries import tuple2str
from pyspc.data.prevision import Prevision19
from pyspc.plotting.colors import (
    COLORS as IMG_COLORS, TABCOLORS as IMG_TABCOLORS)
from pyspc.plotting.config import Config as PlotConfig

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# -----------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES FIGURES
#   SPC_VARNAME      : Variable
#   TREND            : Nom de la tendance
#   FIRST_DTIME      : Première date des runtimes. None si définie par event
#   LAST_DTIME       : Dernière date des runtimes. None si définie par event
# -----------------------------------------------------------------------------
SPC_VARNAME = 'QH'
TREND = ''
FIRST_DTIME = dt(2020, 6, 10)  # None
LAST_DTIME = dt(2020, 6, 16)  # None

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'subreaches'): lambda x: os.path.join(
        os.path.dirname(EVENT_FILENAME), x),
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
})
if FIRST_DTIME is None:
    FIRST_DTIME = EVENT['event']['firstdt']
if LAST_DTIME is None:
    LAST_DTIME = EVENT['event']['lastdt']
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
# ------------------------------------
# --- BASES DE PREVISION
# ------------------------------------
PRV_FILENAMES = glob.glob(os.path.join(HOME, 'fcst', 'oper', '*.mdb'))
# ------------------------------------
# --- CONFIGURATION DES MODELES
# ------------------------------------
MODELS = collections.OrderedDict()
for f in PRV_FILENAMES:
    reader = Prevision19(filename=f)
    _, m = reader.read_models()
    if isinstance(m, dict):
        MODELS.update(m)

TAG_PQ = [1, 2, 3, 8]
TAG_QQ = [5, 6, 7, 9]
LS_PQ = ['--', '-', '-.', ':']
LS_QQ = ['-', '--', '-.', ':']
ITER_COLORS = None
k1000o = -1
for k in MODELS:
    k1000 = int(k/1000)
    k1000m = k - k1000 * 1000
    clm = ('darkgray', '-', '')
    # Modèles pluie-débit avec plusieurs scénarios de pluie
    if k1000 in TAG_PQ:
        if k1000m == 1:
            ITER_COLORS = itertools.cycle(IMG_COLORS)
        clm = (next(ITER_COLORS), LS_PQ[TAG_PQ.index(k1000)], '')
    # Modèles débit-débit avec plusieurs post-traitements
    elif k1000 in TAG_QQ:
        if k1000o != k1000:
            ITER_COLORS = itertools.cycle(IMG_TABCOLORS[1:])
        clm = (next(ITER_COLORS), LS_QQ[TAG_QQ.index(k1000)], '')
    # Modèles correl
    elif k1000 == 4:
        clm = ('black', '', 'd')
    k1000o = k1000
    MODELS[k]['clm'] = clm
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION IMAGE
# ------------------------------------
CFGIMG_FILENAME = os.path.join(
    HOME, 'config', os.path.basename(__file__).replace('.py', '.txt'))
CFGIMG = PlotConfig(filename=CFGIMG_FILENAME)
CFGIMG.load()
# ------------------------------------
# --- LECTURE OBS
# ------------------------------------
for loc in LOCS_HYDRO:
    if len(loc) != 8:
        continue
    if not loc.startswith('K055'):
        continue
    # Lecture des données
    obs = read_PyspcFile(
        dirname=os.path.join(HOME, 'obs', 'phyc', SPC_VARNAME),
        stations=loc, varnames=SPC_VARNAME)
    if obs is None or len(obs) == 0:
        continue
# ------------------------------------
# --- LECTURE PRV
# ------------------------------------
    for prv_filename in PRV_FILENAMES:
        reader = Prevision19(filename=prv_filename)
        _, prv = reader.read_series(codes=[loc],
                                    first_dt=FIRST_DTIME, last_dt=LAST_DTIME,
                                    valid=False, warning=False)
        RUNTIMES = sorted({dt.strptime(v['DateDerObs'], '%Y%m%d%H00')
                           for v in prv.values()})
        for runtime in RUNTIMES:
            prv = read_Prevision19(filename=prv_filename, codes=[loc],
                                   first_dt=runtime, last_dt=runtime,
                                   valid=False, released=False)
            if prv is None or len(prv) == 0:
                continue
# ------------------------------------
# --- MAJ DE LA CONFIGURATION IMAGE
# ------------------------------------
            cfgimg = CFGIMG
            cfgimg['figure']['dirname'] = os.path.join(
                HOME, 'fig', 'oper', loc)
            if not os.path.exists(cfgimg['figure']['dirname']):
                os.makedirs(cfgimg['figure']['dirname'])
            cfgimg['figure']['filename'] = '{}_{}_raw-{}'.format(
                loc,
                runtime.strftime('%Y%m%d%H'),
                TREND
            )
            cfgimg['figure']['title'] = '{} ({}, {}) / {}'.format(
                LOCS_HYDRO[loc]['name'],
                LOCS_HYDRO[loc]['river'],
                loc,
                runtime.strftime('%Y-%m-%d %H:00')
            )
            fd = runtime - td(hours=EVENT['event']['maxltime'])
            ld = runtime + td(hours=2*EVENT['event']['maxltime'])
            cfgimg['figure']['xlim'] = [fd, ld]
# ------------------------------------
# --- SELECTION ET CONFIGURATION DES COURBES
# ------------------------------------
            series = Series(datatype='obs')
            # Prv
            for k in sorted(list(prv.keys())):
                if k[0] != loc or k[1] != SPC_VARNAME or k[2][0] != runtime \
                        or k[2][-1] != TREND or int(k[2][2]) not in MODELS:
                    continue
                series.add(serie=prv[k])
                k2 = tuple2str(k)
                ks = int(k[2][2])
                cfgimg.setdefault(k2, {})
                cfgimg[k2]['color'] = MODELS[ks]['clm'][0]
                cfgimg[k2]['linestyle'] = MODELS[ks]['clm'][1]
                cfgimg[k2]['marker'] = MODELS[ks]['clm'][2]
                cfgimg[k2]['linewidth'] = 1
                cfgimg[k2]['label'] = MODELS[ks]['Nom']
            # Obs
            series.extend(obs)
            k = tuple2str(list(obs.keys())[0])
            cfgimg.setdefault(k, {})
            cfgimg[k]['color'] = IMG_TABCOLORS[0]
            cfgimg[k]['linewidth'] = 2
            cfgimg[k]['label'] = 'obs'
# ------------------------------------
# --- MAJ DE LA CONFIGURATION IMAGE AVEC VALEURS PAR DEFAUT
# ------------------------------------
            for s in cfgimg:
                if s in ['figure', 'defaut']:
                    continue
                for o in cfgimg['defaut']:
                    cfgimg[s].setdefault(o, cfgimg['defaut'][o])
# ------------------------------------
# --- CREATION FIGURE
# ------------------------------------
            # Focer les séries sim et obs à être en avant-plan
            # Forcer les séries prv à être en arrière-plan
            ss = [s for s in series if len(s[0].split('_')) <= 2]
            for s in ss:
                series.move_to_end(s, last=True)
            filename = series.plot_series(plottype='hydro', config=cfgimg)
            print('Création de la figure {}'
                  ''.format(os.path.relpath(filename, start=HOME)))

#            break  # for runtime in RUNTIMES:
#        break  # for prv_filename in PRV_FILENAMES:
    break  # for loc in LOCS_HYDRO:
