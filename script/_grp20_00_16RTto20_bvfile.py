#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_grp20_00_16RTto20_bvfile:

.. role:: blue

.. role:: boldblue

Conversion des fichiers bassins de GRPv2016-Real Time à GRPv2020 (_grp20_00_16RTto20_bvfile.py)
-----------------------------------------------------------------------------------------------

Description
+++++++++++

Conversion des fichiers de bassin de GRP, de la version 2016 Temps-Réel à la version 2020 et copie des fichiers hypsométriques

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES DONNEES GRP16

:boldblue:`BDD_16` : :blue:`Répertoire de la base de données`

:boldblue:`BV_16` : :blue:`Nom du fichier BASSIN.DAT`

:boldblue:`CODES` : :blue:`Liste des bassins à exporter`

.. rubric:: CONFIGURATION DES DONNEES GRP20

:boldblue:`BDD_20` : :blue:`Répertoire de la base de données`

:boldblue:`BV_20` : :blue:`Répertoire des fichiers bassin et hypso`

"""
# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------------------------------------------
# IMPORTS
# ------------------------------------------------------------------------
import os
import os.path
import shutil

from pyspc.model.grp16 import GRPRT_Basin as GRPRT_Basin16
from pyspc.model.grp20 import GRP_Basin as GRP_Basin20

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES GRP16
# ------------------------------------------------------------------------
# BDD_16 = r'L:\O_Chaine_operationnelle\PROD\Plateformes_Modeles\\Grp_allier'\
BDD_16 = r'L:\O_Chaine_operationnelle\PROD\Plateformes_Modeles\\Grp'\
    r'\Temps_Reel\BD_Bassins'
BV_16 = 'BASSIN.DAT'
CODES = [
    'K0100020', 'K0114030', 'K0134010',
    'K0214010', 'K0243010', 'K0253020', 'K0258010', 'K0260010', 'K0274010',
    'K0333010', 'K0356310', 'K0403010', 'K0454010',
    'K0523010', 'K0550010', 'K0567520',
    'K0614010', 'K0624510', 'K0643110', 'K0673310',
    'K0753210', 'K0813020', 'K0983011', 'K1063010',
    'K1084020', 'K1173210',
    'K1251810', 'K1273110', 'K1284810',
    'K1314010', 'K1321810', 'K1363010', 'K1383010',
    'K1414010', 'K1533010',
    'K1713010', 'K1724210', 'K1753110', 'K1764010', 'K1773010',
    'K1833010', 'K1914510', 'K1954010',
    'K4013010', 'K4073110', 'K4123010', 'K4572210', 'K4873120',
    'K5090900', 'K5183020', 'K5234010', 'K5383020', 'K5433020',
    'K5552300', 'K5574100', 'K5623010',
    'K6022420', 'K6102430', 'K6173130', 'K6334010', 'K6373020',
    'K6453010', 'K6593020',
    'K7022620', 'K7143010', 'K7202610', 'K7433030', 'K7514010'
]
# CODES = [
#    'K2010820', 'K2070810', 'K2173020', 'K2240820',
#    'K2300810', 'K2330810', 'K2363020', 'K2383110',
#    'K2514010', 'K2514020', 'K2523010', 'K2593010',
#    'K2654010', 'K2674010', 'K2783010',
#    'K2821910', 'K2834010', 'K2851910', 'K2871910', 'K2884010',
#    'K2944010', 'K2981910', 'K3220210', 'K3222010', 'K3273010', 'K3373010'
# ]

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES GRP20
# ------------------------------------------------------------------------
BDD_20 = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\__MODELISATION\__GRPv2020\TR_LCI'
#    r'\__MODELISATION\__GRPv2020\TR_AL'
BV_20 = os.path.join(BDD_20, 'Bassins')
if not os.path.exists(BV_20):
    os.makedirs(BV_20)

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
print("-- Conversion des fichiers bassins de GRPv2016 Temps-Réel à GRPv2020")
for c in CODES:
    f = os.path.join(BDD_16, c, BV_16)
    # ------------------------------------------------------------------------
    # --- LECTURE DU FICHIER BASSIN GRP v2016 TEMPS-REEL
    # ------------------------------------------------------------------------
    try:
        basin16 = GRPRT_Basin16(filename=f)
        basin16.read()
        print('   -> Lecture du fichier v2016  : {}'
              ''.format(os.path.relpath(f, BDD_16)))
        for x in basin16['P']:
            basin16['P'][x]['t'] = '00J01H00M'
    except Exception:
        print('Avertissement: chargement incorrect v2016')
        continue
    # ------------------------------------------------------------------------
    # --- ECRITURE DU FICHIER BASSIN GRP v2020
    # ------------------------------------------------------------------------
    try:
        f20 = os.path.join(BV_20, '{}_00J01H00M.DAT'.format(c))
        basin20 = GRP_Basin20(filename=f20)
        basin20['E'] = basin16['Q']
        ce = list(basin20['E'].keys())[0]
        basin20['E'][ce]['n'] = basin16['B']
        basin20['P'] = basin16['P']
        if 'D' in basin16:
            basin20['T'] = basin16['D']
        basin20['N'] = 1.0
        basin20['L'] = {
            'n': 'Duree seuil pour les lacunes (format nnJnnHnnM)',
            't': '01J00H00M'}
        basin20.write()
        print('   -> Ecriture du fichier v2020 : {}'
              ''.format(os.path.relpath(f20, BDD_20)))
    except Exception:
        print('Avertissement: écriture incorrecte v2020')
        continue
    # ------------------------------------------------------------------------
    # --- COPIE COURBES HYPSO
    # ------------------------------------------------------------------------
    for e in ['txt']:
        bh = os.path.join(BDD_16, c, '{}-hypso.{}'.format(c, e))
        print('   -> Copie du fichier hypso    : {}'
              ''.format(os.path.relpath(bh, BDD_16)))
        try:
            shutil.copy2(bh, BV_20)
        except IOError:
            print('Avertissement, fichier manquant : {}'.format(bh))
