#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_001_subreach_bylochydro:

.. role:: blue

.. role:: boldblue

Définition des lieux par sous-tronçon (_postEvent_001_subreach_bylochydro.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Définition des lieux par sous-tronçon

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

Fichier de configuration
++++++++++++++++++++++++

.. rubric:: Fichier de configuration des sous-tronçons

.. code-block:: cfg

    [Loire_vellave]
    name = Loire_vellave
    title = Loire vellave
    sites_hydro = K0030020;K0100020;K0260010;K0340001;K0550010

Fichier créé
++++++++++++

.. rubric:: Fichier de configuration des sous-tronçons

.. code-block:: cfg

   [Loire_vellave]
   name = Loire_vellave
   title = Loire vellave
   sites_hydro = K0030020;K0100020;K0260010;K0340001;K0550010
   stations_hydro = K003002010;K010002010;K026001002;K034000101;K055001010

"""

# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import os.path
from pyspc import Config

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202102_Morvan_Cher\config\event.txt'
# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'subreaches'): lambda x: os.path.join(
        os.path.dirname(EVENT_FILENAME), x),
})
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()

# ------------------------------------
# --- LECTURE DE LA CONFIGURATION SUBREACHES
# ------------------------------------
SUBREACHES = Config(filename=EVENT['event']['subreaches'])
SUBREACHES.read()
SUBREACHES.convert({
    (s, 'sites_hydro'): Config.to_listofstr for s in SUBREACHES
})

# ------------------------------------
# --- COMPLETER SUBREACHES PAR LOC_HYDRO
# ------------------------------------
for s in SUBREACHES:
    SUBREACHES[s]['stations_hydro'] = []
    for loc in LOCS_HYDRO:
        if len(loc) == 10 and loc[:8] in SUBREACHES[s]['sites_hydro']:
            SUBREACHES[s]['stations_hydro'].append(loc)
SUBREACHES.convert({
    (s, 'sites_hydro'): Config.from_listofstr for s in SUBREACHES
})
SUBREACHES.convert({
    (s, 'stations_hydro'): Config.from_listofstr for s in SUBREACHES
})
SUBREACHES.write()
