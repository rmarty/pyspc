#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_mfReports:

.. role:: blue

.. role:: boldblue

Télécharger les rapports/bulletins/fiches MF (_mfReports.py)
------------------------------------------------------------

Description
+++++++++++

Télécharger les rapports/bulletins/fiches MF

Paramètres
++++++++++


.. rubric:: Elements communs


:boldblue:`OUTPUT_DIRNAME` : :blue:`Répertoire de stockage local`

:boldblue:`DATA_TYPE` : :blue:`Type de rapport MF`


.. rubric:: Bulletin climatologique mensuel


:boldblue:`YEARS` : :blue:`Liste des années`

:boldblue:`MONTHS` : :blue:`Liste des mois`

:boldblue:`DAYS` : :blue:`Liste des jours`

:boldblue:`DEPTS` : :blue:`Liste des départements (avant 2012)`

:boldblue:`REGIONS` : :blue:`Liste des régions (après 2012)`


.. rubric:: Bulletin climatologique journalier


:boldblue:`FIRST_DT` : :blue:`Date du premier bulletin`

:boldblue:`LAST_DT` : :blue:`Date du dernier bulletin`

:boldblue:`DELTA_DT` : :blue:`Intervalle de temps entre deux bulletins`


.. rubric:: Fiches stations


:boldblue:`STATIONS` : :blue:`Liste des stations à considérer`



"""
# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt, timedelta as td
import os
import sys
import subprocess

# ------------------------------------
# PARAMETRES : ELEMENTS COMMUNS
#   OUTPUT_DIRNAME  : Répertoire de stockage local
#   DATA_TYPE       : Type de rapport MF
# ------------------------------------
OUTPUT_DIRNAME = r'S:\SHPEC\DPEC\Hydrologie\1-Donnees\10-Archives-hydro-meteo'\
    r'\102-MeteoFrance\Bulletins_climato'
DATA_TYPE = "monthly"

print(" -- Télécharger les rapports/bulletins/fiches MF")
print("    - Dossier de sortie : {}".format(OUTPUT_DIRNAME))

# ------------------------------------
# PARAMETRES : BULLETIN CLIMATOLOGIQUE MENSUEL
#   YEARS    : Liste des années
#   MONTHS   : Liste des mois
#   DAYS     : Liste des jours
#   DEPTS    : Liste des départements (avant 2012)
#   REGIONS  : Liste des régions (après 2012)
# ------------------------------------
if DATA_TYPE == "monthly":
    print("    - Bulletin climatique mensuel")
    # Période temporelle
    YEARS = [2014]
    MONTHS = [11, 12]
    DAYS = [1]
    # Zones (département entre 2001 et 2011, région à partir de 2012)
    DATES = [dt(year, month, day)
             for year in YEARS
             for month in MONTHS
             for day in DAYS]
    DEPTS = ['03', '07', '18', '36', '37', '42', '43', '58', '71']
    REGIONS = ['00', '03', '04', '06', '12', '22']
# ------------------------------------
# PARAMETRES : BULLETIN CLIMATOLOGIQUE JOURNALIER
#   FIRST_DT : Date du premier bulletin
#   LAST_DT  : Date du dernier bulletin
#   DELTA_DT : Intervalle de temps entre deux bulletins
# ------------------------------------
elif DATA_TYPE == "daily":
    print("    - Bulletin climatique journalier")
    # Période temporelle
    FIRST_DT = dt(2014, 11, 4)
    LAST_DT = dt(2014, 11, 5)
    DELTA_DT = td(DAYS=1)
    LEN_DT = int((LAST_DT - FIRST_DT) / DELTA_DT) + 1
    DATES = [FIRST_DT + kt * DELTA_DT for kt in range(LEN_DT)]
    # Zone nationale
    FRANCE = ['FR']
# ------------------------------------
# PARAMETRES : FICHES STATIONS
#   STATIONS : Liste des stations à considérer
# ------------------------------------
elif DATA_TYPE == "station":
    print("    - Fiche des stations MF")
    STATIONS = ["03036002", "03155003", "03180001", "03185007", "03186001",
                "03226001", "03248001", "03285001", "07130002", "07154005",
                "18015003", "18033001", "18047001", "18087003", "18092001",
                "18135001", "18172003", "18175003", "18176001", "18187004",
                "18223003", "18251001", "18279005", "21567001", "23001001",
                "23008004", "23013001", "23031002", "23089001", "23113001",
                "36085001", "36089003", "36093002", "36107001", "36127002",
                "36136001", "37023002", "37192001", "41097001", "42005001",
                "42019004", "42039003", "42052002", "42102002", "42156002",
                "42204002", "42224003", "42248002", "42257002", "43046001",
                "43062001", "43091005", "43093001", "43095001", "43111002",
                "43130002", "43137003", "43150001", "58019001", "58062001",
                "58092001", "58160001", "63319002", "63354004", "69174002",
                "71014004", "71022001", "71289001", "71320001", "71464001",
                "71491001", "71540002"]

# ------------------------------------
# DEPENDANCES
# ------------------------------------
# *****************************************
#               SCRIPT
# *****************************************
if DATA_TYPE in ["daily", "monthly"]:
    for date in DATES:
        print("      - Date du bulletin : {}"
              "".format(date.strftime("%d/%m/%Y")))
        # Zone
        zones = ''
        if DATA_TYPE == "monthly" and date < dt(2012, 1, 1):
            zones = DEPTS
        elif DATA_TYPE == "monthly" and date >= dt(2012, 1, 1):
            zones = REGIONS
        elif DATA_TYPE == "daily":
            zones = '00'
        # output dirname
        output_dirname = os.path.join(
            OUTPUT_DIRNAME,
            date.strftime('%Y'),
            date.strftime('%m')
        )
        if not os.path.exists(output_dirname):
            os.makedirs(output_dirname)
        for zone in zones:
            # Construction de la ligne de commande
            print("        - Zone demandée : {}".format(zone))
            processArgs = [
                'python',
                os.environ['PYSPC_BIN'] + '\\'
                'mfReport.py',
                '-O', output_dirname,
                '-s', zone,
                '-t', DATA_TYPE,
                '-r', date.strftime("%Y%m%d")
                ]
#            print(processArgs)
            processRun = subprocess.Popen(
                processArgs, universal_newlines=True,
                shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
            processRun.wait()
elif DATA_TYPE in ["station"]:
    for station in STATIONS:
        print("      - Station : {}".format(station))
        processArgs = [
            'python',
            os.environ['PYSPC_BIN'] + '\\'
            'mfReport.py',
            '-O', OUTPUT_DIRNAME,
            '-s', station,
            '-t', DATA_TYPE
            ]
#            print(processArgs)
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
