#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_110_phyc_download:

.. role:: blue

.. role:: boldblue

Téléchargement des données depuis la PHyC (_postEvent_110_phyc_download.py)
------------------------------------------------------------------------------------

Description
+++++++++++

Téléchargement des données depuis la PHyC

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

"""

# *****************************************************************************
#               PARAMETRES
# *****************************************************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt, timedelta as td
import os.path
import subprocess
import sys
from pyspc import Config

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
})
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO / LOCS_METEO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
LOCS_METEO = Config(filename=os.path.join(HOME, 'config', 'locs_meteo.txt'))
LOCS_METEO.read()

# ------------------------------------
# --- PARAMETRE SUBPROCESS
# ------------------------------------
CFG_FILENAME = os.path.join(HOME, 'config', 'phyc.txt')
for v in ['QI', 'HI', 'PH']:
    d = os.path.join(HOME, 'obs', 'phyc', v)
    if not os.path.exists(d):
        print('Création du répertoire {}'
              ''.format(os.path.relpath(d, start=HOME)))
        os.makedirs(d)
START = EVENT['event']['firstdt']
END = EVENT['event']['lastdt'] + td(hours=EVENT['event']['maxltime'])

# ------------------------------------
# --- EXPORT PHYC
# ------------------------------------
print('Téléchargement des données QI/HI')
for loc in LOCS_HYDRO:
    if len(loc) == 8:
        processArgs = [
            'python',
            os.environ['PYSPC_BIN'] + '\\'
            'phyc2xml.py',
            '-s', loc,
            '-t', 'data_obs_hydro',
            '-F', START.strftime("%Y%m%d%H"),
            '-L', END.strftime("%Y%m%d%H"),
            '-n', 'QI',
            '-c', CFG_FILENAME,
            '-O', os.path.join(HOME, 'obs', 'phyc', 'QI')
        ]
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
#        break
    elif len(loc) == 10:
        processArgs = [
            'python',
            os.environ['PYSPC_BIN'] + '\\'
            'phyc2xml.py',
            '-s', loc,
            '-t', 'data_obs_hydro',
            '-F', START.strftime("%Y%m%d%H"),
            '-L', END.strftime("%Y%m%d%H"),
            '-n', 'HI',
            '-c', CFG_FILENAME,
            '-O', os.path.join(HOME, 'obs', 'phyc', 'HI')
        ]
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
#        break
print('Téléchargement des données PH')
for loc in LOCS_METEO:
    if len(loc) == 8:
        processArgs = [
            'python',
            os.environ['PYSPC_BIN'] + '\\'
            'phyc2xml.py',
            '-s', loc,
            '-t', 'data_obs_meteo',
            '-F', START.strftime("%Y%m%d%H"),
            '-L', END.strftime("%Y%m%d%H"),
            '-n', 'PH',
            '-c', CFG_FILENAME,
            '-O', os.path.join(HOME, 'obs', 'phyc', 'PH')
        ]
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
#        break
