#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_200_bp_plot:

.. role:: blue

.. role:: boldblue

Tracer les BP et les observations pluviométriques (_postEvent_200_bp_plot.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Tracer les BP et les observations pluviométriques

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`FIRST_DTIME` : :blue:`Référentiel du SPC Loire-Cher-Indre`

:boldblue:`LAST_DTIME` : :blue:`Première date des runtimes. None si définie par event`

:boldblue:`LAST_DTIME` : :blue:`Dernière date des runtimes. None si définie par event`

:boldblue:`TARGET_DAYS` : :blue:`Liste des jours à tracer (version 1 fig/jour)`

Fichiers créés
++++++++++++++

.. figure:: ../_static/41003.png
   :alt: Figure
   :figclass: figurecentre

   Figure 41003.png

.. figure:: ../_static/20191020.png
   :alt: Figure
   :figclass: figurecentre

   Figure 20191020.png

"""

# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt
import glob
import os.path
import pandas as pnd

from pyspc import Config, Series, read_BP, read_PyspcFile, read_BdApbp
from pyspc.plotting.bp import plot_bp_byzone, plot_bp_byday

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'config\event.txt'
# -----------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES FIGURES
#   SPC_VARNAME      : Variable
#   FIRST_DTIME      : Première date des runtimes. None si définie par event
#   LAST_DTIME       : Dernière date des runtimes. None si définie par event
# -----------------------------------------------------------------------------
SPC_VARNAME = 'PJ'
FIRST_DTIME = dt(2020, 6, 9)
LAST_DTIME = dt(2020, 6, 15)

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES BULLETINS
#   TARGET_DAYS      : Liste des jours à tracer (version 1 fig/jour)
# ------------------------------------------------------------------------
SOURCE = 'BdApBp'  # 'BdApBp', 'XML'
TARGET_DAYS = [dt(2020, 6, 12), dt(2020, 6, 13)]

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
BP_DIRNAME = os.path.join(HOME, 'fcst', 'bp')
OBS_DIRNAME = os.path.join(HOME, 'obs', 'phyc', 'PH')
RADAR_DIRNAME = os.path.join(HOME, 'obs', 'lamedo', 'PJ')
FIG_DIRNAME = os.path.join(HOME, 'fig', 'bp')
if not os.path.exists(FIG_DIRNAME):
    os.makedirs(FIG_DIRNAME)
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
    ('event', 'zap'): Config.to_listofstr,
})
LOCS_METEO = Config(filename=os.path.join(HOME, 'config', 'locs_meteo.txt'))
LOCS_METEO.read()
if FIRST_DTIME is None:
    FIRST_DTIME = EVENT['event']['firstdt']
if LAST_DTIME is None:
    LAST_DTIME = EVENT['event']['lastdt']
RUNTIMES = pnd.date_range(start=FIRST_DTIME, end=LAST_DTIME, freq='D')
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION ZAP
# ------------------------------------
ZAP = Config(filename=os.path.join(HOME, 'config', 'zap.txt'))
ZAP.read()
ZONES = list(EVENT['event']['zap'])
PROVIDERS = list({ZAP[z]['provider'] for z in ZONES})
# ------------------------------------
# --- SERIES DES BP
# ------------------------------------
bp_series = Series(datatype='fcst', name='BP')
for r in RUNTIMES:
    if SOURCE == 'XML':
        for p in PROVIDERS:
            bp_filenames = glob.glob(os.path.join(
                BP_DIRNAME, 'bp_{}_{}*.xml'.format(p, r.strftime('%Y%m%d'))
            ))
            if len(bp_filenames) == 0:
                print("!!! Avertissement: pas de BP disponible pour le "
                      "fournisseur '{}' à la date '{}'"
                      "".format(p, r.strftime('%Y-%m-%d')))
                continue
            if len(bp_filenames) > 2:
                print("??? Avertissement: plus de 2 BP disponibles pour le "
                      "fournisseur '{}' à la date '{}'"
                      "".format(p, r.strftime('%Y-%m-%d')))
                bp_filenames = [bp_filenames[0], bp_filenames[-1]]
            elif len(bp_filenames) == 1:
                print("??? Avertissement: 1 seul BP disponible pour le "
                      "fournisseur '{}' à la date '{}'"
                      "".format(p, r.strftime('%Y-%m-%d')))
                bp_filenames = [bp_filenames[0], bp_filenames[0]]
            for f in bp_filenames:
                series = read_BP(filename=f, zones=ZONES)
                if len(series) > 0:
                    bp_series.extend(series)
                print(f)
    elif SOURCE == 'BdApBp':
        bp_filenames = glob.glob(os.path.join(
            BP_DIRNAME, '*_{}*_bp-long.json'.format(r.strftime('%Y%m%d'))
        ))
        if len(bp_filenames) == 0:
            print("!!! Avertissement: pas de BP disponible à la date '{}'"
                  "".format(r.strftime('%Y-%m-%d')))
            continue
        if len(bp_filenames) > 2:
            print("??? Avertissement: plus de 2 BP disponiblesà la date '{}'"
                  "".format(r.strftime('%Y-%m-%d')))
            bp_filenames = [bp_filenames[0], bp_filenames[-1]]
        elif len(bp_filenames) == 1:
            print("??? Avertissement: 1 seul BP disponible à la date '{}'"
                  "".format(r.strftime('%Y-%m-%d')))
            bp_filenames = [bp_filenames[0], bp_filenames[0]]
        for f in bp_filenames:
            series = read_BdApbp(filename=f, zones=ZONES)
            # series = read_BP(filename=f, zones=ZONES)
            if len(series) > 0:
                bp_series.extend(series)
# ------------------------------------
# --- SERIES DES PRCP_RADAR
# ------------------------------------
radar_series = Series(datatype='obs', name='RADAR')
for z in ZONES:
    z2 = 'BP{}_antilope,j1,rr,moy'.format(z)
    series = read_PyspcFile(
        stations=z2, varnames=SPC_VARNAME, dirname=RADAR_DIRNAME)
    for serie in series.values():
        serie.code = z
        radar_series.add(serie=serie)
# print(radar_series)
# ------------------------------------
# --- SERIES DES PRCP
# ------------------------------------
prcp_series = Series(datatype='obs', name='PRCP')
for z in ZONES:
    series_ph = read_PyspcFile(
        stations=ZAP[z]['sites_meteo'].split(';'), varnames='PH',
        dirname=OBS_DIRNAME)
    if series_ph is None:
        continue
    series_pj = series_ph.upscale(toparam=SPC_VARNAME, dayhour=0)
    wa = series_pj.weighted_average(weights=1)
    wa.code = z
    prcp_series.add(serie=wa)
# print(prcp_series)
if len(prcp_series) == 0:
    prcp_series = None
# ------------------------------------
# --- TRACER LA FIGURE 'EVENT'
# ------------------------------------
fig_filenames = plot_bp_byzone(
    bp_series=bp_series, radar_series=radar_series, prcp_series=prcp_series,
    dirname=FIG_DIRNAME)
for f in fig_filenames:
    print('Fichier image {}'.format(os.path.relpath(f, start=HOME)))
# ------------------------------------
# --- TRACER LA FIGURE 'DAYS'
# ------------------------------------
fig_filenames = plot_bp_byday(
    bp_series=bp_series, radar_series=radar_series, prcp_series=prcp_series,
    target_days=TARGET_DAYS, dirname=FIG_DIRNAME)
for f in fig_filenames:
    print('Fichier image {}'.format(os.path.relpath(f, start=HOME)))
