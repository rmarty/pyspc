#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_grp20_01_16RTto20_cfgfile:

.. role:: blue

.. role:: boldblue

Conversion des fichiers de runs de GRPv2016 Temps-Réel à GRPv2020 (_grp20_01_16RTto20_cfgfile.py)
-------------------------------------------------------------------------------------------------

Description
+++++++++++

Conversion des fichiers de runs de GRP Temps-Réel, de la version 2016 à la version 2020

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES DONNEES GRP16 Temps-Réel

:boldblue:`BDD_16` : :blue:`Répertoire de configuration de GRP Temps-Réel`

:boldblue:`CFG_16` : :blue:`Fichier de configuration Temps-Réel`

:boldblue:`CODES` : :blue:`Liste des bassins à exporter`


.. rubric:: CONFIGURATION DES DONNEES GRP20

:boldblue:`BDD_20` : :blue:`Répertoire de configuration de GRP (Paramétrage)`

:boldblue:`CFG_20` : :blue:`Fichier de configuration`

"""
# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------------------------------------------
# IMPORTS
# ------------------------------------------------------------------------
from datetime import datetime as dt
import os
import os.path

from pyspc.model.grp16 import GRPRT_Basin as GRPRT_Basin16
from pyspc.model.grp20 import GRP_Cfg as GRP_Cfg20, GRP_Run as GRP_Run20

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES GRP16
# ------------------------------------------------------------------------
# BDD_16 = r'L:\O_Chaine_operationnelle\PROD\Plateformes_Modeles\Grp'\
BDD_16 = r'L:\O_Chaine_operationnelle\PROD\Plateformes_Modeles\Grp_allier'\
    r'\Temps_Reel\Parametrage'
CFG_16 = os.path.join(BDD_16, 'LISTE_Bassins.DAT')
# CODES = [
#    'K0114030', 'K0134010',
#    'K0214010', 'K0243010', 'K0253020', 'K0258010', 'K0260010', 'K0274010',
#    'K0333010', 'K0356310', 'K0403010', 'K0454010',
#    'K0523010', 'K0550010', 'K0567520',
#    'K0614010', 'K0624510', 'K0643110', 'K0673310',
#    'K0753210', 'K0813020', 'K0983011', 'K1063010', 'K1084020',
#    'K1173210', 'K1251810', 'K1273110', 'K1284810',
#    'K1314010', 'K1321810', 'K1363010', 'K1383010',
#    'K1414010', 'K1533010',
#    'K1713010', 'K1724210', 'K1753110', 'K1764010', 'K1773010',
#    'K1833010', 'K1914510', 'K1954010',
#    'K4013010', 'K4073110', 'K4123010', 'K4572210', 'K4873120',
#    'K5090900', 'K5183020', 'K5234010', 'K5383020', 'K5433020',
#    'K5552300', 'K5574100', 'K5623010',
#    'K6022420', 'K6102430', 'K6173130', 'K6334010', 'K6373020',
#    'K6453010', 'K6593020',
#    'K7022620', 'K7143010', 'K7202610', 'K7433030', 'K7514010'
# ]
CODES = [
    'K2010820', 'K2070810', 'K2173020', 'K2240820',
    'K2300810', 'K2330810', 'K2363020', 'K2383110',
    'K2514010', 'K2514020', 'K2523010', 'K2593010',
    'K2654010', 'K2674010', 'K2783010',
    'K2821910', 'K2834010', 'K2851910', 'K2871910', 'K2884010',
    'K2944010', 'K2981910', 'K3220210', 'K3222010', 'K3273010', 'K3373010'
]

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES GRP20
# ------------------------------------------------------------------------
BDD_20 = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\__MODELISATION\__GRPv2020\TR_AL'
#    r'\__MODELISATION\__GRPv2020\TR_LCI'
# CFG_20 = os.path.join(BDD_20, 'LISTE_BASSINS_LCI.DAT')
CFG_20 = os.path.join(BDD_20, 'LISTE_BASSINS_AL.DAT')
if not os.path.exists(BDD_20):
    os.makedirs(BDD_20)

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
print("-- Conversion des fichiers de runs de GRPv2016 Temps-Réel à GRPv2020")
print('   -> Lecture du fichier v2016 Temps-Réel  : {}'
      ''.format(os.path.relpath(CFG_16, BDD_16)))
runs20 = GRP_Cfg20(filename=CFG_20)
rs20 = []
with open(CFG_16, 'r', encoding='iso-8859-1') as f16:
    for l16 in f16.readlines():
        if not l16.startswith('!K'):
            continue
        r16 = l16.strip().split('!')
        if r16[1] not in CODES:
            continue
        bvdir = os.path.abspath(
            os.path.join(BDD_16, '..', 'BD_Bassins', r16[1]))
        basin16 = GRPRT_Basin16(filename=os.path.join(bvdir, 'BASSIN.DAT'))
        basin16.read()
        print('   -> Lecture du fichier v2016 Temps-Réel  : {}'
              ''.format(os.path.relpath(basin16.filename, BDD_16)))
        arch16 = os.path.join(bvdir, 'QV_10A.DAT')
        start = end = ''
        with open(arch16, 'r', encoding='iso-8859-1') as a16:
            for x in range(5):
                a16.readline()
            start = dt.strptime(
                a16.readline().split(';')[1],
                '%Y%m%d%H%M').strftime('%d/%m/%Y %H:%M')
        print('   -> Lecture du fichier v2016 Temps-Réel  : {}'
              ''.format(os.path.relpath(arch16, BDD_16)))
        snow = bool(int(float(r16[5])))
        rna = os.path.exists(os.path.join(bvdir, 'Parametre_RNA.DAT'))
        r20 = {
            'CODE': r16[1], 'PDT': '00J01H00M', 'NOM': basin16['B'],
            'SURFACE': basin16['S'], 'RT': r16[2], 'DEB': start, 'FIN': end,
            'ST': 1 if not snow and not rna else 0,
            'SR': 1 if not snow and rna else 0,
            'AT': 1 if snow and not rna else 0,
            'AR': 1 if snow and rna else 0,
            'HOR1': '00J00H00M', 'HOR2': '00J00H00M',
            'SC1': -99.0, 'SC2': -99.0,
            'SV1': float(r16[9]), 'SV2': float(r16[10]), 'SV3': float(r16[11]),
            'NJ': 0, 'HC': '', 'EC': 0, 'ECART': 0, 'INC': 0,
        }
        r20 = GRP_Run20(**r20)
#        print(r20)
        rs20.append(r20)
#        break

runs20.extend(rs20)
runs20.write()
print('   -> Ecriture du fichier v2020  : {}'
      ''.format(os.path.relpath(CFG_20, BDD_20)))
