#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:
"""
import os.path
import geopandas as gpd
import matplotlib.pyplot as mplt
from matplotlib.lines import Line2D
import pandas as pnd

# ======================================================================
# --- PARAMETRES
# ======================================================================
# HOME = os.path.abspath(os.path.dirname(__file__))
HOME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20\__MODELISATION'\
    r'\__GRPv2020'
GIS_DIRNAME = os.path.join(HOME, 'SIG')
BV_BASENAME = 'bnbv_125_grp'  # 'bv_tete' 'bv_125' 'bnbv_125_grp'
BV_FILENAME = os.path.join(GIS_DIRNAME, f'{BV_BASENAME}.geojson')
LACI_FILENAME = os.path.join(GIS_DIRNAME, 'spc_laci.geojson')
RIVER_FILENAME = os.path.join(GIS_DIRNAME, 'river.geojson')
VERIF_FILENAME = os.path.join(HOME, 'LISTE_BASSINS_CALVAL_finalconfig.txt')
OUTPUT_DIRNAME = os.path.join(HOME, 'SIG')
DPI = 150
ALPHA = 1

# ======================================================================
# --- CHARGEMENT DES DONNEES
# ======================================================================
DF_BV = gpd.read_file(BV_FILENAME)
DF_LACI = gpd.read_file(LACI_FILENAME)
DF_RIVER = gpd.read_file(RIVER_FILENAME)
DF_VERIF = pnd.read_csv(VERIF_FILENAME, sep=';', index_col=None)
DF_VERIF['tag'] = DF_VERIF.apply(
    lambda row: '{} - {} - {}'.format(row['HOR'], row['SC'], row['STAR']),
    axis=1)

df = DF_BV.merge(DF_VERIF, on='CODE', how='left')
gdf = gpd.GeoDataFrame(df)

gdf['SURFCALC'] = gdf['SURFCALC'].astype(float)
gdf = gdf.sort_values('SURFCALC', ascending=False)

color_mapping = {
    "HOR1 - SC1 - ST": "tab:purple",
    "HOR1 - SC1 - SR": "tab:blue",
    "HOR1 - SC1 - AT": "tab:red",
    "HOR1 - SC1 - AR": "tab:green",
    "HOR1 - SC2 - ST": "tab:pink",
    "HOR1 - SC2 - SR": "tab:cyan",
    "HOR1 - SC2 - AT": "tab:orange",
    "HOR1 - SC2 - AR": "tab:olive",
    "HOR2 - SC2 - SR": "grey",
}

# ======================================================================
# --- BOXPLOT CALVAL SCORES
# ======================================================================
filename = os.path.splitext(VERIF_FILENAME)[0] + '.png'
print(f'>>> {os.path.relpath(filename, HOME)}')
fig, (ax1, ax2) = mplt.subplots(ncols=2)

# Box 'remplie'
DF_VERIF.boxplot(ax=ax1, column=['Eff_Cal', 'Eff_Val'], patch_artist=True)

ax1.set_ylim((0, 1))
ax1.set_title('Efficience')

# Box 'remplie'
DF_VERIF.boxplot(ax=ax2, column=['POD', 'FAR', 'CSI'], patch_artist=True)

ax2.set_ylim((0, 100))
ax2.set_title('Critères de contingence')

for a in [ax1, ax2]:
    for x in a.get_lines():
        if x.get_color() == '#2ca02c':  # median
            x.set_color('#e26c57')
            x.set_linewidth(2)
        elif x.get_color() == 'black':  # decile
            x.set_color('#414edf')
            x.set_linewidth(1.5)
        elif x.get_color() == '#1f77b4':  # whiskerp
            x.set_color('#82bfea')
            x.set_linewidth(1.5)
    for x in a.artists:
        x.set(facecolor='#82bfea', edgecolor='#82bfea')

fig.tight_layout()
fig.savefig(filename, dpi=DPI)
mplt.close(fig)

# ======================================================================
# --- BOXPLOT CALVAL CONFIG
# ======================================================================
filename = os.path.splitext(VERIF_FILENAME)[0] + '_config.png'
print(f'>>> {os.path.relpath(filename, HOME)}')
fig, ax = mplt.subplots(ncols=1)

configs = DF_VERIF['tag'].value_counts().to_frame()
configs = configs.reset_index(drop=False)
configs.columns = ['configuration', 'effectif']
configs = configs.sort_values(by='configuration', axis=0, ascending=False)

configs.plot.barh(x='configuration', y='effectif', ax=ax, rot=0, stacked=True)
ax.set_title('Configurations pertinentes')

fig.tight_layout()
fig.savefig(filename, dpi=DPI)
mplt.close(fig)

# ======================================================================
# --- CARTE EFFICIENCE
# ======================================================================
filename = os.path.join(OUTPUT_DIRNAME, f'{BV_BASENAME}_calval_eff.png')
fig, (ax1, ax2) = mplt.subplots(ncols=2, sharex=True, sharey=True)
fig.set_size_inches(11.69, 8.27)  # A4 paysage
cb_ax = fig.add_axes([0.10, 0.10, 0.80, 0.02])

DF_LACI.boundary.plot(ax=ax1, color='black')
DF_LACI.boundary.plot(ax=ax2, color='black')

gdf.plot(ax=ax1, column='Eff_Cal', edgecolor='darkgrey',
         vmin=0, vmax=1, cmap='jet_r', alpha=ALPHA)
gdf.plot(ax=ax2, column='Eff_Val', edgecolor='darkgrey',
         vmin=0, vmax=1, cmap='jet_r', alpha=ALPHA,
         legend=True, cax=cb_ax,
         legend_kwds={'label': "Efficience", 'orientation': "horizontal"})

DF_RIVER.plot(ax=ax1, color='tab:brown')
DF_RIVER.plot(ax=ax2, color='tab:brown')

ax1.set_axis_off()
ax2.set_axis_off()
ax1.set_title('Efficience en calage')
ax2.set_title('Efficience en validation')

fig.tight_layout()
fig.suptitle('Modélisation GRP v2021 - Calage Contrôle', fontsize=14)
fig.savefig(filename, dpi=DPI)
mplt.close(fig)
print(f'>>> {filename}')

# ======================================================================
# --- CARTE VERIFICATION
# ======================================================================
filename = os.path.join(OUTPUT_DIRNAME, f'{BV_BASENAME}_calval_table.png')
fig, (ax1, ax2, ax3) = mplt.subplots(ncols=3, sharex=True, sharey=True)
fig.set_size_inches(11.69, 8.27)  # A4 paysage

DF_LACI.boundary.plot(ax=ax1, color='black')
DF_LACI.boundary.plot(ax=ax2, color='black')
DF_LACI.boundary.plot(ax=ax3, color='black')

gdf.plot(ax=ax1, column='POD', edgecolor='darkgrey',
         vmin=0, vmax=100, cmap='jet_r', alpha=ALPHA,
         legend=True, legend_kwds={'label': "POD",
                                   'orientation': "horizontal"})
gdf.plot(ax=ax2, column='FAR', edgecolor='darkgrey',
         vmin=0, vmax=100, cmap='jet', alpha=ALPHA,
         legend=True, legend_kwds={'label': "FAR",
                                   'orientation': "horizontal"})
gdf.plot(ax=ax3, column='CSI', edgecolor='darkgrey',
         vmin=0, vmax=100, cmap='jet_r', alpha=ALPHA,
         legend=True, legend_kwds={'label': "CSI",
                                   'orientation': "horizontal"})

DF_RIVER.plot(ax=ax1, color='tab:brown')
DF_RIVER.plot(ax=ax2, color='tab:brown')
DF_RIVER.plot(ax=ax3, color='tab:brown')

ax1.set_axis_off()
ax2.set_axis_off()
ax3.set_axis_off()
ax1.set_title('Probabilité de détection')
ax2.set_title('Ratio de fausses alertes')
ax3.set_title('Indice de Succès Critique')

fig.tight_layout()
fig.suptitle('Modélisation GRP v2021 - Calage Contrôle', fontsize=14)
fig.savefig(filename, dpi=DPI)
mplt.close(fig)
print(f'>>> {filename}')

# ======================================================================
# --- CARTE CONFIGURATION
# ======================================================================
filename = os.path.join(OUTPUT_DIRNAME, f'{BV_BASENAME}_calval_config.png')
fig = mplt.figure(dpi=DPI)
fig.set_size_inches(8.27, 11.69)  # A4 portrait
ax = fig.add_axes([0.05, 0.05, 0.85, 0.90])

DF_LACI.boundary.plot(ax=ax, color='black')

gdf['color'] = gdf.apply(
    lambda row: color_mapping.get(row['tag'], 'grey'), axis=1)

gdf.plot(ax=ax, color=gdf['color'].values, edgecolor='darkgrey')

DF_RIVER.plot(ax=ax, color='tab:brown')

ax.set_axis_off()
# ax.set_title('Configuration la plus adaptée')

legend_elements = [Line2D([0], [0], marker='o', linestyle='',
                          label=x, color=color_mapping[x])
                   for x in sorted(color_mapping)]
ax.legend(handles=legend_elements, loc='upper center',
          bbox_to_anchor=(0.25, 0.35), fancybox=True, shadow=True, ncol=1,
          fontsize=14)

fig.tight_layout()
fig.suptitle('Modélisation GRP v2021 - Calage Contrôle', fontsize=14)
fig.savefig(filename, dpi=DPI)
mplt.close(fig)
print(f'>>> {filename}')
