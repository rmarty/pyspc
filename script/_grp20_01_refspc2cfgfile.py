#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_grp20_01_refspc2cfgfile:

.. role:: blue

.. role:: boldblue

Création du fichier LISTE_BASSINS.DAT à partir du référentiel (_grp20_01_refspc2cfgfile.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Création du fichier des runs de GRP, à partir du référentiel

Paramètres
++++++++++

"""
# ------------------------------------------------------------------------
# IMPORTS
# ------------------------------------------------------------------------
from datetime import datetime as dt
import glob
import os.path

from pyspc.metadata.refspc import RefSPC
from pyspc.model.grp20 import GRP_Cfg as GRP_Cfg20, GRP_Run as GRP_Run20

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES GRP20
# ------------------------------------------------------------------------
BDD = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\__MODELISATION\__GRPv2020\CAL_TEST'
BV = os.path.join(BDD, 'Bassins')
CFG = os.path.join(BDD, 'LISTE_BASSINS.DAT')
START = dt(2008, 8, 1)
END = dt(2022, 9, 1)
# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES REFSPC
# ------------------------------------------------------------------------
REFSPC = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20\BDD.sqlite'

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
#PATCH = {
#    'K2163110': {'t2': 13, 't5': 20},
#    'K2283110': {'t2': 11, 't5': 18},
#    'K2316210': {'t2': 12, 't5': 17},
#    'K2335510': {'t2': 7, 't5': 12},
#    'K2753010': {'t2': 9, 't5': 14},
#    'K2756110': {'t2': 2, 't5': 4},
#    'K5730001': {'t2': 10, 't5': None},
#    'K6063010': {'t2': 9, 't5': 15},
#}
print("-- Création du fichier des runs de GRP, à partir du référentiel")
bvfiles = glob.glob(os.path.join(BV, '*.DAT'))
runs20 = GRP_Cfg20(filename=CFG)
for f in bvfiles:
    s = os.path.splitext(os.path.basename(f))[0].split('_')[0]
    t = os.path.splitext(os.path.basename(f))[0].split('_')[1]
    print(s, t)
    # Lecteur du référentiel
    reader = RefSPC(filename=REFSPC)
    # Méta-données du site hydrologique
    header, loc = reader.get_loc_hydro(codes=[s])
    loc = list(loc.values())[0]
    # Statistiques du site hydrologique
    sc1 = -99.0
    sc2 = -99.0
    sv1 = -99.0
    sv2 = -99.0
    sv3 = -99.0
    ecart = -99
    header, stat = reader.get_stat_hydro(codes=[s])
    for v in stat.values():
        if v['stat_ref_spc'] != 1:
            continue
#        sc1 = 0.1 * v['t2']  # Débits classés par HP
#        sc2 = 0.2 * v['t2']  # Débits classés par HP
        sv1 = 0.5 * v['t2']
        sv2 = 1.0 * v['t2']
        sv3 = 1.0 * v['t5'] if v['t5'] is not None else 1.5 * v['t2']
        ecart = max(1, int(0.25 * sv2))
#    if sv1 == -99.0 and s in PATCH:
#        sv1 = 0.5 * PATCH[s]['t2']
#        sv2 = 1.0 * PATCH[s]['t2']
#        sv3 = 1.0 * PATCH[s]['t5'] if PATCH[s]['t5'] is not None
#        else 1.5 * PATCH[s]['t2']
#        ecart = max(1, int(0.25 * sv2))

    if sv1 == -99.0:
        print('>>> BESOIN DE STATS DANS REFERENTIEL')

    r20 = {
        'CODE': s, 'PDT': t,
        'NOM': '{} à {}'.format(loc['nom_riviere'], loc['nom_site_hydro']),
        'SURFACE': loc['taille_bv'], 'RT': 'TU',
        'DEB': START.strftime('%d/%m/%Y %H:%M'),
        'FIN': END.strftime('%d/%m/%Y %H:%M'),
        'ST': 1, 'SR': 1, 'AT': 0, 'AR': 0,
        'HOR1': '00J00H00M', 'HOR2': '01J00H00M',
        'SC1': sc1, 'SC2': sc2, 'SV1': sv1, 'SV2': sv2, 'SV3': sv3,
        'NJ': 4, 'HC': '01J00H00M', 'EC': 6, 'ECART': ecart, 'INC': 0,
    }
    r20 = GRP_Run20(**r20)
    runs20.append(r20)

#    break

runs20.write()
print('   -> Ecriture du fichier v2020 : {}'
      ''.format(os.path.relpath(CFG, BDD)))
