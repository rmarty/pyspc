#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_grpCal_wav:

.. role:: blue

.. role:: boldblue

Calcul des lames d'eau des bassins GRP (_grpCal_wav.py)
-------------------------------------------------------

Description
+++++++++++

Calcul de lames d'eau de bassin à partir de fichiers de pluie et du fichier bassin de GRP

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES DONNEES


:boldblue:`BDD_DIRNAME` : :blue:`Répertoire des données GRP`

:boldblue:`BV_DIRNAME` : :blue:`Répertoire des fichiers bassin`

:boldblue:`DATA_P_DIRNAME` : :blue:`Répertoire des données de pluie`

:boldblue:`DATA_AP_DIRNAME` : :blue:`Répertoire de sortie`

:boldblue:`DATA_AP_SUFFIX` : :blue:`Suffixe des séries de pluie`

"""

# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import os
import os.path
import sys
import subprocess
import glob

from pyspc.model.grp16 import GRP_Basin
import pyspc.core.exception as _exception

# ------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   BDD_DIRNAME     : Répertoire des données GRP
#   BV_DIRNAME      : Répertoire des fichiers bassin
#   DATA_P_DIRNAME  : Répertoire des données de pluie
#   DATA_AP_DIRNAME : Répertoire de sortie
#   DATA_AP_SUFFIX  : Suffixe des séries de pluie
# ------------------------------------
BDD_DIRNAME = r'D:\2-grp-3.3\20170523_BDD'
BV_DIRNAME = os.path.join(BDD_DIRNAME, 'Bassins')
DATA_P_DIRNAME = os.path.join(BDD_DIRNAME, 'BDD_P')
DATA_AP_DIRNAME = os.path.join(BDD_DIRNAME, 'BDD_P_LAME')
DATA_AP_SUFFIX = '_thiessen'
# ------------------------------------
# DEPENDANCES
# ------------------------------------

# *****************************************
#               SCRIPT
# *****************************************
# ------------------------------------
# BOUCLE SUR LES BASSINS CONCERNES
# ------------------------------------
print(' -- Calcul de pluie de bassin')
files = glob.glob(BV_DIRNAME + os.sep + "K*.dat")
for file in files:
    # Initialisation
    listP = []
    listW = []
    # Détermination du code de la station DEBIT
#    print("   + Fichier Bassin : {0}".format(file))
    station_Q = os.path.splitext(os.path.basename(file))[0]
#    print("     - Station DEBIT '{0}'".format(station_Q))
    # Lecture du fichier Kxxx.dat:
    # extraction des stations E, P et leurs poids respectifs
    bassin_info = GRP_Basin(filename=file)
    bassin_info.read(version=2)
    if 'P' in bassin_info["data"]:
        if bassin_info["data"]['P'] is not None:
            for station_P in bassin_info["data"]['P']:
                listP.append(station_P + "\n")
                listW.append(
                    str(bassin_info["data"]['P'][station_P]['w']) + "\n")
#                print("     - Station PRCP  '{0}' : {1}"
#                      "".format(
#                          station_P,
#                          bassin_info["data"]['P'][station_P]))
# ------------------------------------
# ECRITURE DES LISTES STATION/POIDS
# ------------------------------------
            listP_filename = os.path.join(
                DATA_AP_DIRNAME,
                station_Q + '_listP.txt'
            )
            listW_filename = os.path.join(
                DATA_AP_DIRNAME,
                station_Q + '_listW.txt'
            )
            with open(listP_filename, 'w', encoding='utf-8') as fp:
                with open(listW_filename, 'w', encoding='utf-8') as fw:
                    # Ecriture des listes
                    fp.writelines(listP)
                    fw.writelines(listW)
# ------------------------------------
# CALCULER LA LAME D'EAU 'GRP'
# ------------------------------------
            if os.path.exists(listP_filename) and \
                    os.path.exists(listW_filename):
                processArgs = [
                    'python',
                    os.environ['PYSPC_BIN'] + '\\'
                    'grp2grp.py',
                    '-I', DATA_P_DIRNAME,
                    '-l', listP_filename,
                    '-n', 'PH',
                    '-O', DATA_AP_DIRNAME,
                    '-M', 'arp', station_Q + DATA_AP_SUFFIX, listW_filename
                    ]
#                processArgs.append('-v')
#                print(" ".join(processArgs))
                processRun = subprocess.Popen(
                    processArgs, universal_newlines=True,
                    shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
                processRun.wait()
                dataFileName = os.path.join(
                    DATA_AP_DIRNAME,
                    station_Q + DATA_AP_SUFFIX + '_P.txt'
                )
                if os.path.exists(dataFileName):
                    processArgs = [
                        'python',
                        os.environ['PYSPC_BIN'] + '\\'
                        'grpInfo.py',
                        '-I', os.path.dirname(dataFileName),
                        '-s', station_Q + DATA_AP_SUFFIX,
                        '-n', 'PH',
                        '-M', 'missing_value'
                        ]
#                    print(processArgs)
                    processRun = subprocess.Popen(
                        processArgs, universal_newlines=True,
                        shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
                    processRun.wait()
            else:
                _exception.Warning(
                    __name__,
                    "le fichier station '{0}' et/ou le fichier pondération"
                    " '{1}' est/sont manquant(s)"
                    "".format(listP_filename, listW_filename))
            # sys.exit(0)
