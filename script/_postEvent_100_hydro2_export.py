#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_100_hydro2_export:

.. role:: blue

.. role:: boldblue

Définir les procédures d'export de données HYDRO-2 (_postEvent_100_hydro2_export.py)
------------------------------------------------------------------------------------

Description
+++++++++++

Définir les procédures d'export de données HYDRO-2

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`HYDRO2_TYPE` : :blue:`Liste des types de données HYDRO-2 à exporter`

"""

# *****************************************************************************
#               PARAMETRES
# *****************************************************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt, timedelta as td
import os.path
import subprocess
import sys
from pyspc import Config

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\_TEST\config\event.txt'
# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   HYDRO2_TYPE     : Liste des types de données HYDRO
# ------------------------------------------------------------------------
HYDRO2_TYPE = ['QTVAR', 'H-TEMPS']

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
})
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(
    filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()

# ------------------------------------
# --- PARAMETRE SUBPROCESS
# ------------------------------------
DST_DIRNAME = os.path.join(HOME, 'obs', 'hydro2', 'exports')
if not os.path.exists(DST_DIRNAME):
    print('Création du répertoire {}'
          ''.format(os.path.relpath(DST_DIRNAME, start=HOME)))
    os.makedirs(DST_DIRNAME)
START = EVENT['event']['firstdt']
END = EVENT['event']['lastdt'] + td(hours=EVENT['event']['maxltime'])
CODES = []
for loc in LOCS_HYDRO:
    if 'code_hydro2' not in LOCS_HYDRO[loc]:
        continue
    CODES.extend(LOCS_HYDRO[loc]['code_hydro2'].split(';'))

LIST_FILENAME = os.path.join(
    HOME, 'config', os.path.basename(__file__).replace('.py', '.txt'))
with open(LIST_FILENAME, 'w', encoding='utf-8', newline='\n') as f:
    for c in CODES:
        f.write('{}\n'.format(c))

# ------------------------------------
# --- LANCEMENT SUBPROCESS
# ------------------------------------
for t in HYDRO2_TYPE:
    d = os.path.join(os.path.dirname(DST_DIRNAME), t)
    if not os.path.exists(d):
        print('Création du répertoire {}'
              ''.format(os.path.relpath(d, start=HOME)))
        os.makedirs(d)
    processArgs = [
        'python',
        os.environ['PYSPC_BIN'] + '\\'
        'hydroExport.py',
        '-t', t,
        '-l', LIST_FILENAME,
        '-O', DST_DIRNAME,
        '-F', START.strftime("%Y%m%d%H"),
        '-L', END.strftime("%Y%m%d%H")
    ]
    processRun = subprocess.Popen(
        processArgs, universal_newlines=True,
        shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
    processRun.wait()
