#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_150_subreach_stats:

.. role:: blue

.. role:: boldblue

Synthèse des pointes de crue par sous-tronçon (_postEvent_150_subreach_stats.py)
--------------------------------------------------------------------------------

Description
+++++++++++

Synthèse des pointes de crue par sous-tronçon (valeur, date et temps de retour).

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

"""

# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt
import os.path
from pyspc import Config, read_PyspcFile  # , read_Sacha
from pyspc.metadata.statistics import set_tr

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'


# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
HOME_CSV = os.path.join(HOME, 'obs', 'stats')
if not os.path.exists(HOME_CSV):
    os.makedirs(HOME_CSV)
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'subreaches'): lambda x: os.path.join(
        os.path.dirname(EVENT_FILENAME), x),
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
})
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION SUBREACHES
# ------------------------------------
SUBREACHES = Config(filename=EVENT['event']['subreaches'])
SUBREACHES.read()
SUBREACHES.convert({
    (s, 'sites_hydro'): Config.to_listofstr for s in SUBREACHES
})
SUBREACHES.convert({
    (s, 'stations_hydro'): Config.to_listofstr for s in SUBREACHES
})
# ------------------------------------
# --- STATISTIQUES SITES HYDRO
# ------------------------------------
dbase_filename = os.path.join(
    HOME, 'obs', '{}.mdb'.format(EVENT['event']['name']))

for s in SUBREACHES:
    # Lecture des données
#    series = read_Sacha(
#        filename=dbase_filename,
#        codes=SUBREACHES[s]['sites_hydro'],
#        realtime=False,
#        first_dt=EVENT['event']['firstdt'],
#        last_dt=EVENT['event']['lastdt'],
#        spc_varname='QH',
#        prcp_src=None,
#        hydro_version='hydro3',
#        warning=True
#    )
    series = read_PyspcFile(
        dirname=os.path.join(HOME, 'obs', 'phyc', 'QH'),
        stations=SUBREACHES[s]['sites_hydro'], varnames='QH')
    maxis = series.max()
    # Fichier csv
    header = [
        'Code Site',
        'Nom',
        'Rivière',
        'Qmax (m3/s)',
        'Date max',
        'Temps de retour'
    ]
    csv_filename = os.path.join(
        HOME_CSV, '{}_QH.csv'.format(SUBREACHES[s]['name']))
    print('Export des maxis QH {}'
          ''.format(os.path.relpath(csv_filename, start=HOME)))
    with open(csv_filename, 'w', encoding='utf-8', newline='\n') as f:
        f.write(';'.join(header))
        f.write('\n')
        for q in sorted(series.keys()):
            loc = q[0]
            code = LOCS_HYDRO[loc]['code']
            name = LOCS_HYDRO[loc]['name']
            river = LOCS_HYDRO[loc]['river']
            qmax = maxis[q][0]
            dtmax = maxis[q][1]
            tr = set_tr(qmax, LOCS_HYDRO[loc], 'stats_t')
            f.write(';'.join([code, name, river]))
            f.write(';{0:.1f}'.format(qmax))
            f.write(';{0}'.format(dtmax.strftime('%Y-%m-%d %Hh')))
            f.write(';{0}'.format(tr))
            f.write('\n')
# ------------------------------------
# --- STATISTIQUES STATIONS HYDRO
# ------------------------------------
    # Lecture des données
#    series = read_Sacha(
#        filename=dbase_filename,
#        codes=SUBREACHES[s]['stations_hydro'],
#        realtime=False,
#        first_dt=EVENT['event']['firstdt'],
#        last_dt=EVENT['event']['lastdt'],
#        spc_varname='HH',
#        prcp_src=None,
#        hydro_version='hydro3',
#        warning=True
#    )
    series = read_PyspcFile(
        dirname=os.path.join(HOME, 'obs', 'phyc', 'HH'),
        stations=SUBREACHES[s]['stations_hydro'], varnames='HH')
    maxis = series.max()
    # Fichier csv
    header = [
        'Code Site',
        'Nom',
        'Rivière',
        'Hmax (m)',
        'Date max',
    ]
    csv_filename = os.path.join(
        HOME_CSV, '{}_HH.csv'.format(SUBREACHES[s]['name']))
    print('Export des maxis HH {}'
          ''.format(os.path.relpath(csv_filename, start=HOME)))
    with open(csv_filename, 'w', encoding='utf-8', newline='\n') as f:
        f.write(';'.join(header))
        f.write('\n')
        for h in sorted(series.keys()):
            loc = h[0]
            code = LOCS_HYDRO[loc]['code']
            name = LOCS_HYDRO[loc]['name']
            river = LOCS_HYDRO[loc[:8]]['river']
            hmax = maxis[h][0]
            dtmax = maxis[h][1]
            f.write(';'.join([code, name, river]))
            f.write(';{0:.2f}'.format(hmax))
            f.write(';{0}'.format(dtmax.strftime('%Y-%m-%d %Hh')))
            f.write('\n')
