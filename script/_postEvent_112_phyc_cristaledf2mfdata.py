#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_112_phyc_cristaledf2mfdata:

.. role:: blue

.. role:: boldblue

Conversion au format MF des pluvios hors-MF de la PHyC (_postEvent_112_phyc_cristaledf2mfdata.py)
-------------------------------------------------------------------------------------------------

Description
+++++++++++

Conversion au format MF des pluvios hors-MF de la PHyC pour les formatter en vue d'être intégrée dans SACHA

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

"""

# *****************************************************************************
#               PARAMETRES
# *****************************************************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import os.path
import subprocess
import sys
from pyspc import Config

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_METEO = Config(filename=os.path.join(HOME, 'config', 'locs_meteo.txt'))
LOCS_METEO.read()
# ------------------------------------
# --- PARAMETRE SUBPROCESS
# ------------------------------------
LIST_FILENAME = os.path.join(
    HOME, 'config', os.path.basename(__file__).replace('.py', '.txt'))
with open(LIST_FILENAME, 'w', encoding='utf-8', newline='\n') as f:
    for loc in LOCS_METEO:
        if LOCS_METEO[loc]['provider'] != 'MF':
            f.write('{}\n'.format(loc))
# ------------------------------------
# --- LANCEMENT SUBPROCESS
# ------------------------------------
processArgs = [
    'python',
    os.environ['PYSPC_BIN'] + '\\'
    'csv2mf.py',
    '-I', os.path.join(HOME, 'obs', 'phyc', 'PH'),
    '-O', os.path.join(HOME, 'obs', 'phyc', 'PH'),
    '-n', 'PH',
    '-t', 'data',
    '-l', LIST_FILENAME,
    '-w'
]
processRun = subprocess.Popen(
    processArgs, universal_newlines=True,
    shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
processRun.wait()
