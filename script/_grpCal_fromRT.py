#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_grpCal_fromRT:

.. role:: blue

.. role:: boldblue

Extraction de données de GRP TEMPS REEL et conversion au format GRP Calage (_grpCal_fromRT.py)
----------------------------------------------------------------------------------------------

Description
+++++++++++

Paramètres
++++++++++

.. rubric:: Configuration des donnees

:boldblue:`BDD_DIRNAME` : :blue:`Répertoire des données GRP`

:boldblue:`STATIONS_FILENAME` : :blue:`Fichier contenant la liste des stations`

:boldblue:`BV_DIRNAME` : :blue:`Répertoire des fichiers bassin`

:boldblue:`RT_DIRNAME` : :blue:`Répertoire des fichiers Temps Réel`

:boldblue:`DATA_DIRNAME` : :blue:`Répertoires des données, par variable`

:boldblue:`FIRST_DT` : :blue:`Premier pas de temps`

:boldblue:`LAST_DT` : :blue:`Dernier pas de temps`

:boldblue:`VARNAMES` : :blue:`Correspondance des variables GRP <-> SPC`

"""
# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------------------------------------------
# IMPORTS
# ------------------------------------------------------------------------
from datetime import datetime as dt
import os
import os.path

from pyspc import read_GRP16

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   BDD_DIRNAME       : Répertoire des données GRP
#   STATIONS_FILENAME : Fichier contenant la liste des stations
#   BV_DIRNAME        : Répertoire des fichiers bassin
#   RT_DIRNAME        : Répertoire des fichiers Temps Réel
#   DATA_DIRNAME      : Répertoires des données, par variable
#   FIRST_DT          : Premier pas de temps
#   LAST_DT           : Dernier pas de temps
#   VARNAMES          : Correspondance des variables GRP <-> SPC
# ------------------------------------------------------------------------
BDD_DIRNAME = r'D:\2-grp-3.3\20190426_BDD'
RT_DIRNAME = {
    'P': r'S:\SHPEC\DPEC\O_Chaine_operationnelle\PROD\Plateformes_Modeles'
         r'\Grp\Temps_Reel\BD_Pluies',
}
BV_DIRNAME = os.path.join(BDD_DIRNAME, 'Bassins')
DATA_DIRNAME = {
    'P': os.path.join(BDD_DIRNAME, 'BDD_P'),
    'Pexport': os.path.join(BDD_DIRNAME, 'BDD_P', 'CRISTAL_TR'),
}
FIRST_DT = dt(2016, 12, 31, 23)
LAST_DT = dt(2019, 4, 1)
VARNAMES = {
    'P': 'PH',
}
STATIONS_FILENAME = os.path.join(BDD_DIRNAME, 'P_cristal.txt')

# ------------------------------------------------------------------------
# DEPENDANCES
# ------------------------------------------------------------------------
STATIONS = []
with open(STATIONS_FILENAME, 'r', encoding='utf-8') as f:
    for line in f.readlines():
        line = line.strip('\n')
        STATIONS.append(line)
STATIONS = sorted(STATIONS)

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
print("-- Extraction de données de GRP TEMPS REEL et conversion au format "
      "GRP Calage")
for station in STATIONS:
    print("   + Extraction pour {}".format(station))
    for varname, spc_varname in VARNAMES.items():
        if spc_varname == 'PH':
            filename = os.path.join(RT_DIRNAME[varname], station, 'PV_10A.DAT')
        elif spc_varname == 'QH':
            filename = os.path.join(RT_DIRNAME[varname], station, 'QV_10A.DAT')
        elif spc_varname == 'TH':
            filename = os.path.join(RT_DIRNAME[varname], station, 'TV_10A.DAT')
        else:
            continue
        if not os.path.exists(filename):
            continue
        print("     + Grandeur {}".format(spc_varname))
        series = read_GRP16(
            datatype='grp16_rt_archive',
            dirname=os.path.dirname(filename),
            filename=os.path.basename(filename)
        )
        series.between_dates(
            first_dtime=FIRST_DT,
            last_dtime=LAST_DT,
            inplace=True
        )
        series.to_GRP_Data(dirname=DATA_DIRNAME[varname+'export'])

        # break
    # if station == '03127005':
        # break
