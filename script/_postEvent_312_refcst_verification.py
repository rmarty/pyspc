#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_312_refcst_verification:

.. role:: blue

.. role:: boldblue

Calculer les critères de vérification des prévisions rejouées (_postEvent_312_refcst_verification.py)
-----------------------------------------------------------------------------------------------------

Description
+++++++++++

Calculer les critères de vérification des prévisions rejouées

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des figures

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`REFORCAST_MODEL` : :blue:`Nom du modèle (sous-répertoire de fcst/)`

:boldblue:`FIRST_DTIME` : :blue:`Première date des runtimes. None si définie par event`

:boldblue:`LAST_DTIME` : :blue:`Dernière date des runtimes. None si définie par event`

.. rubric:: Configuration des statistiques

:boldblue:`SCORES_ABS` : :blue:`Calcul en valeur absolue ? (T/F) sous forme de liste`

:boldblue:`SCORES_REL` : :blue:`Calcul en valeur relative ? (T/F) sous forme de liste`

:boldblue:`SCORES_DELTA` : :blue:`Pas, en fréquence, des statistiques d'erreur`

:boldblue:`SCORES_FREQS` : :blue:`Fréquences des statistiques d'erreur`

"""

# *****************************************s
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import collections
from datetime import datetime as dt
import glob
import os.path
from pyspc import Config, read_PyspcFile, Series

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# -----------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES SERIES
#   REFORCAST_MODEL  : Nom du modèle (sous-répertoire de fcst/)
#   SPC_VARNAME      : Variable
#   FIRST_DTIME      : Première date des runtimes. None si définie par event
#   LAST_DTIME       : Dernière date des runtimes. None si définie par event
# -----------------------------------------------------------------------------
REFORECAST_MODEL = 'grp'
SPC_VARNAME = 'QH'
FIRST_DTIME = dt(2020, 6, 10)  # None
LAST_DTIME = dt(2020, 6, 16)  # None
# -----------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES STATISTIQUES
#   SCORES_ABS       : Calcul en valeur absolue ? (T/F) sous forme de liste
#   SCORES_REL       : Calcul en valeur relative ? (T/F) sous forme de liste
#   SCORES_DELTA     : Pas, en fréquence, des statistiques d'erreur
#   SCORES_FREQS     : Fréquences des statistiques d'erreur
# -----------------------------------------------------------------------------
SCORES_ABS = [False, True]
SCORES_REL = [False, True]
SCORES_ABSREL = [(a, r) for a in SCORES_ABS for r in SCORES_REL]
SCORES_DELTA = 5
SCORES_FREQS = [x / 100 for x in range(SCORES_DELTA, 100, SCORES_DELTA)]

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
})
if FIRST_DTIME is None:
    FIRST_DTIME = EVENT['event']['firstdt']
if LAST_DTIME is None:
    LAST_DTIME = EVENT['event']['lastdt']
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
# ------------------------------------
# --- LECTURE DES DONNEES
# ------------------------------------
fcst_filenames = glob.glob(
    os.path.join(
        HOME, 'fcst', REFORECAST_MODEL,
        '*', '*', '*',  # année / mois / jour
        'grp16rtfcstdiff_*_{}.txt'.format(SPC_VARNAME)
    )
)
FCST_SERIES = Series(datatype='fcst', name='grp16')
for f in fcst_filenames:
    tmp_series = read_PyspcFile(filename=f)
    r = tmp_series.meta[0][0]
    if r < FIRST_DTIME or r > LAST_DTIME:
        continue
    print(os.path.relpath(f, HOME))
    FCST_SERIES.extend(tmp_series)
# ------------------------------------
# --- LECTURE OBS
# ------------------------------------
for loc in LOCS_HYDRO:
    if len(loc) != 8:
        continue
    if not loc.startswith('K055'):
        continue
    # Lecture des données
    obs = read_PyspcFile(
        dirname=os.path.join(HOME, 'obs', 'phyc', SPC_VARNAME),
        stations=loc, varnames=SPC_VARNAME)
    if obs is None or len(obs) == 0:
        continue
    serie_ref = obs[(loc, SPC_VARNAME, None)]
# ------------------------------------
# --- LECTURE PRV - REFORECAST
# ------------------------------------
    prv_series = collections.OrderedDict()
    for k, s in FCST_SERIES.items():
        if (k[0] != loc or k[1] != SPC_VARNAME or not isinstance(k[2], tuple)
                or k[2][0] < FIRST_DTIME or k[2][0] > LAST_DTIME):
            continue
        # Conservation de la série déterministe uniquement
        if k[2][-1] is not None:
            continue
        idserie = '{}-00'.format(k[2][1])
        prv_series.setdefault(idserie, Series(datatype='fcst'))
        s.code = k[0]
        prv_series[idserie].add(code=k[0], serie=s, meta=k[2])
    lens = list({len(prv_series[i]) for i in prv_series})
    if not prv_series or (len(lens) == 1 and lens[0] == 0):
        continue
# ------------------------------------
# --- CALCUL DES ERREURS DE PREVISION
# ------------------------------------
    stat_dirname = os.path.join(HOME, 'verif', REFORECAST_MODEL, loc)
    if not os.path.exists(stat_dirname):
        os.makedirs(stat_dirname)
    for idserie in prv_series:
        for s in SCORES_ABSREL:
            stat_filename = '{station}_{idserie}'.format(
                station=loc, idserie=idserie)
            if s[0] and s[1]:
                stat_filename += '_absrel'
            elif s[0]:
                stat_filename += '_abs'
            elif s[1]:
                stat_filename += '_rel'
            stat_filename += '.csv'
            stat_filename = os.path.join(stat_dirname, stat_filename)
            print('Fichier verif : {}'
                  ''.format(os.path.relpath(stat_filename, start=HOME)))
            # Calcul
            err_sample, err_stats = prv_series[idserie].errors(
                ref=serie_ref, absolute=s[0], relative=s[1])
            # Export CSV
            err_stats.to_csv(
                stat_filename, sep=';', float_format='%.3f',
                encoding='utf-8')

#    break  # for loc in LOCS_HYDRO:
