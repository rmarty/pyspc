#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_221_mohys_run:

.. role:: blue

.. role:: boldblue

Lancement de MOHYS en PREVISION et conversion en csv (_postEvent_221_mohys_run.py)
----------------------------------------------------------------------------------

Description
+++++++++++

Lancement de MOHYS en PREVISION et conversion en csv

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration de MOHYS

:boldblue:`MOH_DIRNAME` : :blue:`Répertoire de MOHYS`

.. rubric:: Configuration des fichiers csv

:boldblue:`CSV_DIRNAME` : :blue:`Répertoire des données au format csv`

:boldblue:`PRV_DIRNAME` : :blue:`Répertoire des données au format prv`

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`DELTA_DT` : :blue:`Intervalle entre 2 pas de temps`

:boldblue:`LTIME_DT` : :blue:`Horizon maximal des prévisions`

"""

# *****************************************************************************
#               PARAMETRES
# *****************************************************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt, timedelta as td
import os.path
import shutil
import subprocess
import sys
from pyspc import Config, Series
from pyspc.io.prv import read_prv

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# ------------------------------------
# PARAMETRES : CONFIGURATION DE GRP
#   MOH_DIRNAME         : Répertoire de MOHYS
# -----------------------------------------------------------------------------
MOH_DIRNAME = r'D:\Utilisateurs\renaud.marty\Documents\3-bitbucket\pybarrage'\
    r'\Mohys'
MOH_CFG_FILENAME = os.path.join(MOH_DIRNAME, 'Liste_Modeles.cfg')
MOH_EXE_FILENAME = os.path.join(MOH_DIRNAME, 'Mohys.py')
MOH_IN = os.path.join(MOH_DIRNAME, '_in')
MOH_OUT = os.path.join(MOH_DIRNAME, '_out')
# -----------------------------------------------------------------------------
# PARAMETRES : ARCHIVAGE PRV, CSV et PNG
#   CSV_DIRNAME         : Répertoire des données au format csv
#   PRV_DIRNAME         : Répertoire des données au format prv
#   SPC_VARNAME         : Variable
#   DELTA_DT            : Intervalle entre 2 pas de temps
#   LTIME_DT            : Horizon maximal des prévisions
# -----------------------------------------------------------------------------
CSV_DIRNAME = os.path.join('fcst', 'moh')
PRV_DIRNAME = os.path.join('fcst', 'prv')
SPC_VARNAME = 'QH'
DELTA_DT = td(hours=3)  # Intervalle entre 2 prévisions
LTIME_DT = td(hours=120)  # Horizon de la prévision

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
os.chdir(HOME)
OBS_DIRNAME = os.path.join(HOME, 'obs', 'phyc', SPC_VARNAME)
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
})
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(
    filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION Liste_Modeles
# ------------------------------------
MOH_MODELS = Config(filename=MOH_CFG_FILENAME)
MOH_MODELS.read()
for m in MOH_MODELS:
    f = os.path.join(MOH_DIRNAME, m, 'stations.txt')
    c = Config.from_multitxt(filenames=[f])
    test = [i in LOCS_HYDRO for i in c['stations']['in'].split(';')]
    test.extend([o in LOCS_HYDRO for o in c['stations']['out'].split(';')])
    if all(test):
        MOH_MODELS[m]['in'] = c['stations']['in'].split(';')
        MOH_MODELS[m]['out'] = c['stations']['out'].split(';')
# ------------------------------------
# --- LANCEMENT SUBPROCESS
# ------------------------------------
RUNTIMES = [EVENT['event']['firstdt'] + kt * DELTA_DT
            for kt in range(
                int((EVENT['event']['lastdt'] - EVENT['event']['firstdt'])
                    / DELTA_DT + 1))]
for m in MOH_MODELS:
    if 'in' not in MOH_MODELS[m] or m.startswith('LVD'):
        continue
    print('Simulation du modèle {} ({})'.format(m, MOH_MODELS[m]['name']))
# ------------------------------------
# --- INFO RECALAGE
# ------------------------------------
    pproc = {}
    for k in MOH_MODELS[m]:
        if k not in LOCS_HYDRO:
            continue
        for x in MOH_MODELS[m][k].split(','):
            x2 = int(x)
            if x2 == 999:
                pproc.setdefault('ARR', x2)
            else:
                pproc.setdefault('PRO', x2)
    print(' -> Recalage : {}'.format(pproc))
# ------------------------------------
# --- DONNEES ENTREE
# ------------------------------------
    print(' -> Inputs')
    LIST_FILENAME = os.path.join(
        HOME, 'config', os.path.basename(__file__).replace('.py', '.txt'))
    with open(LIST_FILENAME, 'w', encoding='utf-8', newline='\n') as f:
        for loc in MOH_MODELS[m]['in']:
            f.write('{}\n'.format(loc))
        for loc in MOH_MODELS[m]['out']:
            f.write('{}\n'.format(loc))
    processArgs = [
        'python',
        os.environ['PYSPC_BIN'] + '\\'
        'csv2grpRT.py',
        '-I', OBS_DIRNAME,
        '-O', MOH_IN,
        '-n', SPC_VARNAME,
        '-t', 'grp16_rt_data',
        '-l', LIST_FILENAME,
        '-w'
    ]
    processRun = subprocess.Popen(
        processArgs, universal_newlines=True,
        shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
    processRun.wait()
    in_src = os.path.join(MOH_IN, 'Debit__postEvent_220_mohys_sim.txt')
    in_dst = os.path.join(MOH_IN, 'Debit.txt')
    if not os.path.exists(in_src):
        print('!!! Fichier obs inexistant !!!')
        continue
    shutil.copy2(in_src, in_dst)
# ------------------------------------
# --- CALCUL MOHYS
# ------------------------------------
    for runtime in RUNTIMES:
        print(' -> Runtime {} : '
              ''.format(runtime.strftime('%Y-%m-%d %H:%M')), end='')
        start = max(EVENT['event']['firstdt'], runtime - LTIME_DT)
        end = min(EVENT['event']['lastdt'], runtime + LTIME_DT)
        print('execution', end=', ')
        BAT_FILENAME = os.path.join(
            HOME, 'config', os.path.basename(__file__).replace('.py', '.bat'))
        with open(BAT_FILENAME, 'w', encoding='utf-8', newline='\n') as f:
            f.write('@echo off \n')
            f.write('call activate py2_qt5 \n')
            f.write('pushd {} \n'.format(os.path.dirname(MOH_EXE_FILENAME)))
            cmd = 'python {exe} -n {mod} -F {start} -L {end} -r {run}'\
                ' -s mohys'.format(
                    exe=os.path.basename(MOH_EXE_FILENAME),
                    mod=m,
                    start=start.strftime('%Y%m%d%H'),
                    end=end.strftime('%Y%m%d%H'),
                    run=runtime.strftime('%Y%m%d%H')
                )
            if pproc:
                for n, v in pproc.items():
                    cmd += ' -g {0:d} {1:3s} '.format(v, n)
            f.write(cmd)
            f.write('\n')
            f.write('popd \n')
            f.write('\n')
        processRun = subprocess.Popen(
            BAT_FILENAME, universal_newlines=True, cwd=MOH_DIRNAME,
            shell=True, stdout=subprocess.DEVNULL, stderr=sys.stderr)  # nosec
        processRun.wait()
# ------------------------------------
# --- DONNEES SORTIE
# ------------------------------------
        print('outputs', end='=')
        current_csv_dirname = os.path.join(
            CSV_DIRNAME,
            runtime.strftime('%Y'),
            runtime.strftime('%m'),
            runtime.strftime('%d')
        )
        if not os.path.exists(current_csv_dirname):
            os.makedirs(current_csv_dirname)
        current_prv_dirname = os.path.join(
            PRV_DIRNAME,
            runtime.strftime('%Y'),
            runtime.strftime('%m'),
            runtime.strftime('%d')
        )
        if not os.path.exists(current_prv_dirname):
            os.makedirs(current_prv_dirname)

        src = os.path.join(MOH_OUT, 'MOH_B_{r}_{m}.prv'.format(
            m=m, r=runtime.strftime('%Y%m%d_%H00')
        ))
        shutil.copy2(src, current_prv_dirname)
        print(os.path.basename(src), end=',')

        prv_series = read_prv(filename=src, datatype='otamin16_trend')
        prv_series.between_dates(
            first_dtime=runtime, last_dtime=end, inplace=True)
        series = Series(datatype='fcst', name='Mohys')
        for p in prv_series:
            series.add(
                code=p[0],
                meta=(p[2][0], 'mohys-{}'.format(
                    p[2][2].replace('45hMOHd', '').replace('_-1', '')), None),
                serie=prv_series[p]
            )
        filenames = series.to_GRP_Data(
            dirname=current_csv_dirname, codefrom='series')
        for f in filenames:
            print("{}".format(os.path.relpath(f, start=HOME)), end=',')
        os.remove(src)
        print()
# ------------------------------------
# --- NETTOYAGE
# ------------------------------------
        for x in [in_src, in_dst]:
            try:
                os.remove(x)
            except IOError:
                pass
#        break  # for runtime in RUNTIMES:
#    break  # for m in MOH_MODELS:
