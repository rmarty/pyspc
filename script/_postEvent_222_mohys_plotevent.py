#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_222_mohys_plotevent:

.. role:: blue

.. role:: boldblue

Tracer la simulation événementielle et les prévisions (_postEvent_222_mohys_plotevent.py)
---------------------------------------------------------------------------------------

Description
+++++++++++

Tracer la simulation événementielle et les prévisions MOHYS

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des fichiers csv

:boldblue:`CSV_DIRNAME` : :blue:`Répertoire des prévisions au format csv`

:boldblue:`SPC_VARNAME` : :blue:`Variable`

.. rubric:: Configuration des figures

:boldblue:`INCLUDE_FORECAST` : :blue:`Inclure les prévisions ? (T/F)`

:boldblue:`MAXLTIME_FORECAST` : :blue:`Echéance maximal. Si None, reprend la valeur EVENT`

:boldblue:`COLORS_FORECAST` : :blue:`Liste de couleurs pour les prévisions`

"""

# *****************************************************************************
#               PARAMETRES
# *****************************************************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt, timedelta as td
import glob
import itertools
import matplotlib.pyplot as mplt
import os.path
from pyspc import Config, read_PyspcFile
from pyspc.plotting.config import Config as PlotConfig
from pyspc.core.keyseries import tuple2str


# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\__SCHAPI\REX\Rex-Schapi\config\event.txt'
# -----------------------------------------------------------------------------
# PARAMETRES : ARCHIVAGE CSV
#   CSV_DIRNAME         : Répertoire des données au format csv
#   SPC_VARNAME         : Variable
# -----------------------------------------------------------------------------
CSV_DIRNAME = os.path.join('fcst', 'moh')
SPC_VARNAME = 'QH'
# ------------------------------------
# PARAMETRES : CONFIGURATION DE LA FIGURE
#   INCLUDE_FORECAST    : Inclure les prévisions ? (T/F)
#   MAXLTIME_FORECAST   : Echéance maximal. Si None, reprend la valeur EVENT
#   COLORS_FORECAST     : Liste de couleurs pour les prévisions
# ------------------------------------
INCLUDE_FORECAST = False
MAXLTIME_FORECAST = 24
COLORS_FORECAST = [
    mplt.cm.tab20c(4), mplt.cm.tab20c(5), mplt.cm.tab20c(6), mplt.cm.tab20c(7)]
# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
COLORS_FORECAST = itertools.cycle(COLORS_FORECAST)
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
})
if MAXLTIME_FORECAST is None:
    MAXLTIME_FORECAST = EVENT['event']['maxltime']
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
# ------------------------------------
# --- LECTURE DES DONNEES
# ------------------------------------
for loc in LOCS_HYDRO:
    if INCLUDE_FORECAST:
        raise NotImplementedError
#    fcst_filenames = None
    sim_filenames = glob.glob(
        os.path.join(
            HOME, CSV_DIRNAME,
            '{}*mohys-sim*{}.txt'.format(loc, SPC_VARNAME)
        )
    )
    if not sim_filenames:
        continue
#    FCST_SERIES = None
    SIM_SERIES = read_PyspcFile(filename=sim_filenames[0])
    SERIES = read_PyspcFile(
        dirname=os.path.join(HOME, 'obs', 'phyc', SPC_VARNAME),
        stations=loc, varnames=SPC_VARNAME)
    SERIES.between_dates(
        first_dtime=EVENT['event']['firstdt'],
        last_dtime=EVENT['event']['lastdt']
        + td(hours=EVENT['event']['maxltime']),
        inplace=True
    )
    for k in SIM_SERIES:
        s = SIM_SERIES[k]
        s.code = tuple2str((k[0], k[1], 'sim')).replace(
            '_{}'.format(SPC_VARNAME), '')
        SERIES.add(s)

# ------------------------------------
# --- LECTURE DE LA CONFIGURATION IMAGE
# ------------------------------------
    CFGIMG_FILENAME = os.path.join(
        HOME, 'config', os.path.basename(__file__).replace('.py', '.txt'))
    CFGIMG = PlotConfig(filename=CFGIMG_FILENAME)
    CFGIMG.load()
# ------------------------------------
# --- MAJ DE LA CONFIGURATION IMAGE
# ------------------------------------
    CFGIMG['figure']['dirname'] = os.path.join(HOME, 'fig', 'moh')
    if not os.path.exists(CFGIMG['figure']['dirname']):
        os.makedirs(CFGIMG['figure']['dirname'])
    if INCLUDE_FORECAST:
        CFGIMG['figure']['filename'] = '{}_fcst-{}'.format(
            loc, MAXLTIME_FORECAST)
    else:
        CFGIMG['figure']['filename'] = loc
    CFGIMG['figure']['title'] = '{}\n{} ({}, {})'.format(
        EVENT['event']['title'], LOCS_HYDRO[loc]['name'],
        LOCS_HYDRO[loc]['river'], loc)
    src_s = [s for s in CFGIMG if s.startswith('SITE')]
    for s in src_s:
        dst_s = s.replace('SITE', loc)
        CFGIMG.setdefault(dst_s, {})
        for o in CFGIMG[s]:
            try:
                CFGIMG[dst_s][o] = CFGIMG[s][o].replace('SITE', loc)\
                    .replace('NAME', LOCS_HYDRO[loc]['name'])
            except AttributeError:
                CFGIMG[dst_s][o] = CFGIMG[s][o]
# ------------------------------------
# --- CONFIGURATION DES COURBES DES PREVISIONS
# ------------------------------------
    if INCLUDE_FORECAST:
        pass  # voir 212 plot grp
# ------------------------------------
# --- MAJ DE LA CONFIGURATION IMAGE AVEC VALEURS PAR DEFAUT
# ------------------------------------
    for s in CFGIMG:
        if s in ['figure', 'defaut']:
            continue
        for o in CFGIMG['defaut']:
            CFGIMG[s].setdefault(o, CFGIMG['defaut'][o])
# ------------------------------------
# --- CREATION DE L'IMAGE
# ------------------------------------
    filename = SERIES.plot_series(plottype='hydro', config=CFGIMG)
    print('Création de la figure {}'
          ''.format(os.path.relpath(filename, start=HOME)))

#    break
