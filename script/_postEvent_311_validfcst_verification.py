#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_311_validfcst_verification:

.. role:: blue

.. role:: boldblue

Calculer les critères de vérification des prévisions validées (_postEvent_311_validfcst_verification.py)
--------------------------------------------------------------------------------------------------------

Description
+++++++++++

Calculer les critères de vérification des prévisions validées

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des figures

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`SKIP_TREND` : :blue:`Préfixe de la tendance validée à ignorer (mode auto)`

:boldblue:`FIRST_DTIME` : :blue:`Première date des runtimes. None si définie par event`

:boldblue:`LAST_DTIME` : :blue:`Dernière date des runtimes. None si définie par event`

.. rubric:: Configuration des statistiques

:boldblue:`SCORES_ABS` : :blue:`Calcul en valeur absolue ? (T/F) sous forme de liste`

:boldblue:`SCORES_REL` : :blue:`Calcul en valeur relative ? (T/F) sous forme de liste`

:boldblue:`SCORES_DELTA` : :blue:`Pas, en fréquence, des statistiques d'erreur`

:boldblue:`SCORES_FREQS` : :blue:`Fréquences des statistiques d'erreur`

"""

# *****************************************s
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import collections
from datetime import datetime as dt
import glob
import os.path
from pyspc import Config, read_PyspcFile, read_Prevision19, Series

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# -----------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES SERIES
#   SPC_VARNAME      : Variable
#   SKIP_TREND       : Préfixe de la tendance validée à ignorer (mode auto)
#   FIRST_DTIME      : Première date des runtimes. None si définie par event
#   LAST_DTIME       : Dernière date des runtimes. None si définie par event
# -----------------------------------------------------------------------------
SPC_VARNAME = 'QH'
SKIP_TREND = 'auto'
FIRST_DTIME = dt(2020, 6, 10)  # None
LAST_DTIME = dt(2020, 6, 16)  # None
# -----------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES STATISTIQUES
#   IDSERIES         : Table de regroupement des séries par tendance
#   SCORES_ABS       : Calcul en valeur absolue ? (T/F) sous forme de liste
#   SCORES_REL       : Calcul en valeur relative ? (T/F) sous forme de liste
#   SCORES_DELTA     : Pas, en fréquence, des statistiques d'erreur
#   SCORES_FREQS     : Fréquences des statistiques d'erreur
# -----------------------------------------------------------------------------
IDSERIES = {
    'expresso': 'valid-00',
    'pilote': 'valid-00',
    'expresso10': 'valid-10',
    'pilote10': 'valid-10',
    'expresso50': 'valid-50',
    'pilote50': 'valid-50',
    'expresso90': 'valid-90',
    'pilote90': 'valid-90',
}
#SCORES_ABS = [False, True]
#SCORES_REL = [False, True]
SCORES_ABS = [False]
SCORES_REL = [False]
SCORES_ABSREL = [(a, r) for a in SCORES_ABS for r in SCORES_REL]
SCORES_DELTA = 5
SCORES_FREQS = [x / 100 for x in range(SCORES_DELTA, 100, SCORES_DELTA)]

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'subreaches'): lambda x: os.path.join(
        os.path.dirname(EVENT_FILENAME), x),
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
})
if FIRST_DTIME is None:
    FIRST_DTIME = EVENT['event']['firstdt']
if LAST_DTIME is None:
    LAST_DTIME = EVENT['event']['lastdt']
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
# ------------------------------------
# --- BASES DE PREVISION
# ------------------------------------
PRV_FILENAMES = glob.glob(os.path.join(HOME, 'fcst', 'oper', '*.mdb'))
# ------------------------------------
# --- LECTURE OBS
# ------------------------------------
for loc in LOCS_HYDRO:
    if len(loc) != 8:
        continue
    if not loc.startswith('K055'):
        continue
    # Lecture des données
    obs = read_PyspcFile(
        dirname=os.path.join(HOME, 'obs', 'phyc', SPC_VARNAME),
        stations=loc, varnames=SPC_VARNAME)
    if obs is None or len(obs) == 0:
        continue
    serie_ref = obs[(loc, SPC_VARNAME, None)]
# ------------------------------------
# --- LECTURE PRV
# ------------------------------------
    prv_series = collections.OrderedDict()
    for prv_filename in PRV_FILENAMES:
        prv = read_Prevision19(filename=prv_filename, codes=[loc],
                               first_dt=FIRST_DTIME, last_dt=LAST_DTIME,
                               valid=True, released=False, warning=False)
        if prv is None or len(prv) == 0:
            continue
# ------------------------------------
# --- SELECTION DES SERIES PAR TENDANCE
# ------------------------------------
        # Prv validées
        for k in sorted(list(prv.keys())):
            if k[0] != loc or k[1] != SPC_VARNAME or k[2][1] == SKIP_TREND:
                continue
            idserie = IDSERIES[k[2][1] + k[2][-1]]
            prv_series.setdefault(idserie, Series(datatype='fcst'))
            prv[k].code = k[0]
            prv_series[idserie].add(code=k[0], serie=prv[k], meta=k[2])
    lens = list({len(prv_series[i]) for i in prv_series})
    if not prv_series or (len(lens) == 1 and lens[0] == 0):
        continue
# ------------------------------------
# --- CALCUL DES ERREURS DE PREVISION
# ------------------------------------
    stat_dirname = os.path.join(HOME, 'verif', 'oper', loc)
    if not os.path.exists(stat_dirname):
        os.makedirs(stat_dirname)
    for idserie in prv_series:
        for s in SCORES_ABSREL:
            stat_filename = '{station}_{idserie}'.format(
                station=loc, idserie=idserie)
            if s[0] and s[1]:
                stat_filename += '_absrel'
            elif s[0]:
                stat_filename += '_abs'
            elif s[1]:
                stat_filename += '_rel'
            stat_filename += '.csv'
            stat_filename = os.path.join(stat_dirname, stat_filename)
            print('Fichier verif : {}'
                  ''.format(os.path.relpath(stat_filename, start=HOME)))
            # Calcul
            err_sample, err_stats = prv_series[idserie].errors(
                ref=serie_ref, absolute=s[0], relative=s[1])
            # Export CSV
            err_stats.to_csv(
                stat_filename, sep=';', float_format='%.3f',
                encoding='utf-8')

#    break  # for loc in LOCS_HYDRO:
