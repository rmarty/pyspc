#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_120_lamedo_download:

.. role:: blue

.. role:: boldblue

Téléchargement des données BdImages (_postEvent_120_lamedo_download.py)
-----------------------------------------------------------------------

Description
+++++++++++

Téléchargement des données BdImages

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

Fichier de configuration
++++++++++++++++++++++++

.. rubric:: Fichier de configuration des images

.. code-block:: cfg

   [obs]
   image = antilope;j1;rr
   stats = standard
   precision = forte

"""


# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt, timedelta as td
import os.path
import subprocess
import sys
from pyspc import Config

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
    ('event', 'zap'): Config.to_listofstr,
})
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()

LAMEDO = Config(filename=os.path.join(HOME, 'config', 'lamedo.txt'))
LAMEDO.read()
LAMEDO.convert({('obs', 'image'): Config.to_listofstr})
# ------------------------------------
# --- PARAMETRE SUBPROCESS
# ------------------------------------
START = EVENT['event']['firstdt']
END = EVENT['event']['lastdt'] + td(hours=EVENT['event']['maxltime'])

try:
    STATS = LAMEDO['obs']['stats']
except KeyError:
    STATS = None
else:
    if not STATS:
        STATS = None
try:
    PRECISION = LAMEDO['obs']['precision']
except KeyError:
    PRECISION = None
else:
    if not PRECISION:
        PRECISION = None
BDI_TYPE = LAMEDO['obs']['image'][0]
BDI_STYPE = LAMEDO['obs']['image'][1]
BDI_BAND = LAMEDO['obs']['image'][2]

# ------------------------------------
# --- EXPORT LAMEDO - BASSIN
# ------------------------------------
v = 'PH'
d = os.path.join(HOME, 'obs', 'lamedo', v)
if not os.path.exists(d):
    print('Création du répertoire {}'
          ''.format(os.path.relpath(d, start=HOME)))
    os.makedirs(d)
print('Téléchargement des données {}'.format(v))
for loc in LOCS_HYDRO:
    if len(loc) != 8:
        continue
    if 'bnbv' not in LOCS_HYDRO[loc]:
        continue
    s = LOCS_HYDRO[loc]['bnbv'].replace('Loire_', 'LO')
    processArgs = [
        'python',
        os.path.join(os.environ['PYSPC_BIN'], 'bdimage2xml.py'),
#        '-c', LAMEDO.filename,
        '-F', START.strftime('%Y%m%d%H'),
        '-L', END.strftime('%Y%m%d%H'),
        '-n', v,
        '-O', d,
        '-r', dt.utcnow().strftime('%Y%m%d%H00'),
        '-S', s,
        '-t', BDI_TYPE, BDI_STYPE, BDI_BAND
    ]
    if STATS is not None:
        processArgs.extend(['-U', 'stats', STATS])
    if PRECISION is not None:
        processArgs.extend(['-U', 'precision', PRECISION])
    processRun = subprocess.Popen(
        processArgs, universal_newlines=True,
        shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
    processRun.wait()

# ------------------------------------
# --- EXPORT LAMEDO - ZONAGE AP
# ------------------------------------
v = 'PJ'
d = os.path.join(HOME, 'obs', 'lamedo', v)
if not os.path.exists(d):
    print('Création du répertoire {}'
          ''.format(os.path.relpath(d, start=HOME)))
    os.makedirs(d)
print('Téléchargement des données {}'.format(v))
for ap in EVENT['event']['zap']:
    s = 'BP{}'.format(ap)
    processArgs = [
        'python',
        os.path.join(os.environ['PYSPC_BIN'], 'bdimage2xml.py'),
#        '-c', LAMEDO.filename,
        '-F', START.strftime('%Y%m%d00'),
        '-L', END.strftime('%Y%m%d00'),
        '-n', v,
        '-O', d,
        '-r', dt.utcnow().strftime('%Y%m%d%H00'),
        '-S', s,
        '-t', BDI_TYPE, BDI_STYPE, BDI_BAND
    ]
    if STATS is not None:
        processArgs.extend(['-U', 'stats', STATS])
    if PRECISION is not None:
        processArgs.extend(['-U', 'precision', PRECISION])
    processRun = subprocess.Popen(
        processArgs, universal_newlines=True,
        shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
    processRun.wait()
