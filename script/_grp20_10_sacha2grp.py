#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_grp20_10_sacha2grp:

.. role:: blue

.. role:: boldblue

Création de la base de données GRP v2020 à partir des données SACHA (_grp20_10_sacha2grp.py)
--------------------------------------------------------------------------------------------

Description
+++++++++++

Les fichiers nécessaires pour GRP sont créés automatiquement à partir de fichiers de bassin GRP et de bases SACHA

Paramètres
++++++++++

.. rubric:: CONFIGURATION DE GRP

:boldblue:`BDD_DIRNAME` : :blue:`Répertoire des données GRP`

:boldblue:`BV_DIRNAME` : :blue:`Répertoire des fichiers bassin`

:boldblue:`DATA_DIRNAME` : :blue:`Répertoires des données, par variable`

:boldblue:`VARNAMES` : :blue:`Correspondance des variables GRP <-> SPC`

.. rubric:: CONFIGURATION DE SACHA

:boldblue:`SACHA_DIRNAME` : :blue:`Répertoir des bases SACHA`

:boldblue:`SACHA_FILENAMES` : :blue:`Base Sacha par couple (variable, fournisseur)`

:boldblue:`HYDRO_VERSIONS` : :blue:`Codification Hydro2 ou Hydro3 par couple (variable, fournisseur)`

:boldblue:`SUFFIX` : :blue:`Suffixe du nom des postes pour reconnaitre le fournisseur`

.. rubric:: CONFIGURATION TEMPORELLE

:boldblue:`FIRST_DT` : :blue:`Premier pas de temps`

:boldblue:`LAST_DT` : :blue:`Dernier pas de temps`

.. rubric:: CONFIGURATION DES EXPORTS

:boldblue:`EXPORTS` : :blue:`Couples (variable, fournisseur) à exporter`

"""
# ------------------------------------------------------------------------
# IMPORTS
# ------------------------------------------------------------------------
from datetime import datetime as dt, timedelta as td
import glob
import os
import os.path
import sys

from pyspc.model.grp20 import GRP_Basin, str2td
from pyspc import read_Sacha

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   BDD_DIRNAME       : Répertoire des données GRP
#   BV_DIRNAME        : Répertoire des fichiers bassin
#   DATA_DIRNAME      : Répertoires des données, par variable
#   SACHA_DIRNAME     : Répertoir des bases SACHA
#   SACHA_FILENAMES   : Base Sacha par couple (variable, fournisseur)
#   FIRST_DT          : Premier pas de temps
#   LAST_DT           : Dernier pas de temps
#   VARNAMES          : Correspondance des variables GRP <-> SPC
#   HYDRO_VERSIONS    : Codification Hydro2 ou Hydro3 par couple (variable, fournisseur)
#   SUFFIX            : Suffixe du nom des postes pour reconnaitre le fournisseur
#   EXPORTS           : Couples (variable, fournisseur) à exporter
# ------------------------------------------------------------------------
BDD_DIRNAME = r'D:\Utilisateurs\renaud.marty\Documents'\
    r'\2-grp-2020\00_Donnees_LACI'
BV_DIRNAME = os.path.join(BDD_DIRNAME, 'Bassins')
DATA_DIRNAME = {'P': os.path.join(BDD_DIRNAME, 'BDD_P'),
                'Q': os.path.join(BDD_DIRNAME, 'BDD_Q'),
                'T': os.path.join(BDD_DIRNAME, 'BDD_T')}
SACHA_DIRNAME = r'D:\Utilisateurs\renaud.marty\Documents\1-data\0-sacha'
SACHA_FILENAMES = {('P', 'CRISTAL'): 'base_Pluies_SPC.mdb',
                   ('P', 'EDF'): 'base_Pluies_EDF.mdb',
                   ('P', 'MF'): 'base_Pluies_MF.mdb',
                   ('Q', 'CRISTAL'): 'base_CEMAGREF_Debits.mdb',
                   ('T', 'EDF'): 'base_Temperatures_EDF.mdb',
                   ('T', 'MF'): 'base_Temperatures_MF.mdb'}
FIRST_DT = dt(2001, 1, 1)
LAST_DT = dt(2022, 1, 1)
VARNAMES = {'P': 'PH', 'T': 'TH', 'Q': 'QH'}
HYDRO_VERSIONS = {
    ('P', 'CRISTAL'): 'hydro2', ('P', 'EDF'): 'hydro2', ('P', 'MF'): 'hydro2',
    ('Q', 'CRISTAL'): 'hydro3', ('T', 'EDF'): 'hydro2', ('T', 'MF'): 'hydro2'}
SUFFIX = {'MF': None, 'CRISTAL': '-SPC', 'EDF': '-EDF'}
EXPORTS = [('P', 'CRISTAL')]
#EXPORTS = [('P', 'CRISTAL'), ('P', 'EDF'), ('P', 'MF'),
#           ('Q', 'CRISTAL'), ('T', 'EDF'), ('T', 'MF')]

# ------------------------------------------------------------------------
# DEPENDANCES
# ------------------------------------------------------------------------
STATIONS = {x: [] for x in SACHA_FILENAMES}

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
print("-- Exports des données Sacha pour GRP Calage")
# ------------------------------------------------------------------------
# --- LISTER LES BASSINS CONCERNES
# ------------------------------------------------------------------------
filenames = glob.glob(os.path.join(BV_DIRNAME, 'K233*.DAT'))
for filename in filenames:
    # Détermination du code de la station DEBIT
    print("   + Fichier Bassin : {0}".format(filename))
    q, t = os.path.splitext(os.path.basename(filename))[0].split('_')
    t = str2td(t)
    STATIONS[('Q', 'CRISTAL')].append((q, t))
    print("     - Station DEBIT '{0}' au pas de temps {1}".format(q, t))
    # Lecture du fichier Kxxx.dat:
    bassin_info = GRP_Basin(filename=filename)
    bassin_info.read()
    try:
        for s in bassin_info['E']:
            print("     - Station ETP   '{0}' avec pondération={1}"
                  "".format(s, bassin_info['E'][s]['w']))
    except KeyError:
        pass
    for v, sv in zip(['P', 'T'], ['PRCP', 'TEMP']):
        print(v, sv)
        try:
            for s in bassin_info[v]:
                try:
                    bassin_info[v][s]['t'] = str2td(bassin_info[v][s]['t'])
                except Exception:
                    bassin_info[v][s]['t'] = td(hours=1)

                print("     - Station {sv}  '{s}' au pas de temps {t} "
                      "avec pondération={w} ({n})"
                      "".format(s=s, **bassin_info[v][s], sv=sv))
                n = bassin_info[v][s].get('n', "")
                t = bassin_info[v][s]['t']
                if n.endswith(SUFFIX['CRISTAL']):
                    STATIONS[(v, 'CRISTAL')].append((s, t))
                elif n.endswith(SUFFIX['EDF']):
                    STATIONS[(v, 'EDF')].append((s, t))
                else:
                    STATIONS[(v, 'MF')].append((s, t))
        except KeyError:
            pass
    sys.stdout.flush()
# Nettoyage des listes de stations P,Q par suppression des doublons
# et des séries non-horaires
for src in STATIONS:
    if STATIONS[src]:
        STATIONS[src] = sorted(list({k[0] for k in STATIONS[src]
                                     if k[1] == td(hours=1)}))
sys.stdout.flush()
# ------------------------------------------------------------------------
# EXTRAIRE LES DONNEES
# ------------------------------------------------------------------------
for export in EXPORTS:
    varname = export[0]
    provider = export[1]
    if export not in STATIONS or not STATIONS[export]:
        continue
    if varname not in VARNAMES or varname not in DATA_DIRNAME:
        continue
    if export not in HYDRO_VERSIONS:
        continue
    print("   + Export : {0}".format(export))
    if varname == 'P':
        prcp_src = 'gauge'
    else:
        prcp_src = None
    series = read_Sacha(
        filename=os.path.join(SACHA_DIRNAME, SACHA_FILENAMES[export]),
        codes=STATIONS[export],
        varname=VARNAMES[varname],
        first_dt=FIRST_DT,
        last_dt=LAST_DT,
        prcp_src=prcp_src,
        hydro_version=HYDRO_VERSIONS[export])
    series = series.stripna()
    if not os.path.exists(DATA_DIRNAME[varname]):
        os.makedirs(DATA_DIRNAME[varname])
    ffs = series.to_GRP_Data(
        dirname=DATA_DIRNAME[varname], version='2020', how='overwrite')
    for fs in ffs:
        print('     - Ecriture du fichier v2020 : {}'
              ''.format(os.path.relpath(fs, BDD_DIRNAME)))

print("-- Fin des exports des données Sacha pour GRP Calage")
