#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_bdimage2csv_extract:

.. role:: blue

.. role:: boldblue

Convertir les données BdImages (_bdimage2csv_extract.py)
--------------------------------------------------------

Description
+++++++++++

Extraire les données BdImages et les convertir au format csv (GRP, PyspcFile)

.. warning: Ce script est à reprendre.

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES DONNEES BDIMAGES

:boldblue:`BDI_TYPE` : :blue:`Type d'image (ex: antilope)`

:boldblue:`BDI_STYPE` : :blue:`Sous-type d'image (ex: temps-reel)`

:boldblue:`BDI_DURATION` : :blue:`Durée de cumul de l'image (ex: 000100)`

:boldblue:`BDI_FCT` : :blue:`Fonction appliquée`

:boldblue:`BDI_FCTPAR` : :blue:`Paramètre de la fonction appliquée`

.. rubric:: CONFIGURATION DES DONNEES LOCALES

:boldblue:`RADAR_DIRNAME` : :blue:`Répertoire de stockage`

:boldblue:`RAD_FCT` : :blue:`Fonction image`

:boldblue:`RAD_FCTPAR` : :blue:`Paramètre de la fonction image`

:boldblue:`ZONES_FILENAME` : :blue:`Fichier contenant la liste des zones`

.. rubric:: AUTRE CONFIGURATION

:boldblue:`OVERWRITE` : :blue:`Ecraser la donnée existante ?`

"""

# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import os
import os.path
import subprocess
import glob

# ------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   RADAR_DIRNAME   : Répertoire de stockage
#   BDI_TYPE        : Type d'image
#   BDI_STYPE       : Sous-type d'image
#   BDI_DURATION    : Durée de cumul de l'image
#   BDI_FCT         : Fonction appliquée
#   BDI_FCTPAR      : Paramètre de la fonction
#   ZONES_FILENAME  : Fichier contenant la liste des zones
#   RAD_FCT         : Fonction image
#   RAD_FCTPAR      : Paramètre de la fonction image
#   OVERWRITE       : Ecraser la donnée existante?
#   RAD_VARNAME     : Variable
# ------------------------------------
RADAR_DIRNAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20\__CIC_ANALOGUES\DATA\20221202_complemente_LE_LoireAllier\xml'
# BDI_TYPE = 'comephore'
# BDI_STYPE = 'france'
BDI_TYPE = 'antilope'
BDI_STYPE = 'temps-reel'
# BDI_TYPE = 'panthere'
# BDI_STYPE = 'france'
BDI_DURATION = '000100'
BDI_FCT = 'getStats'
BDI_FCTPAR = 'standard'
ZONES_FILENAME = 'D:\\1-data\\20140617_listBV_grp-irstea-bdimage.txt'
RAD_FCT = 'stats'
RAD_FCTPAR = 'moy'
OVERWRITE = False

# ------------------------------------
# DEPENDANCES
# ------------------------------------
RAD_VARNAME = 'P'
if BDI_DURATION == '010000':
    RAD_VARNAME = 'PJ'

# *****************************************
#               SCRIPT
# *****************************************
# ------------------------------------
# LECTURE DE LA LISTE DES ZONES
# ------------------------------------
with open(ZONES_FILENAME, 'r', encoding='utf-8') as fz:
    fz.readline()
    grp_zones = []
    bv_zones = []
    bdi_zones = []
    # Ignorer entête en commençant par '1'
    for line in fz.readlines():
        zone = line.split(";")
        # 1e colonne: id_grp
        grp_zones.append(zone[0].replace("\n", '').strip())
        # 2e colonne: id_irstea
        bv_zones.append(zone[1].replace("\n", '').strip())
        # 3e colonne: id_bdimage
        bdi_zones.append(zone[2].replace("\n", '').strip())
    # ---------------------------------------------------
    # TRAITEMENT DES ZONES ET DECOUPAGE TEMPOREL REQUETE
    # ---------------------------------------------------
    for (zGRP, zBV, zBDI) in zip(grp_zones, bv_zones, bdi_zones):
        print("   + Zone en cours de traitement: "
              "GRP='{0}' // BV='{1}' // BDI='{2}'"
              "".format(zGRP, zBV, zBDI))
        zoneDir = os.path.join(
            RADAR_DIRNAME,  # Dossier parent
            "-".join([BDI_TYPE, BDI_STYPE]),  # Sous-dossier <type>-<soustype>
            BDI_DURATION,  # Sous-dossier <duree>
            zBDI  # Sous-dossier <zone>
        )
        listXmlFiles = glob.glob(zoneDir + "/*.xml")
        for kxml in listXmlFiles:
            print("     - extraction depuis le fichier xml : {0}"
                  "".format(kxml))
            kdir = os.path.dirname(kxml)
            kfile = os.path.basename(kxml)
        # Construction de la commande <bdimage2xml>
        # -----------------------------------------
            processArgs = [
                'python',
                os.environ['PYSPC_BIN'] + '\\'
                'bdimagexml2grp.py',
                '-I', kdir,
                '-O', os.path.dirname(kdir),
                '-d', kfile,
                '-M', RAD_FCT, RAD_FCTPAR,
                '-v'
                ]
            if OVERWRITE:
                processArgs.append('-o')
        # Exécution de <bdimage2xml> et affichage du log
        # ----------------------------------------------
            # Dossier parent
            # Sous-dossier <type>-<soustype>
            # Sous-dossier <duree>
            # Fichier : <zone><RAD_FCTPAR>_<RAD_VARNAME>.log
            processLog = os.path.join(
                RADAR_DIRNAME,
                "-".join(
                    [BDI_TYPE, BDI_STYPE]),
                BDI_DURATION,
                "_".join([
                    zBDI + RAD_FCTPAR,
                    RAD_VARNAME]) + ".log")
            if not os.path.exists(os.path.dirname(processLog)):
                os.makedirs(os.path.dirname(processLog))
            with open(processLog, 'a', encoding='utf-8', newline="\n") as flog:
                processRun = subprocess.Popen(
                    processArgs,
                    universal_newlines=True,
                    shell=True,  # nosec
                    stdout=flog,
                    stderr=subprocess.STDOUT)
                processRun.wait()
