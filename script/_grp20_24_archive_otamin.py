#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:
"""
import glob
import os.path
import shutil
import zipfile

from pyspc.model.grp20 import GRP_Cfg

# ======================================================================
# --- PARAMETRES
# ======================================================================
# HOME = os.path.abspath(os.path.dirname(__file__))
HOME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20\__MODELISATION'\
    r'\__GRPv2020'
VERIF_ROOT = os.path.join(HOME, 'LOCAL_TREE')
VERIF_SUBDIR = 'otamin'
CFG_FILENAME = os.path.join(HOME, 'LISTE_BASSINS_RTIME.DAT')
OTAMIN_ROOT = os.path.join(HOME, 'OTAMIN')
OTAMIN_GRP = os.path.join(OTAMIN_ROOT, 'grp')
OTAMIN_FIG = os.path.join(OTAMIN_ROOT, '_out', 'INC_ABA')
OTAMIN_TR = os.path.join(OTAMIN_ROOT, '_tr')

# ======================================================================
# --- CHARGEMENT DES DONNEES
# ======================================================================
runs20 = GRP_Cfg(filename=CFG_FILENAME)
runs20.read()
for run in runs20:
    print(run.CODE)
    dirname = os.path.join(
        glob.glob(os.path.join(VERIF_ROOT, f"{run.CODE}*", "*"))[0],
        VERIF_SUBDIR)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    # ------------------------------------------------------------------
    # --- ZIP FICHIER PREVISION GRP
    # ------------------------------------------------------------------
    src = glob.glob(os.path.join(OTAMIN_GRP, f"*{run.CODE}*"))[0]
    dst = os.path.join(dirname, os.path.basename(src).split('.')[0]) + '.zip'
    print(f">>> {os.path.relpath(dst, HOME)}")
    # writing files to a zipfile
    with zipfile.ZipFile(dst, 'w', compression=zipfile.ZIP_LZMA) as f:
        f.write(src, arcname=os.path.basename(src))
    # ------------------------------------------------------------------
    # --- ARCHIVE ZIP DES IMAGES OTAMIN
    # ------------------------------------------------------------------
    dst = os.path.join(
        dirname, f"{run.CODE}_{os.path.basename(OTAMIN_FIG)}.zip")
    print(f">>> {os.path.relpath(dst, HOME)}")
    # writing files to a zipfile
    with zipfile.ZipFile(dst, 'w', compression=zipfile.ZIP_DEFLATED) as f:
        for src in glob.glob(os.path.join(OTAMIN_FIG, run.CODE, '*.png')):
            f.write(src, arcname=os.path.basename(src))
    # ------------------------------------------------------------------
    # --- FICHIER ABAQUE OTAMIN
    # ------------------------------------------------------------------
    for src in glob.glob(os.path.join(OTAMIN_TR, f"{run.CODE}*")):
        dst = os.path.join(dirname, os.path.basename(src))
        print(f">>> {os.path.relpath(dst, HOME)}")
        shutil.copy2(src, dst)
#    break
