#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:
"""
import os.path
import geopandas as gpd
import matplotlib.pyplot as mplt
from matplotlib.lines import Line2D
import pandas as pnd

# ======================================================================
# --- PARAMETRES
# ======================================================================
# HOME = os.path.abspath(os.path.dirname(__file__))
HOME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20\__MODELISATION'\
    r'\__GRPv2020'
GIS_DIRNAME = os.path.join(HOME, 'SIG')
BV_BASENAME = 'bnbv_125_grp'  # 'bv_tete' 'bv_125' 'bnbv_125_grp'
BV_FILENAME = os.path.join(GIS_DIRNAME, f'{BV_BASENAME}.geojson')
LACI_FILENAME = os.path.join(GIS_DIRNAME, 'spc_laci.geojson')
RIVER_FILENAME = os.path.join(GIS_DIRNAME, 'river.geojson')
SELECT_FILENAME = os.path.join(HOME, 'LISTE_BASSINS_CALVAL_select4rtime.txt')
OUTPUT_DIRNAME = os.path.join(HOME, 'SIG')
DPI = 150
ALPHA = 1
# ======================================================================
# --- CHARGEMENT DES DONNEES
# ======================================================================
DF_BV = gpd.read_file(BV_FILENAME)
DF_LACI = gpd.read_file(LACI_FILENAME)
DF_RIVER = gpd.read_file(RIVER_FILENAME)
DF_SELECT = pnd.read_csv(SELECT_FILENAME, sep=';', index_col=None)
df = DF_BV.merge(DF_SELECT, on='CODE', how='left')
gdf = gpd.GeoDataFrame(df)
color_mapping1 = {
    "ajout": "tab:green",
    "maintien": "lightskyblue",
    "retrait": "tab:red",
    "rejet": "grey",
}
color_mapping2 = {
    "1-Amont-Montrond": "tab:purple",
    "2-Aval-Montrond": "tab:blue",
    "3-Amont-Vic": "tab:red",
    "4-Aval-Vic": "tab:green",
    "5-Cher-Indre": "tab:olive",
}

# ======================================================================
# --- CARTE EFFICIENCE
# ======================================================================
filename = os.path.join(OUTPUT_DIRNAME, f'{BV_BASENAME}_calval_runs4rtime.png')
fig, (ax1, ax2) = mplt.subplots(ncols=2, sharex=True, sharey=True)
fig.set_size_inches(11.69, 8.27)  # A4 paysage

DF_LACI.boundary.plot(ax=ax1, color='black')
DF_LACI.boundary.plot(ax=ax2, color='black')

gdf['SURFCALC'] = gdf['SURFCALC'].astype(float)
gdf = gdf.sort_values('SURFCALC', ascending=False)

gdf['color1'] = gdf.apply(
    lambda row: color_mapping1.get(row['GRP V2021'], 'grey'), axis=1)
gdf.plot(ax=ax1, color=gdf['color1'].values, edgecolor='darkgrey')

gdf['color2'] = gdf.apply(
    lambda row: color_mapping2.get(row['BASE V2021'], (0, 0, 0, 0)), axis=1)
gdf.plot(ax=ax2, color=gdf['color2'].values, edgecolor='darkgrey')

DF_RIVER.plot(ax=ax1, color='tab:brown')
DF_RIVER.plot(ax=ax2, color='tab:brown')

ax1.set_axis_off()
ax2.set_axis_off()
ax1.set_title('Statut de la modélisation')
ax2.set_title('Regroupement par base Temps-Réel')

legend_elements = [Line2D([0], [0], marker='o', linestyle='',
                          label=x, color=color_mapping1[x])
                   for x in sorted(color_mapping1)]
ax1.legend(handles=legend_elements, loc='upper center',
           bbox_to_anchor=(0.25, 0.35), fancybox=True, shadow=True, ncol=1,
           fontsize=14)

legend_elements = [Line2D([0], [0], marker='o', linestyle='',
                          label=x, color=color_mapping2[x])
                   for x in sorted(color_mapping2)]
ax2.legend(handles=legend_elements, loc='upper center',
           bbox_to_anchor=(0.25, 0.35), fancybox=True, shadow=True, ncol=1,
           fontsize=14)


fig.tight_layout()
fig.suptitle('Modélisation GRP v2021', fontsize=14)
fig.savefig(filename, dpi=DPI)
mplt.close(fig)
print(f'>>> {filename}')
