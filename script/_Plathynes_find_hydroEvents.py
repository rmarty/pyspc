#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_Plathynes_find_hydroEvents:

.. role:: blue

.. role:: boldblue

Détermination des événements hydrométriques pour PLATHYNES (_Plathynes_find_hydroEvents.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Détermination des événements hydrométriques pour PLATHYNES

Paramètres
++++++++++


.. rubric:: CONFIGURATION DES DONNEES

:boldblue:`LOCS_HYDRO` : :blue:`Liste des sites hydro`

:boldblue:`THRESHOLDS` : :blue:`Dictionnaire des seuils définissant un événement = station:valeur`

:boldblue:`EVENT_PERIOD` : :blue:`Période de recherche`

:boldblue:`EVENT_DELTA` : :blue:`Durée minimale d'un événement`

.. rubric:: CONFIGURATION DE LA PHYC

:boldblue:`PHYC_CONFIG_FILENAME` : :blue:`Fichier de configuration pour l'accès à la PHyC`

:boldblue:`PHYC_DATATYPE` : :blue:`Type de données PHyC`

.. rubric:: CONFIGURATION LOCALE

:boldblue:`OUTPUT_HOME` : :blue:`Répertoire racine local`

:boldblue:`OUTPUT_QIXM` : :blue:`Sous-répertoire des données QIXM`

:boldblue:`OUTPUT_QIXJ` : :blue:`Sous-répertoire des données QIXJ`

:boldblue:`OUTPUT_QI` : :blue:`Sous-répertoire des données QI`

:boldblue:`EVENT_FILENAME` : :blue:`Fichiers contenant les informations des événements retenus`

:boldblue:`PLOT_CONFIG` : :blue:`Configuration des figures`

.. rubric:: CONFIGURATION DES ACTIONS

:boldblue:`REQUEST_QIXM` : :blue:`Réquêter les débits instantanés maximals mensuels`

:boldblue:`REQUEST_QIXJ` : :blue:`Réquêter les débits instantanés maximals journaliers`

:boldblue:`REQUEST_QI` : :blue:`Réquêter les débits instantanés`

:boldblue:`CONVERT_QI` : :blue:`Convertir les débits instantanés`

"""

# *****************************************************************************
#               PARAMETRES
# *****************************************************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import collections
from datetime import datetime as dt, timedelta as td
import glob
import os

from pyspc.core.timeutil import group_events
from pyspc.webservice.phyc import PHyC
from pyspc import Config, read_Sandre, Series


# ------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   LOCS_HYDRO      : Lieux hydrométriques
# ------------------------------------
LOCS_HYDRO = [
    'K3222010',  # K3222010 - La Sioule à Pontgibaud
    'K3273010',  # K3273010 - Le Sioulet à Miremont [La Prugne]
    'K3302010',  # K3302010 - La Sioule à Châteauneuf-les-Bains
]
THRESHOLDS = {
    'K3222010': 50,  # Q2emp=50, Q2=50, Q5=74
    'K3273010': 40,  # Q2emp=40, Q2=50, Q5=78
    'K3302010': 75,  # Q2emp=74, Q2=89, Q5=142
}
EVENT_PERIOD = [dt(2000, 1, 1), dt(2021, 9, 1)]
EVENT_DELTA = td(days=5)
PHYC_CONFIG_FILENAME = os.path.join(os.environ['PYSPC_BIN'], 'phyc2xml_RM.txt')
PHYC_DATATYPE = 'data_obs_hydro'
OUTPUT_HOME = 'events'
OUTPUT_QIXM = os.path.join(OUTPUT_HOME, 'QIXM')
OUTPUT_QIXJ = os.path.join(OUTPUT_HOME, 'QIXJ')
OUTPUT_QI = os.path.join(OUTPUT_HOME, 'QI')
EVENT_FILENAME = os.path.join(OUTPUT_QI, 'events.csv')
PLOT_CONFIG = {
    'figure': {
        'plottype': 'hydro',
        'dpi': 300,
        'format': 'png',
        'xtitle': 'temps',
        'xfmt': '%Y-%m-%d\n%H:%M',
        'xminor': [0, 25, 6],
        'ytitleq': 'debit ($m^3.s^{-1}$)',
#        'legend': 0,
        'legend': 9,
        'legendcol': 4,
        'legendbottom': 0.10,
        'legendbox': (0.5, -0.20),
        'lfontsize': 6,
        'xfontsize': 8,
        'yfontsize': 8,
        'tfontsize': 10
    },
    'defaut': {
        'linewidth': 1,
        'linestyle': '-',
        'marker': '',
        'markersize': 0
    },
    'K3222010_QI': {
        'linewidth': 1,
        'linestyle': '-',
        'color': 'tab:purple',
        'label': 'Pontgibaud (Sioule)'
    },
    'K3273010_QI': {
        'linewidth': 1,
        'linestyle': '-',
        'color': 'tab:pink',
        'label': 'Miremont [La Prugne] (Sioulet)'
    },
    'K3302010_QI': {
        'linewidth': 1,
        'linestyle': '-',
        'color': 'tab:blue',
        'label': 'Chateauneuf-les-Bains (Sioule)'
    },
    'K3222010_QH': {
        'linewidth': 1,
        'linestyle': '-',
        'color': 'tab:purple',
        'label': 'Pontgibaud (Sioule)'
    },
    'K3273010_QH': {
        'linewidth': 1,
        'linestyle': '-',
        'color': 'tab:pink',
        'label': 'Miremont [La Prugne] (Sioulet)'
    },
    'K3302010_QH': {
        'linewidth': 1,
        'linestyle': '-',
        'color': 'tab:blue',
        'label': 'Chateauneuf-les-Bains (Sioule)'
    },
}

# ------------------------------------
# PARAMETRES : CONFIGURATION DES ACTIONS DU SCRIPT
#   REQUEST_QIXM    : Réquêter les débits instantanés maximals mensuels
#   REQUEST_QIXJ    : Réquêter les débits instantanés maximals journaliers
#   REQUEST_QI      : Réquêter les débits instantanés
#   CONVERT_QI      : Convertir les débits instantanés
# ------------------------------------
REQUEST_QIXM = False  # True, False
REQUEST_QIXJ = False  # True, False
REQUEST_QI = False  # True, False
CONVERT_QI = True  # True, False

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
PHYC_CONFIG = Config(filename=PHYC_CONFIG_FILENAME)
PHYC_CONFIG.read()

# ------------------------------------
# --- Réquêter les débits instantanés maximals mensuels
# ------------------------------------
if REQUEST_QIXM:
    print('01 - Réquêter les débits instantanés maximals mensuels'.upper())
    client = PHyC(
        hostname=PHYC_CONFIG['session'].get('host', None),
        username=PHYC_CONFIG['session']['username'],
        password=PHYC_CONFIG['session']['password'])
    client.login()
    print(' -> Session PHyC : {}'.format(client.session))
    if not os.path.exists(OUTPUT_QIXM):
        os.makedirs(OUTPUT_QIXM)
    filenames = client.retrieve(
        dirname=OUTPUT_QIXM, datatype=PHYC_DATATYPE, codes=LOCS_HYDRO,
        varname='QIXM',
        first_dtime=EVENT_PERIOD[0], last_dtime=EVENT_PERIOD[-1])
    for f in filenames:
        print(' -> {}'.format(os.path.relpath(f, OUTPUT_HOME)))
    client.logout()

# ------------------------------------
# --- Réquêter les débits instantanés maximals journaliers
# ------------------------------------
if REQUEST_QIXJ:
    print('02 - Réquêter les débits instantanés maximals journaliers'.upper())
    client = PHyC(
        hostname=PHYC_CONFIG['session'].get('host', None),
        username=PHYC_CONFIG['session']['username'],
        password=PHYC_CONFIG['session']['password'])
    client.login()
    print(' -> Session PHyC : {}'.format(client.session))
    if not os.path.exists(OUTPUT_QIXJ):
        os.makedirs(OUTPUT_QIXJ)
    for f in glob.glob(os.path.join(OUTPUT_QIXM, '*.xml')):
        print('    <- {}'.format(os.path.relpath(f, OUTPUT_HOME)))
        ss = read_Sandre(filename=f, datatype=PHYC_DATATYPE)
        for k, s in ss.items():
            loc = k[0]
            evs = s.events(threshold=THRESHOLDS[loc])
            for ev in evs:
                start = evs[ev]['firstdt'].replace(
                    day=1, hour=0, minute=0)
                end = evs[ev]['lastdt'].replace(
                    day=1, hour=0, minute=0) + td(days=31)
                filenames = client.retrieve(
                    dirname=OUTPUT_QIXJ, datatype=PHYC_DATATYPE, codes=[loc],
                    varname='QIXJ', first_dtime=start, last_dtime=end)
                for ff in filenames:
                    print('       -> {}'
                          ''.format(os.path.relpath(ff, OUTPUT_HOME)))
#                break
#        break
    client.logout()

# ------------------------------------
# --- Réquêter les débits instantanés
# ------------------------------------
if REQUEST_QI:
    print('03 - Réquêter les débits instantanés'.upper())
    events = []
    for f in glob.glob(os.path.join(OUTPUT_QIXJ, '*.xml')):
        print('     <- {}'.format(os.path.relpath(f, OUTPUT_HOME)))
        ss = read_Sandre(filename=f, datatype=PHYC_DATATYPE)
        for k, s in ss.items():
            loc = k[0]
            evs = s.events(threshold=THRESHOLDS[loc])
            for ev in evs:
                fdt = evs[ev]['firstdt']
                ldt = evs[ev]['lastdt']
                xdt = evs[ev]['maxdt']
                start = fdt - EVENT_DELTA if xdt - EVENT_DELTA > fdt else xdt - EVENT_DELTA
                end = ldt + EVENT_DELTA if xdt + EVENT_DELTA < ldt else xdt + EVENT_DELTA
                start = start.replace(hour=0, minute=0)
                end = end.replace(hour=0, minute=0) + td(days=1)
                events.append((start, end))
#                break
#            break
#        break
    events = group_events(events)
    events = {(e[0] + (e[-1] - e[0])/2).strftime('%Y%m%d'): e for e in events}
    client = PHyC(
        hostname=PHYC_CONFIG['session'].get('host', None),
        username=PHYC_CONFIG['session']['username'],
        password=PHYC_CONFIG['session']['password'])
    client.login()
    print(' -> Session PHyC : {}'.format(client.session))
    for ev, period in events.items():
        d = os.path.join(OUTPUT_QI, ev)
        if not os.path.exists(d):
            os.makedirs(d)
        filenames = client.retrieve(
            dirname=d, datatype=PHYC_DATATYPE, codes=LOCS_HYDRO,
            varname='Q', timestep=None, plusvalide=True,
            first_dtime=period[0], last_dtime=period[-1])
        for f in filenames:
            print(' -> {}'.format(os.path.relpath(f, OUTPUT_HOME)))
    client.logout()

# ------------------------------------
# --- Convertir les débits instantanés
# ------------------------------------
if CONVERT_QI:
    print('04 - Convertir les débits instantanés'.upper())
    events = Config()
    for bev in os.listdir(OUTPUT_QI):
        dev = os.path.join(OUTPUT_QI, bev)
        if not os.path.isdir(dev):
            continue
        print(' -> Evénement : {}'.format(bev))
        # Charger les QI
        series = Series(datatype='obs', name='events')
        for f in glob.glob(os.path.join(dev, '*.xml')):
            print('    <- {}'.format(os.path.relpath(f, OUTPUT_HOME)))
            ss = read_Sandre(filename=f, datatype=PHYC_DATATYPE)
            series.extend(ss)
        # Convertir en QH
        hourly_series = series.regularscale()
        # Exporter en pyspcfile
        filenames = series.to_PyspcFile(dirname=dev)
        for f in filenames:
            print('    -> {}'.format(os.path.relpath(f, OUTPUT_HOME)))
        filenames = hourly_series.to_PyspcFile(dirname=dev)
        for f in filenames:
            print('    -> {}'.format(os.path.relpath(f, OUTPUT_HOME)))
        # Exporter en GRP_RT_Data_16
        filenames = hourly_series.to_GRPRT_Data(dirname=dev)
        for f in filenames:
            print('    -> {}'.format(os.path.relpath(f, OUTPUT_HOME)))
        # Fichier évenements
        tmp_events = series.max(asconfig=True)
        events.setdefault(bev, collections.OrderedDict())
        events[bev]['name'] = bev
        events[bev]['start'] = min([s.firstdt for s in series.values()])
        events[bev]['end'] = max([s.lastdt for s in series.values()])
        for loc in LOCS_HYDRO:
            try:
                info = tmp_events[(loc, 'QI', None)]
            except KeyError:
                continue
            events[bev]['{}_max'.format(loc)] = info['max']
            events[bev]['{}_maxdt'.format(loc)] = info['maxdt']
        # Tracer event_name.png
#        f = series.plot_bypandas(
#            filename=os.path.join(dev, '{}_QI.png'.format(bev)))
        config = PLOT_CONFIG
        config['figure']['title'] = bev
        config['figure']['xlim'] = (events[bev]['start'], events[bev]['end'])
        config['figure']['dirname'] = dev
        config['figure']['filename'] = '{}_QI'.format(bev)
        f = series.plot_series(config=config)
        print('    -> {}'.format(os.path.relpath(f, OUTPUT_HOME)))
        config['figure']['filename'] = '{}_QH'.format(bev)
        f = hourly_series.plot_series(config=config)
#        f = hourly_series.plot_bypandas(
#            filename=os.path.join(dev, '{}_QH.png'.format(bev)))
        print('    -> {}'.format(os.path.relpath(f, OUTPUT_HOME)))

#        break

    # Fichier évenements
    f = events.to_csv(filename=EVENT_FILENAME, encoding='utf-8', sep=';')
    print('    -> {}'.format(os.path.relpath(f, OUTPUT_HOME)))
