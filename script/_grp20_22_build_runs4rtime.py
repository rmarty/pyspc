#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:
"""
import os.path
import pandas as pnd

from pyspc.convention.grp20 import CAL_CONFIG_CALNAMES
from pyspc.model.grp20 import GRP_Cfg, GRP_Run

# ======================================================================
# --- PARAMETRES
# ======================================================================
# HOME = os.path.abspath(os.path.dirname(__file__))
HOME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20\__MODELISATION'\
    r'\__GRPv2020'
CFG_FILENAME = os.path.join(HOME, 'LISTE_BASSINS_CALVAL.DAT')
VERIF_FILENAME = os.path.join(HOME, 'LISTE_BASSINS_CALVAL_finalconfig.txt')
SELECT_FILENAME = os.path.join(HOME, 'LISTE_BASSINS_CALVAL_select4rtime.txt')
RTIME_FILENAME = os.path.join(HOME, 'LISTE_BASSINS_RTIME_{}.DAT')

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
print("-- Construction des fichiers de configuration Temps-Réel")
# ---------------------------------------------------------------------
# --- 0 - Lecture des bassins retenus pour le temps-réel
# ---------------------------------------------------------------------
print(f">>> Lecture du fichier {os.path.relpath(SELECT_FILENAME, HOME)}")
SELECT = pnd.read_csv(SELECT_FILENAME, sep=';', index_col=0)
print(f">>> Lecture du fichier {os.path.relpath(VERIF_FILENAME, HOME)}")
VERIF = pnd.read_csv(VERIF_FILENAME, sep=';', index_col=0)
# ---------------------------------------------------------------------
# --- 1 - Initialisation des configurations temps-réel
# ---------------------------------------------------------------------
RTIME_CFG = {k: [] for k in SELECT['BASE V2021'].unique()
             if isinstance(k, str)}
# ---------------------------------------------------------------------
# --- 2 - Lecture des informations générales modèles et affectation temps-réel
# ---------------------------------------------------------------------
print(f">>> Lecture du fichier {os.path.relpath(CFG_FILENAME, HOME)}")
runs_calval = GRP_Cfg(filename=CFG_FILENAME)
runs_calval.read()
print(f"    > {len(runs_calval)} bassins modélisés")
for rcv in runs_calval:
    select = SELECT[SELECT.index == rcv.CODE]['BASE V2021'].values[0]
    rcv_dict = rcv._asdict()
    if select in RTIME_CFG:
        hor = rcv_dict[VERIF[VERIF.index == rcv.CODE]['HOR'].values[0]]
        sc = rcv_dict[VERIF[VERIF.index == rcv.CODE]['SC'].values[0]]
        star = VERIF[VERIF.index == rcv.CODE]['STAR'].values[0]
        rcv_dict['HOR1'] = hor
        rcv_dict['HOR2'] = '00J00H00M'
        rcv_dict['SC1'] = sc
        rcv_dict['SC2'] = -99.0
        for x in CAL_CONFIG_CALNAMES:
            if x == star:
                rcv_dict[x] = 1
            else:
                rcv_dict[x] = 0
        rt_run = GRP_Run(**rcv_dict)
        RTIME_CFG[select].append(rt_run)
#    break
# ---------------------------------------------------------------------
# --- 3 - Ecriture des fichiers pour temps-réel
# ---------------------------------------------------------------------
counter = 0
for rtc in RTIME_CFG:
    f = RTIME_FILENAME.format(rtc)
    print(f">>> Ecriture du fichier {os.path.relpath(f, HOME)}")
    runs = GRP_Cfg(filename=f)
    print(f"    > {len(RTIME_CFG[rtc])} bassins modélisés")
    counter += len(RTIME_CFG[rtc])
    runs.extend(RTIME_CFG[rtc])
    runs.write()
print(f">>> {counter} bassins retenus pour le temps-réel")
