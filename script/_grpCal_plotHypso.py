#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_grpCal_plotHypso:

.. role:: blue

.. role:: boldblue

Création d'un pdf multi-page et de fichiers png des courbes hypso (_grpCal_plotHypso.py)
----------------------------------------------------------------------------------------

.. warning: Script à reprendre

Description
+++++++++++

Paramètres
++++++++++

"""

# ------------------------------------
# IMPORTS
# ------------------------------------
import os
import os.path
import glob
import collections
import matplotlib.pyplot as mplt
from matplotlib.backends.backend_pdf import PdfPages
import pylab

# ------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   DATA_DIRNAME_BV : Répertoire des fichiers
#                     de courbes hypsométriques
#   PDF_FILENAME    : Fichier PDF contenant toutes les
#                     courbes hypsométriques
# ------------------------------------
# DATA_DIRNAME_BV = 'D:\\2-grp-3.1\\BDD\\Bassins'
DATA_DIRNAME_BV = r'D:\tmp\test_intro_pandas_script\hypso'
PDF_FILENAME = DATA_DIRNAME_BV + os.sep + "bassins-hypso.pdf"
# ------------------------------------
# PARAMETRES : CONFIGURATION DES IMAGES
#   DEFAULT_CFG : Configuration des images
#   YLIM        : Limite des axes Y
# ------------------------------------
DEFAULT_CFG = {'label': '',
          'color': [0.2, 0.6, 1.0],
          'linewidth': 3,
          'linestyle': '-',
          'marker': '',
          'markersize': 4}
YLIM = [0, 2000]

# *****************************************
#               SCRIPT
# *****************************************
all_hypso = {}
hypso_files = glob.glob(DATA_DIRNAME_BV + os.sep + "K*hypso*.txt")
for hypso_file in hypso_files:
    print(hypso_file)
    # Lecture donnees
    with open(hypso_file, 'r', encoding='utf-8') as fh:
        fh.readline()
        currentX = []
        currentY = []
        for line in fh.readlines():
            data = line.replace("\n", "")
            data = data.split(";")
            print(data)
            currentX.append(100.*float(data[0]))
            currentY.append(float(data[1]))
        # Fichier PNG
        png_filename = os.path.splitext(hypso_file)
        png_filename = png_filename[0] + '.png'
        print(png_filename)
        # Definition label de la courbe hypso courante
        DEFAULT_CFG['label'] = os.path.basename(hypso_file).split("-")
        DEFAULT_CFG['label'] = DEFAULT_CFG['label'][0]
        # pprint.pprint(DEFAULT_CFG)
        # Sauvegarde de la courbe courante
        all_hypso[DEFAULT_CFG['label']] = {'X': currentX, 'Y': currentY}
        # Creation de la figure et de la zone graphique
        my_fig = mplt.figure(dpi=150)
        my_axes = my_fig.add_axes((0.11, 0.15, 0.84, 0.75))
        # Creation de la courbe hypso et application de la DEFAULT_CFG
        hypso_line = my_axes.plot(currentX, currentY)
        for key in DEFAULT_CFG:
            mplt.setp(hypso_line, key, DEFAULT_CFG[key])
        # Definition des meta-donnees de la figure
        my_axes.set_title('Courbe hypsométrique')
        my_axes.set_xlabel('pourcentage du bassin (%)')
        my_axes.set_xlim(0, 100)
        my_axes.set_ylabel('altitude (m)')
        my_axes.set_ylim(YLIM[0], YLIM[1])
        my_axes.legend(loc=1)
        # mplt.show()
        pylab.savefig(png_filename)
        mplt.close(my_fig)

# pprint.pprint(all_hypso)
# j'utilise <collections.OrderedDict>
# car je veux que le dico soit trié par clé croissante.
all_hypso = collections.OrderedDict(
    sorted(all_hypso.items(), key=lambda t: t[0]))

print(PDF_FILENAME)
with PdfPages(PDF_FILENAME) as pdf:
    for station in all_hypso:
        print(" - %s" % (station))
        my_fig = mplt.figure(1, dpi=150)
        my_axes = my_fig.add_axes((0.11, 0.15, 0.84, 0.75))
        hypso_line = my_axes.plot(all_hypso[station]['X'],
                                   all_hypso[station]['Y'])
        DEFAULT_CFG['label'] = station
        for key in DEFAULT_CFG:
            mplt.setp(hypso_line, key, DEFAULT_CFG[key])
        my_axes.set_title('Courbe hypsométrique')
        my_axes.set_xlabel('pourcentage du bassin (%)')
        my_axes.set_xlim(0, 100)
        my_axes.set_ylabel('altitude (m)')
        my_axes.set_ylim(YLIM[0], YLIM[1])
        my_axes.legend(loc=1)
        # mplt.show(my_fig)
        pdf.savefig()  # saves the current figure into a pdf page
        mplt.clf()
