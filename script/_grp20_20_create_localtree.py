#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:
"""

import os
import shutil

from pyspc.model.grp20 import GRP_Cfg


# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES GRP20
# ------------------------------------------------------------------------
HOME = r'D:\Utilisateurs\diane.lebailly\Documents\GRP'
CFG_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\__MODELISATION\__GRPv2020\LOCAL_TREE\LISTE_BASSINS.DAT'
# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES GRP20
# ------------------------------------------------------------------------
CALIBRATION_NAME = 'calage_2023-03-01'
LOCAL_HOME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\__MODELISATION\__GRPv2020\LOCAL_TREE'
REMOTE_HOME = r'S:\08-SHPECI\02-DPECI\E_Modelisation\Pluie_Debit\GRP'\
    r'\Modeles_Cales'

# *****************************************************************************
#               FONCTIONS
# *****************************************************************************


# *****************************************************************************
#               SCRIPT
# *****************************************************************************
print("-- Création de l'arborescence locale")
# ---------------------------------------------------------------------
# --- 1.1 - Liste des répertoires distants
# ---------------------------------------------------------------------
remote_paths = {x.split('_')[0]: os.path.join(REMOTE_HOME, x)
                for x in os.listdir(REMOTE_HOME)
                if os.path.isdir(os.path.join(REMOTE_HOME, x))}
# ---------------------------------------------------------------------
# --- 1.2 - Lecture des informations générales modèles
# ---------------------------------------------------------------------
runs20 = GRP_Cfg(filename=CFG_FILENAME)
runs20.read()
for run in runs20:
    if run.CODE in remote_paths:
        dst = os.path.basename(remote_paths[run.CODE])
    else:
        name = run.NOM.split(' a ')[-1].replace(' ', '-')
        dst = f'{run.CODE}_{name}'
    print(run.CODE, dst)
    dst = os.path.join(LOCAL_HOME, dst, CALIBRATION_NAME)
    os.makedirs(dst)
#    break

print("-- Archivage de l'arborescence distante")
# ---------------------------------------------------------------------
# --- 2.1 - Archivage des répertoires distants
# ---------------------------------------------------------------------
for d in remote_paths.values():
    for b in os.listdir(d):
        dst = os.path.join(d, b)
        if os.path.isdir(dst):
            print(dst, b)
            shutil.make_archive(dst, 'zip', dst)
#    break
