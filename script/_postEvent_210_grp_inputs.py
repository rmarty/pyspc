#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_210_grp_inputs:

.. role:: blue

.. role:: boldblue

Conversion des données obs en fichier d'entrée GRP (_postEvent_210_grp_inputs.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Conversion des données obs en fichier d'entrée GRP

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`GRP_DIRNAME` : :blue:`Répertoire Temps-Réel de GRP`

"""

# *****************************************************************************
#               PARAMETRES
# *****************************************************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import glob
import os.path
import subprocess
import sys
from pyspc import Config
from pyspc.model.grp16 import GRPRT_Basin

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# ------------------------------------
# PARAMETRES : CONFIGURATION DE GRP
#   GRP_DIRNAME      : Répertoire de GRP
# ------------------------------------
GRP_DIRNAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\20211012_GRP\Temps_Reel'
# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
LOCS_METEO = Config(filename=os.path.join(HOME, 'config', 'locs_meteo.txt'))
LOCS_METEO.read()
# ------------------------------------
# --- PARAMETRE SUBPROCESS
# ------------------------------------
SRC_DIRNAMES = [
    os.path.join(HOME, 'obs', 'mf', 'PH'),
    os.path.join(HOME, 'obs', 'phyc', 'PH'),
    os.path.join(HOME, 'obs', 'phyc', 'QH'),
]
VARNAMES = ['PH', 'PH', 'QH']
INPUTS = {
    'PluieMF.txt': [], 'PluieCRISTALEDF.txt': [], 'Debit.txt': []}
filenames = glob.glob(os.path.join(
    GRP_DIRNAME, 'BD_Bassins', '*', 'BASSIN.DAT'))
for f in filenames:
    grpbv = GRPRT_Basin(filename=f)
    grpbv.read()
    bv = next(iter(grpbv['Q'].keys()))
    if bv not in LOCS_HYDRO:
        continue
    for loc in grpbv['P']:
        try:
            if LOCS_METEO[loc]['provider'] == 'MF':
                INPUTS['PluieMF.txt'].append(loc)
            else:
                INPUTS['PluieCRISTALEDF.txt'].append(loc)
        except KeyError:
            print('pluviomètre inconnu {}'.format(loc))
    INPUTS['Debit.txt'].extend(grpbv['Q'])
for i in INPUTS:
    INPUTS[i] = sorted(list(set(INPUTS[i])))
# ------------------------------------
# --- LANCEMENT SUBPROCESS
# ------------------------------------
for src, var, fname in zip(SRC_DIRNAMES, VARNAMES, INPUTS):
    LIST_FILENAME = os.path.join(
        HOME, 'config', os.path.basename(__file__).replace('.py', '.txt'))
    with open(LIST_FILENAME, 'w', encoding='utf-8', newline='\n') as f:
        for loc in INPUTS[fname]:
            f.write('{}\n'.format(loc))
    processArgs = [
        'python',
        os.environ['PYSPC_BIN'] + '\\'
        'csv2grpRT.py',
        '-I', src,
        '-O', os.path.join(GRP_DIRNAME, 'Entrees'),
        '-n', var,
        '-t', 'grp16_rt_data',
        '-l', LIST_FILENAME,
        '-w'
    ]
    processRun = subprocess.Popen(
        processArgs, universal_newlines=True,
        shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
    processRun.wait()
    dst = os.path.join(GRP_DIRNAME, 'Entrees', fname)
    if os.path.exists(dst):
        os.remove(dst)
    if fname.startswith('Pluie'):
        os.rename(os.path.join(GRP_DIRNAME, 'Entrees',
                               'Pluie__postEvent_210_grp_inputs.txt'), dst)
    elif fname.startswith('Debit'):
        os.rename(os.path.join(GRP_DIRNAME, 'Entrees',
                               'Debit__postEvent_210_grp_inputs.txt'), dst)
