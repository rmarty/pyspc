#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_211_grp_run:

.. role:: blue

.. role:: boldblue

Lancement de GRP en Temps Diff et conversion en csv (_postEvent_211_grp_run.py)
-------------------------------------------------------------------------------

Description
+++++++++++

Lancement de GRP en Temps Diff et conversion en csv

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration de GRP

:boldblue:`GRP_SCEN_NORAIN` : :blue:`Identifiant du scénario Pluie Nulle`

:boldblue:`GRP_MODEL_POM` : :blue:`Code modèle POM`

:boldblue:`GRP_DTYPES` : :blue:`Type de scénario (prv et sim)`

:boldblue:`GRP_SCENS` : :blue:`Codes scénario (prv et sim)`

.. rubric:: Configuration de l'archivage

:boldblue:`CSV_DIRNAME` : :blue:`Répertoire des données au format csv`

:boldblue:`PRV_DIRNAME` : :blue:`Répertoire des données au format prv`

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`DELTA_DT` : :blue:`Intervalle entre 2 pas de temps`

:boldblue:`LTIME_DT` : :blue:`Horizon maximal des prévisions`

"""

# *****************************************************************************
#               PARAMETRES
# *****************************************************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt, timedelta as td
import os.path
import shutil
import subprocess
import sys
from pyspc import Config

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# ------------------------------------
# PARAMETRES : CONFIGURATION DE GRP
#   GRP_DIRNAME         : Répertoire de GRP
#   GRP_CFG             : Répertoire de la configuration
#   GRP_CFG_FILENAME    : Fichier de la configuration
#   GRP_CFG_BACKUP      : Archive du fichier de la configuration
#   GRP_EXE_FILENAME    : Exécutable GRP
#   GRP_IN              : Répertoire des Entrées
#   GRP_OUT             : Répertoire des Sorties
#   GRP_SCEN_NORAIN     : Identifiant du scénario Pluie Nulle
#   GRP_MODEL_POM       : Code modèle POM
#   GRP_DTYPES          : Type de scénario (prv et sim)
#   GRP_SCENS           : Codes scénario (prv et sim)
# ------------------------------------
GRP_DIRNAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\20211012_GRP\Temps_Reel'
GRP_CFG = os.path.join(GRP_DIRNAME, 'Parametrage')
GRP_CFG_FILENAME = 'Config_Prevision.txt'
GRP_CFG_BACKUP = 'Config_Prevision.txt.bak'
GRP_EXE_FILENAME = os.path.join(GRP_DIRNAME, 'GRP_PREVISION.BAT')
GRP_IN = os.path.join(GRP_DIRNAME, 'Entrees')
GRP_OUT = os.path.join(GRP_DIRNAME, 'Sorties')
GRP_SCEN_NORAIN = '2007'
GRP_MODEL_POM = '45gGRPd000'
GRP_DTYPES = {
    'grp16_rt_fcst_diff': 'GRP_D_Prev*',
    'grp16_rt_sim_diff': 'GRP_D_Simu*'
}
GRP_SCENS = [2001, 2001]
# -----------------------------------------------------------------------------
# PARAMETRES : ARCHIVAGE PRV, CSV et PNG
#   CSV_DIRNAME         : Répertoire des données au format csv
#   SPC_VARNAME         : Variable
#   DELTA_DT            : Intervalle entre 2 pas de temps
#   LTIME_DT            : Horizon maximal des prévisions
# -----------------------------------------------------------------------------
CSV_DIRNAME = os.path.join('fcst', 'grp')
PRV_DIRNAME = os.path.join('fcst', 'prv')
SPC_VARNAME = 'QH'
DELTA_DT = td(hours=3)  # Intervalle entre 2 prévisions
LTIME_DT = td(hours=120)  # Horizon de la prévision

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
})
os.chdir(HOME)
# ------------------------------------
# --- PARAMETRE SUBPROCESS
# ------------------------------------
RUNTIMES = [EVENT['event']['firstdt'] + kt * DELTA_DT
            for kt in range(
                int((EVENT['event']['lastdt'] - EVENT['event']['firstdt'])
                    / DELTA_DT + 1))]
# ------------------------------------
# --- LANCEMENT SUBPROCESS
# ------------------------------------
for runtime in RUNTIMES:
    print("Instant de prévision : {}".format(
        runtime.strftime("%d/%m/%Y %H:00")))
    current_csv_dirname = os.path.join(
        CSV_DIRNAME,
        runtime.strftime('%Y'), runtime.strftime('%m'), runtime.strftime('%d'))
    if not os.path.exists(current_csv_dirname):
        os.makedirs(current_csv_dirname)
    current_prv_dirname = os.path.join(
        PRV_DIRNAME,
        runtime.strftime('%Y'), runtime.strftime('%m'), runtime.strftime('%d'))
    if not os.path.exists(current_prv_dirname):
        os.makedirs(current_prv_dirname)
    # ------------------------------------------------------------------------
    # MAJ CONFIG
    # ------------------------------------------------------------------------
    print(" + Mise-à-jour de la configuration de GRP Temps Réel")
    processArgs = [
        'python',
        os.environ['PYSPC_BIN'] + '\\'
        'duplicateGrpRTCfg.py',
        '-I', GRP_CFG,
        '-D', GRP_CFG_BACKUP,
        '-O', GRP_CFG,
        '-c', GRP_CFG_FILENAME,
        '-t', 'grp16',
        '-U', "OBSCHE", GRP_IN + os.sep,
        '-U', "SCECHE", GRP_IN + os.sep,
        '-U', "PRVCHE", GRP_OUT + os.sep,
        '-U', "MODFON", 'Temps_diff',
        '-U', "INSTPR", runtime.strftime("%Y-%m-%d %H:00:00"),
        '-U', "HORMAX", str(int(LTIME_DT.total_seconds() / 3600))
    ]
    processRun = subprocess.Popen(
        processArgs, universal_newlines=True,
        shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
    processRun.wait()
    # ------------------------------------------------------------------------
    # RUN GRP
    # ------------------------------------------------------------------------
    print(" + Lancement de GRP Temps Réel")
    os.chdir(GRP_DIRNAME)
    processRun = subprocess.Popen(
        GRP_EXE_FILENAME, universal_newlines=True,
        shell=True, stdout=subprocess.DEVNULL, stderr=sys.stderr)  # nosec
    processRun.wait()
    os.chdir(HOME)
    # ------------------------------------------------------------------------
    # CSV GRP CALAGE
    # ------------------------------------------------------------------------
    print(" + Conversion au format CSV")
    for dtype, scen in zip(GRP_DTYPES, GRP_SCENS):
        print('   - Fichiers {} pour le scénario {}'.format(dtype, scen))
        processArgs = [
            'python',
            os.environ['PYSPC_BIN'] + '\\'
            'grpRT2csv.py',
            '-I', GRP_OUT,
            '-d', GRP_DTYPES[dtype] + '*{}*'.format(scen),
            '-O', current_csv_dirname,
            '-t', dtype,
            '-C', 'pyspc', '-1'
        ]
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        src = os.path.join(current_csv_dirname, 'GRP16_QH.txt')
        dst = os.path.join(current_csv_dirname,
                           '{}_{}_QH.txt'.format(dtype.replace('_', ''),
                                                 runtime.strftime("%Y%m%d%H")))
        shutil.copy2(src, dst)
    # ------------------------------------------------------------------------
    # PRV OTAMIN
    # ------------------------------------------------------------------------
    print(" + Conversion au format PRV")
    processArgs = [
        'python',
        r'D:\Utilisateurs\renaud.marty\Documents\3-bitbucket\pyOTAMIN\GRP2prv_py3.py',
        '-i', GRP_OUT,
        '-m', 'TD',
        '-o', current_prv_dirname,
        '-d', '-u',
        '-p', GRP_MODEL_POM,
        '-G', 'GRPv2016', '-O', 'OTAMINv2016',
        '-s', ' '.join(list(set(GRP_SCENS)))
    ]
    processRun = subprocess.Popen(
        processArgs, universal_newlines=True,
#        shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        shell=True, stdout=subprocess.DEVNULL, stderr=sys.stderr)  # nosec
    processRun.wait()

#    break
