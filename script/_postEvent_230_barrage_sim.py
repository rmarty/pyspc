#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_230_barrage_sim:

.. role:: blue

.. role:: boldblue

Lancement de MOHYS en SIMULATION et conversion en csv (_postEvent_230_barrage_sim.py)
-----------------------------------------------------------------------------------

Description
+++++++++++

Lancement de MOHYS en SIMULATION et conversion en csv

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration de MOHYS

:boldblue:`MOH_DIRNAME` : :blue:`Répertoire de MOHYS`

:boldblue:`BGE_DIRNAME` : :blue:`Répertoire de BARRAGE`

.. rubric:: Configuration des fichiers csv

:boldblue:`CSV_DIRNAME` : :blue:`Répertoire des données au format csv`

:boldblue:`SPC_VARNAME` : :blue:`Variable`

"""

# *****************************************************************************
#               PARAMETRES
# *****************************************************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt
import glob
import os.path
import shutil
import subprocess
import sys
from pyspc import Config, Series, read_PyspcFile
from pyspc.binutils.csv import read_csvlike
from pyspc.io.prv import read_prv
from pyspc.core.keyseries import tuple2str

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\__SCHAPI\REX\Rex-Schapi\config\event.txt'
# ------------------------------------
# PARAMETRES : CONFIGURATION DE MOHYS et BARRAGE
#   MOH_DIRNAME         : Répertoire de MOHYS
# -----------------------------------------------------------------------------
MOH_DIRNAME = r'D:\Utilisateurs\renaud.marty\Documents\3-bitbucket\pybarrage'\
    r'\Mohys'
MOH_CFG_FILENAME = os.path.join(MOH_DIRNAME, 'Liste_Modeles.cfg')
MOH_SIM_DIRNAME = os.path.join('fcst', 'moh')
BGE_DIRNAME = r'D:\Utilisateurs\renaud.marty\Documents\3-bitbucket\pybarrage'\
    r'\Barrages'
BGE_IN = os.path.join(BGE_DIRNAME, '_in')
BGE_OUT = os.path.join(BGE_DIRNAME, '_out')
BGE_EXE_FILENAME = os.path.join(BGE_DIRNAME, 'Barrages.py')
# -----------------------------------------------------------------------------
# PARAMETRES : ARCHIVAGE PRV, CSV et PNG
#   CSV_DIRNAME         : Répertoire des données au format csv
#   SPC_VARNAME         : Variable
# -----------------------------------------------------------------------------
CSV_DIRNAME = os.path.join('fcst', 'bge')
PNG_DIRNAME = os.path.join('fig', 'bge')
SPC_VARNAME = 'QH'
SPC_VARNAME2 = 'HH'

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
os.chdir(HOME)
OBS_DIRNAME = os.path.join(HOME, 'obs', 'phyc', SPC_VARNAME)
OBS_DIRNAME2 = os.path.join(HOME, 'obs', 'phyc', SPC_VARNAME2)
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
})
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(
    filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION Liste_Modeles
# ------------------------------------
MOH_MODELS = Config(filename=MOH_CFG_FILENAME)
MOH_MODELS.read()
for m in MOH_MODELS:
    if MOH_MODELS[m]['BGE'] == 'None':
        continue
#    if m != 'LMV':
#        continue
    b = MOH_MODELS[m]['BGE']

    f = os.path.join(MOH_DIRNAME, m, 'stations.txt')
    c = Config.from_multitxt(filenames=[f])
    moh_sta = c['stations']['out']
    bge_sta = c['stations']['barrage_sta'][:8]

    f = os.path.join(BGE_DIRNAME, b, 'stations.txt')
    c = Config.from_multitxt(filenames=[f])
    test = [i in LOCS_HYDRO for i in c['stations']['in'].split(';')]
    test.extend([o in LOCS_HYDRO for o in c['stations']['out'].split(';')])
    if all(test):
        MOH_MODELS[m]['in'] = c['stations']['in'].split(';')
        MOH_MODELS[m]['out'] = c['stations']['out'].split(';')
    else:
        continue
    print(f"Simulation du modèle barrage '{b}' associé au Mohys '{m}'")
# ------------------------------------
# --- DONNEES ENTREE
# ------------------------------------
    print(' -> Inputs Q')
    series = read_csvlike(
        stations=[loc
                  for r in ['in', 'out']
                  for loc in MOH_MODELS[m][r]
                  if len(loc) == 8],
        varnames=SPC_VARNAME, dirname=OBS_DIRNAME, csvtype='pyspc'
    )
    series.name = '_postEvent_230_barrage_sim_Q'
#    print(series)
    for k in series:
        mfs = glob.glob(os.path.join(MOH_SIM_DIRNAME, f'{k[0]}*'))
        for mf in mfs:
            mseries = read_PyspcFile(filename=mf)
            for mk in mseries:
                if mk[0] != moh_sta:
                    continue
                s = mseries[mk]
                s.code = tuple2str((bge_sta + '_mohys', mk[1], None)).replace(
                    '_{}'.format(SPC_VARNAME), '')
                series.add(s)
    series.to_GRPRT_Data(version='grp16', dirname=BGE_IN)
    in_src = os.path.join(BGE_IN, 'Debit_{}.txt'.format(series.name))
    in_dst = os.path.join(BGE_IN, 'Debit.txt')
    if not os.path.exists(in_src):
        print('!!! Fichier obs inexistant !!!')
        continue
    shutil.copy2(in_src, in_dst)
    print(f"    -> {in_dst}")
# ------------------------------------
# --- DONNEES ENTREE H
# ------------------------------------
    print(' -> Inputs H')
    series = read_csvlike(
        stations=[loc
                  for r in ['in', 'out']
                  for loc in MOH_MODELS[m][r]
                  if len(loc) == 10],
        varnames=SPC_VARNAME2, dirname=OBS_DIRNAME2, csvtype='pyspc'
    )
    series.name = '_postEvent_230_barrage_sim_H'
    series.to_GRPRT_Data(version='grp16', dirname=BGE_IN)
    in_src2 = os.path.join(BGE_IN, 'Hauteur_{}.txt'.format(series.name))
    in_dst2 = os.path.join(BGE_IN, 'Hauteur.txt')
    if not os.path.exists(in_src2):
        print('!!! Fichier obs inexistant !!!')
        continue
    shutil.copy2(in_src2, in_dst2)
    print(f"    -> {in_dst2}")
# ------------------------------------
# --- CALCUL BARRAGE
# ------------------------------------
    print(' -> Execution')
    BAT_FILENAME = os.path.join(
        HOME, 'config', os.path.basename(__file__).replace('.py', '.bat'))
    with open(BAT_FILENAME, 'w', encoding='utf-8', newline='\n') as f:
        f.write('@echo off \n')
        f.write('call activate py2_qt5 \n')
        f.write('pushd {} \n'.format(os.path.dirname(BGE_EXE_FILENAME)))
        f.write('python {exe} -n {mod} -F {start} -L {end} -r {start}'
                ' --debug '.format(
                    exe=os.path.basename(BGE_EXE_FILENAME),
                    mod=b,
                    start=EVENT['event']['firstdt'].strftime('%Y%m%d%H'),
                    end=EVENT['event']['lastdt'].strftime('%Y%m%d%H'),
                ))
        f.write('\n')
        f.write('popd \n')
        f.write('\n')
    processRun = subprocess.Popen(
        BAT_FILENAME, universal_newlines=True, cwd=MOH_DIRNAME,
        shell=True, stdout=subprocess.DEVNULL, stderr=sys.stderr)  # nosec
    processRun.wait()
# ------------------------------------
# --- DONNEES SORTIE
# ------------------------------------
    print(' -> Outputs')
    src = os.path.join(BGE_DIRNAME, b, 'test_BGE_{b}_{r}.png'.format(
        b=b, r=EVENT['event']['firstdt'].strftime('%Y%m%d_%H00')
    ))
    dst = os.path.join(PNG_DIRNAME, os.path.basename(src))
    if not os.path.exists(os.path.dirname(dst)):
        os.makedirs(os.path.dirname(dst))
    try:
        shutil.copy2(src, dst)
    except IOError:
        pass
    else:
        print("    -> Copie de l'image : {}"
              "".format(os.path.relpath(dst, start=HOME)))

    src = os.path.join(BGE_OUT, 'BGE_B_{r}_{b}.prv'.format(
        b=b, r=EVENT['event']['firstdt'].strftime('%Y%m%d_%H00')
    ))
    if os.path.exists(src):
        prv_series = read_prv(filename=src, datatype='otamin16_trend')
        series = Series(datatype='fcst', name='Barrage')
        for p in prv_series:
            series.add(code=p[0], meta=(p[2][0], 'barrage-sim', None),
                       serie=prv_series[p])
        dst = os.path.join(HOME, CSV_DIRNAME)
        if not os.path.exists(dst):
            os.makedirs(dst)
        filenames = series.to_PyspcFile(dirname=dst)
        for f in filenames:
            print("    -> Export : {}".format(os.path.relpath(f, start=HOME)))
        os.remove(src)
    else:
        print(f"Fichier de sortie inexistant : '{src}'")
# ------------------------------------
# --- NETTOYAGE
# ------------------------------------
    for x in [in_src, in_dst, in_src2, in_dst2]:
        try:
            os.remove(x)
        except IOError:
            pass
#    break  # for m in MOH_MODELS:
