#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_313_verif_plot:

.. role:: blue

.. role:: boldblue

Tracer les critères de vérification des prévisions (_postEvent_313_verif_plot.py)
---------------------------------------------------------------------------------

Description
+++++++++++

Calculer les critères de vérification des prévisions

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des figures

:boldblue:`MODEL` : :blue:`Nom du modèle (sous-répertoire de fcst/)`

:boldblue:`SCENS` : :blue:`Nom des scénarios (STATION_SCEN-FREQ_*.csv), sous forme de liste`

.. rubric:: Configuration des statistiques

:boldblue:`SCORES_ABS` : :blue:`Calcul en valeur absolue ? (T/F) sous forme de liste`

:boldblue:`SCORES_REL` : :blue:`Calcul en valeur relative ? (T/F) sous forme de liste`

:boldblue:`SCORES_DELTA` : :blue:`Pas, en fréquence, des statistiques d'erreur`

:boldblue:`SCORES_FREQS` : :blue:`Fréquences des statistiques d'erreur`

"""

# *****************************************s
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import glob
import os.path
from pyspc import Config
from pyspc.verification.csv import read_csv
from pyspc.plotting.verification import plot

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# -----------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES SERIES
#   MODEL  : Nom du modèle (sous-répertoire de fcst/)
#   SCENS  : Nom des scénarios (STATION_SCEN-FREQ_*.csv), sous forme de liste
# -----------------------------------------------------------------------------
MODEL = 'oper'
SCENS = ['valid']
# -----------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES STATISTIQUES
#   SCORES_ABS       : Calcul en valeur absolue ? (T/F) sous forme de liste
#   SCORES_REL       : Calcul en valeur relative ? (T/F) sous forme de liste
#   SCORES_DELTA     : Pas, en fréquence, des statistiques d'erreur
#   SCORES_FREQS     : Fréquences des statistiques d'erreur
# -----------------------------------------------------------------------------
SCORES_ABS = [False, True]
SCORES_REL = [False, True]
SCORES_ABSREL = [(a, r) for a in SCORES_ABS for r in SCORES_REL]

LABELS = {'00': 'prév. déterm.', '10': 'tend. basse',
          '50': 'tend. centrale', '90': 'tend. haute'}
YUNIT = '[$m^3/s$]'

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
# ------------------------------------
# --- LIEU DE PREVISION
# ------------------------------------
for loc in LOCS_HYDRO:
    if len(loc) != 8:
        continue
    if not loc.startswith('K055'):
        continue
    for scen in SCENS:
        for s in SCORES_ABSREL:
            stat_filename = '{}_{}-[0-9][0-9]'.format(loc, scen)
            suffix = ''
            if s[0] and s[1]:
                suffix += '_absrel'
            elif s[0]:
                suffix += '_abs'
            elif s[1]:
                suffix += '_rel'
            stat_filename += suffix
            stat_filename += '.csv'
# ------------------------------------
# --- FICHIERS DE VERIFICATION
# ------------------------------------
            filenames = glob.glob(
                os.path.join(HOME, 'verif', MODEL, loc, stat_filename))
            df = read_csv(filenames)
            if df is None:
                continue
# ------------------------------------
# --- FIGURE DE VERIFICATION
# ------------------------------------
            fig_dirname = os.path.join(HOME, 'fig', 'verif', loc)
            fig_filename = os.path.join(
                fig_dirname, '{}_{}_{}{}.png'.format(loc, MODEL, scen, suffix))
            if not os.path.exists(fig_dirname):
                os.makedirs(fig_dirname)
            title = '{}\n{} ({}, {})'.format(
                EVENT['event']['title'], LOCS_HYDRO[loc]['name'],
                LOCS_HYDRO[loc]['river'], loc)
            if s[0] and s[1]:
                ytitle = 'Erreur relative absolue moyenne {}'.format(YUNIT)
            elif s[0]:
                ytitle = 'Erreur absolue moyenne {}'.format(YUNIT)
            elif s[1]:
                ytitle = 'Erreur relative moyenne {}'.format(YUNIT)
            else:
                ytitle = 'Erreur moyenne {}'.format(YUNIT)
            try:
                ltime = int(LOCS_HYDRO[loc]['ltime'])
            except (ValueError, TypeError):
                ltime = None
            f = plot(fig_filename, df, scen, ltime, title, ytitle)
            print('Figure {}'.format(f))
#            break  # for s in SCORES_ABSREL:
#        break  # for scen in SCENS:
#    break  # for loc in LOCS_HYDRO:
