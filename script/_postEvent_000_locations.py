#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_000_locations:

.. role:: blue

.. role:: boldblue

Récupération des méta-données des lieux hydro (_postEvent_000_locations.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Récupération des méta-données des lieux hydro à partir du référentiel

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`REFSPC_FILENAME` : :blue:`Référentiel du SPC Loire-Cher-Indre`

Fichier de configuration
++++++++++++++++++++++++

.. rubric:: Fichier de configuration de l'événement

.. code-block:: cfg

   [event]
   name = 202006_Loire_amont
   title = Loire - Juin 2020
   firstdt = 2020061000
   lastdt = 2020061600
   maxltime = 120
   reaches = LC105;LC115;LC123;LC126;LC130;LC134;LC137;LC140;LC145;LC155
   subreaches = subreaches.txt
   meteo_depts = 03;07;21;42;43;58;63;69;71;89
   zap = 21016;21018;21019;21020;41003;41004;41005;41007;41008;41009;41014

Fichier créé
++++++++++++

.. rubric:: Fichier de configuration des lieux (locs_hydro.txt)

.. code-block:: cfg

   [K026001002]
   name = Chadrac [Pont du Monteil]
   code = K026001002
   ltime = 6

   [K0260010]
   name = Chadrac-sur-Loire
   code = K0260010
   code_hydro2 = K0260020;K0260030
   ltime = 6
   bnbv = LO808
   river = Loire
   area = 1328
   stats_t2 = 285
   stats_t5 = 585
   stats_t10 = 819
   stats_t20 = 1043
   stats_t50 = 1333
   stats_t100 = 1550
   stats_period = 1919 – 2018
   stats_size = 99

"""

# *****************************************************************************
#               PARAMETRES
# *****************************************************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import collections
import os.path
from pyspc import Config
from pyspc.metadata.refspc import RefSPC

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'

# ------------------------------------
# PARAMETRES : REFERENTIEL
#   REFSPC_FILENAME   : Référentiel du SPC Loire-Cher-Indre
# ------------------------------------
REFSPC_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\BDD.sqlite'

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'reaches'): Config.to_listofstr,
    ('event', 'meteo_depts'): Config.to_listofstr
})
# ------------------------------------
# --- INIT DES CONFIGURATIONS LOCATIONS
# ------------------------------------
LOCS_METEO = Config(filename=os.path.join(HOME, 'config', 'locs_meteo.txt'))
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))

reader = RefSPC(filename=REFSPC_FILENAME)
# ------------------------------------
# --- HYDRO : STATIONS SELON TRONCON
# ------------------------------------
_, stations = reader.get_loc_hydro(
    codes=EVENT['event']['reaches'],
    how='by_reach'
)
for s in stations:
    sta = stations[s]['code_station']
    LOCS_HYDRO.setdefault(sta, collections.OrderedDict())
    LOCS_HYDRO[sta]['name'] = stations[s]['nom_station_hydro']
    LOCS_HYDRO[sta]['code'] = sta
    LOCS_HYDRO[sta]['ltime'] = stations[s]['delai_vigicrues']

# ------------------------------------
# ---  HYDRO : SITES
# ------------------------------------
    _, sites = reader.get_loc_hydro(
        codes=[sta[:8]]
    )
    for s2 in sites:
        site = sites[s2]['code_site']
        LOCS_HYDRO.setdefault(site, collections.OrderedDict())
        LOCS_HYDRO[site]['name'] = sites[s2]['nom_site_hydro']
        LOCS_HYDRO[site]['code'] = site
        LOCS_HYDRO[site]['code_hydro2'] = []
        LOCS_HYDRO[site]['ltime'] = LOCS_HYDRO[sta]['ltime']
        LOCS_HYDRO[site]['bnbv'] = sites[s2]['id_bnbv']
        LOCS_HYDRO[site]['river'] = sites[s2]['nom_riviere']
        LOCS_HYDRO[site]['area'] = sites[s2]['taille_bv']

# ------------------------------------
# ---  HYDRO : STATIONS HYDRO2
# ------------------------------------
_, stations = reader.get_loc_hydro(
    codes=EVENT['event']['reaches'],
    how='by_reach',
    hydro3=False
)
for s in stations:
    cap = stations[s]['code_capteur']
    try:
        site = cap[:8]
        LOCS_HYDRO[site]['code_hydro2'].append(stations[s]['code_hydro2'])
    except (TypeError, KeyError):
        pass

# ------------------------------------
# ---  HYDRO : STATISTIQUES
# ------------------------------------
sites = [x for x in LOCS_HYDRO.keys() if len(x) == 8]
_, stats = reader.get_stat_hydro(codes=sites)
for s in stats:
    site = stats[s]['code_site']
    test_ref = stats[s]['stat_ref_spc']
    if test_ref != 1:
        continue
    try:
        LOCS_HYDRO[site]['stats_t2'] = stats[s]['t2']
        LOCS_HYDRO[site]['stats_t5'] = stats[s]['t5']
        LOCS_HYDRO[site]['stats_t10'] = stats[s]['t10']
        LOCS_HYDRO[site]['stats_t20'] = stats[s]['t20']
        LOCS_HYDRO[site]['stats_t50'] = stats[s]['t50']
        LOCS_HYDRO[site]['stats_t100'] = stats[s]['t100']
        LOCS_HYDRO[site]['stats_period'] = stats[s]['periode_echantillon']
        LOCS_HYDRO[site]['stats_size'] = stats[s]['taille_echantillon']
    except KeyError:
        pass
for s in sites:
    LOCS_HYDRO.convert({(s, 'code_hydro2'): Config.from_listofstr})

# ------------------------------------
# --- METEO : STATIONS SELON DEPT
# ------------------------------------
_, sites = reader.get_loc_meteo(
    codes=EVENT['event']['meteo_depts'],
    how='by_dept'
)
for s in sites:
    site = sites[s]['code_mf']
    n = sites[s]['nom_mf']
    if n is None:
        continue
    LOCS_METEO.setdefault(site, collections.OrderedDict())
    LOCS_METEO[site]['name'] = n
    LOCS_METEO[site]['code'] = site
    if sites[s]['nom_mf'].endswith('-EDF'):
        LOCS_METEO[site]['provider'] = 'EDF'
    elif sites[s]['nom_mf'].endswith('-SPC'):
        LOCS_METEO[site]['provider'] = 'CRISTAL'
    else:
        LOCS_METEO[site]['provider'] = 'MF'


# ------------------------------------
# ---  HYDRO : ECRITURE DES CONFIGURATION
# ------------------------------------
LOCS_METEO.write()
LOCS_HYDRO.write()
