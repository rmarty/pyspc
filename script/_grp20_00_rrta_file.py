#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:
"""

# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------------------------------------------
# IMPORTS
# ------------------------------------------------------------------------
import glob
import os
import os.path

from pyspc.model.grp20 import GRP_Basin as GRP_Basin20


# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES GRP20
# ------------------------------------------------------------------------
HOME = r'S:\08-SHPECI\02-DPECI\B_Documentation\Modelisation\GRP\etudes'\
    r'\GRP-2020'
BDD = os.path.join(HOME, 'Param_v2020', 'CAL_LACI_2022')
BV_DST = os.path.join(BDD, 'Bassins_renommes')
LIST_FILENAMES = {
    'P': os.path.join(BDD, 'LISTE_PLUVIOMETRES.DAT'),
    'T': os.path.join(BDD, 'LISTE_TEMPERATURES.DAT'),
}
HEADERS = {
    'P': """!--------!--!----------------------------------------!
!Poste P.!RT!Nom Poste Pluviomètre                   !
!--------!--!----------------------------------------!
""",
    'T': """!--------!--!----------------------------------------!
!Poste T.!RT!Nom Poste Température                   !
!--------!--!----------------------------------------!
""",
}
LOCS = {}

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
print("-- Renommage des stations P,T dans les fichiers bassins")
bvfiles = glob.glob(os.path.join(BV_DST, '*.DAT'))
for f in bvfiles:
    fd = os.path.join(BV_DST, os.path.basename(f))
    bsrc = GRP_Basin20(filename=f)
    bsrc.read()
    if 'P' not in bsrc and 'T' not in bsrc:
        continue
    for v in ['P', 'T']:
        if v not in bsrc:
            continue
        LOCS.setdefault(v, {})
        for s in bsrc[v]:
            n = bsrc[v][s].get('n', "")
            if s not in LOCS[v] or not LOCS[v][s]:
                LOCS[v][s] = n

for v in LOCS:
    print(LIST_FILENAMES[v])
    with open(LIST_FILENAMES[v], 'w', encoding='ansi', newline='\r\n') as f:
        f.write(HEADERS[v])
        for s in sorted(LOCS[v]):
            f.write('!{}!TU!{}\n'.format(s, LOCS[v][s]))
