#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_grpRT_fcst16:

.. role:: blue

.. role:: boldblue

Lancement de rejeux GRP v2016 (_grpRT_fcst16.py)
------------------------------------------------

Description
+++++++++++

Script gérant le lancement en temps différé de GRP Temps Réel (16r1571)

Paramètres
++++++++++

.. rubric:: Configuration des periodes de calcul


:boldblue:`FIRST_DT` : :blue:`Premier pas de temps à considérer`

:boldblue:`LAST_DT` : :blue:`Dernier pas de temps à considérer`

:boldblue:`DELTA_DT` : :blue:`Intervalle entre 2 pas de temps`

:boldblue:`LTIME_DT` : :blue:`Horizon maximal des prévisions`


.. rubric:: Arborescence


:boldblue:`HOME` : :blue:`Répertoire de travail`


.. rubric:: Arborescence grp temps reel


:boldblue:`GRP_HOME` : :blue:`Répertoire principal`

:boldblue:`GRP_CFG` : :blue:`Répertoire de la configuration`

:boldblue:`GRP_CFG_FILENAME` : :blue:`Fichier de la configuration`

:boldblue:`GRP_CFG_BACKUP` : :blue:`Archive du fichier de la configuration`

:boldblue:`GRP_EXE_FILENAME` : :blue:`Exécutable GRP`

:boldblue:`GRP_IN` : :blue:`Répertoire des Entrées`

:boldblue:`GRP_OUT` : :blue:`Répertoire des Sorties`

:boldblue:`GRP_SCEN_NORAIN` : :blue:`Identifiant du scénario Pluie Nulle`

:boldblue:`GRP_MODEL_POM` : :blue:`Code modèle POM`


.. rubric:: Arborescence otamin


:boldblue:`OTAMIN_HOME` : :blue:`Répertoire principal`

:boldblue:`OTAMIN_EXE` : :blue:`Exécutable TR OTAMIN`

:boldblue:`OTAMIN_IN` : :blue:`Répertoire des entrées TR OTAMIN`

:boldblue:`OTAMIN_OUT` : :blue:`Répertoire des sorties TR OTAMIN`


.. rubric:: Arborescence scenarios meteo


:boldblue:`MET_HOME` : :blue:`Répertoire de travail`


.. rubric:: Archivage prv, csg et png


:boldblue:`PRV_DIRNAME` : :blue:`Répertoire des données au format prv`

:boldblue:`PRV_BAT_FILENAME` : :blue:`Exécutable de conversion grp2prv`

:boldblue:`CSV_DIRNAME` : :blue:`Répertoire des données au format csv`

:boldblue:`PNG_DIRNAME` : :blue:`Répertoire des données au format png`

:boldblue:`OBS_DIRNAME` : :blue:`Répertoire des données observées`

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`COLORMAP` : :blue:`Palette de couleur`

:boldblue:`DPI` : :blue:`Résolution des images`

"""

# *****************************************************************************
#               PARAMETRES
# *****************************************************************************
# ------------------------------------------------------------------------
# IMPORTS
# ------------------------------------------------------------------------
from datetime import datetime as dt, timedelta as td
import glob
import os.path
import shutil
import subprocess
import sys

from pyspc.core.config import Config
from pyspc.plotting.colormap import build_colormap

# -----------------------------------------------------------------------------
# MODE DEBUG : 1 runtime
# -----------------------------------------------------------------------------
MODE_DEBUG = True

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES PERIODES DE CALCUL
#   FIRST_DT : Premier pas de temps à considérer
#   LAST_DT  : Dernier pas de temps à considérer
#   DELTA_DT : Intervalle entre 2 pas de temps
#   LTIME_DT : Horizon maximal des prévisions
# ------------------------------------------------------------------------
FIRST_DT = [dt(2016, 5, 30, 6)]  # Dates de départ des événements
LAST_DT = [dt(2016, 6, 1, 6)]  # Dates de fin des événements
DELTA_DT = td(hours=24)  # Intervalle entre 2 prévisions
LTIME_DT = td(hours=120)  # Horizon de la prévision
# -----------------------------------------------------------------------------
# PARAMETRES : ARBORESCENCE
#   HOME : Répertoire de travail
# -----------------------------------------------------------------------------
HOME = r'E:\20171212_GRP_fcst33'
# -----------------------------------------------------------------------------
# PARAMETRES : ARBORESCENCE GRP TEMPS REEL
#   GRP_HOME            : Répertoire principal
#   GRP_CFG             : Répertoire de la configuration
#   GRP_CFG_FILENAME    : Fichier de la configuration
#   GRP_CFG_BACKUP      : Archive du fichier de la configuration
#   GRP_EXE_FILENAME    : Exécutable GRP
#   GRP_IN              : Répertoire des Entrées
#   GRP_OUT             : Répertoire des Sorties
#   GRP_SCEN_NORAIN     : Identifiant du scénario Pluie Nulle
#   GRP_MODEL_POM       : Code modèle POM
# -----------------------------------------------------------------------------
GRP_HOME = os.path.join(HOME, 'Temps_Reel')
GRP_CFG = os.path.join(GRP_HOME, 'Parametrage')
GRP_CFG_FILENAME = 'Config_Prevision.txt'
GRP_CFG_BACKUP = 'Config_Prevision.txt.bak'
GRP_EXE_FILENAME = os.path.join(GRP_HOME, 'GRP_PREVISION.BAT')
GRP_IN = os.path.join(GRP_HOME, 'Entrees')
GRP_OUT = os.path.join(GRP_HOME, 'Sorties')
GRP_SCEN_NORAIN = '2007'
GRP_MODEL_POM = '45gGRPd000'
# ------------------------------------------------------------------------
# PARAMETRES : ARBORESCENCE OTAMIN
#   OTAMIN_HOME : Répertoire principal
#   OTAMIN_EXE  : Exécutable TR OTAMIN
#   OTAMIN_IN   : Répertoire des entrées TR OTAMIN
#   OTAMIN_OUT  : Répertoire des sorties TR OTAMIN
# ------------------------------------------------------------------------
OTAMIN_HOME = os.path.join(HOME, 'OTAMIN_2017')
OTAMIN_EXE = os.path.join(OTAMIN_HOME, 'exe_tr.bat')
OTAMIN_IN = os.path.join(OTAMIN_HOME, '_03_TR', '_in')
OTAMIN_OUT = os.path.join(OTAMIN_HOME, '_03_TR', '_out')
# -----------------------------------------------------------------------------
# PARAMETRES : ARBORESCENCE SCENARIOS METEO
#   MET_HOME : Répertoire de travail
# -----------------------------------------------------------------------------
MET_HOME = os.path.join(HOME, 'scen')
# -----------------------------------------------------------------------------
# PARAMETRES : ARCHIVAGE PRV, CSG et PNG
#   PRV_DIRNAME         : Répertoire des données au format prv
#   PRV_BAT_FILENAME    : Exécutable de conversion grp2prv
#   CSV_DIRNAME         : Répertoire des données au format csv
#   PNG_DIRNAME         : Répertoire des données au format png
#   OBS_DIRNAME         : Répertoire des données observées
#   SPC_VARNAME         : Variable
#   COLORMAP            : Palette de couleur
#   DPI                 : Résolution des images
# -----------------------------------------------------------------------------
PRV_DIRNAME = os.path.join(HOME, 'prv')
PRV_BAT_FILENAME = os.path.join(HOME, 'grp2prv.bat')
CSV_DIRNAME = os.path.join(HOME, 'csv')
PNG_DIRNAME = os.path.join(HOME, 'png')
OBS_DIRNAME = os.path.join(HOME, 'obs')
SPC_VARNAME = 'QH'
COLORMAP = build_colormap(cmapname='gist_rainbow', cmapsize=256)
DPI = 300

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
# ------------------------------------------------------------------------
# EVENEMENTS
# ------------------------------------------------------------------------
print('-- Prévisions GRP')
os.chdir(HOME)
for first_dt, last_dt in zip(FIRST_DT, LAST_DT):
    print("   - Evenement du {} au {}".format(
        first_dt.strftime("%d/%m/%Y %H:00"),
        last_dt.strftime("%d/%m/%Y %H:00")))
    runtimes = [first_dt + kt * DELTA_DT
                for kt in range(int((last_dt - first_dt) / DELTA_DT + 1))]
    for runtime in runtimes:
        print("     - Instant de prévision : {}".format(
            runtime.strftime("%d/%m/%Y %H:00")))
    # ------------------------------------------------------------------------
    # Début traitement du run
    # ------------------------------------------------------------------------
        current_prv_dirname = os.path.join(
            PRV_DIRNAME,
            runtime.strftime('%Y'),
            runtime.strftime('%m'),
            runtime.strftime('%d')
        )
        if not os.path.exists(current_prv_dirname):
            os.makedirs(current_prv_dirname)
        current_csv_dirname = os.path.join(
            CSV_DIRNAME,
            runtime.strftime('%Y'),
            runtime.strftime('%m'),
            runtime.strftime('%d')
        )
        if not os.path.exists(current_csv_dirname):
            os.makedirs(current_csv_dirname)
        current_png_dirname = os.path.join(
            PNG_DIRNAME,
            runtime.strftime('%Y'),
            runtime.strftime('%m'),
            runtime.strftime('%d')
        )
        if not os.path.exists(current_png_dirname):
            os.makedirs(current_png_dirname)
    # ------------------------------------------------------------------------
    # CREATION DES SCENARIOS METEO
    # ------------------------------------------------------------------------
        print("       + Gestion des scénarios météorologiques")
        filenames = list(glob.glob(os.path.join(GRP_IN, 'Scen_*.txt')))
        if filenames:
            for f in filenames:
                try:
                    os.remove(f)
                    print('         - Fichier supprimé : {}'
                          ''.format(os.path.relpath(f)))
                except WindowsError:
                    print('         - Fichier NON supprimé : {}'
                          ''.format(os.path.relpath(f)))
        current_met_dirname = os.path.join(
            MET_HOME,
            runtime.strftime('%Y%m%d')
        )
        filenames = list(glob.glob(
            os.path.join(current_met_dirname, 'Scen_*.txt')))
        if filenames:
            for f in filenames:
                try:
                    shutil.copy2(f, GRP_IN)
                    print('         - Fichier copié : {}'
                          ''.format(os.path.relpath(f)))
                except WindowsError:
                    print('         - Fichier NON copié : {}'
                          ''.format(os.path.relpath(f)))

    # ------------------------------------------------------------------------
    # MAJ CONFIG
    # ------------------------------------------------------------------------
        print("       + Mise-à-jour de la configuration de GRP Temps Réel")
        processArgs = [
            'python',
            os.environ['PYSPC_BIN'] + '\\'
            'duplicateGrpRTCfg.py',
            '-I', GRP_CFG,
            '-D', GRP_CFG_BACKUP,
            '-O', GRP_CFG,
            '-c', GRP_CFG_FILENAME,
            '-U', "OBSCHE", GRP_IN + os.sep,
            '-U', "SCECHE", GRP_IN + os.sep,
            '-U', "PRVCHE", GRP_OUT + os.sep,
            '-U', "MODFON", 'Temps_diff',
            '-U', "INSTPR", runtime.strftime("%Y-%m-%d %H:00:00"),
            '-U', "HORMAX", str(int(LTIME_DT.total_seconds() / 3600))
        ]
        if MODE_DEBUG:
            processArgs.append('-v')
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
    # ------------------------------------------------------------------------
    # RUN GRP
    # ------------------------------------------------------------------------
        print("       + Lancement de GRP Temps Réel")
        os.chdir(GRP_HOME)
        if MODE_DEBUG:
            processRun = subprocess.Popen(
                GRP_EXE_FILENAME, universal_newlines=True,
                shell=True)  # nosec
        else:
            processRun = subprocess.Popen(
                GRP_EXE_FILENAME, universal_newlines=True,
                shell=True, stdout=subprocess.DEVNULL, stderr=sys.stderr)  # nosec
        processRun.wait()
        os.chdir(HOME)
    # ------------------------------------------------------------------------
    # GRP2prv
    # ------------------------------------------------------------------------
        print("       + Export des prévisions GRP au format PRV")
        with open(PRV_BAT_FILENAME, 'w', encoding='utf-8') as f:
            f.write("@echo off\n")
            f.write("call activate py2k\n")
            f.write(" ".join([
                'python',
                os.environ['PYSPC_SCRIPT'] + '\\'
                '_grp2prv.py',
                '-i', GRP_OUT,
                '-m', 'TD',
                '-p', GRP_MODEL_POM,
                '-o', OTAMIN_IN,
                '-0', GRP_SCEN_NORAIN
            ]))
            if MODE_DEBUG:
                f.write(' -v ')
            f.write('\n')
            f.write("call deactivate\n")
        if MODE_DEBUG:
            processRun = subprocess.Popen(
                PRV_BAT_FILENAME, shell=True)  # nosec
        else:
            processRun = subprocess.Popen(
                PRV_BAT_FILENAME, shell=True,  # nosec
                stdout=subprocess.DEVNULL, stderr=sys.stderr)
        processRun.wait()
    # ------------------------------------------------------------------------
    # OTAMIN
    # ------------------------------------------------------------------------
        print("       + Ajout des intervalles prédictifs OTAMIN")
        os.chdir(OTAMIN_HOME)
        if MODE_DEBUG:
            processRun = subprocess.Popen(
                OTAMIN_EXE, shell=True)  # nosec
        else:
            processRun = subprocess.Popen(
                OTAMIN_EXE, shell=True,  # nosec
                stdout=subprocess.DEVNULL, stderr=sys.stderr)
        processRun.wait()
        os.chdir(HOME)
    # ------------------------------------------------------------------------
    # Fin du traitement du run
    # ------------------------------------------------------------------------
        print("       + Nettoyage du run OTAMIN")
        filenames = list(glob.glob(
            os.path.join(OTAMIN_IN, 'GRP_B_*.prv')))
        if filenames:
            for f in filenames:
                try:
                    os.remove(f)
                    print('         - Fichier supprimé : {}'
                          ''.format(os.path.relpath(f)))
                except WindowsError:
                    print('         - Fichier NON supprimé : {}'
                          ''.format(os.path.relpath(f)))
        filenames = list(glob.glob(
            os.path.join(OTAMIN_OUT, 'GRP_B_*.prv')))
        if filenames:
            for f in filenames:
                try:
                    shutil.move(f, current_prv_dirname)
                    print('         - Fichier déplacé : {}'
                          ''.format(os.path.relpath(f)))
                except WindowsError:
                    print('         - Fichier NON déplacé : {}'
                          ''.format(os.path.relpath(f)))
    # ------------------------------------------------------------------------
    # CSV GRP CALAGE
    # ------------------------------------------------------------------------
        print("       + Conversion au format CSV")
        filenames = list(glob.glob(
            os.path.join(current_prv_dirname, 'GRP_B_*.prv')))
        if filenames:
            for f in filenames:
                dirname = os.path.dirname(f)
                basename = os.path.basename(f)
                print('         - Fichier : {}'.format(basename))
                processArgs = [
                    'python',
                    os.environ['PYSPC_BIN'] + '\\'
                    'prv2grp.py',
                    '-I', dirname,
                    '-d', basename,
                    '-t', 'fcst_otamin',
                    '-n', SPC_VARNAME,
                    '-O', current_csv_dirname
                ]
                if MODE_DEBUG:
                    processArgs.append('-v')
    #            print(' '.join(processArgs))
                processRun = subprocess.Popen(
                    processArgs, universal_newlines=True,
                    shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
                processRun.wait()

    # ------------------------------------------------------------------------
    # FIGURE
    # ------------------------------------------------------------------------
        print("       + Création des figures au format PNG")
        filenames = list(glob.glob(
            os.path.join(current_csv_dirname, '*.txt')))
        # ---------------------------------------------------------------------
        # Liste des stations à considérer
        # ---------------------------------------------------------------------
        if filenames:
            stations = sorted(list({os.path.basename(f).split('_')[0]
                                    for f in filenames}))
            members = sorted(list({os.path.basename(f).split('_')[2]
                                   for f in filenames}))
            for station in stations:
                # -------------------------------------------------------------
                # Fichier de configuration
                # -------------------------------------------------------------
                config_filename = '{}_{}.txt'.format(
                    station,
                    runtime.strftime('%Y%m%d%H')
                )
                config = Config(filename=os.path.join(
                    current_png_dirname, config_filename))
                config["data"] = {}
                print('         - Création de la configuration : {}'
                      ''.format(config['filename']))
                # -------------------------------------------------------------
                # Configuration de la figure
                # -------------------------------------------------------------
                config["data"]["figure"] = {
                    'datalist': [],
                    'dirname': current_png_dirname,
                    'dpi': 150,
                    'filename': os.path.splitext(config_filename)[0],
                    'format': 'png',
                    'legend': '6',
                    'legendcol': '1',
                    'lfontsize': '6',
                    'tfontsize': '10',
                    'xfontsize': '8',
                    'yfontsize': '8',
                    'plottype': 'hydro',
                    'title': '{}  /  {}'.format(
                        station,
                        runtime.strftime('%d-%m-%Y %H:%M')
                    ),
                    'verbose': False,
                    'xlim': ",".join([
                        first_dt.strftime('%Y%m%d%H'),
                        last_dt.strftime('%Y%m%d%H')]),
                    'xtitle': 'Date (TU)',
                    'ytitle': 'Débit ($m3/s$)'
                }
                if MODE_DEBUG:
                    config["data"]["figure"]['verbose'] = True
                # -------------------------------------------------------------
                # Configuration DEFAUT
                # -------------------------------------------------------------
                config["data"]["defaut"] = {
                    'color': '0.0,0.0,0.0',
                    'firstdatetime': first_dt.strftime('%Y%m%d%H'),
                    'inputdir': current_csv_dirname,
                    'lastdatetime': last_dt.strftime('%Y%m%d%H'),
                    'linestyle': '-',
                    'linewidth': '1',
                    'marker': '',
                    'varname': SPC_VARNAME
                }
                # -------------------------------------------------------------
                # Configuration SCENARIOS
                # -------------------------------------------------------------
                for k, member in enumerate(members):
                    code = '{}_{}_{}'.format(
                        station,
                        runtime.strftime('%Y%m%d%H'),
                        member
                    )
                    color = ['{0:.3f}'.format(c)
                             for c in COLORMAP[
                                 int((k)*255/(len(members)+1))][:3]]
                    color = ','.join(color)
                    config["data"][code] = {
                        'color': color,
                        'inputdir': current_csv_dirname,
                        'label': 'scénario {}'.format(member)
                    }
                    config["data"]["figure"]['datalist'].append(code)
                # -------------------------------------------------------------
                # Configuration OBS
                # -------------------------------------------------------------
                config["data"][station] = {
                    'color': '0.3,0.3,0.3',
                    'inputdir': OBS_DIRNAME,
                    'linewidth': '2',
                    'label': "Obs"
                }
                config["data"]["figure"]['datalist'].append(station)
                # -------------------------------------------------------------
                # Ecriture de la Configuration
                # -------------------------------------------------------------
                config["data"]["figure"]['datalist'] = ",".join(
                    config["data"]["figure"]['datalist'])
                config.write()
                # -------------------------------------------------------------
                # Création de la figure
                # -------------------------------------------------------------
                print('        - Création de la figure        : {}'
                      ''.format(
                          os.path.join(
                              current_png_dirname,
                              os.path.splitext(config_filename)[0] + '.png')
                          ))
                processArgs = [
                    'python',
                    os.environ['PYSPC_BIN'] + '\\'
                    'plotGrpData.py',
                    '-c', config['filename']
                ]
                if MODE_DEBUG:
                    processArgs.append('-v')
                processRun = subprocess.Popen(
                    processArgs, universal_newlines=True,
                    shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
                processRun.wait()
                if MODE_DEBUG:  # FIGURE PNG
                    break
    # ------------------------------------------------------------------------
    # FIN DU RUN
    # ------------------------------------------------------------------------
        if MODE_DEBUG:  # RUNTIME
            break
    if MODE_DEBUG:  # EVENT
        break
print(' -- Fin du script')
sys.exit(0)
