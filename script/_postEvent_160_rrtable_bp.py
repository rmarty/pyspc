# -*- coding: utf-8 -*-
"""
Created on Mon May  6 11:58:49 2024

@author: renaud.marty

SYNTHESE DES CUMULS JOURNALIERS POUR REX 1 MOIS - ZONAGE BP.

"""
# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import os.path
from pyspc import Config, read_PyspcFile

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = 'config/event.txt'
ZAP_FILENAME = 'config/zap.txt'

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
HOME_CSV = os.path.join(HOME, 'obs', 'table')
CSV_FILENAME = os.path.join(HOME_CSV, 'table_rr_bp.csv')
if not os.path.exists(HOME_CSV):
    os.makedirs(HOME_CSV)
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'zap'): Config.to_listofstr,
})
LAMEDO = Config(filename=os.path.join(HOME, 'config', 'lamedo.txt'))
LAMEDO.read()
LAMEDO.convert({('obs', 'image'): Config.to_listofstr})
ZAP = Config(filename=ZAP_FILENAME)
ZAP.read()
# ------------------------------------
# --- EXPORT LAMEDO - BASSIN
# ------------------------------------
v = 'PJ'
d = os.path.join(HOME, 'obs', 'lamedo', v)
LAMEDO['obs']['image'].append('moy')
img = ','.join(LAMEDO['obs']['image'])
z = ['BP{}_{}'.format(x, img) for x in EVENT['event']['zap']]
series = read_PyspcFile(dirname=d, varnames=v, stations=z)
df = series.concat()

for c in df.columns:
    print('='*50)
    print(c)
    print(ZAP[c[0].replace('BP', '')])
    print(ZAP[c[0].replace('BP', '')]['lib_ap'])

df.columns = [ZAP[c[0].replace('BP', '')]['lib_ap']
              for c in df.columns]
df = df.T.astype(int)
print(df)
df.to_csv(CSV_FILENAME, sep=';')
