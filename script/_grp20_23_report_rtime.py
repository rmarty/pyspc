#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:
"""
import glob
import os.path
import pandas as pnd

from pyspc.model.grp20 import GRP_Cfg

# ======================================================================
# --- PARAMETRES
# ======================================================================
# HOME = os.path.abspath(os.path.dirname(__file__))
HOME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20\__MODELISATION'\
    r'\__GRPv2020'
VERIF_ROOT = os.path.join(HOME, 'LOCAL_TREE')
VERIF_PATTERN = 'synthese_*_00J01H00M_rtime.csv'
CFG_FILENAME = os.path.join(HOME, 'LISTE_BASSINS_RTIME.DAT')
SV_NAMES = ['SV1', 'SV2', 'SV3']
VERIF_COLS = ['Eff_Val', 'POD', 'FAR', 'CSI']


# ======================================================================
# --- FONCTION
# ======================================================================
def find_run(runs, code):
    """Trouver le run GRP correspondant au code"""
    target = [r for r in runs if r.CODE == code]
    if target:
        return target[0]
    return None


# ======================================================================
# --- CHARGEMENT DES DONNEES
# ======================================================================
runs20 = GRP_Cfg(filename=CFG_FILENAME)
runs20.read()
dfsvs = {}
for f in glob.glob(os.path.join(VERIF_ROOT, '*', '*', 'verif', VERIF_PATTERN)):
    c = os.path.basename(f).split('_')[1]
#    print(f"{os.path.relpath(f, HOME)}")
    df = pnd.read_csv(f, sep=';', index_col=0)
    run = find_run(runs20, c)
    rundict = run._asdict()
    svs = [rundict[n] for n in SV_NAMES]
#    print(c, svs)
    for sv, svn in zip(svs, SV_NAMES):
        dfsvs.setdefault(svn, [])
        subdf = df[df.SV == sv][VERIF_COLS]
        if subdf.empty:
            continue
        subdf.index = [c]
        subdf.index.name = 'CODE'
        dfsvs[svn].append(subdf.reset_index(drop=False))
#        print(subdf)
#        break
#    break
#    if c.startswith('K025'):
#        break
# ======================================================================
# --- EXPORT DE LA SYNTHESE
# ======================================================================
DFS_VERIF = {}
for sn in SV_NAMES:
    try:
        df = pnd.concat(dfsvs[sn], ignore_index=True)
    except ValueError:
        continue
    f = os.path.join(
        HOME,
        os.path.basename(os.path.splitext(CFG_FILENAME)[0]) + f"_{sn}.csv")
    df.to_csv(f, sep=';', float_format='%.3f')
    print(f)
