#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_phyc2xml:

.. role:: blue

.. role:: boldblue

Télécharger les données PHyC (_phyc2xml.py)
-------------------------------------------------------

Description
+++++++++++

Télécharger les données (valeurs, statistique) d'images BdImages

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES DONNEES

:boldblue:`OUTPUT_DIRNAME` : :blue:`Répertoire de stockage`

:boldblue:`LOCS` : :blue:`Liste des stations`

:boldblue:`VARNAME` : :blue:`Grandeur`

:boldblue:`DATATYPE` : :blue:`Type de donnée PHyC`

.. rubric:: CONFIGURATION DE LA PERIODE

:boldblue:`FIRST_DT` : :blue:`Premier pas de temps à considérer`

:boldblue:`LAST_DT` : :blue:`Dernier pas de temps à considérer`

:boldblue:`DELTA_DT` : :blue:`Intervalle entre 2 requêtes`

"""

# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt, timedelta as td
import os
import os.path
import subprocess
import sys

from pyspc import Parameter

# ------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   OUTPUT_DIRNAME  : Répertoire de stockage
#   LOCS            : Liste des stations
# ------------------------------------
OUTPUT_DIRNAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\__MODELISATION\__GRPv2020\xml'
LOCS = ['48080004', '07136003']
VARNAME = 'PH'
DATATYPE = 'data_obs_meteo'
#LOCS = ['K637']
#VARNAME = 'PH'
#DATATYPE = 'data_obs_hydro'

# ------------------------------------
# PARAMETRES : CONFIGURATION DE LA PERIODE
#   FIRST_DT : Premier pas de temps à considérer
#   LAST_DT  : Dernier pas de temps à considérer
#   DELTA_DT : Intervalle entre 2 requêtes
# ------------------------------------
FIRST_DT = dt(2024, 6, 1)
LAST_DT = dt(2025, 1, 1)
DELTA_DT = td(days=30)

# ------------------------------------
# DEPENDANCES
# ------------------------------------
PARAMETER = Parameter(varname=VARNAME)
STEP_DT = PARAMETER.timestep
LEN_DT = int((LAST_DT - FIRST_DT) / DELTA_DT) + 1
if not os.path.exists(OUTPUT_DIRNAME):
    os.makedirs(OUTPUT_DIRNAME)

# *****************************************
#               SCRIPT
# *****************************************
for loc in LOCS:
    print("--- Station {}".format(loc))
    for kt in range(LEN_DT):
        t0 = FIRST_DT + kt * DELTA_DT
        tN = FIRST_DT + (kt+1) * DELTA_DT - STEP_DT
        tN = min(tN, LAST_DT)
        print("    - Requête du {0} au {1}".format(
            t0.strftime("%Y-%m-%d %H:%M UTC"),
            tN.strftime("%Y-%m-%d %H:%M UTC")))
        processArgs = [
            'python',
            os.path.join(os.environ['PYSPC_BIN'], 'phyc2xml.py'),
            '-s', loc,
            '-t', DATATYPE,
            '-F', t0.strftime("%Y%m%d%H%M"),
            '-L', tN.strftime("%Y%m%d%H%M"),
            '-n', VARNAME,
            '-c', os.path.join(os.environ['PYSPC_BIN'], 'phyc2xml_RM.txt'),
            '-O', OUTPUT_DIRNAME
        ]
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
#        break
#    break
