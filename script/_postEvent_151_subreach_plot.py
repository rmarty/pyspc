#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_151_subreach_plot:

.. role:: blue

.. role:: boldblue

Synthèse graphiques par sous-tronçon (_postEvent_151_subreach_plot.py)
----------------------------------------------------------------------

Description
+++++++++++

Synthèse graphiques par sous-tronçon

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`COLORS` : :blue:`Liste des couleurs`

:boldblue:`LINESTYLES` : :blue:`Liste des types de ligne`

"""

# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt
import itertools
import os.path
from pyspc import Config, read_PyspcFile, Series  # , read_Sacha
from pyspc.plotting import colors as _colors
from pyspc.plotting.config import Config as PlotConfig

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# ------------------------------------
# PARAMETRES : CONFIGURATION DE LA FIGURE
#   COLORS     : Liste de couleurs pour les sites
# ------------------------------------
COLORS = _colors.TABCOLORS
LINESTYLES = ['-', '--', '-.', ':']
# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'subreaches'): lambda x: os.path.join(
        os.path.dirname(EVENT_FILENAME), x),
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
})
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
LOCS_METEO = Config(filename=os.path.join(HOME, 'config', 'locs_meteo.txt'))
LOCS_METEO.read()
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION SUBREACHES
# ------------------------------------
SUBREACHES = Config(filename=EVENT['event']['subreaches'])
SUBREACHES.read()
SUBREACHES.convert({
    (s, 'sites_hydro'): Config.to_listofstr for s in SUBREACHES
})
SUBREACHES.convert({
    (s, 'stations_hydro'): Config.to_listofstr for s in SUBREACHES
})
SUBREACHES.convert({
    (s, 'sites_meteo'): Config.to_listofstr for s in SUBREACHES
})
# ------------------------------------
# --- LECTURE DES DONNEES
# ------------------------------------
dbase_filename = os.path.join(
    HOME, 'obs', '{}.mdb'.format(EVENT['event']['name']))

for s in SUBREACHES:
    # Lecture des données
    series_q = read_PyspcFile(
        dirname=os.path.join(HOME, 'obs', 'phyc', 'QH'),
        stations=SUBREACHES[s]['sites_hydro'], varnames='QH')
#    series_q =read_Sacha(
#        filename=dbase_filename,
#        codes=SUBREACHES[s]['sites_hydro'],
#        realtime=False,
#        first_dt=EVENT['event']['firstdt'],
#        last_dt=EVENT['event']['lastdt'],
#        spc_varname='QH',
#        prcp_src=None,
#        hydro_version='hydro3',
#        warning=True
#    )
    series_h = read_PyspcFile(
        dirname=os.path.join(HOME, 'obs', 'phyc', 'HH'),
        stations=SUBREACHES[s]['stations_hydro'], varnames='HH')
#    series_h = read_Sacha(
#        filename=dbase_filename,
#        codes=SUBREACHES[s]['stations_hydro'],
#        realtime=False,
#        first_dt=EVENT['event']['firstdt'],
#        last_dt=EVENT['event']['lastdt'],
#        spc_varname='HH',
#        prcp_src=None,
#        hydro_version='hydro3',
#        warning=True
#    )
    series_p = read_PyspcFile(
        dirname=os.path.join(HOME, 'obs', 'phyc', 'PH'),
        stations=SUBREACHES[s]['sites_meteo'], varnames='PH')
#    series_p = read_Sacha(
#        filename=dbase_filename,
#        codes=SUBREACHES[s]['sites_meteo'],
#        realtime=False,
#        first_dt=EVENT['event']['firstdt'],
#        last_dt=EVENT['event']['lastdt'],
#        spc_varname='PH',
#        prcp_src='gauge',
#        hydro_version='hydro3',
#        warning=True
#    )
    series_p = series_p.between_dates(
        first_dtime=EVENT['event']['firstdt'],
        last_dtime=EVENT['event']['lastdt'],
        inplace=False
    )

    series_pcum = series_p.cumsum()
    series = Series(datatype='obs')
    series.extend(series_q)
    series.extend(series_h)
    series.extend(series_pcum)
    series = series.between_dates(
        first_dtime=EVENT['event']['firstdt'],
        last_dtime=EVENT['event']['lastdt'],
        inplace=False
    )
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION IMAGE
# ------------------------------------
    CFGIMG_FILENAME = os.path.join(
        HOME, 'config', os.path.basename(__file__).replace('.py', '.txt'))
    CFGIMG = PlotConfig(filename=CFGIMG_FILENAME)
    CFGIMG.load()
# ------------------------------------
# --- MAJ DE LA CONFIGURATION IMAGE
# ------------------------------------
    CFGIMG['figure']['dirname'] = os.path.join(HOME, 'fig', 'obs')
    if not os.path.exists(CFGIMG['figure']['dirname']):
        os.makedirs(CFGIMG['figure']['dirname'])
    CFGIMG['figure']['filename'] = s
    CFGIMG['figure']['title'] = '{} / {}'.format(
        EVENT['event']['title'],
        SUBREACHES[s]['title'],
    )
# ------------------------------------
# --- CONFIGURATION DES COURBES
# ------------------------------------
    ITER_COLORS = itertools.cycle(COLORS)
    ITER_LINESTYLES = {}
    for x in sorted(series_q.keys()):
        key = '{}_{}'.format(x[0], x[1])
        CFGIMG.setdefault(key, {})
        CFGIMG[key]['color'] = next(ITER_COLORS)
        CFGIMG[key]['linestyle'] = LINESTYLES[0]
        CFGIMG[key]['label'] = '{} ({})'.format(
            LOCS_HYDRO[x[0]]['name'],
            LOCS_HYDRO[x[0]]['river']
        )
        ITER_LINESTYLES.setdefault(x[0], itertools.cycle(LINESTYLES))
    for x in sorted(series_h.keys()):
        key = '{}_{}'.format(x[0], x[1])
        CFGIMG.setdefault(key, {})
        c = 'darkgray'
        ls = '-'
        for k in CFGIMG:
            if k.startswith('{}_'.format(x[0][:8])):
                c = CFGIMG[k]['color']
                ls = next(ITER_LINESTYLES[x[0][:8]])
        CFGIMG[key]['color'] = c
        CFGIMG[key]['linestyle'] = ls
        CFGIMG[key]['label'] = '{}'.format(
            LOCS_HYDRO[x[0]]['name']
        )
    cl = [(c, ls) for ls in LINESTYLES for c in COLORS]
    iter_cl = itertools.cycle(cl)
    for x in sorted(series_pcum.keys()):
        key = '{}_{}'.format(x[0], x[1])
        CFGIMG.setdefault(key, {})
        y = next(iter_cl)
        CFGIMG[key]['color'] = y[0]
        CFGIMG[key]['linestyle'] = y[1]
        CFGIMG[key]['label'] = '{}'.format(
            LOCS_METEO[x[0]]['name']
        )
# ------------------------------------
# --- MAJ DE LA CONFIGURATION IMAGE AVEC VALEURS PAR DEFAUT
# ------------------------------------
    for s in CFGIMG:
        if s in ['figure', 'defaut']:
            continue
        for o in CFGIMG['defaut']:
            CFGIMG[s].setdefault(o, CFGIMG['defaut'][o])
# ------------------------------------
# --- CREATION DE L'IMAGE
# ------------------------------------
    filename = series.plot_series(plottype='subreach', config=CFGIMG)
    print('Création de la figure {}'
          ''.format(os.path.relpath(filename, start=HOME)))
#    break
