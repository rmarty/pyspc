#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:
"""

# ------------------------------------------------------------------------
# IMPORTS
# ------------------------------------------------------------------------
from datetime import datetime as dt
import os.path
import pandas as pnd

from pyspc.model.grp20 import GRP_Basin, GRP_Cfg, GRP_Data, report_model

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES GRP20
# ------------------------------------------------------------------------
HOME = r'D:\Utilisateurs\diane.lebailly\Documents\GRP'
CFG_FILENAME = os.path.join(
    HOME, '00_GRP_v2021', 'Parametrage', 'LISTE_BASSINS.DAT')
BDD_DIRNAME = os.path.join(HOME, '00_Donnees_LACI')
BV_DIRNAME = os.path.join(BDD_DIRNAME, 'Bassins')
DATA_DIRNAMES = {'E': os.path.join(BDD_DIRNAME, 'BDD_E'),
                 'Q': os.path.join(BDD_DIRNAME, 'BDD_Q'),
                 'P': os.path.join(BDD_DIRNAME, 'BDD_P'),
                 'T': os.path.join(BDD_DIRNAME, 'BDD_T')}
LIST_FILENAMES = {
    'P': os.path.join(BDD_DIRNAME, 'LISTE_PLUVIOMETRES.DAT'),
    'T': os.path.join(BDD_DIRNAME, 'LISTE_TEMPERATURES.DAT'),
}
HEADERS = {
    'P': """!--------!--!----------------------------------------!
!Poste P.!RT!Nom Poste Pluviomètre                   !
!--------!--!----------------------------------------!
""",
    'T': """!--------!--!----------------------------------------!
!Poste T.!RT!Nom Poste Température                   !
!--------!--!----------------------------------------!
""",
}
LOCS_METEO = {}
MV_THRESHOLD = 0.10


# *****************************************************************************
#               FONCTIONS
# *****************************************************************************
def describe_data(filename, data_coverage, data_missing):
    """Définir la couverture des données"""
    if filename not in data_coverage:
        # print('   - Ajout des informations {}'
        #       ''.format(os.path.relpath(filename, HOME)))
        reader = GRP_Data(filename=filename)
        df = reader.read()
        nrows = len(df.index)
        nvalid = df.count(numeric_only=True).values[0]
        nmissing = nrows - nvalid
        ratiomissing = nmissing / nrows
        data_missing[filename] = (nrows, nvalid, nmissing, ratiomissing)
        df = df.dropna()
        start = min(df.index).to_pydatetime()
        end = max(df.index).to_pydatetime()
        data_coverage[filename] = (start, end)
    return data_coverage, data_missing


# *****************************************************************************
#               SCRIPT
# *****************************************************************************
print("-- Synthèse des informations de modélisation")
data_coverage = {}
data_missing = {}
# ---------------------------------------------------------------------
# --- 1 - Lecture des informations générales modèles
# ---------------------------------------------------------------------
runs20 = GRP_Cfg(filename=CFG_FILENAME)
runs20.read()
for run in runs20:
    run_start = dt.strptime(run.DEB, '%d/%m/%Y %H:%M')
    run_end = dt.strptime(run.FIN, '%d/%m/%Y %H:%M')
# ---------------------------------------------------------------------
# --- 2 - Lecture des informations spécifiques modèles
# ---------------------------------------------------------------------
    bv_filename = os.path.join(
        BV_DIRNAME,
        GRP_Basin.join_basename(location=run.CODE, timestep=run.PDT))
    if not os.path.exists(bv_filename):
        print('!!! Fichier introuvable: {}'
              ''.format(os.path.relpath(bv_filename, HOME)))
        continue
    try:
        bv_info = GRP_Basin(filename=bv_filename)
        bv_info.read()
    except Exception:
        print('!!! Fichier mal formaté {bv_filename}')
        continue
    print(' - Traitement du modèle {}'
          ''.format(os.path.relpath(bv_filename, HOME)))
# ---------------------------------------------------------------------
# --- 3 - Lecture des informations Q
# ---------------------------------------------------------------------
    v = 'Q'
    d = DATA_DIRNAMES[v]
    vf = os.path.join(d, GRP_Data.join_basename(station=run.CODE, varname=v))
    try:
        data_coverage, data_missing = describe_data(
            vf, data_coverage, data_missing)
    except Exception:
        print(f'!!! Fichier absent => {vf}')
        continue
    if data_coverage[vf][0] > run_start:
        print('!!! Date de début à revoir: {} != {} pour {}'
              ''.format(data_coverage[vf][0], run_start,
                        os.path.basename((vf))))
        bv_info['start'] = data_coverage[vf][0]
    else:
        bv_info['start'] = run_start
    if data_coverage[vf][1] < run_end:
        print('!!! Date de fin à revoir: {} != {} pour {}'
              ''.format(data_coverage[vf][1], run_end,
                        os.path.basename((vf))))
        bv_info['end'] = data_coverage[vf][1]
    else:
        bv_info['end'] = run_end
    if data_missing[vf][-1] > MV_THRESHOLD:
        print('!!! Taux de valeurs manquantes élevé: {} > {}'.format(
            data_missing[vf][-1], MV_THRESHOLD))
    bv_info['mv'] = data_missing[vf][-1]
# ---------------------------------------------------------------------
# --- 4 - Lecture des informations ETP
# ---------------------------------------------------------------------
    v = 'E'
    d = DATA_DIRNAMES[v]
    vf = os.path.join(d, GRP_Data.join_basename(
        station=run.CODE, varname=v, timestep='01J00H00M'))
    try:
        data_coverage, data_missing = describe_data(
            vf, data_coverage, data_missing)
    except Exception:
        print(f'!!! Fichier absent => {vf}')
        continue
    if data_coverage[vf][0] > run_start:
        print('!!! Date de début à revoir: {} != {} pour {}'
              ''.format(data_coverage[vf][0], run_start,
                        os.path.basename((vf))))
    if data_coverage[vf][1] < run_end:
        print('!!! Date de fin à revoir: {} != {} pour {}'
              ''.format(data_coverage[vf][1], run_end,
                        os.path.basename((vf))))
    if data_missing[vf][-1] > MV_THRESHOLD:
        print('!!! Taux de valeurs manquantes élevé: {} > {}'.format(
            data_missing[vf][-1], MV_THRESHOLD))
# ---------------------------------------------------------------------
# --- 5 - Lecture des informations P, T
# ---------------------------------------------------------------------
    for v in ['P', 'T']:
        LOCS_METEO.setdefault(v, {})
        d = DATA_DIRNAMES[v]
        if v not in bv_info:
            continue
        for s in bv_info[v]:
            n = bv_info[v][s].get('n', "")
            vf = os.path.join(d, GRP_Data.join_basename(
                station=s, varname=v, timestep=bv_info[v][s].get('t', None)))
            try:
                data_coverage, data_missing = describe_data(
                    vf, data_coverage, data_missing)
            except Exception:
                print(f'!!! Fichier absent => {vf}')
                continue
            bv_info[v][s]['start'] = data_coverage[vf][0]
            bv_info[v][s]['end'] = data_coverage[vf][1]
            if s not in LOCS_METEO[v] or not LOCS_METEO[v][s]:
                LOCS_METEO[v][s] = n
            bv_info[v][s]['mv'] = data_missing[vf][-1]
            if bv_info[v][s]['mv'] > MV_THRESHOLD:
                print('!!! Taux de valeurs manquantes élevé pour {}: {} > {}'
                      ''.format((s, v), bv_info[v][s]['mv'], MV_THRESHOLD))
# ---------------------------------------------------------------------
# --- 6 - Lecture des informations Hypsométriques
# ---------------------------------------------------------------------
    hf = os.path.join(BV_DIRNAME, '{}-hypso.txt'.format(run.CODE))
    if os.path.exists(hf):
        print('   - Courbe hypsométrique {}'.format(os.path.relpath(hf, HOME)))
        hypso = pnd.read_csv(hf, sep=';')
        hypso.columns = ['pct', 'Z']
        hypso.pct = hypso.pct * 100
        hypso.pct = hypso.pct.astype(int)
    else:
        hypso = None
# ---------------------------------------------------------------------
# --- 7 - Ecriture du rapport modele en PDF
# ---------------------------------------------------------------------
    pdf_filename = os.path.splitext(bv_filename)[0] + '.pdf'
    print('   - Rapport PDF {}'
          ''.format(os.path.relpath(pdf_filename, HOME)))
    report_model(pdf_filename, run, bv_info, hypso, hypso_lim=(0, 2000))

    # break

# ---------------------------------------------------------------------
# --- 8 - Ecriture des fichiers LIST_PLUVIOMETRES/TEMPERATURE
# ---------------------------------------------------------------------
for v in LOCS_METEO:
    print(LIST_FILENAMES[v])
    with open(LIST_FILENAMES[v], 'w', encoding='ansi', newline='\r\n') as f:
        f.write(HEADERS[v])
        for s in sorted(LOCS_METEO[v]):
            f.write('!{}!TU!{}\n'.format(s, LOCS_METEO[v][s]))
