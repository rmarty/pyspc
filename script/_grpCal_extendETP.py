#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_grpCal_extendETP:

.. role:: blue

.. role:: boldblue

Extension des données ETP des bassins GRP (_grpCal_extendETP.py)
----------------------------------------------------------------

Description
+++++++++++

Paramètres
++++++++++

.. rubric:: Configuration des donnees

:boldblue:`DATA_DIRNAME` : :blue:`Répertoire des données GRP`

:boldblue:`DATA_DIRNAME_BV` : :blue:`Répertoire des fichiers bassin`

:boldblue:`DATA_DIRNAME_E` : :blue:`Répertoires des données d'ETP`

:boldblue:`OUTPUT_DIRNAME` : :blue:`Répertoire de sortie`

.. rubric:: Configuration de la periode de calcul

:boldblue:`FIRST_DT` : :blue:`Premier pas de temps à considérer`

:boldblue:`LAST_DT` : :blue:`Dernier pas de temps à considérer`

:boldblue:`SRC_YEAR` : :blue:`Année à dupliquer`

"""
# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import os
import os.path
import sys
import subprocess
import glob

from pyspc.model.grp16 import GRP_Basin
import pyspc.core.exception as _exception

# -----------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   DATA_DIRNAME    : Répertoire des données GRP
#   DATA_DIRNAME_BV : Répertoire des fichiers bassin
#   DATA_DIRNAME_E  : Répertoires des données d'ETP
#   OUTPUT_DIRNAME  : Répertoire de sortie
# -----------------------------------------------------------------------------
DATA_DIRNAME = r'D:\2-grp-3.3\20161026_BDD'
DATA_DIRNAME_BV = os.path.join(DATA_DIRNAME, 'Bassins')
DATA_DIRNAME_E = os.path.join(DATA_DIRNAME, 'BDD_E', '2014')
OUTPUT_DIRNAME = os.path.join(DATA_DIRNAME, 'BDD_E')

# ------------------------------------
# PARAMETRES : CONFIGURATION DE LA PERIODE DE CALCUL
#   FIRST_DT : Premier pas de temps à considérer
#   LAST_DT  : Dernier pas de temps à considérer
#   SRC_YEAR : Année à dupliquer
# ------------------------------------
FIRST_DT = '1993010100'
LAST_DT = '2016123123'
SRC_YEAR = '2008'

# ------------------------------------
# DEPENDANCES
# ------------------------------------
# *****************************************
#               SCRIPT
# *****************************************
# ------------------------------------
# BOUCLE SUR LES BASSINS CONCERNES
# ------------------------------------
files = glob.glob(DATA_DIRNAME_BV + "/" + 'K*.dat')
for file in files:
    # Détermination du code de la station DEBIT
    print("   + Fichier Bassin : {0}".format(file))
    station_Q = os.path.splitext(os.path.basename(file))[0]
    # Lecture du fichier Kxxx.dat:
    # extraction des stations E, P et leurs poids respectifs
    bassin_info = GRP_Basin(filename=file)
    bassin_info.read(version=2)
    if 'E' not in bassin_info:
        continue
    if bassin_info['E'] is None:
        continue
    for station_E in bassin_info['E']:
        etpInputFileName = os.path.join(DATA_DIRNAME_E, station_E + '_EH.txt')
        if os.path.exists(etpInputFileName):
            # ------------------------------------
            # INFOS AVANT EXTENSION
            # ------------------------------------
            processArgs = [
                'python',
                os.environ['PYSPC_BIN'] + '\\'
                'grpInfo.py',
                '-I', os.path.dirname(etpInputFileName),
                '-s', station_E,
                '-n', 'EH',
                '-M', 'missing_value'
                ]
            processRun = subprocess.Popen(
                processArgs, universal_newlines=True,
                shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
            processRun.wait()
            # -------------------------------------
            # EXTENSION DE LA SERIE PAR DUPLICATION
            # -------------------------------------
            etpOutputFileName = OUTPUT_DIRNAME + os.sep + station_E + '_EH.txt'
            processArgs = [
                'python',
                os.environ['PYSPC_BIN'] + '\\'
                'grp2grp.py',
                '-I', os.path.dirname(etpInputFileName),
                '-s', station_E,
                '-n', 'EH',
                '-O', os.path.dirname(etpOutputFileName),
                '-M', 'cpy', station_E, SRC_YEAR,
                FIRST_DT, LAST_DT
                ]
            processRun = subprocess.Popen(
                processArgs, universal_newlines=True,
                shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
            processRun.wait()
            # ------------------------------------
            # INFOS APRES EXTENSION
            # ------------------------------------
            processArgs = [
                'python',
                os.environ['PYSPC_BIN'] + '\\'
                'grpInfo.py',
                '-I', os.path.dirname(etpOutputFileName),
                '-s', station_E,
                '-n', 'EH',
                '-M', 'missing_value'
                ]
            processRun = subprocess.Popen(
                processArgs, universal_newlines=True,
                shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
            processRun.wait()
        else:
            _exception.Warning(
                __name__,
                "fichier ETP '{0}' absent".format(etpInputFileName))
