#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_grp20_02_16to20_etp:

.. role:: blue

.. role:: boldblue

Conversion des fichiers bassins de GRPv2016 à GRPv2020 (_grp20_02_16to20_etp.py)
--------------------------------------------------------------------------------

Description
+++++++++++

Conversion des fichiers d'ETP de GRP, de la version 2016 à la version 2020

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES DONNEES GRP16

:boldblue:`BDD_16` : :blue:`Répertoire de la base de données`

:boldblue:`ETP_16` : :blue:`Répertoire des fichiers ETP`

.. rubric:: CONFIGURATION DES DONNEES GRP20

:boldblue:`BDD_20` : :blue:`Répertoire de la base de données`

:boldblue:`ETP_20` : :blue:`Répertoire des fichiers ETP`

"""
# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------------------------------------------
# IMPORTS
# ------------------------------------------------------------------------
import glob
import os
import os.path

from pyspc.io.grp16 import read_GRP16

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES GRP16
# ------------------------------------------------------------------------
BDD_16 = r'D:\Utilisateurs\renaud.marty\Documents\2-grp-3.3\01_BDD'
ETP_16 = os.path.join(BDD_16, 'BDD_E')

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES GRP20
# ------------------------------------------------------------------------
BDD_20 = r'D:\Utilisateurs\renaud.marty\Documents\2-grp-2020\00_Donnees_LACI'
ETP_20 = os.path.join(BDD_20, 'BDD_E')
if not os.path.exists(ETP_20):
    os.makedirs(ETP_20)

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
print("-- Conversion des fichiers ETP de GRPv2016 à GRPv2020")
bvfiles = glob.glob(os.path.join(ETP_16, '*EH.txt'))
for f in bvfiles:
    if not os.path.splitext(os.path.basename(f))[0].startswith('K040'):
        continue
    # ------------------------------------------------------------------------
    # --- LECTURE DU FICHIER GRP v2016
    # ------------------------------------------------------------------------
    try:
        series = read_GRP16(filename=f, datatype='grp16_cal_data')
        print('   -> Lecture du fichier v2016  : {}'
              ''.format(os.path.relpath(f, BDD_16)))
    except Exception:
        print('Avertissement: chargement incorrect v2016')
        continue
    # ------------------------------------------------------------------------
    # --- ECRITURE DU FICHIER BASSIN GRP v2020
    # ------------------------------------------------------------------------
    try:
        ffs = series.to_GRP_Data(dirname=ETP_20, version='2020')
        for fs in ffs:
            print('   -> Ecriture du fichier v2020 : {}'
                  ''.format(os.path.relpath(fs, BDD_20)))
    except Exception:
        print('Avertissement: écriture incorrecte v2020')
