#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_grpCal_computeETP:

.. role:: blue

.. role:: boldblue

Calcul d'ETP (_grpCal_computeETP.py)
-------------------------------------------------------

Description
+++++++++++

Calcul des ETP à partir de séries de température horaires, les latitudes des stations et selon la pondération du bassin

Paramètres
++++++++++

.. rubric:: Configuration des donnees

:boldblue:`BDD_DIRNAME` : :blue:`Répertoire des données GRP`

:boldblue:`BV_DIRNAME` : :blue:`Répertoire des fichiers bassin`

:boldblue:`DATA_DIRNAME` : :blue:`Répertoires des données, par variable`

:boldblue:`COORD_FILENAME` : :blue:`Fichier contenant les latitudes des stations`

:boldblue:`VARNAMES` : :blue:`Correspondance des variables GRP <-> SPC`

:boldblue:`TIMESTEP` : :blue:`Pas de temps`

"""
# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import collections
import glob
import os
import os.path
import subprocess
import sys

from pyspc.model.grp16 import GRP_Basin

# -----------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   BDD_DIRNAME     : Répertoire des données GRP
#   BV_DIRNAME      : Répertoire des fichiers bassin
#   DATA_DIRNAME    : Répertoires des données, par variable
#   COORD_FILENAME  : Fichier contenant les latitudes des stations
#   VARNAMES        : Correspondance des variables GRP <-> SPC
#   TIMESTEP        : Pas de temps
# -----------------------------------------------------------------------------
MODE_DEBUG = False
BDD_DIRNAME = r'D:\2-grp-3.3\20190426_BDD'
BV_DIRNAME = os.path.join(BDD_DIRNAME, 'Bassins')
DATA_DIRNAME = {
    'E': os.path.join(BDD_DIRNAME, 'BDD_E'),
    'ETP': os.path.join(BDD_DIRNAME, 'BDD_E', 'ETPfromTH'),
    'T': os.path.join(BDD_DIRNAME, 'BDD_T'),
}
COORD_FILENAME = os.path.join(DATA_DIRNAME['ETP'], 'coord_stations.csv')
VARNAMES = {
    'T': 'TH',
    'E': 'EH'
}
TIMESTEP = 'hourly'

# -----------------------------------------------------------------------------
# DEPENDANCES
# -----------------------------------------------------------------------------
STATIONS = collections.OrderedDict()
COORD = collections.OrderedDict()
with open(COORD_FILENAME, 'r', encoding='utf-8') as f:
    header = f.readline().strip('\n').split(';')
    for line in f.readlines():
        line = line.strip('\n').split(';')
        key = line[0]
        COORD.setdefault(key, {h: line[k] for k, h in enumerate(header)})

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
print("-- Calcul des ETP horaires à partir des séries TH pour GRP Calage")
# -----------------------------------------------------------------------------
# LISTER LES BASSINS CONCERNES
# -----------------------------------------------------------------------------
filenames = glob.glob(os.path.join(BV_DIRNAME, 'K*.dat'))
for filename in filenames:
    basename = os.path.basename(filename)
    # Détermination du code de la station DEBIT
    print("   + Fichier Bassin : {0}".format(filename))
    station_Q = os.path.splitext(os.path.basename(filename))[0]
    print("     - Station DEBIT '{0}'".format(station_Q))
    # Init stations
    STATIONS.setdefault(station_Q, {'TH': [], 'w': []})
    # extraction des stations E, P et leurs poids respectifs
    bassin_info = GRP_Basin(filename=filename)
    bassin_info.read(version=3)
    if 'T' in bassin_info and bassin_info['T'] is not None:
        for station_T in bassin_info['T']:
            print("     - Station TEMP  '{0}' : {1}"
                  "".format(station_T, bassin_info['T'][station_T]))
            name = bassin_info['T'][station_T].get('n', None)
            STATIONS[station_Q]['TH'].append(station_T)
            STATIONS[station_Q]['w'].append(
                bassin_info['T'][station_T].get('w', None))
    if MODE_DEBUG:
        break
# -----------------------------------------------------------------------------
# CALCUL TH => ETP à la station
# -----------------------------------------------------------------------------
codes = list({t
              for q in STATIONS
              for t in STATIONS[q]['TH']
              if t in COORD})
for code in codes:
    print("   + Conversion TH vers EH de la station {}".format(code))
    processArgs = [
        'python',
        os.environ['PYSPC_BIN'] + '\\'
        'grp2grp.py',
        '-I', DATA_DIRNAME['T'],
        '-s', code,
        '-n', VARNAMES['T'],
        '-O', DATA_DIRNAME['ETP'],
        '-M', 'etp', TIMESTEP, COORD[code]['LAT']
    ]
    if MODE_DEBUG:
        processArgs.append('-v')
        print(' '.join(processArgs))
    processRun = subprocess.Popen(
        processArgs, universal_newlines=True,
        shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
    processRun.wait()
# -----------------------------------------------------------------------------
# ETP à la station => ETP Bassin
# -----------------------------------------------------------------------------
for basin in STATIONS:
    print("   + Calcul de l'ETP bassin pour {}".format(basin))
    basin_stations_filename = os.path.join(
        DATA_DIRNAME['ETP'],
        '{}_codes_TH.csv'.format(basin)
    )
    print('     - Liste des stations source : {}'
          ''.format(basin_stations_filename))
    with open(basin_stations_filename, 'w',
              encoding='utf-8', newline='\n') as f:
        for t in STATIONS[basin]['TH']:
            f.write('{}\n'.format(t))
    basin_weigths_filename = os.path.join(
        DATA_DIRNAME['ETP'],
        '{}_poids_TH.csv'.format(basin)
    )
    print('     - Liste des pondérations    : {}'
          ''.format(basin_weigths_filename))
    with open(basin_weigths_filename, 'w',
              encoding='utf-8', newline='\n') as f:
        for t in STATIONS[basin]['w']:
            f.write('{}\n'.format(t))
    print("     - Application moyenne pondérée")
    processArgs = [
        'python',
        os.environ['PYSPC_BIN'] + '\\'
        'grp2grp.py',
        '-I', DATA_DIRNAME['ETP'],
        '-l', basin_stations_filename,
        '-n', VARNAMES['E'],
        '-O', DATA_DIRNAME['ETP'],
        '-M', 'wav', basin_weigths_filename
    ]
    if MODE_DEBUG:
        processArgs.append('-v')
        print(' '.join(processArgs))
    processRun = subprocess.Popen(
        processArgs, universal_newlines=True,
        shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
    processRun.wait()
    etp_tmpname = os.path.join(
        DATA_DIRNAME['ETP'],
        '{}_{}_{}.txt'.format(
            os.path.basename(basin_stations_filename).replace('.csv', ''),
            os.path.basename(basin_weigths_filename).replace('.csv', ''),
            VARNAMES['E']
        )
    )
    if os.path.exists(etp_tmpname):
        etp_filename = os.path.join(
            DATA_DIRNAME['ETP'],
            '{}_{}.txt'.format(basin, VARNAMES['E'])
        )
        print("     - Fichier ETP horaire       : {}".format(etp_filename))
        os.rename(etp_tmpname, etp_filename)
print(' -- Fin du script')
sys.exit(0)
