#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_grpCal_sacha2grp:

.. role:: blue

.. role:: boldblue

Création de la base de données GRP à partir des données SACHA (_grpCal_sacha2grp.py)
------------------------------------------------------------------------------------

Description
+++++++++++

Les fichiers nécessaires pour GRP sont créés automatiquement à partir de fichiers de bassin GRP et de bases SACHA

Paramètres
++++++++++

.. rubric:: CONFIGURATION DE GRP

:boldblue:`BDD_DIRNAME` : :blue:`Répertoire des données GRP`

:boldblue:`BV_DIRNAME` : :blue:`Répertoire des fichiers bassin`

:boldblue:`DATA_DIRNAME` : :blue:`Répertoires des données, par variable`

:boldblue:`VARNAMES` : :blue:`Correspondance des variables GRP <-> SPC`

.. rubric:: CONFIGURATION DE SACHA

:boldblue:`SACHA_DIRNAME` : :blue:`Répertoir des bases SACHA`

:boldblue:`SACHA_FILENAMES` : :blue:`Base Sacha par couple (variable, fournisseur)`

:boldblue:`HYDRO_VERSIONS` : :blue:`Codification Hydro2 ou Hydro3 par couple (variable, fournisseur)`

:boldblue:`SUFFIX` : :blue:`Suffixe du nom des postes pour reconnaitre le fournisseur`

.. rubric:: CONFIGURATION TEMPORELLE

:boldblue:`FIRST_DT` : :blue:`Premier pas de temps`

:boldblue:`LAST_DT` : :blue:`Dernier pas de temps`

.. rubric:: CONFIGURATION DES EXPORTS

:boldblue:`EXPORTS` : :blue:`Couples (variable, fournisseur) à exporter`

"""
# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------------------------------------------
# IMPORTS
# ------------------------------------------------------------------------
import glob
import os
import os.path
import subprocess
import sys

from pyspc.model.grp16 import GRP_Basin

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   BDD_DIRNAME       : Répertoire des données GRP
#   BV_DIRNAME        : Répertoire des fichiers bassin
#   DATA_DIRNAME      : Répertoires des données, par variable
#   SACHA_DIRNAME     : Répertoir des bases SACHA
#   SACHA_FILENAMES   : Base Sacha par couple (variable, fournisseur)
#   FIRST_DT          : Premier pas de temps
#   LAST_DT           : Dernier pas de temps
#   VARNAMES          : Correspondance des variables GRP <-> SPC
#   HYDRO_VERSIONS    : Codification Hydro2 ou Hydro3 par couple (variable, fournisseur)
#   SUFFIX            : Suffixe du nom des postes pour reconnaitre le fournisseur
#   EXPORTS           : Couples (variable, fournisseur) à exporter
# ------------------------------------------------------------------------
BDD_DIRNAME = r'D:\2-grp-3.3\20190426_BDD'
BV_DIRNAME = os.path.join(BDD_DIRNAME, 'Bassins')
DATA_DIRNAME = {
    'P': os.path.join(BDD_DIRNAME, 'BDD_P'),
    'Q': os.path.join(BDD_DIRNAME, 'BDD_Q'),
    'T': os.path.join(BDD_DIRNAME, 'BDD_T'),
}
SACHA_DIRNAME = 'D:\\1-data\\0-sacha'
SACHA_FILENAMES = {
    ('P', 'CRISTAL'): 'base_Pluies_CRISTAL.mdb',
    ('P', 'EDF'): 'base_Pluies_EDF.mdb',
    ('P', 'MF'): 'base_Pluies_MF.mdb',
    ('Q', 'CRISTAL'): 'base_CEMAGREF_Debits.mdb',
    ('T', 'EDF'): 'base_Temperatures_EDF.mdb',
    ('T', 'MF'): 'base_Temperatures_MF.mdb'
}
FIRST_DT = '1992080100'
LAST_DT = '2019033123'
VARNAMES = {
    'P': 'PH',
    'T': 'TH',
    'Q': 'QH'
}
HYDRO_VERSIONS = {
    ('P', 'CRISTAL'): '-2',
    ('P', 'EDF'): '-2',
    ('P', 'MF'): '-2',
    ('Q', 'CRISTAL'): '-3',
    ('T', 'EDF'): '-2',
    ('T', 'MF'): '-2'
}
#DISJOIN_CFG = {
#    'MF': ['EDF', 'CRISTAL'],
#    'CRISTAL': ['EDF', 'MF'],
#    'EDF': ['MF', 'CRISTAL']
#}
#DISJOIN = True
SUFFIX = {
    'MF': None,
    'CRISTAL': '-SPC',
    'EDF': '-EDF'
}
EXPORTS = [
    ('P', 'CRISTAL'),
    ('P', 'EDF'),
    ('P', 'MF'),
    ('Q', 'CRISTAL'),
    ('T', 'EDF'),
    ('T', 'MF')
]

# ------------------------------------------------------------------------
# DEPENDANCES
# ------------------------------------------------------------------------
STATIONS = {x: [] for x in SACHA_FILENAMES}

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
print("-- Exports des données Sacha pour GRP Calage")
# ------------------------------------------------------------------------
# LISTER LES BASSINS CONCERNES
# ------------------------------------------------------------------------
filenames = glob.glob(os.path.join(BV_DIRNAME, 'K*.dat'))
for filename in filenames:
    basename = os.path.basename(filename)
    # Détermination du code de la station DEBIT
    print("   + Fichier Bassin : {0}".format(filename))
    station_Q = os.path.splitext(os.path.basename(filename))[0]
    print("     - Station DEBIT '{0}'".format(station_Q))
    # Affectation Débit Cristal
    STATIONS[('Q', 'CRISTAL')].append(station_Q)
    # Lecture du fichier Kxxx.dat:
    # extraction des stations E, P et leurs poids respectifs
    bassin_info = GRP_Basin(filename=filename)
    bassin_info.read(version=3)
    if 'E' in bassin_info and bassin_info['E'] is not None:
        for station_E in bassin_info['E']:
            print("     - Station ETP   '{0}' : {1}"
                  "".format(station_E, bassin_info['E'][station_E]))
    if 'P' in bassin_info and bassin_info['P'] is not None:
        for station_P in bassin_info['P']:
            print("     - Station PRCP  '{0}' : {1}"
                  "".format(station_P, bassin_info['P'][station_P]))
            name = bassin_info['P'][station_P].get('n', None)
            if name is not None:
                if name.endswith(SUFFIX['CRISTAL']):
                    STATIONS[('P', 'CRISTAL')].append(station_P)
                elif name.endswith(SUFFIX['EDF']):
                    STATIONS[('P', 'EDF')].append(station_P)
                    print('EDF', name, station_P)
                else:
                    STATIONS[('P', 'MF')].append(station_P)
            else:
                STATIONS[('P', 'MF')].append(station_P)
    if 'T' in bassin_info and bassin_info['T'] is not None:
        for station_T in bassin_info['T']:
            print("     - Station TEMP  '{0}' : {1}"
                  "".format(station_T, bassin_info['T'][station_T]))
            name = bassin_info['T'][station_T].get('n', None)
            if name is not None:
                if name.endswith(SUFFIX['EDF']):
                    STATIONS[('T', 'EDF')].append(station_T)
                else:
                    STATIONS[('T', 'MF')].append(station_T)
            else:
                STATIONS[('T', 'MF')].append(station_T)

    sys.stdout.flush()
# Nettoyage des listes de stations P,Q par suppression des doublons
for key in STATIONS:
    if STATIONS[key]:
        STATIONS[key] = list(set(STATIONS[key]))
        STATIONS[key].sort()

#STATIONS[('P', 'EDF')] = copy.deepcopy(STATIONS[('P', 'MF')])
#STATIONS[('T', 'EDF')] = copy.deepcopy(STATIONS[('T', 'MF')])
#STATIONS[('P', 'CRISTAL')] = copy.deepcopy(STATIONS[('P', 'MF')])
#STATIONS[('T', 'CRISTAL')] = copy.deepcopy(STATIONS[('T', 'MF')])

sys.stdout.flush()

# ------------------------------------------------------------------------
# EXTRAIRE LES DONNEES
# ------------------------------------------------------------------------
for export in EXPORTS:
    print("   + Export : {0}".format(export))
    varname = export[0]
    provider = export[1]
    if export not in SACHA_FILENAMES:
        continue
    if export not in STATIONS:
        continue
    if not STATIONS[export]:
        continue
    if varname not in VARNAMES:
        continue
    if varname not in DATA_DIRNAME:
        continue
    if export not in HYDRO_VERSIONS:
        continue
    for station in STATIONS[export]:
        processArgs = [
            'python',
            os.environ['PYSPC_BIN'] + '\\'
            'dbase2grp.py',
            '-I', SACHA_DIRNAME,
            '-d', SACHA_FILENAMES[export],
            '-t', 'sacha',
            '-s', station,
            '-n', VARNAMES[varname],
            '-O', DATA_DIRNAME[varname],
            '-F', FIRST_DT,
            '-L', LAST_DT,
            HYDRO_VERSIONS[export]
            ]
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        filename = os.path.join(
            DATA_DIRNAME[varname],
            station + '_{}.txt'.format(varname)
        )
        if os.path.exists(filename):
            processArgs = [
                'python',
                os.environ['PYSPC_BIN'] + '\\'
                'grpInfo.py',
                '-I', os.path.dirname(filename),
                '-s', station,
                '-n', VARNAMES[varname],
                '-M', 'missing_value'
                ]
            processRun = subprocess.Popen(
                processArgs, universal_newlines=True,
                shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
            processRun.wait()
            sys.stdout.flush()
#            if DISJOIN and provider in DISJOIN_CFG:
#                for p in DISJOIN_CFG[provider]:
#                    if (varname, p) in EXPORTS and (varname, p) in STATIONS:
#                        STATIONS[(varname, p)].remove(station)
print("-- Fin des exports des données Sacha pour GRP Calage")
