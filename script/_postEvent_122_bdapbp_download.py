#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_122_bdapbp_download:

.. role:: blue

.. role:: boldblue

Télécharger les BP depuis BDApBp (Lamedo) (_postEvent_122_bdapbp_download.py)
-----------------------------------------------------------------------------

Description
+++++++++++

Télécharger les BP depuis BDApBp (Lamedo)

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

"""


# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt, timedelta as td
import os.path
import pandas as pnd
import subprocess
import sys
from pyspc import Config

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'config\event.txt'

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
    ('event', 'zap'): Config.to_listofstr,
})
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()

LAMEDO = Config(filename=os.path.join(HOME, 'config', 'lamedo.txt'))
LAMEDO.read()
# ------------------------------------
# --- PARAMETRE SUBPROCESS
# ------------------------------------
START = EVENT['event']['firstdt'] - td(days=3)
END = EVENT['event']['lastdt'] + td(hours=EVENT['event']['maxltime'])
RUNS = [r for r in pnd.date_range(START, END, freq='H') if r.hour in [8, 14]]

# ------------------------------------
# --- EXPORT LAMEDO - BDAPBP
# ------------------------------------
d = os.path.join(HOME, 'fcst', 'bp')
if not os.path.exists(d):
    print('Création du répertoire {}'
          ''.format(os.path.relpath(d, start=HOME)))
    os.makedirs(d)
print('Téléchargement des BP')
for r in RUNS:
    processArgs = [
        'python',
        os.path.join(os.environ['PYSPC_BIN'], 'bdapbp2json.py'),
        '-O', d,
        '-S', '+'.join(EVENT['event']['zap']),
        '-r', r.strftime('%Y%m%d%H'),
        '-t', 'long',
        '-c', LAMEDO.filename
    ]
    processRun = subprocess.Popen(
        processArgs, universal_newlines=True,
        shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
    processRun.wait()
