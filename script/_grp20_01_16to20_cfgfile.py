#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_grp20_01_16to20_cfgfile:

.. role:: blue

.. role:: boldblue

Conversion des fichiers de runs de GRPv2016 à GRPv2020 (_grp20_01_16to20_cfgfile.py)
------------------------------------------------------------------------------------

Description
+++++++++++

Conversion des fichiers de runs de GRP, de la version 2016 à la version 2020

Paramètres
++++++++++

.. rubric:: CONFIGURATION DES DONNEES GRP16

:boldblue:`BDD_16` : :blue:`Répertoire de configuration de GRP (Paramétrage)`

:boldblue:`CFG_16` : :blue:`Fichier de configuration`

:boldblue:`CODES` : :blue:`Liste des bassins à exporter`


.. rubric:: CONFIGURATION DES DONNEES GRP20

:boldblue:`BDD_20` : :blue:`Répertoire de configuration de GRP (Paramétrage)`

:boldblue:`CFG_20` : :blue:`Fichier de configuration`

"""
# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------------------------------------------
# IMPORTS
# ------------------------------------------------------------------------
from datetime import timedelta as td
import os
import os.path

from pyspc.model.grp16 import GRP_Cfg as GRP_Cfg16
from pyspc.model.grp20 import (
    GRP_Cfg as GRP_Cfg20, GRP_Run as GRP_Run20, td2str)

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES GRP16
# ------------------------------------------------------------------------
BDD_16 = r'D:\Utilisateurs\renaud.marty\Documents\2-grp-3.3\GRP_16r1628'\
    r'\Parametrage'
CFG_16 = os.path.join(BDD_16, 'LISTE_BASSINS_CALTR.DAT')
CODES = [
    'K0114030', 'K0134010',
    'K0214010', 'K0243010', 'K0253020', 'K0258010', 'K0260010', 'K0274010',
    'K0333010', 'K0356310', 'K0403010', 'K0454010',
    'K0523010', 'K0550010', 'K0567520',
    'K0614010', 'K0624510', 'K0643110', 'K0673310',
    'K0753210', 'K0813020', 'K0983011', 'K1063010', 'K1084020',
    'K1173210', 'K1251810', 'K1273110', 'K1284810',
    'K1314010', 'K1321810', 'K1363010', 'K1383010',
    'K1414010', 'K1533010',
    'K1713010', 'K1724210', 'K1753110', 'K1764010', 'K1773010',
    'K1833010', 'K1914510', 'K1954010',
    'K4013010', 'K4073110', 'K4123010', 'K4572210', 'K4873120',
    'K5090900', 'K5183020', 'K5234010', 'K5383020', 'K5433020',
    'K5552300', 'K5574100', 'K5623010',
    'K6022420', 'K6102430', 'K6173130', 'K6334010', 'K6373020',
    'K6453010', 'K6593020',
    'K7022620', 'K7143010', 'K7202610', 'K7433030', 'K7514010'
]

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES GRP20
# ------------------------------------------------------------------------
BDD_20 = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\__MODELISATION\__GRPv2020\CAL_LCI'
CFG_20 = os.path.join(BDD_20, 'LISTE_BASSINS_LCI.DAT')
if not os.path.exists(BDD_20):
    os.makedirs(BDD_20)

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
print("-- Conversion des fichiers de runs de GRPv2016 à GRPv2020")
print('   -> Lecture du fichier v2016  : {}'
      ''.format(os.path.relpath(CFG_16, BDD_16)))
runs16 = GRP_Cfg16(filename=CFG_16)
runs16.read()
runs20 = GRP_Cfg20(filename=CFG_20)
rs20 = []
for r16 in runs16:
    if r16.CODE not in CODES:
        continue
    r20 = {
        'CODE': r16.CODE, 'PDT': '00J01H00M', 'NOM': r16.NOM,
        'SURFACE': r16.SURFACE, 'RT': r16.RT,
        'DEB': r16.DEB + ' 00:00', 'FIN': r16.FIN + ' 00:00',
        'ST': r16.ST, 'SR': r16.SR, 'AT': r16.AT, 'AR': r16.AR,
        'HOR1': td2str(r16.HOR * td(hours=1)), 'HOR2': '00J00H00M',
        'SC1': r16.SC, 'SC2': -99.0, 'SV1': r16.SV, 'SV2': -99.0, 'SV3': -99.0,
        'NJ': r16.NJ, 'HC': td2str(r16.HC * td(hours=1)), 'EC': 6,
        'ECART': r16.ECART, 'INC': 0,
    }
    r20 = GRP_Run20(**r20)
    rs20.append(r20)
runs20.extend(rs20)
runs20.write()
print('   -> Ecriture du fichier v2020  : {}'
      ''.format(os.path.relpath(CFG_20, BDD_20)))
