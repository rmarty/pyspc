#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_140_regularscale:

.. role:: blue

.. role:: boldblue

Mise au pas de temps horaire des données instantanées (_postEvent_140_regularscale.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Mise au pas de temps horaire des données instantanées. L'archivage suit une arborescence obs/PROVIDER/VARNAME

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`VARNAMES` : :blue:`Liste des grandeurs`

:boldblue:`PROVIDERS` : :blue:`Liste des producteurs`

"""

# *****************************************************************************
#               PARAMETRES
# *****************************************************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import glob
import os.path
import subprocess
import sys
from pyspc import Parameter

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   VARNAMES     : Liste des variables d'origine
#   VARNAMES     : Liste des fournisseurs de données
# ------------------------------------------------------------------------
VARNAMES = ['QI', 'HI']
PROVIDERS = ['phyc']

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- PARAMETRE SUBPROCESS
# ------------------------------------
for p in PROVIDERS:
    for v1 in VARNAMES:
        p1 = Parameter(varname=v1)
        v2 = p1.to_regularscale().spc_varname
        d1 = os.path.join(HOME, 'obs', p, v1)
        d2 = os.path.join(HOME, 'obs', p, v2)
        if not os.path.exists(d2):
            print('Création du répertoire {}'
                  ''.format(os.path.relpath(d2, start=HOME)))
            os.makedirs(d2)
        filenames = glob.glob(os.path.join(d1, '*_{}.txt'.format(v1)))
        if not filenames:
            continue
        codes = [os.path.basename(f).replace('_{}.txt'.format(v1), '')
                 for f in filenames]
        LIST_FILENAME = os.path.join(
            HOME, 'config', os.path.basename(__file__).replace('.py', '.txt'))
        with open(LIST_FILENAME, 'w', encoding='utf-8', newline='\n') as f:
            for code in codes:
                f.write('{}\n'.format(code))

# ------------------------------------
# --- LANCEMENT SUBPROCESS
# ------------------------------------
        processArgs = [
            'python',
            os.path.join(os.environ['PYSPC_BIN'], 'csv2csv.py'),
            '-I', d1,
            '-O', d2,
            '-n', v1,
            '-l', LIST_FILENAME,
            '-M', 'rsc'
        ]
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
