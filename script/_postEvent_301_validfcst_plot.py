#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_301_validfcst_plot:

.. role:: blue

.. role:: boldblue

Tracer les prévisions opérationnelles validées par lieu/runtime (_postEvent_301_validfcst_plot.py)
--------------------------------------------------------------------------------------------------

Description
+++++++++++

Tracer les prévisions opérationnelles validées par lieu/runtime

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des figures

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`RAW_TREND` : :blue:`Nom de la prévision brute à conserver`

:boldblue:`SKIP_TREND` : :blue:`Préfixe de la tendance validée à ignorer (mode auto)`

:boldblue:`FIRST_DTIME` : :blue:`Première date des runtimes. None si définie par event`

:boldblue:`LAST_DTIME` : :blue:`Dernière date des runtimes. None si définie par event`

:boldblue:`COLORS` : :blue:`Couleurs par prévision/tendance`

:boldblue:`LABELS` : :blue:`Libellés par prévision/tendance`

"""

# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt, timedelta as td
import glob
import os.path
from pyspc import Config, read_PyspcFile, read_Prevision19, Series
from pyspc.core.keyseries import tuple2str
from pyspc.data.prevision import Prevision19
from pyspc.plotting.colors import TABCOLORS as IMG_TABCOLORS
from pyspc.plotting.config import Config as PlotConfig

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# -----------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES FIGURES
#   SPC_VARNAME      : Variable
#   RAW_TREND        : Nom de la prévision brute à conserver
#   SKIP_TREND       : Préfixe de la tendance validée à ignorer (mode auto)
#   FIRST_DTIME      : Première date des runtimes. None si définie par event
#   LAST_DTIME       : Dernière date des runtimes. None si définie par event
#   COLORS           : Couleurs par prévision/tendance
#   LABELS           : Libellés par prévision/tendance
# -----------------------------------------------------------------------------
SPC_VARNAME = 'QH'
RAW_TREND = ''
SKIP_TREND = 'auto'
FIRST_DTIME = dt(2020, 6, 10)  # None
LAST_DTIME = dt(2020, 6, 16)  # None
COLORS = {
    'det_00': 'tab:green',
    'tend_10': 'tab:orange',
    'tend_50': 'tab:red',
    'tend_90': 'tab:orange',
    'pilote': 'tab:green',
    'pilote10': 'tab:orange',
    'pilote50': 'tab:red',
    'pilote90': 'tab:orange',
    'expresso': 'tab:green',
    'expresso10': 'tab:orange',
    'expresso50': 'tab:red',
    'expresso90': 'tab:orange',
    RAW_TREND: 'darkgray'
}
LABELS = {
    'det_00': 'prévision déterministe',
    'tend_10': 'tendance basse',
    'tend_50': 'tendance centrale',
    'tend_90': 'tendance haute',
    'pilote': 'prévision déterministe',
    'pilote10': 'tendance basse (pilote)',
    'pilote50': 'tendance centrale (pilote)',
    'pilote90': 'tendance haute (pilote)',
    'expresso': 'prévision déterministe',
    'expresso10': 'tendance basse (expresso)',
    'expresso50': 'tendance centrale (expresso)',
    'expresso90': 'tendance haute (expresso)',
    RAW_TREND: 'prévision brute'
}

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'subreaches'): lambda x: os.path.join(
        os.path.dirname(EVENT_FILENAME), x),
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
})
if FIRST_DTIME is None:
    FIRST_DTIME = EVENT['event']['firstdt']
if LAST_DTIME is None:
    LAST_DTIME = EVENT['event']['lastdt']
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
# ------------------------------------
# --- BASES DE PREVISION
# ------------------------------------
PRV_FILENAMES = glob.glob(os.path.join(HOME, 'fcst', 'oper', '*.mdb'))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION IMAGE
# ------------------------------------
CFGIMG_FILENAME = os.path.join(
    HOME, 'config', os.path.basename(__file__).replace('.py', '.txt'))
CFGIMG = PlotConfig(filename=CFGIMG_FILENAME)
CFGIMG.load()
# ------------------------------------
# --- LECTURE OBS
# ------------------------------------
for loc in LOCS_HYDRO:
    if len(loc) != 8:
        continue
    if not loc.startswith('K055'):
        continue
    # Lecture des données
    obs = read_PyspcFile(
        dirname=os.path.join(HOME, 'obs', 'phyc', SPC_VARNAME),
        stations=loc, varnames=SPC_VARNAME)
    if obs is None or len(obs) == 0:
        continue
# ------------------------------------
# --- LECTURE PRV
# ------------------------------------
    for prv_filename in PRV_FILENAMES:
        reader = Prevision19(filename=prv_filename)
        _, prv = reader.read_series(codes=[loc],
                                    first_dt=FIRST_DTIME, last_dt=LAST_DTIME,
                                    valid=True, warning=False)
        RUNTIMES = sorted({dt.strptime(v['DateDerObs'], '%Y%m%d%H00')
                           for v in prv.values()})
        for runtime in RUNTIMES:
            prv = read_Prevision19(filename=prv_filename, codes=[loc],
                                   first_dt=runtime, last_dt=runtime,
                                   valid=True, released=False)
            if prv is None or len(prv) == 0:
                continue
            raw = read_Prevision19(filename=prv_filename, codes=[loc],
                                   first_dt=runtime, last_dt=runtime,
                                   valid=False, released=False)
# ------------------------------------
# --- MAJ DE LA CONFIGURATION IMAGE
# ------------------------------------
            cfgimg = CFGIMG
            cfgimg['figure']['dirname'] = os.path.join(
                HOME, 'fig', 'oper', loc)
            if not os.path.exists(cfgimg['figure']['dirname']):
                os.makedirs(cfgimg['figure']['dirname'])
            cfgimg['figure']['filename'] = '{}_{}_valid'.format(
                loc,
                runtime.strftime('%Y%m%d%H')
            )
            cfgimg['figure']['title'] = '{} ({}, {}) / {}'.format(
                LOCS_HYDRO[loc]['name'],
                LOCS_HYDRO[loc]['river'],
                loc,
                runtime.strftime('%Y-%m-%d %H:00')
            )
            fd = runtime - td(hours=EVENT['event']['maxltime'])
            ld = runtime + td(hours=2*EVENT['event']['maxltime'])
            cfgimg['figure']['xlim'] = [fd, ld]
# ------------------------------------
# --- SELECTION ET CONFIGURATION DES COURBES
# ------------------------------------
            series = Series(datatype='obs')
            # Prv validées
            for k in sorted(list(prv.keys())):
                if (k[0] != loc or k[1] != SPC_VARNAME or k[2][0] != runtime
                        or k[2][1] == (SKIP_TREND)):
                    continue
                series.add(serie=prv[k])
                k2 = tuple2str(k)
                trend = k[2][1] + k[2][-1]
                cfgimg.setdefault(k2, {})
                cfgimg[k2]['color'] = COLORS.get(trend, 'darkgray')
                cfgimg[k2]['linewidth'] = 1
                cfgimg[k2]['label'] = LABELS.get(trend, '')
            # Si les validées = mode auto (SKIP_TREND), j'ignore le run
            if len(series) == 0:
                continue
            # Prévisions brutes
            test_raw = True
            for k in sorted(list(raw.keys())):
                if (k[0] != loc or k[1] != SPC_VARNAME or k[2][0] != runtime
                        or k[2][-1] != RAW_TREND):
                    continue
                series.add(serie=raw[k])
                k2 = tuple2str(k)
                cfgimg.setdefault(k2, {})
                cfgimg[k2]['color'] = COLORS.get(RAW_TREND, 'dargray')
                cfgimg[k2]['linestyle'] = ':'
                if test_raw:
                    cfgimg[k2]['label'] = LABELS.get(RAW_TREND, '')
                    test_raw = False
                else:
                    cfgimg[k2]['label'] = ''
            # Obs
            series.extend(obs)
            k = tuple2str(list(obs.keys())[0])
            cfgimg.setdefault(k, {})
            cfgimg[k]['color'] = IMG_TABCOLORS[0]
            cfgimg[k]['linewidth'] = 2
            cfgimg[k]['label'] = 'obs'
# ------------------------------------
# --- MAJ DE LA CONFIGURATION IMAGE AVEC VALEURS PAR DEFAUT
# ------------------------------------
            for s in cfgimg:
                if s in ['figure', 'defaut']:
                    continue
                for o in cfgimg['defaut']:
                    cfgimg[s].setdefault(o, cfgimg['defaut'][o])
# ------------------------------------
# --- CREATION FIGURE
# ------------------------------------
            # Focer les séries sim et obs à être en avant-plan
            # Forcer les séries prv à être en arrière-plan
            ss = [s for s in series if len(s[0].split('_')) <= 2]
            for s in ss:
                series.move_to_end(s, last=True)
            filename = series.plot_series(plottype='hydro', config=cfgimg)
            print('Création de la figure {}'
                  ''.format(os.path.relpath(filename, start=HOME)))

#            break  # for runtime in RUNTIMES:
#        break  # for prv_filename in PRV_FILENAMES:
    break  # for loc in LOCS_HYDRO:
