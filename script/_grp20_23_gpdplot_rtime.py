#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:
"""
import os.path
import geopandas as gpd
import matplotlib.pyplot as mplt
import pandas as pnd

# ======================================================================
# --- PARAMETRES
# ======================================================================
# HOME = os.path.abspath(os.path.dirname(__file__))
HOME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20\__MODELISATION'\
    r'\__GRPv2020'
GIS_DIRNAME = os.path.join(HOME, 'SIG')
BV_BASENAME = 'bnbv_125_grp'  # 'bv_tete' 'bv_125' 'bnbv_125_grp'
BV_FILENAME = os.path.join(GIS_DIRNAME, f'{BV_BASENAME}.geojson')
LACI_FILENAME = os.path.join(GIS_DIRNAME, 'spc_laci.geojson')
RIVER_FILENAME = os.path.join(GIS_DIRNAME, 'river.geojson')
CFG_FILENAME = os.path.join(HOME, 'LISTE_BASSINS_RTIME.DAT')
CFG_BASENAME = os.path.basename(os.path.splitext(CFG_FILENAME)[0])
SV_NAMES = ['SV1', 'SV2', 'SV3']
VERIF_COLS = ['Eff_Val', 'POD', 'FAR', 'CSI']
DPI = 150
ALPHA = 1


# ======================================================================
# --- CHARGEMENT DES DONNEES
# ======================================================================
DF_BV = gpd.read_file(BV_FILENAME)
DF_LACI = gpd.read_file(LACI_FILENAME)
DF_RIVER = gpd.read_file(RIVER_FILENAME)
DFS_VERIF = {}
GDFS_VERIF = {}
for sn in SV_NAMES:
    f = os.path.join(HOME, CFG_BASENAME + f"_{sn}.csv")
    print(f"{os.path.relpath(f, HOME)}")
    DFS_VERIF[sn] = pnd.read_csv(f, sep=';', index_col=0)
#    print(DFS_VERIF[sn])

# ======================================================================
# --- BOXPLOT CALTR SCORES
# ======================================================================
filename = os.path.join(HOME, CFG_BASENAME + "_verif.png")
print(f'>>> {os.path.relpath(filename, HOME)}')
fig, axs = mplt.subplots(ncols=2, nrows=3, figsize=(8.27, 11.69))
for sn, axn in zip(SV_NAMES, axs):
    ax1 = axn[0]
    ax2 = axn[1]

    # Box 'remplie'
    DFS_VERIF[sn].boxplot(ax=ax1, column=['Eff_Val'], patch_artist=True)

    ax1.set_ylim((0, 1))
#    if sn == 'SV1':
#        ax1.set_title('Efficience')
    ax1.set_title(sn)

    # Box 'remplie'
    DFS_VERIF[sn].boxplot(ax=ax2, column=['POD', 'FAR', 'CSI'],
                          patch_artist=True)

    ax2.set_ylim((0, 100))
#    if sn == 'SV1':
#        ax2.set_title('Critères de contingence')
    ax2.set_title(sn)
#    ft += 1
#    mplt.figtext(0.50, 0.30 * ft, sn)

    for a in axn:
        for x in a.get_lines():
            if x.get_color() == '#2ca02c':  # median
                x.set_color('#e26c57')
                x.set_linewidth(2)
            elif x.get_color() == 'black':  # decile
                x.set_color('#414edf')
                x.set_linewidth(1.5)
            elif x.get_color() == '#1f77b4':  # whiskerp
                x.set_color('#82bfea')
                x.set_linewidth(1.5)
        for x in a.artists:
            x.set(facecolor='#82bfea', edgecolor='#82bfea')

# fig.suptitle("Performance en calage complet")
fig.tight_layout()
fig.savefig(filename, dpi=DPI)
mplt.close(fig)

# ======================================================================
# --- GEOPANDAS - DATAFRAME
# ======================================================================
for sn in SV_NAMES:
    DFS_VERIF[sn] = DF_BV.merge(DFS_VERIF[sn], on='CODE', how='left')
    GDFS_VERIF[sn] = gpd.GeoDataFrame(DFS_VERIF[sn])

# ======================================================================
# --- CARTE EFFICIENCE
# ======================================================================
filename = os.path.join(HOME, CFG_BASENAME + "_eff.png")
print(f'>>> {os.path.relpath(filename, HOME)}')

fig, (ax1, ax2, ax3) = mplt.subplots(ncols=3, sharex=True, sharey=True)
fig.set_size_inches(11.69, 8.27)  # A4 paysage

DF_LACI.boundary.plot(ax=ax1, color='black')
DF_LACI.boundary.plot(ax=ax2, color='black')
DF_LACI.boundary.plot(ax=ax3, color='black')

for sn, ax in zip(SV_NAMES, [ax1, ax2, ax3]):
    GDFS_VERIF[sn].plot(
        ax=ax, column='Eff_Val', edgecolor='darkgrey',
        vmin=0, vmax=1, cmap='jet_r', alpha=ALPHA,
        legend=True,  # cax=cb_ax,
        legend_kwds={'label': "Efficience", 'orientation': "horizontal"})


DF_RIVER.plot(ax=ax1, color='tab:brown')
DF_RIVER.plot(ax=ax2, color='tab:brown')
DF_RIVER.plot(ax=ax3, color='tab:brown')

ax1.set_axis_off()
ax2.set_axis_off()
ax3.set_axis_off()
ax1.set_title('SV1')
ax2.set_title('SV2')
ax3.set_title('SV3')

fig.tight_layout()
fig.suptitle('Modélisation GRP v2021 - Calage Complet', fontsize=14)
fig.savefig(filename, dpi=DPI)
mplt.close(fig)

# ======================================================================
# --- CARTE CONTINGENCE
# ======================================================================
for sn in SV_NAMES:
    filename = os.path.join(HOME, CFG_BASENAME + f"_{sn}_table.png")
    print(f'>>> {os.path.relpath(filename, HOME)}')

    fig, (ax1, ax2, ax3) = mplt.subplots(ncols=3, sharex=True, sharey=True)
    fig.set_size_inches(11.69, 8.27)  # A4 paysage

    DF_LACI.boundary.plot(ax=ax1, color='black')
    DF_LACI.boundary.plot(ax=ax2, color='black')
    DF_LACI.boundary.plot(ax=ax3, color='black')

    GDFS_VERIF[sn].plot(
        ax=ax1, column='POD', edgecolor='darkgrey',
        vmin=0, vmax=100, cmap='jet_r', alpha=ALPHA,
        legend=True, legend_kwds={'label': "POD", 'orientation': "horizontal"})
    GDFS_VERIF[sn].plot(
        ax=ax2, column='FAR', edgecolor='darkgrey',
        vmin=0, vmax=100, cmap='jet', alpha=ALPHA,
        legend=True, legend_kwds={'label': "FAR", 'orientation': "horizontal"})
    GDFS_VERIF[sn].plot(
        ax=ax3, column='CSI', edgecolor='darkgrey',
        vmin=0, vmax=100, cmap='jet_r', alpha=ALPHA,
        legend=True, legend_kwds={'label': "CSI", 'orientation': "horizontal"})

    DF_RIVER.plot(ax=ax1, color='tab:brown')
    DF_RIVER.plot(ax=ax2, color='tab:brown')
    DF_RIVER.plot(ax=ax3, color='tab:brown')

    ax1.set_axis_off()
    ax2.set_axis_off()
    ax3.set_axis_off()
    ax1.set_title('Probabilité de détection')
    ax2.set_title('Ratio de fausses alertes')
    ax3.set_title('Indice de Succès Critique')

    fig.tight_layout()
    fig.suptitle(f"Modélisation GRP v2021 - Calage Complet\nSeuil = {sn}",
                 fontsize=14)
    fig.savefig(filename, dpi=DPI)
    mplt.close(fig)
