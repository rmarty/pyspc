#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_130_mf_convert:

.. role:: blue

.. role:: boldblue

Conversion des données de la Publithèque Météo-France (_postEvent_130_mf_convert.py)
-------------------------------------------------------------------------------------------

Description
+++++++++++

Conversion des données de la Publithèque Météo-France

Paramètres
++++++++++

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

:boldblue:`VARNAMES` : :blue:`Liste des grandeurs`

"""

# *****************************************************************************
#               PARAMETRES
# *****************************************************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import glob
import os.path
import subprocess
import sys

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   VARNAMES     : Liste des variables
# ------------------------------------------------------------------------
VARNAMES = ['PH']

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- PARAMETRE SUBPROCESS
# ------------------------------------
for v in VARNAMES:
    d = os.path.join(HOME, 'obs', 'mf', v)

# ------------------------------------
# --- LANCEMENT SUBPROCESS
# ------------------------------------
    filenames = glob.glob(os.path.join(d, '*.data'))
    for f in filenames:
        print('Conversion du fichier {}'.format(os.path.relpath(f, HOME)))
        processArgs = [
            'python',
            os.path.join(os.environ['PYSPC_BIN'], 'mf2csv.py'),
            '-I', os.path.dirname(f),
            '-d', os.path.basename(f),
#            '-n', v,
            '-t', 'data',
            '-O', d
        ]
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
