#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
TODO: à transformer en binaire grpInfo ou remplacer grpVerif (v16, v18) ??
"""
# ------------------------------------------------------------------------
# IMPORTS
# ------------------------------------------------------------------------
import glob
import os.path
import pandas as pnd

from pyspc import Config
from pyspc.convention.grp20 import CAL_CONFIG_CALNAMES, CAL_VERIF_SCORES
from pyspc.model.grp20 import GRP_Cfg, report_verif

# ------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
# ------------------------------------------------------------------------
HOME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\__MODELISATION\__GRPv2020'
VERIF_DATATYPE = 'rtime'  # 'cal', 'rtime'
if VERIF_DATATYPE == 'cal':
    CFG_FILENAME = os.path.join(HOME, 'LISTE_BASSINS_CALVAL.DAT')
elif VERIF_DATATYPE == 'rtime':
    CFG_FILENAME = os.path.join(HOME, 'LISTE_BASSINS_RTIME.DAT')
else:
    raise NotImplementedError(
        f"Type de vérification '{VERIF_DATATYPE}' inconnue")
VERIF_SUBDIR = 'fiches'
INPUT_DIRNAME = os.path.join(HOME, 'LOCAL_TREE')

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
print("-- Synthèse des fiches de performance")
# ---------------------------------------------------------------------
# --- 0 - Configuration retenue
# ---------------------------------------------------------------------
if VERIF_DATATYPE == 'cal':
    CFG_SELECTION = Config.from_csv(
        filename=os.path.join(HOME, 'LISTE_BASSINS_CALVAL_finalconfig.txt'),
        sectionname='CODE')
    print(f' - Selection : {os.path.relpath(CFG_SELECTION.filename, HOME)}')
else:
    CFG_SELECTION = None

# ---------------------------------------------------------------------
# --- 1 - Lecture des informations générales modèles
# ---------------------------------------------------------------------
runs20 = GRP_Cfg(filename=CFG_FILENAME)
runs20.read()
for run in runs20:
    print(' - Traitement du modèle {} {}'.format(run.CODE, run.PDT))
    rundict = run._asdict()
    p = os.path.join(INPUT_DIRNAME, f'{run.CODE}*', 'calage*', VERIF_SUBDIR)
    ds = glob.glob(p)
    if not ds:
        print(f'!!! Répertoire des fichiers de performance inexistant : {p}')
        continue
    d = ds[0]
    print(f'   - Répertoire source : {os.path.relpath(d, HOME)}')
    o = os.path.join(os.path.dirname(d), 'verif')
    if not os.path.exists(o):
        os.makedirs(o)
    try:
        filenames = report_verif(
            run=run, src_dirname=d, datatype=VERIF_DATATYPE, dest_dirname=o)
    except Exception:
        print('!!! Erreur lors de la lecture des fiches performance')
        continue
    for f in filenames:
        print('   > {}'.format(os.path.relpath(f, HOME)))
        if f.endswith('csv') and isinstance(CFG_SELECTION, Config) \
                and run.CODE in CFG_SELECTION:
            select = CFG_SELECTION[run.CODE]
            hor = rundict[select['HOR']]
            sc = rundict[select['SC']]
            star = CAL_CONFIG_CALNAMES[select['STAR']]
            sv = rundict['SV1']
            df = pnd.read_csv(f, sep=';', index_col=0)
            select_df = df[(df['HOR'] == hor)
                           & (df['SC'] == sc)
                           & (df['SA_RT'] == star)
                           & (df['SV'] == sv)]
            select_df = select_df[CAL_VERIF_SCORES]
            if select_df.empty:
                print(df)
                print(rundict)
                print(hor, sc, star, sv)
                continue
            select_df = select_df.astype(float).round(3)
            select.update({k: list(v.values())[0]
                           for k, v in select_df.to_dict().items()})
#    break
if isinstance(CFG_SELECTION, Config):
    CFG_SELECTION.to_csv(CFG_SELECTION.filename, float_format='%.3f')
