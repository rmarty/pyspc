# -*- coding: utf-8 -*-
"""
Created on Tue Nov 15 10:42:22 2022

@author: renaud.marty
"""

# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt
import os
import os.path
import subprocess

# ------------------------------------
# PARAMETRES : CONFIGURATION DES DONNEES
#   OUTPUT_DIRNAME  : Répertoire de stockage
#   LOCS            : Liste des stations
# ------------------------------------
SRC_DIRNAME = 'C:/CRISTAL'
OUTPUT_DIRNAME = r'D:\Utilisateurs\renaud.marty\Documents\1-data\0-sacha'\
    r'\cristal_2017-2021'
CSV_DIRNAME = os.path.join(OUTPUT_DIRNAME, 'csv_UT50')
LOCS = [
# -----
# UT 58 : ....-..-.. > 2021-08-31
# -----
#    'K0009910',
#    'K0029910', 'K0100030', 'K0109910', 'K0119910', 'K0253030',
#    'K0339910', 'K0359910', 'K0403020', 'K0403910', 'K0459910', 'K0529910',
#    'K0549910', 'K0569910', 'K0669910', 'K0673310', 'K0739910', 'K0749910',
#    'K0809910', 'K0989910', 'K1149910', 'K1179910', 'K1219910', 'K1359910',
#    'K1519910', 'K1563020', 'K1719910', 'K1729910', 'K1749910', 'K1773010',
#    'K2049910', 'K2070810', 'K2075010', 'K2119910', 'K2229910', 'K2254010',
#    'K2269910', 'K2319910', 'K2349910', 'K2409910', 'K2509910', 'K2519910',
#    'K2649910', 'K2659910', 'K2839910', 'K2869910', 'K3219910', 'K3229910',
#    'K3239910', 'K3349910', 'K3379910', 'K5183020', 'K5383020', 'K5433020',
#    'K5490900', 'K5623010', 'K6034000', 'K6200900'
# -----
# UT 50 : 2021-08-31 > 2022-09-01
# -----
#    'K0009910',
#    'K0029910', 'K0100030', 'K0109910', 'K0119910', 'K0253030',
#    'K0339910', 'K0359910', 'K0403020', 'K0403910', 'K0459910', 'K0529910',
#    'K0549910', 'K0569910', 'K0669910', 'K0673310', 'K0739910', 'K0749910',
#    'K0809910', 'K0989910', 'K1149910', 'K1179910', 'K1219910', 'K1359910',
#    'K1519910', 'K1563020', 'K1719910', 'K1729910', 'K1749910', 'K1773010',
#    'K2049910', 'K2070810', 'K2075010', 'K2119910', 'K2229910', 'K2254010',
#    'K2269910', 'K2319910', 'K2349910', 'K2409910', 'K2509910', 'K2519910',
#    'K2649910', 'K2659910', 'K2839910', 'K2869910', 'K3219910', 'K3229910',
#    'K3239910', 'K3349910', 'K3379910', 'K5183020', 'K5383020', 'K5433020',
#    'K5490900', 'K5623010', 'K6034000', 'K6200900'
# -----
# UT 50 : ....-..-.. > 2022-09-01
# -----
#    'K0403910'
]

# ------------------------------------
# PARAMETRES : CONFIGURATION DE LA PERIODE
#   FIRST_DT : Premier pas de temps à considérer
#   LAST_DT  : Dernier pas de temps à considérer
# ------------------------------------
FIRST_DT = dt(2017, 1, 1)
LAST_DT = dt(2021, 8, 31, 23)

# *****************************************
#               SCRIPT
# *****************************************
for loc in LOCS:
    print("--- Station {}".format(loc))
    log_filename = os.path.join(OUTPUT_DIRNAME, '{}.log'.format(loc))
    # =====================================================================
    processArgs = [
        'python',
        os.path.join(os.environ['PYSPC_BIN'], 'cristal2csv.py'),
        '-I', SRC_DIRNAME,
        '-O', CSV_DIRNAME,
        '-F', FIRST_DT.strftime('%Y%m%d%H'),
        '-L', LAST_DT.strftime('%Y%m%d%H'),
        '-s', loc,
        '-C', 'pyspc'
    ]
    # =====================================================================
#    print(' '.join(processArgs))
    with open(log_filename, 'w', encoding='utf-8', newline='\n') as f:
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=f, stderr=f)  # nosec
        processRun.wait()
    # =====================================================================
#
#    break
