#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_grpCal_run16:

.. role:: blue

.. role:: boldblue

Calage automatique de GRP-16r (_grpCal_run16.py)
------------------------------------------------

.. warning: Script à reprendre

Description
+++++++++++

Script realisant le calage automatique de GRP dans sa version 2016

Paramètres
++++++++++


.. rubric:: Operations


:boldblue:`DEBUG` : :blue:`Mode débug`

:boldblue:`DRY_RUN` : :blue:`Exécution sans lancement d'exécutables GRP ni copie fichier`

:boldblue:`RUN_CAL` : :blue:`Calage-contrôle`

:boldblue:`RUN_RTIME` : :blue:`Calage complet`

:boldblue:`RUN_COPY` : :blue:`Copie sur le réseau`


.. rubric:: Arborescence grp


:boldblue:`HOME_DIRNAME` : :blue:`Répertoire principale`

:boldblue:`GRP_DIRNAME` : :blue:`Répertoire de GRP`

:boldblue:`dirnames` : :blue:`Dictionnaires des répertoires de calcul`

:boldblue:`BVINI_FILENAME` : :blue:`Fichier bassin des modèles à caler`

:boldblue:`BVALL_PARAMS` : :blue:`Dictionnaires des fichiers des paramètres supplémentaires à tester`

:boldblue:`PAR` : :blue:`fichier, avec PAR dans ['SC', 'SV', 'HC']`


.. rubric:: Archivage


:boldblue:`ARCHIVE_DIRNAME` : :blue:`Répertoire de stockage`

:boldblue:`ARCHIVE_DIRNAME_DATA` : :blue:`Sous-répertoire des données`

:boldblue:`ARCHIVE_DIRNAME_CFG` : :blue:`Sous-répertoire des config`

:boldblue:`ARCHIVE_DIRNAME_VERIF` : :blue:`Sous-répertoire des fiches`

:boldblue:`ARCHIVE_PREFIX` : :blue:`Préfixe du dossier de stockage`

:boldblue:`ARCHIVE_PATTERN_VERIF` : :blue:`Chaine de caractère permettant de reconnaitre les fiches de performance`

:boldblue:`ARCHIVE_PATTERN_EVENT` : :blue:`Chaine de caractère permettant de reconnaitre les fiches d'événements`

:boldblue:`ARCHIVE_TIME` : :blue:`Date du calage`

:boldblue:`TABLE_MODSTA_FILENAME` : :blue:`Fichier de correspondance Modèle Stations`

:boldblue:`TABLE_STAMOD_FILENAME` : :blue:`Fichier de correspondance Station Modèles`


.. rubric:: Executables grp


:boldblue:`EXE_OBS` : :blue:`Exécutable Chroniques`

:boldblue:`EXE_CAL` : :blue:`Exécutable calage-contrôle`

:boldblue:`EXE_VERIF` : :blue:`Exécutable fiche de performance`

:boldblue:`EXE_EVENT` : :blue:`Exécutable fiche d'événements`

:boldblue:`EXE_RTIME` : :blue:`Exécutable calage-complet`

:boldblue:`STDIN_OBS` : :blue:`Entrée standard pour l'exécutable Chroniques`

:boldblue:`STDIN_CAL` : :blue:`Entrée standard pour l'exécutable calage`

:boldblue:`STDIN_VERIF` : :blue:`Entrée standard pour l'exécutable fiche de performance`

:boldblue:`STDIN_EVENT` : :blue:`Entrée standard pour l'exécutable fiche d'événements`

:boldblue:`STDIN_RTIME` : :blue:`Entrée standard pour l'exécutable calage complet`

:boldblue:`BVINI_FILENAME` : :blue:`Fichier bassin des modèles à caler`

:boldblue:`BV_FILENAME` : :blue:`Fichier bassin de GRP`

:boldblue:`BV_HEADER_LEN` : :blue:`Longueur de l'entête du fichier de bassin`

:boldblue:`CFG_FILENAME` : :blue:`Fichier de configuration de GRP`



"""

# *****************************************
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import copy
from datetime import datetime as dt
import os
import os.path
import sys
import subprocess
import glob
import shutil
import collections

from libpySPC import irstea as _irstea
import pyspc.core.exception as _exception

# ------------------------------------
# PARAMETRES : OPERATIONS
#   DEBUG       : Mode débug
#   DRY_RUN     : Exécution sans lancement d'exécutables GRP ni copie fichier
#   RUN_CAL     : Calage-contrôle
#   RUN_RTIME   : Calage complet
#   RUN_COPY    : Copie sur le réseau
# ------------------------------------
DEBUG = False
DRY_RUN = False
RUN_CAL = False
RUN_RTIME = False
RUN_COPY = False

# ------------------------------------
# PARAMETRES : ARBORESCENCE GRP
#   HOME_DIRNAME        : Répertoire principale
#   GRP_DIRNAME         : Répertoire de GRP
#   dirnames            : Dictionnaires des répertoires de calcul
#   BVINI_FILENAME      : Fichier bassin des modèles à caler
#   BVALL_PARAMS        : Dictionnaires des fichiers des paramètres supplémentaires à tester
#                         PAR: fichier, avec PAR dans ['SC', 'SV', 'HC']
# ------------------------------------
HOME_DIRNAME = r'D:\2-grp-3.3'
GRP_DIRNAME = os.path.join(HOME_DIRNAME, 'GRP_16r1628')
dirnames = {
    'DATA_DIRNAME': os.path.join(HOME_DIRNAME, '20190426_BDD'),
    'CAL_DIRNAME': os.path.join(HOME_DIRNAME, '20190426_CAL'),
    'RTIME_DIRNAME': os.path.join(HOME_DIRNAME, '20190426_TR'),
}
BVINI_FILENAME = os.path.join(
    GRP_DIRNAME, 'Parametrage', '2019_LISTE_BASSINS_NEW.DAT')
BVALL_PARAMS = {
        'SC': os.path.join(HOME_DIRNAME, '20190426_CAL', 'SC.txt'),
        'SV': os.path.join(HOME_DIRNAME, '20190426_CAL', 'SV.txt'),
}

# ------------------------------------
# PARAMETRES : ARCHIVAGE
#   ARCHIVE_DIRNAME         : Répertoire de stockage
#   ARCHIVE_DIRNAME_DATA    : Sous-répertoire des données
#   ARCHIVE_DIRNAME_CFG     : Sous-répertoire des config
#   ARCHIVE_DIRNAME_VERIF   : Sous-répertoire des fiches
#   ARCHIVE_PREFIX          : Préfixe du dossier de stockage
#   ARCHIVE_PATTERN_VERIF   : Chaine de caractère permettant de reconnaitre
#                             les fiches de performance
#   ARCHIVE_PATTERN_EVENT   : Chaine de caractère permettant de reconnaitre
#                             les fiches d'événements
#   ARCHIVE_TIME            : Date du calage
#   TABLE_MODSTA_FILENAME   : Fichier de correspondance Modèle Stations
#   TABLE_STAMOD_FILENAME   : Fichier de correspondance Station Modèles
# ------------------------------------
ARCHIVE_DIRNAME = r'D:\2-grp-3.3\20190426_ARCHIVE'
ARCHIVE_DIRNAME_DATA = 'data'
ARCHIVE_DIRNAME_CFG = 'cfg'
ARCHIVE_DIRNAME_VERIF = 'fiches'
ARCHIVE_PREFIX = 'test'  # 'calage'
ARCHIVE_PATTERN_VERIF = 'FichePerf'  # 'FichePerf_calage_complet'
ARCHIVE_PATTERN_EVENT = 'HydroPrevu'  # 'HydroPrevu_calage_complet'
ARCHIVE_TIME = '2019-04-30'  # dt.strftime(dt.now(), "%Y-%m-%d")
TABLE_MODSTA_FILENAME = {
    'P': os.path.join(ARCHIVE_DIRNAME, 'mod_sta_P.txt'),
    'T': os.path.join(ARCHIVE_DIRNAME, 'mod_sta_T.txt')
}
TABLE_STAMOD_FILENAME = {
    'P': os.path.join(ARCHIVE_DIRNAME, 'sta_mod_P.txt'),
    'T': os.path.join(ARCHIVE_DIRNAME, 'sta_mod_T.txt')
}

# ------------------------------------
# PARAMETRES : EXECUTABLES GRP
#   EXE_OBS         : Exécutable Chroniques
#   EXE_CAL         : Exécutable calage-contrôle
#   EXE_VERIF       : Exécutable fiche de performance
#   EXE_EVENT       : Exécutable fiche d'événements
#   EXE_RTIME       : Exécutable calage-complet
#   STDIN_OBS       : Entrée standard pour l'exécutable Chroniques
#   STDIN_CAL       : Entrée standard pour l'exécutable calage
#   STDIN_VERIF     : Entrée standard pour l'exécutable fiche de performance
#   STDIN_EVENT     : Entrée standard pour l'exécutable fiche d'événements
#   STDIN_RTIME     : Entrée standard pour l'exécutable calage complet
#   BVINI_FILENAME  : Fichier bassin des modèles à caler
#   BV_FILENAME     : Fichier bassin de GRP
#   BV_HEADER_LEN   : Longueur de l'entête du fichier de bassin
#   CFG_FILENAME    : Fichier de configuration de GRP
# ------------------------------------
EXE_OBS = '00-Trace_Chroniques_Observations.exe'
EXE_CAL = '01-Calage_GRP.exe'
EXE_VERIF = '02-Fiches_Performances.exe'
EXE_EVENT = '03-Trace_Hydrogrammes_Prevus.exe'
EXE_RTIME = '04-Creation_Base_Temps_reel_GRP.exe'
STDIN_OBS = '00-Trace_Chroniques_Observations.txt'
STDIN_CAL = '01-Calage_GRP.txt'
STDIN_VERIF = '02-Fiches_Performances.txt'
STDIN_EVENT = '03-Trace_Hydrogrammes_Prevus.txt'
STDIN_RTIME = '04-Creation_Base_Temps_reel_GRP.txt'
BV_FILENAME = os.path.join(GRP_DIRNAME, 'Parametrage', 'LISTE_BASSINS.DAT')
BV_HEADER_LEN = 25  # Nb ligne entête
CFG_FILENAME = os.path.join(GRP_DIRNAME, 'Parametrage', 'Config_Calage.txt')

# -------------------------------------
# DEPENDANCES A LA CONFIGURATION DE GRP
# -------------------------------------
target = None
with open(CFG_FILENAME, 'r', encoding='cp1252', newline='\n') as f:
    for line in f.readlines():
        if line.startswith('#01#'):
            target = 'DATA_DIRNAME'
        elif line.startswith('#02#'):
            target = 'CAL_DIRNAME'
        elif line.startswith('#03#'):
            target = 'RTIME_DIRNAME'
        else:
            if target is not None:
                dirnames[target] = line.replace('\n', '').replace('\r', '')
                target = None

DATA_DIRNAME = dirnames['DATA_DIRNAME']
DATA_DIRNAME_BV = os.path.join(DATA_DIRNAME, 'Bassins')
DATA_DIRNAME_P = os.path.join(DATA_DIRNAME, 'BDD_P')
DATA_DIRNAME_E = os.path.join(DATA_DIRNAME, 'BDD_E')
DATA_DIRNAME_Q = os.path.join(DATA_DIRNAME, 'BDD_Q')
DATA_DIRNAME_T = os.path.join(DATA_DIRNAME, 'BDD_T')
DATA_DIRNAME_PEQ = os.path.join(DATA_DIRNAME, 'BDD_PEQ')

CAL_DIRNAME = dirnames['CAL_DIRNAME']
OBS_DIRNAME = os.path.join(CAL_DIRNAME, '00-Chroniques_Observations')
RES_DIRNAME = os.path.join(CAL_DIRNAME, '01-Resultats_bruts')
VERIF_DIRNAME = os.path.join(CAL_DIRNAME, '02-Fiches_Performances')
EVENT_DIRNAME = os.path.join(CAL_DIRNAME, '03-Hydrogrammes_Prevus')
SYNTHESE_VERIF_FILENAME = {
    'cal': os.path.join(CAL_DIRNAME, 'synthese_fichesperf_cal.txt'),
    'rtime': os.path.join(CAL_DIRNAME, 'synthese_fichesperf_rtime.txt')
}
SYNTHESE_STATIONS_FILENAME = os.path.join(CAL_DIRNAME,
                                          'synthese_stations.txt')

os.chdir(GRP_DIRNAME)

# *****************************************
#               FONCTIONS
# *****************************************
# *****************************************
#               SCRIPT
# *****************************************
# ------------------------------------
# LECTURE ET MISE EN FORME <BVINI_FILENAME>
# ------------------------------------
BVINI_RUNS = _irstea.GRP_Cfg(filename=BVINI_FILENAME)
BVINI_RUNS.read()
BVINI_RUNS.write()
counter = 0
# ------------------------------------
# CALIBRATION
# ------------------------------------
if RUN_CAL:
    synthese_stations = set()
    runs_exe_obs = set()
    calibrationvalues_old = None
    print(" -- Calibration des modèles GRP")
    # --------------------------------------------
    # Définition des runs GRP
    # --------------------------------------------
    BVALL_RUNS = BVINI_RUNS.product(filenames=BVALL_PARAMS, only_2digits=False)
    for krun, run in enumerate(BVALL_RUNS):
        srun = ', '.join(['{}: {}'.format(n, run[n])
                          for n in BVALL_RUNS._names])
        print('   -----------------------------------------------------------')
        print("   + Traitement du bassin n°{0:0>2d}: {1} ({2}) \n{3}"
              "".format(run['NB'], run['NOM'], run['CODE'], srun))
        # --------------------------------------------
        # Récupération des infos sur le bassin courant
        # --------------------------------------------
        run['NB'] = 1
        BV_RUNS = _irstea.GRP_Cfg(filename=BV_FILENAME)
        BV_RUNS.append(run)
        BV_RUNS.write()
        # -----------------
        # Ouverture DEVNULL
        # -----------------
        if DEBUG:
            processStdOut = sys.stdout
        else:
            processStdOut = subprocess.DEVNULL
        # ----------
        # CHRONIQUES
        # ----------
        # ------
        # CALAGE
        # ------
        calibrationvalues = BV_RUNS.calibrationvalues()[0]
        if calibrationvalues != calibrationvalues_old:
            print("     - Calage du modèle GRP")
            print(dt.utcnow().strftime('%d/%m/%Y %H:%M:%S'))
            if not DRY_RUN:
                fFP = open(STDIN_CAL, 'r', encoding='utf-8')
                processRun = subprocess.Popen(
                    EXE_CAL, universal_newlines=True,
                    shell=True, stdin=fFP,  # nosec
                    stdout=processStdOut, stderr=sys.stderr)
                processRun.wait()
                fFP.close()
            calibrationvalues_old = copy.deepcopy(calibrationvalues)
            print(dt.utcnow().strftime('%d/%m/%Y %H:%M:%S'))
        # ----------
        # FICHE PERF
        # ----------
        print("     - Fiches Performances")
        print(dt.utcnow().strftime('%d/%m/%Y %H:%M:%S'))
        if not DRY_RUN:
            fFP = open(STDIN_VERIF, 'r', encoding='utf-8')
            processRun = subprocess.Popen(
                EXE_VERIF, universal_newlines=True,
                shell=True, stdin=fFP, stdout=processStdOut, stderr=sys.stderr)  # nosec
            processRun.wait()
            fFP.close()
        print(dt.utcnow().strftime('%d/%m/%Y %H:%M:%S'))
        # ------------
        # SELECT EVENT
        # ------------
        print("     - Sélection Evénements")
        print(dt.utcnow().strftime('%d/%m/%Y %H:%M:%S'))
        if not DRY_RUN:
            fFP = open(STDIN_EVENT, 'r', encoding='utf-8')
            processRun = subprocess.Popen(
                EXE_EVENT, universal_newlines=True,
                shell=True, stdin=fFP, stdout=processStdOut, stderr=sys.stderr)  # nosec
            processRun.wait()
            fFP.close()
        print(dt.utcnow().strftime('%d/%m/%Y %H:%M:%S'))
        # -----------------
        # Fermeture DEVNULL
        # -----------------
        synthese_stations.add(run['CODE'])
#        break
    # --------------------------------------------
    # Synthèse des fiches de performance GRP
    # --------------------------------------------
    if synthese_stations and not DRY_RUN:
        print("     - Synthèse des fiches de performance GRP")
        with open(SYNTHESE_STATIONS_FILENAME, 'w',
                  encoding='utf-8', newline='\n') as f:
            for s in synthese_stations:
                f.write('{}\n'.format(s))
        processArgs = [
            'python',
            os.environ['PYSPC_BIN'] + '\\'
            'grpVerif.py',
            '-I', VERIF_DIRNAME,
            '-l', SYNTHESE_STATIONS_FILENAME,
            '-t', 'cal',
            '-c', SYNTHESE_VERIF_FILENAME['cal']
            ]
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()

# ------------------------------------
# CREATION BASE TR
# ------------------------------------
counter = 0
if RUN_RTIME:
    print(" -- Création de la base temps-réel des modèles GRP")
    synthese_stations = set()
    calibrationvalues_old = None
    # --------------------------------------------
    # Définition des runs GRP
    # --------------------------------------------
    BVALL_RUNS = BVINI_RUNS.product(filenames=BVALL_PARAMS,
                                    only_2digits=False)
#    BVALL_RUNS = BVINI_RUNS
    for krun, run in enumerate(BVALL_RUNS):
        print('   -----------------------------------------------------------')
        print("   + Traitement du bassin n°{0:0>2d}: {1} ({2}) : \n{3}"
              "".format(run['NB'], run['NOM'], run['CODE'], run))
        # --------------------------------------------
        # Récupération des infos sur le bassin courant
        # --------------------------------------------
        run['NB'] = 1
        BV_RUNS = _irstea.GRP_Cfg(filename=BV_FILENAME)
        BV_RUNS.append(run)
        BV_RUNS.write()
        # -----------------
        # Ouverture DEVNULL
        # -----------------
        if DEBUG:
            processStdOut = sys.stdout
        else:
            processStdOut = subprocess.DEVNULL
        # ------
        # CALAGE
        # ------
        print("     - Calage du modèle GRP sur TOUTE l'archive "
              "et création base Temps-Réel")
        counter += 1
        print(dt.utcnow().strftime('%d/%m/%Y %H:%M:%S'))
        if not DRY_RUN:
            fTR = open(STDIN_RTIME, 'r', encoding='utf-8')
            processRun = subprocess.Popen(
                EXE_RTIME, universal_newlines=True,
                shell=True, stdin=fTR,  # nosec
                stdout=processStdOut, stderr=processStdOut)
            processRun.wait()
            fTR.close()
        print(dt.utcnow().strftime('%d/%m/%Y %H:%M:%S'))
        # -----------------
        # Fermeture DEVNULL
        # -----------------
#        synthese_stations.add(run['CODE'])
    # --------------------------------------------
    # Synthèse des fiches de performance GRP
    # --------------------------------------------
#    if synthese_stations:
#        print("     - Synthèse des fiches de performance GRP")
#        with open(SYNTHESE_STATIONS_FILENAME, 'w',
#                  encoding='utf-8', newline='\n') as f:
#            for s in synthese_stations:
#                f.write('{}\n'.format(s))
#        with open(SYNTHESE_VERIF_FILENAME['rtime'], 'w',
#                  encoding='utf-8', newline='\n') as f:
#            processArgs = [
#                'python',
#                os.environ['PYSPC_BIN'] + '\\'
#                'grpVerif.py',
#                '-I', VERIF_DIRNAME,
#                '-l', SYNTHESE_STATIONS_FILENAME,
#                '-t', 'rtime'
#                ]
#            processRun = subprocess.Popen(
#                processArgs, universal_newlines=True,
#                shell=True, stdout=f, stderr=sys.stderr)  # nosec
#            processRun.wait()

# ------------------------------------
# ARCHIVAGE SUR SERVEUR
# ------------------------------------
if RUN_COPY:
    dicModele = {}
    dicStation = {'P': {}, 'T': {}}
    print(" -- Archivage sur serveur")
    # --------------------------------------------
    # Définition des runs GRP
    # --------------------------------------------
    for krun, run in enumerate(BVINI_RUNS):
        print('   -----------------------------------------------------------')
        print("   + Traitement du bassin n°{0:0>2d}: {1} ({2}) : \n{3}"
              "".format(run['NB'], run['NOM'], run['CODE'], run))
        # ---------------------------------------------------------------
        # Initialisation liste des pluvios utilisés par le modèle courant
        # ---------------------------------------------------------------
        dicModele[run['CODE']] = {'P': [], 'T': []}
        # -------------------------------
        # Définition du dossier du bassin
        # -------------------------------
        searchSBV = os.path.join(ARCHIVE_DIRNAME, run['CODE'] + "*")
        dirSBV = glob.glob(searchSBV)
        if dirSBV:
            dirSBV = [os.path.join(ARCHIVE_DIRNAME,
                                   run['CODE'] + "_" +
                                   run['NOM'].replace(" ", "-"))]
            _exception.Warning(
                os.path.basename(sys.argv[0]),
                "aucun dossier dans les archives ne correspond."
                "Définition du dossier {0}".format(dirSBV[0]))
        # -----------------------------------
        # Archivage vers le dossier du bassin
        # -----------------------------------
        if len(dirSBV) == 1:
            # ------------
            # Arborescence
            # ------------
            dirSBV = dirSBV[0]
            timeSBV = ARCHIVE_TIME
            dirSBV = os.path.join(dirSBV, ARCHIVE_PREFIX + "_" + timeSBV)
            dirSBVData = os.path.join(dirSBV, ARCHIVE_DIRNAME_DATA)
            dirSBVCfg = os.path.join(dirSBV, ARCHIVE_DIRNAME_CFG)
            dirSBVFich = os.path.join(dirSBV, ARCHIVE_DIRNAME_VERIF)
            print("     - Dossier : {0}".format(dirSBV))
            if not os.path.exists(dirSBV):
                os.makedirs(dirSBV)
            if not os.path.exists(dirSBVData):
                os.makedirs(dirSBVData)
            if not os.path.exists(dirSBVCfg):
                os.makedirs(dirSBVCfg)
            if not os.path.exists(dirSBVFich):
                os.makedirs(dirSBVFich)
            # --------------------------------------------
            # Récupération des infos sur le bassin courant
            # --------------------------------------------
            bv_filename = os.path.join(dirSBVCfg,
                                       os.path.basename(BV_FILENAME))
            BV_RUNS = _irstea.GRP_Cfg(filename=bv_filename)
            BV_RUNS.append(run)
            if DRY_RUN:
                pass
            else:
                BV_RUNS.write()
            print("     - Création du fichier {0}".format(bv_filename))
            # ---------------------------
            # Copie du fichier Bassin.dat
            # ---------------------------
            fileBDDBV = os.path.join(DATA_DIRNAME_BV, run['CODE'] + ".dat")
            if DRY_RUN:
                pass
            else:
                shutil.copy2(fileBDDBV, dirSBVCfg)
            print("     - Copie du fichier {0}".format(fileBDDBV))
            # -------------------------------------------
            # Copie du fichier de la courbe hypsométrique
            # -------------------------------------------
            filename = os.path.join(DATA_DIRNAME_BV,
                                    run['CODE'] + "-hypso.txt")
            if os.path.exists(filename):
                if DRY_RUN:
                    pass
                else:
                    shutil.copy2(filename, dirSBVCfg)
                print("     - Copie du fichier {0}".format(filename))
            # -----------------------
            # Copie des données DEBIT
            # -----------------------
            station = os.path.splitext(os.path.basename(fileBDDBV))[0]
            filename = os.path.join(DATA_DIRNAME_Q, station + "_Q.txt")
            if DRY_RUN:
                pass
            else:
                shutil.copy2(filename, dirSBVData)
            print("     - Copie du fichier {0}".format(filename))
            # -----------------------
            # Copie des données PEQ
            # -----------------------
            filename = os.path.join(DATA_DIRNAME_PEQ, station + "_PEQ.txt")
            if os.path.exists(filename):
                if DRY_RUN:
                    pass
                else:
                    shutil.copy2(filename, dirSBVData)
                print("     - Copie du fichier {0}".format(filename))
            # -----------------------
            # Copie des données PEQT
            # -----------------------
            filename = os.path.join(DATA_DIRNAME_PEQ, station + "_PEQT.txt")
            if os.path.exists(filename):
                if DRY_RUN:
                    pass
                else:
                    shutil.copy2(filename, dirSBVData)
                print("     - Copie du fichier {0}".format(filename))
            bassin_info = _irstea.GRP_Basin(filename=fileBDDBV)
            bassin_info.read(version=3)
            # -----------------------
            # Copie des données ETP
            # -----------------------
            if 'E' in bassin_info and bassin_info['E'] is not None:
                for station in bassin_info['E']:
                    filename = os.path.join(DATA_DIRNAME_E,
                                            station + "_EH.txt")
                    if DRY_RUN:
                        pass
                    else:
                        shutil.copy2(filename, dirSBVData)
                    print("     - Copie du fichier {0}".format(filename))
            # -----------------------
            # Copie des données PLUIE
            # -----------------------
            if 'P' in bassin_info and bassin_info['P'] is not None:
                for station in bassin_info['P']:
                    filename = os.path.join(DATA_DIRNAME_P,
                                            station + "_P.txt")
                    if DRY_RUN:
                        pass
                    else:
                        shutil.copy2(filename, dirSBVData)
                    print("     - Copie du fichier {0}".format(filename))
                    dicModele[run['CODE']]['P'].append(station)
                    if station not in dicStation['P']:
                        dicStation['P'][station] = []
                    dicStation['P'][station].append(run['CODE'])
            # -----------------------
            # Copie des données TEMP
            # -----------------------
            if 'T' in bassin_info and bassin_info['T'] is not None:
                for station in bassin_info['T']:
                    filename = os.path.join(DATA_DIRNAME_T,
                                            station + "_T.txt")
                    if DRY_RUN:
                        pass
                    else:
                        shutil.copy2(filename, dirSBVData)
                    print("     - Copie du fichier {0}".format(filename))
                    dicModele[run['CODE']]['T'].append(station)
                    if station not in dicStation['T']:
                        dicStation['T'][station] = []
                    dicStation['T'][station].append(run['CODE'])
            # -----------------------
            # Copie Fiche Performance
            # -----------------------
            searchSFP = os.path.join(
                VERIF_DIRNAME,
                '*{}*{}*.pdf'.format(ARCHIVE_PATTERN_VERIF, run['CODE'])
            )
            filesSFP = glob.glob(searchSFP)
            for filename in filesSFP:
                if DRY_RUN:
                    pass
                else:
                    shutil.copy2(filename, dirSBVFich)
                print("     - Copie du fichier {0}".format(filename))
            # --------------------------
            # Copie Sélection Evenements
            # --------------------------
            searchSSE = os.path.join(
                EVENT_DIRNAME,
                '*_{}*_{}_*.pdf'.format(ARCHIVE_PATTERN_EVENT, run['CODE'])
            )
            filesSSE = glob.glob(searchSSE)
            for filename in filesSSE:
                if DRY_RUN:
                    pass
                else:
                    shutil.copy2(filename, dirSBVFich)
                print("     - Copie du fichier {0}".format(filename))
        else:
            _exception.Warning(
                os.path.basename(sys.argv[0]),
                "plusieurs fichiers correspondent")
        if DEBUG:
            break
    # ------------------------------------------------
    # Ecriture des tables d'association
    # ------------------------------------------------
    # j'utilise <collections.OrderedDict>
    # car je veux que le dico soit trié par clé croissante.
    dicModele = collections.OrderedDict(sorted(dicModele.items(),
                                               key=lambda t: t[0]))
    for varname in ['P', 'T']:
        # --------------------------
        # Association modele-station
        # --------------------------
        print("   + Table d'association: modèle GRP - station(s) {0} : {1}"
              "".format(varname, TABLE_MODSTA_FILENAME[varname]))
        with open(TABLE_MODSTA_FILENAME[varname], 'w',
                  encoding='utf-8', newline='\n') as f:
            for modele in dicModele.keys():
                txtout = '{};{}'.format(modele,
                                        len(dicModele[modele][varname]))
                stations = sorted(dicModele[modele][varname])
                for station in stations:
                    txtout += ';{}'.format(station)
                txtout += "\n"
                f.write(txtout)
        # --------------------------
        # Association station-modele
        # --------------------------
        print("   + Table d'association: station(s) {0} - modèle GRP : {1}"
              "".format(varname, TABLE_STAMOD_FILENAME[varname]))
        # j'utilise <collections.OrderedDict>
        # car je veux que le dico soit trié par clé croissante.
        stations = collections.OrderedDict(
            sorted(dicStation[varname].items(),
                   key=lambda t: t[0]))
        with open(TABLE_STAMOD_FILENAME[varname], 'w',
                  encoding='utf-8', newline='\n') as f:
            for station in stations.keys():
                txtout = '{};{}'.format(station, len(stations[station]))
                models = sorted(stations[station])
                for model in models:
                    txtout += ';{}'.format(model)
                txtout += "\n"
                f.write(txtout)
