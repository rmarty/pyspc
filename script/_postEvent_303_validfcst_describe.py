#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_303_validfcst_describe:

.. role:: blue

.. role:: boldblue

Synthèse des modèles et outils de validation (_postEvent_303_validfcst_describe.py)
-----------------------------------------------------------------------------------

Description
+++++++++++

Synthèse des modèles et outils de validation

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des figures

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`SKIP_TREND` : :blue:`Préfixe de la tendance validée à ignorer (mode auto)`

:boldblue:`FIRST_DTIME` : :blue:`Première date des runtimes. None si définie par event`

:boldblue:`LAST_DTIME` : :blue:`Dernière date des runtimes. None si définie par event`

:boldblue:`MODEL_CMAPS` : :blue:`Couleurs des modèles`

:boldblue:`TEND_COLORS` : :blue:`Couleurs des tendances`

:boldblue:`SAMPLESIZE_FONTSIZE` : :blue:`Taille de police`

"""


# *****************************************s
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
import collections
from datetime import datetime as dt
import glob
import matplotlib.pyplot as plt
import os.path
import textwrap
from pyspc import Config
from pyspc.data.prevision import Prevision19

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# -----------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES FIGURES
#   SPC_VARNAME      : Variable
#   SKIP_TREND       : Préfixe de la tendance validée à ignorer (mode auto)
#   FIRST_DTIME      : Première date des runtimes. None si définie par event
#   LAST_DTIME       : Dernière date des runtimes. None si définie par event
# -----------------------------------------------------------------------------
SPC_VARNAME = 'QH'
SKIP_TREND = 'auto'
FIRST_DTIME = None
FIRST_DTIME = dt(2020, 6, 10)  # None
LAST_DTIME = dt(2020, 6, 16)  # None
MODEL_CMAPS = {
    'COR': plt.cm.get_cmap('Greys'), 'EXT': plt.cm.get_cmap('RdPu'),
    'HYD': plt.cm.get_cmap('Reds'), 'MAS': plt.cm.get_cmap('Purples'),
    'GR4': plt.cm.get_cmap('Greens'), 'GRP': plt.cm.get_cmap('Blues'),
    'MOH': plt.cm.get_cmap('Oranges'),
    'MTP': plt.cm.get_cmap('Greens'), 'PLA': plt.cm.get_cmap('Purples'),
}
TEND_COLORS = {'pilote': 'tab:blue', 'expresso': 'tab:green'}
SAMPLESIZE_FONTSIZE = 6

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'subreaches'): lambda x: os.path.join(
        os.path.dirname(EVENT_FILENAME), x),
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
})
if FIRST_DTIME is None:
    FIRST_DTIME = EVENT['event']['firstdt']
if LAST_DTIME is None:
    LAST_DTIME = EVENT['event']['lastdt']
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
# ------------------------------------
# --- BASES DE PREVISION
# ------------------------------------
PRV_FILENAMES = glob.glob(os.path.join(HOME, 'fcst', 'oper', '*.mdb'))
# ------------------------------------
# --- CONFIGURATION DES MODELES
# ------------------------------------
MODELS = collections.OrderedDict()
for f in PRV_FILENAMES:
    reader = Prevision19(filename=f)
    _, m = reader.read_models()
    if isinstance(m, dict):
        MODELS.update(m)

nmod = {}
for m in MODELS:
    n = MODELS[m]['Nom'].split('_')[0]
    nmod.setdefault(n, [])
    nmod[n].append(m)
for n in nmod:
    cmap = MODEL_CMAPS.get(n)
    x = len(nmod[n])
    for k, m in enumerate(nmod[n]):
        MODELS[m]['color'] = cmap(float(k+1) / float(x+1))

# ------------------------------------
# --- SYNTHESE DE PREVISION
# ------------------------------------
SYNTH = {}
SYNTH_FILENAME = os.path.join(
    HOME, 'fig', 'oper', 'valid_{}.csv'.format(EVENT['event']['name']))
FIG_FILENAME = os.path.join(
    HOME, 'fig', 'oper', 'valid_{}.png'.format(EVENT['event']['name']))
# ------------------------------------
# --- SYNTHESE DES PRV VALIDEES
# ------------------------------------
for prv_filename in PRV_FILENAMES:
    reader = Prevision19(filename=prv_filename)
    _, prv = reader.read_series(
        codes=[loc for loc in LOCS_HYDRO if len(loc) == 8],
        first_dt=FIRST_DTIME, last_dt=LAST_DTIME,
        valid=True, warning=False)
    for p in prv.values():
        loc = p['CodeStation']
        mod = str(p['CodeModele'])
        src = p['Source']
        if src == SKIP_TREND:
            continue
        SYNTH.setdefault(loc, {})
        SYNTH[loc].setdefault(mod, 0)
        SYNTH[loc][mod] += 1
        SYNTH[loc].setdefault(src, 0)
        SYNTH[loc][src] += 1
        SYNTH[loc].setdefault('total', 0)
        SYNTH[loc]['total'] += 1
with open(SYNTH_FILENAME, 'w', encoding='utf-8', newline='\n') as f:
    f.write('Loc;Tag;Nb\n')
    for s in sorted(SYNTH):
        for t in sorted(SYNTH[s]):
            f.write('{};{};{}\n'.format(s, t, SYNTH[s][t]))
# ------------------------------------
# --- FIGURE DE LA SYNTHESE
# ------------------------------------
stations = sorted(list(SYNTH.keys()))
models = sorted(list({m for s in SYNTH for m in SYNTH[s] if m.isdigit()}))
valids = sorted(list({m for s in SYNTH for m in SYNTH[s]
                      if not m.isdigit() and m != 'total'}))
Y_POS = range(len(stations))

fig, ax = plt.subplots(figsize=(11.69, 8.27))
# afficher les modèles
old_values = [0 for s in stations]
for model in models:
    values = [SYNTH[s].get(model, 0) for s in stations]
    m = int(model)
    ax.barh(Y_POS, values, color=MODELS[m]['color'],
            left=old_values, label=MODELS[m]['Nom'])
    old_values = [o + v for o, v in zip(old_values, values)]
# afficher les validations
old_values = [0 for s in stations]
for valid in valids:
    values = [-1 * SYNTH[s].get(valid, 0) for s in stations]
    ax.barh(Y_POS, values, color=TEND_COLORS.get(valid, 'black'),
            left=old_values, label=valid)
    old_values = [o + v for o, v in zip(old_values, values)]

# afficher stations en censurant à 20 caractères
ax.set_yticklabels(
    [textwrap.shorten(LOCS_HYDRO[s]['name'], 20) for s in stations],
    fontsize=8)
ax.set_yticks(Y_POS)

# set labels to absolute values and with integer representation
lim = ax.get_xlim()
maxlim = max(abs(lim[0]), abs(lim[1]))
ax.set_xlim([-1 * maxlim, maxlim])
lim = ax.get_xlim()
ticks = ax.get_xticks()
ax.set_xticklabels([int(abs(tick)) for tick in ticks])

# afficher les valeurs de chaque bar
for r in ax.patches:
    pts = r.get_bbox().get_points()
    xpos = (pts[1][0] + pts[0][0]) / 2
    xval = int(float((pts[1][0] - pts[0][0])))
    ypos = (pts[1][1] + pts[0][1]) / 2
    # définir la couleur du texte en fct de la couleur du rectangle
    r, g, b, _ = r.get_facecolor()
    text_color = 'white' if r * g * b < 0.1 else 'black'
    if xval > 0:
        ax.text(xpos, ypos, "{0:d}".format(xval),
                fontsize=SAMPLESIZE_FONTSIZE, color=text_color,
                horizontalalignment='center', verticalalignment='center')
    elif xval < 0:
        ax.text(xpos, ypos, "{0:d}".format(abs(xval)),
                fontsize=SAMPLESIZE_FONTSIZE, color=text_color,
                horizontalalignment='center', verticalalignment='center')
ax.plot([0, 0], ax.get_ylim(), scaley=False, linewidth=1, color='black')
ax.legend(loc=9, ncol=7, bbox_to_anchor=(0.5, -0.05), fontsize=8)
fig.suptitle(
    '{}\n{}'.format(EVENT['event']['title'], 'Synthèse des prévisions'),
    fontsize=16)

plt.savefig(FIG_FILENAME, dpi=300)
plt.close(fig)
