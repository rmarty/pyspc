#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
:orphan:

.. _script_postEvent_302_validfcst_plotevent:

.. role:: blue

.. role:: boldblue

Tracer les prévisions opérationnelles validées par lieu (_postEvent_302_validfcst_plotevent.py)
-----------------------------------------------------------------------------------------------

Description
+++++++++++

Tracer les prévisions opérationnelles validées par lieu

Paramètres
++++++++++

.. rubric:: Configuration de l'événement

:boldblue:`EVENT_FILENAME` : :blue:`Fichier de configuration de l'événement`

.. rubric:: Configuration des figures

:boldblue:`SPC_VARNAME` : :blue:`Variable`

:boldblue:`RAW_TREND` : :blue:`Nom de la prévision brute à conserver`

:boldblue:`SKIP_TREND` : :blue:`Préfixe de la tendance validée à ignorer (mode auto)`

:boldblue:`FIRST_DTIME` : :blue:`Première date des runtimes. None si définie par event`

:boldblue:`LAST_DTIME` : :blue:`Dernière date des runtimes. None si définie par event`

:boldblue:`COLORS` : :blue:`Couleurs par prévision/tendance`

:boldblue:`LINESTYLES` : :blue:`Styles de ligne, tendance [0] et déterministe [1]`

:boldblue:`FILLED` : :blue:`Afficher les incert sous forme d'enveloppe (True/False)`

"""

# *****************************************s
#               PARAMETRES
# *****************************************
# ------------------------------------
# IMPORTS
# ------------------------------------
from datetime import datetime as dt, timedelta as td
import itertools
import glob
import os.path
from pyspc import Config, read_PyspcFile, read_Prevision19, Series
from pyspc.core.keyseries import tuple2str
from pyspc.plotting.colors import TABCOLORS as IMG_TABCOLORS
from pyspc.plotting.config import Config as PlotConfig

# ------------------------------------
# PARAMETRES : CONFIGURATION DE L'EVENEMENT
#   EVENT_FILENAME   : Fichier de configuration de l'événement
# ------------------------------------
EVENT_FILENAME = r'D:\Utilisateurs\renaud.marty\Desktop\TODO_2019-09-20'\
    r'\REX_SCHAPI\202006_Loire_amont\config\event.txt'
# -----------------------------------------------------------------------------
# PARAMETRES : CONFIGURATION DES FIGURES
#   SPC_VARNAME      : Variable
#   SKIP_TREND       : Préfixe de la tendance validée à ignorer (mode auto)
#   FIRST_DTIME      : Première date des runtimes. None si définie par event
#   LAST_DTIME       : Dernière date des runtimes. None si définie par event
#   COLORS           : Couleurs des courbes
#   LINESTYLES       : Styles de ligne, tendance [0] et déterministe [1]
#   FILLED           : Afficher les incert sous forme d'enveloppe (True/False)
# -----------------------------------------------------------------------------
SPC_VARNAME = 'QH'
SKIP_TREND = 'auto'
FIRST_DTIME = dt(2020, 6, 10)  # None
LAST_DTIME = dt(2020, 6, 16)  # None
COLORS = IMG_TABCOLORS[1:]
LINESTYLES = ['-', ':']
FILLED = True

# *****************************************************************************
#               SCRIPT
# *****************************************************************************
HOME = os.path.dirname(os.path.dirname(EVENT_FILENAME))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION EVENT
# ------------------------------------
EVENT = Config(filename=EVENT_FILENAME)
EVENT.read()
EVENT.convert({
    ('event', 'subreaches'): lambda x: os.path.join(
        os.path.dirname(EVENT_FILENAME), x),
    ('event', 'firstdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'lastdt'): lambda x: dt.strptime(x, '%Y%m%d%H'),
    ('event', 'maxltime'): Config.to_int,
})
if FIRST_DTIME is None:
    FIRST_DTIME = EVENT['event']['firstdt']
if LAST_DTIME is None:
    LAST_DTIME = EVENT['event']['lastdt']
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION LOCS_HYDRO
# ------------------------------------
LOCS_HYDRO = Config(filename=os.path.join(HOME, 'config', 'locs_hydro.txt'))
LOCS_HYDRO.read()
# ------------------------------------
# --- BASES DE PREVISION
# ------------------------------------
PRV_FILENAMES = glob.glob(os.path.join(HOME, 'fcst', 'oper', '*.mdb'))
# ------------------------------------
# --- LECTURE DE LA CONFIGURATION IMAGE
# ------------------------------------
CFGIMG_FILENAME = os.path.join(
    HOME, 'config', os.path.basename(__file__).replace('.py', '.txt'))
CFGIMG = PlotConfig(filename=CFGIMG_FILENAME)
CFGIMG.load()
# ------------------------------------
# --- LECTURE OBS
# ------------------------------------
for loc in LOCS_HYDRO:
    if len(loc) != 8:
        continue
    if not loc.startswith('K055'):
        continue
    # Lecture des données
    obs = read_PyspcFile(
        dirname=os.path.join(HOME, 'obs', 'phyc', SPC_VARNAME),
        stations=loc, varnames=SPC_VARNAME)
    if obs is None or len(obs) == 0:
        continue
# ------------------------------------
# --- LECTURE PRV
# ------------------------------------
    ITER_COLORS = itertools.cycle(COLORS)
    runcolors = {}
    series = Series(datatype='obs')
    uncert = {}
    for prv_filename in PRV_FILENAMES:
        prv = read_Prevision19(filename=prv_filename, codes=[loc],
                               first_dt=FIRST_DTIME, last_dt=LAST_DTIME,
                               valid=True, released=False)
        if prv is None or len(prv) == 0:
            continue
# ------------------------------------
# --- MAJ DE LA CONFIGURATION IMAGE
# ------------------------------------
        cfgimg = CFGIMG
        cfgimg['figure']['dirname'] = os.path.join(
            HOME, 'fig', 'oper', loc)
        if not os.path.exists(cfgimg['figure']['dirname']):
            os.makedirs(cfgimg['figure']['dirname'])
        if FILLED:
            cfgimg['figure']['filename'] = '{}_valid-filled'.format(loc)
        else:
            cfgimg['figure']['filename'] = '{}_valid'.format(loc)
        cfgimg['figure']['title'] = '{} ({}, {})'.format(
            LOCS_HYDRO[loc]['name'],
            LOCS_HYDRO[loc]['river'],
            loc
        )
        fd = FIRST_DTIME  # - td(hours=EVENT['event']['maxltime'])
        ld = LAST_DTIME + td(hours=EVENT['event']['maxltime'])
        cfgimg['figure']['xlim'] = [fd, ld]
# ------------------------------------
# --- SELECTION ET CONFIGURATION DES COURBES
# ------------------------------------
        # Prv validées
        for k in sorted(list(prv.keys())):
            if k[0] != loc or k[1] != SPC_VARNAME or k[2][1] == (SKIP_TREND):
                continue
            k2 = tuple2str(k)
            k1 = k2.replace('_{}'.format(SPC_VARNAME), '')
            cfgimg.setdefault(k2, {})
            if k[2][0] not in runcolors:
                runcolors[k[2][0]] = next(ITER_COLORS)
            cfgimg[k2]['color'] = runcolors[k[2][0]]
            cfgimg[k2]['linewidth'] = 1
            if k[2][-1].endswith('50'):
                cfgimg[k2]['label'] = k[2][0].strftime('%d/%m %H:%M')
            else:
                cfgimg[k2]['label'] = ''
            if k[2][-1] in ['10', '90']:
                cfgimg[k2]['alpha'] = 0.5
            if k[2][-1] in ['10', '50', '90']:
                cfgimg[k2]['linestyle'] = '-'
            else:
                cfgimg[k2]['linestyle'] = ':'

            # Cas avec incertitude remplie
            if FILLED and k[2][-1] in ['10', '90']:
                ku = k2.replace('{}_{}'.format(k[2][-1], SPC_VARNAME),
                                '{}_{}'.format('50', SPC_VARNAME))
                uncert.setdefault(ku, Series(datatype='obs'))
                uncert[ku].add(code=k1, serie=prv[k])
            else:
                series.add(code=k1, serie=prv[k])

#        break  # for prv_filename in PRV_FILENAMES:

    # Si les validées = mode auto (SKIP_TREND), j'ignore la figure
    if len(series) == 0:
        continue
    # Obs
    series.extend(obs)
    k = tuple2str(list(obs.keys())[0])
    cfgimg.setdefault(k, {})
    cfgimg[k]['color'] = IMG_TABCOLORS[0]
    cfgimg[k]['linewidth'] = 2
    cfgimg[k]['label'] = 'obs'
# ------------------------------------
# --- MAJ DE LA CONFIGURATION IMAGE AVEC VALEURS PAR DEFAUT
# ------------------------------------
    for s in cfgimg:
        if s in ['figure', 'defaut']:
            continue
        for o in cfgimg['defaut']:
            cfgimg[s].setdefault(o, cfgimg['defaut'][o])
# ------------------------------------
# --- CREATION FIGURE
# ------------------------------------
    # Focer les séries sim et obs à être en avant-plan
    # Forcer les séries prv à être en arrière-plan
    ss = [s for s in series if len(s[0].split('_')) <= 2]
    for s in ss:
        series.move_to_end(s, last=True)
    filename = series.plot_series(
        plottype='hydro', config=cfgimg, uncert=uncert)
    print('Création de la figure {}'
          ''.format(os.path.relpath(filename, start=HOME)))

#    break  # for loc in LOCS_HYDRO:
