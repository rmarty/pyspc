#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Cfg in pyspc.model.grp16

To run all tests just type:
    python -m unittest test_model_GRP16_RT_Cfg

To run only a class test:
    python -m unittest test_model_GRP16_RT_Cfg.TestGRPRT_Cfg

To run only a specific test:
    python -m unittest test_model_GRP16_RT_Cfg.TestGRPRT_Cfg.test_init

"""
# Imports
import filecmp
import os
import unittest

from pyspc.model.grp16 import GRPRT_Cfg


class TestGRPRT_Cfg(unittest.TestCase):
    """
    GRPRT_Cfg class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp16', 'rt')
        self.valid = {
            'MODFON': 'Temps_reel',
            'INSTPR': '2008-01-01 00:00:00',
            'OBSCHE': 'C:\\Grp\\Temps_Reel\\Entrees\\',
            'OBSTYP': 'TXT',
            'SCECHE': 'C:\\Grp\\Temps_Reel\\Entrees\\',
            'SCETYP': 'TXT',
            'SCEREF': 'TU',
            'SCENBR': '010',
            'PRVCHE': 'C:\\Grp\\Temps_Reel\\Sorties\\',
            'PRVTYP': 'TXT',
            'PRVREF': 'TU',
            'PRVUNI': 'm3/s',
            'HORMAX': '120',
            'CODMOD': '2001;2002;2003;2004;2005;2006;2007;2008;2009;2010;',
            'CONFIR': 'NON',
            'CHOIXH': '24;48',
            'CODMPP': '3001;3002;',
            'CHEMIN': 'C:\\R\\R-3.3.2\\bin\\Rscript.exe',
            'SIMULA': '0'
        }
        self.maxDiff = None

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'Config_Prevision.txt')
        config = GRPRT_Cfg(filename=filename)
        self.assertEqual(config.filename, filename)

    def test_read(self):
        """
        Test de la lecture du fichier de configuration GRP *Temps Reel*
        """
        filename = os.path.join(self.dirname, 'Config_Prevision.txt')
        config = GRPRT_Cfg(filename=filename)
        config.read()
        for k, v in self.valid.items():
            self.assertEqual(config[k]['value'], v)

    def test_update(self):
        """
        Test de la mise à jour de la configuration GRP *Temps Reel*
        """
        old_valids = {
            'MODFON': 'Temps_reel',
            'INSTPR': '2018-07-10 05:00:00',
        }
        new_valids = {
            'MODFON': 'Temps_diff',
            'INSTPR': '2008-11-01 18:00:00',
        }
        filename = os.path.join(self.dirname, 'Config_Prevision.txt')
        config = GRPRT_Cfg(filename=filename)
        config.read()
        config.update_config(new_valids)
        for k, v in old_valids.items():
            self.assertNotEqual(config[k]['value'], v)
        for k, v in new_valids.items():
            self.assertEqual(config[k]['value'], v)

    def test_write(self):
        """
        Test de l'écriture du fichier de configuration GRP *Temps Reel*
        """
        filename = os.path.join(self.dirname, 'Config_Prevision.txt')
        config = GRPRT_Cfg(filename=filename)
        config.read()
        tmpfile = os.path.join('data', 'Config_Prevision.txt')
        config.filename = tmpfile
        config.write()
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)

    def test_keys(self):
        """
        Test de la liste des clés de configuration
        """
        valids = ['MODFON', 'INSTPR',
                  'OBSCHE', 'OBSTYP',
                  'SCECHE', 'SCETYP', 'SCEREF', 'SCENBR',
                  'PRVCHE', 'PRVTYP', 'PRVREF', 'PRVUNI',
                  'HORMAX', 'CODMOD', 'CONFIR', 'CHOIXH', 'CODMPP',
                  'CHEMIN', 'SIMULA']
        self.assertEqual(GRPRT_Cfg.get_cfg_keys(), valids)
