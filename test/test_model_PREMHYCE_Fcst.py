#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Fcst in pyspc.model.premhyce.forecast

To run all tests just type:
    python -m unittest test_model_PREMHYCE_Fcst

To run only a class test:
    python -m unittest test_model_PREMHYCE_Fcst.TestPREMHYCE_Fcst

To run only a specific test:
    python -m unittest test_model_PREMHYCE_Fcst.TestPREMHYCE_Fcst.test_init

"""
# Imports
from datetime import datetime as dt
import filecmp
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
import pyspc.model.premhyce as _model


class TestPREMHYCE_Fcst(unittest.TestCase):
    """
    PREMHYCE_Fcst class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'premhyce')
        self.df = pnd.DataFrame(
            {
                'Date': [dt(2020, 6, 11)] * 11,
                'Membre': ['cf', 'pf1', 'pf2', 'pf3', 'pf4', 'pf5',
                           'pf6', 'pf7', 'pf8', 'pf9', 'pf10'],
                'jp1': [5059, 3907, 3117, 4524, 4482, 4054, 4838, 3160, 4821,
                        4564, 3449],
                'jp2': [12811, 6181, 2821, 9394, 9159, 6900, 11319, 2983,
                        11248, 9725, 4132],
                'jp3': [8097, 3927, 2013, 5792, 5665, 4366, 7054, 2095, 7086,
                        6228, 2676],
                'jp4': [5506, 3137, 1816, 4255, 4184, 3411, 4963, 1885, 4969,
                        4497, 2351],
                'jp5': [4661, 2976, 1748, 3579, 3501, 2926, 4108, 1713, 4065,
                        3752, 2229],
                'jp6': [4413, 3738, 1871, 3283, 3007, 2662, 3696, 1570, 3438,
                        3225, 2644],
                'jp7': [3782, 3909, 1795, 3276, 2675, 2867, 3479, 1449, 2977,
                        2847, 3109],
                'jp8': [3300, 3266, 1646, 2947, 2475, 3453, 3030, 1346, 2624,
                        2528, 2506],
            }
        )

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, '20190718_K0550010_GR6J_CEP.txt')
        reader = _model.Fcst(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.runtime, dt(2019, 7, 18))
        self.assertEqual(reader.station, 'K0550010')
        self.assertEqual(reader.model, 'GR6J')
        self.assertEqual(reader.meteo, 'CEP')

    def test_basename(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, '20190718_K0550010_GR6J_CEP.txt')
        [runtime, station, model, meteo] = \
            _model.Fcst.split_basename(filename=filename)
        self.assertEqual(runtime, dt(2019, 7, 18))
        self.assertEqual(station, 'K0550010')
        self.assertEqual(model, 'GR6J')
        self.assertEqual(meteo, 'CEP')
        filename = os.path.join(self.dirname, '20190718_K0550010_GR6J.txt')
        with self.assertRaises(ValueError):
            _model.Fcst.split_basename(filename=filename)
        filename = os.path.join(self.dirname, '20190718_K0550010.txt')
        with self.assertRaises(ValueError):
            _model.Fcst.split_basename(filename=filename)
        filename = os.path.join(self.dirname, 'r_s_h_m_toto.txt')
        with self.assertRaises(ValueError):
            _model.Fcst.split_basename(filename=filename)
        # =====================================================================
        runtime = dt(2019, 7, 18)
        station = 'K0550010'
        model = 'GR6J'
        meteo = 'CEP'
        filename = _model.Fcst.join_basename(
            runtime=runtime, station=station, model=model, meteo=meteo
        )
        self.assertEqual(filename, '20190718_K0550010_GR6J_CEP.txt')
        with self.assertRaises(ValueError):
            _model.Fcst.join_basename(
                runtime=runtime, station=station, model=model
            )
        with self.assertRaises(ValueError):
            _model.Fcst.join_basename(
                runtime=runtime, station=station, meteo=meteo
            )
        with self.assertRaises(ValueError):
            _model.Fcst.join_basename(
                runtime=runtime, model=model, meteo=meteo
            )
        with self.assertRaises(ValueError):
            _model.Fcst.join_basename(
                station=station, model=model, meteo=meteo
            )
        with self.assertRaises(ValueError):
            _model.Fcst.join_basename(
                runtime='', station=station, model=model, meteo=meteo
            )
        # =====================================================================

    def test_read(self):
        """
        Test de lecture d'un fichier de prévisions QMJ par PREMHYCE
        """
        filename = os.path.join(self.dirname, '20200612_K0253030_GR6J_CEP.txt')
        reader = _model.Fcst(filename=filename)
        df = reader.read()
        assert_frame_equal(df, self.df)

    def test_write(self):
        """
        Test d'écriture d'un fichier de données QMJ pour PREMHYCE
        """
        filename = os.path.join(self.dirname, '20200612_K0253030_GR6J_CEP.txt')
        tmpfile = os.path.join('data', '20200612_K0253030_GR6J_CEP.txt')
        writer = _model.Fcst(filename=tmpfile)
        writer.write(self.df)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
