#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary hydroStat

To run all tests just type:
    python -m unittest test_bin_hydroStat

To run only a class test:
    python -m unittest test_bin_hydroStat.Test_hydroStat

To run only a specific test:
    python -m unittest test_bin_hydroStat.Test_hydroStat.test_DEBCLA

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import hydroStat as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_hydroStat(unittest.TestCase):
    """
    hydroStat bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'hydroStat')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_hydro2 = os.path.join('data', 'metadata', 'hydro2')
#        for filename in os.listdir(self.dir_out):
#            os.remove(os.path.join(self.dir_out, filename))
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_DEBCLA(self):
        """
        Test du traitement DEBCLA
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/hydroStat.py',
            '-d', 'debcla1.txt',
            '-I', self.dir_hydro2,
            '-c', os.path.join(self.dir_out, 'test_debcla.csv')
        ]
#            '-t', 'DEBCLA',
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydroStat()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'test_debcla.csv'))
        self.assertEqual(args.filename, 'debcla1.txt')
        self.assertEqual(args.input_dir, self.dir_hydro2)
#        self.assertEqual(args.data_type, 'DEBCLA')
#        self.assertEqual(args.verbose, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        for filename in os.listdir(self.dir_out):
            if filename.endswith('debcla.csv'):
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dir_out, filename),
                    os.path.join(self.dir_ctl, filename),
                ))
                os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion du fichier d'export Hydro2 {d} situé dans "\
            "le répertoire {I} vers le fichier {c}".format(
                d=args.filename, I=args.input_dir, c=args.output_filename)
        # =====================================================================

    def test_CRUCAL(self):
        """
        Test du traitement CRUCAL
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/hydroStat.py',
            '-d', 'crucal1.txt',
            '-I', self.dir_hydro2,
            '-c', os.path.join(self.dir_out, 'test_crucal.csv')
        ]
#            '-t', 'CRUCAL',
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydroStat()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'test_crucal.csv'))
        self.assertEqual(args.filename, 'crucal1.txt')
        self.assertEqual(args.input_dir, self.dir_hydro2)
#        self.assertEqual(args.data_type, 'CRUCAL')
#        self.assertEqual(args.verbose, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        for filename in os.listdir(self.dir_out):
            if filename.endswith('crucal.csv'):
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dir_out, filename),
                    os.path.join(self.dir_ctl, filename),
                ))
                os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion du fichier d'export Hydro2 {d} situé dans "\
            "le répertoire {I} vers le fichier {c}".format(
                d=args.filename, I=args.input_dir, c=args.output_filename)
        # =====================================================================

    def test_SYNTHESE(self):
        """
        Test du traitement SYNTHESE
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/hydroStat.py',
            '-d', 'synth1.txt',
            '-I', self.dir_hydro2,
            '-c', os.path.join(self.dir_out, 'test_synth.csv')
        ]
#            '-t', 'SYNTHESE',
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydroStat()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'test_synth.csv'))
        self.assertEqual(args.filename, 'synth1.txt')
        self.assertEqual(args.input_dir, self.dir_hydro2)
#        self.assertEqual(args.data_type, 'SYNTHESE')
#        self.assertEqual(args.verbose, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        for filename in os.listdir(self.dir_out):
            if filename.endswith('synth.csv'):
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dir_out, filename),
                    os.path.join(self.dir_ctl, filename),
                ))
                os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion du fichier d'export Hydro2 {d} situé dans "\
            "le répertoire {I} vers le fichier {c}".format(
                d=args.filename, I=args.input_dir, c=args.output_filename)
        # =====================================================================
