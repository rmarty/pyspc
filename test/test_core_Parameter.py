#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Parameter in pyspc.core.Parameter

To run all tests just type:
    python -m unittest test_core_Parameter

To run only a class test:
    python -m unittest test_core_Parameter.TestParameter

To run only a specific test:
    python -m unittest test_core_Parameter.TestParameter.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import numpy as np
import pandas as pnd
import unittest

# Imports pyspc
from pyspc.core.parameter import Parameter


class TestParameter(unittest.TestCase):
    """
    Parameter class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """

    def test_init(self):
        """
        Test de la création de l'instance
        """
        param = Parameter(varname='PH', provider='SPC')
        self.assertEqual(param.varname, 'PH')
        self.assertEqual(param.provider.name, 'SPC')
        self.assertEqual(param.spc_varname, 'PH')
        self.assertEqual(param.long_varname, 'Précipitation horaire')
        self.assertEqual(param.timestep, td(hours=1))
        self.assertEqual(param.timeunits, 'hour')
        self.assertEqual(param.units, 'mm')
        self.assertEqual(param.dtfmt, '%Y%m%d%H')
        self.assertEqual(param.np_dtype, np.float32)
        self.assertTrue(param.cumulable)

    def test_init_error(self):
        """
        Test de la création de l'instance - Cas avec ERREUR
        """
        with self.assertRaises(ValueError):
            Parameter()
        with self.assertRaises(ValueError):
            Parameter(varname='P')
#        with self.assertRaises(ValueError):
#            Parameter(provider='SPC')
#        with self.assertRaises(ValueError):
#            Parameter(varname='PH', provider='GRP')
#        with self.assertRaises(ValueError):
#            Parameter(varname='azerty', provider='SPC')
#        with self.assertRaises(ValueError):
#            Parameter(varname=1, provider='GRP')

    def test_varnames(self):
        """
        Test des variables
        """
        valid = [
            'EH', 'EJ',
            'HH', 'HI',
            'HU2J',
            'P15m', 'P3H', 'P5m', 'P6m', 'PH', 'PJ', 'PM',
            'QH', 'QI', 'QJ', 'QM',
            'TH', 'TI', 'TJ',
            'VH', 'VI',
            'ZH', 'ZI'
         ]
        self.assertEqual(valid, Parameter.get_spcvarnames())

    def test_missing_values(self):
        """
        Test des valeurs manquantes
        """
#        from numpy.ma.testutils import assert_equal as np_assertEqual
        from numpy.ma.testutils import assert_mask_equal as np_assertEqual
        valid = {
            'PH': [np.nan, np.array(-9.9900, np.float32),
                   np.array(-99.900, np.float32),
                   np.array(-99.9900, np.float32),
                   np.array(-99.9000, np.float32),
                   np.array(-999.999, np.float32),
                   np.array(-9999.000, np.float32),
                   ''],
            'TH': [np.nan, np.array(-99.9900, np.float32),
                   np.array(-99.9000, np.float32),
                   np.array(-999.999, np.float32),
                   np.array(-9999.000, np.float32),
                   ''],
            'EH': [np.nan, np.array(-9.9900, np.float32),
                   np.array(-99.900, np.float32),
                   np.array(-99.9900, np.float32),
                   np.array(-99.9000, np.float32),
                   np.array(-999.999, np.float32),
                   np.array(-9999.000, np.float32),
                   ''],
            'QH': [np.nan, np.array(-9.9900, np.float32),
                   np.array(-99.900, np.float32),
                   np.array(-99.9900, np.float32),
                   np.array(-99.9000, np.float32),
                   np.array(-999.999, np.float32),
                   np.array(-9999.000, np.float32),
                   ''],
            'HH': [np.nan, np.array(-99.900, np.float32),
                   np.array(-99.9900, np.float32),
                   np.array(-99.9000, np.float32),
                   np.array(-999.999, np.float32),
                   np.array(-9999.000, np.float32),
                   ''],
            'ZH': [np.nan, np.array(-9.9900, np.float32),
                   np.array(-99.900, np.float32),
                   np.array(-99.9900, np.float32),
                   np.array(-99.9000, np.float32),
                   np.array(-999.999, np.float32),
                   np.array(-9999.000, np.float32),
                   ''],
            'VH': [np.nan, np.array(-9.9900, np.float32),
                   np.array(-99.900, np.float32),
                   np.array(-99.9900, np.float32),
                   np.array(-99.9000, np.float32),
                   np.array(-999.999, np.float32),
                   np.array(-9999.000, np.float32),
                   ''],
            'HU2J': [np.nan, np.array(-9.9900, np.float32),
                     np.array(-99.900, np.float32),
                     np.array(-99.9900, np.float32),
                     np.array(-99.9000, np.float32),
                     np.array(-999.999, np.float32),
                     np.array(-9999.000, np.float32),
                     '']
        }
        for k, v in valid.items():
            mvs = Parameter.set_missing_values(spc_varname=k)
            np_assertEqual(v, mvs)

    def test_find(self):
        """
        Tester la recherche de grandeur
        """
        # =====================================================================
        valid = {
            ('P', '', td(minutes=5)): 'P5m',
            ('P', '', td(minutes=15)): 'P15m',
            ('P', '', td(hours=1)): 'PH',
            ('P', '', td(hours=3)): 'P3H',
            ('Q', '', td(days=1)): 'QJ',
            ('Q', '', td(days=31)): 'QM',
            ('', 'H', td(hours=1)): 'EH',
        }
        for k, v in valid.items():
            p = Parameter.find(*k)
            self.assertIsInstance(p, Parameter)
            self.assertEqual(p.spc_varname, v)
        # =====================================================================
        unvalid = [
            ('P', '', td(minutes=30)),
            ('T', '', td(minutes=15)),
            ('T', '', td(hours=3)),
            ('H', '', td(hours=3)),
            ('H', '', td(days=31)),
            ('A', '', td(hours=1)),
            ('', 'C', td(hours=1)),
        ]
        for k in unvalid:
            with self.assertRaises(ValueError):
                Parameter.find(*k)
        # =====================================================================

    def test_infer_timestep(self):
        """
        Tester la déduction du pas de temps
        """
        # =====================================================================
        prefix = 'P'
        idxs = [
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(minutes=1)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(minutes=5)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3),
                           freq=td(minutes=10)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=1)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=2)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=3)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=12)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(days=1)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(days=2)),
        ]
        valid = [
            td(minutes=5), td(minutes=5), td(minutes=5),
            td(hours=1), td(hours=1),
            td(hours=3), td(hours=3),
            td(days=1), td(days=1),
        ]
        for i, v in zip(idxs, valid):
            target = Parameter.infer_timestep(index=i, prefix=prefix)
            self.assertEqual(target, v)
        # =====================================================================
        prefix = 'Q'
        idxs = [
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(minutes=5)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3),
                           freq=td(minutes=30)),
            pnd.Index([dt(2020, 1, 1), dt(2020, 1, 1, 0, 5),
                       dt(2020, 1, 1, 0, 25), dt(2020, 1, 1, 0, 55),
                       dt(2020, 1, 1, 1, 15), dt(2020, 1, 1, 1, 25)]),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=1)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=2)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=3)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=12)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(days=1)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(days=2)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 12, 1), freq=td(days=31)),
        ]
        valid = [
            None, None, None,
            td(hours=1), td(hours=1), td(hours=1), td(hours=1),
            td(days=1), td(days=1),
            td(days=31),
        ]
        for i, v in zip(idxs, valid):
            target = Parameter.infer_timestep(index=i, prefix=prefix)
            self.assertEqual(target, v)
        # =====================================================================
        idxs = [
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(minutes=5)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3),
                           freq=td(minutes=30)),
            pnd.Index([dt(2020, 1, 1), dt(2020, 1, 1, 0, 5),
                       dt(2020, 1, 1, 0, 25), dt(2020, 1, 1, 0, 55),
                       dt(2020, 1, 1, 1, 15), dt(2020, 1, 1, 1, 25)]),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=1)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=2)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=3)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=12)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(days=1)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(days=2)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 12, 1), freq=td(days=31)),
        ]
        valid = [
            None, None, None,
            td(hours=1), td(hours=1), td(hours=1), td(hours=1),
            td(hours=1), td(hours=1), td(hours=1),
        ]
        for prefix in ['H', 'V', 'Z']:
            for i, v in zip(idxs, valid):
                target = Parameter.infer_timestep(index=i, prefix=prefix)
                self.assertEqual(target, v)
        # =====================================================================
        prefix = 'T'
        idxs = [
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(minutes=5)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3),
                           freq=td(minutes=30)),
            pnd.Index([dt(2020, 1, 1), dt(2020, 1, 1, 0, 5),
                       dt(2020, 1, 1, 0, 25), dt(2020, 1, 1, 0, 55),
                       dt(2020, 1, 1, 1, 15), dt(2020, 1, 1, 1, 25)]),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=1)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=2)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=3)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(hours=12)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(days=1)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 1, 3), freq=td(days=2)),
            pnd.date_range(dt(2020, 1, 1), dt(2020, 12, 1), freq=td(days=31)),
        ]
        valid = [
            None, None, None,
            td(hours=1), td(hours=1), td(hours=1), td(hours=1),
            td(days=1), td(days=1), td(days=1),
        ]
        for i, v in zip(idxs, valid):
            target = Parameter.infer_timestep(index=i, prefix=prefix)
            self.assertEqual(target, v)
        # =====================================================================

    def test_downscalable(self):
        """
        Tester le test de désagrégation
        """
        # =====================================================================
        provider = 'SPC'
        # =====================================================================
        param1 = Parameter(
            varname='PJ',
            provider=provider
        )
        param2 = Parameter(
            varname='PH',
            provider=provider
        )
        self.assertTrue(param1.isDownscalable(param2))
        self.assertTrue(param1.isDownscalable('P3H'))
        self.assertTrue(param1.isDownscalable('P5m'))
        self.assertFalse(param1.isDownscalable('PJ'))
        self.assertFalse(param1.isDownscalable('PM'))
        # =====================================================================
        param1 = Parameter(
            varname='EJ',
            provider=provider
        )
        param2 = Parameter(
            varname='EH',
            provider=provider
        )
        self.assertTrue(param1.isDownscalable(param2))
        self.assertTrue(param1.isDownscalable('EH'))
        self.assertFalse(param1.isDownscalable('EJ'))
        # =====================================================================
        param1 = Parameter(
            varname='TJ',
            provider=provider
        )
        param2 = Parameter(
            varname='TH',
            provider=provider
        )
        self.assertTrue(param1.isDownscalable(param2))
        self.assertTrue(param1.isDownscalable('TH'))
        self.assertFalse(param1.isDownscalable('TJ'))
        # =====================================================================
        param1 = Parameter(
            varname='QJ',
            provider=provider
        )
        param2 = Parameter(
            varname='QH',
            provider=provider
        )
        self.assertTrue(param1.isDownscalable(param2))
        self.assertTrue(param1.isDownscalable('QH'))
        self.assertFalse(param1.isDownscalable('QI'))
        self.assertFalse(param1.isDownscalable('QJ'))
        self.assertFalse(param1.isDownscalable('QM'))
        # =====================================================================

    def test_nearlyequalscalable(self):
        """
        Tester le test de pas de temps très proche
        """
        # =====================================================================
        provider = 'SPC'
        # =====================================================================
        param1 = Parameter(
            varname='P5m',
            provider=provider
        )
        param2 = Parameter(
            varname='P6m',
            provider=provider
        )
        # =====================================================================
        self.assertTrue(param1.isNearlyequalscalable(param2))
        self.assertFalse(param1.isNearlyequalscalable('P5m'))
        self.assertFalse(param1.isNearlyequalscalable('PH'))
        self.assertFalse(param1.isNearlyequalscalable('PJ'))
        # =====================================================================
        self.assertTrue(param2.isNearlyequalscalable(param1))
        self.assertFalse(param2.isNearlyequalscalable('P6m'))
        # =====================================================================

    def test_regularscale(self):
        """
        Tester le paramètre à pas de temps régulier
        """
        # =====================================================================
        provider = 'SPC'
        # =====================================================================
        valid = {
            'HI': 'HH',
            'QI': 'QH',
            'TI': 'TH',
            'ZI': 'ZH',
            'VI': 'VH'
        }
        unvalid = [p
                   for p in Parameter.get_spcvarnames()
                   if p not in valid]
        # =====================================================================
        for k, v in valid.items():
            p = Parameter(
                varname=k,
                provider=provider
            )
            p = p.to_regularscale()
            self.assertIsInstance(p, Parameter)
            self.assertEqual(p.spc_varname, v)
        # =====================================================================
        for k in unvalid:
            p = Parameter(
                varname=k,
                provider=provider
            )
            with self.assertRaises(ValueError):
                p.to_regularscale()
        # =====================================================================

    def test_subhourlyscale(self):
        """
        Tester le paramètre à pas de temps infra-horaire
        """
        # =====================================================================
        provider = 'SPC'
        # =====================================================================
        valid = {
            'HH': 'HI',
            'QH': 'QI',
            'TH': 'TI',
            'ZH': 'ZI',
            'VH': 'VI'
        }
        unvalid = [p
                   for p in Parameter.get_spcvarnames()
                   if p not in valid]
        # =====================================================================
        for k, v in valid.items():
            p = Parameter(
                varname=k,
                provider=provider
            )
            p = p.to_subhourlyscale()
            self.assertIsInstance(p, Parameter)
            self.assertEqual(p.spc_varname, v)
        # =====================================================================
        for k in unvalid:
            p = Parameter(
                varname=k,
                provider=provider
            )
            with self.assertRaises(ValueError):
                p.to_subhourlyscale()
        # =====================================================================

    def test_upscalable(self):
        """
        Tester le test d'agrégation
        """
        # =====================================================================
        provider = 'SPC'
        # =====================================================================
        param1 = Parameter(
            varname='PH',
            provider=provider
        )
        param2 = Parameter(
            varname='PJ',
            provider=provider
        )
        self.assertTrue(param1.isUpscalable(param2))
        self.assertTrue(param1.isUpscalable('P3H'))
        self.assertTrue(param1.isUpscalable('PJ'))
        self.assertTrue(param1.isUpscalable('PM'))
        self.assertFalse(param1.isUpscalable('P5m'))
        # =====================================================================
        param1 = Parameter(
            varname='EH',
            provider=provider
        )
        param2 = Parameter(
            varname='EJ',
            provider=provider
        )
        self.assertTrue(param1.isUpscalable(param2))
        self.assertTrue(param1.isUpscalable('EJ'))
        self.assertFalse(param1.isUpscalable('EH'))
        # =====================================================================
        param1 = Parameter(
            varname='TH',
            provider=provider
        )
        param2 = Parameter(
            varname='TJ',
            provider=provider
        )
        self.assertTrue(param1.isUpscalable(param2))
        self.assertTrue(param1.isUpscalable('TJ'))
        self.assertFalse(param1.isUpscalable('TH'))
        # =====================================================================
        param1 = Parameter(
            varname='QH',
            provider=provider
        )
        param2 = Parameter(
            varname='QJ',
            provider=provider
        )
        self.assertTrue(param1.isUpscalable(param2))
        self.assertTrue(param1.isUpscalable('QJ'))
        self.assertTrue(param1.isUpscalable('QM'))
        self.assertFalse(param1.isUpscalable('QH'))
        self.assertFalse(param1.isUpscalable('QI'))
        # =====================================================================

    def test_apply_RatingCurves(self):
        """
        test apply_RatingCurves
        """
        # =====================================================================
        provider = 'SPC'
        # =====================================================================
        valid = {
            'HI': 'QI',
            'HH': 'QH',
            'QI': 'HI',
            'QH': 'HH'
        }
        unvalid = [p
                   for p in Parameter.get_spcvarnames()
                   if p not in valid]
        # =====================================================================
        for k, v in valid.items():
            p = Parameter(
                varname=k,
                provider=provider
            )
            p = p.apply_RatingCurves()
            self.assertIsInstance(p, Parameter)
            self.assertEqual(p.spc_varname, v)
        # =====================================================================
        for k in unvalid:
            p = Parameter(
                varname=k,
                provider=provider
            )
            with self.assertRaises(ValueError):
                p.apply_RatingCurves()
        # =====================================================================

    def test_apply_ReservoirTable(self):
        """
        test apply_ReservoirTable
        """
        # =====================================================================
        provider = 'SPC'
        # =====================================================================
        valid = {
            ('QI', 'H'): 'HI',
            ('QH', 'H'): 'HH',
            ('QI', 'Z'): 'ZI',
            ('QH', 'Z'): 'ZH',
            ('ZI', 'Q'): 'QI',
            ('ZH', 'Q'): 'QH',
            ('ZI', 'Qd'): 'QI',
            ('ZH', 'Qd'): 'QH',
            ('ZI', 'V'): 'VI',
            ('ZH', 'V'): 'VH',
            ('ZI', 'V+0.5*Vd1'): 'VI',
            ('ZH', 'V+0.5*Vd1'): 'VH',
        }
        # =====================================================================
        for k, v in valid.items():
            p = Parameter(
                varname=k[0],
                provider=provider
            )
            p = p.apply_ReservoirTable(col=k[1])
            self.assertIsInstance(p, Parameter)
            self.assertEqual(p.spc_varname, v)
        # =====================================================================

    def test_apply_ReservoirZ0(self):
        """
        test apply_ReservoirZ0
        """
        # =====================================================================
        provider = 'SPC'
        # =====================================================================
        valid = {
            'HI': 'ZI',
            'HH': 'ZH',
            'ZI': 'HI',
            'ZH': 'HH'
        }
        unvalid = [p
                   for p in Parameter.get_spcvarnames()
                   if p not in valid]
        # =====================================================================
        for k, v in valid.items():
            p = Parameter(
                varname=k,
                provider=provider
            )
            p = p.apply_ReservoirZ0()
            self.assertIsInstance(p, Parameter)
            self.assertEqual(p.spc_varname, v)
        # =====================================================================
        for k in unvalid:
            p = Parameter(
                varname=k,
                provider=provider
            )
            with self.assertRaises(ValueError):
                p.apply_ReservoirZ0()
        # =====================================================================
