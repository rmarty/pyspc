#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_MF_OpenAPI in pyspc.io.meteofrance

To run all tests just type:
    python -m unittest test_data_MF_OpenAPI

To run only a class test:
    python -m unittest test_data_MF_OpenAPI.TestMF_OpenAPI

To run only a specific test:
    python -m unittest test_data_MF_OpenAPI.TestMF_OpenAPI.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.core.series import Series
from pyspc.io.meteofrance import read_MF_OpenAPI


class TestMF_OpenAPI(unittest.TestCase):
    """
    MF_OpenAPI_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'mf', 'open_api')

    def test_MN(self):
        """
        Test de lecture/écriture de données RR6 / P6m
        """
        # =====================================================================
        dates = pnd.date_range(dt(2024, 10, 17, 5), dt(2024, 10, 17, 7, 54),
                               freq=td(minutes=6))
        valid = {
            ('43091005', 'P6m'): pnd.DataFrame(
                [0.6, 0.0, 0.0, 0.4, 1.3, 3.2, 4.2, 4.1, 5.2, 6.6, 1.4, 1.2,
                 0.2, 0.0, 1.8, 11.5, 4.5, 1.6, 0.7, 1.6, 1.0, 0.8, 0.8,
                 0.4, 3.5, 4.6, 3.7, 1.8, 6.3, 3.3],
                index=dates
            )
        }
        # =====================================================================
        filename = os.path.join(self.dirname,
                                '43091005_MN_202410170500_202410170700.csv')
        series = read_MF_OpenAPI(filename=filename)
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            kcv = (c, v)
            self.assertIn(kcv, valid)
            valid[kcv].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[kcv],
                               check_less_precise=3)
        # =====================================================================

    def test_H(self):
        """
        Test de lecture/écriture de données RR1 / PH
        """
        # =====================================================================
        dates = pnd.date_range(
            dt(2024, 10, 17), dt(2024, 10, 17, 12), freq='H')
        valid = {
            ('43091005', 'PH'): pnd.DataFrame(
                [9.3, 13.2, 5.6, 14.3, 8.3, 13.2, 26.4, 24.1, 26.0, 7.3,
                 6.3, 5.3, 3.0],
                index=dates
            ),
            ('43091005', 'TH'): pnd.DataFrame(
                [12.6, 12.2, 12.2, 11.9, 12.5, 12.5, 12.3, 12.8, 9.8, 12.7,
                 12.9, 12.7, 12.7],
                index=dates
            )
        }
        # =====================================================================
        filename = os.path.join(self.dirname,
                                '43091005_H_202410170000_202410171200.csv')
        series = read_MF_OpenAPI(filename=filename)
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            kcv = (c, v)
            self.assertIn(kcv, valid)
            valid[kcv].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[kcv],
                               check_less_precise=3)
        # =====================================================================

    def test_Q(self):
        """
        Test de lecture/écriture de données RR1 / PH
        """
        # =====================================================================
        dates = pnd.date_range(dt(2024, 10, 15), dt(2024, 10, 19), freq='D')
        valid = {
            ('43091005', 'PJ'): pnd.DataFrame(
                [38.6, 189.0, 130.4, 2.4, 0.0],
                index=dates
            ),
            ('43091005', 'TJ'): pnd.DataFrame(
                [15.2, 12.4, 11.4, 8.0, 10.4],
                index=dates
            )
        }
        # =====================================================================
        filename = os.path.join(self.dirname,
                                '43091005_Q_202410150000_202410190000.csv')
        series = read_MF_OpenAPI(filename=filename)
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            kcv = (c, v)
            self.assertIn(kcv, valid)
            valid[kcv].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[kcv],
                               check_less_precise=3)
        # =====================================================================
