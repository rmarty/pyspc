#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Data in pyspc.model.grp16

To run all tests just type:
    python -m unittest test_model_GRP16_RT_Data

To run only a class test:
    python -m unittest test_model_GRP16_RT_Data.TestGRPRT_Data

To run only a specific test:
    python -m unittest test_model_GRP16_RT_Data.TestGRPRT_Data.test_init

"""
# Imports
import filecmp
import numpy as np
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.model.grp16 import GRPRT_Data


class TestGRPRT_Data(unittest.TestCase):
    """
    GRPRT_Data class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp16', 'rt')
        self.valid_scen = None

        self.valid_P = pnd.DataFrame({
            'PREFIX': ['PLU']*18,
            'CODE': sorted([43091005, 43101002]*9),
            'DATE': [20170613]*18,
            'HOUR': ['12:00', '13:00', '14:00', '15:00', '16:00', '17:00',
                     '18:00', '19:00', '20:00']*2,
            'VALUE': [0.000, 0.000, 0.000, 23.800, 2.600, 2.200, 1.200, 4.900,
                      34.200, 0.000, 0.000, 0.800, 0.200, 2.600, 21.600,
                      49.000, 123.000, 26.800],
            None: [np.nan]*18,
        })
        self.valid_Q = pnd.DataFrame({
            'PREFIX': ['CQT']*24,
            'CODE': sorted(['K0114020', 'K0114030']*12),
            'DATE': [20170613]*24,
            'HOUR': ['12:00', '13:00', '14:00', '15:00', '16:00', '17:00',
                     '18:00', '19:00', '20:00', '21:00', '22:00', '23:00']*2,
            'VALUE': [0.490, .480, .480, 1.000, 3.920, 3.870,
                      5.210, 17.700, 37.300, 35.900, 19.400,
                      14.400, .570, .570, .560, .590, .580,
                      0.700, 11.200, 121.000, 203.000, 159.000,
                      92.800, 53.800],
            None: [np.nan]*24,
        })
        self.valid_scen = pnd.DataFrame({
            'PREFIX': ['PLU']*22,
            'CODE': sorted(['K0114020', 'K0114030']*11),
            'DATE': [20170613]*22,
            'HOUR': ['12:00', '13:00', '14:00', '15:00', '16:00', '17:00',
                     '18:00', '19:00', '20:00', '21:00', '22:00']*2,
            'VALUE': [0.0, 0.0, 2.0, 2.0, 2.0, 5.0, 5.0, 5.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 10.0, 10.0, 10.0, 15.0, 15.0, 15.0, 0.0, 0.0,
                      0.0],
            None: [np.nan]*22,
        })

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'Debit.txt')
        reader = GRPRT_Data(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'Q')
        self.assertEqual(reader.lineprefix, 'CQT')
        self.assertIsNone(reader.scen)
        # =====================================================================
        filename = os.path.join(self.dirname, 'Scen_006_PluMA.txt')
        reader = GRPRT_Data(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'P')
        self.assertEqual(reader.lineprefix, 'PLU')
        self.assertEqual(reader.scen, '006')
        # =====================================================================

    def test_read_P(self):
        """
        Test de lecture d'un fichier de données OBS pour GRP RT - Cas Pluie
        """
        filename = os.path.join(self.dirname, 'Pluie.txt')
        reader = GRPRT_Data(filename=filename)
        df = reader.read()
        assert_frame_equal(df, self.valid_P)

    def test_read_Q(self):
        """
        Test de lecture d'un fichier de données OBS pour GRP RT - Cas Débit
        """
        filename = os.path.join(self.dirname, 'Debit.txt')
        reader = GRPRT_Data(filename=filename)
        df = reader.read()
        assert_frame_equal(df, self.valid_Q)

    def test_read_scen(self):
        """
        Test de lecture d'un fichier de données OBS pour GRP RT - Cas Pluie
        """
        filename = os.path.join(self.dirname, 'Scen_006_PluMA.txt')
        reader = GRPRT_Data(filename=filename)
        df = reader.read()
        assert_frame_equal(df, self.valid_scen)

    def test_write_P(self):
        """
        Test d'écriture d'un fichier de données OBS pour GRP RT
        """
        filename = os.path.join(self.dirname, 'Pluie.txt')
        tmpfile = os.path.join('data', 'Pluie.txt')
        writer = GRPRT_Data(filename=tmpfile)
        writer.write(self.valid_P)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)

    def test_write_Q(self):
        """
        Test d'écriture d'un fichier de données OBS pour GRP RT
        """
        filename = os.path.join(self.dirname, 'Debit.txt')
        tmpfile = os.path.join('data', 'Debit.txt')
        writer = GRPRT_Data(filename=tmpfile)
        writer.write(self.valid_Q)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)

    def test_write_scen(self):
        """
        Test d'écriture d'un fichier de données OBS pour GRP RT
        """
        filename = os.path.join(self.dirname, 'Scen_006_PluMA.txt')
        tmpfile = os.path.join('data', 'Scen_006_PluMA.txt')
        writer = GRPRT_Data(filename=tmpfile)
        writer.write(self.valid_scen)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
