#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Archive in pyspc.model.grp20

To run all tests just type:
    python -m unittest test_model_GRP20_RT_Archive

To run only a class test:
    python -m unittest test_model_GRP20_RT_Archive.TestGRPRT_Archive

To run only a specific test:
    python -m unittest test_model_GRP20_RT_Archive.TestGRPRT_Archive.test_init

"""
# Imports
from datetime import datetime as dt
import filecmp
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.model.grp20 import GRPRT_Archive


class TestGRPRT_Archive(unittest.TestCase):
    """
    GRPRT_Archive class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp20', 'rt')
        self.valid_P = pnd.DataFrame(
            {'90065003': [
                0.8, 0.6, 1.0, 4.8, 7.2, 8.0, 11.2, 7.4, 7.6, 6.8, 8.8, 7.6,
                5.8, 7.8, 8.2, 5.2, 6.6, 7.2, 9.0, 6.0, 5.0, 3.6, 2.8, 2.0,
                1.8, 2.2, 1.6, 3.2, 1.8, 1.2, 0.8]},
            index=pnd.date_range(
                dt(2007, 1, 18, 12),
                dt(2007, 1, 19, 18),
                freq='H'
            )
        )
        self.valid_P.index.name = 'Date(TU)'
        self.valid_Q = pnd.DataFrame(
            {'RH10585x': [3.950,  4.190,  5.110,  6.210,  6.560,  7.920,
                          13.600, 13.700, 15.800, 16.700, 17.300, 19.200,
                          19.800, 20.400, 21.500, 23.000, 23.600, 23.400,
                          23.600, 24.700, 28.600, 30.600, 35.100, 36.600,
                          35.900, 32.100, 28.600, 26.700, 25.300, 23.500,
                          22.800, 20.800]},
            index=[dt(2007, 1, 18, 18, 0), dt(2007, 1, 18, 18, 8),
                   dt(2007, 1, 18, 18, 30), dt(2007, 1, 18, 18, 54),
                   dt(2007, 1, 18, 19, 0), dt(2007, 1, 18, 19, 30),
                   dt(2007, 1, 18, 22, 0), dt(2007, 1, 18, 22, 3),
                   dt(2007, 1, 18, 23, 0), dt(2007, 1, 18, 23, 30),
                   dt(2007, 1, 19, 0, 0), dt(2007, 1, 19, 1, 0),
                   dt(2007, 1, 19, 1, 30), dt(2007, 1, 19, 2, 45),
                   dt(2007, 1, 19, 3, 24), dt(2007, 1, 19, 4, 0),
                   dt(2007, 1, 19, 4, 30), dt(2007, 1, 19, 5, 30),
                   dt(2007, 1, 19, 6, 0), dt(2007, 1, 19, 6, 30),
                   dt(2007, 1, 19, 7, 30), dt(2007, 1, 19, 7, 45),
                   dt(2007, 1, 19, 8, 30), dt(2007, 1, 19, 10, 0),
                   dt(2007, 1, 19, 11, 30), dt(2007, 1, 19, 12, 15),
                   dt(2007, 1, 19, 12, 49), dt(2007, 1, 19, 13, 30),
                   dt(2007, 1, 19, 14, 15), dt(2007, 1, 19, 15, 30),
                   dt(2007, 1, 19, 16, 30), dt(2007, 1, 19, 18, 0)]
        )
        self.valid_Q.index.name = 'Date(TU)'

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'QV_2007.DAT')
        reader = GRPRT_Archive(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'QV')
        self.assertIsNone(reader.timestep)
        self.assertEqual(reader.year, 2007)
        # =====================================================================
        filename = os.path.join(self.dirname, 'PV_00J01H00M_2007.DAT')
        reader = GRPRT_Archive(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'PV')
        self.assertEqual(reader.timestep, '00J01H00M')
        self.assertEqual(reader.year, 2007)
        # =====================================================================

    def test_read_PV(self):
        """
        Test de lecture d'un fichier Archive de GRP Temps Réel - Pluie hor.
        """
        filename = os.path.join(self.dirname, 'PV_00J01H00M_2007.DAT')
        reader = GRPRT_Archive(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_P, df)

    def test_read_QV(self):
        """
        Test de lecture d'un fichier Archive de GRP Temps Réel - Débit inst.
        """
        filename = os.path.join(self.dirname, 'QV_2007.DAT')
        reader = GRPRT_Archive(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_Q, df)

    def test_write_PV(self):
        """
        Test de l'écriture d'un fichier Archive de GRP Temps Réel
        """
        filename = os.path.join(self.dirname, 'PV_00J01H00M_2007.DAT')
        varname, timestep, year = ('PV', '00J01H00M', 2007)
        writer = GRPRT_Archive()
        writer.write(data=self.valid_P, dirname='data', varname=varname,
                     timestep=timestep, year=year)
        self.assertTrue(filecmp.cmp(
            filename,
            writer.filename
        ))
        os.remove(writer.filename)

    def test_write_QV(self):
        """
        Test de l'écriture d'un fichier Archive de GRP Temps Réel
        """
        filename = os.path.join(self.dirname, 'QV_2007.DAT')
        varname, timestep, year = ('QV', None, 2007)
        writer = GRPRT_Archive()
        writer.write(data=self.valid_Q, dirname='data', varname=varname,
                     timestep=timestep, year=year)
        self.assertTrue(filecmp.cmp(
            filename,
            writer.filename
        ))
        os.remove(writer.filename)

    def test_basename(self):
        """
        Test nom de base
        """
        valid = {
            ('QV', None, 2007): 'QV_2007.DAT',
            ('PV', '00J01H00M', 2007): 'PV_00J01H00M_2007.DAT'
        }
        for k, v in valid.items():
            self.assertEqual(
                 GRPRT_Archive.split_basename(filename=v),
                 k
            )
            self.assertEqual(
                GRPRT_Archive.join_basename(
                    varname=k[0], timestep=k[1], year=k[2]),
                v
            )

    def test_varname(self):
        """
        Test de la correspondance des variables selon GRP et selon SPC_LCI
        """
        valid = ['PV', 'QV', 'TV']
        self.assertEqual(GRPRT_Archive.get_varnames(), valid)
