#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP_Cfg in pyspc.model.grp20

To run all tests just type:
    python -m unittest test_model_GRP20_CAL_Cfg

To run only a class test:
    python -m unittest test_model_GRP20_CAL_Cfg.TestGRP_Cfg

To run only a specific test:
    python -m unittest test_model_GRP20_CAL_Cfg.TestGRP_Cfg.test_init

"""
# Imports
import filecmp
import os
import unittest

from pyspc.model.grp20 import GRP_Run, GRP_Cfg


class TestGRP_Cfg(unittest.TestCase):
    """
    GRP_Cfg class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp20', 'cal')
        self.valid = [
            {'CODE': 'RH10585x', 'PDT': '00J01H00M',
             'NOM': 'La Capricieuse amont', 'SURFACE': 31.0, 'RT': 'TU',
             'DEB': '01/08/2005 00:00', 'FIN': '27/05/2019 23:00',
             'ST': 1, 'SR': 0, 'AT': 0, 'AR': 0, 'HOR1': '00J03H00M',
             'HOR2': '00J00H00M', 'SC1': 5.0, 'SC2': -99.0,
             'SV1': 5.0, 'SV2': 7.0, 'SV3': 9.0, 'NJ': 4, 'HC': '00J03H00M',
             'EC': 3, 'ECART': 5, 'INC': 0},
            {'CODE': 'RH10585x', 'PDT': '01J00H00M',
             'NOM': 'La Capricieuse amont_Jour', 'SURFACE': 31.0, 'RT': 'TU',
             'DEB': '01/08/2005 00:00', 'FIN': '27/05/2019 00:00',
             'ST': 1, 'SR': 0, 'AT': 0, 'AR': 0, 'HOR1': '03J00H00M',
             'HOR2': '00J00H00M', 'SC1': 0.04, 'SC2': -99.0,
             'SV1': 1.0, 'SV2': 3.0, 'SV3': 5.0, 'NJ': 12, 'HC': '03J00H00M',
             'EC': 3, 'ECART': 5, 'INC': 0},
            {'CODE': 'RH10599x', 'PDT': '00J01H00M',
             'NOM': 'La Capricieuse aval', 'SURFACE': 141.0, 'RT': 'TU',
             'DEB': '01/08/2005 00:00', 'FIN': '27/05/2019 23:00',
             'ST': 1, 'SR': 0, 'AT': 0, 'AR': 0, 'HOR1': '00J06H00M',
             'HOR2': '00J00H00M', 'SC1': 10.0, 'SC2': -99.0,
             'SV1': 10.0, 'SV2': 13.0, 'SV3': 15.0, 'NJ': 8, 'HC': '00J06H00M',
             'EC': 10, 'ECART': 6, 'INC': 1},
            {'CODE': 'RH10599x', 'PDT': '01J00H00M',
             'NOM': 'La Capricieuse aval_Jour', 'SURFACE': 141.0, 'RT': 'TU',
             'DEB': '01/08/2005 00:00', 'FIN': '27/05/2019 00:00',
             'ST': 1, 'SR': 0, 'AT': 0, 'AR': 0, 'HOR1': '02J00H00M',
             'HOR2': '00J00H00M', 'SC1': 1.5, 'SC2': -99.0,
             'SV1': 2.0, 'SV2': 3.0, 'SV3': 4.0, 'NJ': 20, 'HC': '05J00H00M',
             'EC': 5, 'ECART': 6, 'INC': 1},
            {'CODE': 'RH10599x', 'PDT': '00J12H00M',
             'NOM': 'La Capricieuse aval__12H', 'SURFACE': 141.0, 'RT': 'TU',
             'DEB': '01/08/2005 06:00', 'FIN': '27/05/2019 06:00',
             'ST': 1, 'SR': 0, 'AT': 0, 'AR': 0, 'HOR1': '01J00H00M',
             'HOR2': '00J00H00M', 'SC1': 3.5, 'SC2': -99.0,
             'SV1': 4.0, 'SV2': 5.0, 'SV3': 6.0, 'NJ': 16, 'HC': '04J00H00M',
             'EC': 8, 'ECART': 6, 'INC': 1},
        ]
        self.valid = [
            GRP_Run(**r) for r in self.valid
        ]

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        with self.assertRaises(ValueError):
            GRP_Cfg()
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        self.assertEqual(runs.filename, filename)
        self.assertIsInstance(runs, list)
        # =====================================================================

    def test_read(self):
        """
        Test de la lecture du fichier de configuration GRP *Calage*
        """

        # =====================================================================
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
#        for run in runs:
#            print('='*50)
#            print('{' + ', '.join(["'{}': {}".format(k, v)
#                                   for k, v in run._asdict().items()]) + '},')
#            print(run._asdict())
        self.assertEqual(runs, self.valid)
        # =====================================================================

    def test_basin_values(self):
        """
        Test de la lecture du fichier de configuration GRP *Calage*
        Cas des clés des bassins
        """
        # =====================================================================
        valid = [
            ('RH10585x', '00J01H00M', 'La Capricieuse amont', 31.0, 'TU',
             '01/08/2005 00:00', '27/05/2019 23:00'),
            ('RH10585x', '01J00H00M', 'La Capricieuse amont_Jour', 31.0, 'TU',
             '01/08/2005 00:00', '27/05/2019 00:00'),
            ('RH10599x', '00J01H00M', 'La Capricieuse aval', 141.0, 'TU',
             '01/08/2005 00:00', '27/05/2019 23:00'),
            ('RH10599x', '01J00H00M', 'La Capricieuse aval_Jour', 141.0, 'TU',
             '01/08/2005 00:00', '27/05/2019 00:00'),
            ('RH10599x', '00J12H00M', 'La Capricieuse aval__12H', 141.0, 'TU',
             '01/08/2005 06:00', '27/05/2019 06:00')
        ]
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        calval = runs.get_basinvalues()
#        for x in calval:
#            print(x)
        self.assertEqual(calval, valid)
        # =====================================================================

    def test_calibration_values(self):
        """
        Test de la lecture du fichier de configuration GRP *Calage*
        Cas des clés de calibration
        """
        # =====================================================================
        valid = [
            ('RH10585x', '00J01H00M', '01/08/2005 00:00', '27/05/2019 23:00',
             1, 0, 0, 0, '00J03H00M', '00J00H00M', 5.0, -99.0),
            ('RH10585x', '01J00H00M', '01/08/2005 00:00', '27/05/2019 00:00',
             1, 0, 0, 0, '03J00H00M', '00J00H00M', 0.04, -99.0),
            ('RH10599x', '00J01H00M', '01/08/2005 00:00', '27/05/2019 23:00',
             1, 0, 0, 0, '00J06H00M', '00J00H00M', 10.0, -99.0),
            ('RH10599x', '01J00H00M', '01/08/2005 00:00', '27/05/2019 00:00',
             1, 0, 0, 0, '02J00H00M', '00J00H00M', 1.5, -99.0),
            ('RH10599x', '00J12H00M', '01/08/2005 06:00', '27/05/2019 06:00',
             1, 0, 0, 0, '01J00H00M', '00J00H00M', 3.5, -99.0),
        ]
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        calval = runs.get_calibrationvalues()
        self.assertEqual(calval, valid)
        # =====================================================================

    def test_model_values(self):
        """
        Test de la lecture du fichier de configuration GRP *Calage*
        Cas des clés des modèles
        """
        # =====================================================================
        valid = [
            (1, 0, 0, 0, '00J03H00M', '00J00H00M', 5.0, -99.0),
            (1, 0, 0, 0, '03J00H00M', '00J00H00M', 0.04, -99.0),
            (1, 0, 0, 0, '00J06H00M', '00J00H00M', 10.0, -99.0),
            (1, 0, 0, 0, '02J00H00M', '00J00H00M', 1.5, -99.0),
            (1, 0, 0, 0, '01J00H00M', '00J00H00M', 3.5, -99.0)
        ]
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        calval = runs.get_modelvalues()
        self.assertEqual(calval, valid)
        # =====================================================================

    def test_write(self):
        """
        Test de l'écriture du fichier de configuration GRP *Calage*
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        tmpfile = os.path.join('data', 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=tmpfile)
        runs.extend(self.valid)
        runs.write()
        self.assertTrue(filecmp.cmp(filename, tmpfile))
        os.remove(tmpfile)
        # =====================================================================

    def test_product_0(self):
        """
        Test du produit des configurations - Cas 0 paramètre demandé
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        tmpfile = os.path.join('data', 'LISTE_BASSINS.DAT')
        newruns = runs.product(output_filename=tmpfile, filenames={})
        self.assertIsInstance(newruns, GRP_Cfg)
        self.assertEqual(len(newruns), 5)
        # =====================================================================

    def test_product_1(self):
        """
        Test du produit des configurations - Cas 1 paramètre demandé
        """
        # =====================================================================
        filenames = {'SV1': os.path.join(self.dirname, 'SV.txt')}
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        tmpfile = os.path.join('data', 'LISTE_BASSINS.DAT')
        newruns = runs.product(output_filename=tmpfile, filenames=filenames)
        self.assertIsInstance(newruns, GRP_Cfg)
        self.assertEqual(len(newruns), 9)
        # =====================================================================
        filenames = {'SV': os.path.join(self.dirname, 'SV.txt')}
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        tmpfile = os.path.join('data', 'LISTE_BASSINS.DAT')
        newruns = runs.product(output_filename=tmpfile, filenames=filenames)
        self.assertIsInstance(newruns, GRP_Cfg)
        self.assertEqual(len(newruns), 9)
        # =====================================================================

    def test_product_2(self):
        """
        Test du produit des configurations - Cas 2 paramètres demandés
        """
        # =====================================================================
        filenames = {
            'SV1': os.path.join(self.dirname, 'SV.txt'),
            'SC1': os.path.join(self.dirname, 'SC.txt')
        }
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        tmpfile = os.path.join('data', 'LISTE_BASSINS.DAT')
        newruns = runs.product(output_filename=tmpfile, filenames=filenames)
        self.assertIsInstance(newruns, GRP_Cfg)
        self.assertEqual(len(newruns), 18)
        # =====================================================================
