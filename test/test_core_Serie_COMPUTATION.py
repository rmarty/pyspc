#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Serie in pyspc.core.serie - computation

To run all tests just type:
    python -m unittest test_core_Serie_COMPUTATION

To run only a class test:
    python -m unittest test_core_Serie_COMPUTATION.TestSerie

To run only a specific test:
    python -m unittest test_core_Serie_COMPUTATION.TestSerie.test_bwd

"""
# Imports
from datetime import datetime as dt
import numpy as np
import pandas as pnd
from pandas.util.testing import assert_frame_equal
from random import Random
import unittest
import warnings

# Imports pyspc
from pyspc.core.serie import Serie


class TestSerie(unittest.TestCase):
    """
    Serie class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        self.code = 'K0000000'
        self.provider = 'SPC'
        self.varname = 'QH'
        self.list_dtval = [
            [dt(2014, 11, 4, 0), 35.600],
            [dt(2014, 11, 4, 1), 41.200],
            [dt(2014, 11, 4, 2), 46.900],
            [dt(2014, 11, 4, 3), 62.900],
            [dt(2014, 11, 4, 4), 85.700],
            [dt(2014, 11, 4, 5), 106.000],
            [dt(2014, 11, 4, 6), 118.000],
            [dt(2014, 11, 4, 7), 115.000],
            [dt(2014, 11, 4, 8), 103.000],
            [dt(2014, 11, 4, 9), 92.500],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 11), 82.300],
            [dt(2014, 11, 4, 12), 78.300]
        ]

    def test_ath(self):
        """
        Test méthode above_threshold
        """
        # =====================================================================
        code_valid = '{0}_ath100'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([x[1] if x[1] > 100 else np.nan
                                 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [(code_valid, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie.above_threshold(threshold=100)
        assert_frame_equal(serie.data_frame, valid_1)
        self.assertEqual(serie.code, code_valid)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie.above_threshold(threshold=100, inplace=True)
        valid_1.columns = [(self.code, self.varname)]
        assert_frame_equal(serie.data_frame, valid_1)
        self.assertEqual(serie.code, self.code)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        with self.assertRaises(ValueError):
            serie.above_threshold()
        with self.assertRaises(ValueError):
            serie.above_threshold(threshold=None)
        with self.assertRaises(ValueError):
            serie.above_threshold(threshold='1')
        # =====================================================================

    def test_bth(self):
        """
        Test méthode below_threshold
        """
        # =====================================================================
        code_valid = '{0}_bth100'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([x[1] if x[1] < 100 else np.nan
                                 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [(code_valid, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie.below_threshold(threshold=100)
        assert_frame_equal(serie.data_frame, valid_1)
        self.assertEqual(serie.code, code_valid)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie.below_threshold(threshold=100, inplace=True)
        valid_1.columns = [(self.code, self.varname)]
        assert_frame_equal(serie.data_frame, valid_1)
        self.assertEqual(serie.code, self.code)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        with self.assertRaises(ValueError):
            serie.below_threshold()
        with self.assertRaises(ValueError):
            serie.below_threshold(threshold=None)
        with self.assertRaises(ValueError):
            serie.below_threshold(threshold='1')
        # =====================================================================

    def test_mv(self):
        """
        Test méthode counter_missing
        """
        # =====================================================================
        valid_1 = pnd.DataFrame([x[1] if x[1] > 100 else np.nan
                                 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [(self.code, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid_1,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        self.assertEqual(serie.counter_missing(), 9)
        # =====================================================================

    def test_csum(self):
        """
        Test méthode cumsum
        """
        # =====================================================================
        code_valid = '{0}_csum'.format(self.code)
        valid = pnd.DataFrame([x[1] if x[1] > 100 else np.nan
                               for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([x[1] if x[1] > 100 else np.nan
                                 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [(code_valid, self.varname)]
        valid_1 = valid_1.cumsum(skipna=True)
        valid_2 = pnd.DataFrame([x[1] if x[1] > 100 else np.nan
                                 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_2.columns = [(code_valid, self.varname)]
        valid_2 = valid_2.cumsum(skipna=False)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie.cumsum()
        assert_frame_equal(serie.data_frame, valid_1)
        self.assertEqual(serie.code, code_valid)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie.cumsum(skipna=False)
        assert_frame_equal(serie.data_frame, valid_2)
        self.assertEqual(serie.code, code_valid)
        # =====================================================================

    def test_describe(self):
        """
        Test describe
        """
        # =====================================================================
        serie = pnd.DataFrame([x[1]
                               for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        serie.columns = [(self.code, self.varname)]
        serie = Serie(
            datval=serie,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        # =====================================================================
        valid = {
            'count': 13.000000,
            'mean': 81.061539,
            'std': 27.328329,
            'min': 35.599998,
            '10%': 42.340001,
            '50%': 85.699997,
            '90%': 113.200000,
            'max': 118.000000,
        }
        dsc = serie.describe()
        self.assertIsInstance(dsc, pnd.DataFrame)
        for k, v in valid.items():
            self.assertAlmostEqual(
                dsc[(self.code, self.varname)].loc[k], v, places=4)
        # =====================================================================
        valid = {
            'count': 13.000000,
            'mean': 81.061539,
            'std': 27.328329,
            'min': 35.599998,
            '10%': 42.340001,
            '25%': 62.900002,
            '50%': 85.699997,
            '75%': 103.000000,
            '90%': 113.200000,
            'max': 118.000000,
        }
        dsc = serie.describe(freqs=[10, 25, 50, 75, 90])
        self.assertIsInstance(dsc, pnd.DataFrame)
        for k, v in valid.items():
            self.assertAlmostEqual(
                dsc[(self.code, self.varname)].loc[k], v, places=4)
        dsc = serie.describe(freqs=[0.10, 0.25, 0.50, 0.75, 0.90])
        self.assertIsInstance(dsc, pnd.DataFrame)
        for k, v in valid.items():
            self.assertAlmostEqual(
                dsc[(self.code, self.varname)].loc[k], v, places=4)
        # =====================================================================

    def test_annual_max(self):
        """
        Test méthode find_annual_max
        """
        self.list_dtval.extend([
            [dt(2003, 12, 1, 18), 55.5000],
            [dt(2003, 12, 1, 19), 71.6000],
            [dt(2003, 12, 1, 20), 86.9000],
            [dt(2003, 12, 1, 21), 99.0000],
            [dt(2003, 12, 1, 22), 113.0000],
            [dt(2003, 12, 1, 23), 121.0000],
            [dt(2003, 12, 2, 0), 115.0000],
            [dt(2003, 12, 2, 1), 110.0000],
            [dt(2003, 12, 2, 2), 103.0000],
            [dt(2003, 12, 2, 3), 101.0000],
            [dt(2003, 12, 2, 4), 103.0000],
            [dt(2003, 12, 2, 5), 118.0000],
            [dt(2003, 12, 2, 6), 133.0000],
            [dt(2003, 12, 2, 7), 155.0000],
            [dt(2003, 12, 2, 8), 178.0000],
            [dt(2003, 12, 2, 9), 193.0000],
            [dt(2003, 12, 2, 10), 197.0000],
            [dt(2003, 12, 2, 11), 188.0000],
            [dt(2003, 12, 2, 12), 164.0000],
            [dt(2003, 12, 2, 13), 139.0000],
            [dt(2003, 12, 2, 14), 121.0000],
            [dt(2003, 12, 2, 15), 107.0000],
            [dt(2003, 12, 2, 16), 97.4000],
            [dt(2003, 12, 2, 17), 89.4000],
            [dt(2003, 12, 2, 18), 81.4000]
        ])
        valid_ymax = {
            2004: {
                'max': 197.0,
                'maxdt': dt(2003, 12, 2, 10),
                'pmv': 0.9971
            },
            2015: {
                'max': 118.0,
                'maxdt': dt(2014, 11, 4, 6),
                'pmv': 0.9985
            },
        }
        # =====================================================================
        valid = pnd.DataFrame([x[1]
                               for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        ymax = serie.find_annual_max()
        self.assertIsInstance(ymax, dict)
        for k, v in valid_ymax.items():
            self.assertIn(k, ymax.keys())
            for k2, v2 in v.items():
                self.assertIn(k2, ymax[k].keys())
                if k2 == 'pmv':
                    self.assertAlmostEqual(v2, ymax[k][k2], places=3)
                else:
                    self.assertEqual(v2, ymax[k][k2])
        # =====================================================================

    def test_annual_min(self):
        """
        Test méthode find_annual_min
        """
        self.list_dtval.extend([
            [dt(2003, 12, 1, 18), 55.5000],
            [dt(2003, 12, 1, 19), 71.6000],
            [dt(2003, 12, 1, 20), 86.9000],
            [dt(2003, 12, 1, 21), 99.0000],
            [dt(2003, 12, 1, 22), 113.0000],
            [dt(2003, 12, 1, 23), 121.0000],
            [dt(2003, 12, 2, 0), 115.0000],
            [dt(2003, 12, 2, 1), 110.0000],
            [dt(2003, 12, 2, 2), 103.0000],
            [dt(2003, 12, 2, 3), 101.0000],
            [dt(2003, 12, 2, 4), 103.0000],
            [dt(2003, 12, 2, 5), 118.0000],
            [dt(2003, 12, 2, 6), 133.0000],
            [dt(2003, 12, 2, 7), 155.0000],
            [dt(2003, 12, 2, 8), 178.0000],
            [dt(2003, 12, 2, 9), 193.0000],
            [dt(2003, 12, 2, 10), 197.0000],
            [dt(2003, 12, 2, 11), 188.0000],
            [dt(2003, 12, 2, 12), 164.0000],
            [dt(2003, 12, 2, 13), 139.0000],
            [dt(2003, 12, 2, 14), 121.0000],
            [dt(2003, 12, 2, 15), 107.0000],
            [dt(2003, 12, 2, 16), 97.4000],
            [dt(2003, 12, 2, 17), 89.4000],
            [dt(2003, 12, 2, 18), 81.4000]
        ])
        valid_ymin = {
            2003: {
                'min': 55.5000,
                'mindt': dt(2003, 12, 1, 18),
                'pmv': 0.9971
            },
            2014: {
                'min': 35.600,
                'mindt': dt(2014, 11, 4, 0),
                'pmv': 0.9985
            },
        }
        # =====================================================================
        valid = pnd.DataFrame([x[1]
                               for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        ymin = serie.find_annual_min()
        self.assertIsInstance(ymin, dict)
        for k, v in valid_ymin.items():
            self.assertIn(k, ymin.keys())
            for k2, v2 in v.items():
                self.assertIn(k2, ymin[k].keys())
                self.assertAlmostEqual(v2, ymin[k][k2], places=3)
#                if k2 == 'pmv':
#                    self.assertAlmostEqual(v2, ymin[k][k2], places=3)
#                else:
#                    self.assertEqual(v2, ymin[k][k2])
        # =====================================================================

    def test_max(self):
        """
        Test de la méthode max
        """
        # =====================================================================
        valid = pnd.DataFrame([x[1]
                               for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        max_value, max_dt = serie.max()
        self.assertEqual(max_value, 118.0)
        self.assertEqual(max_dt, dt(2014, 11, 4, 6))
        # =====================================================================

    def test_min(self):
        """
        Test de la méthode min
        """
        # =====================================================================
        valid = pnd.DataFrame([x[1]
                               for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        min_value, min_dt = serie.min()
        self.assertAlmostEqual(min_value, 35.6, places=3)
        self.assertEqual(min_dt, dt(2014, 11, 4, 0))
        # =====================================================================

    def test_regime_PH(self):
        """
        Test méthode regime - PH
        """
        # =====================================================================
        random = Random('pyspc')
        # =====================================================================
        dates = pnd.date_range(dt(2000, 1, 1), dt(2023, 7, 1), freq='H')
        # source: FICHECLIM_03155003.pdf (LURCY−LEVIS SA (03))
        means = [x / 30.
                 for x in [53.0, 47.3, 45.9, 64.3, 77.6, 57.8,
                           55.3, 61.6, 60.3, 63.4, 69.1, 61.6]]
        stds = [x * 0.9 for x in [1.2, 1.2, 0.9, 1.9, 2.5, 1.9,
                                  1.5, 1.8, 2.1, 1.7, 1.8, 1.4]]
        shapes = [m**2 / s**2 for m, s in zip(means, stds)]
        scales = [m / s**2 for m, s in zip(means, stds)]
        sample = [random.gammavariate(shapes[d.month-1],
                                      scales[d.month-1]) / 24.
                  for d in dates]
        df = pnd.DataFrame(sample, index=dates)
        serie = Serie(df, code='MASTATION', varname='PH')
        # =====================================================================
        # CAS DAYOFYEAR
        regime = serie.regime(groupby='dayofyear')
        self.assertIsInstance(regime, pnd.DataFrame)
        self.assertEqual(len(regime.index), 365)
        self.assertEqual(len(regime.columns), 8)
        self.assertListEqual(regime.columns.to_list(),
                             [('MASTATION', 'PJ', 'amin'),
                              ('MASTATION', 'PJ', 'mean'),
                              ('MASTATION', 'PJ', 'amax'),
                              ('MASTATION', 'PJ', 'q10'),
                              ('MASTATION', 'PJ', 'q25'),
                              ('MASTATION', 'PJ', 'q50'),
                              ('MASTATION', 'PJ', 'q75'),
                              ('MASTATION', 'PJ', 'q90')])
        # =====================================================================
        # CAS MONTH
        valid = pnd.DataFrame(
            {('MASTATION', 'PM', 'amin'): [
                120.27869257400982, 76.58250399210651, 248.2080802002774,
                33.139389938764104, 19.56549028457899, 23.97485082999946,
                56.032042567456614, 37.76472274559556, 17.264093295664743,
                50.2476243834164, 51.399853868695644, 100.32770497951164],
             ('MASTATION', 'PM', 'mean'): [
                 125.54966157055107, 80.82655484783936, 256.6352872394741,
                 34.50535420107864, 20.97866900645747, 25.33833941985165,
                 58.45066135414932, 39.237826887305864, 19.26552050519626,
                 53.216579097024685, 53.2195592208493, 106.17424175809528],
             ('MASTATION', 'PM', 'amax'): [
                 130.87886783851934, 84.861205545055, 262.7430999089087,
                 37.00687934881292, 22.424463005239755, 26.858427400049745,
                 61.12894786884297, 41.6868239361353, 20.91424176088218,
                 55.86906531955163, 55.50917367882319, 113.32651948176364],
             ('MASTATION', 'PM', 'q10'): [
                 122.88913957893337, 77.85490092466249, 253.54530121657086,
                 33.339052105170936, 20.15607892335094, 24.486979660031192,
                 56.53562641457125, 37.996969618392, 18.577248926770487,
                 51.88661556745586, 51.6675692747832, 103.08521611229293],
             ('MASTATION', 'PM', 'q25'): [
                 124.18158722261362, 78.977126607455, 255.3171763566656,
                 33.62328198795981, 20.588642568369092, 24.67796753199098,
                 57.41961267547643, 38.6822013426422, 18.850138649396364,
                 52.27497963123074, 52.320379839532066, 104.98259604015524],
             ('MASTATION', 'PM', 'q50'): [
                 125.1199755179705, 80.60168794623159, 256.16947066089074,
                 34.43210670781798, 21.019510604151485, 25.267396242938396,
                 58.64520008666637, 38.98999225141334, 19.290564054739253,
                 53.15900305537108, 53.11163844498111, 106.13846845649691],
             ('MASTATION', 'PM', 'q75'): [
                 127.27852125947567, 82.67631048050715, 258.62589903480364,
                 35.31097541332589, 21.336420498877473, 25.86619577551369,
                 59.24013659679147, 39.620692556124254, 19.604258322300502,
                 54.107842581926334, 54.1178277121833, 107.36274970908639],
             ('MASTATION', 'PM', 'q90'): [
                 128.3922934804134, 83.93166090727426, 260.4445920874796,
                 35.87189641988972, 21.778912832926665, 26.268034530663947,
                 60.17407327670492, 40.64075909396221, 20.30791346092347,
                 54.82076242370583, 54.970245279875, 108.54960463790681],
             }, index=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        )
        valid.index.name = 'month'
        regime = serie.regime(groupby='month')
        regime.to_csv('toto.csv')
        self.assertIsInstance(regime, pnd.DataFrame)
        self.assertEqual(len(regime.index), 12)
        self.assertEqual(len(regime.columns), 8)
        assert_frame_equal(regime, valid)
        # =====================================================================

    def test_regime_TJ(self):
        """
        Test méthode regime - TJ
        """
        # =====================================================================
        random = Random('pyspc')
        # =====================================================================
        dates = pnd.date_range(dt(2000, 1, 1), dt(2023, 7, 1), freq='D')
        df = pnd.DataFrame(
            {'sigma': [5 + 4 * np.sin(
                2 * np.pi * min(365, d.timetuple().tm_yday/365.0))
                       for d in dates],
             'mu': [10 * np.cos(
                 2 * np.pi * min(365, d.timetuple().tm_yday/365.0))
                    for d in dates]},
            index=dates
        )
        df['normalvariate'] = df.apply(
            lambda row: random.normalvariate(row['mu'], row['sigma']), axis=1)
        serie = Serie(df['normalvariate'], code='MASTATION', varname='TJ')
        # =====================================================================
        # CAS DAYOFYEAR
        regime = serie.regime(groupby='dayofyear')
        self.assertIsInstance(regime, pnd.DataFrame)
        self.assertEqual(len(regime.index), 365)
        self.assertEqual(len(regime.columns), 8)
        self.assertListEqual(regime.columns.to_list(),
                             [('MASTATION', 'TJ', 'amin'),
                              ('MASTATION', 'TJ', 'mean'),
                              ('MASTATION', 'TJ', 'amax'),
                              ('MASTATION', 'TJ', 'q10'),
                              ('MASTATION', 'TJ', 'q25'),
                              ('MASTATION', 'TJ', 'q50'),
                              ('MASTATION', 'TJ', 'q75'),
                              ('MASTATION', 'TJ', 'q90')])
        # =====================================================================
        # CAS MONTH
        valid = pnd.DataFrame(
            {('MASTATION', 'TJ', 'amin'): [
                -10.987349, -20.927502, -28.532033, -28.076151, -36.541390,
                -29.483204, -24.401203, -16.569580, -8.038793, -2.843159,
                0.709528, -2.465142],
             ('MASTATION', 'TJ', 'mean'): [
                9.775277, 7.647808, 2.447466, -2.284140, -7.150944, -9.808500,
                -9.630296, -6.969922, -2.551329, 2.679328, 6.937878, 9.774600],
             ('MASTATION', 'TJ', 'amax'): [
                30.366130, 31.096103, 29.276437, 22.403386, 18.317250,
                8.399533, 5.242768, 0.025375, 2.903838, 8.982568, 15.738727,
                22.267054],
             ('MASTATION', 'TJ', 'q10'): [
                2.175369, -2.161437, -9.260649, -13.540367, -17.218375,
                -17.139951, -15.005319, -10.138419, -4.974818, 0.252474,
                3.912094, 5.040022],
             ('MASTATION', 'TJ', 'q25'): [
                5.483506, 2.162670, -3.558794, -8.314738, -12.848633,
                -13.776696, -12.217287, -8.672136, -3.823146, 1.242906,
                5.228819, 7.199803],
             ('MASTATION', 'TJ', 'q50'): [
                9.881925, 7.702300, 2.684536, -2.430664, -7.027587, -9.963173,
                -9.512707, -6.874644, -2.513706, 2.467920, 6.599372, 9.765345],
             ('MASTATION', 'TJ', 'q75'): [
                13.756946, 13.467442, 8.463805, 3.609519, -1.690938, -5.798924,
                -7.089044, -5.273049, -1.179915, 3.998118, 8.564590, 12.172330
                ],
             ('MASTATION', 'TJ', 'q90'): [
                17.887767, 17.929788, 14.078995, 9.348240, 3.763987, -2.059937,
                -4.420848, -3.783082, -0.131121, 5.349232, 10.420266,
                14.699299],
             }, index=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        )
        valid.index.name = 'month'
        regime = serie.regime(groupby='month')
        self.assertIsInstance(regime, pnd.DataFrame)
        self.assertEqual(len(regime.index), 12)
        self.assertEqual(len(regime.columns), 8)
        assert_frame_equal(regime, valid)
        # =====================================================================
        with self.assertRaises(ValueError):
            serie.regime(groupby='me')
        with self.assertRaises(ValueError):
            serie.regime(freqs='me')
        # =====================================================================

    def test_timecentroid(self):
        """
        Test méthode timecentroid
        """
        # =====================================================================
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        serie = Serie(datval=valid, code=self.code, varname=self.varname)
        tc = serie.timecentroid()
        self.assertEqual(tc, dt(2014, 11, 4, 8))
        # =====================================================================
        valid = pnd.DataFrame([x[1] if x[1] > 100 or x[1] < 80 else np.nan
                               for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        serie = Serie(datval=valid, code=self.code, varname=self.varname)
        tc = serie.timecentroid()
        self.assertEqual(tc, dt(2014, 11, 4, 7))
        # =====================================================================
