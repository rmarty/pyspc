#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP_Cfg in pyspc.model.grp18

To run all tests just type:
    python -m unittest test_model_GRP18_CAL_Cfg

To run only a class test:
    python -m unittest test_model_GRP18_CAL_Cfg.TestGRP_Cfg

To run only a specific test:
    python -m unittest test_model_GRP18_CAL_Cfg.TestGRP_Cfg.test_init

"""
# Imports
import filecmp
import os
import unittest

from pyspc.model.grp18 import GRP_Run, GRP_Cfg


class TestGRP_Cfg(unittest.TestCase):
    """
    GRP_Cfg class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp18', 'cal')
        self.valid = [
            {'CODE': 'RH10585x', 'PDT': '00J01H00M',
             'NOM': 'La Capricieuse amont',
             'ST': 1, 'SR': 1, 'AT': 0, 'AR': 0, 'SURFACE': 31.0, 'RT': 'TU',
             'HOR': '00J03H00M', 'SC': 5.0, 'DEB': '01/08/2005 00:00',
             'FIN': '27/05/2019 23:00', 'SV': 5.0, 'NJ': 4, 'HC': '00J03H00M',
             'EC': 3, 'ECART': 5, 'INC': 0},
            {'CODE': 'RH10585x', 'PDT': '01J00H00M',
             'NOM': 'La Capricieuse amont_Jour',
             'ST': 1, 'SR': 0, 'AT': 1, 'AR': 0, 'SURFACE': 31.0, 'RT': 'TU',
             'HOR': '03J00H00M', 'SC': 0.04, 'DEB': '01/08/2005 00:00',
             'FIN': '27/05/2019 00:00', 'SV': 1.0, 'NJ': 12, 'HC': '03J00H00M',
             'EC': 3, 'ECART': 5, 'INC': 0},
            {'CODE': 'RH10599x', 'PDT': '00J01H00M',
             'NOM': 'La Capricieuse aval',
             'ST': 1, 'SR': 1, 'AT': 1, 'AR': 0, 'SURFACE': 141.0, 'RT': 'TU',
             'HOR': '00J06H00M', 'SC': 10.0, 'DEB': '01/08/2005 00:00',
             'FIN': '27/05/2019 23:00', 'SV': 10.0, 'NJ': 8, 'HC': '00J06H00M',
             'EC': 10, 'ECART': 6, 'INC': 1},
            {'CODE': 'RH10599x', 'PDT': '01J00H00M',
             'NOM': 'La Capricieuse aval_Jour',
             'ST': 1, 'SR': 0, 'AT': 0, 'AR': 0, 'SURFACE': 141.0, 'RT': 'TU',
             'HOR': '02J00H00M', 'SC': 1.5, 'DEB': '01/08/2005 00:00',
             'FIN': '27/05/2019 00:00', 'SV': 2.0, 'NJ': 20, 'HC': '05J00H00M',
             'EC': 5, 'ECART': 6, 'INC': 1},
            {'CODE': 'RH10599x', 'PDT': '00J12H00M',
             'NOM': 'La Capricieuse aval__12H',
             'ST': 1, 'SR': 0, 'AT': 1, 'AR': 1, 'SURFACE': 141.0, 'RT': 'TU',
             'HOR': '01J00H00M', 'SC': 3.5, 'DEB': '01/08/2005 06:00',
             'FIN': '27/05/2019 06:00', 'SV': 4.0, 'NJ': 16, 'HC': '04J00H00M',
             'EC': 8, 'ECART': 6, 'INC': 1},
        ]
        self.valid = [
            GRP_Run(**r) for r in self.valid
        ]

    def test_init(self):
        """
        Test de la création de l'instance
        """
        with self.assertRaises(ValueError):
            GRP_Cfg()
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        self.assertEqual(runs.filename, filename)
        self.assertIsInstance(runs, list)

    def test_read(self):
        """
        Test de la lecture du fichier de configuration GRP *Calage*
        """

        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
#        for run in runs:
#            print('{' + ', '.join(["'{}': {}".format(k, v)
#                                   for k, v in run._asdict().items()]) + '},')
#            print(run._asdict())
        self.assertEqual(runs, self.valid)

    def test_calibration_values(self):
        """
        Test de la lecture du fichier de configuration GRP *Calage*
        Cas des clés de calibration
        """
        valid = [
            ('RH10585x', '00J01H00M', 'La Capricieuse amont',
             1, 1, 0, 0, 31.0,  'TU', '00J03H00M', 5.0,
             '01/08/2005 00:00', '27/05/2019 23:00'),
            ('RH10585x', '01J00H00M', 'La Capricieuse amont_Jour',
             1, 0, 1, 0, 31.0, 'TU', '03J00H00M', 0.04,
             '01/08/2005 00:00', '27/05/2019 00:00'),
            ('RH10599x', '00J01H00M', 'La Capricieuse aval',
             1, 1, 1, 0, 141.0, 'TU', '00J06H00M', 10.0,
             '01/08/2005 00:00', '27/05/2019 23:00'),
            ('RH10599x', '01J00H00M', 'La Capricieuse aval_Jour',
             1, 0, 0, 0, 141.0, 'TU', '02J00H00M', 1.5,
             '01/08/2005 00:00', '27/05/2019 00:00'),
            ('RH10599x', '00J12H00M', 'La Capricieuse aval__12H',
             1, 0, 1, 1, 141.0, 'TU', '01J00H00M', 3.5,
             '01/08/2005 06:00', '27/05/2019 06:00')
        ]
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        calval = runs.get_calibrationvalues()
        self.assertEqual(calval, valid)

    def test_write(self):
        """
        Test de l'écriture du fichier de configuration GRP *Calage*
        """
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        tmpfile = os.path.join('data', 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=tmpfile)
        runs.extend(self.valid)
        runs.write()
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)

    def test_product_0(self):
        """
        Test du produit des configurations - Cas 0 paramètre demandé
        """
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        tmpfile = os.path.join('data', 'LISTE_BASSINS.DAT')
        newruns = runs.product(output_filename=tmpfile, filenames={})
        self.assertIsInstance(newruns, GRP_Cfg)
        self.assertEqual(len(newruns), 5)

    def test_product_1(self):
        """
        Test du produit des configurations - Cas 1 paramètre demandé
        """
        filenames = {
            'SV': os.path.join(self.dirname, 'SV.txt'),
        }
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        tmpfile = os.path.join('data', 'LISTE_BASSINS.DAT')
        newruns = runs.product(output_filename=tmpfile, filenames=filenames)
        self.assertIsInstance(newruns, GRP_Cfg)
        self.assertEqual(len(newruns), 9)

    def test_product_2(self):
        """
        Test du produit des configurations - Cas 2 paramètres demandés
        """
        filenames = {
            'SV': os.path.join(self.dirname, 'SV.txt'),
            'SC': os.path.join(self.dirname, 'SC.txt')
        }
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        tmpfile = os.path.join('data', 'LISTE_BASSINS.DAT')
        newruns = runs.product(output_filename=tmpfile, filenames=filenames)
        self.assertIsInstance(newruns, GRP_Cfg)
        self.assertEqual(len(newruns), 18)
