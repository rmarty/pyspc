#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Provider in pyspc.core.Provider

To run all tests just type:
    python -m unittest test_core_Provider

To run only a class test:
    python -m unittest test_core_Provider.TestProvider

To run only a specific test:
    python -m unittest test_core_Provider.TestProvider.test_init

"""
# Imports
import unittest

# Imports pyspc
from pyspc.core.provider import Provider


class TestProvider(unittest.TestCase):
    """
    Provider class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """

    def test_init(self):
        """
        Test de la création de l'instance
        """
        name = 'Météo-France'
        prov = Provider(name=name)
        self.assertEqual(prov.name, name)
