#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary plathynes2csv

To run all tests just type:
    python -m unittest test_bin_plathynes2csv

To run only a class test:
    python -m unittest test_bin_plathynes2csv.Test_plathynes2csv

To run only a specific test:
    python -m unittest test_bin_plathynes2csv.Test_plathynes2csv.test_read

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import plathynes2csv as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_plathynes2csv(unittest.TestCase):
    """
    _plathynes2csv class test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'plathynes2csv')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_in2 = os.path.join('data', 'model', 'plathynes')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_plathynes_export(self):
        """
        Test PLATHYNES - Export
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/plathynes2csv.py',
            '-O', self.dir_out,
            '-I', self.dir_in2,
            '-t', 'export',
            '-d', 'plathynes_export_1.txt',
            '-C', 'pyspc',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.plathynes2csv()
        self.assertEqual(args.data_filename, 'plathynes_export_1.txt')
        self.assertEqual(args.input_dir, self.dir_in2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'export')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
#        self.assertEqual(args.overwrite, True)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for filename in os.listdir(self.dir_out):
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dir_ctl, filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier PLATHYNES {d} de type '{t}' "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, d=args.data_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_plathynes_data_mqi(self):
        """
        Test PLATHYNES - Data - .mqi
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/plathynes2csv.py',
            '-O', self.dir_out,
            '-I', self.dir_in2,
            '-t', 'data',
            '-d', '8001_1.mqi',
            '-C', 'grp18',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.plathynes2csv()
        self.assertEqual(args.data_filename, '8001_1.mqi')
        self.assertEqual(args.input_dir, self.dir_in2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'data')
        self.assertEqual(args.csv_type, 'grp18')
        self.assertEqual(args.onefile, False)
#        self.assertEqual(args.overwrite, True)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'Pont-la-Borie_Q.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier PLATHYNES {d} de type '{t}' "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, d=args.data_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_plathynes_data_mqo(self):
        """
        Test PLATHYNES - Data - .mqo
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/plathynes2csv.py',
            '-O', self.dir_out,
            '-I', self.dir_in2,
            '-t', 'data',
            '-d', '8001_1.mqo',
            '-C', 'grp16',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.plathynes2csv()
        self.assertEqual(args.data_filename, '8001_1.mqo')
        self.assertEqual(args.input_dir, self.dir_in2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'data')
        self.assertEqual(args.csv_type, 'grp16')
        self.assertEqual(args.onefile, False)
#        self.assertEqual(args.overwrite, True)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'LaLoireChadrac_Q.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier PLATHYNES {d} de type '{t}' "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, d=args.data_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_plathynes_data_mgr(self):
        """
        Test PLATHYNES - Data - .mgr
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/plathynes2csv.py',
            '-O', self.dir_out,
            '-I', self.dir_in2,
            '-t', 'data',
            '-d', '8001_RRobs.mgr',
            '-C', 'grp18',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.plathynes2csv()
        self.assertEqual(args.data_filename, '8001_RRobs.mgr')
        self.assertEqual(args.input_dir, self.dir_in2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'data')
        self.assertEqual(args.csv_type, 'grp18')
        self.assertEqual(args.onefile, False)
#        self.assertEqual(args.overwrite, True)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for filename in os.listdir(self.dir_out):
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dir_ctl, filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier PLATHYNES {d} de type '{t}' "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, d=args.data_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_plathynes_results(self):
        """
        Test PLATHYNES - Results
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/plathynes2csv.py',
            '-O', self.dir_out,
            '-I', self.dir_in2,
            '-t', 'results',
            '-d', '8001_ResultsRaw.txt',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.plathynes2csv()
        self.assertEqual(args.data_filename, '8001_ResultsRaw.txt')
        self.assertEqual(args.input_dir, self.dir_in2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'results')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
#        self.assertEqual(args.overwrite, True)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'PLATHYNES-results_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier PLATHYNES {d} de type '{t}' "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, d=args.data_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================
