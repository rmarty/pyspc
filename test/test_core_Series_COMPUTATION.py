#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Series in pyspc.core.series

To run all tests just type:
    python -m unittest test_core_Series_COMPUTATION

To run only a class test:
    python -m unittest test_core_Series_COMPUTATION.TestSeries

To run only a specific test:
    python -m unittest test_core_Series_COMPUTATION.TestSeries.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import pandas as pnd
import unittest
from unittest import mock
import warnings

# Imports pyspc
from pyspc.core.serie import Serie
from pyspc.core.series import Series


class TestSeries(unittest.TestCase):
    """
    Series class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        # =====================================================================
        provider1 = 'SPC'
        varname1 = 'QH'
        code1 = 'K0000000'
        serie1 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500, 86.400, 82.300, 78.300],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie1.columns = [(code1, varname1)]
        self.serie1 = Serie(
            datval=serie1, code=code1, provider=provider1, varname=varname1,
        )
        # =====================================================================
        provider2 = 'SPC'
        varname2 = 'QH'
        code2 = 'K9999999'
        serie2 = pnd.DataFrame(
            [10, 12, 14, 17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie2.columns = [(code2, varname2)]
        self.serie2 = Serie(
            datval=serie2, code=code2, provider=provider2, varname=varname2
        )
        # =====================================================================
        provider3 = 'SPC'
        varname3 = 'QH'
        code3 = 'K3333333'
        serie3 = pnd.DataFrame(
            [17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 3), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie3.columns = [(code3, varname3)]
        self.serie3 = Serie(
            datval=serie3, code=code3, provider=provider3, varname=varname3
        )
        # =====================================================================
        provider4 = 'SPC'
        varname4 = 'QH'
        code4 = 'K4444444'
        serie4 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 9), freq=td(hours=1)
            )
        )
        serie4.columns = [(code4, varname4)]
        self.serie4 = Serie(
            datval=serie4, code=code4, provider=provider4, varname=varname4,
        )
        # =====================================================================
        self.model = 'PLATHYNES'
        self.series = Series(datatype='obs')
        self.series.add(serie=self.serie1)
        self.series.add(serie=self.serie2)
        self.series.add(serie=self.serie3)
        self.series.add(serie=self.serie4)
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.above_threshold')
    def test_ath(self, mock_method, mock_add):
        """
        Test méthode above_threshold
        """
        # =====================================================================
        valid_arg = {
            ('K0000000', 'QH', None): 1,
            ('K9999999', 'QH', None): 2,
            ('K3333333', 'QH', None): 3,
            ('K4444444', 'QH', None): 4,
        }
        # =====================================================================
        self.series.above_threshold(1, inplace=False)
        mock_method.assert_called_with(1, inplace=False)
        # =====================================================================
        self.series.above_threshold(valid_arg, inplace=False)
        for v in valid_arg.values():
            mock_method.assert_any_call(v, inplace=False)
        # =====================================================================
        self.series.above_threshold(1, inplace=True)
        mock_method.assert_called_with(1, inplace=True)
        # =====================================================================
        self.series.above_threshold(valid_arg, inplace=True)
        for v in valid_arg.values():
            mock_method.assert_any_call(v, inplace=True)
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.below_threshold')
    def test_bth(self, mock_method, mock_add):
        """
        Test méthode below_threshold
        """
        # =====================================================================
        valid_arg = {
            ('K0000000', 'QH', None): 1,
            ('K9999999', 'QH', None): 2,
            ('K3333333', 'QH', None): 3,
            ('K4444444', 'QH', None): 4,
        }
        # =====================================================================
        self.series.below_threshold(1, inplace=False)
        mock_method.assert_called_with(1, inplace=False)
        # =====================================================================
        self.series.below_threshold(valid_arg, inplace=False)
        for v in valid_arg.values():
            mock_method.assert_any_call(v, inplace=False)
        # =====================================================================
        self.series.below_threshold(1, inplace=True)
        mock_method.assert_called_with(1, inplace=True)
        # =====================================================================
        self.series.below_threshold(valid_arg, inplace=True)
        for v in valid_arg.values():
            mock_method.assert_any_call(v, inplace=True)
        # =====================================================================

    @mock.patch('pyspc.core.serie.Serie.counter_missing')
    def test_mv(self, mock_method):
        """
        Test méthode counter_missing
        """
        # =====================================================================
        x = self.series.counter_missing()
        mock_method.assert_called_with()
        self.assertIsInstance(x, dict)
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.cumsum')
    def test_csum(self, mock_method, mock_add):
        """
        Test méthode cumsum
        """
        # =====================================================================
        valid_arg = {
            ('K0000000', 'QH', None): True,
            ('K9999999', 'QH', None): False,
            ('K3333333', 'QH', None): True,
            ('K4444444', 'QH', None): False,
        }
        # =====================================================================
        self.series.cumsum(skipna=False)
        mock_method.assert_called_with(skipna=False)
        # =====================================================================
        self.series.cumsum(skipna=True)
        mock_method.assert_called_with(skipna=True)
        # =====================================================================
        self.series.cumsum(skipna=valid_arg)
        for v in valid_arg.values():
            mock_method.assert_any_call(skipna=v)
        # =====================================================================

    @mock.patch('pyspc.core.serie.Serie.events')
    def test_events(self, mock_method):
        """
        Test méthode events
        """
        # =====================================================================
        valid_arg = {
            ('K0000000', 'QH', None):
                (1, 'basic', None, None, None, None, None),
            ('K9999999', 'QH', None):
                (2, 'scipy', 10, 20, None, None, None),
            ('K3333333', 'QH', None):
                (3, 'scipy', 10, 20, 30, None, None),
            ('K4444444', 'QH', None):
                (4, 'scipy', 10, 20, 30, 40, None),
        }
        # =====================================================================
        x = self.series.events(threshold=1)
        mock_method.assert_called_with(
            threshold=1,
            engine='basic',
            prominence=None,
            width=None,
            before=None,
            after=None,
            filename=None
        )
        self.assertIsInstance(x, dict)
        mock_method.reset_mock()
        # =====================================================================
        for v in valid_arg.values():
            x = self.series.events(
                threshold=v[0],
                engine=v[1],
                prominence=v[2],
                width=v[3],
                before=v[4],
                after=v[5],
                filename=v[6]
            )
            mock_method.assert_any_call(
                threshold=v[0],
                engine=v[1],
                prominence=v[2],
                width=v[3],
                before=v[4],
                after=v[5],
                filename=v[6]
            )
            self.assertIsInstance(x, dict)
            mock_method.reset_mock()
        # =====================================================================

    @mock.patch('pyspc.core.serie.Serie.find_annual_max')
    def test_annual_max(self, mock_method):
        """
        Test méthode find_annual_max
        """
        # =====================================================================
        x = self.series.find_annual_max()
        mock_method.assert_called_with()
        self.assertIsInstance(x, dict)
        # =====================================================================

    @mock.patch('pyspc.core.serie.Serie.find_annual_min')
    def test_annual_min(self, mock_method):
        """
        Test méthode find_annual_min
        """
        # =====================================================================
        x = self.series.find_annual_min()
        mock_method.assert_called_with()
        self.assertIsInstance(x, dict)
        # =====================================================================

    @mock.patch('pyspc.core.serie.Serie.max')
    def test_max(self, mock_method):
        """
        Test méthode max
        """
        # =====================================================================
        x = self.series.max()
        mock_method.assert_called_with()
        self.assertIsInstance(x, dict)
        # =====================================================================

    @mock.patch('pyspc.core.serie.Serie.min')
    def test_min(self, mock_method):
        """
        Test méthode min
        """
        # =====================================================================
        x = self.series.min()
        mock_method.assert_called_with()
        self.assertIsInstance(x, dict)
        # =====================================================================

    @mock.patch('pyspc.plotting.regime.plot_regime')
    @mock.patch('pyspc.core.serie.Serie.regime')
    def test_regime(self, mock_method, mock_plot):
        """
        Test méthode régime
        """
        # =====================================================================
        m = mock.MagicMock()
        mock_method.return_value = m
        # =====================================================================
        x, y = self.series.regime()
        mock_method.assert_called_with(dayhour=6, freqs=None, groupby=None,
                                       ignore_upscale=False, strict=True)
        mock_plot.assert_called_with(boxplot=False, data=m, fill=False)
        self.assertIsInstance(x, dict)
        self.assertIsInstance(y, dict)
        # =====================================================================
        valid_meth = {
            'dayhour': 0, 'freqs': [0.10, 0.50, 0.90], 'groupby': 'month',
            'ignore_upscale': True, 'strict': True}
        valid_plot = {
            'boxplot': True, 'fill': True}
        x, y = self.series.regime(**valid_meth, **valid_plot)
        mock_method.assert_called_with(**valid_meth)
        mock_plot.assert_called_with(data=m, **valid_plot)
        self.assertIsInstance(x, dict)
        self.assertIsInstance(y, dict)
        # =====================================================================

    @mock.patch('pyspc.core.serie.Serie.timecentroid')
    def test_timecentroid(self, mock_method):
        """
        Test méthode timecentroid
        """
        # =====================================================================
        x = self.series.timecentroid()
        mock_method.assert_called_with()
        self.assertIsInstance(x, dict)
        # =====================================================================
