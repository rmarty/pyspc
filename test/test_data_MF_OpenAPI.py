#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for MF_OpenAPI in pyspc.data.meteofrance

To run all tests just type:
    python -m unittest test_MF_OpenAPI

To run only a class test:
    python -m unittest test_MF_OpenAPI.TestMF_OpenAPI

To run only a specific test:
    python -m unittest test_MF_OpenAPI.TestMF_OpenAPI.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.data.meteofrance import MF_OpenAPI


class TestMF_OpenAPI(unittest.TestCase):
    """
    MF_OpenAPI class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'mf', 'open_api')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = '43091005_Q_202410150000_202410190000.csv'
        reader = MF_OpenAPI(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.prefix, 'Q')
        self.assertEqual(reader.timestep, td(days=1))
        self.assertEqual(reader.code, '43091005')
        self.assertEqual(reader.start, dt(2024, 10, 15))
        self.assertEqual(reader.end, dt(2024, 10, 19))

    def test_MN(self):
        """
        Test de lecture/écriture de données 6-MINUTES
        """
        # ===================================================================
        valid = pnd.DataFrame({
            'POSTE': ['43091005']*30,
            'DATE': pnd.date_range(dt(2024, 10, 17, 5),
                                   dt(2024, 10, 17, 7, 54),
                                   freq=td(minutes=6)),
            'RR6': [0.6, 0.0, 0.0, 0.4, 1.3, 3.2, 4.2, 4.1, 5.2, 6.6, 1.4, 1.2,
                    0.2, 0.0, 1.8, 11.5, 4.5, 1.6, 0.7, 1.6, 1.0, 0.8, 0.8,
                    0.4, 3.5, 4.6, 3.7, 1.8, 6.3, 3.3],
        })
        # ===================================================================
        filename = os.path.join(self.dirname,
                                '43091005_MN_202410170500_202410170700.csv')
        reader = MF_OpenAPI(filename=filename)
        content = reader.read()
        assert_frame_equal(content, valid)
        # ===================================================================

    def test_H(self):
        """
        Test de lecture/écriture de données HORAIRE
        """
        # ===================================================================
        valid = pnd.DataFrame({
            'POSTE': ['43091005']*13,
            'DATE': pnd.date_range(dt(2024, 10, 17), dt(2024, 10, 17, 12),
                                   freq='H'),
            'RR1': [9.3, 13.2, 5.6, 14.3, 8.3, 13.2, 26.4, 24.1, 26.0, 7.3,
                    6.3, 5.3, 3.0],
            'T': [12.6, 12.2, 12.2, 11.9, 12.5, 12.5, 12.3, 12.8, 9.8, 12.7,
                  12.9, 12.7, 12.7],
            'TN': [12.5, 12.2, 12.0, 11.5, 11.9, 12.3, 12.3, 11.5, 9.8, 9.8,
                   12.7, 12.6, 12.6],
            'HTN': [2301, 39, 103, 228, 301, 411, 600, 632, 757, 801, 901,
                    1036, 1106],
            'TX': [12.6, 12.6, 12.4, 12.3, 12.5, 12.6, 12.7, 12.9, 12.8, 12.7,
                   12.9, 12.8, 12.8],
            'HTX': [2302, 1, 133, 201, 337, 445, 535, 652, 701, 854, 959, 1001,
                    1120],
            'DG': [0]*13,
        })
        # ===================================================================
        filename = os.path.join(self.dirname,
                                '43091005_H_202410170000_202410171200.csv')
        reader = MF_OpenAPI(filename=filename)
        content = reader.read()
        assert_frame_equal(content, valid)
        # ===================================================================

    def test_Q(self):
        """
        Test de lecture/écriture de données QUOTIDIEN
        """
        # ===================================================================
        valid = pnd.DataFrame({
            'POSTE': ['43091005']*5,
            'DATE': pnd.date_range(dt(2024, 10, 15), dt(2024, 10, 19),
                                   freq='D'),
            'RR': [38.6, 189.0, 130.4, 2.4, 0.0],
            'TN': [12.4, 11.5, 9.8, 5.3, 7.2],
            'HTN': [1855, 217, 757, 644, 618],
            'TX': [18.0, 13.3, 13.0, 10.7, 13.6],
            'HTX': [1156, 1411, 1302, 1207, 1416],
            'TM': [14.1, 12.4, 11.5, 8.2, 9.0],
            'TMNX': [15.2, 12.4, 11.4, 8.0, 10.4],
            'DG': [0]*5,
            'TAMPLI': [5.6, 1.8, 3.2, 5.4, 6.4],
            'TNTXM': [15.2, 12.4, 11.4, 8.0, 10.4],
            'ETPGRILLE': [1.9, 0.1, 0.1, 0.4, 1.1],
        })
        # ===================================================================
        filename = os.path.join(self.dirname,
                                '43091005_Q_202410150000_202410190000.csv')
        reader = MF_OpenAPI(filename=filename)
        content = reader.read()
        assert_frame_equal(content, valid)
        # ===================================================================
