#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Location in pyspc.core.location

To run all tests just type:
    python -m unittest test_core_Location

To run only a class test:
    python -m unittest test_core_Location.TestLocation

To run only a specific test:
    python -m unittest test_core_Location.TestLocation.test_init

"""
# Imports
import unittest

# Imports pyspc
from pyspc.core.location import Location


class TestLocation(unittest.TestCase):
    """
    Location class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """

    def test_init(self):
        """
        Test de la création de l'instance
        """
        valid = {
            'code': 'K0550010',
            'name': 'La Loire à Bas-en-Basset',
            'loctype': 'basin',
            'longname': None,
            'reach': None,
            'locality': None,
            'x': 787596.0,
            'y': 6466797.0,
            'z': 442.89,
            'area': 3234.0,
            'river': 'Loire'
        }
        loc = Location(**valid)
        valid = {'_{}'.format(k): v for k, v in valid.items()}
        self.assertEqual(valid, vars(loc))

    def test_init_error(self):
        """
        Test de la création de l'instance - CAS avec ERREUR
        """
        valid = {
            'code': 'K0550010',
            'name': 'La Loire à Bas-en-Basset',
            'x': 787596.0,
            'y': 6466797.0,
            'z': 442.89,
            'area': 3234.0
        }
        with self.assertRaises(ValueError):
            Location(**valid)
        with self.assertRaises(ValueError):
            Location(**valid, loctype=1)
        with self.assertRaises(ValueError):
            Location(**valid, loctype='toto')

    def test_loctypes(self):
        """
        Test des types de lieu
        """
        valid = ['basin', 'point']
        self.assertEqual(valid, Location.get_loctypes())
