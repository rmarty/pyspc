#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary grpVerif

To run all tests just type:
    python -m unittest test_bin_grpVerif

To run only a class test:
    python -m unittest test_bin_grpVerif.Test_grpVerif

To run only a specific test:
    python -m unittest test_bin_grpVerif.Test_grpVerif.test_cal

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import grpVerif as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_grpVerif(unittest.TestCase):
    """
    grpVerif bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'grpVerif')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_2016 = os.path.join('data', 'model', 'grp16', 'cal')
        cls.dir_2018 = os.path.join('data', 'model', 'grp18', 'cal')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_cal_2016(self):
        """
        Test fiche perf GRP - Calage-Contrôle
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpVerif.py',
            '-I', self.dir_2016,
            '-c', os.path.join(self.dir_out, 'K0403010_cal.csv'),
            '-s', 'K0403010',
            '-t', 'grp16_cal'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.grpVerif()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'K0403010_cal.csv'))
        self.assertEqual(args.input_dir, self.dir_2016)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, 'K0403010')
        self.assertEqual(args.data_type, 'grp16_cal')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les valeurs des critères de performance des "\
            "fiches de calage-contrôle de GRP v2016 ('{t}') situées dans le "\
            "répertoie {I} pour la station {s}. Les valeurs sont exportées "\
            "dans le fichier {c}."\
            "".format(
                t=args.data_type, s=args.station_name, I=args.input_dir,
                c=args.output_filename
                )
        # =====================================================================

    def test_cal_2018(self):
        """
        Test fiche perf GRP - Calage-Contrôle
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpVerif.py',
            '-I', self.dir_2018,
            '-c', os.path.join(self.dir_out, 'RH10585x_cal.csv'),
            '-s', 'RH10585x',
            '-t', 'grp18_cal'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.grpVerif()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'RH10585x_cal.csv'))
        self.assertEqual(args.input_dir, self.dir_2018)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, 'RH10585x')
        self.assertEqual(args.data_type, 'grp18_cal')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les valeurs des critères de performance des "\
            "fiches de calage-contrôle de GRP v2016 ('{t}') situées dans le "\
            "répertoie {I} pour la station {s}. Les valeurs sont exportées "\
            "dans le fichier {c}."\
            "".format(
                t=args.data_type, s=args.station_name, I=args.input_dir,
                c=args.output_filename
                )
        # =====================================================================

    def test_rtime_2016(self):
        """
        Test fiche perf GRP - Calage complet
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpVerif.py',
            '-I', self.dir_2016,
            '-c', os.path.join(self.dir_out, 'K0403010_rtime.csv'),
            '-s', 'K0403010',
            '-t', 'grp16_rtime'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.grpVerif()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'K0403010_rtime.csv'))
        self.assertEqual(args.input_dir, self.dir_2016)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, 'K0403010')
        self.assertEqual(args.data_type, 'grp16_rtime')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les valeurs des critères de performance des "\
            "fiches de calage complet de GRP v2016 ('{t}') situées dans le "\
            "répertoie {I} pour la station {s}. Les valeurs sont exportées "\
            "dans le fichier {c}."\
            "".format(
                t=args.data_type, s=args.station_name, I=args.input_dir,
                c=args.output_filename
                )
        # =====================================================================

    def test_rtime_2018(self):
        """
        Test fiche perf GRP - Calage complet
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpVerif.py',
            '-I', self.dir_2018,
            '-c', os.path.join(self.dir_out, 'RH10585x_rtime.csv'),
            '-s', 'RH10585x',
            '-t', 'grp18_rtime'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.grpVerif()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'RH10585x_rtime.csv'))
        self.assertEqual(args.input_dir, self.dir_2018)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, 'RH10585x')
        self.assertEqual(args.data_type, 'grp18_rtime')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les valeurs des critères de performance des "\
            "fiches de calage complet de GRP v2018 ('{t}') situées dans le "\
            "répertoie {I} pour la station {s}. Les valeurs sont exportées "\
            "dans le fichier {c}."\
            "".format(
                t=args.data_type, s=args.station_name, I=args.input_dir,
                c=args.output_filename
                )
        # =====================================================================
