#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_Vigicrues in pyspc.io.vigicrues

To run all tests just type:
    python -m unittest test_io_Vigicrues

To run only a class test:
    python -m unittest test_io_Vigicrues.TestVigicrues

To run only a specific test:
    python -m unittest test_io_Vigicrues.TestVigicrues.test_init

"""
# Imports
from datetime import datetime as dt
import os.path
import unittest

from pyspc.core.location import Locations
from pyspc.core.reach import Reaches
from pyspc.core.series import Series
from pyspc.io.vigicrues import read_Vigicrues


class TestVigicrues(unittest.TestCase):
    """
    Vigicrues_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname1 = os.path.join('data', 'data', 'vigicrues')
        self.dirname2 = os.path.join('data', 'metadata', 'vigicrues')

    def test_fcst(self):
        """
        Test Vigicrues Json Fcst
        """
        # =====================================================================
        datatype = 'vigicrues_fcst'
        filename = os.path.join(self.dirname1, 'K118001010_prevision.json')
        series = read_Vigicrues(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 3)
        k = ('K118001010', 'QI',
             (dt(2022, 3, 29, 12, 6, 52), 'Vigicrues', 'Released', '50'))
        self.assertIn(k, series)
        # =====================================================================

    def test_obs(self):
        """
        Test Vigicrues Json Data Obs
        """
        # =====================================================================
        datatype = 'vigicrues_obs'
        filename = os.path.join(self.dirname1, 'K118001010_observation.json')
        series = read_Vigicrues(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('K118001010', 'QI', None), series)
        # =====================================================================

    def test_loc(self):
        """
        Test Vigicrues Json Location
        """
        # =====================================================================
        datatype = 'vigicrues_loc'
        filename = os.path.join(self.dirname2, 'K118001010_station.json')
        locs = read_Vigicrues(filename=filename, datatype=datatype)
        self.assertIsInstance(locs, Locations)
        self.assertEqual(len(locs), 1)
        self.assertIn('K118001010', locs)
        # =====================================================================

    def test_reach(self):
        """
        Test Vigicrues Json Reach
        """
        # =====================================================================
        datatype = 'vigicrues_reach'
        filename = os.path.join(self.dirname2, 'troncons.json')
        reaches = read_Vigicrues(filename=filename, datatype=datatype)
        self.assertIsInstance(reaches, Reaches)
        self.assertEqual(len(reaches), 2)
        self.assertIn('LC110', reaches)
        self.assertIn('LC120', reaches)
        # =====================================================================
