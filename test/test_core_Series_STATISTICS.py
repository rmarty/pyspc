#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Series in pyspc.core.series

To run all tests just type:
    python -m unittest test_core_Series_STATISTICS

To run only a class test:
    python -m unittest test_core_Series_STATISTICS.TestSeries

To run only a specific test:
    python -m unittest test_core_Series_STATISTICS.TestSeries.test_describe

"""
# Imports
from datetime import datetime as dt, timedelta as td
import numpy as np
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest
from unittest import mock
import warnings

# Imports pyspc
from pyspc.core.config import Config
from pyspc.core.keyseries import str2tuple
from pyspc.core.serie import Serie
from pyspc.core.series import Series


class TestSeries(unittest.TestCase):
    """
    Series class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        # =====================================================================
        provider1 = 'SPC'
        varname1 = 'QH'
        code1 = 'K0000000'
        serie1 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500, 86.400, 82.300, 78.300],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie1.columns = [(code1, varname1)]
        self.serie1 = Serie(
            datval=serie1, code=code1, provider=provider1, varname=varname1,
        )
        # =====================================================================
        provider2 = 'SPC'
        varname2 = 'QH'
        code2 = 'K9999999'
        serie2 = pnd.DataFrame(
            [10, 12, 14, 17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie2.columns = [(code2, varname2)]
        self.serie2 = Serie(
            datval=serie2, code=code2, provider=provider2, varname=varname2
        )
        # =====================================================================
        provider3 = 'SPC'
        varname3 = 'QH'
        code3 = 'K3333333'
        serie3 = pnd.DataFrame(
            [17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 3), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie3.columns = [(code3, varname3)]
        self.serie3 = Serie(
            datval=serie3, code=code3, provider=provider3, varname=varname3
        )
        # =====================================================================
        provider4 = 'SPC'
        varname4 = 'QH'
        code4 = 'K4444444'
        serie4 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 9), freq=td(hours=1)
            )
        )
        serie4.columns = [(code4, varname4)]
        self.serie4 = Serie(
            datval=serie4, code=code4, provider=provider4, varname=varname4,
        )
        # =====================================================================
        self.series = Series(datatype='obs')
        self.series.add(serie=self.serie1)
        self.series.add(serie=self.serie2)
        self.series.add(serie=self.serie3)
        self.series.add(serie=self.serie4)
        # =====================================================================
        # PREVISION QH
        # ---------------------------------------------------------------------
        fcst_15 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500, 86.400, 82.300, 78.300],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        fcst_11 = fcst_15 * 0.9
        fcst_19 = fcst_15 * 1.1
        self.fcst_11 = Serie(datval=fcst_11, code='K0000000', varname='QH')
        self.fcst_15 = Serie(datval=fcst_15, code='K0000000', varname='QH')
        self.fcst_19 = Serie(datval=fcst_19, code='K0000000', varname='QH')
        fcst_25 = pnd.DataFrame(
            [10, 12, 14, 17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        self.fcst_25 = Serie(datval=fcst_25, code='K0000000', varname='QH')
        fcst_35 = pnd.DataFrame(
            [17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 3), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        self.fcst_35 = Serie(datval=fcst_35, code='K0000000', varname='QH')
        # =====================================================================

    @mock.patch('pyspc.core.serie.Serie.describe')
    def test_describe(self, mock_method):
        """
        Test méthode describe
        """
        # =====================================================================
        freqs = [0.10, 0.50, 0.90]
        x = self.series.describe(freqs=freqs)
        mock_method.assert_called_with(freqs=freqs)
        self.assertIsInstance(x, dict)
        # =====================================================================

    def test_describe_asconfig(self):
        """
        Test méthode describe - Cas avec sortie sous forme de Config
        """
        # =====================================================================
        valid = {
            ('K0000000', 'QH', None): {
                'count': 13.0,
                'mean': 81.06153846153846,
                'std': 27.32832896652418,
                'min': 35.6,
                '10%': 42.34,
                '50%': 85.7,
                '90%': 113.20000000000002,
                'max': 118.0,
            },
            ('K9999999', 'QH', None): {
                'count': 13.0,
                'mean': 26.923076923076923,
                'std': 11.397818054796996,
                'min': 10.0,
                '10%': 12.4,
                '50%': 28.0,
                '90%': 40.599999999999994,
                'max': 42.0,
            },
            ('K3333333', 'QH', None): {
                'count': 10.0,
                'mean': 31.4,
                'std': 8.70759566253637,
                'min': 17.0,
                '10%': 20.6,
                '50%': 32.5,
                '90%': 41.099999999999994,
                'max': 42.0,
            },
            ('K4444444', 'QH', None): {
                'count': 10.0,
                'mean': 80.67999999999999,
                'std': 31.487097886806485,
                'min': 35.6,
                '10%': 40.64000000000001,
                '50%': 89.1,
                '90%': 115.3,
                'max': 118.0,
            },
        }
        # =====================================================================
        freqs = [10, 50, 90]
        dsc = self.series.describe(freqs=freqs, asconfig=True)
#        for k, v in dsc.items():
#            print("{}: {}".format(k, '{'))
#            for k2, v2 in v.items():
#                print("    '{}': {}{}{}".format(k2, '{', v2, '},'))
#            print('},')
        self.assertIsInstance(dsc, Config)
        for s in valid:
            self.assertIn(s, dsc)
            for o, v in valid[s].items():
                self.assertIn(o, dsc[s])
                self.assertAlmostEqual(dsc[s][o], v, places=4)
        # =====================================================================

    def test_describe_byrow(self):
        """
        Test méthode describe - Cas avec statistiques par ligne
        """
        # =====================================================================
        valid = {
            ('series_count', 'QH', None): pnd.DataFrame(
                {('series_count', 'QH'): [
                    3.0, 3.0, 3.0, 4.0, 4.0, 4.0, 4.0,
                    4.0, 4.0, 4.0, 3.0, 3.0, 3.0]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                )
            ),
            ('series_mean', 'QH', None): pnd.DataFrame(
                {('series_mean', 'QH'): [
                    27.066667, 31.466667, 35.933333, 39.950000, 53.350000,
                    65.000000, 73.000000, 73.000000, 68.500000, 64.750000,
                    54.800000, 54.766667, 54.100000]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                )
            ),
            ('series_std', 'QH', None): pnd.DataFrame(
                {('series_std', 'QH'): [
                    14.780167, 16.858628, 18.994824, 26.500377, 37.354562,
                    47.342722, 51.961524, 48.497423, 39.837169, 32.042940,
                    27.366403, 23.844566, 20.957815]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                )
            ),
            ('series_min', 'QH', None): pnd.DataFrame(
                {('series_min', 'QH'): [
                    10.0, 12.0, 14.0, 17.0, 21.0, 24.0, 28.0, 31.0, 34.0,
                    37.0, 39.0, 41.0, 42.0]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                )
            ),
            ('series_10%', 'QH', None): pnd.DataFrame(
                {('series_10%', 'QH'): [
                    15.12, 17.84, 20.58, 17.00, 21.00, 24.00, 28.00, 31.00,
                    34.00, 37.00, 39.00, 41.00, 42.00]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                )
            ),
            ('series_50%', 'QH', None): pnd.DataFrame(
                {('series_50%', 'QH'): [
                    35.60, 41.20, 46.90, 39.95, 53.35, 65.00, 73.00, 73.00,
                    68.50, 64.75, 39.00, 41.00, 42.00]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                )
            ),
            ('series_90%', 'QH', None): pnd.DataFrame(
                {('series_90%', 'QH'): [
                    35.60, 41.20, 46.90, 62.90, 85.70, 106.00, 118.00, 115.00,
                    103.00, 92.50, 76.92, 74.04, 71.04]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                )
            ),
            ('series_max', 'QH', None): pnd.DataFrame(
                {('series_max', 'QH'): [
                    35.6, 41.2, 46.9, 62.9, 85.7, 106.0, 118.0, 115.0, 103.0,
                    92.5, 86.4, 82.3, 78.3]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                )
            ),
        }
        # =====================================================================
        freqs = [10, 50, 90]
        dsc = self.series.describe(freqs=freqs, bydate=True)
        for k, v in valid.items():
            self.assertIn(k, dsc)
            v.columns = v.columns.to_flat_index()
            assert_frame_equal(dsc[k].data_frame, v)
        # =====================================================================

    def test_errors_fcst(self):
        """
        Test du calcul des erreurs de prévision
        """
        # =====================================================================
        code = 'K1440010'
        varname = 'QH'
        dict_fcst = {
            'K1440010_2020030207_pilote_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    100.400, 99.900, 99.600, 99.400, 99.400, 99.600, 100.000,
                    100.500, 101.100, 101.800, 102.600, 103.600]},
                index=pnd.date_range(dt(2020, 3, 2, 8),
                                     dt(2020, 3, 2, 19), freq=td(hours=1))),
            'K1440010_2020030306_pilote_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    140.200, 142.900, 145.500, 148.400, 151.700, 155.300,
                    159.000, 162.300, 165.700, 169.400, 173.800, 179.300]},
                index=pnd.date_range(dt(2020, 3, 3, 7),
                                     dt(2020, 3, 3, 18), freq=td(hours=1))),
            'K1440010_2020030407_pilote_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    241.600, 240.300, 239.000, 237.500, 235.800, 234.100,
                    232.200, 230.400, 228.500, 226.700, 224.900, 223.300]},
                index=pnd.date_range(dt(2020, 3, 4, 8),
                                     dt(2020, 3, 4, 19), freq=td(hours=1))),
            'K1440010_2020030507_pilote_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    208.600, 206.700, 205.100, 206.000, 208.500, 212.200,
                    216.200, 218.800, 221.600, 224.500, 227.800, 231.400]},
                index=pnd.date_range(dt(2020, 3, 5, 8),
                                     dt(2020, 3, 5, 19), freq=td(hours=1))),
            'K1440010_2020030513_pilote_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    196.300, 196.500, 197.400, 198.900, 201.200, 204.600,
                    209.600, 215.900, 223.200, 230.800, 238.300, 245.300]},
                index=pnd.date_range(dt(2020, 3, 5, 14),
                                     dt(2020, 3, 6, 1), freq=td(hours=1))),
            'K1440010_2020030607_expresso_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    281.300, 290.265, 300.366, 310.306, 319.092, 326.128,
                    331.820, 336.873, 341.392, 345.983, 350.750, 355.598]},
                index=pnd.date_range(dt(2020, 3, 6, 8),
                                     dt(2020, 3, 6, 19), freq=td(hours=1))),
            'K1440010_2020030612_expresso_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    332.079, 338.988, 345.479, 351.479, 356.962, 362.103,
                    367.122, 372.224, 377.538, 383.173, 389.193, 395.466]},
                index=pnd.date_range(dt(2020, 3, 6, 13),
                                     dt(2020, 3, 7, 0), freq=td(hours=1))),
            'K1440010_2020030707_pilote_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    421.100, 424.300, 427.100, 429.400, 430.800, 430.800,
                    429.400, 426.400, 422.500, 418.100, 413.700, 409.500]},
                index=pnd.date_range(dt(2020, 3, 7, 8),
                                     dt(2020, 3, 7, 19), freq=td(hours=1))),
            'K1440010_2020030807_expresso_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    346.100, 339.685, 335.228, 331.913, 328.721, 324.633,
                    319.030, 313.066, 306.736, 300.633, 295.038, 289.943]},
                index=pnd.date_range(dt(2020, 3, 8, 8),
                                     dt(2020, 3, 8, 19), freq=td(hours=1))),
            'K1440010_2020030908_expresso_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    234.885, 233.853, 233.579, 233.604, 233.272, 231.873,
                    229.375, 226.379, 223.484, 221.091, 219.400, 218.509]},
                index=pnd.date_range(dt(2020, 3, 9, 9),
                                     dt(2020, 3, 9, 20), freq=td(hours=1))),
            'K1440010_2020031008_pilote_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    214.600, 217.300, 220.000, 221.400, 221.400, 220.400,
                    218.600, 216.600, 214.400, 212.000, 209.800, 207.900]},
                index=pnd.date_range(dt(2020, 3, 10, 9),
                                     dt(2020, 3, 10, 20), freq=td(hours=1))),
            'K1440010_2020031106_expresso_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    191.186, 188.995, 191.308, 194.908, 197.869, 198.262,
                    196.102, 192.830, 189.307, 186.196, 183.860, 182.462]},
                index=pnd.date_range(dt(2020, 3, 11, 7),
                                     dt(2020, 3, 11, 18), freq=td(hours=1))),
            'K1440010_2020031206_expresso_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    183.000, 181.100, 181.500, 183.100, 184.700, 185.200,
                    183.900, 181.400, 178.000, 174.100, 170.300, 167.000]},
                index=pnd.date_range(dt(2020, 3, 12, 7),
                                     dt(2020, 3, 12, 18), freq=td(hours=1))),
            'K1440010_2020031308_pilote_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    153.700, 154.800, 156.900, 158.900, 159.900, 159.400,
                    # 157.700, 155.800, np.nan, np.nan, np.nan, np.nan]},
                    157.700, 155.800, 154.200, 153.100, 152.600, 152.700]},
                index=pnd.date_range(dt(2020, 3, 13, 9),
                                     dt(2020, 3, 13, 20), freq=td(hours=1))),
            'K1440010_2020031406_expresso_5210_50_QH':  pnd.DataFrame({
                'fcst': [
                    142.000, 138.600, 136.895, 137.037, 138.100, 139.008,
                    # 138.795, np.nan, np.nan, np.nan, np.nan, np.nan]},
                    138.795, 137.277, 134.806, 132.143, 129.752, 127.798]},
                index=pnd.date_range(dt(2020, 3, 14, 7),
                                     dt(2020, 3, 14, 18), freq=td(hours=1))),
            'K1440010_2020031412_expresso_5000_50_QH':  pnd.DataFrame({
                'fcst': [
                    146.554, 148.087, 147.913, 144.861, 142.328, 140.452,
                    # np.nan, np.nan, np.nan, np.nan, np.nan, np.nan]},
                    139.450, 138.922, 138.869, 139.504, 140.752, 142.236]},
                index=pnd.date_range(dt(2020, 3, 14, 13),
                                     dt(2020, 3, 15, 0), freq=td(hours=1))),
        }
        obs = pnd.DataFrame({
            'obs': [159.601, 157.160, 153.618, 149.187, 146.077, 142.538,
                    140.338, 138.807, 137.718, 136.416, 136.849, 136.199,
                    135.983, 136.199, 135.983, 135.335, 133.399, 130.625,
                    127.877, 124.530, 121.428, 118.768, 115.932, 113.326,
                    111.340, 108.976, 106.830, 105.280, 103.549, 102.402,
                    101.641, 100.693, 100.314, 100.126, 100.126, 100.126,
                    100.314, 100.503, 101.261, 101.831, 102.593, 104.125,
                    105.860, 107.608, 109.172, 110.944, 112.530, 114.726,
                    116.537, 118.564, 120.812, 123.700, 127.877, 132.757,
                    137.283, 140.996, 143.423, 146.077, 147.903, 150.106,
                    152.876, 156.412, 161.488, 165.668, 169.118, 171.046,
                    173.176, 175.121, 181.991, 194.199, 208.310, 220.124,
                    229.969, 236.621, 241.993, 244.922, 246.278, 246.278,
                    244.245, 243.343, 241.095, 239.302, 238.407, 236.621,
                    235.953, 234.840, 233.952, 232.622, 231.073, 229.749,
                    228.207, 226.012, 224.917, 223.170, 221.645, 219.907,
                    217.958, 216.662, 214.939, 213.650, 212.578, 211.722,
                    210.867, 209.800, 208.522, 206.400, 203.441, 200.719,
                    198.469, 197.042, 197.449, 198.469, 200.924, 204.285,
                    203.652, 204.496, 206.612, 211.936, 221.210, 230.852,
                    239.749, 246.957, 253.786, 259.299, 263.462, 266.717,
                    269.519, 274.213, 280.360, 290.396, 301.294, 312.094,
                    321.034, 329.385, 335.047, 339.828, 343.121, 345.819,
                    347.320, 350.023, 353.935, 359.363, 366.622, 375.120,
                    383.652, 392.521, 400.501, 406.042, 410.051, 413.449,
                    416.232, 418.089, 420.258, 422.739, 426.464, 429.262,
                    431.751, 432.063, 432.374, 428.018, 422.739, 415.613,
                    408.817, 403.270, 397.736, 392.827, 387.012, 383.041,
                    379.077, 377.859, 378.164, 378.164, 376.033, 371.778,
                    361.478, 353.935, 343.720, 338.930, 334.450, 332.363,
                    329.087, 322.553, 312.835, 306.186, 297.160, 287.276,
                    280.360, 273.272, 267.183, 261.840, 255.848, 254.015,
                    251.502, 251.958, 254.244, 256.537, 256.996, 254.931,
                    251.502, 245.826, 239.749, 235.953, 234.840, 235.730,
                    237.290, 237.737, 235.063, 231.073, 225.574, 219.907,
                    215.154, 211.294, 208.948, 207.248, 208.310, 211.508,
                    216.447, 221.428, 225.355, 226.889, 226.450, 223.388,
                    218.607, 214.939, 213.435, 215.585, 218.607, 221.863,
                    223.606, 223.825, 221.863, 217.958, 212.364, 206.612,
                    201.540, 198.061, 196.228, 196.838, 200.924, 205.341,
                    209.587, 212.578, 212.578, 210.867, 207.036, 202.156,
                    197.449, 193.996, 191.572, 191.170, 193.389, 197.449,
                    200.309, 201.130, 199.491, 195.821, 191.774, 188.158,
                    184.966, 182.783, 181.991, 183.377, 186.959, 192.379,
                    197.042, 200.309, 201.540, 201.130, 198.061, 192.379,
                    187.958, 182.981, 179.821, 179.625, 182.783, 186.559,
                    189.963, 191.170, 189.963, 186.759, 181.794, 176.487,
                    171.432, 167.390, 164.524, 162.056, 161.299, 162.435,
                    165.096, 168.157, 169.888, 169.310, 166.815, 163.573,
                    159.601, 156.225, 153.989, 152.506, 152.876, 155.852,
                    158.660, 160.732, 160.355, 157.910, 154.547, 151.027,
                    147.903, 145.895, 144.309, 143.201, 142.980, 145.349,
                    149.554, 153.804, 156.412, 156.412, 154.175, 150.290,
                    146.259, 141.877, 138.807, 137.066, 137.500, 140.777,
                    145.349, 148.453, 149.187, 147.355, 144.087, 139.900,
                    135.551, 132.543, 130.413, 128.509, 127.667, 128.298,
                    131.689, 136.849, 141.436, 143.644, 142.759, 139.681,
                    135.335, 130.838, 127.457, 124.946, 123.285, 122.458,
                    121.634, 121.428, 120.812, 119.993, 119.584, 119.176,
                    118.768, 118.158, 117.549, 116.942, 116.537, 116.134,
                    115.530, 115.128, 114.526, 113.925, 112.729, 111.142,
                    109.369, 106.830, 104.317, 102.212, 100.126, 98.246,
                    97.125, 96.567, 97.311, 99.560, 102.212, 104.317, 105.473,
                    105.087, 104.125, 102.402, 99.937, 97.685, 95.640, 93.982,
                    93.432, 94.349, 95.825, 97.498, 98.620, 99.372, 99.937,
                    101.831, 105.860, 112.928, 120.197, 126.617, 130.201,
                    131.689, 131.050, 128.931, 125.572, 122.458, 119.380,
                    116.739, 114.726, 113.127, 111.142, 108.780, 106.054,
                    103.166, 100.503, 97.871, 96.753, 99.937, 107.608,
                    118.158, 124.946, 131.263, 134.473, 136.199, 135.551,
                    132.757, 128.298, 122.458, 115.731, 109.369, 104.317,
                    100.314, 98.246, 98.808, 102.593, 108.194, 114.125,
                    118.361, 120.197, 119.584, 117.144, 113.326, 108.194,
                    103.741, 100.314, 98.433, 98.058, 98.996, 99.372, 98.433,
                    97.498, 95.640, 93.615, 91.790, 90.342, 89.083, 88.367,
                    88.011, 88.725]},
            index=pnd.date_range(
                dt(2020, 3, 1,), dt(2020, 3, 20), freq=td(hours=1))
        )
        obs = Serie(datval=obs, code=code, varname=varname)
        fcst = Series(datatype='fcst')
        for k, df, in dict_fcst.items():
            key = str2tuple(k)
            s = Serie(datval=df, code=code, varname=varname)
            fcst.add(serie=s, meta=key[2])
        # =====================================================================
        valid_sample = pnd.DataFrame({
            'K1440010_2020030207_pilote_5210_50_QH': [
                0.086, -0.226, -0.526, -0.726, -0.914, -0.903, -1.261, -1.331,
                -1.493, -2.325, -3.260, -4.008],
            'K1440010_2020030306_pilote_5210_50_QH': [
                -0.796, -0.523, -0.577, 0.497, 1.594, 2.424, 2.588, 0.812,
                0.032, 0.282, 2.754, 6.124],
            'K1440010_2020030407_pilote_5210_50_QH': [
                0.505, 0.998, 0.593, 0.879, -0.153, -0.740, -1.752, -2.222,
                -2.573, -3.049, -3.307, -2.712],
            'K1440010_2020030507_pilote_5210_50_QH': [
                0.078, 0.300, 1.659, 5.281, 10.031, 15.158, 18.751, 20.331,
                20.676, 20.215, 24.148, 26.904],
            'K1440010_2020030513_pilote_5210_50_QH': [
                -1.149, -1.969, -3.524, -5.385, -2.452, 0.104, 2.988, 3.964,
                1.990, -0.052, -1.449, -1.657],
            'K1440010_2020030607_expresso_5210_50_QH': [
                0.940, -0.131, -0.928, -1.788, -1.942, -3.257, -3.227, -2.955,
                -1.729, 0.164, 3.430, 5.575],
            'K1440010_2020030612_expresso_5210_50_QH': [
                2.694, 3.941, 5.651, 8.358, 11.143, 14.783, 17.099, 18.289,
                18.175, 16.551, 14.073, 11.814],
            'K1440010_2020030707_pilote_5210_50_QH': [
                0.842, 1.561, 0.636, 0.138, -0.951, -1.263, -2.974, -1.618,
                -0.239, 2.487, 4.883, 6.230],
            'K1440010_2020030807_expresso_5210_50_QH': [
                2.380, 0.755, 0.778, -0.450, -0.366, 2.080, 6.195, 6.880,
                9.576, 13.357, 14.678, 16.671],
            'K1440010_2020030908_expresso_5210_50_QH': [
                -1.068, -0.987, -2.151, -3.686, -4.465, -3.190, -1.698, 0.805,
                3.577, 5.937, 8.106, 9.561],
            'K1440010_2020031008_pilote_5210_50_QH': [
                -0.985, -1.307, -1.863, -2.206, -2.425, -1.463, 0.642, 4.236,
                7.788, 10.460, 11.739, 11.672],
            'K1440010_2020031106_expresso_5210_50_QH': [
                -2.810, -2.577, 0.138, 1.519, 0.420, -2.047, -5.028, -6.661,
                -6.514, -5.578, -4.298, -2.504],
            'K1440010_2020031206_expresso_5210_50_QH': [
                0.019, 1.279, 1.875, 0.317, -1.859, -4.763, -7.270, -8.563,
                -8.759, -7.694, -6.187, -4.432],
            'K1440010_2020031308_pilote_5210_50_QH': [
                1.194, 1.924, 1.048, 0.240, -0.832, -0.955, -0.210, 1.253,
                3.173, 5.197, 6.705, 8.391],
            'K1440010_2020031406_expresso_5210_50_QH': [
                0.123, -0.207, -0.171, -0.463, -2.677, -6.341, -9.658,
                -11.910, -12.549, -11.944, -10.148, -7.753],
            'K1440010_2020031412_expresso_5000_50_QH': [
                -1.899, -1.100, 0.558, 0.774, 2.428, 4.901, 6.907, 8.509,
                10.360, 11.837, 12.454, 10.547],
            },
            index=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        )
        valid_sample.index.name = 'leadtime'
        valid_stats = pnd.DataFrame({
            '10%': [-1.524, -1.638, -2.007, -2.946, -2.564, -4.010,
                    -6.149, -7.612, -7.636, -6.636, -5.242, -4.220],
            '50%': [0.082, -0.169, 0.348, 0.189, -0.873, -0.929, -0.736,
                    0.808, 1.011, 1.385, 4.156, 6.177],
            '90%': [1.787, 1.743, 1.767, 3.400, 6.230, 9.842, 12.003,
                    13.399, 14.268, 14.954, 14.375, 14.243],
            'count': [16.000, 16.000, 16.000, 16.000, 16.000, 16.000,
                      16.000, 16.000, 16.000, 16.000, 16.000, 16.000],
            'count+': [10.000, 7.000, 9.000, 9.000, 5.000, 6.000, 7.000,
                       9.000, 9.000, 10.000, 10.000, 10.000],
            'count-': [6.000, 9.000, 7.000, 7.000, 11.000, 10.000, 9.000,
                       7.000, 7.000, 6.000, 6.000, 6.000],
            'f+': [0.625, 0.438, 0.562, 0.562, 0.312, 0.375, 0.438, 0.562,
                   0.562, 0.625, 0.625, 0.625],
            'f-': [0.375, 0.562, 0.438, 0.438, 0.688, 0.625, 0.562, 0.438,
                   0.438, 0.375, 0.375, 0.375],
            'max': [2.694, 3.941, 5.651, 8.358, 11.143, 15.158, 18.751,
                    20.331, 20.676, 20.215, 24.148, 26.904],
            'mean': [0.010, 0.108, 0.200, 0.206, 0.411, 0.908, 1.381,
                     1.864, 2.593, 3.490, 4.645, 5.651],
            'min': [-2.810, -2.577, -3.524, -5.385, -4.465, -6.341,
                    -9.658, -11.910, -12.549, -11.944, -10.148, -7.753],
            'std': [1.456, 1.624, 2.039, 3.191, 4.317, 6.128, 7.805,
                    8.665, 9.038, 9.062, 9.243, 9.162],
            },
            index=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        )
        valid_stats['count'] = valid_stats['count'].astype(int)
        valid_stats['count+'] = valid_stats['count+'].astype(int)
        valid_stats['count-'] = valid_stats['count-'].astype(int)
        valid_stats.index.name = 'leadtime'
        # =====================================================================
        sample, stats = fcst.errors(ref=obs)
        assert_frame_equal(sample, valid_sample, check_less_precise=3)
        assert_frame_equal(stats, valid_stats, check_less_precise=1)
        # =====================================================================

    def test_percentile(self):
        """
        Test du calcul des percentiles

        CAS DE SERIES D'OBSERVATION / SIMULATION

        """
        # =====================================================================
        freqs = [10, 50, 90]
        # =====================================================================
        valid = {
            ('series-q010', 'QH', None): pnd.DataFrame(
                {('series-q010', 'QH'): [
                    np.nan, np.nan, np.nan,
                    17.00, 21.00, 24.00, 28.00, 31.00, 34.00, 37.00,
                    np.nan, np.nan, np.nan]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                ),
            ),
            ('series-q050', 'QH', None): pnd.DataFrame(
                {('series-q050', 'QH'):  [
                    np.nan, np.nan, np.nan,
                    39.95, 53.35, 65.00, 73.00, 73.00, 68.50, 64.75,
                    np.nan, np.nan, np.nan]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                ),
            ),
            ('series-q090', 'QH', None): pnd.DataFrame(
                {('series-q090', 'QH'): [
                    np.nan, np.nan, np.nan,
                    62.90, 85.70, 106.00, 118.00, 115.00, 103.00, 92.50,
                    np.nan, np.nan, np.nan]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                ),
            ),
        }
        # =====================================================================
        perc = self.series.percentile(freqs=freqs, skipna=False)
        for k, v in valid.items():
            self.assertIn(k, perc)
            v.columns = v.columns.to_flat_index()
            assert_frame_equal(perc[k].data_frame, v)
        # =====================================================================

    def test_percentile_fcst_uniquemodel(self):
        """
        Test du calcul des percentiles

        CAS DE SERIES DE PREVISION - MODELE UNIQUE

        """
        # =====================================================================
        freqs = [10, 50, 90]
        model = 'model'
        name = 'Prevision'
        runtime = dt(2014, 11, 4)
        scen = 'percentile'
        # =====================================================================
        valid = {
            ('K0000000', 'QH', (runtime, model, scen, '010')): pnd.DataFrame(
                {('K0000000_2014110400_model_percentile_010', 'QH'): [
                    np.nan, np.nan, np.nan, 17.0, 21.0, 24.0, 28.0, 31.0, 34.0,
                    37.0, 39.0, 41.0, 42.0]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                ),
            ),
            ('K0000000', 'QH', (runtime, model, scen, '050')): pnd.DataFrame(
                {('K0000000_2014110400_model_percentile_050', 'QH'):  [
                    np.nan, np.nan, np.nan, 56.61, 77.13, 95.4, 106.2, 103.5,
                    92.7, 83.25, 77.76, 74.07, 70.47]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                ),
            ),
            ('K0000000', 'QH', (runtime, model, scen, '090')): pnd.DataFrame(
                {('K0000000_2014110400_model_percentile_090', 'QH'): [
                    np.nan, np.nan, np.nan, 66.674, 90.842, 112.36, 125.08,
                    121.9, 109.18, 98.05, 91.584, 87.238, 82.998]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                ),
            ),
        }
        # =====================================================================
        series = Series(datatype='fcst', name=name)
        series.add(serie=self.fcst_11, meta=(runtime, model, 'scen1', None))
        series.add(serie=self.fcst_15, meta=(runtime, model, 'scen2', None))
        series.add(serie=self.fcst_19, meta=(runtime, model, 'scen3', None))
        series.add(serie=self.fcst_25, meta=(runtime, model, 'scen4', None))
        series.add(serie=self.fcst_35, meta=(runtime, model, 'scen5', None))
        perc = series.percentile(freqs=freqs, skipna=False)
        self.maxDiff = None
        for k, v in valid.items():
            self.assertIn(k, perc)
            v.columns = v.columns.to_flat_index()
            assert_frame_equal(perc[k].data_frame, v)
        # =====================================================================

    def test_percentile_fcst_multimodel(self):
        """
        Test du calcul des percentiles

        CAS DE SERIES DE PREVISION - PLUSIEURS MODELES

        """
        # =====================================================================
        freqs = [10, 50, 90]
        model = 'model'
        name = 'Prevision'
        runtime = dt(2014, 11, 4)
        scen = 'percentile'
        # =====================================================================
        valid = {
            ('K0000000', 'QH', (runtime, name, scen, '010')): pnd.DataFrame(
                {('K0000000_2014110400_Prevision_percentile_010', 'QH'): [
                    np.nan, np.nan, np.nan, 17.0, 21.0, 24.0, 28.0, 31.0, 34.0,
                    37.0, 39.0, 41.0, 42.0]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                ),
            ),
            ('K0000000', 'QH', (runtime, name, scen, '050')): pnd.DataFrame(
                {('K0000000_2014110400_Prevision_percentile_050', 'QH'):  [
                    np.nan, np.nan, np.nan, 56.61, 77.13, 95.4, 106.2, 103.5,
                    92.7, 83.25, 77.76, 74.07, 70.47]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                ),
            ),
            ('K0000000', 'QH', (runtime, name, scen, '090')): pnd.DataFrame(
                {('K0000000_2014110400_Prevision_percentile_090', 'QH'): [
                    np.nan, np.nan, np.nan, 66.674, 90.842, 112.36, 125.08,
                    121.9, 109.18, 98.05, 91.584, 87.238, 82.998]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                ),
            ),
        }
        # =====================================================================
        series = Series(datatype='fcst', name=name)
        series.add(serie=self.fcst_11, meta=(runtime, model, 'scen1', None))
        series.add(serie=self.fcst_15, meta=(runtime, model, 'scen2', None))
        series.add(serie=self.fcst_19, meta=(runtime, model, 'scen3', None))
        series.add(serie=self.fcst_25, meta=(runtime, model, 'scen4', None))
        series.add(
            serie=self.fcst_35, meta=(runtime, model + '2', 'scen5', None))
        perc = series.percentile(freqs=freqs, skipna=False)
        self.maxDiff = None
        for k, v in valid.items():
            self.assertIn(k, perc)
            v.columns = v.columns.to_flat_index()
            assert_frame_equal(perc[k].data_frame, v)
        # =====================================================================

    def test_percentile_skipna(self):
        """
        Test du calcul des percentiles - skipna
        """
        # =====================================================================
        valid = {
            ('series-q010', 'QH', None): pnd.DataFrame(
                {('series-q010', 'QH'): [
                    15.12, 17.84, 20.58, 17.00, 21.00, 24.00, 28.00, 31.00,
                    34.00, 37.00, 39.00, 41.00, 42.00]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                ),
            ),
            ('series-q050', 'QH', None): pnd.DataFrame(
                {('series-q050', 'QH'):  [
                    35.60, 41.20, 46.90, 39.95, 53.35, 65.00, 73.00, 73.00,
                    68.50, 64.75, 39.00, 41.00, 42.00]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                ),
            ),
            ('series-q090', 'QH', None): pnd.DataFrame(
                {('series-q090', 'QH'): [
                    35.60, 41.20, 46.90, 62.90, 85.70, 106.00, 118.00, 115.00,
                    103.00, 92.50, 76.92, 74.04, 71.04]},
                index=pnd.date_range(
                    dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
                ),
            ),
        }
        # =====================================================================
        freqs = [10, 50, 90]
        perc = self.series.percentile(freqs=freqs)
        for k, v in valid.items():
            self.assertIn(k, perc)
            v.columns = v.columns.to_flat_index()
            assert_frame_equal(perc[k].data_frame, v)
        # =====================================================================

    def test_waverage(self):
        """
        Test méthode weighted_average
        """
        # =====================================================================
        valid = pnd.DataFrame(
            {('series_waverage', 'QH'): [
                np.nan, np.nan, np.nan,
                39.950001, 53.349998, 65.000000, 73.000000, 73.000000,
                68.500000, 64.750000,
                np.nan, np.nan, np.nan]},
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            ),
        )
        valid.columns = valid.columns.to_flat_index()
        # =====================================================================
        weights = {
            ('K0000000', 'QH', None): 0.4,
            ('K9999999', 'QH', None): 0.3,
            ('K3333333', 'QH', None): 0.2,
            ('K4444444', 'QH', None): 0.1
        }
        waverage = self.series.weighted_average(weights=weights, skipna=False)
        assert_frame_equal(waverage.data_frame, valid)
        # =====================================================================

    def test_waverage_skipna(self):
        """
        Test méthode weighted_average - skipna
        """
        # =====================================================================
        valid = pnd.DataFrame(
            {('series_waverage', 'QH'): [
                26.000000, 30.250000, 34.562500, 39.950001, 53.349998,
                65.000000, 73.000000, 73.000000, 68.500000, 64.750000,
                60.066666, 59.355556, 58.133335]},
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            ),
        )
        valid.columns = valid.columns.to_flat_index()
        # =====================================================================
        weights = {
            ('K0000000', 'QH', None): 0.4,
            ('K9999999', 'QH', None): 0.3,
            ('K3333333', 'QH', None): 0.2,
            ('K4444444', 'QH', None): 0.1
        }
        waverage = self.series.weighted_average(weights=weights)
        assert_frame_equal(waverage.data_frame, valid)
        # =====================================================================
