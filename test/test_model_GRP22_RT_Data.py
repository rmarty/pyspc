#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2022  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Data in pyspc.model.grp22

To run all tests just type:
    python -m unittest test_model_GRP22_RT_Data

To run only a class test:
    python -m unittest test_model_GRP22_RT_Data.TestGRPRT_Data

To run only a specific test:
    python -m unittest test_model_GRP22_RT_Data.TestGRPRT_Data.test_init

"""
# Imports
import filecmp
import numpy as np
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.model.grp22 import GRPRT_Data


class TestGRPRT_Data(unittest.TestCase):
    """
    GRPRT_Data class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp22', 'rt')
        self.valid_scen = None

        self.valid_P = pnd.DataFrame({
            'PREFIX': ['PLU']*36,
            'CODE': sorted([90052002, 90065003]*18),
            'DATE': [20240124, 20240124, 20240124, 20240124, 20240124,
                     20240124, 20240124, 20240124, 20240124, 20240124,
                     20240124, 20240124, 20240125, 20240125, 20240125,
                     20240125, 20240125, 20240125]*2,
            'HOUR': ['12:00', '13:00', '14:00', '15:00', '16:00', '17:00',
                     '18:00', '19:00', '20:00', '21:00', '22:00', '23:00',
                     '00:00', '01:00', '02:00', '03:00', '04:00', '05:00']*2,
            'VALUE': [0.0]*36,
            None: [np.nan]*36,
        })
        self.valid_Q = pnd.DataFrame({
            'PREFIX': ['DEB']*18,
            'CODE': ['RH10585x']*18,
            'DATE': [20240124, 20240124, 20240124, 20240124, 20240124,
                     20240124, 20240124, 20240124, 20240124, 20240124,
                     20240124, 20240124, 20240125, 20240125, 20240125,
                     20240125, 20240125, 20240125],
            'HOUR': ['12:00', '13:00', '14:00', '15:00', '16:00', '17:00',
                     '18:00', '19:00', '20:00', '21:00', '22:00', '23:00',
                     '00:00', '01:00', '02:00', '03:00', '04:00', '05:00'],
            'VALUE': [17.100, 17.100, 17.100, 17.100, 17.100, 17.100, 17.100,
                      17.100, 17.100, 16.800, 16.800, 16.800, 17.200, 18.200,
                      19.100, 19.300, 19.300, 18.700],
            None: [np.nan]*18,
        })

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'Debit.txt')
        reader = GRPRT_Data(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'Q')
        self.assertEqual(reader.lineprefix, 'DEB')
        self.assertIsNone(reader.timestep)
        # =====================================================================
        filename = os.path.join(self.dirname, 'Pluie_00J01H00M.txt')
        reader = GRPRT_Data(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'P')
        self.assertEqual(reader.lineprefix, 'PLU')
        self.assertEqual(reader.timestep, '00J01H00M')
        # =====================================================================

    def test_read_P(self):
        """
        Test de lecture d'un fichier de données OBS pour GRP RT - Cas Pluie
        """
        filename = os.path.join(self.dirname, 'Pluie_00J01H00M.txt')
        reader = GRPRT_Data(filename=filename)
        df = reader.read()
        assert_frame_equal(df, self.valid_P)

    def test_read_Q(self):
        """
        Test de lecture d'un fichier de données OBS pour GRP RT - Cas Débit
        """
        filename = os.path.join(self.dirname, 'Debit.txt')
        reader = GRPRT_Data(filename=filename)
        df = reader.read()
        assert_frame_equal(df, self.valid_Q)

    def test_write_P(self):
        """
        Test d'écriture d'un fichier de données OBS pour GRP RT
        """
        filename = os.path.join(self.dirname, 'Pluie_00J01H00M.txt')
        tmpfile = os.path.join('data', 'Pluie_00J01H00M.txt')
        writer = GRPRT_Data(filename=tmpfile)
        writer.write(self.valid_P)
        self.assertTrue(filecmp.cmp(filename, tmpfile))
        os.remove(tmpfile)

    def test_write_Q(self):
        """
        Test d'écriture d'un fichier de données OBS pour GRP RT
        """
        filename = os.path.join(self.dirname, 'Debit.txt')
        tmpfile = os.path.join('data', 'Debit.txt')
        writer = GRPRT_Data(filename=tmpfile)
        writer.write(self.valid_Q)
        self.assertTrue(filecmp.cmp(filename, tmpfile))
        os.remove(tmpfile)
