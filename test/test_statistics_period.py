#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for functions in pyspc.statistics.period

To run all tests just type:
    python -m unittest test_metadata_statistics

To run only a class test:
    python -m unittest test_metadata_statistics.TestPeriod

To run only a specific test:
    python -m unittest test_metadata_statistics.TestPeriod.test_asstr

"""
# Imports
import os
import unittest

from pyspc.statistics.period import asstr, to_freq


class TestPeriod(unittest.TestCase):
    """
    statistics class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'metadata', 'statistics')

    def test_tofreq(self):
        """
        Test de la conversion d'un temps de retour en fréquence
        """
        # ====================================================================
        # CAS HAUTS DEBITS
        # ====================================================================
        valid = {1: 0, 2: 0.5, 5: 0.8, 10: 0.9, 20: 0.95, 50: 0.98,
                 100: 0.99, 200: 0.995, 500: 0.998, 1000: 0.999}
        for t, v in valid.items():
            self.assertEqual(to_freq(t), v)
            self.assertEqual(to_freq(t, highflow=True), v)
        # ====================================================================
        # CAS BAS DEBITS
        # ====================================================================
        valid = {2: 0.5, 5: 0.2, 10: 0.1, 20: 0.05, 50: 0.02,
                 100: 0.01, 200: 0.005, 500: 0.002, 1000: 0.001, }
        for t, v in valid.items():
            self.assertEqual(to_freq(t, highflow=False), v)
        # ====================================================================
        # ERREURS
        # ====================================================================
        with self.assertRaises(ValueError):
            to_freq(0)
        with self.assertRaises(ValueError):
            to_freq(1.5)
        with self.assertRaises(ValueError):
            to_freq('0')
        with self.assertRaises(ValueError):
            to_freq(2, highflow='a')

    def test_asstr(self):
        """
        Test du temps de retour d'une crue
        """
        # ====================================================================
        # CAS STATISTIQUES COMPLETES
        # ====================================================================
        info = {'2': 2, '5': 5, '10': 10, '20': 20, '50': 50, '100': 100}
        valid = {
            1: '< 2 ans',
            2: '2 ans',
            3: 'entre 2 et 5 ans',
            5: '5 ans',
            8: 'entre 5 et 10 ans',
            10: '10 ans',
            15: 'entre 10 et 20 ans',
            20: '20 ans',
            30: 'entre 20 et 50 ans',
            50: '50 ans',
            70: 'entre 50 et 100 ans',
            100: '100 ans',
            1000: '> 100 ans',
        }
        for q, v in valid.items():
            t = asstr(q, info)
            self.assertEqual(t, v)
        # ====================================================================
        # CAS STATISTIQUES INCOMPLETES
        # ====================================================================
        info = {'2': 2, '5': 5, '10': 10, '20': 20}
        valid = {
            1: '< 2 ans',
            2: '2 ans',
            3: 'entre 2 et 5 ans',
            5: '5 ans',
            8: 'entre 5 et 10 ans',
            10: '10 ans',
            15: 'entre 10 et 20 ans',
            20: '20 ans',
            30: '> 20 ans',
            50: '> 20 ans',
            70: '> 20 ans',
            100: '> 20 ans',
            1000: '> 20 ans',
        }
        for q, v in valid.items():
            t = asstr(q, info)
            self.assertEqual(t, v)
        # ====================================================================
        # CAS STATISTIQUES COMPLETES AVEC PREFIXE
        # ====================================================================
        info = {'t2': 2, 't5': 5, 't10': 10, 't20': 20, 't50': 50, 't100': 100}
        valid = {
            1: '< 2 ans',
            2: '2 ans',
            3: 'entre 2 et 5 ans',
            5: '5 ans',
            8: 'entre 5 et 10 ans',
            10: '10 ans',
            15: 'entre 10 et 20 ans',
            20: '20 ans',
            30: 'entre 20 et 50 ans',
            50: '50 ans',
            70: 'entre 50 et 100 ans',
            100: '100 ans',
            1000: '> 100 ans',
        }
        for q, v in valid.items():
            t = asstr(q, info, 't')
            self.assertEqual(t, v)
