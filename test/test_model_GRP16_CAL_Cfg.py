#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP_Cfg in pyspc.model.grp16

To run all tests just type:
    python -m unittest test_model_GRP16_CAL_Cfg

To run only a class test:
    python -m unittest test_model_GRP16_CAL_Cfg.TestGRP_Cfg

To run only a specific test:
    python -m unittest test_model_GRP16_CAL_Cfg.TestGRP_Cfg.test_init

"""
# Imports
import filecmp
import os
import unittest

from pyspc.model.grp16 import GRP_Run, GRP_Cfg


class TestGRP_Cfg(unittest.TestCase):
    """
    GRP_Cfg class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp16', 'cal')
        self.valid = [
            {'NB': 1, 'CODE': 'K0114020', 'NOM': 'La Besseyre Haute',
             'ST': 0, 'SR': 1, 'AT': 0, 'AR': 0, 'SURFACE': 51.0, 'RT': 'TU',
             'HOR': 3, 'SC': 2.5, 'DEB': '01/01/2014', 'FIN': '31/12/2020',
             'SV': 30.0, 'NJ': 4, 'HC': 24, 'EC': 6, 'ECART': 10, 'INC': 0,
             'TGR': 0},
            {'NB': 2, 'CODE': 'K0114030', 'NOM': 'Chadron',
             'ST': 0, 'SR': 1, 'AT': 0, 'AR': 0, 'SURFACE': 102.0, 'RT': 'TU',
             'HOR': 6, 'SC': 3.0, 'DEB': '01/01/2014', 'FIN': '31/12/2020',
             'SV': 60.0, 'NJ': 4, 'HC': 24, 'EC': 6, 'ECART': 20, 'INC': 0,
             'TGR': 0}
        ]
        self.valid = [
            GRP_Run(**r) for r in self.valid
        ]

    def test_init(self):
        """
        Test de la création de l'instance
        """
        with self.assertRaises(ValueError):
            GRP_Cfg()
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        self.assertEqual(runs.filename, filename)
        self.assertIsInstance(runs, list)

    def test_read(self):
        """
        Test de la lecture du fichier de configuration GRP *Calage*
        """

        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        self.assertEqual(runs, self.valid)

    def test_calibration_values(self):
        """
        Test de la lecture du fichier de configuration GRP *Calage*
        Cas des clés de calibration
        """
        valid = [
            (1, 'K0114020', 'La Besseyre Haute', 0, 1, 0, 0, 51.0,
             'TU', 3, 2.5, '01/01/2014', '31/12/2020'),
            (2, 'K0114030', 'Chadron', 0, 1, 0, 0, 102.0,
             'TU', 6, 3.0, '01/01/2014', '31/12/2020')
        ]
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        calval = runs.get_calibrationvalues()
        self.assertEqual(calval, valid)

    def test_write(self):
        """
        Test de l'écriture du fichier de configuration GRP *Calage*
        """
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        tmpfile = os.path.join('data', 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=tmpfile)
        runs.extend(self.valid)
        runs.write()
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)

    def test_product_0(self):
        """
        Test du produit des configurations - Cas 0 paramètre demandé
        """
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        tmpfile = os.path.join('data', 'LISTE_BASSINS.DAT')
        newruns = runs.product(output_filename=tmpfile, filenames={})
        self.assertIsInstance(newruns, GRP_Cfg)
        self.assertEqual(len(newruns), 2)

    def test_product_1(self):
        """
        Test du produit des configurations - Cas 1 paramètre demandé
        """
        filenames = {
            'SV': os.path.join(self.dirname, 'SV.txt'),
        }
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        tmpfile = os.path.join('data', 'LISTE_BASSINS.DAT')
        newruns = runs.product(output_filename=tmpfile, filenames=filenames)
        self.assertIsInstance(newruns, GRP_Cfg)
        self.assertEqual(len(newruns), 6)

    def test_product_2(self):
        """
        Test du produit des configurations - Cas 2 paramètres demandés
        """
        filenames = {
            'SV': os.path.join(self.dirname, 'SV.txt'),
            'SC': os.path.join(self.dirname, 'SC.txt')
        }
        filename = os.path.join(self.dirname, 'LISTE_BASSINS.DAT')
        runs = GRP_Cfg(filename=filename)
        runs.read()
        tmpfile = os.path.join('data', 'LISTE_BASSINS.DAT')
        newruns = runs.product(output_filename=tmpfile, filenames=filenames)
        self.assertIsInstance(newruns, GRP_Cfg)
        self.assertEqual(len(newruns), 18)
