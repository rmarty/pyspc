#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for colormap in pyspc.plotting

To run all tests just type:
    python -m unittest test_plotting_Colormap

To run only a class test:
    python -m unittest test_plotting_Colormap.TestColormap

To run only a specific test:
    python -m unittest test_plotting_Colormap.TestColormap.test_cmap

"""
# Imports
import numpy as np
import unittest

from pyspc.plotting import colormap as _cmap


class TestColormap(unittest.TestCase):
    """
    Colormap class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """

    def test_cmap(self):
        """
        Test de la définition de palette de couleur
        """
        pal = _cmap.build_colormap(cmapname='jet', cmapsize=8)
        ctl = [
            [0., 0., 0.5, 1.],
            [0., 0.06470588, 1., 1.],
            [0., 0.62941176, 1., 1.],
            [0.24984187, 1., 0.71790006, 1.],
            [0.70524984, 1., 0.26249209, 1.],
            [1., 0.72694263, 0., 1.],
            [1., 0.20406681, 0., 1.],
            [0.5, 0., 0., 1.]
        ]
        try:
            np.testing.assert_array_almost_equal(pal, ctl)
        except AssertionError:
            self.fail('Configuration inattendue de la palette testée')

    def test_cmap_errors(self):
        """
        Test des erreurs soulevées la définition de palette de couleur
        """
        with self.assertRaises(ValueError):
            _cmap.build_colormap(cmapname='jet', cmapsize=0)
        with self.assertRaises(ValueError):
            _cmap.build_colormap(cmapname='jet', cmapsize=300)

    def test_cmap_default(self):
        """
        Test de la définition de la palette de couleur par défaut
        """
        pal1 = _cmap.build_colormap()
        pal2 = _cmap.build_colormap(cmapname='gist_rainbow')
        pal3 = _cmap.build_colormap(cmapsize=256)
        pal4 = _cmap.build_colormap(cmapname='gist_rainbow', cmapsize=256)
        try:
            np.testing.assert_array_equal(pal1, pal2)
        except AssertionError:
            self.fail('Configuration inattendue de la palette par défaut 2')
        try:
            np.testing.assert_array_equal(pal1, pal3)
        except AssertionError:
            self.fail('Configuration inattendue de la palette par défaut 3')
        try:
            np.testing.assert_array_equal(pal1, pal4)
        except AssertionError:
            self.fail('Configuration inattendue de la palette par défaut 4')

    def test_cmap_reverse(self):
        """
        Test de la définition de la palette de couleur par défaut
        """
        # ==================================================================
        g1 = _cmap.build_colormap(cmapname='gray', cmapsize=11)
        v1 = [[0., 0., 0., 1.],
              [0.09803922, 0.09803922, 0.09803922, 1.],
              [0.2, 0.2, 0.2, 1.],
              [0.29803922, 0.29803922, 0.29803922, 1.],
              [0.4, 0.4, 0.4, 1.],
              [0.49803922, 0.49803922, 0.49803922, 1.],
              [0.6, 0.6, 0.6, 1.],
              [0.69803922, 0.69803922, 0.69803922, 1.],
              [0.8, 0.8, 0.8, 1.],
              [0.89803922, 0.89803922, 0.89803922, 1.],
              [1., 1., 1., 1.]]
        np.testing.assert_allclose(g1, v1, rtol=1e-04)
        # ==================================================================
        g2 = _cmap.build_colormap(cmapname='gray', cmapsize=11, reverse=True)
        v2 = [[1., 1., 1., 1.],
              [0.89803922, 0.89803922, 0.89803922, 1.],
              [0.8, 0.8, 0.8, 1.],
              [0.69803922, 0.69803922, 0.69803922, 1.],
              [0.6, 0.6, 0.6, 1.],
              [0.49803922, 0.49803922, 0.49803922, 1.],
              [0.4, 0.4, 0.4, 1.],
              [0.29803922, 0.29803922, 0.29803922, 1.],
              [0.2, 0.2, 0.2, 1.],
              [0.09803922, 0.09803922, 0.09803922, 1.],
              [0., 0., 0., 1.]]
        np.testing.assert_allclose(g2, v2, rtol=1e-04)
        # ==================================================================

    def test_cmap_partial(self):
        """
        Test de la définition de la palette de couleur par défaut
        """
        # ==================================================================
        g1 = _cmap.build_colormap(cmapname='gray', cmapsize=5, partial=0.25)
        v1 = [[0., 0., 0., 1.],
              [0.05882353, 0.05882353, 0.05882353, 1.],
              [0.12156863, 0.12156863, 0.12156863, 1.],
              [0.18431373, 0.18431373, 0.18431373, 1.],
              [0.24705882, 0.24705882, 0.24705882, 1.]]
        np.testing.assert_allclose(g1, v1, rtol=1e-04)
        # ==================================================================
        g2 = _cmap.build_colormap(cmapname='gray', cmapsize=5, reverse=True,
                                  partial=0.25)
        v2 = [[1., 1., 1., 1.],
              [0.9372549, 0.9372549, 0.9372549, 1.],
              [0.8745098, 0.8745098, 0.8745098, 1.],
              [0.81176471, 0.81176471, 0.81176471, 1.],
              [0.75294118, 0.75294118, 0.75294118, 1.]]
        np.testing.assert_allclose(g2, v2, rtol=1e-04)
        # ==================================================================
