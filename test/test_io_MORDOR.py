#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_Mordor and write_Mordor in pyspc.io.mordor

To run all tests just type:
    python -m unittest test_data_Mordor

To run only a class test:
    python -m unittest test_data_Mordor.TestMordor

To run only a specific test:
    python -m unittest test_data_Mordor.TestMordor.test_init

"""
# Imports
from datetime import datetime as dt
import os
import unittest

from pyspc.core.series import Series
from pyspc.io.mordor import read_Mordor  # , write_Mordor


class TestMordor(unittest.TestCase):
    """
    Mordor_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'mordor')

    def test_read_fcst(self):
        """
        Test de lecture d'un fichier de prévisions QMJ par MORDOR
        """
        # =====================================================================
        scens = ['CEPQ2018', 'CEPQ2019', 'CEPQ2020']
        # =====================================================================
        filename = os.path.join(self.dirname, 'Spaghettis_BASSIN_20220623.txt')
        meteo = 'CEP'
        series = read_Mordor(filename=filename, meteo=meteo)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 3)
        for s in scens:
            self.assertIn(
                ('BASSIN', 'QJ', (dt(2022, 6, 23, 0, 0), 'MORDOR', s, None)),
                series)
        # =====================================================================
