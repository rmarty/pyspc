#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary dbaseCreate

To run all tests just type:
    python -m unittest test_bin_dbaseCreate

To run only a class test:
    python -m unittest test_bin_dbaseCreate.Test_dbaseCreate

To run only a specific test:
    python -m unittest test_bin_dbaseCreate.Test_dbaseCreate.test_sacha

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest
import hashlib

from pyspc.binutils.args import dbaseCreate as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


def md5sum(filename, blocksize=65536):
    """Calcul MD5sum"""
    hashvalue = hashlib.md5()  # nosec
    with open(filename, "rb") as f:
        for block in iter(lambda: f.read(blocksize), b""):
            hashvalue.update(block)
    return hashvalue.hexdigest()


class Test_dbaseCreate(unittest.TestCase):
    """
    dbaseCreate bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'dbaseCreate')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.resources = os.path.join('..', 'resources', 'dbase')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_sacha_defaut(self):
        """
        Test creation base SACHA
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbaseCreate.py',
            '-O', self.dir_out,
            '-t', 'sacha'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbaseCreate()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'sacha')
        self.assertIsNone(args.db_filename)
        self.assertIsNone(args.cfg_filename)
#        self.assertTrue(args.verbose)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'sacha.mdb'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.resources, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Créer une base vierge de type {t} dans le "\
            "répertoire {O} avec le nom par défaut (sacha.mdb)"\
            "".format(
                t=args.data_type, O=args.output_dir
                )
        # =====================================================================

    def test_sacha_filename(self):
        """
        Test creation base SACHA
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbaseCreate.py',
            '-O', self.dir_out,
            '-d', 'mydbase_sacha.mdb',
            '-t', 'sacha'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbaseCreate()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'sacha')
        self.assertEqual(args.db_filename, 'mydbase_sacha.mdb')
        self.assertIsNone(args.cfg_filename)
#        self.assertTrue(args.verbose)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, args.db_filename),
            os.path.join(self.resources, 'sacha.mdb'),
        ))
        os.remove(os.path.join(self.dir_out, args.db_filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Créer une base vierge de type {t} dans le "\
            "répertoire {O}, l'utilisateur ayant défini le nom de la base "\
            "vierge {d}"\
            "".format(
                t=args.data_type, O=args.output_dir, d=args.db_filename
                )
        # =====================================================================

    def test_previ19_empty(self):
        """
        Test creation base PREVISION 2019 -- VERSION ACCESS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbaseCreate.py',
            '-O', self.dir_out,
            '-t', 'previ19',
            '-d', 'previ2019_empty.mdb'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbaseCreate()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'previ19')
        self.assertEqual(args.db_filename, 'previ2019_empty.mdb')
        self.assertIsNone(args.cfg_filename)
#        self.assertTrue(args.verbose)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        # =====================================================================
        md5test = md5sum(os.path.join(self.dir_out, args.db_filename))
        self.assertEqual(md5test, 'e04cb9f1f81ba78480afc4014d59758e')
        os.remove(os.path.join(self.dir_out, args.db_filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Créer une base vierge de type {t} dans le "\
            "répertoire {O}, l'utilisateur ayant défini le nom de la base "\
            "vierge {d}"\
            "".format(
                t=args.data_type, O=args.output_dir, d=args.db_filename
                )
        # =====================================================================

    def test_previ19_models(self):
        """
        Test creation base PREVISION 2019 -- VERSION ACCESS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbaseCreate.py',
            '-O', self.dir_out,
            '-t', 'previ19',
            '-d', 'previ19_models.mdb',
            '-c', os.path.join(self.dir_in, 'previ19_models.txt')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbaseCreate()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'previ19')
        self.assertEqual(args.db_filename, 'previ19_models.mdb')
        self.assertEqual(args.cfg_filename,
                         os.path.join(self.dir_in, 'previ19_models.txt'))
#        self.assertTrue(args.verbose)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        # =====================================================================
        md5test = md5sum(os.path.join(self.dir_out, args.db_filename))
        self.assertEqual(md5test, 'c8f717818c44ceebbc9027445a2788a0')
        os.remove(os.path.join(self.dir_out, args.db_filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Créer une base de type {t} dans le "\
            "répertoire {O}, l'utilisateur ayant défini le nom de la base "\
            "{d}. Celle-ci est complétée par les informations contenues "\
            "dans le fichier {c}."\
            "".format(
                t=args.data_type, O=args.output_dir, d=args.db_filename,
                c=args.cfg_filename
                )
        # =====================================================================
