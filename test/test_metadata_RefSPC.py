#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for RefSPC in pyspc.metadata.refspc

To run all tests just type:
    python -m unittest test_metadata_RefSPC

To run only a class test:
    python -m unittest test_metadata_RefSPC.TestRefSPC

To run only a specific test:
    python -m unittest test_metadata_RefSPC.TestRefSPC.test_init

"""
# Imports
import os
import unittest
from unittest import mock

from pyspc.metadata.refspc import RefSPC


class TestRefSPC(unittest.TestCase):
    """
    RefSPC class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'metadata', 'refspc')
        self.filename = os.path.join(self.dirname, 'BDD_light.sqlite')
#        self.maxDiff = None

    def test_init(self):
        """
        Test de la création de l'instance
        """
        with self.assertRaises(ValueError):
            RefSPC()
        reader = RefSPC(filename=self.filename)
        self.assertEqual(reader.filename, self.filename)
        self.assertIsNone(reader._dbase_connect)
        self.assertIsNone(reader._dbase_cursor)
        self.assertIsNone(reader.sql)
        self.assertIsNone(reader._tables)

    def test_check_codelens(self):
        """
        Test du contrôle des longueurs des identifiants
        """
        reader = RefSPC(filename=self.filename)
        with self.assertRaises(ValueError):
            reader._check_codelens(codes=['123456'])
        with self.assertRaises(ValueError):
            reader._check_codelens(codes=['123456', '12345678'])
        with self.assertRaises(ValueError):
            reader._check_codelens(codes=['1234567890'], target=8)
        with self.assertRaises(ValueError):
            reader._check_codelens(codes=['12345678'], target=[10, 12])
        self.assertEqual(reader._check_codelens(
            codes=['123456789012']), 12)
        self.assertEqual(reader._check_codelens(
            codes=['1234567890']), 10)
        self.assertEqual(reader._check_codelens(
            codes=['12345678']), 8)
        self.assertEqual(reader._check_codelens(
            codes=['12345678', '12345678']), 8)

    def test_check_codes(self):
        """
        Test du contrôle des identifiants
        """
        reader = RefSPC(filename=self.filename)
        with self.assertRaises(ValueError):
            reader._check_codes(codes=1)
        codes = ['1234', '5678']
        self.assertEqual(reader._check_codes(codes=codes), codes)
        self.assertEqual(reader._check_codes(codes=codes[0]), [codes[0]])

    def test_check_datatype(self):
        """
        Test du contrôle du type d'entité
        """
        valid = ['loc_hydro', 'loc_meteo', 'reach', 'stat_hydro', 'stat_meteo']
        reader = RefSPC(filename=self.filename)
        for v in valid:
            self.assertTrue(reader._check_datatype(datatype=v))
        with self.assertRaises(ValueError):
            reader._check_datatype(datatype=1)
        with self.assertRaises(ValueError):
            reader._check_datatype(datatype='1')
        with self.assertRaises(ValueError):
            reader._check_datatype(datatype='toto')

    def test_datatypes(self):
        """
        Test des types de données RefSPC
        """
        dtypes = ['loc_hydro', 'loc_meteo', 'reach',
                  'stat_hydro', 'stat_meteo']
        self.assertEqual(RefSPC.get_datatypes(), dtypes)

    def test_get(self):
        """
        test de la méthode principale d'extraction
        """
        reader = RefSPC(filename=self.filename)
        with mock.patch.object(reader, 'get_loc_hydro') as mockget:
            codes = ['K0010020', 'K0114020', 'K0550010']
            reader.get(
                codes=codes,
                datatype='loc_hydro'
            )
            mockget.assert_called_with(
                codes=codes,
                how='by_code',
                hydro3=True
            )
            reader.get(
                codes=codes,
                datatype='loc_hydro',
                how='by_reach',
                hydro3=False
            )
            mockget.assert_called_with(
                codes=codes,
                how='by_reach',
                hydro3=False
            )
        with mock.patch.object(reader, 'get_loc_meteo') as mockget:
            codes = ['K0010020', 'K0114020', 'K0550010']
            reader.get(
                codes=codes,
                datatype='loc_meteo'
            )
            mockget.assert_called_with(
                codes=codes
            )
        with mock.patch.object(reader, 'get_reach') as mockget:
            codes = ['K0010020', 'K0114020', 'K0550010']
            reader.get(
                codes=codes,
                datatype='reach'
            )
            mockget.assert_called_with(
                codes=codes,
                how='by_code',
                hydro3=True
            )
            reader.get(
                codes=codes,
                datatype='reach',
                how='by_loc',
                hydro3=False
            )
            mockget.assert_called_with(
                codes=codes,
                how='by_loc',
                hydro3=False
            )
        with mock.patch.object(reader, 'get_stat_hydro') as mockget:
            codes = ['K0010020', 'K0114020', 'K0550010']
            reader.get(
                codes=codes,
                datatype='stat_hydro'
            )
            mockget.assert_called_with(
                codes=codes
            )
        with mock.patch.object(reader, 'get_stat_meteo') as mockget:
            codes = ['K0010020', 'K0114020', 'K0550010']
            reader.get(
                codes=codes,
                datatype='stat_meteo'
            )
            mockget.assert_called_with(
                codes=codes
            )

    def test_get_loc_hydro_site(self):
        """
        Test extraction info sur lieu hydro - SITE
        """
        codes = ['K0010020', 'K0114020', 'K0550010']
        valid_atts = ["id_site_hydro", "code_site", "nom_site_hydro",
                      "nom_riviere", "type", "departement",
                      "id_bnbv", "taille_bv", "x_l93", "y_l93",
                      'reaches']
        reader = RefSPC(filename=self.filename)
        header, content = reader.get_loc_hydro(codes=codes)
        self.assertTrue(len(content) > 0)
        for v in content.values():
            self.assertIn(v['code_site'], codes)
        self.assertListEqual(header, valid_atts)

    def test_get_loc_hydro_reach(self):
        """
        Test extraction info sur lieu hydro - STATION par TRONCON
        """
        codes = ['LC105']
        valid_atts = ["id_station", "code_station", "nom_station_hydro",
                      "type", "departement", "x_l93", "y_l93",
                      'delai_vigicrues', 'reaches']
        reader = RefSPC(filename=self.filename)
        header, content = reader.get_loc_hydro(codes=codes, how='by_reach')
        self.assertTrue(len(content) > 0)
        for v in content.values():
            self.assertTrue(len(v['reaches']) > 0)
            for r in v['reaches']:
                self.assertIn(r, codes)
        self.assertListEqual(header, valid_atts)

    def test_get_loc_hydro_reach_hydro2(self):
        """
        Test extraction info sur lieu hydro - STATION par TRONCON - HYDRO2
        """
        codes = ['LC105']
        valid_atts = ["id_capteur_hydro", "code_hydro2", "code_capteur",
                      "nom_capteur_phyc", "type", "departement",
                      "x_l93", "y_l93",
                      'reaches']
        reader = RefSPC(filename=self.filename)
        header, content = reader.get_loc_hydro(
            codes=codes, how='by_reach', hydro3=False)
        self.assertTrue(len(content) > 0)
        for v in content.values():
            for r in v['reaches']:
                self.assertIn(r, codes)
        self.assertListEqual(header, valid_atts)

    def test_get_loc_hydro_station(self):
        """
        Test extraction info sur lieu hydro - STATION

        # base light à refaire : 'delai_vigicrues_2018'
        """
        codes = ['K010002010', 'K026001002', 'K055001010']
        valid_atts = ["id_station", "code_station", "nom_station_hydro",
                      "type", "departement", "x_l93", "y_l93",
                      'delai_vigicrues', 'reaches']
        reader = RefSPC(filename=self.filename)
        header, content = reader.get_loc_hydro(codes=codes)
        self.assertTrue(len(content) > 0)
        for v in content.values():
            self.assertIn(v['code_station'], codes)
        self.assertListEqual(header, valid_atts)

    def test_get_loc_hydro_capteur(self):
        """
        Test extraction info sur lieu hydro - CAPTEUR
        """
        codes = ['K01000201001', 'K05500101001', 'K05500101002']
        valid_atts = ["id_capteur_hydro", "code_hydro2", "code_capteur",
                      "nom_capteur_hydro", "type", "departement",
                      "x_l93", "y_l93",
                      'reaches']
        reader = RefSPC(filename=self.filename)
        header, content = reader.get_loc_hydro(codes=codes)
        self.assertTrue(len(content) > 0)
        for v in content.values():
            self.assertIn(v['code_capteur'], codes)
        self.assertListEqual(header, valid_atts)

    def test_get_loc_hydro_station_hydro2(self):
        """
        Test extraction info sur lieu hydro - STATION HYDRO-2
        """
        codes = ['K0100020', 'K0100030', 'K0260020', 'K0260030']
        valid_atts = ["id_capteur_hydro", "code_hydro2", "code_capteur",
                      "nom_capteur_phyc", "type", "departement",
                      "x_l93", "y_l93",
                      'reaches']
        reader = RefSPC(filename=self.filename)
        header, content = reader.get_loc_hydro(codes=codes, hydro3=False)
        self.assertTrue(len(content) > 0)
        for v in content.values():
            self.assertIn(v['code_hydro2'], codes)
        self.assertListEqual(header, valid_atts)

    def test_get_stat_hydro(self):
        """
        Test extraction stat sur site hydro
        """
        codes = ['K0260010']
        valid_atts = ["id_stats_hydro", "code_site",
                      "t2", "t5", "t10", "t20", "t50", "t100",
                      "methode", "type_loi", "taille_echantillon",
                      "periode_echantillon", "date_calcul", "stat_ref_spc"]
        reader = RefSPC(filename=self.filename)
        header, content = reader.get_stat_hydro(codes=codes)
        self.assertTrue(len(content) > 0)
        for v in content.values():
            self.assertIn(v['code_site'], codes)
        self.assertListEqual(header, valid_atts)

    def test_get_loc_meteo(self):
        """
        Test extraction info sur site meteo - PAR CODE
        """
        codes = ['07235005', '43042002', '43091005']
        valid_atts = ["id_site_meteo", "code_mf", "nom_mf", "nom_site_meteo",
                      "x_l93", "y_l93", "z"]
        reader = RefSPC(filename=self.filename)
        header, content = reader.get_loc_meteo(codes=codes)
        self.assertEqual(len(content), 3)
        for v in content.values():
            self.assertIn(v['code_mf'], codes)
        self.assertListEqual(header, valid_atts)

    def test_get_loc_meteo_dept(self):
        """
        Test extraction info sur site meteo - PAR DEPT
        """
        codes = ['07']
        valid_atts = ["id_site_meteo", "code_mf", "nom_mf", "nom_site_meteo",
                      "x_l93", "y_l93", "z"]
        reader = RefSPC(filename=self.filename)
        header, content = reader.get_loc_meteo(codes=codes, how='by_dept')
        self.assertEqual(len(content), 32)
        for v in content.values():
            self.assertTrue(v['code_mf'].startswith(codes[0]))
        self.assertListEqual(header, valid_atts)

    def test_get_stat_meteo(self):
        """
        Test extraction stat sur site meteo
        """
        codes = ['07119002']
        valid_atts = ["id_stats_pluvio", "code_mf", "duree",
                      "t2", "t5", "t10", "t20", "t50", "t100",
                      "methode", "taille_echantillon", "periode_echantillon",
                      "date_calcul", "source"]
        reader = RefSPC(filename=self.filename)
        header, content = reader.get_stat_meteo(codes=codes)
        self.assertEqual(len(content), 12)
        for v in content.values():
            self.assertIn(v['code_mf'], codes)
        self.assertListEqual(header, valid_atts)

    def test_get_reach(self):
        """
        Test extraction info sur troncon - PAR CODE
        """
        codes = ['LC105', 'LC134', 'LC265']
        valid = {
            1: {'id_troncon': 1,
                'code_troncon': 'LC105',
                'nom_troncon': 'Loire vellave',
                'locations': ['K001002010', 'K001872010', 'K001872301',
                              'K003002010', 'K004551001', 'K010002010',
                              'K011402001', 'K011403001', 'K013401001',
                              'K021401001', 'K026001002', 'K027401002',
                              'K033301001', 'K034000101', 'K035631001',
                              'K051301001', 'K052301001', 'K054301001',
                              'K054301201', 'K055001010', 'K056752001',
                              'K058000101']},
            6: {'id_troncon': 6,
                'code_troncon': 'LC134',
                'nom_troncon': 'Arroux',
                'locations': ['K124301001', 'K125181001', 'K127311001',
                              'K128481001', 'K131401001', 'K132181010',
                              'K134181001', 'K139181001']},
            22: {'id_troncon': 22,
                 'code_troncon': 'LC265',
                 'nom_troncon': 'Sauldre amont',
                 'locations': ['K632251010', 'K633252001', 'K633401001',
                               'K637302002', 'K640252001']}
        }
        valid_atts = ["id_troncon", "code_troncon", "nom_troncon",
                      'locations']
        reader = RefSPC(filename=self.filename)
        header, content = reader.get_reach(codes=codes)
        self.assertDictEqual(content, valid)
        self.assertListEqual(header, valid_atts)
        header, content = reader.get_reach(codes=codes, how='by_code')
        self.assertDictEqual(content, valid)
        self.assertListEqual(header, valid_atts)
        with self.assertRaises(ValueError):
            reader.get_reach(codes=codes, how='toto')

    def test_locs_from_reaches(self):
        """
        Test extraction info sur station - PAR TRONCON
        """
        codes = ['LC105', 'LC123']
        valid = {
            'K001002010': ['LC105'],
            'K001872010': ['LC105'],
            'K001872301': ['LC105'],
            'K003002010': ['LC105'],
            'K004551001': ['LC105'],
            'K010002010': ['LC105'],
            'K011402001': ['LC105'],
            'K011403001': ['LC105'],
            'K013401001': ['LC105'],
            'K021401001': ['LC105'],
            'K026001002': ['LC105'],
            'K027401002': ['LC105'],
            'K033301001': ['LC105'],
            'K034000101': ['LC105'],
            'K035631001': ['LC105'],
            'K040301001': ['LC123'],
            'K040302002': ['LC123'],
            'K043302010': ['LC123'],
            'K043303001': ['LC123'],
            'K045401001': ['LC123'],
            'K045402001': ['LC123'],
            'K046301001': ['LC123'],
            'K046301002': ['LC123'],
            'K051301001': ['LC105'],
            'K052301001': ['LC105'],
            'K054301001': ['LC105'],
            'K054301201': ['LC105'],
            'K055001010': ['LC105'],
            'K056752001': ['LC105'],
            'K058000101': ['LC105'],
        }
        reader = RefSPC(filename=self.filename)
        content = reader._locs_from_reaches(codes=codes)
        self.assertDictEqual(content, valid)
        codes = ['LC105']
        valid = {
            'K0010010': ['LC105', 'LC105'],
            'K0010020': ['LC105'],
            'K0018720': ['LC105'],
            'K0030020': ['LC105', 'LC105'],
            'K0045510': ['LC105'],
            'K0100020': ['LC105'],
            'K0100030': ['LC105', 'LC105'],
            'K0114020': ['LC105'],
            'K0114030': ['LC105'],
            'K0134010': ['LC105'],
            'K0214010': ['LC105'],
            'K0260020': ['LC105'],
            'K0260030': ['LC105'],
            'K0274020': ['LC105'],
            'K0333010': ['LC105'],
            'K0340010': ['LC105'],
            'K0356310': ['LC105'],
            'K0513010': ['LC105'],
            'K0523010': ['LC105'],
            'K0543010': ['LC105'],
            'K0543020': ['LC105'],
            'K0550010': ['LC105', 'LC105'],
            'K0550020': ['LC105'],
            'K0567520': ['LC105'],
            'K0580010': ['LC105'],
        }
        content = reader._locs_from_reaches(codes=codes, hydro3=False)
        self.assertDictEqual(content, valid)

    def test_reaches_from_locs(self):
        """
        Test extraction info sur troncon - PAR STATION
        """
        codes = ['K001002010', 'K055001010']
        valid = {
            'LC105': ['K001002010', 'K055001010']
        }
        reader = RefSPC(filename=self.filename)
        content = reader._reaches_from_locs(codes=codes)
        self.assertDictEqual(content, valid)
        valid = {
            1: {'id_troncon': 1,
                'code_troncon': 'LC105',
                'nom_troncon': 'Loire vellave',
                'locations': ['K001002010', 'K055001010']},
        }
        valid_atts = ["id_troncon", "code_troncon", "nom_troncon",
                      'locations']
        header, content = reader.get_reach(codes=codes, how='by_loc')
        self.assertListEqual(header, valid_atts)
        self.assertDictEqual(content, valid)

    def test_meteo_from_depts(self):
        """
        Test stations par département
        """
        codes = ['07']
        valid = ["07019005", "07064005", "07071001", "07117001", "07119002",
                 "07119003", "07105003", "07130003", "07136003", "07144003",
                 "07154001", "07154005", "07161001", "07161007", "07202002",
                 "07204008", "07232002", "07235001", "07235005", "07326004",
                 "07334003", "07025001", "07347001", "07011002", "07105001",
                 "07187001", "07326004", "07101002", "07075001", "07154003",
                 "07232001", "07128001"]
        reader = RefSPC(filename=self.filename)
        content = reader._meteo_from_depts(codes=codes)
        self.assertListEqual(content, valid)
