#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for RatingCurve in pyspc.core.ratingcurve

To run all tests just type:
    python -m unittest test_core_RatingCurve

To run only a class test:
    python -m unittest test_core_RatingCurve.TestRatingCurve

To run only a specific test:
    python -m unittest test_core_RatingCurve.TestRatingCurve.test_init

"""
# Imports
from datetime import datetime as dt
from numpy.testing import assert_almost_equal
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.core.ratingcurve import RatingCurve


class TestRatingCurve(unittest.TestCase):
    """
    RatingCurve class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """

    def test_init(self):
        """
        Test de la création de l'instance
        """
        code = 'K010002010'
        num = 'H200708'
        provider = 'Bareme'
        valid_dt = (dt(2013, 3, 1), dt(2018, 8, 31))
        valid_interval = (0, 10)
        update_dt = dt.utcnow()
        curve = RatingCurve(
            code=code,
            num=num,
            provider=provider,
            valid_dt=valid_dt,
            valid_interval=valid_interval,
            update_dt=update_dt
        )
        self.assertEqual(curve.code, code)
        self.assertEqual(curve.num, num)
#        self.assertEqual(curve.provider, provider)
        self.assertEqual(curve.provider.name, provider)
        self.assertEqual(curve.valid_dt, valid_dt)
        self.assertEqual(curve.timeinterval.left, valid_dt[0])
        self.assertEqual(curve.timeinterval.right, valid_dt[1])
        self.assertEqual(curve.valid_interval, valid_interval)
        self.assertEqual(curve.update_dt, update_dt)
        self.assertIsNone(curve.levelcor)
        self.assertIsNone(curve.flowmes)
        self.assertIsNone(curve.hq)

    def test_init_error(self):
        """
        Test de la création de l'instance - Cas avec erreurs
        """
        with self.assertRaises(ValueError):
            RatingCurve(
                valid_dt=1
            )
        with self.assertRaises(ValueError):
            RatingCurve(
                valid_dt=(1, )
            )
        with self.assertRaises(ValueError):
            RatingCurve(
                valid_dt=(1, 2)
            )
        with self.assertRaises(ValueError):
            RatingCurve(
                valid_interval=1
            )
        with self.assertRaises(ValueError):
            RatingCurve(
                valid_interval=(1, )
            )
        with self.assertRaises(ValueError):
            RatingCurve(
                valid_interval=('a', 2)
            )
        with self.assertRaises(ValueError):
            RatingCurve(
                update_dt=1
            )

    def test_interpolate_levelcor(self):
        """
        Test de l'interpolation des courbes de correction
        """
        # =====================================================================
        levelcor = pnd.DataFrame({
                'h': [0, -30, -30, -30, -30, -30, 0,  0]
            },
            index=[dt(2014, 4, 23, 13, 40), dt(2014, 5, 22, 8, 20),
                   dt(2014, 6, 5, 8, 35), dt(2014, 6, 18, 6, 20),
                   dt(2014, 7, 1, 9, 50), dt(2014, 7, 6, 20), dt(2014, 7, 9),
                   dt(2014, 8, 4, 12)]
        )
        # =====================================================================
        valid_index = [dt(2014, 4, 1), dt(2014, 5, 1), dt(2014, 6, 1),
                       dt(2014, 7, 1), dt(2014, 7, 6), dt(2014, 7, 7),
                       dt(2014, 7, 8), dt(2014, 7, 9), dt(2014, 7, 10),
                       dt(2014, 8, 1), dt(2014, 9, 1)]
        valid = pnd.DataFrame({
                'h': [0, -7.75, -30, -30, -30, -27.69, -13.85, 0, 0, 0, 0]
            },
            index=valid_index
        )
        # =====================================================================
        curve = RatingCurve(levelcor=levelcor)
        interp = curve.interpolate_levelcor(valid_index)
        assert_frame_equal(interp, valid, check_less_precise=2)
        # =====================================================================

    def test_convert(self):
        """
        Test de la conversion H <-> Q
        """
        # =====================================================================
        hq = pnd.DataFrame({
                'h': [
                    1040, 1070, 1110, 1160, 1170, 1170, 1210, 1250, 1360, 1420,
                    1500, 1500, 1530, 1550, 1580, 1630, 1690, 1780, 1900, 2020,
                    2180, 2220, 2240, 2270, 2290, 2330, 2350, 2580, 2830, 3140,
                    3350, 3530, 4050, 4740, 5120, 5140, 5190, 5210],
                'q': [
                    0.41, 0.63, 0.95, 1.39, 1.49, 1.51, 2.12, 2.71, 4.97, 6.89,
                    10.00, 9.74, 12.30, 14.00, 16.60, 21.29, 27.50, 37.79,
                    53.40, 70.80, 96.69, 104.00, 107.00, 113.00, 116.00,
                    124.00, 127.00, 173.00, 229.00, 306.00, 362.00, 417.00,
                    592.00, 849.00, 1000.00, 1010.00, 1030.00, 1040.00],
            },
            index=range(1, 39)
        )
        curve = RatingCurve(hq=hq)
        # =====================================================================
        valid = {
            1200: 1.97,
            1500: 9.74,
            2000: 67.90,
            3000: 271.23,
            4000: 575.17,
            5200: 1035.00,
        }
        assert_almost_equal(
            curve.convert(list(valid.keys()), 'h'),
            list(valid.values()),
            decimal=2
        )
        # =====================================================================
        valid = {
            5: 1360.94,
            50: 1873.86,
            100: 2198.11,
            200: 2700.54,
            500: 3776.63,
            1000: 5120.00,
        }
        assert_almost_equal(
            curve.convert(list(valid.keys()), 'q'),
            list(valid.values()),
            decimal=2
        )
        # =====================================================================
