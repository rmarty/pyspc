#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Bareme in pyspc.data.Bareme

To run all tests just type:
    python -m unittest test_data_Bareme

To run only a class test:
    python -m unittest test_data_Bareme.TestBareme

To run only a specific test:
    python -m unittest test_data_Bareme.TestBareme.test_init

"""
# Imports
from datetime import datetime as dt
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.data.bareme import Bareme


class TestBareme(unittest.TestCase):
    """
    Bareme class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'io', 'dbase')
        self.source = os.path.join(self.dirname, 'bareme.mdb')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        reader = Bareme(filename=self.source)
        self.assertEqual(reader.filename, self.source)
        self.assertIsNone(reader._dbase_connect)
        self.assertIsNone(reader._dbase_cursor)
        self.assertIsNone(reader.sql)
        self.assertIsNone(reader._tables)

    def test_datatypes(self):
        """
        Test des types de données Bareme
        """
        dtypes = ['flowmes', 'levelcor', 'ratingcurve']
        self.assertEqual(Bareme.get_datatypes(), dtypes)

    def test_read_flowmes(self):
        """
        Test de lecture des données flowmes
        """
        # =====================================================================
        valid = pnd.DataFrame({
                'jcote': [
                    -1190, -1250, -1270, 5050, 5070, 5070, 4660, 4360, -785],
                'jdebit': [
                    9.27, 6.66, 6.26, 252.00, 2790.00, 2850.00, 2790.00,
                    2500.00, 42.60],
            },
            index=[dt(2008, 9, 2, 7), dt(2008, 9, 25, 13, 15),
                   dt(2008, 10, 2, 7), dt(2008, 11, 2), dt(2008, 11, 2, 11, 7),
                   dt(2008, 11, 2, 11, 8), dt(2008, 11, 2, 11, 9),
                   dt(2008, 11, 2, 13, 15), dt(2009, 1, 29, 14, 45)]
        )
        valid.index.name = 'jdatedeb'
        # =====================================================================
        code = 'K055001010'
        first_dt = dt(2008, 9, 1)
        last_dt = dt(2009, 2, 1)
        hydro3 = True
        # =====================================================================
        reader = Bareme(filename=self.source)
        df = reader.read_flowmes(
            first_dt=first_dt, last_dt=last_dt, code=code, hydro3=hydro3)
        assert_frame_equal(df, valid)
        # =====================================================================

    def test_read_levelcor(self):
        """
        Test de lecture des données levelcor
        """
        # =====================================================================
        valid = pnd.DataFrame({
                'valeur': [0, -30, -30, -30, -30, -30, 0,  0]
            },
            index=[dt(2014, 4, 23, 13, 40), dt(2014, 5, 22, 8, 20),
                   dt(2014, 6, 5, 8, 35), dt(2014, 6, 18, 6, 20),
                   dt(2014, 7, 1, 9, 50), dt(2014, 7, 6, 20), dt(2014, 7, 9),
                   dt(2014, 8, 4, 12)]
        )
        valid.index.name = 'ladate'
        # =====================================================================
        code = 'K055001010'
        first_dt = dt(2014, 1, 1)
        last_dt = dt(2015, 1, 1)
        hydro3 = True
        # =====================================================================
        reader = Bareme(filename=self.source)
        df = reader.read_levelcor(
            first_dt=first_dt, last_dt=last_dt, code=code, hydro3=hydro3)
        assert_frame_equal(df, valid)
        # =====================================================================

    def test_read_ratingcurve(self):
        """
        Test de lecture des données ratingcurve
        """
        # =====================================================================
        cases = {
            ('K010002010', 'H200708', True):
                {200752: {'update_dt': dt(2009, 5, 15, 0, 0),
                          'levelcor': None,
                          'code': 'K010002010',
                          'valid_dt': (dt(2007, 8, 30, 13, 10),
                                       dt(2008, 11, 2, 11, 59, 59)),
                          'valid_interval': (700, 5200),
                          'num': 'H200708'}},
            ('K0100020', 'H200708', False):
                {200752: {'update_dt': dt(2009, 5, 15, 0, 0),
                          'levelcor': None,
                          'code': 'K0100020',
                          'valid_dt': (dt(2007, 8, 30, 13, 10),
                                       dt(2008, 11, 2, 11, 59, 59)),
                          'valid_interval': (700, 5200),
                          'num': 'H200708'}},
            ('K0403010', 'H200809', False):
                {200653: {'update_dt': dt(2009, 11, 13, 15, 20),
                          'levelcor': None,
                          'code': 'K0403010',
                          'valid_dt': (dt(2008, 7, 6, 22, 40),
                                       dt(2009, 10, 21, 23, 59, 59)),
                          'valid_interval': (-50, 2800),
                          'num': 'H200809'}},
        }
        reader = Bareme(filename=self.source)
        for case in cases:
            valid = cases[case]
            content = reader.read_ratingcurve(
                code=case[0], code_rtc=case[1], hydro3=case[2])
            self.assertIsInstance(content, dict)
            for k, v in valid.items():
                self.assertIn(k, content)
                for k2, v2 in v.items():
                    self.assertIn(k2, content[k])
                    self.assertEqual(v2, content[k][k2])
        # =====================================================================
        cases = {
            ('K010002010', dt(2008, 10, 31), dt(2008, 11, 5), True):
                {200752: {'valid_interval': (700, 5200),
                          'code': 'K010002010',
                          'num': 'H200708',
                          'update_dt': dt(2009, 5, 15, 0, 0),
                          'levelcor': None,
                          'valid_dt': (dt(2007, 8, 30, 13, 10),
                                       dt(2008, 11, 2, 11, 59, 59))},
                 200758: {'valid_interval': (1050, 5210),
                          'code': 'K010002010',
                          'num': 'H200809',
                          'update_dt': None,
                          'levelcor': None,
                          'valid_dt': (dt(2008, 11, 2, 12, 0),
                                       dt(2009, 12, 1, 10, 59, 59))}},
            ('K0100020', dt(2008, 10, 31), dt(2008, 11, 5), False):
                {200752: {'valid_interval': (700, 5200),
                          'code': 'K0100020',
                          'update_dt': dt(2009, 5, 15, 0, 0),
                          'levelcor': None,
                          'num': 'H200708',
                          'valid_dt': (dt(2007, 8, 30, 13, 10),
                                       dt(2008, 11, 2, 11, 59, 59))},
                 200758: {'valid_interval': (1050, 5210),
                          'code': 'K0100020',
                          'update_dt': None,
                          'levelcor': None,
                          'num': 'H200809',
                          'valid_dt': (dt(2008, 11, 2, 12, 0),
                                       dt(2009, 12, 1, 10, 59, 59))}},
            ('K040301001', dt(2008, 10, 31), dt(2008, 11, 5), True):
                {200664: {'valid_dt': (dt(2006, 6, 1, 0, 0),
                                       dt(2010, 12, 23, 18, 44, 59)),
                          'code': 'K040301001',
                          'valid_interval': (-50, 2500),
                          'levelcor': None,
                          'update_dt': None,
                          'num': 'H200610 GC'},
                 200653: {'valid_dt': (dt(2008, 7, 6, 22, 40),
                                       dt(2009, 10, 21, 23, 59, 59)),
                          'code': 'K040301001',
                          'valid_interval': (-50, 2800),
                          'levelcor': None,
                          'update_dt': dt(2009, 11, 13, 15, 20),
                          'num': 'H200809'}},
            ('K0403010', dt(2008, 10, 31), dt(2008, 11, 5), False):
                {200664: {'num': 'H200610 GC',
                          'code': 'K0403010',
                          'update_dt': None,
                          'valid_dt': (dt(2006, 6, 1, 0, 0),
                                       dt(2010, 12, 23, 18, 44, 59)),
                          'levelcor': None,
                          'valid_interval': (-50, 2500)},
                 200653: {'num': 'H200809',
                          'code': 'K0403010',
                          'update_dt': dt(2009, 11, 13, 15, 20),
                          'valid_dt': (dt(2008, 7, 6, 22, 40),
                                       dt(2009, 10, 21, 23, 59, 59)),
                          'levelcor': None,
                          'valid_interval': (-50, 2800)}},
        }
        reader = Bareme(filename=self.source)
        for case in cases:
            valid = cases[case]
            content = reader.read_ratingcurve(code=case[0], first_dt=case[1],
                                              last_dt=case[2], hydro3=case[3])
            self.assertIsInstance(content, dict)
            for k, v in valid.items():
                self.assertIn(k, content)
                for k2, v2 in v.items():
                    self.assertIn(k2, content[k])
                    self.assertEqual(v2, content[k][k2])
        # =====================================================================

    def test_read_rtc_points(self):
        """
        Test de lecture des courbes de tarage par points
        """
        # =====================================================================
        valid = pnd.DataFrame({
                'h': [
                    1040, 1070, 1110, 1160, 1170, 1170, 1210, 1250, 1360, 1420,
                    1500, 1500, 1530, 1550, 1580, 1630, 1690, 1780, 1900, 2020,
                    2180, 2220, 2240, 2270, 2290, 2330, 2350, 2580, 2830, 3140,
                    3350, 3530, 4050, 4740, 5120, 5140, 5190, 5210],
                'q': [
                    0.41, 0.63, 0.95, 1.39, 1.49, 1.51, 2.12, 2.71, 4.97, 6.89,
                    10.00, 9.74, 12.30, 14.00, 16.60, 21.29, 27.50, 37.79,
                    53.40, 70.80, 96.69, 104.00, 107.00, 113.00, 116.00,
                    124.00, 127.00, 173.00, 229.00, 306.00, 362.00, 417.00,
                    592.00, 849.00, 1000.00, 1010.00, 1030.00, 1040.00],
            },
            index=range(1, 39)
        )
        valid.index.name = 'nopt'
        # =====================================================================
        noct = 200758
        reader = Bareme(filename=self.source)
        df = reader.read_rtc_points(noct=noct)
        assert_frame_equal(df, valid, check_less_precise=1)
        # =====================================================================

    def test_read_rtc_power(self):
        """
        Test de lecture des courbes de tarage par tronçon
        """
        # =====================================================================
        valid = pnd.DataFrame({
                'h': [
                    -118.0, -18.0, 82.0, 128.0, 228.0, 255.0, 355.0, 455.0,
                    555.0, 655.0, 755.0, 855.0, 955.0, 1055.0, 1155.0, 1255.0,
                    1355.0, 1455.0, 1555.0, 1595.0, 1695.0, 1795.0, 1895.0,
                    1995.0, 2095.0, 2195.0, 2295.0, 2395.0, 2495.0, 2595.0,
                    2695.0, 2795.0, 2895.0, 2995.0, 3095.0, 3195.0, 3295.0,
                    3395.0, 3495.0, 3595.0, 3695.0, 3795.0, 3895.0, 3995.0,
                    4095.0, 4195.0, 4295.0, 4395.0, 4495.0, 4595.0, 4695.0,
                    4795.0, 4895.0, 4995.0, 5095.0, 5195.0, 5295.0, 5395.0,
                    5495.0, 5595.0, 5695.0, 5795.0, 5895.0, 5995.0, 6095.0,
                    6195.0, 6295.0, 6395.0, 6495.0, 6595.0, 6695.0, 6795.0,
                    6895.0, 6995.0],
                'q': [
                    0.00, 0.17, 0.49, 0.67, 1.16, 1.31, 2.97, 5.09, 7.61,
                    10.50, 13.72, 17.26, 21.10, 25.21, 29.61, 34.26, 39.16,
                    44.30, 49.69, 51.90, 58.68, 65.75, 73.13, 80.79, 88.74,
                    96.97, 105.46, 114.22, 123.24, 132.52, 142.04, 151.81,
                    161.82, 172.07, 182.56, 193.28, 204.22, 215.39, 226.78,
                    238.38, 250.21, 262.25, 274.50, 286.95, 299.62, 312.48,
                    325.55, 338.82, 352.29, 365.95, 379.81, 393.86, 408.10,
                    422.53, 437.15, 451.95, 466.93, 482.10, 497.45, 512.98,
                    528.69, 544.58, 560.64, 576.87, 593.28, 609.86, 626.61,
                    643.53, 660.62, 677.88, 695.30, 712.89, 730.65, 748.56],
            },
            index=range(1, 75)
        )
        valid.index.name = 'nopt'
        # =====================================================================
        noct = 200664
        delta = 100
        reader = Bareme(filename=self.source)
        df = reader.read_rtc_power(noct=noct, delta=delta)
        assert_frame_equal(df, valid, check_less_precise=1)
        # =====================================================================
