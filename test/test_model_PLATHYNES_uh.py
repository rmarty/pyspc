#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for ETP by Oudin in pyspc.model.oudin

To run all tests just type:
    python -m unittest test_model_PLATHYNES_uh

To run only a class test:
    python -m unittest test_model_PLATHYNES_uh.TestMohysUH

To run only a specific test:
    python -m unittest test_model_PLATHYNES_uh.TestMohysUH.test_FBN

"""
# Imports
import filecmp
import os.path
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.model.plathynes import unit_hydrograph as uh


class TestPlathynesUH(unittest.TestCase):
    """
    Functions class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """

    def test_plot(self):
        """
        Test Unit Hydrograph - Constructeur AVEC normalisation
        """
        # ===================================================================
        ts = 60
        tb = 840
        c = 0
        params = {
            'FBN': [1.5, 0.3],
            'FPU': [5],
            'RE1': [],
            'TG1': [-4],
        }
        df = uh.build(ts=ts, tb=tb, c=c, params=params)
        # ===================================================================
        f = uh.plot(df, filename=os.path.join('data', 'plathynes_uh.png'))
        self.assertTrue(filecmp.cmp(
            os.path.join('data', 'model', 'plathynes', os.path.basename(f)),
            f
        ))
        os.remove(f)
        # ===================================================================
        f = uh.plot(df, subplots=True,
                    filename=os.path.join('data', 'plathynes_uh_subplots.png'))
        self.assertTrue(filecmp.cmp(
            os.path.join('data', 'model', 'plathynes', os.path.basename(f)),
            f
        ))
        os.remove(f)
        # ===================================================================

    def test_build_c0(self):
        """
        Test Unit Hydrograph - Constructeur Centré
        """
        # ===================================================================
        ts = 60
        tb = 840
        c = 0
        params = {
            'FBN': [1.5, 0.3],
            'FPU': [5],
            'RE1': [],
            'TG1': [-4],
        }
        valid = pnd.DataFrame(
            {'FBN': [0.066394, 0.118480, 0.137048, 0.135565, 0.122396,
                     0.104168, 0.085180, 0.067690, 0.052580, 0.039958,
                     0.029564, 0.020973, 0.013644, 0.006360],
             'FPU': [0.000148747, 0.002379960, 0.012048551, 0.038079371,
                     0.092967216, 0.192776819, 0.357142857, 0.192776819,
                     0.092967216, 0.038079371, 0.012048551, 0.002379960,
                     0.000148747, 0.0],
             'RE1': [1/14]*14,
             'TG1': [0.047619, 0.095238, 0.142857, 0.129870, 0.116883,
                     0.103896, 0.090909, 0.077922, 0.064935, 0.051948,
                     0.038961, 0.025974, 0.012987, 0.000000]},
            index=list(range(-6, 8))
        )
        # ===================================================================
        df = uh.build(ts=ts, tb=tb, c=c, params=params)
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(valid, df, check_less_precise=3)
        # ===================================================================

    def test_build_c(self):
        """
        Test Unit Hydrograph - Constructeur Décentré
        """
        # ===================================================================
        ts = 60
        tb = 840
        c = -120
        params = {
            'FBN': [1.5, 0.3],
            'FPU': [5],
            'RE1': [],
            'TG1': [-4],
        }
        valid = pnd.DataFrame(
            {'FBN': [0.066394, 0.118480, 0.137048, 0.135565, 0.122396,
                     0.104168, 0.085180, 0.067690, 0.052580, 0.039958,
                     0.029564, 0.020973, 0.013644, 0.006360],
             'FPU': [0.000148747, 0.002379960, 0.012048551, 0.038079371,
                     0.092967216, 0.192776819, 0.357142857, 0.192776819,
                     0.092967216, 0.038079371, 0.012048551, 0.002379960,
                     0.000148747, 0.0],
             'RE1': [1/14]*14,
             'TG1': [0.047619, 0.095238, 0.142857, 0.129870, 0.116883,
                     0.103896, 0.090909, 0.077922, 0.064935, 0.051948,
                     0.038961, 0.025974, 0.012987, 0.000000]},
            index=list(range(-8, 6))
        )
        # ===================================================================
        df = uh.build(ts=ts, tb=tb, c=c, params=params)
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(valid, df, check_less_precise=3)
        # ===================================================================

    def test_build_None(self):
        """
        Test Unit Hydrograph - Constructeur Décentré
        """
        # ===================================================================
        ts = 60
        tb = 840
        c = -120
        params = None
        valid = pnd.DataFrame(
            {'FBN': [0.07142857142857142, 0.07142857142857142,
                     0.07142857142857142, 0.07142857142857142,
                     0.07142857142857145, 0.0714285714285714,
                     0.07142857142857145, 0.0714285714285714,
                     0.07142857142857151, 0.0714285714285714,
                     0.0714285714285714, 0.0714285714285714,
                     0.07142857142857151, 0.0714285714285714],
             'FPU': [0.07142857142857142, 0.07142857142857142,
                     0.07142857142857142, 0.07142857142857142,
                     0.07142857142857142, 0.07142857142857142,
                     0.07142857142857142, 0.07142857142857142,
                     0.07142857142857142, 0.07142857142857142,
                     0.07142857142857142, 0.07142857142857142,
                     0.07142857142857142, 0.07142857142857142],
             'RE1': [1/14]*14,
             'TG1': [0.02040816326530612, 0.04081632653061224,
                     0.061224489795918366, 0.08163265306122448,
                     0.10204081632653061, 0.12244897959183673,
                     0.14285714285714285, 0.12244897959183673,
                     0.10204081632653061, 0.08163265306122448,
                     0.061224489795918366, 0.04081632653061224,
                     0.02040816326530612, 0.0]},
            index=list(range(-8, 6))
        )
        # ===================================================================
        df = uh.build(ts=ts, tb=tb, c=c)
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(valid, df, check_less_precise=3)
        # ===================================================================
        df = uh.build(ts=ts, tb=tb, c=c, params=params)
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(valid, df, check_less_precise=3)
        # ===================================================================

    def test_FBN(self):
        """
        Test Unit Hydrograph - FBN
        """
        # ===================================================================
        alpha = 1.5
        kappa = 0.3
        valid = pnd.DataFrame(
            {'FBN': [0.066394, 0.118480, 0.137048, 0.135565, 0.122396,
                     0.104168, 0.085180, 0.067690, 0.052580, 0.039958,
                     0.029564, 0.020973, 0.013644, 0.006360]},
            index=list(range(1, 15))
        )
        # ===================================================================
        df = uh.FBN(valid.index, alpha, kappa)
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(valid, df, check_less_precise=3)
        # ===================================================================

    def test_FPU(self):
        """
        Test Unit Hydrograph - FPU
        """
        # ===================================================================
        alpha = 5
        valid = pnd.DataFrame(
            {'FPU': [0.000148747, 0.002379960, 0.012048551, 0.038079371,
                     0.092967216, 0.192776819, 0.357142857, 0.192776819,
                     0.092967216, 0.038079371, 0.012048551, 0.002379960,
                     0.000148747, 0.0]},
            index=list(range(1, 15))
        )
        # ===================================================================
        df = uh.FPU(valid.index, alpha)
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(valid, df, check_less_precise=3)
        # ===================================================================

    def test_RE1(self):
        """
        Test Unit Hydrograph - RE1
        """
        # ===================================================================
        valid = pnd.DataFrame(
            {'RE1': [1/14]*14},
            index=list(range(1, 15))
        )
        # ===================================================================
        df = uh.RE1(valid.index)
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(valid, df, check_less_precise=3)
        # ===================================================================

    def test_TG1_d0(self):
        """
        Test Unit Hydrograph - TG1 avec D nul
        """
        # ===================================================================
        valid = pnd.DataFrame(
            {'TG1': [0.020408, 0.040816, 0.061224, 0.081633, 0.102041,
                     0.122449, 0.142857, 0.122449, 0.102041, 0.081633,
                     0.061224, 0.040816, 0.020408, 0.000000]},
            index=list(range(1, 15))
        )
        # ===================================================================
        df = uh.TG1(valid.index)
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(valid, df, check_less_precise=3)
        # ===================================================================
        df = uh.TG1(valid.index, d=0)
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(valid, df, check_less_precise=3)
        # ===================================================================

    def test_TG1_d(self):
        """
        Test Unit Hydrograph - TG1 avec D non nul
        """
        # ===================================================================
        d = -4
        valid = pnd.DataFrame(
            {'TG1': [0.047619, 0.095238, 0.142857, 0.129870, 0.116883,
                     0.103896, 0.090909, 0.077922, 0.064935, 0.051948,
                     0.038961, 0.025974, 0.012987, 0.000000]},
            index=list(range(1, 15))
        )
        # ===================================================================
        df = uh.TG1(valid.index, d=d)
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(valid, df, check_less_precise=3)
        # ===================================================================
