#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Vol in pyspc.model.mordor

To run all tests just type:
    python -m unittest test_model_MORDOR_Vol

To run only a class test:
    python -m unittest test_model_MORDOR_Vol.TestMORDOR_Vol

To run only a specific test:
    python -m unittest test_model_MORDOR_Vol.TestMORDOR_Vol.test_init

"""
# Imports
from datetime import datetime as dt
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.model.mordor import Vol


class TestMORDOR_Vol(unittest.TestCase):
    """
    Vol class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'mordor')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'volumes_BASSIN_20220623-20220623-20221231_CEP.txt')
        reader = Vol(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.station, 'BASSIN')
        self.assertEqual(reader.varname, 'V')
        self.assertEqual(reader.runtime, dt(2022, 6, 23))
        self.assertEqual(reader.start, dt(2022, 6, 23))
        self.assertEqual(reader.end, dt(2022, 12, 31))
        self.assertEqual(reader.model, 'MORDOR')
        self.assertEqual(reader.meteo, 'CEP')
        self.assertEqual(reader.datatype, 'volumes')
        # =====================================================================

    def test_read(self):
        """
        Test de la lecture du fichier
        """
        # =====================================================================
        valid = pnd.DataFrame(
            {
                50: [11.100672, 317.6479, 193.68461, 0.00000, 121.12589,
                     51.42182, 23.479200, 58.162752, 1.131840, 115.06320,
                     0.00000, 1.401408, 0.00000, 6.099840, 209.94163, 0.000000,
                     8.500032, 1.734048, 42.158880, 0.000000, 0.586656,
                     0.000000, 0.320544, 12.214368, 6.802272, 30.981312,
                     0.247968, 1.905120, 77.76605, 0.00000, 86.49331, 4.948992,
                     0.00000, 0.00000, 0.000000, 0.006912, 32.158080, 429.0598,
                     164.56176, 60.39446, 78.17472, 111.59510, 8.187264,
                     27.467424, 5.810400, 127.29658, 1.069632, 90.67939,
                     142.14010, 4.574016, 19.140192, 5.945184, 21.542112,
                     0.000000, 0.000000, 121.24598, 16.816896, 97.47475,
                     0.00000, 0.00000, 35.657280, 40.836096, 10.269504,
                     7.840800, 8.500032, 0.000000, 0.00000, 111.22790,
                     91.30579, 0.000000, 315.7229, 198.32429, 120.12883],
                55: [26.546400, 373.2947, 237.48768, 0.00000, 151.98970,
                     65.51453, 32.643648, 96.142464, 4.018464, 154.48925,
                     0.00000, 5.322240, 0.00000, 12.064032, 258.72826,
                     0.000000, 21.002112, 7.033824, 61.945344, 0.000000,
                     2.277504, 0.021600, 3.245184, 23.927616, 13.171680,
                     44.841600, 1.880064, 4.696704, 99.78941, 0.00000,
                     118.73088, 12.870144, 0.00000, 0.00000, 0.000000,
                     1.588032, 45.404064, 496.7965, 201.54960, 81.47952,
                     100.99210, 149.29229, 18.139680, 40.747968, 12.203136,
                     154.20586, 5.645376, 114.58368, 184.68000, 11.364192,
                     29.390688, 13.436064, 38.238912, 0.654912, 0.781920,
                     155.21760, 27.881280, 131.99414, 0.00000, 0.00000,
                     61.218720, 63.834048, 18.482688, 14.084928, 19.858176,
                     0.640224, 0.00000, 141.83510, 121.13366, 0.167616,
                     367.5707, 241.10611, 148.01184],
                60: [50.848992, 430.1346, 284.44090, 0.40176, 185.29862,
                     83.00966, 42.987456, 140.725728, 8.662464, 198.40637,
                     0.40176, 12.412224, 0.40176, 19.606752, 311.42189,
                     1.698624, 41.545440, 15.136416, 85.548960, 0.447552,
                     5.679072, 1.889568, 8.163936, 39.923712, 21.924000,
                     62.093952, 7.271424, 8.947584, 124.09805, 0.40176,
                     156.29587, 24.245568, 0.40176, 0.40176, 0.484704,
                     5.404320, 63.350208, 566.2241, 241.70918, 104.82394,
                     124.91971, 191.10384, 35.957088, 56.788128, 23.258016,
                     182.79302, 12.823488, 139.66128, 229.04208, 21.384000,
                     42.293664, 22.794912, 59.278176, 4.859136, 3.872448,
                     191.51597, 40.468032, 172.60733, 0.40176, 0.40176,
                     90.307008, 90.232704, 29.558304, 23.221728, 36.990432,
                     3.259872, 0.40176, 173.94653, 154.22141, 3.646944,
                     421.0350, 287.55734, 177.21072],
            },
            index=['Q1948', 'Q1949', 'Q1950', 'Q1951', 'Q1952', 'Q1953',
                   'Q1954', 'Q1955', 'Q1956', 'Q1957', 'Q1958', 'Q1959',
                   'Q1960', 'Q1961', 'Q1962', 'Q1963', 'Q1964', 'Q1965',
                   'Q1966', 'Q1967', 'Q1968', 'Q1969', 'Q1970', 'Q1971',
                   'Q1972', 'Q1973', 'Q1974', 'Q1975', 'Q1976', 'Q1977',
                   'Q1978', 'Q1979', 'Q1980', 'Q1981', 'Q1982', 'Q1983',
                   'Q1984', 'Q1985', 'Q1986', 'Q1987', 'Q1988', 'Q1989',
                   'Q1990', 'Q1991', 'Q1992', 'Q1993', 'Q1994', 'Q1995',
                   'Q1996', 'Q1997', 'Q1998', 'Q1999', 'Q2000', 'Q2001',
                   'Q2002', 'Q2003', 'Q2004', 'Q2005', 'Q2006', 'Q2007',
                   'Q2008', 'Q2009', 'Q2010', 'Q2011', 'Q2012', 'Q2013',
                   'Q2014', 'Q2015', 'Q2016', 'Q2017', 'Q2018', 'Q2019',
                   'Q2020']
        )
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'volumes_BASSIN_20220623-20220623-20221231_CEP.txt')
        reader = Vol(filename=filename)
        df = reader.read()
        assert_frame_equal(valid, df)
        # =====================================================================
