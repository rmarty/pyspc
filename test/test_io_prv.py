#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_prv in pyspc.io.prv

To run all tests just type:
    python -m unittest test_io_PRV

To run only a class test:
    python -m unittest test_io_PRV.TestPRV

To run only a specific test:
    python -m unittest test_io_PRV.TestPRV.test_init

"""
# Imports
from datetime import datetime as dt
import os.path
import unittest

from pyspc.core.series import Series
from pyspc.io.prv import read_prv


class TestPRV(unittest.TestCase):
    """
    PRV_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.otamin = os.path.join('data', 'model', 'otamin16')
        self.scores = os.path.join('data', 'verification', 'scores')

    def test_otamin(self):
        """
        Test des méthodes de lecture - otamin
        """
        # =====================================================================
        # CAS : PREVISION OTAMIN
        # ---------------------------------------------------------------------
        datatype = 'otamin16_fcst'
        filename = os.path.join(self.otamin, 'GRP_B_20200911_1515_2.prv')
        series = read_prv(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 3)
        self.assertIn(
            ('A6701210', 'QH',
             (dt(2020, 2, 3, 12), '57gGRPd000', '2001', None)), series)
        self.assertIn(
            ('A6701210', 'QH',
             (dt(2020, 2, 3, 12), '57gGRPd000', '2002', None)), series)
        self.assertIn(
            ('A6701210', 'QH',
             (dt(2020, 2, 3, 12), '57gGRPd000', '2003', None)), series)
        # =====================================================================
        # CAS : PREVISION OTAMIN - CAS RR,TA
        # ---------------------------------------------------------------------
        datatype = 'otamin16_fcst'
        filename = os.path.join(self.otamin, 'GRP_B_20200911_1515_DA_2.prv')
        series = read_prv(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 6)
        self.assertIn(
            ('A6701210', 'PH',
             (dt(2020, 2, 3, 12), '57gGRPd000', '2001', None)), series)
        self.assertIn(
            ('A6701210', 'PH',
             (dt(2020, 2, 3, 12), '57gGRPd000', '2002', None)), series)
        self.assertIn(
            ('A6701210', 'PH',
             (dt(2020, 2, 3, 12), '57gGRPd000', '2003', None)), series)
        self.assertIn(
            ('A6701210', 'TH',
             (dt(2020, 2, 3, 12), '57gGRPd000', '2001', None)), series)
        self.assertIn(
            ('A6701210', 'TH',
             (dt(2020, 2, 3, 12), '57gGRPd000', '2002', None)), series)
        self.assertIn(
            ('A6701210', 'TH',
             (dt(2020, 2, 3, 12), '57gGRPd000', '2003', None)), series)
        # =====================================================================
        # CAS : CAS TENDANCE OTAMIN
        # ---------------------------------------------------------------------
        datatype = 'otamin16_trend'
        filename = os.path.join(self.otamin, 'GRP_B_20200203_1200_2.prv')
        series = read_prv(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 4)
        self.assertIn(
            ('A6701210', 'QH',
             (dt(2020, 2, 3, 12), '57gGRPd000', '2001', '-1')), series)
        self.assertIn(
            ('A6701210', 'QH',
             (dt(2020, 2, 3, 12), '57gGRPp000', '2001', '10')), series)
        self.assertIn(
            ('A6701210', 'QH',
             (dt(2020, 2, 3, 12), '57gGRPp000', '2001', '50')), series)
        self.assertIn(
            ('A6701210', 'QH',
             (dt(2020, 2, 3, 12), '57gGRPp000', '2001', '90')), series)
        # =====================================================================

    def test_scores(self):
        """
        Test des méthodes de lecture - scores
        """
        # =====================================================================
        # CAS : OBSERVATION
        # ---------------------------------------------------------------------
        datatype = 'scores_obs'
        filename = os.path.join(self.scores, 'K6373020_Q.csv')
        series = read_prv(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('K6373020', 'QH', None), series)
        # =====================================================================
        # CAS : SIMULATION
        # ---------------------------------------------------------------------
        datatype = 'scores_sim'
        filename = os.path.join(self.scores, 'K6373020_Q_sim.csv')
        series = read_prv(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('K6373020', 'QH', 'sim'), series)
        # =====================================================================
        # CAS : PREVISION SCORES
        # ---------------------------------------------------------------------
        datatype = 'scores_fcst'
        filename = os.path.join(self.scores, 'K6373020_Q_9.csv')
        series = read_prv(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(
            ('K6373020', 'QH',
             (dt(2016, 5, 31, 12), '45hEAOtt00', '2007', '50')), series)
        # =====================================================================
