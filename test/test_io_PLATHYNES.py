#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_Plathynes in pyspc.io.plathynes

To run all tests just type:
    python -m unittest test_io_PLATHYNES

To run only a class test:
    python -m unittest test_io_PLATHYNES.TestPLATHYNES

To run only a specific test:
    python -m unittest test_io_PLATHYNES.TestPLATHYNES.test_data

"""
# Imports
from datetime import datetime as dt, timedelta as td
import os.path
import unittest

from pyspc.core.config import Config
from pyspc.core.location import Location, Locations
from pyspc.core.series import Series
from pyspc.io.plathynes import read_Plathynes


class TestPLATHYNES(unittest.TestCase):
    """
    PRV_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'plathynes')

    def test_data(self):
        """
        Test des DATA de PLATHYNES
        """
        # =====================================================================
        datatype = 'data'
        # =====================================================================
        # Données de précipitations
        # ---------------------------------------------------------------------
        filename = os.path.join(self.dirname, '8001_RRobs.mgr')
        series = read_Plathynes(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 3)
        self.assertIn(('LE-PUY-CHADRAC', 'PH', None), series)
        self.assertIn(('MAZAN-ABBAYE-RAD', 'PH', None), series)
        self.assertIn(('CAYRES', 'PH', None), series)
        # =====================================================================
        # Données de débits
        # ---------------------------------------------------------------------
        filename = os.path.join(self.dirname, '8001_1.mqo')
        series = read_Plathynes(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('LaLoireChadrac', 'QH', None), series)
        # =====================================================================
        # Données de débits injectés
        # ---------------------------------------------------------------------
        filename = os.path.join(self.dirname, '8001_1.mqi')
        series = read_Plathynes(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('Pont-la-Borie', 'QH', None), series)
        # =====================================================================

    def test_export(self):
        """
        Test des EXPORTS de PLATHYNES
        """
        # =====================================================================
        datatype = 'export'
        filename = os.path.join(self.dirname, 'plathynes_export_1.txt')
        series = read_Plathynes(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 10)
        self.assertIn(('LaLoireChadrac', 'PH', '2008-11'), series)
        self.assertIn(('LaLoireChadrac', 'QH', '2008-11'), series)
        self.assertIn(('LaLoireChadrac', 'QH', '2008-11-sim'), series)
        self.assertIn(('LaLoireChadrac', 'PH', '2017-06'), series)
        self.assertIn(('LaLoireChadrac', 'QH', '2017-06'), series)
        self.assertIn(('LaLoireChadrac', 'QH', '2017-06-sim'), series)
        self.assertIn(('Goudet', 'PH', '2008-11'), series)
        self.assertIn(('Goudet', 'QH', '2008-11-sim'), series)
        self.assertIn(('Goudet', 'PH', '2017-06'), series)
        self.assertIn(('Goudet', 'QH', '2017-06-sim'), series)
        # =====================================================================

    def test_results(self):
        """
        Test des RESULTS de PLATHYNES
        """
        # =====================================================================
        datatype = 'results'
        filename = os.path.join(self.dirname, '8001_ResultsRaw.txt')
        series = read_Plathynes(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(('LaLoireChadrac', 'PH', '8001'), series)
        self.assertIn(('LaLoireChadrac', 'QH', '8001'), series)
        # =====================================================================

    def test_event(self):
        """
        Test des EVENT de PLATHYNES
        """
        # =====================================================================
        valid = {
            '8001': {
                'name': '8001',
                'desc': '',
                'datatype': 'event',
                'first_dt': dt(2019, 10, 16, 8),
                'last_dt': dt(2019, 10, 26, 8),
                'input_ts': td(hours=1),
                'model_ts': td(hours=1),
                'output_ts': td(hours=1),
                'balance_ts': td(hours=1),
                'rrobs_filenames': ['Ev_8001\\8001_RRobs.mgr'],
                'qinj_filenames': ['Ev_8001\\8001_1.mqi'],
                'qobs_filenames': ['Ev_8001\\8001_1.mqo'],
                'parameters_evenementiels': {
                    'Q0_moy_Chadrac': ['U', '98.9', '1', 'Q0_moy_Chadrac']}
            }
        }
        # =====================================================================
        datatype = 'event'
        filename = os.path.join(self.dirname, 'plathynes_event.evt')
        event = read_Plathynes(filename=filename, datatype=datatype)
        self.assertIsInstance(event, Config)
        self.assertDictEqual(event, valid)
        # =====================================================================

    def test_project(self):
        """
        Test des PROJETS de PLATHYNES
        """
        # =====================================================================
        valid = {
            'project': {
                'version': '1.8.1',
                'name': 'Chadrac_TR',
                'root': 'C:/Plathynes/PROJET_TR',
                'basin': 'Chadrac_TR',
                'datatype': 'project',
                'tables': ['Sol_12_Classes.txt', 'Ods_12_Classes.txt',
                           'Ods_CorineLandCover.txt'],
                'figures': [],
                'structures': [],
                'stations_rr': {'LE_PUY_CHADRAC': ['LE_PUY_CHADRAC',
                                                   '43157008',
                                                   '722740.000000',
                                                   '2007270.000000'],
                                'LANDOS_CHARBON': ['LANDOS_CHARBON',
                                                   '43111002',
                                                   '719124.000000',
                                                   '1985280.000000'],
                                'LES_ESTABLES_SAPC': ['LES_ESTABLES_SAPC',
                                                      '43091005',
                                                      '744104.000000',
                                                      '1990535.000000'],
                                'MAZAN_ABBAYE_RAD': ['MAZAN_ABBAYE_RAD',
                                                     '07154005',
                                                     '738420.000000',
                                                     '1971872.000000'],
                                'FIX_ST_GENEYS': ['FIX_ST_GENEYS',
                                                  '43095001',
                                                  '704457.000000',
                                                  '2017020.000000'],
                                'LE_PUY_LOUDES': ['LE_PUY_LOUDES',
                                                  '43062001',
                                                  '712389.000000',
                                                  '2009227.000000'],
                                'LE_MONASTIER_SUR_GAZEILLE': [
                                    'LE_MONASTIER_SUR_GAZEILLE',
                                    '43135003',
                                    '730405.000000',
                                    '1995324.000000'],
                                'CAYRES': ['CAYRES',
                                           '43042002',
                                           '716010.000000',
                                           '1992118.000000'],
                                'GOUDET2': ['GOUDET2',
                                            '43101002',
                                            '725249.000000',
                                            '1988805.000000'],
                                'MACHABERT': ['MACHABERT',
                                              '43186003',
                                              '740559.000000',
                                              '1999403.000000'],
                                'LE_LAC_D_ISSARLES': ['LE_LAC_D_ISSARLES',
                                                      '07119002',
                                                      '736900.000000',
                                                      '1981781.000000'],
                                'SAINTE_EULALIE': ['SAINTE_EULALIE',
                                                   '07235005',
                                                   '746566.000000',
                                                   '1980608.000000'],
                                'Fix_Saint_Geneys_SPC': [
                                    'Fix_Saint_Geneys_SPC',
                                    '43095003',
                                    '704500.000000',
                                    '2017019.000000'],
                                'FELINES_SAPC': ['FELINES_SAPC',
                                                 '43093001',
                                                 '710301.000000',
                                                 '2030537.000000'],
                                'Lanarce_SPC': ['Lanarce_SPC',
                                                '07130003',
                                                '732451.625000',
                                                '1970793.875000']},
                'stations_q': {'LaLoireChadrac': ['LaLoireChadrac',
                                                  'K0260010',
                                                  '723552.000000',
                                                  '2008719.000000',
                                                  '1.000000'],
                               'Pont_la_Borie': ['Pont_la_Borie',
                                                 'K0030020',
                                                 '735667.000000',
                                                 '1981483.000000',
                                                 '227.000000']},
                'baseflows': {'LaLoireChadrac': ['LaLoireChadrac',
                                                 '1',
                                                 'FIRST_OBS',
                                                 'LaLoireChadrac',
                                                 '1.000000']},
                'configurations': {'_SCS_2LR_classique': ['_SCS_2LR_classique',
                                                          'ACTIVE']},
                'events': {'8001': ['8001', '-1', '0', '0.0', '1'],
                           '8002': ['8002', '-1', '0', '0.0', '1'],
                           '8003': ['8003', '-1', '0', '0.0', '1'],
                           '8004': ['8004', '-1', '0', '0.0', '1'],
                           '8005': ['8005', '-1', '0', '0.0', '1'],
                           '8006': ['8006', '-1', '0', '0.0', '1'],
                           '8007': ['8007', '-1', '0', '0.0', '1'],
                           '8008': ['8008', '-1', '0', '0.0', '1'],
                           '8009': ['8009', '-1', '0', '0.0', '1'],
                           '8010': ['8010', '-1', '0', '0.0', '1']},
                'simulations': ['SIM']
            }
        }
        # =====================================================================
        datatype = 'project'
        filename = os.path.join(self.dirname, 'plathynes_project.prj')
        project = read_Plathynes(filename=filename, datatype=datatype)
        self.assertIsInstance(project, Config)
        self.assertDictEqual(project, valid)
        # =====================================================================

    def test_project_locations(self):
        """
        Test des PROJETS de PLATHYNES
        """
        # =====================================================================
        valid = {
            '43157008': {'code': '43157008',
                         'name': 'LE_PUY_CHADRAC',
                         'loctype': 'point',
                         'x': 722740.0,
                         'y': 2007270.0},
            '43111002': {'code': '43111002',
                         'name': 'LANDOS_CHARBON',
                         'loctype': 'point',
                         'x': 719124.0,
                         'y': 1985280.0},
            '43091005': {'code': '43091005',
                         'name': 'LES_ESTABLES_SAPC',
                         'loctype': 'point',
                         'x': 744104.0,
                         'y': 1990535.0},
            '07154005': {'code': '07154005',
                         'name': 'MAZAN_ABBAYE_RAD',
                         'loctype': 'point',
                         'x': 738420.0,
                         'y': 1971872.0},
            '43095001': {'code': '43095001',
                         'name': 'FIX_ST_GENEYS',
                         'loctype': 'point',
                         'x': 704457.0,
                         'y': 2017020.0},
            '43062001': {'code': '43062001',
                         'name': 'LE_PUY_LOUDES',
                         'loctype': 'point',
                         'x': 712389.0,
                         'y': 2009227.0},
            '43135003': {'code': '43135003',
                         'name': 'LE_MONASTIER_SUR_GAZEILLE',
                         'loctype': 'point',
                         'x': 730405.0,
                         'y': 1995324.0},
            '43042002': {'code': '43042002',
                         'name': 'CAYRES',
                         'loctype': 'point',
                         'x': 716010.0,
                         'y': 1992118.0},
            '43101002': {'code': '43101002',
                         'name': 'GOUDET2',
                         'loctype': 'point',
                         'x': 725249.0,
                         'y': 1988805.0},
            '43186003': {'code': '43186003',
                         'name': 'MACHABERT',
                         'loctype': 'point',
                         'x': 740559.0,
                         'y': 1999403.0},
            '07119002': {'code': '07119002',
                         'name': 'LE_LAC_D_ISSARLES',
                         'loctype': 'point',
                         'x': 736900.0,
                         'y': 1981781.0},
            '07235005': {'code': '07235005',
                         'name': 'SAINTE_EULALIE',
                         'loctype': 'point',
                         'x': 746566.0,
                         'y': 1980608.0},
            '43095003': {'code': '43095003',
                         'name': 'Fix_Saint_Geneys_SPC',
                         'loctype': 'point',
                         'x': 704500.0,
                         'y': 2017019.0},
            '43093001': {'code': '43093001',
                         'name': 'FELINES_SAPC',
                         'loctype': 'point',
                         'x': 710301.0,
                         'y': 2030537.0},
            '07130003': {'code': '07130003',
                         'name': 'Lanarce_SPC',
                         'loctype': 'point',
                         'x': 732451.625,
                         'y': 1970793.875},
            'K0260010': {'code': 'K0260010',
                         'name': 'LaLoireChadrac',
                         'loctype': 'basin',
                         'x': 723552.0,
                         'y': 2008719.0},
            'K0030020': {'code': 'K0030020',
                         'name': 'Pont_la_Borie',
                         'loctype': 'basin',
                         'x': 735667.0,
                         'y': 1981483.0},
        }
        # =====================================================================
        datatype = 'project'
        filename = os.path.join(self.dirname, 'plathynes_project.prj')
        locs = read_Plathynes(filename=filename, datatype=datatype, asloc=True)
        self.assertIsInstance(locs, Locations)
        self.assertEqual(locs.name, 'plathynes_project')
        for k, v in valid.items():
            self.assertIn(k, locs)
            self.assertIsInstance(locs[k], Location)
            self.assertEqual(locs[k].code, v['code'])
            self.assertEqual(locs[k].name, v['name'])
            self.assertEqual(locs[k].loctype, v['loctype'])
            self.assertEqual(locs[k].x, v['x'])
            self.assertEqual(locs[k].y, v['y'])
        # =====================================================================
