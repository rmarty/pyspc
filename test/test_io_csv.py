#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_csv and read_xls in pyspc.io.csv

To run all tests just type:
    python -m unittest test_io_csv

To run only a class test:
    python -m unittest test_io_csv.TestCSVXLS

To run only a specific test:
    python -m unittest test_io_csv.TestCSVXLS.test_init

"""
# Imports
from datetime import datetime as dt
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.core.series import Series
from pyspc.io.csv import read_csv, read_xls


class TestCSVXLS(unittest.TestCase):
    """
    CSVXLS_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'io', 'csv')
        self.dfs = pnd.DataFrame({
                ('K0403010', 'QH'): [
                    12.1, 17.0, 19.6, 22.2, 24.8, 29.9, 35.6, 41.2, 46.9, 62.9,
                    85.7, 106.0, 118.0, 115.0, 103.0, 92.5, 86.4, 82.3, 78.3],
                ('43091005', 'PH'): [
                    8.9, 5.9, 9.5, 6.7, 6.0, 9.1, 9.3, 12.6, 15.7, 12.5, 8.5,
                    1.6, 3.9, 3.2, 0.4, 2.0, 2.2, 0.8, 1.1],
                ('43091005', 'TH'): [
                    8.9, 9.0, 8.8, 8.8, 8.7, 8.5, 8.4, 7.7, 8.1, 8.4, 7.9, 7.2,
                    6.6, 7.0, 7.2, 6.9, 6.6, 7.3, 7.1],
                ('43130002', 'PH'): [
                    7.7, 4.0, 5.4, 4.4, 6.1, 4.8, 6.5, 3.6, 8.3, 7.6, 3.8, 2.3,
                    3.8, 4.6, 0.0, 1.8, 2.0, 2.4, 1.5],
                ('43130002', 'TH'): [
                    9.7, 9.7, 9.1, 9.5, 9.5, 9.8, 9.4, 9.5, 9.2, 9.2, 8.6, 8.2,
                    8.3, 8.0, 8.5, 8.2, 8.1, 8.0, 8.2],
            }, index=pnd.date_range(
                dt(2014, 11, 3, 18), dt(2014, 11, 4, 12), freq='H')
        )

    def test_csv(self):
        """
        Test fichier csv
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'event.csv')
        datatype = 'obs'
        series = read_csv(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 5)
        self.assertIn(('K0403010', 'QH', None), series)
        self.assertIn(('43091005', 'PH', None), series)
        self.assertIn(('43130002', 'PH', None), series)
        self.assertIn(('43091005', 'TH', None), series)
        self.assertIn(('43130002', 'TH', None), series)
        for k in series:
            df = self.dfs[(k[0], k[1])].to_frame()
            df.columns = df.columns.to_flat_index()
            assert_frame_equal(series[k].data_frame, df)
        # =====================================================================
        with self.assertRaises(ValueError):
            read_csv(filename=filename, datatype='fcst')
        # =====================================================================
        filename = os.path.join(self.dirname, 'forecast.csv')
        datatype = 'fcst'
        series = read_csv(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 5)
        self.assertIn(
            ('K0403010', 'QH', (dt(2014, 11, 3, 18), 'spc', None, None)),
            series)
        self.assertIn(
            ('43091005', 'PH', (dt(2014, 11, 3, 18), 'spc', None, None)),
            series)
        self.assertIn(
            ('43130002', 'PH', (dt(2014, 11, 3, 18), 'spc', None, None)),
            series)
        self.assertIn(
            ('43091005', 'TH', (dt(2014, 11, 3, 18), 'spc', None, None)),
            series)
        self.assertIn(
            ('43130002', 'TH', (dt(2014, 11, 3, 18), 'spc', None, None)),
            series)
        # =====================================================================
        with self.assertRaises(ValueError):
            read_csv(filename=filename, datatype='obs')
        # =====================================================================

    def test_xls(self):
        """
        Test fichier xls
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'event.xls')
        datatype = 'obs'
        sheetname = 'sheet_event'
        series = read_xls(
            filename=filename, sheetname=sheetname, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 5)
        self.assertIn(('K0403010', 'QH', None), series)
        self.assertIn(('43091005', 'PH', None), series)
        self.assertIn(('43130002', 'PH', None), series)
        self.assertIn(('43091005', 'TH', None), series)
        self.assertIn(('43130002', 'TH', None), series)
        for k in series:
            df = self.dfs[(k[0], k[1])].to_frame()
            df.columns = df.columns.to_flat_index()
            assert_frame_equal(series[k].data_frame, df)
        # =====================================================================
        with self.assertRaises(ValueError):
            read_xls(filename=filename, sheetname=sheetname, datatype='fcst')
        # =====================================================================
        filename = os.path.join(self.dirname, 'forecast.xls')
        datatype = 'fcst'
        sheetname = 'sheet_fcst'
        series = read_xls(
            filename=filename, sheetname=sheetname, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 5)
        self.assertIn(
            ('K0403010', 'QH', (dt(2014, 11, 3, 18), 'spc', None, None)),
            series)
        self.assertIn(
            ('43091005', 'PH', (dt(2014, 11, 3, 18), 'spc', None, None)),
            series)
        self.assertIn(
            ('43130002', 'PH', (dt(2014, 11, 3, 18), 'spc', None, None)),
            series)
        self.assertIn(
            ('43091005', 'TH', (dt(2014, 11, 3, 18), 'spc', None, None)),
            series)
        self.assertIn(
            ('43130002', 'TH', (dt(2014, 11, 3, 18), 'spc', None, None)),
            series)
        # =====================================================================
        with self.assertRaises(ValueError):
            read_csv(filename=filename, datatype='obs')
        # =====================================================================
