#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary csv2prv

To run all tests just type:
    python -m unittest test_bin_csv2prv

To run only a class test:
    python -m unittest test_bin_csv2prv.Test_csv2prv

To run only a specific test:
    python -m unittest test_bin_csv2prv.Test_csv2prv.test_obs

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import csv2prv as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_csv2prv(unittest.TestCase):
    """
    csv2prv bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'csv2prv')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_grp16 = os.path.join('data', 'model', 'grp16', 'cal')
        cls.dir_grp18 = os.path.join('data', 'model', 'grp18', 'cal')
        cls.dir_pyspc = os.path.join('data', 'core', 'csv')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.stations_list_file = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)

    def test_scores_obs_grp18(self):
        """
        Test série OBS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2prv.py',
            '-I', self.dir_grp18,
            '-s', 'RH10585x',
            '-O', self.dir_out,
            '-n', 'QI',
            '-C', 'grp18',
            '-t', 'scores_obs'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2prv()
        self.assertEqual(args.input_dir, self.dir_grp18)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QI')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'RH10585x')
        self.assertEqual(args.data_type, 'scores_obs')
        self.assertEqual(args.csv_type, 'grp18')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.prv'.format(args.station_name, args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion au format prv ('{t}') des données "\
            "de débits ('{n}') à la station {s} "\
            "depuis le fichier csv de type GRP v2018 ('{C}') dans le "\
            "répertoire {I} vers le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_scores_obs_pyspc(self):
        """
        Test série OBS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2prv.py',
            '-I', self.dir_pyspc,
            '-s', 'K1341810',
            '-O', self.dir_out,
            '-n', 'QI',
            '-C', 'pyspc',
            '-t', 'scores_obs'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2prv()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QI')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K1341810')
        self.assertEqual(args.data_type, 'scores_obs')
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.prv'.format(args.station_name, args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion au format prv ('{t}') des données "\
            "de débits ('{n}') à la station {s} "\
            "depuis le fichier csv de type '{C}' dans le "\
            "répertoire {I} vers le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_scores_sim_pyspc(self):
        """
        Test série OBS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2prv.py',
            '-I', self.dir_pyspc,
            '-s', 'K1321810_mohys',
            '-O', self.dir_out,
            '-n', 'QI',
            '-C', 'pyspc',
            '-t', 'scores_sim'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2prv()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QI')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K1321810_mohys')
        self.assertEqual(args.data_type, 'scores_sim')
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.prv'.format(args.station_name, args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion au format prv ('{t}') des données "\
            "de débits ('{n}') à la station {s} "\
            "depuis le fichier csv de type '{C}' dans le "\
            "répertoire {I} vers le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_scores_fcst_pyspc(self):
        """
        Test série OBS
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in,
                                               'list_fcst_scores.txt')
        processArgs = [
            'python',
            '../bin/csv2prv.py',
            '-I', self.dir_pyspc,
            '-l', self.stations_list_file,
            '-O', self.dir_out,
            '-n', 'QI',
            '-C', 'pyspc',
            '-t', 'scores_fcst'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2prv()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, 'QI')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'scores_fcst')
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'list_fcst_scores_{}.prv'.format(args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion au format prv ('{t}') des données "\
            "de débits ('{n}') des séries listées dans le fichier {l} "\
            "depuis le fichier csv de type '{C}' dans le "\
            "répertoire {I} vers le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir, C=args.csv_type,
                l=args.stations_list_file, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_otamin_fcst_pyspc(self):
        """
        Test série OBS
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in,
                                               'list_fcst_otamin.txt')
        processArgs = [
            'python',
            '../bin/csv2prv.py',
            '-I', self.dir_pyspc,
            '-l', self.stations_list_file,
            '-O', self.dir_out,
            '-n', 'QH',
            '-C', 'pyspc',
            '-t', 'otamin18_fcst'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2prv()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'otamin18_fcst')
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'list_fcst_otamin_{}.prv'.format(args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion au format prv ('{t}') des données "\
            "de débits ('{n}') des séries listées dans le fichier {l} "\
            "depuis le fichier csv de type '{C}' dans le "\
            "répertoire {I} vers le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir, C=args.csv_type,
                l=args.stations_list_file, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_otamin_trend_pyspc(self):
        """
        Test série OBS
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in,
                                               'list_trend_otamin.txt')
        processArgs = [
            'python',
            '../bin/csv2prv.py',
            '-I', self.dir_pyspc,
            '-l', self.stations_list_file,
            '-O', self.dir_out,
            '-n', 'QH',
            '-C', 'pyspc',
            '-t', 'otamin18_trend'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2prv()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'otamin18_trend')
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'list_trend_otamin_{}.prv'.format(args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion au format prv ('{t}') des données "\
            "de débits ('{n}') des séries listées dans le fichier {l} "\
            "depuis le fichier csv de type '{C}' dans le "\
            "répertoire {I} vers le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir, C=args.csv_type,
                l=args.stations_list_file, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================
