#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for MF_BP in pyspc.data.meteofrance

To run all tests just type:
    python -m unittest test_MF_BP

To run only a class test:
    python -m unittest test_MF_BP.TestMF_BP

To run only a specific test:
    python -m unittest test_MF_BP.TestMF_BP.test_init

"""
# Imports
from datetime import datetime as dt
import numpy as np
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.data.meteofrance import BP_Data, BP_value_to_interval


class TestMF_BP(unittest.TestCase):
    """
    MF_BP class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'mf')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = 'toto.txt'
        data = BP_Data(filename=filename)
        self.assertEqual(data.filename, filename)
        # =====================================================================

    def test_read_xml_2008(self):
        """
        Lecture BP XML version 2008
        """
        # =====================================================================
        valid_alerts = {
            '401': (None, None),
            '402': (dt(2008, 11, 1, 6, 3, 39), dt(2008, 11, 1, 23, 0)),
            '403': (dt(2008, 11, 1, 6, 3, 39), dt(2008, 11, 1, 23, 0)),
            '410': (dt(2008, 11, 1, 6, 3, 39), dt(2008, 11, 1, 23, 0)),
            '411': (dt(2008, 11, 1, 6, 3, 39), dt(2008, 11, 1, 23, 0)),
            '412': (dt(2008, 11, 1, 6, 3, 39), dt(2008, 11, 1, 23, 0)),
            '413': (dt(2008, 11, 1, 6, 3, 39), dt(2008, 11, 1, 23, 0)),
            '414': (dt(2008, 11, 1, 6, 3, 39), dt(2008, 11, 1, 23, 0)),
            '415': (dt(2008, 11, 1, 6, 3, 39), dt(2008, 11, 1, 23, 0)),
            '420': (None, None), '421': (None, None), '422': (None, None),
            '423': (None, None), '424': (None, None), '425': (None, None),
            '426': (None, None), '430': (None, None), '431': (None, None),
            '432': (None, None), '433': (None, None), '440': (None, None),
            '441': (None, None), '442': (None, None), '443': (None, None)
        }
        valid_obs = pnd.DataFrame({
            'DATE DEBUT': [dt(2008, 10, 31, 6)]*24,
            'MAXRR': [np.nan]*24,
            'AVGRR': [np.nan]*24,
            'CODE': ['401', '402', '403', '410', '411', '412', '413', '414',
                     '415', '420', '421', '422', '423', '424', '425', '426',
                     '430', '431', '432', '433', '440', '441', '442', '443'],
            'DTPROD': [dt(2008, 11, 1, 6, 3, 39)]*24,
            'NAME': ['Loire Morvan', 'Loire amont', 'Haut bassin Loire',
                     'Allier aval', 'Sioule', 'Dore', 'Allier amont',
                     'Allier intermediaire', 'Alagnon', 'Saone moyenne',
                     'Saone inferieure', 'Seille', 'Ain inferieur',
                     'Gier bas Dauphine', 'Rhone superieur', 'Rhone prealpin',
                     'Isere aval', 'Drac Romanche', 'Arly Isere moyenne',
                     'Haute Isere Arc', 'Yonne aval', 'Yonne intermediaire',
                     'Yonne-Morvan', 'Yonne-Auxois'],
            'PROVIDER': ['DIRCE']*24,
        })
        valid_prv = pnd.DataFrame({
            'DATE DEBUT': [dt(2008, 11, 1, 6), dt(2008, 11, 2, 6),
                           dt(2008, 11, 3, 6)]*24,
            'MAXRR': ['', '', '', '50/80', '', '', '50/80', '', '', '40/60',
                      '', '', '40/60', '', '', '50/80', '', '', '50/80', '',
                      '', '40/60', '', '', '50/80', '', '', '', '', '', '',
                      '', '', '', '', '', '', '', '', '', '', '', '', '', '',
                      '', '', '', '', '', '', '', '', '', '', '', '', '', '',
                      '', '', '', '', '', '', '', '', '', '', '', '', ''],
            'AVGRR': ['10/20', '1/5', '0', '20/40', '3/10', '0', '30/50',
                      '10/20', 'Tr/3', '10/20', '1/5', '0', '10/20', '7/15',
                      '0', '40/60', '3/10', '0', '30/50', '15/30', 'Tr/3',
                      '30/50', '7/15', '0', '30/50', '10/20', 'Tr/3', '7/15',
                      'Tr/3', 'Tr/3', '10/20', '1/5', 'Tr/3', '3/10', '1/5',
                      'Tr/3', '1/5', '1/5', 'Tr/3', '10/20', '3/10', 'Tr/3',
                      'Tr/3', '3/10', 'Tr/3', 'Tr/3', '1/5', 'Tr/3', '1/5',
                      '7/15', 'Tr/3', 'Tr/3', '3/10', 'Tr/3', '0', '1/5',
                      'Tr/3', 'Tr/3', '7/15', 'Tr/3', '3/10', 'Tr/3', '0',
                      '3/10', 'Tr/3', 'Tr/3', '7/15', 'Tr/3', 'Tr/3', '10/20',
                      'Tr/3', 'Tr/3'],
            'CODE': ['401', '401', '401', '402', '402', '402', '403', '403',
                     '403', '410', '410', '410', '411', '411', '411', '412',
                     '412', '412', '413', '413', '413', '414', '414', '414',
                     '415', '415', '415', '420', '420', '420', '421', '421',
                     '421', '422', '422', '422', '423', '423', '423', '424',
                     '424', '424', '425', '425', '425', '426', '426', '426',
                     '430', '430', '430', '431', '431', '431', '432', '432',
                     '432', '433', '433', '433', '440', '440', '440', '441',
                     '441', '441', '442', '442', '442', '443', '443', '443'],
            'NAME': ['Loire Morvan', 'Loire Morvan', 'Loire Morvan',
                     'Loire amont', 'Loire amont', 'Loire amont',
                     'Haut bassin Loire', 'Haut bassin Loire',
                     'Haut bassin Loire', 'Allier aval', 'Allier aval',
                     'Allier aval', 'Sioule', 'Sioule', 'Sioule', 'Dore',
                     'Dore', 'Dore', 'Allier amont', 'Allier amont',
                     'Allier amont', 'Allier intermediaire',
                     'Allier intermediaire', 'Allier intermediaire',
                     'Alagnon', 'Alagnon', 'Alagnon', 'Saone moyenne',
                     'Saone moyenne', 'Saone moyenne', 'Saone inferieure',
                     'Saone inferieure', 'Saone inferieure', 'Seille',
                     'Seille', 'Seille', 'Ain inferieur', 'Ain inferieur',
                     'Ain inferieur', 'Gier bas Dauphine', 'Gier bas Dauphine',
                     'Gier bas Dauphine', 'Rhone superieur', 'Rhone superieur',
                     'Rhone superieur', 'Rhone prealpin', 'Rhone prealpin',
                     'Rhone prealpin', 'Isere aval', 'Isere aval',
                     'Isere aval', 'Drac Romanche', 'Drac Romanche',
                     'Drac Romanche', 'Arly Isere moyenne',
                     'Arly Isere moyenne', 'Arly Isere moyenne',
                     'Haute Isere Arc', 'Haute Isere Arc', 'Haute Isere Arc',
                     'Yonne aval', 'Yonne aval', 'Yonne aval',
                     'Yonne intermediaire', 'Yonne intermediaire',
                     'Yonne intermediaire', 'Yonne-Morvan', 'Yonne-Morvan',
                     'Yonne-Morvan', 'Yonne-Auxois', 'Yonne-Auxois',
                     'Yonne-Auxois'],
            'DTPROD': [dt(2008, 11, 1, 6, 3, 39)]*72,
            'PROVIDER': ['DIRCE']*72,
        })
        # =====================================================================
        filename = os.path.join(self.dirname, 'bp_ly_200811010718.xml')
        reader = BP_Data(filename=filename)
        content = reader.read()
        self.assertDictEqual(content['alerts'], valid_alerts)
        assert_frame_equal(content['obs'], valid_obs)
        assert_frame_equal(content['prv'], valid_prv)
        # =====================================================================

    def test_read_xml_2016(self):
        """
        Lecture BP XML version 2016
        """
        # =====================================================================
        valid_alerts = {
            '70701': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '70702': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '70703': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '70704': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '70705': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '70708': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '70707': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '70402': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '70403': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '70901': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '71002': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '71003': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '71004': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '71005': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '71006': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0)),
            '71007': (dt(2016, 5, 31, 5, 2, 5), dt(2016, 5, 31, 14, 0))
        }
        valid_obs = pnd.DataFrame({
            'DATE DEBUT': [dt(2016, 5, 30)]*16,
            'MAXRR': [np.nan]*16,
            'AVGRR': [34.0, 30.0, 28.0, 31.0, 41.0, 53.0, 50.0, 21.0, 9.0,
                      21.0, 20.0, 33.0, 30.0, 35.0, 26.0, 22.0],
            'CODE': ['70701', '70702', '70703', '70704', '70705', '70708',
                     '70707', '70402', '70403', '70901', '71002', '71003',
                     '71004', '71005', '71006', '71007'],
            'DTPROD': [dt(2016, 5, 31, 5, 2, 5)]*16,
            'NAME': ['Boucles de Seine', 'Oise aval', 'Marne aval',
                     'Les Morins', 'Seine moyenne', 'Loing aval',
                     'Loing amont', 'Avre Eure amont', 'Perche', 'Loir amont',
                     'Cher aval Indre aval', 'Loire Tourangelle',
                     'Loire Giennoise', 'Sauldre', 'Yevre Cher moyen',
                     'Indre amont Arnon Theols'],
            'PROVIDER': ['DIRIC']*16,
        })
        valid_prv = pnd.DataFrame({
            'DATE DEBUT': [dt(2016, 5, 31), dt(2016, 6, 1),
                           dt(2016, 6, 2)]*16,
            'MAXRR': ['']*48,
            'AVGRR': ['15/30', 'Tr/3', '7/15', '15/30', 'Tr/3', '7/15',
                      '10/20', 'Tr/3', '10/20', '15/30', 'Tr/3', '10/20',
                      '15/30', 'Tr/3', '7/15', '15/30', '1/5', '10/20',
                      '15/30', '1/5', '7/15', '15/30', 'Tr/3', '7/15', '15/30',
                      'Tr/3', '1/5', '20/40', '1/5', '3/10', '20/40', '1/5',
                      '3/10', '20/40', '1/5', '3/10', '15/30', '1/5', '3/10',
                      '15/30', '1/5', '3/10', '15/30', '1/5', '3/10', '15/30',
                      '1/5', '1/5'],
            'CODE': [
                '70701', '70701', '70701', '70702', '70702', '70702', '70703',
                '70703', '70703', '70704', '70704', '70704', '70705', '70705',
                '70705', '70708', '70708', '70708', '70707', '70707', '70707',
                '70402', '70402', '70402', '70403', '70403', '70403', '70901',
                '70901', '70901', '71002', '71002', '71002', '71003', '71003',
                '71003', '71004', '71004', '71004', '71005', '71005', '71005',
                '71006', '71006', '71006', '71007', '71007', '71007'],
            'NAME': [
                'Boucles de Seine', 'Boucles de Seine', 'Boucles de Seine',
                'Oise aval', 'Oise aval', 'Oise aval',
                'Marne aval', 'Marne aval', 'Marne aval',
                'Les Morins', 'Les Morins', 'Les Morins',
                'Seine moyenne', 'Seine moyenne', 'Seine moyenne',
                'Loing aval', 'Loing aval', 'Loing aval',
                'Loing amont', 'Loing amont', 'Loing amont',
                'Avre Eure amont', 'Avre Eure amont', 'Avre Eure amont',
                'Perche', 'Perche', 'Perche',
                'Loir amont', 'Loir amont', 'Loir amont',
                'Cher aval Indre aval', 'Cher aval Indre aval',
                'Cher aval Indre aval',
                'Loire Tourangelle', 'Loire Tourangelle', 'Loire Tourangelle',
                'Loire Giennoise', 'Loire Giennoise', 'Loire Giennoise',
                'Sauldre', 'Sauldre', 'Sauldre',
                'Yevre Cher moyen', 'Yevre Cher moyen', 'Yevre Cher moyen',
                'Indre amont Arnon Theols', 'Indre amont Arnon Theols',
                'Indre amont Arnon Theols'],
            'DTPROD': [dt(2016, 5, 31, 5, 2, 5)]*48,
            'PROVIDER': ['DIRIC']*48,
        })
        # =====================================================================
        filename = os.path.join(self.dirname, 'bp_ic_201605310503.xml')
        reader = BP_Data(filename=filename)
        content = reader.read()
        self.assertDictEqual(content['alerts'], valid_alerts)
        assert_frame_equal(content['obs'], valid_obs)
        assert_frame_equal(content['prv'], valid_prv)
        # =====================================================================

    def test_value_to_interval(self):
        """
        Test de la définition de l'intervalle BP selon une valeur numérique
        """
        # =====================================================================
        with self.assertRaises(ValueError):
            BP_value_to_interval(value=-1)
        with self.assertRaises(ValueError):
            BP_value_to_interval(value='toto')
        with self.assertRaises(ValueError):
            BP_value_to_interval(value=[0])
        # =====================================================================
        valids = [
            [2, "Tr/3"],
            [17, "10/20"],
            [42, "30/50"],
            [83, "70/100"],
        ]
        for v in valids:
            self.assertEqual(BP_value_to_interval(value=v[0]), v[1])
        # =====================================================================
