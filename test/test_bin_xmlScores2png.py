#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary xmlScores2png

To run all tests just type:
    python -m unittest test_bin_xmlScores2png

To run only a class test:
    python -m unittest test_bin_xmlScores2png.Test_xmlScores2png

To run only a specific test:
    python -m unittest test_bin_xmlScores2png.Test_xmlScores2png.test_sim

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import xmlScores2png as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_xmlScores2png(unittest.TestCase):
    """
    xmlScores2png bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'xmlScores2png')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_scores = os.path.join('data', 'verification', 'scores')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.figure = None

    def tearDown(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        figure=self.figure)

    def test_sim(self):
        """
        Test XML Scores - Cas de simulations
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlScores2png.py',
            '-I', self.dir_scores,
            '-d', 'resultat_simulation.xml',
            '-O', self.dir_out,
            '-t', 'sim'
        ]
#        processArgs.append('-v')
#        for n in self.valid_names:
#            processArgs.extend(['-U', n])
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.xmlScores2png()
        self.assertEqual(args.cfg_filename, None)
        self.assertEqual(args.xml_filename, 'resultat_simulation.xml')
        self.assertEqual(args.data_type, 'sim')
        self.assertEqual(args.input_dir, self.dir_scores)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.score_name, ['MAE', 'MARE', 'RMSE'])
        self.assertEqual(args.verbose, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        self.figure = []
        for filename in os.listdir(self.dir_out):
            if filename.startswith('resultat_simulation') and \
                    filename.endswith('csv'):
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dir_out, filename),
                    os.path.join(self.dir_scores, filename),
                ))
                os.remove(os.path.join(self.dir_out, filename))
            elif filename.startswith('resultat_simulation') and \
                    filename.endswith('png'):
                self.figure.append(filename)
                os.remove(os.path.join(self.dir_out, filename))
            else:
                os.remove(os.path.join(self.dir_out, filename))
#            if filename.startswith('resultat_simulation'):
#                self.assertTrue(filecmp.cmp(
#                    os.path.join(self.dir_out, filename),
#                    os.path.join(self.dir_scores, filename),
#                ))
#                os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = 'Conversion des critères numériques des données ({t}) '\
            'contenues dans le fichier {d} situé dans le répertoire {i} aux '\
            'formats csv et png vers le répertoire {o}'.format(
                d=args.xml_filename, i=args.input_dir, o=args.output_dir,
                t=args.data_type)
        # =====================================================================

    def test_fcst(self):
        """
        Test XML Scores - Cas de prévisions
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlScores2png.py',
            '-I', self.dir_scores,
            '-d', 'resultat_prevision.xml',
            '-O', self.dir_out,
            '-t', 'fcst'
        ]
#        for n in self.valid_fcstnames:
#            processArgs.extend(['-U', n])
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.xmlScores2png()
        self.assertEqual(args.cfg_filename, None)
        self.assertEqual(args.xml_filename, 'resultat_prevision.xml')
        self.assertEqual(args.data_type, 'fcst')
        self.assertEqual(args.input_dir, self.dir_scores)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.score_name, ['MAE', 'MARE', 'RMSE'])
        self.assertEqual(args.verbose, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        self.figure = []
        for filename in os.listdir(self.dir_out):
            if filename.startswith('resultat_prevision') and \
                    filename.endswith('csv'):
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dir_out, filename),
                    os.path.join(self.dir_scores, filename),
                ))
                os.remove(os.path.join(self.dir_out, filename))
            elif filename.startswith('resultat_prevision') and \
                    filename.endswith('png'):
                self.figure.append(filename)
                os.remove(os.path.join(self.dir_out, filename))
            else:
                os.remove(os.path.join(self.dir_out, filename))
#            if filename.startswith('resultat_prevision'):
#                self.assertTrue(filecmp.cmp(
#                    os.path.join(self.dir_out, filename),
#                    os.path.join(self.dir_scores, filename),
#                ))
#                os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = 'Conversion des critères numériques des données ({t}) '\
            'contenues dans le fichier {d} situé dans le répertoire {i} aux '\
            'formats csv et png vers le répertoire {o}'.format(
                d=args.xml_filename, i=args.input_dir, o=args.output_dir,
                t=args.data_type)
        # =====================================================================
