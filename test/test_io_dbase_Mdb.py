#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Mdb in pyspc.io.dbase.mdb

To run all tests just type:
    python -m unittest test_io_dbase_Mdb

To run only a class test:
    python -m unittest test_io_dbase_Mdb.TestMdb

To run only a specific test:
    python -m unittest test_io_dbase_Mdb.TestMdb.test_init

"""
# Imports
from datetime import datetime as dt
import os
import pyodbc
import unittest
import pyspc.io.dbase.mdb as _dbase


class TestMdb(unittest.TestCase):
    """
    Mdb class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'io', 'dbase')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        with self.assertRaises(ValueError):
            _dbase.Mdb()
        filename = os.path.join(self.dirname, 'sacha_montpezat.mdb')
        reader = _dbase.Mdb(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertIsNone(reader._dbase_connect)
        self.assertIsNone(reader._dbase_cursor)
        self.assertIsNone(reader.sql)
        self.assertIsNone(reader._tables)

    def test_connect_error(self):
        """
        Test de la connexion, fermeture de la base
        """
        filename = os.path.join(self.dirname, 'toto.mdb')
        with self.assertRaises(ValueError):
            _dbase.Mdb(filename=filename)

    def test_connect_close(self):
        """
        Test de la connexion, fermeture de la base
        """
        filename = os.path.join(self.dirname, 'sacha_montpezat.mdb')
        reader = _dbase.Mdb(filename=filename)
        self.assertIsNone(reader._dbase_connect)
        self.assertIsNone(reader._dbase_cursor)
        reader.connect()
        self.assertIsNotNone(reader._dbase_connect)
        self.assertIsNotNone(reader._dbase_cursor)
        self.assertIsInstance(reader._dbase_connect, pyodbc.Connection)
        self.assertIsInstance(reader._dbase_cursor, pyodbc.Cursor)
        reader.close()
        self.assertIsNone(reader._dbase_connect)
        self.assertIsNone(reader._dbase_cursor)

    def test_connect_execute_close(self):
        """
        Test de la connexion, lecture et fermeture de la base
        """
        valid = [
            (2, 'K0010020'),
            (3, 'K0018720'),
            (5, 'K0030020'),
            (6, 'K0100020'),
        ]
        sql = "SELECT station.nosta, station.codeh "\
            "FROM station "\
            "WHERE station.codeh in "\
            "('K0010020','K0018720','K0030020','K0100020')"
        filename = os.path.join(self.dirname, 'sacha_montpezat.mdb')
        reader = _dbase.Mdb(filename=filename)
        reader.connect()
        with self.assertRaises(ValueError):
            reader.execute()
        reader.sql = 1
        with self.assertRaises(ValueError):
            reader.execute()
        reader.sql = sql
        data = reader.execute()
        for d, v in zip(data, valid):
            self.assertEqual(list(d), list(v))
        reader.close()

    def test_access_dtime_dt(self):
        """
        Test de la date dans Access - Cas datetime.datetime
        """
        self.assertEqual(
            2,
            _dbase.Mdb.from_datetime(dtime=dt(1900, 1, 1)))
        self.assertEqual(
            39448,
            _dbase.Mdb.from_datetime(dtime=dt(2008, 1, 1)))
        self.assertEqual(
            39448.25,
            _dbase.Mdb.from_datetime(dtime=dt(2008, 1, 1, 6)))
        self.assertEqual(
            39453,
            _dbase.Mdb.from_datetime(dtime=dt(2008, 1, 6)))
        self.assertEqual(
            39453.00069444445,
            _dbase.Mdb.from_datetime(dtime=dt(2008, 1, 6), tolerance=60))

    def test_access_dtime_str(self):
        """
        Test de la date dans Access - Cas str
        """
        self.assertEqual(
            39448,
            _dbase.Mdb.from_datetime(dtime='200801', fmt='%Y%m'))
        self.assertEqual(
            39448,
            _dbase.Mdb.from_datetime(dtime='20080101', fmt='%Y%m%d'))
        self.assertEqual(
            39448.25,
            _dbase.Mdb.from_datetime(dtime='2008010106', fmt='%Y%m%d%H'))
        self.assertEqual(
            39453,
            _dbase.Mdb.from_datetime(dtime='20080106', fmt='%Y%m%d'))
        self.assertEqual(
            39453.00069444445,
            _dbase.Mdb.from_datetime(dtime='20080106', fmt='%Y%m%d',
                                     tolerance=60))

    def test_access_dtime_error(self):
        """
        Test de la date dans Access - Cas avec erreurs soulevées
        """
        with self.assertRaises(ValueError):
            _dbase.Mdb.from_datetime()
        with self.assertRaises(ValueError):
            _dbase.Mdb.from_datetime(dtime='')
        with self.assertRaises(ValueError):
            _dbase.Mdb.from_datetime(dtime='20080101')
        with self.assertRaises(ValueError):
            _dbase.Mdb.from_datetime(dtime='20080101', fmt='%Y%m%d',
                                     tolerance='1')
