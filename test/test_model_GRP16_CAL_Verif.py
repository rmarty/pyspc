#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP_Verif in pyspc.model.grp16

To run all tests just type:
    python -m unittest test_model_GRP16_CAL_Verif

To run only a class test:
    python -m unittest test_model_GRP16_CAL_Verif.TestGRP_Verif

To run only a specific test:
    python -m unittest test_model_GRP16_CAL_Verif.TestGRP_Verif.test_init

"""
# Imports
import collections
import filecmp
import os
import unittest

from pyspc.model.grp16.cal_verif import (
    GRP_Verif, fix_SV, plot_verif, read_csv, reset_key)


class TestGRP_Verif(unittest.TestCase):
    """
    GRP_Verif class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp16', 'cal')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        dtype = 'cal'
        filename = os.path.join(
            self.dirname,
            '20170713191539_FichePerf_K0403010_SC_9_HC_3_SV_54.pdf'
        )
        # =====================================================================
        reader = GRP_Verif(filename=filename, datatype=dtype)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.datatype, dtype)
        self.assertEqual(reader.loc, 'K0403010')
        self.assertEqual(reader.hc, '3')
        self.assertEqual(reader.sc, '9')
        self.assertEqual(reader.sv, '54')
        # =====================================================================

    def test_splitbasename_cal(self):
        """
        Test de la configuration lue à partir d'une ficher de type CAL
        """
        # =====================================================================
        dtype = 'cal'
        filename = os.path.join(
            self.dirname,
            '20170713191539_FichePerf_K0403010_SC_9_HC_3_SV_54.pdf'
        )
        # =====================================================================
        meta = GRP_Verif.split_basename(filename=filename, datatype=dtype)
        self.assertEqual(meta[0], 'K0403010')
        self.assertEqual(meta[1], '3')
        self.assertEqual(meta[2], '9')
        self.assertEqual(meta[3], '54')
        # =====================================================================

    def test_splitbasename_rtime(self):
        """
        Test de la configuration lue à partir d'une ficher de type RTIME
        """
        # =====================================================================
        dtype = 'rtime'
        filename = os.path.join(
            self.dirname,
            '20171010140931_FichePerf_calage_complet_'
            'K0403010_SC_9_HC_3_SV_54.pdf'
        )
        # =====================================================================
        meta = GRP_Verif.split_basename(filename=filename, datatype=dtype)
        self.assertEqual(meta[0], 'K0403010')
        self.assertEqual(meta[1], '3')
        self.assertEqual(meta[2], '9')
        self.assertEqual(meta[3], '54')
        # =====================================================================

    def test_splitbasename_errors(self):
        """
        Test de l'erreur soulevée en cas de type de fiche inconnu
        """
        # =====================================================================
        filename = 'toto.pdf'
        dtype = 'toto'
        with self.assertRaises(ValueError):
            GRP_Verif.split_basename(filename=filename, datatype=dtype)
        # =====================================================================

    def test_read_cal(self):
        """
        Test de la lecture d'une ficher de type CAL
        """
        # =====================================================================
        valids = {
            'SMN_TAN': {'Eff': 0.831, 'POD': 64.1, 'FAR': 16.7, 'CSI': 56.8},
            'SMN_RNA': {'Eff': 0.868, 'POD': 76.9, 'FAR': 11.8, 'CSI': 69.8},
        }
        # =====================================================================
        filename = os.path.join(
            self.dirname,
            '20170713191539_FichePerf_K0403010_SC_9_HC_3_SV_54.pdf'
        )
        dtype = 'cal'
        # =====================================================================
        reader = GRP_Verif(filename=filename, datatype=dtype)
        data = reader.read()
        self.assertDictEqual(data, valids)
        # =====================================================================

    def test_read_cal_snow(self):
        """
        Test de la lecture d'une ficher de type CAL (avec module Neige)
        """
        # =====================================================================
        valids = {
            'SMN_RNA': {'Eff': 0.868, 'POD': 76.9, 'FAR': 11.8, 'CSI': 69.8},
            'AMN_TAN': {'Eff': 0.829, 'POD': 74.4, 'FAR': 21.6, 'CSI': 61.7},
            'AMN_RNA': {'Eff': 0.876, 'POD': 89.7, 'FAR': 10.3, 'CSI': 81.4},
        }
        # =====================================================================
        filename = os.path.join(
            self.dirname,
            '20180221210453_FichePerf_K0403010_SC_9_HC_3_SV_54.pdf'
        )
        dtype = 'cal'
        # =====================================================================
        reader = GRP_Verif(filename=filename, datatype=dtype)
        data = reader.read()
        self.assertDictEqual(data, valids)
        # =====================================================================

    def test_read_rtime(self):
        """
        Test de la lecture d'une ficher de type RTIME
        """
        # =====================================================================
        valids = {
            'SMN_TAN': {'Eff': 0.846, 'POD': 64.1, 'FAR': 13.8, 'CSI': 58.1},
        }
        # =====================================================================
        filename = os.path.join(
            self.dirname,
            '20171010140931_FichePerf_calage_complet_'
            'K0403010_SC_9_HC_3_SV_54.pdf'
        )
        dtype = 'rtime'
        # =====================================================================
        reader = GRP_Verif(filename=filename, datatype=dtype)
        data = reader.read()
        self.assertDictEqual(data, valids)
        # =====================================================================

    def test_datatype(self):
        """
        Test des types de fiche de performance
        """
        valids = ['cal', 'rtime']
        self.assertEqual(GRP_Verif.get_datatypes(), valids)


class TestGRP_Verif_Plot(unittest.TestCase):
    """
    GRP_Verif class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp16', 'cal')
        self.filename = os.path.join(self.dirname, 'synthese.txt')
        self.grp_config = {
            'SC': ['ref', 'DC50', 'DC90', 'DC95'],
            'HC': ['ref'],
            'SV': ['ref', 'DC95'],
            'MODE': ['SMN_TAN', 'SMN_RNA']
        }
        self.scores_names = ['POD', 'FAR', 'CSI']
        self.toprint_vars = [(s, t)
                             for s in self.scores_names
                             for t in self.grp_config['SV']]
        self.toprint_sets = [(m, c)
                             for m in self.grp_config['MODE']
                             for c in self.grp_config['SC']]
        self.content = collections.OrderedDict()
        self.content[('SMN_TAN', 'ref')] = collections.OrderedDict()
        self.content[('SMN_TAN', 'ref')][('POD', 'ref')] = 71.8
        self.content[('SMN_TAN', 'ref')][('FAR', 'ref')] = 34.9
        self.content[('SMN_TAN', 'ref')][('CSI', 'ref')] = 51.9
        self.content[('SMN_TAN', 'ref')][('POD', 'DC95')] = 57.6
        self.content[('SMN_TAN', 'ref')][('FAR', 'DC95')] = 42.0
        self.content[('SMN_TAN', 'ref')][('CSI', 'DC95')] = 40.7
        self.content[('SMN_RNA', 'ref')] = collections.OrderedDict()
        self.content[('SMN_RNA', 'ref')][('POD', 'ref')] = 71.8
        self.content[('SMN_RNA', 'ref')][('FAR', 'ref')] = 28.2
        self.content[('SMN_RNA', 'ref')][('CSI', 'ref')] = 56.0
        self.content[('SMN_RNA', 'ref')][('POD', 'DC95')] = 71.9
        self.content[('SMN_RNA', 'ref')][('FAR', 'DC95')] = 35.3
        self.content[('SMN_RNA', 'ref')][('CSI', 'DC95')] = 51.7
        self.content[('SMN_TAN', 'DC50')] = collections.OrderedDict()
        self.content[('SMN_TAN', 'DC50')][('POD', 'ref')] = 71.8
        self.content[('SMN_TAN', 'DC50')][('FAR', 'ref')] = 33.3
        self.content[('SMN_TAN', 'DC50')][('CSI', 'ref')] = 52.8
        self.content[('SMN_TAN', 'DC50')][('POD', 'DC95')] = 57.9
        self.content[('SMN_TAN', 'DC50')][('FAR', 'DC95')] = 41.1
        self.content[('SMN_TAN', 'DC50')][('CSI', 'DC95')] = 41.2
        self.content[('SMN_RNA', 'DC50')] = collections.OrderedDict()
        self.content[('SMN_RNA', 'DC50')][('POD', 'ref')] = 74.4
        self.content[('SMN_RNA', 'DC50')][('FAR', 'ref')] = 27.5
        self.content[('SMN_RNA', 'DC50')][('CSI', 'ref')] = 58.0
        self.content[('SMN_RNA', 'DC50')][('POD', 'DC95')] = 72.4
        self.content[('SMN_RNA', 'DC50')][('FAR', 'DC95')] = 34.6
        self.content[('SMN_RNA', 'DC50')][('CSI', 'DC95')] = 52.4
        self.content[('SMN_TAN', 'DC90')] = collections.OrderedDict()
        self.content[('SMN_TAN', 'DC90')][('POD', 'ref')] = 71.8
        self.content[('SMN_TAN', 'DC90')][('FAR', 'ref')] = 37.8
        self.content[('SMN_TAN', 'DC90')][('CSI', 'ref')] = 50.0
        self.content[('SMN_TAN', 'DC90')][('POD', 'DC95')] = 57.9
        self.content[('SMN_TAN', 'DC90')][('FAR', 'DC95')] = 43.2
        self.content[('SMN_TAN', 'DC90')][('CSI', 'DC95')] = 40.2
        self.content[('SMN_RNA', 'DC90')] = collections.OrderedDict()
        self.content[('SMN_RNA', 'DC90')][('POD', 'ref')] = 74.4
        self.content[('SMN_RNA', 'DC90')][('FAR', 'ref')] = 29.3
        self.content[('SMN_RNA', 'DC90')][('CSI', 'ref')] = 56.9
        self.content[('SMN_RNA', 'DC90')][('POD', 'DC95')] = 72.9
        self.content[('SMN_RNA', 'DC90')][('FAR', 'DC95')] = 35.7
        self.content[('SMN_RNA', 'DC90')][('CSI', 'DC95')] = 51.9
        self.content[('SMN_TAN', 'DC95')] = collections.OrderedDict()
        self.content[('SMN_TAN', 'DC95')][('POD', 'ref')] = 74.4
        self.content[('SMN_TAN', 'DC95')][('FAR', 'ref')] = 37.0
        self.content[('SMN_TAN', 'DC95')][('CSI', 'ref')] = 51.8
        self.content[('SMN_TAN', 'DC95')][('POD', 'DC95')] = 58.4
        self.content[('SMN_TAN', 'DC95')][('FAR', 'DC95')] = 44.1
        self.content[('SMN_TAN', 'DC95')][('CSI', 'DC95')] = 40.0
        self.content[('SMN_RNA', 'DC95')] = collections.OrderedDict()
        self.content[('SMN_RNA', 'DC95')][('POD', 'ref')] = 82.1
        self.content[('SMN_RNA', 'DC95')][('FAR', 'ref')] = 31.9
        self.content[('SMN_RNA', 'DC95')][('CSI', 'ref')] = 59.3
        self.content[('SMN_RNA', 'DC95')][('POD', 'DC95')] = 75.1
        self.content[('SMN_RNA', 'DC95')][('FAR', 'DC95')] = 35.3
        self.content[('SMN_RNA', 'DC95')][('CSI', 'DC95')] = 53.3

    def test_fix_SV(self):
        """
        Test sélection SV
        """
        toremove_sv, towarn_sv, toprint_vars2 = fix_SV(
            content=self.content,
            toprint_sets=self.toprint_sets, toprint_vars=self.toprint_vars)
        self.assertIsInstance(toremove_sv, list)
        self.assertEqual(len(toremove_sv), 0)
        self.assertIsInstance(towarn_sv, list)
        self.assertEqual(len(towarn_sv), 0)
        self.assertEqual(self.toprint_vars, toprint_vars2)

    def test_plot(self):
        """
        Test création fichier image 'radar'
        """
        # =====================================================================
        cases = [(ftop, colv, fhal) for ftop in [True, False]
                 for colv in [True, False] for fhal in [True, False]]
        for case in cases:
            fig_filename = os.path.join(
                'data', 'grpverif_{}{}{}.png'.format(*case))
            msg = plot_verif(
                filename=fig_filename,
                title='unittest {}'.format(fig_filename),
                toprint_sample=self.content,
                toprint_vars=self.toprint_vars,
                toprint_sets=self.toprint_sets,
                dpi=75
            )
            self.assertEqual(msg, fig_filename)
            srcfile = os.path.join(
                self.dirname, os.path.basename(fig_filename))
            self.assertTrue(filecmp.cmp(
                fig_filename,
                srcfile
            ))
            os.remove(fig_filename)
        # =====================================================================

    def test_read_csv(self):
        """
        Test GRP Verif - Lecture de la synthese CSV
        """
        valid = {
            (('K0260010', '6'), ('SMN_TAN', '13', '210', 1), 'POD'): 65.2,
            (('K0260010', '6'), ('SMN_TAN', '13', '210', 1), 'FAR'): 25.9,
            (('K0260010', '6'), ('SMN_TAN', '13', '210', 1), 'CSI'): 53.1,
            (('K0260010', '6'), ('SMN_RNA', '23', '31', 1), 'POD'): 73.7,
            (('K0260010', '6'), ('SMN_RNA', '23', '31', 1), 'FAR'): 54.6,
            (('K0260010', '6'), ('SMN_RNA', '23', '31', 1), 'CSI'): 39.0,
        }
        content = read_csv(filename=self.filename,
                           scores_names=self.scores_names)
        self.assertIsInstance(content, dict)
        for key in valid:
            k1 = key[0]
            k2 = key[1]
            k3 = key[2]
            self.assertIn(k1, content)
            self.assertIsInstance(content[k1], dict)
            self.assertIn(k2, content[k1])
            self.assertIsInstance(content[k1][k2], dict)
            self.assertIn(k3, content[k1][k2])
            self.assertIsInstance(content[k1][k2][k3], float)
            self.assertEqual(valid[key], content[k1][k2][k3])

    def test_reset_key(self):
        """
        Test GRP Verif - clé numérique -> clés avec titre
        """
        valid = self.content
        content = collections.OrderedDict()
        content[('K0403010', '3')] = collections.OrderedDict()

        content[('K0403010', '3')][('SMN_TAN', '3', '50', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_TAN', '3', '50', 1)]['POD'] = 71.8
        content[('K0403010', '3')][('SMN_TAN', '3', '50', 1)]['FAR'] = 34.9
        content[('K0403010', '3')][('SMN_TAN', '3', '50', 1)]['CSI'] = 51.9

        content[('K0403010', '3')][('SMN_RNA', '3', '50', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_RNA', '3', '50', 1)]['POD'] = 71.8
        content[('K0403010', '3')][('SMN_RNA', '3', '50', 1)]['FAR'] = 28.2
        content[('K0403010', '3')][('SMN_RNA', '3', '50', 1)]['CSI'] = 56.0

        content[('K0403010', '3')][('SMN_TAN', '2', '50', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_TAN', '2', '50', 1)]['POD'] = 71.8
        content[('K0403010', '3')][('SMN_TAN', '2', '50', 1)]['FAR'] = 33.3
        content[('K0403010', '3')][('SMN_TAN', '2', '50', 1)]['CSI'] = 52.8

        content[('K0403010', '3')][('SMN_RNA', '2', '50', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_RNA', '2', '50', 1)]['POD'] = 74.4
        content[('K0403010', '3')][('SMN_RNA', '2', '50', 1)]['FAR'] = 27.5
        content[('K0403010', '3')][('SMN_RNA', '2', '50', 1)]['CSI'] = 58.0

        content[('K0403010', '3')][('SMN_TAN', '7', '50', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_TAN', '7', '50', 1)]['POD'] = 71.8
        content[('K0403010', '3')][('SMN_TAN', '7', '50', 1)]['FAR'] = 37.8
        content[('K0403010', '3')][('SMN_TAN', '7', '50', 1)]['CSI'] = 50.0

        content[('K0403010', '3')][('SMN_RNA', '7', '50', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_RNA', '7', '50', 1)]['POD'] = 74.4
        content[('K0403010', '3')][('SMN_RNA', '7', '50', 1)]['FAR'] = 29.3
        content[('K0403010', '3')][('SMN_RNA', '7', '50', 1)]['CSI'] = 56.9

        content[('K0403010', '3')][('SMN_TAN', '9', '50', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_TAN', '9', '50', 1)]['POD'] = 74.4
        content[('K0403010', '3')][('SMN_TAN', '9', '50', 1)]['FAR'] = 37.0
        content[('K0403010', '3')][('SMN_TAN', '9', '50', 1)]['CSI'] = 51.8

        content[('K0403010', '3')][('SMN_RNA', '9', '50', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_RNA', '9', '50', 1)]['POD'] = 82.1
        content[('K0403010', '3')][('SMN_RNA', '9', '50', 1)]['FAR'] = 31.9
        content[('K0403010', '3')][('SMN_RNA', '9', '50', 1)]['CSI'] = 59.3

        content[('K0403010', '3')][('SMN_TAN', '3', '9', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_TAN', '3', '9', 1)]['POD'] = 57.6
        content[('K0403010', '3')][('SMN_TAN', '3', '9', 1)]['FAR'] = 42.0
        content[('K0403010', '3')][('SMN_TAN', '3', '9', 1)]['CSI'] = 40.7

        content[('K0403010', '3')][('SMN_RNA', '3', '9', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_RNA', '3', '9', 1)]['POD'] = 71.9
        content[('K0403010', '3')][('SMN_RNA', '3', '9', 1)]['FAR'] = 35.3
        content[('K0403010', '3')][('SMN_RNA', '3', '9', 1)]['CSI'] = 51.7

        content[('K0403010', '3')][('SMN_TAN', '2', '9', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_TAN', '2', '9', 1)]['POD'] = 57.9
        content[('K0403010', '3')][('SMN_TAN', '2', '9', 1)]['FAR'] = 41.1
        content[('K0403010', '3')][('SMN_TAN', '2', '9', 1)]['CSI'] = 41.2

        content[('K0403010', '3')][('SMN_RNA', '2', '9', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_RNA', '2', '9', 1)]['POD'] = 72.4
        content[('K0403010', '3')][('SMN_RNA', '2', '9', 1)]['FAR'] = 34.6
        content[('K0403010', '3')][('SMN_RNA', '2', '9', 1)]['CSI'] = 52.4

        content[('K0403010', '3')][('SMN_TAN', '7', '9', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_TAN', '7', '9', 1)]['POD'] = 57.9
        content[('K0403010', '3')][('SMN_TAN', '7', '9', 1)]['FAR'] = 43.2
        content[('K0403010', '3')][('SMN_TAN', '7', '9', 1)]['CSI'] = 40.2

        content[('K0403010', '3')][('SMN_RNA', '7', '9', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_RNA', '7', '9', 1)]['POD'] = 72.9
        content[('K0403010', '3')][('SMN_RNA', '7', '9', 1)]['FAR'] = 35.7
        content[('K0403010', '3')][('SMN_RNA', '7', '9', 1)]['CSI'] = 51.9

        content[('K0403010', '3')][('SMN_TAN', '9', '9', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_TAN', '9', '9', 1)]['POD'] = 58.4
        content[('K0403010', '3')][('SMN_TAN', '9', '9', 1)]['FAR'] = 44.1
        content[('K0403010', '3')][('SMN_TAN', '9', '9', 1)]['CSI'] = 40.0

        content[('K0403010', '3')][('SMN_RNA', '9', '9', 1)] = \
            collections.OrderedDict()
        content[('K0403010', '3')][('SMN_RNA', '9', '9', 1)]['POD'] = 75.1
        content[('K0403010', '3')][('SMN_RNA', '9', '9', 1)]['FAR'] = 35.3
        content[('K0403010', '3')][('SMN_RNA', '9', '9', 1)]['CSI'] = 53.3
        newcontent = reset_key(
            content[('K0403010', '3')], config=self.grp_config,
            scores_names=self.scores_names)
        self.assertDictEqual(newcontent, valid)
