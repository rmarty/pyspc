#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Metscen in pyspc.model.grp18

To run all tests just type:
    python -m unittest test_model_GRP18_RT_Metscen

To run only a class test:
    python -m unittest test_model_GRP18_RT_Metscen.TestGRPRT_Metscen

To run only a specific test:
    python -m unittest test_model_GRP18_RT_Metscen.TestGRPRT_Metscen.test_init

"""
# Imports
import filecmp
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.model.grp18 import GRPRT_Metscen


class TestGRPRT_Metscen(unittest.TestCase):
    """
    GRPRT_Metscen class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp18', 'rt')
        self.valid_scen = None
        self.valid_scen = pnd.DataFrame({
            'PREFIX': ['PLU']*20,
            'CODE': ['RH10585x']*20,
            'DATETIME': [202105310100, 202105310200, 202105310300,
                         202105310400, 202105310500, 202105310600,
                         202105310700, 202105310800, 202105310900,
                         202105311000, 202105311100, 202105311200,
                         202105311300, 202105311400, 202105311500,
                         202105311600, 202105311700, 202105311800,
                         202105311900, 202105312000],
            'VALUE': [0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 1.07, 1.07, 1.07,
                      2.37, 2.37, 2.37, 3.35, 3.35, 3.35, 4.07, 4.07, 4.07,
                      0.00, 0.00],
        })

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'Scen_001_PluRR_00J01H00M.txt')
        reader = GRPRT_Metscen(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'P')
        self.assertEqual(reader.lineprefix, 'PLU')
        self.assertEqual(reader.scen, '001')
        self.assertEqual(reader.timestep, '00J01H00M')
        # =====================================================================

    def test_read_scen(self):
        """
        Test de lecture d'un fichier de données OBS pour GRP RT - Cas Pluie
        """
        filename = os.path.join(self.dirname, 'Scen_001_PluRR_00J01H00M.txt')
        reader = GRPRT_Metscen(filename=filename)
        df = reader.read()
        assert_frame_equal(df, self.valid_scen)

    def test_write_scen(self):
        """
        Test d'écriture d'un fichier de données OBS pour GRP RT
        """
        filename = os.path.join(self.dirname, 'Scen_001_PluRR_00J01H00M.txt')
        tmpfile = os.path.join('data', 'Scen_001_PluRR_00J01H00M.txt')
        writer = GRPRT_Metscen(filename=tmpfile)
        writer.write(self.valid_scen)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
