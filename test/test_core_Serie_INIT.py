#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Serie in pyspc.core.serie - instanciation

To run all tests just type:
    python -m unittest test_core_Serie_INIT

To run only a class test:
    python -m unittest test_core_Serie_INIT.TestSerie

To run only a specific test:
    python -m unittest test_core_Serie_INIT.TestSerie.test_init_list_str

"""
# Imports
from datetime import datetime as dt, timedelta as td
import numpy as np
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest
import warnings

# Imports pyspc
from pyspc.core.serie import Serie


class TestSerie(unittest.TestCase):
    """
    Serie class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        self.code = 'K0000000'
        self.provider = 'SPC'
        self.varname = 'QH'
        self.list_str = [
            ['2014110400', '35.600'],
            ['2014110401', '41.200'],
            ['2014110402', '46.900'],
            ['2014110403', '62.900'],
            ['2014110404', '85.700'],
            ['2014110405', '106.000'],
            ['2014110406', '118.000'],
            ['2014110407', '115.000'],
            ['2014110408', '103.000'],
            ['2014110409', '92.500'],
            ['2014110410', '86.400'],
            ['2014110411', '82.300'],
            ['2014110412', '78.300']
        ]
        self.list_dtval = [
            [dt(2014, 11, 4, 0), 35.600],
            [dt(2014, 11, 4, 1), 41.200],
            [dt(2014, 11, 4, 2), 46.900],
            [dt(2014, 11, 4, 3), 62.900],
            [dt(2014, 11, 4, 4), 85.700],
            [dt(2014, 11, 4, 5), 106.000],
            [dt(2014, 11, 4, 6), 118.000],
            [dt(2014, 11, 4, 7), 115.000],
            [dt(2014, 11, 4, 8), 103.000],
            [dt(2014, 11, 4, 9), 92.500],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 11), 82.300],
            [dt(2014, 11, 4, 12), 78.300]
        ]
        self.list_misc = [
            ['2014110400', '35.600'],
            ['2014110401', '41.200'],
            ['2014110402', '46.900'],
            ['2014110403', '62.900'],
            ['2014110404', '85.700'],
            ['2014110405', '106.000'],
            ['2014110406', '118.000'],
            [dt(2014, 11, 4, 7), 115.000],
            [dt(2014, 11, 4, 8), 103.000],
            [dt(2014, 11, 4, 9), 92.500],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 11), 82.300],
            [dt(2014, 11, 4, 12), 78.300]
        ]
        self.list_dupl = [
            [dt(2014, 11, 4, 0), -35.600],
            [dt(2014, 11, 4, 1), -41.200],
            [dt(2014, 11, 4, 2), -46.900],
            [dt(2014, 11, 4, 3), -62.900],
            [dt(2014, 11, 4, 4), -85.700],
            [dt(2014, 11, 4, 5), -106.000],
            [dt(2014, 11, 4, 6), -118.000],
            [dt(2014, 11, 4, 0), 35.600],
            [dt(2014, 11, 4, 1), 41.200],
            [dt(2014, 11, 4, 2), 46.900],
            [dt(2014, 11, 4, 3), 62.900],
            [dt(2014, 11, 4, 4), 85.700],
            [dt(2014, 11, 4, 5), 106.000],
            [dt(2014, 11, 4, 6), 118.000],
            [dt(2014, 11, 4, 7), 115.000],
            [dt(2014, 11, 4, 8), 103.000],
            [dt(2014, 11, 4, 9), 92.500],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 11), 82.300],
            [dt(2014, 11, 4, 12), 78.300]
        ]
        self.list_mv = [
            [dt(2014, 11, 4, 0), 35.600],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 1), 41.200],
            [dt(2014, 11, 4, 11), 82.300],
            [dt(2014, 11, 4, 2), 46.900],
            [dt(2014, 11, 4, 12), 78.300],
            [dt(2014, 11, 4, 3), ''],
            [dt(2014, 11, 4, 6), np.nan],
            [dt(2014, 11, 4, 9), -999.999],
        ]
        self.valid_mv = [
            [dt(2014, 11, 4, 0), 35.600],
            [dt(2014, 11, 4, 1), 41.200],
            [dt(2014, 11, 4, 2), 46.900],
            [dt(2014, 11, 4, 3), np.nan],
            [dt(2014, 11, 4, 4), np.nan],
            [dt(2014, 11, 4, 5), np.nan],
            [dt(2014, 11, 4, 6), np.nan],
            [dt(2014, 11, 4, 7), np.nan],
            [dt(2014, 11, 4, 8), np.nan],
            [dt(2014, 11, 4, 9), np.nan],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 11), 82.300],
            [dt(2014, 11, 4, 12), 78.300]
        ]
        self.valid_mv_nofilled = [
            [dt(2014, 11, 4, 0), 35.600],
            [dt(2014, 11, 4, 1), 41.200],
            [dt(2014, 11, 4, 2), 46.900],
            [dt(2014, 11, 4, 3), np.nan],
            [dt(2014, 11, 4, 6), np.nan],
            [dt(2014, 11, 4, 9), np.nan],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 11), 82.300],
            [dt(2014, 11, 4, 12), 78.300]
        ]

    def test_init_str(self):
        """
        Test de la création de l'instance - Cas Liste de str
        """
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        serie = Serie(
            datval=self.list_str,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        assert_frame_equal(serie.data_frame, valid)
        assert_frame_equal(serie.df, valid)
        self.assertEqual(serie.code, self.code)
        self.assertEqual(serie.provider.name, self.provider)
        self.assertEqual(serie.varname, self.varname)
        self.assertEqual(serie.spc_varname, 'QH')
        self.assertEqual(serie.dtfmt, '%Y%m%d%H')
        self.assertEqual(serie.firstdt, dt(2014, 11, 4, 0))
        self.assertEqual(serie.lastdt, dt(2014, 11, 4, 12))
        self.assertEqual(serie.length, 13)
        self.assertEqual(serie.long_varname, 'Débit horaire')
        self.assertEqual(serie.np_dtype, np.float32)
        self.assertEqual(serie.timestep, td(hours=1))
        self.assertEqual(serie.timeunits, 'hour')
        self.assertEqual(serie.units, 'm3/s')
        self.assertTrue(serie.warning)

    def test_init_dtval(self):
        """
        Test de la création e l'instance - Cas Liste de (dt, float)
        """
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        serie = Serie(
            datval=self.list_dtval,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        assert_frame_equal(serie.data_frame, valid)
        assert_frame_equal(serie.df, valid)
        self.assertEqual(serie.code, self.code)
        self.assertEqual(serie.provider.name, self.provider)
        self.assertEqual(serie.varname, self.varname)
        self.assertEqual(serie.spc_varname, 'QH')
        self.assertEqual(serie.dtfmt, '%Y%m%d%H')
        self.assertEqual(serie.firstdt, dt(2014, 11, 4, 0))
        self.assertEqual(serie.lastdt, dt(2014, 11, 4, 12))
        self.assertEqual(serie.length, 13)
        self.assertEqual(serie.long_varname, 'Débit horaire')
        self.assertEqual(serie.np_dtype, np.float32)
        self.assertEqual(serie.timestep, td(hours=1))
        self.assertEqual(serie.timeunits, 'hour')
        self.assertEqual(serie.units, 'm3/s')
        self.assertTrue(serie.warning)

    def test_init_pnd(self):
        """
        Test de la création e l'instance - Cas Pandas
        """
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        assert_frame_equal(serie.data_frame, valid)
        assert_frame_equal(serie.df, valid)
        self.assertEqual(serie.code, self.code)
        self.assertEqual(serie.provider.name, self.provider)
        self.assertEqual(serie.varname, self.varname)
        self.assertEqual(serie.spc_varname, 'QH')
        self.assertEqual(serie.dtfmt, '%Y%m%d%H')
        self.assertEqual(serie.firstdt, dt(2014, 11, 4, 0))
        self.assertEqual(serie.lastdt, dt(2014, 11, 4, 12))
        self.assertEqual(serie.length, 13)
        self.assertEqual(serie.long_varname, 'Débit horaire')
        self.assertEqual(serie.np_dtype, np.float32)
        self.assertEqual(serie.timestep, td(hours=1))
        self.assertEqual(serie.timeunits, 'hour')
        self.assertEqual(serie.units, 'm3/s')
        self.assertTrue(serie.warning)

    def test_init_misc(self):
        """
        Test de la création e l'instance - Cas Mélange
        """
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        serie = Serie(
            datval=self.list_misc,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        assert_frame_equal(serie.data_frame, valid)
        assert_frame_equal(serie.df, valid)
        self.assertEqual(serie.code, self.code)
        self.assertEqual(serie.provider.name, self.provider)
        self.assertEqual(serie.varname, self.varname)
        self.assertEqual(serie.spc_varname, 'QH')
        self.assertEqual(serie.dtfmt, '%Y%m%d%H')
        self.assertEqual(serie.firstdt, dt(2014, 11, 4, 0))
        self.assertEqual(serie.lastdt, dt(2014, 11, 4, 12))
        self.assertEqual(serie.length, 13)
        self.assertEqual(serie.long_varname, 'Débit horaire')
        self.assertEqual(serie.np_dtype, np.float32)
        self.assertEqual(serie.timestep, td(hours=1))
        self.assertEqual(serie.timeunits, 'hour')
        self.assertEqual(serie.units, 'm3/s')
        self.assertTrue(serie.warning)

    def test_init_dupl(self):
        """
        Test de la création e l'instance - Cas avec doublon
        """
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        serie = Serie(
            datval=self.list_dupl,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        assert_frame_equal(serie.data_frame, valid)
        assert_frame_equal(serie.df, valid)
        self.assertEqual(serie.code, self.code)
        self.assertEqual(serie.provider.name, self.provider)
        self.assertEqual(serie.varname, self.varname)
        self.assertEqual(serie.spc_varname, 'QH')
        self.assertEqual(serie.dtfmt, '%Y%m%d%H')
        self.assertEqual(serie.firstdt, dt(2014, 11, 4, 0))
        self.assertEqual(serie.lastdt, dt(2014, 11, 4, 12))
        self.assertEqual(serie.length, 13)
        self.assertEqual(serie.long_varname, 'Débit horaire')
        self.assertEqual(serie.np_dtype, np.float32)
        self.assertEqual(serie.timestep, td(hours=1))
        self.assertEqual(serie.timeunits, 'hour')
        self.assertEqual(serie.units, 'm3/s')
        self.assertTrue(serie.warning)

    def test_init_mv_filled(self):
        """
        Test de la création e l'instance - Cas avec val. manq. + fill=True
        """
        valid = pnd.DataFrame([x[1] for x in self.valid_mv],
                              index=[x[0] for x in self.valid_mv],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        serie = Serie(
            datval=self.list_mv,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        assert_frame_equal(serie.data_frame, valid)
        assert_frame_equal(serie.df, valid)
        self.assertEqual(serie.code, self.code)
        self.assertEqual(serie.provider.name, self.provider)
        self.assertEqual(serie.varname, self.varname)
        self.assertEqual(serie.spc_varname, 'QH')
        self.assertEqual(serie.dtfmt, '%Y%m%d%H')
        self.assertEqual(serie.firstdt, dt(2014, 11, 4, 0))
        self.assertEqual(serie.lastdt, dt(2014, 11, 4, 12))
        self.assertEqual(serie.length, 13)
        self.assertEqual(serie.long_varname, 'Débit horaire')
        self.assertEqual(serie.np_dtype, np.float32)
        self.assertEqual(serie.timestep, td(hours=1))
        self.assertEqual(serie.timeunits, 'hour')
        self.assertEqual(serie.units, 'm3/s')
        self.assertTrue(serie.warning)

    def test_init_mv_nofilled(self):
        """
        Test de la création e l'instance - Cas avec val. manq. + fill=False
        """
        varname = 'QI'
        valid = pnd.DataFrame([x[1] for x in self.valid_mv_nofilled],
                              index=[x[0] for x in self.valid_mv_nofilled],
                              dtype=np.float32)
        valid.columns = [(self.code, varname)]
        serie = Serie(
            datval=self.list_mv,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        assert_frame_equal(serie.data_frame, valid)
        assert_frame_equal(serie.df, valid)
        self.assertEqual(serie.code, self.code)
        self.assertEqual(serie.provider.name, self.provider)
        self.assertEqual(serie.varname, varname)
        self.assertEqual(serie.spc_varname, varname)
        self.assertEqual(serie.dtfmt, '%Y%m%d%H%M')
        self.assertEqual(serie.firstdt, dt(2014, 11, 4, 0))
        self.assertEqual(serie.lastdt, dt(2014, 11, 4, 12))
        self.assertEqual(serie.length, 9)
        self.assertEqual(serie.long_varname, 'Débit instantané')
        self.assertEqual(serie.np_dtype, np.float32)
        self.assertIsNone(serie.timestep)
        self.assertIsNone(serie.timeunits)
        self.assertEqual(serie.units, 'm3/s')
        self.assertTrue(serie.warning)

#    def test_init_multits(self):
#        """
#        Test plusieurs pas de temps
#        """
#        # =====================================================================
#        varname = 'QJ'
#        serie = pnd.DataFrame(
#            {(self.code, varname): [27.382, 71.262, 26.112]},
#            index=pnd.date_range(
#                dt(2014, 11, 3),
#                dt(2014, 11, 5),
#                freq=td(days=1)
#            )
#        )
#        serie = Serie(
#            datval=serie,
#            code=self.code,
#            provider=self.provider,
#            varname=varname,
#        )
#        # =====================================================================
#        varname = 'PH'
#        serie = pnd.DataFrame(
#            {(self.code, varname): [1.2, 4.9, 34.2]},
#            index=pnd.date_range(
#                dt(2017, 6, 13, 18),
#                dt(2017, 6, 13, 20),
#                freq=td(hours=1)
#            )
#        )
#        serie = Serie(
#            datval=serie,
#            code=self.code,
#            provider=self.provider,
#            varname=varname,
#        )
#        # =====================================================================
#        varname = 'P6m'
#        dtval = [
#            [dt(2017, 6, 13, 17, 0), 0.0],
#            [dt(2017, 6, 13, 17, 6), 1.0],
#            [dt(2017, 6, 13, 17, 12), 0.2],
#            [dt(2017, 6, 13, 17, 18), 0.4],
#            [dt(2017, 6, 13, 17, 24), 0.4],
#            [dt(2017, 6, 13, 17, 30), 0.4],
#            [dt(2017, 6, 13, 17, 36), 2.2],
#            [dt(2017, 6, 13, 17, 42), 11.7],
#            [dt(2017, 6, 13, 17, 48), 13.3],
#            [dt(2017, 6, 13, 17, 54), 8.2],
#            [dt(2017, 6, 13, 18, 0), 6.3],
#        ]
#        serie = Serie(
#            datval=dtval,
#            code=self.code,
#            provider=self.provider,
#            varname=varname,
#        )
#        # =====================================================================
#        varname = 'QI'
#        dtval = [
#            [dt(2014, 11, 3, 23, 55), 35.600],
#            [dt(2014, 11, 4, 0, 15), 41.200],
#            [dt(2014, 11, 4, 2, 45), 46.900],
#            [dt(2014, 11, 4, 3, 30), 62.900],
#            [dt(2014, 11, 4, 4, 12), 85.700],
#            [dt(2014, 11, 4, 5, 58), 106.000],
#            [dt(2014, 11, 4, 6, 2), 118.000],
#            [dt(2014, 11, 4, 7, 59), 115.000],
#            [dt(2014, 11, 4, 8, 1), 103.000],
#            [dt(2014, 11, 4, 9, 30), 92.500],
#            [dt(2014, 11, 4, 10, 30), 86.400],
#            [dt(2014, 11, 4, 11, 45), 82.300],
#            [dt(2014, 11, 4, 12, 15), 78.300]
#        ]
#        serie = Serie(
#            datval=dtval,
#            code=self.code,
#            provider=self.provider,
#            varname=varname,
#        )
#        # =====================================================================
