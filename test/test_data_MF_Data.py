#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for MF_Data in pyspc.data.meteofrance

To run all tests just type:
    python -m unittest test_MF_Data

To run only a class test:
    python -m unittest test_MF_Data.TestMF_Data

To run only a specific test:
    python -m unittest test_MF_Data.TestMF_Data.test_init

"""
# Imports
import filecmp
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.data.meteofrance import MF_Data


class TestMF_Data(unittest.TestCase):
    """
    MF_Data class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'mf')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = 'toto.txt'
        data = MF_Data(filename=filename)
        self.assertEqual(data.filename, filename)

    def test_RR6(self):
        """
        Test de lecture/écriture de données RR6/P6M
        """
        # =====================================================================
        valid = pnd.DataFrame({
            'POSTE': ['43111002']*30,
            'DATE': ['201706131700', '201706131706', '201706131712',
                     '201706131718', '201706131724', '201706131730',
                     '201706131736', '201706131742', '201706131748',
                     '201706131754', '201706131800', '201706131806',
                     '201706131812', '201706131818', '201706131824',
                     '201706131830', '201706131836', '201706131842',
                     '201706131848', '201706131854', '201706131900',
                     '201706131906', '201706131912', '201706131918',
                     '201706131924', '201706131930', '201706131936',
                     '201706131942', '201706131948', '201706131954'],
            'RR6': [0.0, 1.0, 0.2, 0.4, 0.4, 0.4, 2.2, 11.7,
                    13.3, 8.2, 6.3, 13.4, 14.2, 12.9, 21.5,
                    14.1, 17.0, 17.0, 8.0, 2.8, 1.8, 6.1,
                    11.2, 6.6, 3.5, 1.6, 0.4, 0.6, 5.0, 1.8],
            'QRR6': ['v']*30,
        })
        # =====================================================================
        filename = os.path.join(self.dirname, 'RR6.data')
        reader = MF_Data(filename=filename)
        content = reader.read()
        assert_frame_equal(content, valid)
        # =====================================================================
        tmpfile = os.path.join('data', os.path.basename(filename))
        writer = MF_Data(filename=tmpfile)
        writer.write(valid)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_RR1(self):
        """
        Test de lecture/écriture de données RR1/PH
        """
        # =====================================================================
        valid = pnd.DataFrame({
            'POSTE': ['42039003']*25,
            'DATE': ['2010111412', '2010111413', '2010111414', '2010111415',
                     '2010111416', '2010111417', '2010111418', '2010111419',
                     '2010111420', '2010111421', '2010111422', '2010111423',
                     '2010111500', '2010111501', '2010111502', '2010111503',
                     '2010111504', '2010111505', '2010111506', '2010111507',
                     '2010111508', '2010111509', '2010111510', '2010111511',
                     '2010111512'],
            'RR1': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5.0, 1.4, 1.2, 1.2, 1.6,
                    5.5, 5.2, 6.4, 4.8, 3.6, 4.9, 5.8, 3.6, 1.6, 0.8, 1.2, 0.8,
                    0.2],
            'QRR1': ['v']*25,
        })
        # =====================================================================
        filename = os.path.join(self.dirname, 'RR1.data')
        reader = MF_Data(filename=filename)
        content = reader.read()
        assert_frame_equal(content, valid)
        # =====================================================================
        tmpfile = os.path.join('data', os.path.basename(filename))
        writer = MF_Data(filename=tmpfile)
        writer.write(valid)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_RR(self):
        """
        Test de lecture/écriture de données RR/PJ
        """
        # =====================================================================
        valid = pnd.DataFrame({
            'POSTE': sorted(['07075001', '07105001', '07154005']*10),
            'DATE': ['20130510', '20130511', '20130512', '20130513',
                     '20130514', '20130515', '20130516', '20130517',
                     '20130518', '20130519', '20130510', '20130511',
                     '20130512', '20130513', '20130514', '20130515',
                     '20130516', '20130517', '20130518', '20130519',
                     '20130510', '20130511', '20130512', '20130513',
                     '20130514', '20130515', '20130516', '20130517',
                     '20130518', '20130519'],
            'RR': [0.0, 0.0, 0.0, 0.0, 3.6, 37.0, 2.5, 23.5, 71.0, 0.0, 0.2,
                   0.1, 0.0, 0.2, 2.2, 28.8, 1.6, 26.7, 65.7, 0.1, 0.0, 0.0,
                   0.0, 0.0, 4.6, 42.2, 2.6, 30.0, 74.3, 1.0],
            'QRR': ['v']*30,
        })
        # =====================================================================
        filename = os.path.join(self.dirname, 'RR.data')
        reader = MF_Data(filename=filename)
        content = reader.read()
        assert_frame_equal(content, valid)
        # =====================================================================
        tmpfile = os.path.join('data', os.path.basename(filename))
        writer = MF_Data(filename=tmpfile)
        writer.write(valid)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_T(self):
        """
        Test de lecture/écriture de données T/TH
        """
        # =====================================================================
        valid = pnd.DataFrame({
            'POSTE': ['21567001']*24,
            'DATE': ['2000020400', '2000020401', '2000020402', '2000020403',
                     '2000020404', '2000020405', '2000020406', '2000020407',
                     '2000020408', '2000020409', '2000020410', '2000020411',
                     '2000020412', '2000020413', '2000020414', '2000020415',
                     '2000020416', '2000020417', '2000020418', '2000020419',
                     '2000020420', '2000020421', '2000020422', '2000020423'],
            'T': [1.9, 1.2, 0.3, -1.0, -1.4, -1.4, -2.6, -2.6, -2.7, -1.9,
                  -0.5, 2.2, 6.0, 7.6, 8.2, 7.4, 6.1, 4.7, 3.7, 2.6, 2.3,
                  1.8, 0.5, 0.1],
            'QT': ['v']*24,
        })
        # =====================================================================
        filename = os.path.join(self.dirname, 'T.data')
        reader = MF_Data(filename=filename)
        content = reader.read()
        assert_frame_equal(content, valid)
        # =====================================================================
        tmpfile = os.path.join('data', os.path.basename(filename))
        writer = MF_Data(filename=tmpfile)
        writer.write(valid)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_TNTXM(self):
        """
        Test de lecture/écriture de données TNTXM/TJ
        """
        # =====================================================================
        valid = pnd.DataFrame({
            'POSTE': sorted(['03060001', '18125004']*9),
            'DATE': ['20191001', '20191002', '20191003', '20191004',
                     '20191005', '20191006', '20191007', '20191008',
                     '20191009', '20191001', '20191002', '20191003',
                     '20191004', '20191005', '20191006', '20191007',
                     '20191008', '20191009'],
            'TNTXM': [16.2, 15.1, 9.9, 11.9, 14.3, 12.8, 10.8, 16.4, 15.3,
                      15.9, 15.3, 8.4, 11.3, 15.6, 16, 12.3, 16.2, 14.3],
            'QTNTXM': ['v']*18,
        })
        # =====================================================================
        filename = os.path.join(self.dirname, 'TNTXM.data')
        reader = MF_Data(filename=filename)
        content = reader.read()
        assert_frame_equal(content, valid)
        # =====================================================================
        tmpfile = os.path.join('data', os.path.basename(filename))
        writer = MF_Data(filename=tmpfile)
        writer.write(valid)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_ETPMON(self):
        """
        Test de lecture/écriture de données ETPMON/EJ
        """
        # =====================================================================
        valid = pnd.DataFrame({
            'POSTE': sorted(['43111002', '63113001']*9),
            'DATE': ['20180701', '20180702', '20180703', '20180704',
                     '20180705', '20180706', '20180707', '20180708',
                     '20180709', '20180701', '20180702', '20180703',
                     '20180704', '20180705', '20180706', '20180707',
                     '20180708', '20180709'],
            'ETPMON': [6.5, 5.4, 4.3, 4.8, 3.3, 2.5, 5.2, 4.9, 5.4, 7.6, 6.4,
                       5.7, 4.2, 3.9, 4.3, 5.6, 5.9, 5.9],
            'QETPMON': ['v']*18,
        })
        # =====================================================================
        filename = os.path.join(self.dirname, 'ETPMON.data')
        reader = MF_Data(filename=filename)
        content = reader.read()
        assert_frame_equal(content, valid)
        # =====================================================================
        tmpfile = os.path.join('data', os.path.basename(filename))
        writer = MF_Data(filename=tmpfile)
        writer.write(valid)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
