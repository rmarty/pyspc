#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary hydro2csv

To run all tests just type:
    python -m unittest test_bin_hydro2csv

To run only a class test:
    python -m unittest test_bin_hydro2csv.Test_hydro2csv

To run only a specific test:
    python -m unittest test_bin_hydro2csv.Test_hydro2csv.test_QTVAR

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import hydro2csv as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_hydro2csv(unittest.TestCase):
    """
    hydro2csv bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'hydro2csv')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_hydro2 = os.path.join('data', 'data', 'hydro2')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_grp16_QTFIX(self):
        """
        Test de la conversion de fichier QTFIX
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/hydro2csv.py',
            '-d', 'qtfix.txt',
            '-I', self.dir_hydro2,
            '-O', self.dir_out,
            '-n', 'QH',
            '-C', 'grp16'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydro2csv()
        self.assertEqual(args.data_filename, 'qtfix.txt')
        self.assertEqual(args.varname, 'QH')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.onefile, False)
        self.assertEqual(args.csv_type, 'grp16')
        self.assertEqual(args.input_dir, self.dir_hydro2)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for filename in os.listdir(self.dir_out):
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dir_ctl, 'grp16', filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier Hydro2 {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "Les données correspondent à la grandeur {n}."\
            ""\
            "".format(
                I=args.input_dir, d=args.data_filename, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_grp18_QTVAR(self):
        """
        Test de la conversion de fichier QTVAR
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/hydro2csv.py',
            '-d', 'qtvar.txt',
            '-I', self.dir_hydro2,
            '-O', self.dir_out,
            '-n', 'QI',
            '-C', 'grp18'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydro2csv()
        self.assertEqual(args.data_filename, 'qtvar.txt')
        self.assertEqual(args.varname, 'QI')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.onefile, False)
        self.assertEqual(args.csv_type, 'grp18')
        self.assertEqual(args.input_dir, self.dir_hydro2)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for filename in os.listdir(self.dir_out):
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dir_ctl, 'grp18', filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier Hydro2 {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "Les données correspondent à la grandeur {n}."\
            ""\
            "".format(
                I=args.input_dir, d=args.data_filename, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_pyspc_QTFIX(self):
        """
        Test de la conversion de fichier QTFIX
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/hydro2csv.py',
            '-d', 'qtfix.txt',
            '-I', self.dir_hydro2,
            '-O', self.dir_out,
            '-n', 'QH',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydro2csv()
        self.assertEqual(args.data_filename, 'qtfix.txt')
        self.assertEqual(args.varname, 'QH')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.onefile, False)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.input_dir, self.dir_hydro2)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for filename in os.listdir(self.dir_out):
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dir_ctl, filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier Hydro2 {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "Les données correspondent à la grandeur {n}."\
            ""\
            "".format(
                I=args.input_dir, d=args.data_filename, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_pyspc_QTVAR(self):
        """
        Test de la conversion de fichier QTVAR
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/hydro2csv.py',
            '-d', 'qtvar.txt',
            '-I', self.dir_hydro2,
            '-O', self.dir_out,
            '-n', 'QI',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydro2csv()
        self.assertEqual(args.data_filename, 'qtvar.txt')
        self.assertEqual(args.varname, 'QI')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.onefile, False)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.input_dir, self.dir_hydro2)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for filename in os.listdir(self.dir_out):
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dir_ctl, filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier Hydro2 {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "Les données correspondent à la grandeur {n}."\
            ""\
            "".format(
                I=args.input_dir, d=args.data_filename, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_pyspc_HTEMPS(self):
        """
        Test de la conversion de fichier HTEMPS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/hydro2csv.py',
            '-d', 'htemps.txt',
            '-I', self.dir_hydro2,
            '-O', self.dir_out,
            '-n', 'HI',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydro2csv()
        self.assertEqual(args.data_filename, 'htemps.txt')
        self.assertEqual(args.varname, 'HI')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.onefile, False)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.input_dir, self.dir_hydro2)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for filename in os.listdir(self.dir_out):
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dir_ctl, filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier Hydro2 {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "Les données correspondent à la grandeur {n}."\
            ""\
            "".format(
                I=args.input_dir, d=args.data_filename, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_pyspc_QJM(self):
        """
        Test de la conversion de fichier QJM
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/hydro2csv.py',
            '-d', 'qjm.txt',
            '-I', self.dir_hydro2,
            '-O', self.dir_out,
            '-n', 'QJ',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydro2csv()
        self.assertEqual(args.data_filename, 'qjm.txt')
        self.assertEqual(args.varname, 'QJ')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.onefile, False)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.input_dir, self.dir_hydro2)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for filename in os.listdir(self.dir_out):
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dir_ctl, filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier Hydro2 {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "Les données correspondent à la grandeur {n}."\
            ""\
            "".format(
                I=args.input_dir, d=args.data_filename, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_pyspc_TOUSMOIS(self):
        """
        Test de la conversion de fichier TOUSMOIS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/hydro2csv.py',
            '-d', 'tousmois.txt',
            '-I', self.dir_hydro2,
            '-O', self.dir_out,
            '-n', 'QM',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydro2csv()
        self.assertEqual(args.data_filename, 'tousmois.txt')
        self.assertEqual(args.varname, 'QM')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.onefile, False)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.input_dir, self.dir_hydro2)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for filename in os.listdir(self.dir_out):
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dir_ctl, filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier Hydro2 {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "Les données correspondent à la grandeur {n}."\
            ""\
            "".format(
                I=args.input_dir, d=args.data_filename, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================
