#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP_Verif in pyspc.model.grp18

To run all tests just type:
    python -m unittest test_model_GRP18_CAL_Verif

To run only a class test:
    python -m unittest test_model_GRP18_CAL_Verif.TestGRP_Verif

To run only a specific test:
    python -m unittest test_model_GRP18_CAL_Verif.TestGRP_Verif.test_init

"""
# Imports
import os
import unittest

from pyspc.model.grp18.cal_verif import GRP_Verif


class TestGRP_Verif(unittest.TestCase):
    """
    GRP_Verif class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp18', 'cal')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(
            self.dirname,
            'Perf_CALAG_GRP_RH10585x_PDT_00J01H00M_HOR_00J03H00M_SV_5.DAT'
        )
        # =====================================================================
        reader = GRP_Verif(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.datatype, 'rtime')
        self.assertEqual(reader.model, 'GRP')
        self.assertEqual(reader.loc, 'RH10585x')
        self.assertEqual(reader.timestep, '00J01H00M')
        self.assertEqual(reader.leadtime, '00J03H00M')
        self.assertEqual(reader.threshold, '5')
        # =====================================================================

    def test_splitbasename(self):
        """
        Test de la configuration lue à partir d'une ficher de type CAL
        """
        # =====================================================================
        filename = os.path.join(
            self.dirname,
            'Perf_CALAG_GRP_RH10585x_PDT_00J01H00M_HOR_00J03H00M_SV_5.DAT'
        )
        # =====================================================================
        meta = GRP_Verif.split_basename(filename=filename)
        self.assertEqual(meta['datatype'], 'rtime')
        self.assertEqual(meta['model'], 'GRP')
        self.assertEqual(meta['loc'], 'RH10585x')
        self.assertEqual(meta['timestep'], '00J01H00M')
        self.assertEqual(meta['leadtime'], '00J03H00M')
        self.assertEqual(meta['threshold'], '5')
        # =====================================================================

    def test_read_cal(self):
        """
        Test de la lecture d'une ficher de type CAL
        """
        # =====================================================================
        valids = {
            'SMN_TAN': {'Eff': 0.795, 'POD': 68.9, 'FAR': 32.5, 'CSI': 51.7},
            'SMN_RNA': {'Eff': 0.811, 'POD': 68.9, 'FAR': 32.0, 'CSI': 52.0},
            'AMN_TAN': {'Eff': 0.808, 'POD': 70.2, 'FAR': 33.8, 'CSI': 51.7},
        }
        # =====================================================================
        filename = os.path.join(
            self.dirname,
            'Perf_TESTS_GRP_RH10585x_PDT_00J01H00M_HOR_00J03H00M_SV_10.DAT'
        )
        # =====================================================================
        reader = GRP_Verif(filename=filename)
        data = reader.read()
        self.assertDictEqual(data, valids)
        # =====================================================================

    def test_read_rtime(self):
        """
        Test de la lecture d'une ficher de type RTIME
        """
        # =====================================================================
        valids = {
            'SMN_TAN': {'Eff': 0.807, 'POD': 78.2, 'FAR': 40.7, 'CSI': 50.9},
        }
        # =====================================================================
        filename = os.path.join(
            self.dirname,
            'Perf_CALAG_GRP_RH10585x_PDT_00J01H00M_HOR_00J03H00M_SV_5.DAT'
        )
        # =====================================================================
        reader = GRP_Verif(filename=filename)
        data = reader.read()
        self.assertDictEqual(data, valids)
        # =====================================================================

    def test_datatype(self):
        """
        Test des types de fiche de performance
        """
        valids = ['cal', 'rtime']
        self.assertEqual(GRP_Verif.get_datatypes(), valids)
