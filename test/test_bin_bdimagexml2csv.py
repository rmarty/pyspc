#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary bdimagexml2csv

To run all tests just type:
    python -m unittest test_bin_bdimagexml2csv

To run only a class test:
    python -m unittest test_bin_bdimagexml2csv.Test_bdimagexml2csv

To run only a specific test:
    python -m unittest test_bin_bdimagexml2csv.Test_bdimagexml2csv.test_obs_stats_pix

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import bdimagexml2csv as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_bdimagexml2csv(unittest.TestCase):
    """
    _bdimagexml2csv class test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'bdimagexml2csv')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_in2 = os.path.join('data', 'data', 'lamedo')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_obs_stats_pix(self):
        """
        Test OBS - STATS - PIXELS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimagexml2csv.py',
            '-O', self.dir_out,
            '-I', self.dir_in2,
            '-d', 'getObsStatsByPixels_sim-t-t.xml',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.bdimagexml2csv()
        self.assertEqual(args.xml_filename, 'getObsStatsByPixels_sim-t-t.xml')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.input_dir, self.dir_in2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.ratio_level,
                         {'ratio_image': 1, 'ratio_stats': 1})
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        self.assertEqual(args.onefile, True)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'pixels_TI.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier BdImage {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O} "\
            ", en appliquant les ratios par défaut : {U} "\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                U=args.ratio_level,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_obs_stats_zones_ratio(self):
        """
        Test OBS - STATS - ZONES - RATIO
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimagexml2csv.py',
            '-O', self.dir_out,
            '-I', self.dir_in2,
            '-d', 'getObsStatsByZones_antilope-j1-rr_010000_RATIO.xml',
            '-U', 'ratio_image', '0.8',
            '-U', 'ratio_stats', '0.5',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.bdimagexml2csv()
        self.assertEqual(args.xml_filename,
                         'getObsStatsByZones_antilope-j1-rr_010000_RATIO.xml')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.input_dir, self.dir_in2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.ratio_level,
                         {'ratio_image': 0.8, 'ratio_stats': 0.5})
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        self.assertEqual(args.onefile, True)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=subprocess.DEVNULL, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'LO8060_PJ.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier BdImage {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O} "\
            ", en spécifiant les ratios déclarant une image/stat manquante "\
            ": {U} "\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                U=args.ratio_level,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_obs_values_bbox(self):
        """
        Test OBS - VALUES - BBOX
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimagexml2csv.py',
            '-O', self.dir_out,
            '-I', self.dir_in2,
            '-d', 'getObsValuesByBBox_antilope-j1-rr_010000.xml',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.bdimagexml2csv()
        self.assertEqual(args.xml_filename,
                         'getObsValuesByBBox_antilope-j1-rr_010000.xml')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.input_dir, self.dir_in2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.ratio_level,
                         {'ratio_image': 1, 'ratio_stats': 1})
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        self.assertEqual(args.onefile, True)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'BdImage_PJ.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier BdImage {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O} "\
            ", en appliquant les ratios par défaut : {U} "\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                U=args.ratio_level,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_fcst_stats_pix(self):
        """
        Test FCST - STATS - PIXELS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimagexml2csv.py',
            '-O', self.dir_out,
            '-I', self.dir_in2,
            '-d', 'getPrevByNetworkStatsByPixels_sympo-t-t.xml',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.bdimagexml2csv()
        self.assertEqual(args.xml_filename,
                         'getPrevByNetworkStatsByPixels_sympo-t-t.xml')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.input_dir, self.dir_in2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.ratio_level,
                         {'ratio_image': 1, 'ratio_stats': 1})
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        self.assertEqual(args.onefile, True)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'pixels_2021051006_sympo_t_TI.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier BdImage {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O} "\
            ", en appliquant les ratios par défaut : {U} "\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                U=args.ratio_level,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_fcst_stats_zones(self):
        """
        Test FCST - STATS - ZONES
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimagexml2csv.py',
            '-O', self.dir_out,
            '-I', self.dir_in2,
            '-d', 'getPrevByNetworkStatsByZones_sympo-rr-rr.xml',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.bdimagexml2csv()
        self.assertEqual(args.xml_filename,
                         'getPrevByNetworkStatsByZones_sympo-rr-rr.xml')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.input_dir, self.dir_in2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.ratio_level,
                         {'ratio_image': 1, 'ratio_stats': 1})
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        self.assertEqual(args.onefile, True)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'LO8060_2021051006_sympo_rr_P3H.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier BdImage {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O} "\
            ", en appliquant les ratios par défaut : {U} "\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                U=args.ratio_level,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_fcst_stats_zones_arpege_TI(self):
        """
        Test FCST - STATS - ZONES
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimagexml2csv.py',
            '-O', self.dir_out,
            '-I', self.dir_in2,
            '-d', 'getPrevByNetworkStatsByZones_arpege-t-t.xml',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.bdimagexml2csv()
        self.assertEqual(args.xml_filename,
                         'getPrevByNetworkStatsByZones_arpege-t-t.xml')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.input_dir, self.dir_in2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.ratio_level,
                         {'ratio_image': 1, 'ratio_stats': 1})
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        self.assertEqual(args.onefile, True)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'LO8060_2022033106_arpege_t_TI.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier BdImage {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O} "\
            ", en appliquant les ratios par défaut : {U} "\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                U=args.ratio_level,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_fcst_stats_zones_arome_PH(self):
        """
        Test FCST - STATS - ZONES
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimagexml2csv.py',
            '-O', self.dir_out,
            '-I', self.dir_in2,
            '-d', 'getPrevByNetworkStatsByZones_arome-rr-total.xml',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.bdimagexml2csv()
        self.assertEqual(args.xml_filename,
                         'getPrevByNetworkStatsByZones_arome-rr-total.xml')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.input_dir, self.dir_in2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.ratio_level,
                         {'ratio_image': 1, 'ratio_stats': 1})
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        self.assertEqual(args.onefile, True)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'LO8060_2022033106_arome_rr_PH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier BdImage {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O} "\
            ", en appliquant les ratios par défaut : {U} "\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                U=args.ratio_level,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_fcst_stats_zones_aromeifs_PH(self):
        """
        Test FCST - STATS - ZONES
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimagexml2csv.py',
            '-O', self.dir_out,
            '-I', self.dir_in2,
            '-d', 'getPrevByNetworkStatsByZones_arome-ifs-rr-total.xml',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.bdimagexml2csv()
        self.assertEqual(args.xml_filename,
                         'getPrevByNetworkStatsByZones_arome-ifs-rr-total.xml')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.input_dir, self.dir_in2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.ratio_level,
                         {'ratio_image': 1, 'ratio_stats': 1})
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        self.assertEqual(args.onefile, True)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'LO8060_2022033112_arome-ifs_rr_PH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier BdImage {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O} "\
            ", en appliquant les ratios par défaut : {U} "\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                U=args.ratio_level,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_fcst_stats_zones_aromepi_P15m(self):
        """
        Test FCST - STATS - ZONES
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimagexml2csv.py',
            '-O', self.dir_out,
            '-I', self.dir_in2,
            '-d', 'getPrevByNetworkStatsByZones_arome-pi-rr-total.xml',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.bdimagexml2csv()
        self.assertEqual(args.xml_filename,
                         'getPrevByNetworkStatsByZones_arome-pi-rr-total.xml')
#        self.assertEqual(args.overwrite, True)
        self.assertEqual(args.input_dir, self.dir_in2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.ratio_level,
                         {'ratio_image': 1, 'ratio_stats': 1})
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        self.assertEqual(args.onefile, False)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'LO808_2023083109_arome-pi_rr_moy_P15m.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier BdImage {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O} "\
            ", en appliquant les ratios par défaut : {U} "\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                U=args.ratio_level,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================
