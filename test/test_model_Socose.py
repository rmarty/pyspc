#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for ETP by Oudin in pyspc.model.oudin

To run all tests just type:
    python -m unittest test_model_Oudin

To run only a class test:
    python -m unittest test_model_Oudin.TestOudin

To run only a specific test:
    python -m unittest test_model_Oudin.TestOudin.test_oudin_etpj

"""
# Imports
from datetime import timedelta as td
import pandas as pnd
import unittest

# Imports pyspc
from pyspc.model.socose import socose


class TestSocose(unittest.TestCase):
    """
    Functions class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """

    def test_socose(self):
        """
        Test Socose
        """
        # =====================================================================
        df = pnd.DataFrame(
            [0.167761, 0.185996, 0.223195, 0.390956, 0.674690, 0.870897,
             0.965718, 1.000000, 0.983953, 0.945295, 0.884026, 0.814734,
             0.744712, 0.683443, 0.617068, 0.558716, 0.506929, 0.466083,
             0.431072, 0.401167, 0.371991, 0.350839, 0.334063, 0.317287,
             0.305616],
            index=[td(hours=x) for x in range(-7, 18, 1)]
        )
        df.columns = [('K0000000', 'QH')]
        valid = {('K0000000', 'QH'): {'d': 13, 'rxd': 1.268270287129564}}
        # =====================================================================
        values = socose(df)
        self.assertDictEqual(values, valid)
        # =====================================================================
