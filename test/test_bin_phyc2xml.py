#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary phyc2xml

To run all tests just type:
    python -m unittest test_bin_phyc2xml

To run only a class test:
    python -m unittest test_bin_phyc2xml.Test_phyc2xml

To run only a specific test:
    python -m unittest test_bin_phyc2xml.Test_phyc2xml.test_loc_hydro

"""
# Imports
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import phyc2xml as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_phyc2xml(unittest.TestCase):
    """
    phyc2xml bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'phyc2xml')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dirname2 = os.path.join('data', 'schapi', 'phyc')
        cls.file_cfg = os.path.join('..', 'bin', 'phyc2xml_RM.txt')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.stations_list_file = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)

    def test_data_fcst_hydro(self):
        """
        Test requête PHyC (data_fcst_hydro)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/phyc2xml.py',
            '-s', 'K4350010',
            '-t', 'data_fcst_hydro',
            '-F', '20190101',
            '-L', '20190103',
            '-n', 'QH',
            '-c', self.file_cfg,
            '-O', self.dir_out
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.phyc2xml()
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.station_name, 'K4350010')
        self.assertEqual(args.datatype, 'data_fcst_hydro')
        self.assertEqual(args.first_dtime, '20190101')
        self.assertEqual(args.last_dtime, '20190103')
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        filename = os.path.join(
            self.dir_out, 'K4350010_201901010000_201901030000_Q_fcst.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Récupérer des prévisions hydrologiques ({t}) "\
            "stockées en PHyC pour le lieu {s}, la grandeur {n}, "\
            "la période du {F} au {L} "\
            "et enregistrer le fichier xml Sandre dans le répertoire {O}. "\
            "L'adresse de la PHyC et les identifiants de la connexion sont "\
            "renseignés dans le fichier {c}"\
            .format(
                t=args.datatype, c=args.cfg_filename,
                F=args.first_dtime, L=args.last_dtime,
                n=args.varname, s=args.station_name, O=args.output_dir)
        # =====================================================================

    def test_data_obs_hydro(self):
        """
        Test requête PHyC (data_obs_hydro)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/phyc2xml.py',
            '-s', 'K0550010',
            '-t', 'data_obs_hydro',
            '-F', '20141101',
            '-L', '20141110',
            '-n', 'QH',
            '-c', self.file_cfg,
            '-O', self.dir_out
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.phyc2xml()
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.station_name, 'K0550010')
        self.assertEqual(args.datatype, 'data_obs_hydro')
        self.assertEqual(args.first_dtime, '20141101')
        self.assertEqual(args.last_dtime, '20141110')
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        filename = os.path.join(
            self.dir_out, 'K0550010_201411010000_201411100000_Q_obs.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Récupérer des observations hydrologiques ({t}) "\
            "stockées en PHyC pour le lieu {s}, la grandeur {n}, "\
            "la période du {F} au {L} "\
            "et enregistrer le fichier xml Sandre dans le répertoire {O}. "\
            "L'adresse de la PHyC et les identifiants de la connexion sont "\
            "renseignés dans le fichier {c}"\
            .format(
                t=args.datatype, c=args.cfg_filename,
                F=args.first_dtime, L=args.last_dtime,
                n=args.varname, s=args.station_name, O=args.output_dir)
        # =====================================================================

    def test_data_obs_meteo(self):
        """
        Test requête PHyC (data_obs_meteo)
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'list_pluvio.txt')
        processArgs = [
            'python',
            '../bin/phyc2xml.py',
            '-l', self.stations_list_file,
            '-t', 'data_obs_meteo',
            '-F', '20141101',
            '-L', '20141110',
            '-n', 'PH',
            '-c', self.file_cfg,
            '-O', self.dir_out
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.phyc2xml()
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.stations_list_file,
                         os.path.join(self.dir_in, 'list_pluvio.txt'))
        self.assertEqual(args.varname, 'PH')
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.datatype, 'data_obs_meteo')
        self.assertEqual(args.first_dtime, '20141101')
        self.assertEqual(args.last_dtime, '20141110')
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        filename = os.path.join(
            self.dir_out, '43091005_201411010000_201411100000_RR_obs.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        filename = os.path.join(
            self.dir_out, '43130002_201411010000_201411100000_RR_obs.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Récupérer des observations météorologiques ({t}) "\
            "stockées en PHyC pour les lieux listés dans {l}, la grandeur "\
            "{n}, la période du {F} au {L} "\
            "et enregistrer le fichier xml Sandre dans le répertoire {O}. "\
            "L'adresse de la PHyC et les identifiants de la connexion sont "\
            "renseignés dans le fichier {c}"\
            .format(
                t=args.datatype, c=args.cfg_filename,
                F=args.first_dtime, L=args.last_dtime,
                n=args.varname, l=args.stations_list_file, O=args.output_dir)
        # =====================================================================

    def test_flowmes(self):
        """
        Test requête PHyC (flowmes) - NOT IMPLEMENTED IN PHYC
        """

    def test_levelcor(self):
        """
        Test requête PHyC (levelcor)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/phyc2xml.py',
            '-s', 'K055001010',
            '-t', 'levelcor',
            '-F', '20080101',
            '-L', '20181231',
            '-c', self.file_cfg,
            '-O', self.dir_out
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.phyc2xml()
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, 'K055001010')
        self.assertEqual(args.datatype, 'levelcor')
        self.assertEqual(args.first_dtime, '20080101')
        self.assertEqual(args.last_dtime, '20181231')
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        filename = os.path.join(
            self.dir_out, 'K055001010_200801010000_201812310000_levelcor.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Récupérer des courbes de correction ({t}) "\
            "stockées en PHyC pour le lieu {s}, la grandeur "\
            "{n}, la période du {F} au {L} "\
            "et enregistrer le fichier xml Sandre dans le répertoire {O}. "\
            "L'adresse de la PHyC et les identifiants de la connexion sont "\
            "renseignés dans le fichier {c}"\
            .format(
                t=args.datatype, c=args.cfg_filename,
                F=args.first_dtime, L=args.last_dtime,
                n=args.varname, s=args.station_name, O=args.output_dir)
        # =====================================================================

    def test_loc_hydro(self):
        """
        Test requête PHyC (loc_hydro)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/phyc2xml.py',
            '-s', 'K0550010',
            '-t', 'loc_hydro',
            '-c', self.file_cfg,
            '-O', self.dir_out
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.phyc2xml()
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, 'K0550010')
        self.assertEqual(args.datatype, 'loc_hydro')
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        filename = os.path.join(
            self.dir_out, 'K0550010_SiteHydro.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Récupérer des informations sur les lieux "\
            "hydrométriques ({t}) stockées en PHyC pour le lieu {s}, "\
            "et enregistrer le fichier xml Sandre dans le répertoire {O}. "\
            "L'adresse de la PHyC et les identifiants de la connexion sont "\
            "renseignés dans le fichier {c}"\
            .format(
                t=args.datatype, c=args.cfg_filename,
                s=args.station_name, O=args.output_dir)
        # =====================================================================

    def test_loc_hydro_child(self):
        """
        Test requête PHyC (loc_hydro_child)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/phyc2xml.py',
            '-s', 'K025',
            '-t', 'loc_hydro_child',
            '-c', self.file_cfg,
            '-O', self.dir_out
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.phyc2xml()
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, 'K025')
        self.assertEqual(args.datatype, 'loc_hydro_child')
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        filename = os.path.join(
            self.dir_out, 'K025_ZoneHydro_child.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Récupérer des informations sur les lieux "\
            "hydrométriques ({t}) stockées en PHyC pour le lieu {s}, "\
            "et enregistrer le fichier xml Sandre dans le répertoire {O}. "\
            "L'adresse de la PHyC et les identifiants de la connexion sont "\
            "renseignés dans le fichier {c}"\
            .format(
                t=args.datatype, c=args.cfg_filename,
                s=args.station_name, O=args.output_dir)
        # =====================================================================

    def test_loc_meteo(self):
        """
        Test requête PHyC (loc_meteo)
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'list_pluvio.txt')
        processArgs = [
            'python',
            '../bin/phyc2xml.py',
            '-l', self.stations_list_file,
            '-t', 'loc_meteo',
            '-c', self.file_cfg,
            '-O', self.dir_out
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.phyc2xml()
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.datatype, 'loc_meteo')
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        filename = os.path.join(
            self.dir_out, '43130002_SiteMeteo.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        filename = os.path.join(
            self.dir_out, '43091005_SiteMeteo.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Récupérer des informations sur les lieux "\
            "météorologiques ({t}) stockées en PHyC pour les lieux listés "\
            "dans {l} et enregistrer le fichier xml Sandre dans le "\
            "répertoire {O}. L'adresse de la PHyC et les identifiants de la "\
            "connexion sont renseignés dans le fichier {c}"\
            .format(
                t=args.datatype, c=args.cfg_filename,
                l=args.stations_list_file, O=args.output_dir)
        # =====================================================================

    def test_ratingcurve(self):
        """
        Test requête PHyC (ratingcurve)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/phyc2xml.py',
            '-s', 'K055001010',
            '-t', 'ratingcurve',
            '-F', '20080101',
            '-L', '20131231',
            '-c', self.file_cfg,
            '-O', self.dir_out
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.phyc2xml()
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, 'K055001010')
        self.assertEqual(args.datatype, 'ratingcurve')
        self.assertEqual(args.first_dtime, '20080101')
        self.assertEqual(args.last_dtime, '20131231')
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        filename = os.path.join(
            self.dir_out,
            'K055001010_200801010000_201312310000_ratingcurve.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Récupérer des courbes de tarage ({t}) "\
            "stockées en PHyC pour le lieu {s}, la grandeur "\
            "{n}, la période du {F} au {L} "\
            "et enregistrer le fichier xml Sandre dans le répertoire {O}. "\
            "L'adresse de la PHyC et les identifiants de la connexion sont "\
            "renseignés dans le fichier {c}"\
            .format(
                t=args.datatype, c=args.cfg_filename,
                F=args.first_dtime, L=args.last_dtime,
                n=args.varname, s=args.station_name, O=args.output_dir)
        # =====================================================================

    def test_user(self):
        """
        Test requête PHyC (user)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/phyc2xml.py',
            '-s', '398',
            '-t', 'user',
            '-c', self.file_cfg,
            '-O', self.dir_out
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.phyc2xml()
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, '398')
        self.assertEqual(args.datatype, 'user')
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        filenames = [
            os.path.join(self.dir_out, '398_user_admin.xml'),
            os.path.join(self.dir_out, '398_user_loc_meteo.xml'),
            os.path.join(self.dir_out, '398_user_site_hydro.xml'),
            os.path.join(self.dir_out, '398_user_station_hydro.xml'),
        ]
        for f in filenames:
            self.assertTrue(os.path.exists(f))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Récupérer des informations utilisateur ({t}) "\
            "stockées en PHyC pour l'utilisateur {s} "\
            "et enregistrer le fichier xml Sandre dans le répertoire {O}. "\
            "L'adresse de la PHyC et les identifiants de la connexion sont "\
            "renseignés dans le fichier {c}"\
            .format(
                t=args.datatype, c=args.cfg_filename,
                s=args.station_name, O=args.output_dir)
        # =====================================================================
