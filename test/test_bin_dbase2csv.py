#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary dbase2csv

To run all tests just type:
    python -m unittest test_bin_dbase2csv

To run only a class test:
    python -m unittest test_bin_dbase2csv.Test_dbase2csv

To run only a specific test:
    python -m unittest test_bin_dbase2csv.Test_dbase2csv.test_sacha_Q_h2

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import dbase2csv as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_dbase2csv(unittest.TestCase):
    """
    dbase2csv bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'dbase2csv')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_dbase = os.path.join('data', 'io', 'dbase')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_sacha_Q_h2_grp16(self):
        """
        Tests SACHA Historique - Débit - Hydro2
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2csv.py',
            '-I', self.dir_dbase,
            '-d', 'sacha_montpezat.mdb',
            '-O', self.dir_out,
            '-n', 'QH',
            '-s', 'K0100020',
            '-F', '2008110100',
            '-L', '2008110223',
            '-t', 'sacha',
            '-2',
            '-C', 'grp16'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.dbase2csv()
        self.assertEqual(args.hydro2, True)
        self.assertEqual(args.hydro3, False)
        self.assertEqual(args.dbase_filename, 'sacha_montpezat.mdb')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.station_name, 'K0100020')
        self.assertEqual(args.data_type, 'sacha')
        self.assertEqual(args.input_dir, self.dir_dbase)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.prcp_src, None)
        self.assertEqual(args.first_dtime, '2008110100')
        self.assertEqual(args.last_dtime, '2008110223')
        self.assertEqual(args.csv_type, 'grp16')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'K0100020_Q.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données de la grandeur {n} "\
            "depuis la base SACHA {d} de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Le référentiel est celui d'HYDRO-2. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "".format(
                I=args.input_dir, d=args.dbase_filename,
                F=args.first_dtime, L=args.last_dtime,
                t=args.data_type, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_sacha_H_h3_pyspc(self):
        """
        Tests SACHA Historique - Hauteur - Hydro3
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2csv.py',
            '-I', self.dir_dbase,
            '-d', 'sacha_montpezat.mdb',
            '-O', self.dir_out,
            '-n', 'HH',
            '-s', 'K010002010',
            '-F', '2008110100',
            '-L', '2008110223',
            '-t', 'sacha',
            '-3',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.dbase2csv()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(args.dbase_filename, 'sacha_montpezat.mdb')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'HH')
        self.assertEqual(args.station_name, 'K010002010')
        self.assertEqual(args.data_type, 'sacha')
        self.assertEqual(args.input_dir, self.dir_dbase)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.prcp_src, None)
        self.assertEqual(args.first_dtime, '2008110100')
        self.assertEqual(args.last_dtime, '2008110223')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'K010002010_HH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données de la grandeur {n} "\
            "depuis la base SACHA {d} de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Le référentiel est celui d'HYDRO-3. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "".format(
                I=args.input_dir, d=args.dbase_filename,
                F=args.first_dtime, L=args.last_dtime,
                t=args.data_type, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_sacha_P_sol_grp18(self):
        """
        Tests SACHA Historique - Pluie sol - Hydro3
        """
        processArgs = [
            'python',
            '../bin/dbase2csv.py',
            '-I', self.dir_dbase,
            '-d', 'sacha_montpezat.mdb',
            '-O', self.dir_out,
            '-n', 'PH',
            '-s', '07235005',
            '-F', '2008110100',
            '-L', '2008110223',
            '-t', 'sacha',
            '-3',
            '-C', 'grp18'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.dbase2csv()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(args.dbase_filename, 'sacha_montpezat.mdb')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'PH')
        self.assertEqual(args.station_name, '07235005')
        self.assertEqual(args.data_type, 'sacha')
        self.assertEqual(args.input_dir, self.dir_dbase)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.prcp_src, None)
        self.assertEqual(args.first_dtime, '2008110100')
        self.assertEqual(args.last_dtime, '2008110223')
        self.assertEqual(args.csv_type, 'grp18')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = '07235005_P_00J01H00M.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données de la grandeur {n} "\
            "depuis la base SACHA {d} de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Le référentiel est celui d'HYDRO-3. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "".format(
                I=args.input_dir, d=args.dbase_filename,
                F=args.first_dtime, L=args.last_dtime,
                t=args.data_type, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_sacha_P_rad_pyspc(self):
        """
        Tests SACHA Historique - Pluie Radar - Hydro3
        """
        processArgs = [
            'python',
            '../bin/dbase2csv.py',
            '-I', self.dir_dbase,
            '-d', 'sacha_montpezat.mdb',
            '-O', self.dir_out,
            '-n', 'PH',
            '-s', '43101002',
            '-F', '2008110100',
            '-L', '2008110223',
            '-t', 'sacha',
            '-S', 'radar',
            '-3',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.dbase2csv()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(args.dbase_filename, 'sacha_montpezat.mdb')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'PH')
        self.assertEqual(args.station_name, '43101002')
        self.assertEqual(args.data_type, 'sacha')
        self.assertEqual(args.input_dir, self.dir_dbase)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.prcp_src, 'radar')
        self.assertEqual(args.first_dtime, '2008110100')
        self.assertEqual(args.last_dtime, '2008110223')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = '43101002_radar_PH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données de la grandeur {n} "\
            "depuis la base SACHA {d} de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Le référentiel est celui d'HYDRO-3. "\
            "La donnée pluviométrique est celle du {S}. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "".format(
                I=args.input_dir, d=args.dbase_filename,
                F=args.first_dtime, L=args.last_dtime,
                t=args.data_type, n=args.varname, S=args.prcp_src,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_sacha_T_sol_grp16(self):
        """
        Tests SACHA Historique - Température - Hydro2
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2csv.py',
            '-I', self.dir_dbase,
            '-d', 'sacha_montpezat.mdb',
            '-O', self.dir_out,
            '-n', 'TH',
            '-s', '07154005',
            '-F', '2008110100',
            '-L', '2008110223',
            '-t', 'sacha',
            '-2',
            '-C', 'grp16'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.dbase2csv()
        self.assertEqual(args.hydro2, True)
        self.assertEqual(args.hydro3, False)
        self.assertEqual(args.dbase_filename, 'sacha_montpezat.mdb')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'TH')
        self.assertEqual(args.station_name, '07154005')
        self.assertEqual(args.data_type, 'sacha')
        self.assertEqual(args.input_dir, self.dir_dbase)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.prcp_src, None)
        self.assertEqual(args.first_dtime, '2008110100')
        self.assertEqual(args.last_dtime, '2008110223')
        self.assertEqual(args.csv_type, 'grp16')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = '07154005_T.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données de la grandeur {n} "\
            "depuis la base SACHA {d} de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Le référentiel est celui d'HYDRO-2. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "".format(
                I=args.input_dir, d=args.dbase_filename,
                F=args.first_dtime, L=args.last_dtime,
                t=args.data_type, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_sacha_TR_pyspc(self):
        """
        Test SACHA Temps Réel
        """
        processArgs = [
            'python',
            '../bin/dbase2csv.py',
            '-I', self.dir_dbase,
            '-d', 'sacha_montpezat.mdb',
            '-O', self.dir_out,
            '-n', 'QH',
            '-s', 'K0010020',
            '-F', '2016112100',
            '-L', '2016112223',
            '-t', 'sacha_TR',
            '-2',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.dbase2csv()
        self.assertEqual(args.hydro2, True)
        self.assertEqual(args.hydro3, False)
        self.assertEqual(args.dbase_filename, 'sacha_montpezat.mdb')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.station_name, 'K0010020')
        self.assertEqual(args.data_type, 'sacha_TR')
        self.assertEqual(args.input_dir, self.dir_dbase)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.prcp_src, None)
        self.assertEqual(args.first_dtime, '2016112100')
        self.assertEqual(args.last_dtime, '2016112223')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'K0010020_TR_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données de la grandeur {n} "\
            "depuis la base SACHA {d} de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Le référentiel est celui d'HYDRO-2. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "".format(
                I=args.input_dir, d=args.dbase_filename,
                F=args.first_dtime, L=args.last_dtime,
                t=args.data_type, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_previ14(self):
        """
        Test Base Prévision 2014 - BRUTE
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2csv.py',
            '-I', self.dir_dbase,
            '-d', 'prevision_2014_hydro3.mdb',
            '-O', self.dir_out,
            '-s', 'K6373020',
            '-F', '2016053116',
            '-L', '2016053116',
            '-t', 'previ14',
            '-3',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.dbase2csv()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(args.dbase_filename, 'prevision_2014_hydro3.mdb')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, 'K6373020')
        self.assertEqual(args.data_type, 'previ14')
        self.assertEqual(args.input_dir, self.dir_dbase)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.prcp_src, None)
        self.assertEqual(args.first_dtime, '2016053116')
        self.assertEqual(args.last_dtime, '2016053116')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'K6373020_2016053116_LCI_2001_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données de la grandeur {n} "\
            "depuis la base Prévision {d} de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Le référentiel est celui d'HYDRO-3. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "".format(
                I=args.input_dir, d=args.dbase_filename,
                F=args.first_dtime, L=args.last_dtime,
                t=args.data_type, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_previ14_val(self):
        """
        Test Base Prévision 2014 - VALID
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2csv.py',
            '-I', self.dir_dbase,
            '-d', 'prevision_2014_hydro3.mdb',
            '-O', self.dir_out,
            '-s', 'K6402520',
            '-F', '2016053116',
            '-L', '2016053116',
            '-t', 'previ14_val',
            '-3',
            '-C', 'pyspc',
            '-1'
        ]
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.dbase2csv()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(args.dbase_filename, 'prevision_2014_hydro3.mdb')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, 'K6402520')
        self.assertEqual(args.data_type, 'previ14_val')
        self.assertEqual(args.input_dir, self.dir_dbase)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.prcp_src, None)
        self.assertEqual(args.first_dtime, '2016053116')
        self.assertEqual(args.last_dtime, '2016053116')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'K6402520_2016053116_LCI-valid_3002_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données de la grandeur {n} "\
            "depuis la base Prévision {d} de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Le référentiel est celui d'HYDRO-3. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "".format(
                I=args.input_dir, d=args.dbase_filename,
                F=args.first_dtime, L=args.last_dtime,
                t=args.data_type, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_previ17(self):
        """
        Test Base Prévision 2017 - BRUT
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2csv.py',
            '-I', self.dir_dbase,
            '-d', 'prevision_2017.mdb',
            '-O', self.dir_out,
            '-s', 'K0260010',
            '-F', '2016112112',
            '-L', '2016112112',
            '-t', 'previ17',
            '-3',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.dbase2csv()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(args.dbase_filename, 'prevision_2017.mdb')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, 'K0260010')
        self.assertEqual(args.data_type, 'previ17')
        self.assertEqual(args.input_dir, self.dir_dbase)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.prcp_src, None)
        self.assertEqual(args.first_dtime, '2016112112')
        self.assertEqual(args.last_dtime, '2016112112')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
#        basename = 'K0260010_2016112112_LCI_QH.txt'
        basename = 'Prevision17_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données de la grandeur {n} "\
            "depuis la base Prévision {d} de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Le référentiel est celui d'HYDRO-3. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "".format(
                I=args.input_dir, d=args.dbase_filename,
                F=args.first_dtime, L=args.last_dtime,
                t=args.data_type, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_previ17_val(self):
        """
        Test Base Prévision 2017 - VALID
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2csv.py',
            '-I', self.dir_dbase,
            '-d', 'prevision_2017.mdb',
            '-O', self.dir_out,
            '-s', 'K0403010',
            '-F', '2016112112',
            '-L', '2016112112',
            '-t', 'previ17_val',
            '-3',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.dbase2csv()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(args.dbase_filename, 'prevision_2017.mdb')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, 'K0403010')
        self.assertEqual(args.data_type, 'previ17_val')
        self.assertEqual(args.input_dir, self.dir_dbase)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.prcp_src, None)
        self.assertEqual(args.first_dtime, '2016112112')
        self.assertEqual(args.last_dtime, '2016112112')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
#        basename = 'K0403010_2016112112_pilote_2003_QH.txt'
        basename = 'Prevision17val_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données de la grandeur {n} "\
            "depuis la base Prévision {d} de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Le référentiel est celui d'HYDRO-3. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "".format(
                I=args.input_dir, d=args.dbase_filename,
                F=args.first_dtime, L=args.last_dtime,
                t=args.data_type, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_previ19_sqlite(self):
        """
        Test Base Prévision 2019 - BRUT - SQLITE
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2csv.py',
            '-I', self.dir_dbase,
            '-d', 'PRV_201801.sqlite',
            '-O', self.dir_out,
            '-s', 'K1321810',
            '-F', '2018010412',
            '-L', '2018010412',
            '-t', 'previ19',
            '-3',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.dbase2csv()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(args.dbase_filename, 'PRV_201801.sqlite')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, 'K1321810')
        self.assertEqual(args.data_type, 'previ19')
        self.assertEqual(args.input_dir, self.dir_dbase)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.prcp_src, None)
        self.assertEqual(args.first_dtime, '2018010412')
        self.assertEqual(args.last_dtime, '2018010412')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
#        basename = 'K1321810_2018010412_LCI_QH.txt'
        basename = 'Prevision19_QH.txt'
        basename2 = 'Prevision19sqlite_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename2),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données de la grandeur {n} "\
            "depuis la base Prévision {d} de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Le référentiel est celui d'HYDRO-3. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "".format(
                I=args.input_dir, d=args.dbase_filename,
                F=args.first_dtime, L=args.last_dtime,
                t=args.data_type, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_previ19_mdb(self):
        """
        Test Base Prévision 2019 - BRUT - MDB
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2csv.py',
            '-I', self.dir_dbase,
            '-d', 'PRV_201801.mdb',
            '-O', self.dir_out,
            '-s', 'K1321810',
            '-F', '2018010412',
            '-L', '2018010412',
            '-t', 'previ19',
            '-3',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.dbase2csv()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(args.dbase_filename, 'PRV_201801.mdb')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, 'K1321810')
        self.assertEqual(args.data_type, 'previ19')
        self.assertEqual(args.input_dir, self.dir_dbase)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.prcp_src, None)
        self.assertEqual(args.first_dtime, '2018010412')
        self.assertEqual(args.last_dtime, '2018010412')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
#        basename = 'K1321810_2018010412_LCI_QH.txt'
        basename = 'Prevision19_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données de la grandeur {n} "\
            "depuis la base Prévision {d} de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Le référentiel est celui d'HYDRO-3. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "".format(
                I=args.input_dir, d=args.dbase_filename,
                F=args.first_dtime, L=args.last_dtime,
                t=args.data_type, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_previ19_mdb_val(self):
        """
        Test Base Prévision 2019 - VALID - MDB
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2csv.py',
            '-I', self.dir_dbase,
            '-d', 'PRV_201801.mdb',
            '-O', self.dir_out,
            '-s', 'K1321810',
            '-F', '2018010412',
            '-L', '2018010412',
            '-t', 'previ19_val',
            '-3',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.dbase2csv()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(args.dbase_filename, 'PRV_201801.mdb')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, 'K1321810')
        self.assertEqual(args.data_type, 'previ19_val')
        self.assertEqual(args.input_dir, self.dir_dbase)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.prcp_src, None)
        self.assertEqual(args.first_dtime, '2018010412')
        self.assertEqual(args.last_dtime, '2018010412')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
#        basename = 'K1321810_2018010412_expresso_5200_QH.txt'
        basename = 'Prevision19val_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données de la grandeur {n} "\
            "depuis la base Prévision {d} de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Le référentiel est celui d'HYDRO-3. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "".format(
                I=args.input_dir, d=args.dbase_filename,
                F=args.first_dtime, L=args.last_dtime,
                t=args.data_type, n=args.varname,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================
