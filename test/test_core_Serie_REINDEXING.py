#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Serie in pyspc.core.serie - reindexing

To run all tests just type:
    python -m unittest test_core_Serie_REINDEXING

To run only a class test:
    python -m unittest test_core_Serie_REINDEXING.TestSerie

To run only a specific test:
    python -m unittest test_core_Serie_REINDEXING.TestSerie.test_bwd

"""
# Imports
from datetime import datetime as dt
import numpy as np
import pandas as pnd
from pandas.util.testing import assert_frame_equal
from random import SystemRandom
import unittest
import warnings

# Imports pyspc
from pyspc.core.serie import Serie


class TestSerie(unittest.TestCase):
    """
    Serie class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        self.code = 'K0000000'
        self.provider = 'SPC'
        self.varname = 'QH'
        self.list_dtval = [
            [dt(2014, 11, 4, 0), 35.600],
            [dt(2014, 11, 4, 1), 41.200],
            [dt(2014, 11, 4, 2), 46.900],
            [dt(2014, 11, 4, 3), 62.900],
            [dt(2014, 11, 4, 4), 85.700],
            [dt(2014, 11, 4, 5), 106.000],
            [dt(2014, 11, 4, 6), 118.000],
            [dt(2014, 11, 4, 7), 115.000],
            [dt(2014, 11, 4, 8), 103.000],
            [dt(2014, 11, 4, 9), 92.500],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 11), 82.300],
            [dt(2014, 11, 4, 12), 78.300]
        ]

    def test_bwd(self):
        """
        Test méthode between_dates
        """
        fdt = dt(2014, 11, 4, 6)
        fstr = '2014110406'
        ldt = dt(2014, 11, 4, 8)
        lstr = '2014110408'
        # ====================================================================
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([x[1]
                                 for x in self.list_dtval
                                 if x[0] >= fdt and x[0] <= ldt],
                                index=[x[0]
                                       for x in self.list_dtval
                                       if x[0] >= fdt and x[0] <= ldt],
                                dtype=np.float32)
        valid_1.columns = [(self.code, self.varname)]
        # ====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie.between_dates(fdt, ldt)
        assert_frame_equal(serie.data_frame, valid_1)
        # ====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie.between_dates(fdt, ldt, inplace=True)
        assert_frame_equal(serie.data_frame, valid_1)
        # ====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie.between_dates(fstr, lstr, inplace=True)
        assert_frame_equal(serie.data_frame, valid_1)
        # ====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        with self.assertRaises(TypeError):
            serie.between_dates()
        with self.assertRaises(TypeError):
            serie.between_dates(None)
        with self.assertRaises(TypeError):
            serie.between_dates(fdt, None)
        # ====================================================================

    def test_reindex(self):
        """
        Test méthode reindex
        """
        # ====================================================================
        self.list_dtval.extend([
            [dt(2014, 11, 4, 18), 82.4000]
        ])
        # ====================================================================
        valid = pnd.DataFrame([x[1]
                               for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = [
            [dt(2014, 11, 4, 0), 35.600],
            [dt(2014, 11, 4, 1), 41.200],
            [dt(2014, 11, 4, 2), 46.900],
            [dt(2014, 11, 4, 3), 62.900],
            [dt(2014, 11, 4, 4), 85.700],
            [dt(2014, 11, 4, 5), 106.000],
            [dt(2014, 11, 4, 6), 118.000],
            [dt(2014, 11, 4, 7), 115.000],
            [dt(2014, 11, 4, 8), 103.000],
            [dt(2014, 11, 4, 9), 92.500],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 11), 82.300],
            [dt(2014, 11, 4, 12), 78.300],
            [dt(2014, 11, 4, 13), np.nan],
            [dt(2014, 11, 4, 14), np.nan],
            [dt(2014, 11, 4, 15), np.nan],
            [dt(2014, 11, 4, 16), np.nan],
            [dt(2014, 11, 4, 17), np.nan],
            [dt(2014, 11, 4, 18), 82.4000]
        ]
        valid_1 = pnd.DataFrame([x[1] for x in valid_1],
                                index=[x[0] for x in valid_1],
                                dtype=np.float32)
        valid_1.columns = [(self.code, self.varname)]
        # ====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        assert_frame_equal(serie.data_frame, valid_1)

    def test_split(self):
        """
        Test méthode stripna
        """
        # ====================================================================
        random = SystemRandom('pyspc')
        dates = pnd.date_range(dt(2020, 1, 1), dt(2023, 7, 1), freq='H')
        df = pnd.DataFrame([random.normalvariate(15, 10) for d in dates],
                           index=dates)
        obs = Serie(df, code='MASTATION', varname='TH')
        sim = Serie(df, code='MASTATION_MASIMU', varname='TH')
        # ====================================================================
        # CHUNKS - OBS
        # ------------
        valid = [7663, 7663, 7663, 7660]
        series = obs.split(value=4, method='chunks')
        self.assertEqual(len(series), 4)
        for x in range(4):
            k = ('MASTATION', 'TH', f'split-{x}')
            self.assertIn(k, series)
            self.assertEqual(len(series[k].df.index), valid[x])
        # ====================================================================
        # ----------------
        # CHUNK_SIZE - OBS
        # ----------------
        valid = [10000, 10000, 10000, 649]
        series = obs.split(value=10000, method='chunk_size')
        self.assertEqual(len(series), 4)
        for x in range(4):
            k = ('MASTATION', 'TH', f'split-{x}')
            self.assertIn(k, series)
            self.assertEqual(len(series[k].df.index), valid[x])
        # ----------------
        # CHUNK_SIZE - SIM
        # ----------------
        series = sim.split(value=10000, method='chunk_size')
        self.assertEqual(len(series), 4)
        for x in range(4):
            k = ('MASTATION', 'TH', f'MASIMU-split-{x}')
            self.assertIn(k, series)
            self.assertEqual(len(series[k].df.index), valid[x])
        # ====================================================================
        # DAYS - OBS
        # ----------
        valid = [7200, 7200, 7200, 7200, 1849]
        series = obs.split(value=300, method='days')
        self.assertEqual(len(series), 5)
        for x in range(5):
            k = ('MASTATION', 'TH', f'split-{x}')
            self.assertIn(k, series)
            self.assertEqual(len(series[k].df.index), valid[x])
        # ====================================================================
        # DATES - OBS
        # -----------
        split_dates = [dt(1999, 1, 1), dt(2020, 1, 1), dt(2021, 1, 1),
                       dt(2022, 1, 1), dt(2023, 1, 1), dt(2024, 1, 1)]
        series = obs.split(value=split_dates, method='dates')
        valid = [8784, 8760, 8760, 4345]
        self.assertEqual(len(series), 4)
        for x in range(4):
            k = ('MASTATION', 'TH', f'split-{x}')
            self.assertIn(k, series)
            self.assertEqual(len(series[k].df.index), valid[x])
        # ====================================================================
        # DATES - HYDRO YEAR
        # ------------------
        series = obs.split(value=9, method='hydro_year')
        valid = [5856, 8760, 8760, 7273]
        self.assertEqual(len(series), 4)
        for x, y in enumerate(range(2020, 2024)):
            k = ('MASTATION', 'TH', f'hydro-year-{y}')
            self.assertIn(k, series)
            self.assertEqual(len(series[k].df.index), valid[x])
        # ====================================================================
        with self.assertRaises(ValueError):
            obs.split(value=10000, method='xxxx')
        with self.assertRaises(ValueError):
            obs.split(value=1, method='groups')
        with self.assertRaises(ValueError):
            obs.split(value='1', method='chunk_size')
        # ====================================================================

    def test_stripna(self):
        """
        Test méthode stripna
        """
        # ====================================================================
        valid = pnd.DataFrame([x[1] if x[1] > 100 else np.nan
                               for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([x[1]
                                 for x in self.list_dtval
                                 if x[1] > 100],
                                index=[x[0]
                                       for x in self.list_dtval
                                       if x[1] > 100],
                                dtype=np.float32)
        valid_1.columns = [(self.code, self.varname)]
        # ====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie.stripna()
        assert_frame_equal(serie.data_frame, valid_1)
        # =====================================================================
