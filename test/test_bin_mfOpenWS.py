#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary mfOpenWS

To run all tests just type:
    python -m unittest test_bin_mfOpenWS

To run only a class test:
    python -m unittest test_bin_mfOpenWS.Test_mfOpenWS

To run only a specific test:
    python -m unittest test_bin_mfOpenWS.Test_mfOpenWS.test_debcla

"""
# Imports
from datetime import datetime as dt
import os
# import subprocess
import sys
import unittest

from pyspc.binutils.args import mfOpenWS as _args
from pyspc.binutils.runs import mfOpenWS as _runs
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_mfOpenWS(unittest.TestCase):
    """
    mfOpenWS bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'mfOpenWS')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.cfg_filename = None
        self.stations_list_file = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        config=self.cfg_filename,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)

    def test_retrieve_opendata(self):
        """
        Test requête mfOpenWS - OpenData
        """
        valid = ['Q_07_latest-2024-2025_RR-T-Vent.csv.gz',
                 'Q_07_previous-1950-2023_RR-T-Vent.csv.gz',
                 'Q_43_latest-2024-2025_RR-T-Vent.csv.gz',
                 'Q_43_previous-1950-2023_RR-T-Vent.csv.gz',
                 'Q_descriptif_champs_RR-T-Vent.csv']
        valid = [os.path.join(self.dir_out, v) for v in valid]
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_cfg, 'sitesmeteo.txt')
        end = dt.utcnow().strftime('%Y%m%d')
        datatype = 'MF_OpenData'
        processArgs = [
            'python',
            '../bin/mfOpenWS.py',
            '-l', self.stations_list_file,
            '-t', datatype,
            '-n', 'PJ',
            '-F', '20190901',
            '-L', end,
            '-O', self.dir_out,
            '-v'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.mfOpenWS()
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.datatype, datatype)
        self.assertEqual(args.varname, 'PJ')
        self.assertEqual(args.first_dtime, '20190901')
        self.assertEqual(args.last_dtime, end)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.cfg_filename, None)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.mfOpenWS(args)
        self.assertListEqual(valid, filenames)
        for f in filenames:
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Récupérer des données météorologiques ({t}) "\
            "pour la grandeur {n} pour les lieux listés dans {l} "\
            "entre les dates {F} et {L} "\
            "et enregistrer les fichiers csv.gz dans le répertoire {O}. "\
            .format(
                t=args.datatype,
                n=args.varname, F=args.first_dtime, L=args.last_dtime,
                l=args.stations_list_file, O=args.output_dir)
        # =====================================================================

    def test_retrieve_openapi(self):
        """
        Test requête mfOpenWS - OpenData
        """
        valid = ['45004001_Q_201605290000_201606030000.csv',
                 '45069001_Q_201605290000_201606030000.csv',
                 '45187001_Q_201605290000_201606030000.csv',
                 '45340002_Q_201605290000_201606030000.csv']
        valid = [os.path.join(self.dir_out, v) for v in valid]
        # =====================================================================
        self.cfg_filename = os.path.join('..', 'bin', 'mfOpenWS_RM.txt')
        self.stations_list_file = os.path.join(
            self.dir_cfg, 'sitesmeteo_openapi.txt')
        datatype = 'MF_OpenAPI'
        processArgs = [
            'python',
            '../bin/mfOpenWS.py',
            '-l', self.stations_list_file,
            '-c', self.cfg_filename,
            '-t', datatype,
            '-n', 'PJ',
            '-F', '20160529',
            '-L', '20160603',
            '-O', self.dir_out,
            '-v'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.mfOpenWS()
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.datatype, datatype)
        self.assertEqual(args.varname, 'PJ')
        self.assertEqual(args.first_dtime, '20160529')
        self.assertEqual(args.last_dtime, '20160603')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.cfg_filename, self.cfg_filename)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
#        filenames = _runs.mfOpenWS(args)
        with captured_output():
            filenames = _runs.mfOpenWS(args)
        self.assertListEqual(valid, filenames)
        for f in filenames:
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Récupérer depuis l'API des données météorologiques "\
            "({t}) pour la grandeur {n} pour les lieux listés dans {l} "\
            "entre les dates {F} et {L} "\
            "et enregistrer les fichiers csv dans le répertoire {O}. "\
            "Les éléments de connexion à l'API sont renseignés dans {c}."\
            .format(
                t=args.datatype, c=args.cfg_filename,
                n=args.varname, F=args.first_dtime, L=args.last_dtime,
                l=args.stations_list_file, O=args.output_dir)
        # =====================================================================
