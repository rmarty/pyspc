#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for functions in pyspc.model.plathynes.tdelta

To run all tests just type:
    python -m unittest test_model_PLATHYNES_Tdelta

To run only a class test:
    python -m unittest test_model_PLATHYNES_Tdelta.TestPLATHYNES_Tdelta

To run only a specific test:
    python -m unittest test_model_PLATHYNES_Tdelta.TestPLATHYNES_Tdelta.test_init

"""
# Imports
from datetime import timedelta as td
import unittest

# Imports pyspc
from pyspc.model.plathynes.tdelta import td2str, str2td


class TestPLATHYNES_Tdelta(unittest.TestCase):
    """
    PLATHYNES_Tdelta class test
    """
    def test_tdelta(self):
        """
        Test de la conversion (string) -> (timedelta)
        """
        valid = {
            '0 01:00:00': td(hours=1),
            '1 00:00:00': td(days=1),
            '0 03:00:00': td(hours=3),
            '0 00:05:00': td(minutes=5),
        }
        for k, v in valid.items():
            self.assertEqual(str2td(k), v)
        self.assertIsNone(str2td(None))
        self.assertIsNone(str2td())
        for k, v in valid.items():
            self.assertEqual(td2str(v), k)
        with self.assertRaises(ValueError):
            self.assertIsNone(td2str(None))
        with self.assertRaises(ValueError):
            self.assertIsNone(td2str())
