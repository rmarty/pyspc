#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for PHyC in pyspc.webservice.phyc

To run all tests just type:
    python -m unittest test_webservice_PHyC

To run only a class test:
    python -m unittest test_webservice_PHyC.TestPHyC

To run only a specific test:
    python -m unittest test_webservice_PHyC.TestPHyC.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import filecmp
import os
import unittest
from unittest import mock
import sys
import warnings

from pyspc.webservice.phyc import PHyC


class TestPHyC(unittest.TestCase):
    """
    PHyC class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.hostname = 'schemas.xmlsoap.org/wsdl/'
        self.username = 'user'
        self.password = 'password'
        self.proxies = {}
        self.idsession = '12345'
        self.dirname = os.path.join('data', 'webservice', 'phyc', 'mock')
        warnings.filterwarnings(action="ignore", category=ResourceWarning)

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
#    def test_init(self, mock_client):
    def test_init(self):
        """
        Test de la création de l'instance
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        self.assertEqual(client.hostname, self.hostname)
        self.assertEqual(client.proxies, self.proxies)
        self.assertEqual(client.username, self.username)
        self.assertEqual(client.password, self.password)
        self.assertIs(client.client, m.Client())

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_login(self, mock_ctl):
        """
        Test ouverture session
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
#        mock_ctl.return_value = True
        with mock.patch.object(
                client.client.service['AuthentificationPort'],
                'authentifier',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.login()
            mockwsdl.assert_called_with(
                cdcontact=self.username,
                motdepasse=self.password
            )
            self.assertEqual(client.session, self.idsession)
#        with mock.patch.object(
#                client.client.service['AuthentificationPort'],
#                'authentifier',
#                return_value=mock.Mock(statut='-1', idsession=idsession)
#                ) as mockwsdl:
#            client.login()
#            mockwsdl.assert_called_with(
#                cdcontact=username,
#                motdepasse=password
#            )
#            self.assertIsNone(client.session)

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_logout(self, mock_ctl):
        """
        Test fermeture session
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        with mock.patch.object(
                client.client.service['AuthentificationPort'],
                'fermerSession') as mockwsdl:
            client.session = self.idsession
            client.logout()
            mockwsdl.assert_called_with(
                cdcontact=self.username,
                idsession=self.idsession
            )
            self.assertIsNone(client.session)

##    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
#    def test_control_client(self):
#        """
#        Test du contrôle du client
#        """

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    def test_check_session(self):
        """
        Test du contrôle de la session
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        client.session = '123abc'
        try:
            client.check_session()
        except ValueError:
            self.fail("Erreur dans le contrôle de la session")
        client.session = None
        with self.assertRaises(ValueError):
            client.check_session()

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    def test_check_tstep(self):
        """
        Test du contrôle d'un pas de temps en minutes (int)
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        valid = [1, 60, 3600]
        for v in valid:
            try:
                client.check_tstep(tstep=v)
            except ValueError:
                self.fail("Erreur dans le contrôle du pas de temps (td)")
        with self.assertRaises(ValueError):
            client.check_tstep(tstep=-1)
            client.check_tstep(tstep=0)
            client.check_tstep(tstep='1')

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    def test_check_varname(self):
        """
        Test du contrôle de la variable
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        valid = ['Q', 'H', 'RR', 'TA']
        for v in valid:
            try:
                client.check_varname(v)
            except ValueError:
                self.fail("Erreur dans le contrôle de la variable")

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    def test_define_subperiods(self):
        """
        Test du contrôle de la variable
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        fdt = dt(2018, 1, 1)
        ldt = dt(2018, 12, 1)
        size = 3
        valid = [
            [dt(2018, 1, 1), dt(2018, 4, 22, 8)],
            [dt(2018, 4, 22, 8), dt(2018, 8, 11, 16)],
            [dt(2018, 8, 11, 16), dt(2018, 12, 1)],
        ]
        subperiods = client._define_subperiods(start=fdt, end=ldt, size=size)
        self.assertEqual(valid, subperiods)

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_get(self, mock_ctl):
        """
        Test récupération XML
        """
        # =====================================================================
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        client.session = self.idsession
        codes = ['12345678']
        fdt = dt(2018, 12, 1)
        ldt = dt(2018, 12, 28, 11, 10)
        models = ['45gGRPd000']
        varname1 = 'Q'
        varname2 = 'RR'
        timestep = td(hours=1)
        # =====================================================================
        with mock.patch.object(
                client,
                'get_data_fcst_hydro',
                return_value={('12345678', 'data_fcst_hydro',
                               varname1, fdt, ldt): self.idsession}
                ) as mockget:
            content = client.get(
                datatype='data_fcst_hydro',
                codes=codes,
                varname=varname1,
                timestep=timestep,
                models=models,
                first_dtime=fdt,
                last_dtime=ldt
            )
            self.assertEqual(content, {('12345678', 'data_fcst_hydro',
                                        varname1, fdt, ldt): self.idsession})
            mockget.assert_called_with(
                codes=codes,
                varname=varname1,
                models=models,
                first_dtime=fdt,
                last_dtime=ldt
            )
        # =====================================================================
        with mock.patch.object(
                client,
                'get_data_obs_hydro',
                return_value={('12345678', 'data_obs_hydro',
                               varname1, fdt, ldt): self.idsession}
                ) as mockget:
            content = client.get(
                datatype='data_obs_hydro',
                codes=codes,
                varname=varname1,
                timestep=timestep,
                first_dtime=fdt,
                last_dtime=ldt
            )
            self.assertEqual(content, {('12345678', 'data_obs_hydro',
                                        varname1, fdt, ldt): self.idsession})
            mockget.assert_called_with(
                codes=codes,
                varname=varname1,
                timestep=timestep,
                first_dtime=fdt,
                last_dtime=ldt,
                elab=None, plusvalide=None
            )
        # =====================================================================
        with mock.patch.object(
                client,
                'get_data_obs_meteo',
                return_value={('12345678', 'data_obs_meteo',
                               varname2, fdt, ldt): self.idsession}
                ) as mockget:
            content = client.get(
                datatype='data_obs_meteo',
                codes=codes,
                varname=varname2,
                timestep=timestep,
                first_dtime=fdt,
                last_dtime=ldt
            )
            self.assertEqual(content, {('12345678', 'data_obs_meteo',
                                        varname2, fdt, ldt): self.idsession})
            mockget.assert_called_with(
                codes=codes,
                varname=varname2,
                timestep=timestep,
                first_dtime=fdt,
                last_dtime=ldt
            )
        # =====================================================================
        with mock.patch.object(
                client,
                'get_flowmes',
                return_value={('12345678', 'flowmes',
                               fdt, ldt): self.idsession}
                ) as mockget:
            content = client.get(
                datatype='flowmes',
                codes=codes,
                first_dtime=fdt,
                last_dtime=ldt
            )
            self.assertEqual(content, {('12345678', 'flowmes',
                                        fdt, ldt): self.idsession})
            mockget.assert_called_with(
                codes=codes,
                first_dtime=fdt,
                last_dtime=ldt
            )
        # =====================================================================
        with mock.patch.object(
                client,
                'get_levelcor',
                return_value={('12345678', 'levelcor',
                               fdt, ldt): self.idsession}
                ) as mockget:
            content = client.get(
                datatype='levelcor',
                codes=codes,
                first_dtime=fdt,
                last_dtime=ldt
            )
            self.assertEqual(content, {('12345678', 'levelcor',
                                        fdt, ldt): self.idsession})
            mockget.assert_called_with(
                codes=codes,
                first_dtime=fdt,
                last_dtime=ldt
            )
        # =====================================================================
        with mock.patch.object(
                client,
                'get_loc_hydro',
                return_value={('12345678', 'loc_hydro'): self.idsession}
                ) as mockget:
            content = client.get(
                datatype='loc_hydro',
                codes=codes
            )
            self.assertEqual(content, {('12345678',
                                        'loc_hydro'): self.idsession})
            mockget.assert_called_with(
                codes=codes
            )
        # =====================================================================
        with mock.patch.object(
                client,
                'get_loc_hydro',
                return_value={('12345678', 'loc_hydro_child'): self.idsession}
                ) as mockget:
            content = client.get(
                datatype='loc_hydro_child',
                codes=codes
            )
            self.assertEqual(content, {('12345678',
                                        'loc_hydro_child'): self.idsession})
            mockget.assert_called_with(
                codes=codes, child=True
            )
        # =====================================================================
        with mock.patch.object(
                client,
                'get_loc_meteo',
                return_value={('12345678', 'loc_meteo'): self.idsession}
                ) as mockget:
            content = client.get(
                datatype='loc_meteo',
                codes=codes
            )
            self.assertEqual(content, {('12345678',
                                        'loc_meteo'): self.idsession})
            mockget.assert_called_with(
                codes=codes
            )
        # =====================================================================
        with mock.patch.object(
                client,
                'get_ratingcurve',
                return_value={('12345678', 'ratingcurve',
                               fdt, ldt): self.idsession}
                ) as mockget:
            content = client.get(
                datatype='ratingcurve',
                codes=codes,
                first_dtime=fdt,
                last_dtime=ldt
            )
            self.assertEqual(content, {('12345678', 'ratingcurve',
                                        fdt, ldt): self.idsession})
            mockget.assert_called_with(
                codes=codes,
                first_dtime=fdt,
                last_dtime=ldt
            )
        # =====================================================================
        with mock.patch.object(
                client,
                'get_user',
                return_value={('12345678', 'user'): self.idsession}
                ) as mockget:
            content = client.get(
                datatype='user',
                codes=codes
            )
            self.assertEqual(content, {('12345678',
                                        'user'): self.idsession})
            mockget.assert_called_with(
                codes=codes
            )
        # =====================================================================

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_data_fcst_hydro')
    def test_get_data_fcst_hydro(self, mock_req, mock_ctl):
        """
        Test récupération XML data_fcst_hydro
        """
        # =====================================================================
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        client.session = self.idsession
        codes = ['12345678']
        fdt = dt(2018, 12, 1)
        ldt = dt(2018, 12, 28, 11, 10)
        models = ['45gGRPd000']
        varname = 'Q'
        key = ('12345678', 'data_fcst_hydro', varname, fdt, ldt)
        # (Faux) retour de la requête PHyC
        xml_return = mock.Mock(statut='0', xmlprevcrues=self.idsession)
        mock_req.return_value = xml_return
        # Définition de la récupération
        content = client.get_data_fcst_hydro(
            codes=codes,
            varname=varname,
            models=models,
            first_dtime=fdt,
            last_dtime=ldt
        )
        self.assertEqual(content, {key: self.idsession})
        mock_req.assert_called_with(
            codes=codes,
            models=models,
            varname=varname,
            first_dtime=fdt,
            last_dtime=ldt
        )
        # =====================================================================

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_data_obs_hydro')
    def test_get_data_obs_hydro(self, mock_req, mock_ctl):
        """
        Test récupération XML data_obs_hydro
        """
        # =====================================================================
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        client.session = self.idsession
        codes = ['12345678']
        fdt = dt(2018, 12, 1)
        ldt = dt(2018, 12, 28, 11, 10)
        varname = 'Q'
        timestep = td(hours=1)
        key = ('12345678', 'data_obs_hydro', varname, fdt, ldt)
        # (Faux) retour de la requête PHyC
        xml_return = mock.Mock(statut='0', xmlprevcrues=self.idsession)
        mock_req.return_value = xml_return
        # =====================================================================
        # Définition de la récupération
        content = client.get_data_obs_hydro(
            codes=codes,
            varname=varname,
            timestep=timestep,
            first_dtime=fdt,
            last_dtime=ldt
        )
        self.assertEqual(content, {key: self.idsession})
        mock_req.assert_called_with(
            codes=codes,
            varname=varname,
            timestep=timestep,
            first_dtime=fdt,
            last_dtime=ldt,
            elab=None, plusvalide=None
        )
        # =====================================================================
        # Définition de la récupération
        content = client.get_data_obs_hydro(
            codes=codes,
            varname=varname,
            timestep=timestep,
            first_dtime=fdt,
            last_dtime=ldt,
            elab=True, plusvalide=False
        )
        self.assertEqual(content, {key: self.idsession})
        mock_req.assert_called_with(
            codes=codes,
            varname=varname,
            timestep=timestep,
            first_dtime=fdt,
            last_dtime=ldt,
            elab=True, plusvalide=False
        )
        # =====================================================================

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_data_obs_meteo')
    def test_get_data_obs_meteo(self, mock_req, mock_ctl):
        """
        Test récupération XML data_obs_meteo
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        client.session = self.idsession
        codes = ['12345678']
        fdt = dt(2018, 12, 1)
        ldt = dt(2018, 12, 28, 11, 10)
        varname = 'RR'
        timestep = td(hours=1)
        key = ('12345678', 'data_obs_meteo', varname, fdt, ldt)
        # (Faux) retour de la requête PHyC
        xml_return = mock.Mock(statut='0', xmlprevcrues=self.idsession)
        mock_req.return_value = xml_return
        # Définition de la récupération
        content = client.get_data_obs_meteo(
            codes=codes,
            varname=varname,
            timestep=timestep,
            first_dtime=fdt,
            last_dtime=ldt
        )
        self.assertEqual(content, {key: self.idsession})
        mock_req.assert_called_with(
            codes=codes,
            varname=varname,
            timestep=timestep,
            first_dtime=fdt,
            last_dtime=ldt
        )

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_flowmes')
    def test_get_flowmes(self, mock_req, mock_ctl):
        """
        Test récupération XML request_flowmes
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        client.session = self.idsession
        # (Faux) retour de la requête PHyC
        xml_return = mock.Mock(statut='0', xmlprevcrues=self.idsession)
        mock_req.return_value = xml_return
        # Définition de la récupération
        codes = ['12345678']
        fdt = dt(2018, 12, 1)
        ldt = dt(2018, 12, 28, 11, 10)
        key = ('12345678', 'flowmes', fdt, ldt)
        content = client.get_flowmes(
            codes=codes,
            first_dtime=fdt,
            last_dtime=ldt
        )
        self.assertEqual(content, {key: self.idsession})

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_levelcor')
    def test_get_levelcor(self, mock_req, mock_ctl):
        """
        Test récupération XML request_levelcor
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        client.session = self.idsession
        # (Faux) retour de la requête PHyC
        xml_return = mock.Mock(statut='0', xmlprevcrues=self.idsession)
        mock_req.return_value = xml_return
        # Définition de la récupération
        codes = ['1234567890']
        fdt = dt(2018, 12, 1)
        ldt = dt(2018, 12, 28, 11, 10)
        key = ('1234567890', 'levelcor', fdt, ldt)
        content = client.get_levelcor(
            codes=codes,
            first_dtime=fdt,
            last_dtime=ldt
        )
        self.assertEqual(content, {key: self.idsession})

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_site_hydro')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_station_hydro')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_capteur_hydro')
    def test_get_loc_hydro(self, mock_req0, mock_req1, mock_req2, mock_ctl):
        """
        Test récupération XML request_loc_hydro
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        client.session = self.idsession
        # (Faux) retour de la requête PHyC
        xml_return = mock.Mock(statut='0', xmlprevcrues=self.idsession)
        mock_req0.return_value = xml_return
        mock_req1.return_value = xml_return
        mock_req2.return_value = xml_return
        # Définition de la récupération
        codes = ['12345678']
        key = ('12345678', 'loc_hydro')
        content = client.get_loc_hydro(
            codes=codes
        )
        self.assertEqual(content, {key: self.idsession})
        codes = ['1234567890']
        key = ('1234567890', 'loc_hydro')
        content = client.get_loc_hydro(
            codes=codes
        )
        self.assertEqual(content, {key: self.idsession})
        codes = ['123456789012']
        key = ('123456789012', 'loc_hydro')
        content = client.get_loc_hydro(
            codes=codes
        )
        self.assertEqual(content, {key: self.idsession})
        codes = ['1234']
        key = ('1234', 'loc_hydro')
        content = client.get_loc_hydro(
            codes=codes
        )
        self.assertEqual(content, {})

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_site_hydro_by_zone')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_station_hydro_by_site')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_capteur_hydro_by_station')
    def test_get_loc_hydro_child(self, mock_req0, mock_req1, mock_req2,
                                 mock_ctl):
        """
        Test récupération XML request_loc_hydro_by_xxx
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        client.session = self.idsession
        # (Faux) retour de la requête PHyC
        xml_return = mock.Mock(statut='0', xmlprevcrues=self.idsession)
        mock_req0.return_value = xml_return
        mock_req1.return_value = xml_return
        mock_req2.return_value = xml_return
        # Définition de la récupération
        codes = ['12345678']
        key = ('12345678', 'loc_hydro_child')
        content = client.get_loc_hydro(
            codes=codes, child=True
        )
        self.assertEqual(content, {key: self.idsession})
        codes = ['1234567890']
        key = ('1234567890', 'loc_hydro_child')
        content = client.get_loc_hydro(
            codes=codes, child=True
        )
        self.assertEqual(content, {key: self.idsession})
        codes = ['123456789012']
        key = ('123456789012', 'loc_hydro_child')
        content = client.get_loc_hydro(
            codes=codes, child=True
        )
        self.assertEqual(content, {})
        codes = ['1234']
        key = ('1234', 'loc_hydro_child')
        content = client.get_loc_hydro(
            codes=codes, child=True
        )
        self.assertEqual(content, {key: self.idsession})

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_loc_meteo')
    def test_get_loc_meteo(self, mock_req, mock_ctl):
        """
        Test récupération XML request_loc_meteo
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        client.session = self.idsession
        # (Faux) retour de la requête PHyC
        xml_return = mock.Mock(statut='0', xmlprevcrues=self.idsession)
        mock_req.return_value = xml_return
        # Définition de la récupération
        codes = ['12345678']
        key = ('12345678', 'loc_meteo')
        content = client.get_loc_meteo(
            codes=codes
        )
        self.assertEqual(content, {key: self.idsession})

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_ratingcurve')
    def test_get_ratingcurve(self, mock_req, mock_ctl):
        """
        Test récupération XML request_ratingcurve
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        client.session = self.idsession
        # (Faux) retour de la requête PHyC
        xml_return = mock.Mock(statut='0', xmlprevcrues=self.idsession)
        mock_req.return_value = xml_return
        # Définition de la récupération
        codes = ['1234567890']
        fdt = dt(2018, 12, 1)
        ldt = dt(2018, 12, 28, 11, 10)
        key = ('1234567890', 'ratingcurve', fdt, ldt)
        content = client.get_ratingcurve(
            codes=codes,
            first_dtime=fdt,
            last_dtime=ldt
        )
        self.assertEqual(content, {key: self.idsession})

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_user')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_user_site_hydro')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_user_station_hydro')
    @mock.patch('pyspc.webservice.phyc.PHyC.request_user_loc_meteo')
    def test_get_user(self, mock_reqsim, mock_reqsth, mock_reqsih, mock_reqadm,
                      mock_ctl):
        """
        Test récupération XML request_ratingcurve
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        client.session = self.idsession
        # (Faux) retour de la requête PHyC
        xml_return = mock.Mock(statut='0', xmlprevcrues=self.idsession)
        mock_reqsim.return_value = xml_return
        mock_reqsth.return_value = xml_return
        mock_reqsih.return_value = xml_return
        mock_reqadm.return_value = xml_return
        # Définition de la récupération
        codes = ['1234567890']
        keys = [
            ('1234567890', 'user_admin'),
            ('1234567890', 'user_site_hydro'),
            ('1234567890', 'user_station_hydro'),
            ('1234567890', 'user_loc_meteo'),
        ]
        content = client.get_user(
            codes=codes
        )
        for k in keys:
            self.assertIn(k, content)
            self.assertEqual(content[k], self.idsession)

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_process_xml_return(self, mock_ctl):
        """
        Test requête data_fcst_hydro
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        xml_return = mock.Mock(statut='0', xmlprevcrues=self.idsession)
        key = ('CODE', 'DATATYPE', 'PERIOD')
        content = client._process_xml_return(
            xml_return=xml_return,
            key=key
        )
        self.assertEqual(content, self.idsession)
        xml_return = mock.Mock(statut=0, xmlprevcrues=self.idsession)
        key = ('CODE', 'DATATYPE', 'PERIOD')
        content = client._process_xml_return(
            xml_return=xml_return,
            key=key
        )
        self.assertEqual(content, self.idsession)
        with self.assertRaises(ValueError):
            client._process_xml_return(xml_return=None, key=key)
            client._process_xml_return(
                xml_return=mock.Mock(statut=-1, xmlprevcrues=None), key=key)
            client._process_xml_return(
                xml_return=mock.Mock(statut=1, xmlprevcrues=None), key=key)

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_request_data_fcst_hydro(self, mock_ctl):
        """
        Test requête data_fcst_hydro
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        codes = ['12345678', 'SITE HYDRO']
        models = ['45gGRPd000', '45hEXPt000']
        varname = 'H'
        with mock.patch.object(
                client.client.service['PrevisionsPublicationPort'],
                'publierPrevisionsMajBase',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_data_fcst_hydro(
                codes=codes,
                first_dtime=dt(2018, 12, 1),
                last_dtime=dt(2018, 12, 28, 11, 10)
            )
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdentite=codes,
                filtremodele=[],
                grandeur='Q',
                dtinsertiondebut='2018-12-01T00:00:00',
                dtinsertionfin='2018-12-28T11:10:00'
            )
            client.request_data_fcst_hydro(
                codes=codes,
                first_dtime=dt(2018, 12, 1),
                last_dtime=dt(2018, 12, 28, 11, 10),
                models=models,
                varname=varname
            )
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdentite=codes,
                filtremodele=models,
                grandeur=varname,
                dtinsertiondebut='2018-12-01T00:00:00',
                dtinsertionfin='2018-12-28T11:10:00'
            )

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_request_data_obs_hydro(self, mock_ctl):
        """
        Test requête data_obs_hydro
        """
        # =====================================================================
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        codes = ['12345678', 'SITE HYDRO']
        varname = 'Q'
        timestep = td(hours=1)
        # =====================================================================
        with mock.patch.object(
                client.client.service['ObservationsHydroPublicationPort'],
                'publierObservationsHydro',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_data_obs_hydro(
                codes=codes,
                first_dtime=dt(2018, 12, 1),
                last_dtime=dt(2018, 12, 28, 11, 10),
                varname=varname,
                timestep=None
            )
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdentite=codes,
                grandeur=varname,
                dtmesuredebut='2018-12-01T00:00:00',
                dtmesurefin='2018-12-28T11:10:00',
                plusvalide=False
            )
            client.request_data_obs_hydro(
                codes=codes,
                first_dtime=dt(2018, 12, 1),
                last_dtime=dt(2018, 12, 28, 11, 10),
                varname=varname,
                timestep=None,
                plusvalide=True
            )
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdentite=codes,
                grandeur=varname,
                dtmesuredebut='2018-12-01T00:00:00',
                dtmesurefin='2018-12-28T11:10:00',
                plusvalide=True
            )
        # =====================================================================
        with mock.patch.object(
                client.client.service['ObservationsHydroPublicationPort'],
                'publierObservationsHydroPasDeTemps',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_data_obs_hydro(
                codes=codes,
                first_dtime=dt(2018, 12, 1),
                last_dtime=dt(2018, 12, 28, 11, 10)
            )
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdentite=codes,
                grandeur='Q',
                pasdetemps=60,
                dtmesuredebut='2018-12-01T00:00:00',
                dtmesurefin='2018-12-28T11:10:00'
            )
            client.request_data_obs_hydro(
                codes=codes,
                first_dtime=dt(2018, 12, 1),
                last_dtime=dt(2018, 12, 28, 11, 10),
                varname=varname,
                timestep=timestep
            )
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdentite=codes,
                grandeur=varname,
                pasdetemps=60,
                dtmesuredebut='2018-12-01T00:00:00',
                dtmesurefin='2018-12-28T11:10:00'
            )
        # =====================================================================
        with mock.patch.object(
                client.client.service[
                    'ObservationsHydroElaboreesPublicationPort'],
                'publierObservationsHydroElaborees',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_data_obs_hydro(
                codes=codes,
                first_dtime=dt(2018, 12, 1),
                last_dtime=dt(2018, 12, 28, 11, 10),
                varname="QIXJ"
            )
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdentite=codes,
                typeelabore="QIXJ",
                dtmesuredebut='2018-12-01T00:00:00',
                dtmesurefin='2018-12-28T11:10:00'
            )
        # =====================================================================

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_request_data_obs_meteo(self, mock_ctl):
        """
        Test requête data_obs_meteo
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        codes = ['12345678', 'SITE HYDRO']
        varname = 'RR'
        with mock.patch.object(
                client.client.service['ObservationsMeteoPublicationPort'],
                'publierObservationsMeteo',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_data_obs_meteo(
                codes=codes,
                first_dtime=dt(2018, 12, 1),
                last_dtime=dt(2018, 12, 28, 11, 10)
            )
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdsitemeteo=codes,
                grandeur=varname,
                cmpre=60,
                dtmesuredebut='2018-12-01T00:00:00',
                dtmesurefin='2018-12-28T11:10:00'
            )
        with mock.patch.object(
                client.client.service['ObservationsMeteoPublicationPort'],
                'publierPluvioCumulGlissant',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_data_obs_meteo(
                codes=codes,
                first_dtime=dt(2018, 12, 1),
                last_dtime=dt(2018, 12, 28, 11, 10),
                accum=True,
                timestep=td(hours=3)
            )
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdsitemeteo=codes,
                grandeur=varname,
                pdtcumul=180,
                dtmesuredebut='2018-12-01T00:00:00',
                dtmesurefin='2018-12-28T11:10:00'
            )

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_request_flowmes(self, mock_ctl):
        """
        Test requête flowmes
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        codes = ['12345678', 'SITE HYDRO']
        with mock.patch.object(
                client.client.service['JaugeagePublicationPort'],
                'publierJaugeageHydro',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_flowmes(
                codes=codes,
                first_dtime=dt(2018, 12, 1),
                last_dtime=dt(2018, 12, 28, 11, 10)
            )
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdstationhydro=codes,
                dtdebut='2018-12-01T00:00:00',
                dtfin='2018-12-28T11:10:00'
            )

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_request_levelcor(self, mock_ctl):
        """
        Test requête levelcor
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        codes = ['12345678', 'SITE HYDRO']
        with mock.patch.object(
                client.client.service['CourbeCorrectionPublicationPort'],
                'publierCourbeCorrection',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_levelcor(
                codes=codes,
                first_dtime=dt(2018, 12, 1),
                last_dtime=dt(2018, 12, 28, 11, 10)
            )
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdstationhydro=codes,
                dtdebut='2018-12-01T00:00:00',
                dtfin='2018-12-28T11:10:00'
            )

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_request_loc_hydro(self, mock_ctl):
        """
        Test requête loc_hydro
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        codes = ['12345678', 'SITE HYDRO']
        with mock.patch.object(
                client.client.service['SiteHydroPublicationPort'],
                'publierSiteHydroListe',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_site_hydro(codes=codes)
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdsitehydro=codes
            )
        codes = ['1234567890', 'STATION HYDRO']
        with mock.patch.object(
                client.client.service['SiteHydroPublicationPort'],
                'publierStationHydroListe',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_station_hydro(codes=codes)
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdstationhydro=codes
            )

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_request_loc_meteo(self, mock_ctl):
        """
        Test requête loc_meteo
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        codes = ['123', 'abc']
        with mock.patch.object(
                client.client.service['SiteMeteoPublicationPort'],
                'publierSiteMeteoListe',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_loc_meteo(codes=codes)
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdsitemeteo=codes
            )

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_request_ratingcurve(self, mock_ctl):
        """
        Test requête ratingcurve
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        codes = ['123', 'abc']
        with mock.patch.object(
                client.client.service['CourbeTaragePublicationPort'],
                'publierCourbeTarage',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_ratingcurve(
                codes=codes,
                first_dtime=dt(2018, 12, 1),
                last_dtime=dt(2018, 12, 28, 11, 10)
            )
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdstationhydro=codes,
                dtdebut='2018-12-01T00:00:00',
                dtfin='2018-12-28T11:10:00'
            )

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_request_user(self, mock_ctl):
        """
        Test requête user
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        codes = ['123', 'abc']
        with mock.patch.object(
                client.client.service['ContactPublicationPort'],
                'publierContactListe',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_user(codes=codes)
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdcontact=codes
            )

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_request_user_loc_meteo(self, mock_ctl):
        """
        Test requête user loc meteo
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        codes = ['123', 'abc']
        with mock.patch.object(
                client.client.service['SiteMeteoPublicationPort'],
                'publierSiteMeteoContact',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_user_loc_meteo(codes=codes)
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdcontact=codes
            )

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_request_user_site_hydro(self, mock_ctl):
        """
        Test requête user site hydro
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        codes = ['123', 'abc']
        with mock.patch.object(
                client.client.service['SiteHydroPublicationPort'],
                'publierSiteHydroContact',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_user_site_hydro(codes=codes)
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdcontact=codes
            )

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    def test_request_user_station_hydro(self, mock_ctl):
        """
        Test requête user station hydro
        """
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        codes = ['123', 'abc']
        with mock.patch.object(
                client.client.service['SiteHydroPublicationPort'],
                'publierStationHydroContact',
                return_value=mock.Mock(statut='0', idsession=self.idsession)
                ) as mockwsdl:
            client.session = self.idsession
            client.request_user_station_hydro(codes=codes)
            mockwsdl.assert_called_with(
                idsession=self.idsession,
                listecdcontact=codes
            )

#    @mock.patch('pyspc.webservice.phyc.phyc._sclient')
    @mock.patch('pyspc.webservice.phyc.PHyC.check_client')
    @mock.patch('pyspc.webservice.phyc.PHyC.get')
    def test_retrieve(self, mock_get, mock_ctl):
        """
        Test récupération XML
        """
        # =====================================================================
        m = mock.MagicMock()
        sys.modules["suds.client"] = m
        client = PHyC(
            hostname=self.hostname,
            username=self.username,
            password=self.password,
            proxies=self.proxies
        )
        client.session = self.idsession
        codes = ['12345678']
        fdt = dt(2018, 12, 1)
        ldt = dt(2018, 12, 28, 11, 10)
        models = ['45gGRPd000']
        varname1 = 'Q'
        varname2 = 'RR'
        dirname = 'data'
        timestep = td(hours=1)
        # =====================================================================
        mock_get.return_value = {('12345678', 'data_fcst_hydro',
                                  varname1, fdt, ldt): self.idsession}
        tmp_files = client.retrieve(
            dirname=dirname,
            datatype='data_fcst_hydro',
            codes=codes,
            varname=varname1,
            models=models,
            first_dtime=fdt,
            last_dtime=ldt
        )
        for tmp_file in tmp_files:
            src_file = os.path.join(self.dirname, os.path.basename(tmp_file))
            self.assertTrue(filecmp.cmp(src_file, tmp_file))
            os.remove(tmp_file)
        # =====================================================================
        mock_get.return_value = {('12345678', 'data_obs_hydro',
                                  varname1, fdt, ldt): self.idsession}
        tmp_files = client.retrieve(
            dirname=dirname,
            datatype='data_obs_hydro',
            codes=codes,
            varname=varname1,
            timestep=timestep,
            first_dtime=fdt,
            last_dtime=ldt
        )
        for tmp_file in tmp_files:
            src_file = os.path.join(self.dirname, os.path.basename(tmp_file))
            self.assertTrue(filecmp.cmp(src_file, tmp_file))
            os.remove(tmp_file)
        # =====================================================================
        mock_get.return_value = {('12345678', 'data_obs_meteo',
                                  varname2, fdt, ldt): self.idsession}
        tmp_files = client.retrieve(
            dirname=dirname,
            datatype='data_obs_meteo',
            codes=codes,
            varname=varname2,
            timestep=timestep,
            first_dtime=fdt,
            last_dtime=ldt
        )
        for tmp_file in tmp_files:
            src_file = os.path.join(self.dirname, os.path.basename(tmp_file))
            self.assertTrue(filecmp.cmp(src_file, tmp_file))
            os.remove(tmp_file)
        # =====================================================================
        mock_get.return_value = {('12345678', 'flowmes',
                                  fdt, ldt): self.idsession}
        tmp_files = client.retrieve(
            dirname=dirname,
            datatype='flowmes',
            codes=codes,
            first_dtime=fdt,
            last_dtime=ldt
        )
        for tmp_file in tmp_files:
            src_file = os.path.join(self.dirname, os.path.basename(tmp_file))
            self.assertTrue(filecmp.cmp(src_file, tmp_file))
            os.remove(tmp_file)
        # =====================================================================
        mock_get.return_value = {('12345678', 'levelcor',
                                  fdt, ldt): self.idsession}
        tmp_files = client.retrieve(
            dirname=dirname,
            datatype='levelcor',
            codes=codes,
            first_dtime=fdt,
            last_dtime=ldt
        )
        for tmp_file in tmp_files:
            src_file = os.path.join(self.dirname, os.path.basename(tmp_file))
            self.assertTrue(filecmp.cmp(src_file, tmp_file))
            os.remove(tmp_file)
        # =====================================================================
        mock_get.return_value = {('12345678', 'loc_hydro'): self.idsession}
        tmp_files = client.retrieve(
            dirname=dirname,
            datatype='loc_hydro',
            codes=codes
        )
        for tmp_file in tmp_files:
            src_file = os.path.join(self.dirname, os.path.basename(tmp_file))
            self.assertTrue(filecmp.cmp(src_file, tmp_file))
            os.remove(tmp_file)
        # =====================================================================
        mock_get.return_value = {('1234567890', 'loc_hydro'): self.idsession}
        tmp_files = client.retrieve(
            dirname=dirname,
            datatype='loc_hydro',
            codes=['1234567890']
        )
        for tmp_file in tmp_files:
            src_file = os.path.join(self.dirname, os.path.basename(tmp_file))
            self.assertTrue(filecmp.cmp(src_file, tmp_file))
            os.remove(tmp_file)
        # =====================================================================
        mock_get.return_value = {('12345678', 'loc_meteo'): self.idsession}
        tmp_files = client.retrieve(
            dirname=dirname,
            datatype='loc_meteo',
            codes=codes
        )
        for tmp_file in tmp_files:
            src_file = os.path.join(self.dirname, os.path.basename(tmp_file))
            self.assertTrue(filecmp.cmp(src_file, tmp_file))
            os.remove(tmp_file)
        # =====================================================================
        mock_get.return_value = {('12345678', 'ratingcurve',
                                  fdt, ldt): self.idsession}
        tmp_files = client.retrieve(
            dirname=dirname,
            datatype='ratingcurve',
            codes=codes,
            first_dtime=fdt,
            last_dtime=ldt
        )
        for tmp_file in tmp_files:
            src_file = os.path.join(self.dirname, os.path.basename(tmp_file))
            self.assertTrue(filecmp.cmp(src_file, tmp_file))
            os.remove(tmp_file)
        # =====================================================================
        mock_get.return_value = {('12345678', 'user_admin'): self.idsession}
        tmp_files = client.retrieve(
            dirname=dirname,
            datatype='user',
            codes=codes
        )
        for tmp_file in tmp_files:
            src_file = os.path.join(self.dirname, os.path.basename(tmp_file))
            self.assertTrue(filecmp.cmp(src_file, tmp_file))
            os.remove(tmp_file)
        # =====================================================================
