#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Series in pyspc.core.series

To run all tests just type:
    python -m unittest test_core_Series_PLOTTING

To run only a class test:
    python -m unittest test_core_Series_PLOTTING.TestSeries

To run only a specific test:
    python -m unittest test_core_Series_PLOTTING.TestSeries.test_plot

"""
# Imports
from datetime import datetime as dt, timedelta as td
import filecmp
import numpy as np
import os
import pandas as pnd
import unittest
# from unittest import mock
import warnings

# Imports pyspc
from pyspc.core.serie import Serie
from pyspc.core.series import Series


class TestSeries(unittest.TestCase):
    """
    Series class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        # =====================================================================
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        # =====================================================================
        self.dirname = os.path.join('data', 'core', 'series')
        # =====================================================================
        serie_ph1 = pnd.DataFrame({
            'ph1': [
                0.200, 0.000, 0.400, 1.000, 7.100, 6.400, 8.900, 5.900, 9.500,
                6.700, 6.000, 9.100, 9.300, 12.600, 15.700, 12.500, 8.500,
                1.600, 3.900, 3.200, 0.400, 2.000, 2.200, 0.800, 1.100, 2.800,
                2.000, 3.400, 2.400, 2.500, 3.200, 1.000, 1.200, 0.400, 0.000,
                0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
                0.000, 0.000, 0.000, 0.000, 0.000]},
            index=pnd.date_range(
                dt(2014, 11, 3, 12), dt(2014, 11, 5, 12), freq=td(hours=1))
        )
        serie_ph2 = pnd.DataFrame({
            'ph2': [
                4.000, 6.500, 7.700, 4.000, 5.400, 4.400, 6.100, 4.800, 6.500,
                3.600, 8.300, 7.600, 3.800, 2.300, 3.800, 4.600, 0.000, 1.800,
                2.000, 2.400, 1.500, 4.800, 4.000, 6.200, 3.900, 4.800, 3.400,
                2.600, 1.700, 1.400, 0.200, 0.000, 0.000]},
            index=pnd.date_range(
                dt(2014, 11, 3, 16), dt(2014, 11, 5), freq=td(hours=1))
        )
        self.series_p = Series(datatype='obs', name='prcp')
        serie_ph1 = Serie(datval=serie_ph1, code='ph1', varname='PH')
        self.series_p.add(serie=serie_ph1)
        serie_ph2 = Serie(datval=serie_ph2, code='ph2', varname='PH')
        self.series_p.add(serie=serie_ph2)
        # =====================================================================
        serie_th = pnd.DataFrame({
            'th': [
                8.400, 7.700, 8.100, 8.400, 7.900, 7.200, 6.600, 7.000, 7.200,
                6.900, 6.600, 7.300, 7.100, 6.800, 6.800, 4.900, 3.400, 2.000,
                0.500, -0.100, -0.100, 0.500, 0.000, 0.000, -0.200]},
            index=pnd.date_range(
                dt(2014, 11, 4), dt(2014, 11, 5), freq=td(hours=1))
        )
        serie_ti = pnd.DataFrame({
            'ti': [10.45, 9.25, 8.40, 1.50, -1.50]},
            index=[dt(2014, 11, 4, 0, 30), dt(2014, 11, 4, 6, 15),
                   dt(2014, 11, 4, 12, 30), dt(2014, 11, 4, 18, 45),
                   dt(2014, 11, 5, 0, 15)]
        )
        self.series_t = Series(datatype='obs', name='temp')
        serie_th = Serie(datval=serie_th, code='th', varname='TH')
        self.series_t.add(serie=serie_th)
        serie_ti = Serie(datval=serie_ti, code='ti', varname='TI')
        self.series_t.add(serie=serie_ti)
        # =====================================================================
        # Chambon sur Lignon
        serie_qh = pnd.DataFrame({
            'qh': [
                2.510, 2.700, 2.970, 3.430, 4.810, 8.180, 12.100, 17.000,
                19.600, 22.200, 24.800, 29.900, 35.600, 41.200, 46.900, 62.900,
                85.700, 106.000, 118.000, 115.000, 103.000, 92.500, 86.400,
                82.300, 78.300, 74.400, 70.600, 69.600, 71.400, 76.000, 82.400,
                89.100, 88.900, 84.300, 74.500, 64.500, 55.300, 49.600, 45.100,
                42.500, 40.000, 37.900, 36.700, 35.500, 34.300, 33.000, 31.800,
                30.600, 29.400]},
            index=pnd.date_range(
                dt(2014, 11, 3, 12), dt(2014, 11, 5, 12), freq=td(hours=1))
        )
        # Chapelette qsim
        serie_qi = pnd.DataFrame({
            'qi': [
                39.851, 41.729, 44.025, 47.412, 49.824, 52.130, 54.667, 56.973,
                59.510, 61.357, 63.434, 65.281, 67.359, 69.707, 72.285, 74.633,
                77.212, 83.902, 90.822, 97.512, 104.433, 113.962, 123.721,
                133.250, 143.010, 150.576, 158.374, 165.940, 173.738, 178.632,
                183.758, 188.652, np.nan, np.nan, np.nan, 189.545, 187.875,
                184.431, 179.212, 173.993, 168.774, 164.140, 160.091, 156.041,
                151.991, 148.901, 146.772, 144.643, 142.514]},
            index=pnd.date_range(
                dt(2014, 11, 4), dt(2014, 11, 4, 12), freq=td(minutes=15))
        )
        self.series_q = Series(datatype='obs', name='flow')
        serie_qh = Serie(datval=serie_qh, code='qh', varname='QH')
        self.series_q.add(serie=serie_qh)
        serie_qi = Serie(datval=serie_qi, code='qi', varname='QI')
        self.series_q.add(serie=serie_qi)
        # =====================================================================
        # La Valette obs
        serie_hh = pnd.DataFrame({
            'hh': [
                -4.400, -4.400, -4.410, -4.410, -4.420, -4.420, -4.420, -4.420,
                -4.400, -4.380, -4.330, -4.260, -4.220, -4.200, -4.190, -4.200,
                -4.180, -4.160, -4.150, -4.170, -4.170, -4.170, -4.210, -4.250,
                -4.270, -4.300, -4.320, -4.330, -4.330, -4.330, -4.310, -4.270,
                -4.240, -4.230, -4.260, -4.300, -4.320, -4.350, -4.380, -4.420,
                -4.440, -4.440, -4.460, -4.450, -4.440, -4.440, -4.450, -4.450,
                -4.460]},
            index=pnd.date_range(
                dt(2014, 11, 3, 12), dt(2014, 11, 5, 12), freq=td(hours=1))
        )
        # Chapelette obs
        serie_hi = pnd.DataFrame({
            'hi': [
                -0.860, -0.840, -0.870, -0.890, -0.820, -0.830, -0.840, -0.870,
                -0.840, -0.850, -0.850, -0.800, -0.300, 0.410, 0.650, 0.740,
                0.820, 0.790, 0.980, 1.200, 1.220, 1.260, 1.270, 1.200, 1.110,
                1.110, 1.100, 1.030, 1.020, 1.040, 1.030, 1.030, 1.100, 1.190,
                1.270, 1.230, 1.070, 1.050, 0.950, 0.940, 0.780, 0.770, 0.770,
                0.650, 0.650, 0.630, 0.640, 0.640, 0.640]},
            index=pnd.date_range(
                dt(2014, 11, 3, 12), dt(2014, 11, 5, 12), freq=td(hours=1))
        )
        self.series_h = Series(datatype='obs', name='level')
        serie_hh = Serie(datval=serie_hh, code='hh', varname='HH')
        self.series_h.add(serie=serie_hh)
        serie_hi = Serie(datval=serie_hi, code='hi', varname='HI')
        self.series_h.add(serie=serie_hi)
        # =====================================================================
        # La Valette obs
        serie_zh = pnd.DataFrame({
            'zh': [
                805.740, 805.740, 805.730, 805.730, 805.720, 805.720, 805.720,
                805.720, 805.740, 805.760, 805.810, 805.880, 805.920, 805.940,
                805.950, 805.940, 805.960, 805.980, 805.990, 805.970, 805.970,
                805.970, 805.930, 805.890, 805.870, 805.840, 805.820, 805.810,
                805.810, 805.810, 805.830, 805.870, 805.900, 805.910, 805.880,
                805.840, 805.820, 805.790, 805.760, 805.720, 805.700, 805.700,
                805.680, 805.690, 805.700, 805.700, 805.690, 805.690, 805.680
                ]},
            index=pnd.date_range(
                dt(2014, 11, 3, 12), dt(2014, 11, 5, 12), freq=td(hours=1))
        )
        # La Valette sim
        serie_zi = pnd.DataFrame({
            'zi': [
                805.793, 805.795, 805.796, 805.797, 805.799, 805.800, 805.802,
                805.803, 805.805, 805.806, 805.808, 805.810, 805.811, 805.813,
                805.815, 805.817, 805.819, 805.820, 805.823, 805.825, 805.827,
                805.829, 805.832, 805.835, 805.839, 805.843, 805.847, 805.852,
                805.858, 805.864, 805.871, 805.880, 805.889, 805.899, 805.911,
                805.923, 805.936, 805.950, 805.964, 805.979, 805.994, 806.009,
                806.017, 806.017, 806.017, 806.018, 806.018, 806.018, 806.019
                ]},
            index=pnd.date_range(
                dt(2014, 11, 3, 12), dt(2014, 11, 4), freq=td(minutes=15))
        )
        self.series_z = Series(datatype='obs', name='level')
        serie_zh = Serie(datval=serie_zh, code='zh', varname='ZH')
        self.series_z.add(serie=serie_zh)
        serie_zi = Serie(datval=serie_zi, code='zi', varname='ZI')
        self.series_z.add(serie=serie_zi)
        # =====================================================================
        serie_qinf = serie_qh.data_frame * 0.75
        serie_qsup = serie_qh.data_frame * 1.25
        self.series_u = Series(datatype='obs', name='uncert')
        serie_qinf = Serie(datval=serie_qinf, code='Q_INF', varname='QH')
        self.series_u.add(serie=serie_qinf)
        serie_qsup = Serie(datval=serie_qsup, code='Q_SUP', varname='QH')
        self.series_u.add(serie=serie_qsup)
        # =====================================================================

    def test_plot_pandas(self):
        """
        Test plot - time series - event
        """
        # =====================================================================
        series = Series(datatype='obs', name='test_plot_pandas')
        series.extend(self.series_q)
        tmp_file = os.path.join('data', 'test_plot_pandas.png')
        # =====================================================================
        tmp_file = series.plot_bypandas(filename=tmp_file)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_event(self):
        """
        Test plot - time series - event
        """
        # =====================================================================
        series = Series(datatype='obs', name='event')
        series.extend(self.series_q)
        series.extend(self.series_p)
        series.extend(self.series_t)
        # =====================================================================
        config = {
            'figure': {
                'plottype': 'event',
                'dirname': 'data',
                'filename': 'test_plot_event',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_event',
                'xtitle': 'temps',
                'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [0, 25, 6],
                'ytitlep': 'prcp ($mm.h^{-1}$)',
                'ytitlet': 'temp ($Celsius$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'ylimp': [0, 30],
                'ylimt': [-5, 20],
                'ylimq': [0, 200],
                'legend': 0,
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'qh_QH': {
                'color': [0.2, 0.6, 1.0],
                'label': 'Station 1 (QH)'
            },
            'qi_QI': {
                'color': [0.0, 0.0, 1.0],
                'label': 'Station 2 (QI)'
            },
            'ph1_PH': {
                'color': [1.0, 0.0, 0.4],
                'label': 'Station 1 (PH)'
            },
            'th_TH': {
                'color': [1.0, 0.6, 0.6],
                'label': 'Station 1 (TH)'
            },
            'ph2_PH': {
                'color': [0.0, 0.4, 0.0],
                'label': 'Station 2 (PH)'
            },
            'ti_TI': {
                'color': [0.4, 1.0, 0.4],
                'label': 'Station 2 (TI)'
            },
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='event', config=config)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_event_bottom(self):
        """
        Test plot - time series - event avec légende en bas
        """
        # =====================================================================
        series = Series(datatype='obs', name='event')
        series.extend(self.series_q)
        series.extend(self.series_p)
        series.extend(self.series_t)
        # =====================================================================
        config = {
            'figure': {
                'plottype': 'event',
                'dirname': 'data',
                'filename': 'test_plot_event_bottom',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_event',
                'xtitle': 'temps',
                'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [0, 25, 6],
                'ytitlep': 'prcp ($mm.h^{-1}$)',
                'ytitlet': 'temp ($Celsius$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'ylimp': [0, 30],
                'ylimt': [-5, 20],
                'ylimq': [0, 200],
                'legend': 9,
                'legendcol': 3,
                'legendbottom': 0.10,
                'legendbox': (0.5, -0.30),
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'qh_QH': {
                'color': [0.2, 0.6, 1.0],
                'label': 'Station 1 (QH)'
            },
            'qi_QI': {
                'color': [0.0, 0.0, 1.0],
                'label': 'Station 2 (QI)'
            },
            'ph1_PH': {
                'color': [1.0, 0.0, 0.4],
                'label': 'Station 1 (PH)'
            },
            'th_TH': {
                'color': [1.0, 0.6, 0.6],
                'label': 'Station 1 (TH)'
            },
            'ph2_PH': {
                'color': [0.0, 0.4, 0.0],
                'label': 'Station 2 (PH)'
            },
            'ti_TI': {
                'color': [0.4, 1.0, 0.4],
                'label': 'Station 2 (TI)'
            },
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='event', config=config)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_hydro(self):
        """
        Test de la fonction graphique 'hydrogramme'
        """
        # =====================================================================
        series = Series(datatype='obs', name='hydro')
        series.extend(self.series_q)
        # =====================================================================
        config = {
            'figure': {
                'plottype': 'hydro',
                'dirname': 'data',
                'filename': 'test_plot_hydro',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_hydro',
                'xtitle': 'temps',
                'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [0, 25, 6],
                'ytitleh': 'hauteur ($m$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'legend': 0,
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'qh_QH': {
                'color': [0.2, 0.6, 1.0],
                'label': 'Station 1 (QH)'
            },
            'qi_QI': {
                'color': [0.0, 0.0, 1.0],
                'label': 'Station 2 (QI)'
            },
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='hydro', config=config)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_hydro_bottom(self):
        """
        Test de la fonction graphique 'hydrogramme'
        """
        # =====================================================================
        series = Series(datatype='obs', name='hydro')
        series.extend(self.series_q)
        # =====================================================================
        config = {
            'figure': {
                'plottype': 'hydro',
                'dirname': 'data',
                'filename': 'test_plot_hydro_bottom',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_hydro_bottom',
                'xtitle': 'temps',
                'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [0, 25, 6],
                'ytitleh': 'hauteur ($m$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'legend': 9,
                'legendcol': 4,
                'legendbottom': 0.10,
                'legendbox': (0.5, -0.20),
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'qh_QH': {
                'color': [0.2, 0.6, 1.0],
                'label': 'Station 1 (QH)'
            },
            'qi_QI': {
                'color': [0.0, 0.0, 1.0],
                'label': 'Station 2 (QI)'
            },
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='hydro', config=config)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_hydro_error(self):
        """
        Test de la fonction graphique 'hydrogramme'
        """
        # =====================================================================
        series = Series(datatype='obs', name='hydro')
        series.extend(self.series_q)
        series.extend(self.series_h)
        # =====================================================================
        with self.assertRaises(ValueError):
            series.plot_series(plottype='hydro', config={})
        # =====================================================================

    def test_plot_hydro_uncert(self):
        """
        Test de la fonction graphique 'hydrogramme' + intervalle incertitude
        """
        # =====================================================================
        series = Series(datatype='obs', name='hydro')
        series.extend(self.series_q)
        uncert = {'qh_QH': self.series_u}
        # =====================================================================
        config = {
            'figure': {
                'plottype': 'hydro',
                'dirname': 'data',
                'filename': 'test_plot_hydro_uncert',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_hydro_uncert',
                'xtitle': 'temps',
                'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [0, 25, 6],
                'ytitleh': 'hauteur ($m$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'legend': 0,
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'qh_QH': {
                'color': [0.2, 0.6, 1.0],
                'label': 'Station 1 (QH)'
            },
            'qi_QI': {
                'color': [0.0, 0.0, 1.0],
                'label': 'Station 2 (QI)'
            },
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(
            plottype='hydro', config=config, uncert=uncert)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_hydro_fill_threshold_down(self):
        """
        Test de la fonction graphique 'hydrogramme' + volume sub-seuil
        """
        # =====================================================================
        series = Series(datatype='obs', name='hydro')
        series.extend(self.series_q)
        fill_threshold = {'qh_QH': [(50, 'tab:pink'), (80, 'tab:purple')]}
        fill_reverse = True
        # =====================================================================
        config = {
            'figure': {'plottype': 'hydro', 'dirname': 'data',
                       'filename': 'test_plot_hydro_fill_threshold_down',
                       'verbose': 'True', 'dpi': 150, 'format': 'png',
                       'title': 'test_plot_hydro_fill_threshold_down',
                       'xtitle': 'temps', 'xfmt': '%Y-%m-%d\n%H:%M',
                       'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                       'xminor': [0, 25, 6], 'ytitleq': 'debit ($m^3.s^{-1}$)',
                       'legend': 0, 'lfontsize': 8, 'xfontsize': 10,
                       'yfontsize': 10, 'tfontsize': 14},
            'defaut': {'linewidth': 1, 'linestyle': '-',
                       'marker': '', 'markersize': 0},
            'qh_QH': {'color': [0.2, 0.6, 1.0], 'label': 'Station 1 (QH)'},
            'qi_QI': {'color': [0.0, 0.0, 1.0], 'label': 'Station 2 (QI)'},
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='hydro', config=config,
                                      fill_threshold=fill_threshold,
                                      fill_reverse=fill_reverse)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(filename, tmp_file))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_hydro_fill_threshold_up(self):
        """
        Test de la fonction graphique 'hydrogramme' + volume sup-seuil
        """
        # =====================================================================
        series = Series(datatype='obs', name='hydro')
        series.extend(self.series_q)
        fill_threshold = {'qh_QH': [(80, 'tab:olive'), (100, 'tab:orange')]}
        fill_reverse = False
        # =====================================================================
        config = {
            'figure': {'plottype': 'hydro', 'dirname': 'data',
                       'filename': 'test_plot_hydro_fill_threshold_up',
                       'verbose': 'True', 'dpi': 150, 'format': 'png',
                       'title': 'test_plot_hydro_fill_threshold_up',
                       'xtitle': 'temps', 'xfmt': '%Y-%m-%d\n%H:%M',
                       'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                       'xminor': [0, 25, 6], 'ytitleq': 'debit ($m^3.s^{-1}$)',
                       'legend': 0, 'lfontsize': 8, 'xfontsize': 10,
                       'yfontsize': 10, 'tfontsize': 14},
            'defaut': {'linewidth': 1, 'linestyle': '-',
                       'marker': '', 'markersize': 0},
            'qh_QH': {'color': [0.2, 0.6, 1.0], 'label': 'Station 1 (QH)'},
            'qi_QI': {'color': [0.0, 0.0, 1.0], 'label': 'Station 2 (QI)'},
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='hydro', config=config,
                                      fill_threshold=fill_threshold,
                                      fill_reverse=fill_reverse)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(filename, tmp_file))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_hydrolimni(self):
        """
        Test de la fonction graphique 'hydrogramme / limnigramme'
        """
        # =====================================================================
        series = Series(datatype='obs', name='hydro_limni')
        series.extend(self.series_q)
        series.extend(self.series_h)
        # =====================================================================
        config = {
            'figure': {
                'plottype': 'hydro_limni',
                'dirname': 'data',
                'filename': 'test_plot_hydrolimni',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_hydrolimni',
                'xtitle': 'temps',
                'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [0, 25, 6],
                'ytitleh': 'hauteur ($m$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'legend': 0,
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'qh_QH': {
                'color': [0.2, 0.6, 1.0],
                'label': 'Station 1 (QH)'
            },
            'qi_QI': {
                'color': [0.0, 0.0, 1.0],
                'label': 'Station 2 (QI)'
            },
            'hh_HH': {
                'color': [1.0, 0.0, 0.4],
                'label': 'Station 3 (HH)'
            },
            'hi_HI': {
                'color': [1.0, 0.0, 0.0],
                'label': 'Station 4 (HI)'
            },
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='hydro_limni', config=config)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_hydrolimni_bottom(self):
        """
        Test de la fonction graphique 'hydrogramme / limnigramme'
        """
        # =====================================================================
        series = Series(datatype='obs', name='hydro_limni')
        series.extend(self.series_q)
        series.extend(self.series_h)
        # =====================================================================
        config = {
            'figure': {
                'plottype': 'hydro_limni',
                'dirname': 'data',
                'filename': 'test_plot_hydrolimni_bottom',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_hydrolimni_bottom',
                'xtitle': 'temps',
                'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [0, 25, 6],
                'ytitleh': 'hauteur ($m$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'legend': 9,
                'legendcol': 4,
                'legendbottom': 0.10,
                'legendbox': (0.5, -0.20),
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'qh_QH': {
                'color': [0.2, 0.6, 1.0],
                'label': 'Station 1 (QH)'
            },
            'qi_QI': {
                'color': [0.0, 0.0, 1.0],
                'label': 'Station 2 (QI)'
            },
            'hh_HH': {
                'color': [1.0, 0.0, 0.4],
                'label': 'Station 3 (HH)'
            },
            'hi_HI': {
                'color': [1.0, 0.0, 0.0],
                'label': 'Station 4 (HI)'
            },
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='hydro_limni', config=config)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_hyeto(self):
        """
        Test de la fonction graphique 'hyétogramme'
        """
        # =====================================================================
        series = Series(datatype='obs', name='hyeto')
        series.extend(self.series_p)
        # =====================================================================
        config = {
            'figure': {
                'plottype': 'hyeto',
                'dirname': 'data',
                'filename': 'test_plot_hyeto',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_hyeto',
                'xtitle': 'temps',
                'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [0, 25, 6],
                'ytitleh': 'hauteur ($m$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'legend': 0,
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'ph1_PH': {
                'color': [1.0, 0.0, 0.4],
                'label': 'Station 1 (PH)'
            },
            'ph2_PH': {
                'edgecolor': [0.0, 0.4, 0.0],
                'facecolor': [0.0, 0.4, 0.0, 0.5],
                'label': 'Station 2 (PH)'
            },
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='hyeto', config=config)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_hyeto_bottom(self):
        """
        Test de la fonction graphique 'hyétogramme'
        """
        # =====================================================================
        series = Series(datatype='obs', name='hyeto')
        series.extend(self.series_p)
        # =====================================================================
        config = {
            'figure': {
                'plottype': 'hyeto',
                'dirname': 'data',
                'filename': 'test_plot_hyeto_bottom',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_hyeto_bottom',
                'xtitle': 'temps',
                'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [0, 25, 6],
                'ytitleh': 'hauteur ($m$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'legend': 9,
                'legendcol': 4,
                'legendbottom': 0.10,
                'legendbox': (0.5, -0.20),
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'ph1_PH': {
                'color': [1.0, 0.0, 0.4],
                'label': 'Station 1 (PH)'
            },
            'ph2_PH': {
                'edgecolor': [0.0, 0.4, 0.0],
                'facecolor': [0.0, 0.4, 0.0, 0.5],
                'label': 'Station 2 (PH)'
            },
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='hyeto', config=config)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_reservoir_H(self):
        """
        Test de la fonction graphique 'réservoir'
        """
        # =====================================================================
        series = Series(datatype='obs', name='reservoir')
        series.extend(self.series_q)
        series.extend(self.series_h)
        # =====================================================================
        config = {
            'figure': {
                'plottype': 'hydro_limni',
                'dirname': 'data',
                'filename': 'test_plot_reservoir_H',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_reservoir_H',
                'xtitle': 'temps',
                'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [0, 25, 6],
                'ytitleh': 'hauteur ($m$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'legend': 0,
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'qh_QH': {
                'color': [0.2, 0.6, 1.0],
                'label': 'Station 1 (QH)'
            },
            'qi_QI': {
                'color': [0.0, 0.0, 1.0],
                'label': 'Station 2 (QI)'
            },
            'hh_HH': {
                'color': [1.0, 0.0, 0.4],
                'label': 'Station 3 (HH)'
            },
            'hi_HI': {
                'color': [1.0, 0.0, 0.0],
                'label': 'Station 4 (HI)'
            },
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='reservoir', config=config)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_reservoir_Z(self):
        """
        Test de la fonction graphique 'réservoir'
        """
        # =====================================================================
        series = Series(datatype='obs', name='reservoir')
        series.extend(self.series_q)
        series.extend(self.series_z)
        # =====================================================================
        config = {
            'figure': {
                'plottype': 'reservoir',
                'dirname': 'data',
                'filename': 'test_plot_reservoir_Z',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_reservoir_Z',
                'xtitle': 'temps',
                'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [0, 25, 6],
                'ytitleh': 'hauteur ($m$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'legend': 0,
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'qh_QH': {
                'color': [0.2, 0.6, 1.0],
                'label': 'Station 1 (QH)'
            },
            'qi_QI': {
                'color': [0.0, 0.0, 1.0],
                'label': 'Station 2 (QI)'
            },
            'zh_ZH': {
                'color': [1.0, 0.0, 0.4],
                'label': 'Station 3 (ZH)'
            },
            'zi_ZI': {
                'color': [1.0, 0.0, 0.0],
                'label': 'Station 4 (ZI)'
            },
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='reservoir', config=config)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_reservoir_ZH(self):
        """
        Test de la fonction graphique 'réservoir'
        """
        # =====================================================================
        series = Series(datatype='obs', name='reservoir')
        series.extend(self.series_q)
        series.extend(self.series_h)
        series.extend(self.series_z)
        # =====================================================================
        config = {
            'figure': {
                'plottype': 'hydro_limni',
                'dirname': 'data',
                'filename': 'test_plot_reservoir_ZH',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_reservoir_ZH',
                'xtitle': 'temps',
                'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [0, 25, 6],
                'ytitleh': 'hauteur ($m$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'legend': 0,
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'qh_QH': {
                'color': [0.2, 0.6, 1.0],
                'label': 'Station 1 (QH)'
            },
            'qi_QI': {
                'color': [0.0, 0.0, 1.0],
                'label': 'Station 2 (QI)'
            },
            'zh_ZH': {
                'color': [1.0, 0.0, 0.4],
                'label': 'Station 3 (ZH)'
            },
            'zi_ZI': {
                'color': [1.0, 0.0, 0.0],
                'label': 'Station 4 (ZI)'
            },
            'hh_HH': {
                'color': [1.0, 0.0, 0.4],
                'label': 'Station 3 (HH)',
                'linestyle': '--',
            },
            'hi_HI': {
                'color': [1.0, 0.0, 0.0],
                'label': 'Station 4 (HI)',
                'linestyle': '--',
            },
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='reservoir', config=config)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_reservoir_Z_bottom(self):
        """
        Test de la fonction graphique 'réservoir'
        """
        # =====================================================================
        series = Series(datatype='obs', name='reservoir')
        series.extend(self.series_q)
        series.extend(self.series_z)
        # =====================================================================
        config = {
            'figure': {
                'plottype': 'hydro_limni',
                'dirname': 'data',
                'filename': 'test_plot_reservoir_Z_bottom',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_reservoir_Z_bottom',
                'xtitle': 'temps',
                'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [0, 25, 6],
                'ytitleh': 'hauteur ($m$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'legend': 9,
                'legendcol': 4,
                'legendbottom': 0.05,
                'legendbox': (0.5, -0.35),
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'qh_QH': {
                'color': [0.2, 0.6, 1.0],
                'label': 'Station 1 (QH)'
            },
            'qi_QI': {
                'color': [0.0, 0.0, 1.0],
                'label': 'Station 2 (QI)'
            },
            'zh_ZH': {
                'color': [1.0, 0.0, 0.4],
                'label': 'Station 3 (ZH)'
            },
            'zi_ZI': {
                'color': [1.0, 0.0, 0.0],
                'label': 'Station 4 (ZI)'
            },
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='reservoir', config=config)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_plot_subreach(self):
        """
        Test de la fonction graphique 'subreach'
        """
        # =====================================================================
        series = Series(datatype='obs', name='subreach')
        series.extend(self.series_q)
        series.extend(self.series_h)
        series.extend(self.series_p)
        # =====================================================================
        config = {
            'figure': {
                'plottype': 'subreach',
                'dirname': 'data',
                'filename': 'test_plot_subreach',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_subreach',
                'xtitle': 'temps',
                'xlim': (dt(2014, 11, 3), dt(2014, 11, 6)),
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [0, 25, 6],
                'ytitlep': 'prcp ($mm.h^{-1}$)',
                'ytitleh': 'hauteur ($m$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'ylimp': [0, 30],
                'ylimh': [-5, 2],
                'ylimq': [0, 200],
                'legend': 0,
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'qh_QH': {
                'color': [0.2, 0.6, 1.0],
                'label': 'Station 1 (QH)'
            },
            'qi_QI': {
                'color': [0.0, 0.0, 1.0],
                'label': 'Station 2 (QI)'
            },
            'hh_HH': {
                'color': [1.0, 0.0, 0.4],
                'label': 'Station 3 (HH)'
            },
            'hi_HI': {
                'color': [1.0, 0.0, 0.0],
                'label': 'Station 4 (HI)'
            },
            'ph1_PH': {
                'color': [1.0, 0.0, 0.4],
                'label': 'Station 1 (PH)'
            },
            'ph2_PH': {
                'color': [0.0, 0.4, 0.0],
                'label': 'Station 2 (PH)'
            },
        }
        for section in config:
            if section in ['figure', 'defaut']:
                continue
            for option in config['defaut']:
                config[section].setdefault(option, config['defaut'][option])
        # =====================================================================
        tmp_file = series.plot_series(plottype='subreach', config=config)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================
