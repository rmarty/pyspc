#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary xmlSandre2csv

To run all tests just type:
    python -m unittest test_bin_xmlSandre2csv

To run only a class test:
    python -m unittest test_bin_xmlSandre2csv.Test_xmlSandre2csv

To run only a specific test:
    python -m unittest test_bin_xmlSandre2csv.Test_xmlSandre2csv.test_data_obs_hydro

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import xmlSandre2csv as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_xmlSandre2csv(unittest.TestCase):
    """
    xmlSandre2csv bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'xmlSandre2csv')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dirname2 = os.path.join('data', 'data', 'sandre')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_data_obs_hydro_grp16(self):
        """
        Test export PHyC (data_obs_hydro)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandre2csv.py',
            '-I', self.dirname2,
            '-d', 'dataobs_hydro_Q.xml',
            '-t', 'data_obs_hydro',
            '-O', self.dir_out,
            '-C', 'grp16'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.xmlSandre2csv()
        self.assertEqual(args.xml_filename, 'dataobs_hydro_Q.xml')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'data_obs_hydro')
        self.assertEqual(args.modscen, None)
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'grp16')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'K5183020_Q.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} "\
            "du format XML Sandre de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_data_obs_meteo_grp16(self):
        """
        Test export PHyC (data_obs_hydro)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandre2csv.py',
            '-I', self.dirname2,
            '-d', 'dataobs_meteo.xml',
            '-t', 'data_obs_meteo',
            '-O', self.dir_out,
            '-C', 'grp16'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.xmlSandre2csv()
        self.assertEqual(args.xml_filename, 'dataobs_meteo.xml')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'data_obs_meteo')
        self.assertEqual(args.modscen, None)
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'grp16')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = '23209001_P.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} "\
            "du format XML Sandre de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_data_obs_meteo_grp18(self):
        """
        Test export PHyC (data_obs_hydro)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandre2csv.py',
            '-I', self.dirname2,
            '-d', 'dataobs_meteo.xml',
            '-t', 'data_obs_meteo',
            '-O', self.dir_out,
            '-C', 'grp18'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.xmlSandre2csv()
        self.assertEqual(args.xml_filename, 'dataobs_meteo.xml')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'data_obs_meteo')
        self.assertEqual(args.modscen, None)
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'grp18')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = '23209001_P_00J01H00M.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} "\
            "du format XML Sandre de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_data_obs_hydro_pyspc(self):
        """
        Test export PHyC (data_obs_hydro)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandre2csv.py',
            '-I', self.dirname2,
            '-d', 'dataobs_hydro_H.xml',
            '-t', 'data_obs_hydro',
            '-O', self.dir_out,
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.xmlSandre2csv()
        self.assertEqual(args.xml_filename, 'dataobs_hydro_H.xml')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'data_obs_hydro')
        self.assertEqual(args.modscen, None)
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'K518302001_HH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} "\
            "du format XML Sandre de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_data_fcst_hydro_pyspc(self):
        """
        Test export PHyC (data_obs_hydro)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandre2csv.py',
            '-I', self.dirname2,
            '-d', 'datafcst_hydro.xml',
            '-t', 'data_fcst_hydro',
            '-O', self.dir_out,
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.xmlSandre2csv()
        self.assertEqual(args.xml_filename, 'datafcst_hydro.xml')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'data_fcst_hydro')
        self.assertEqual(args.modscen, None)
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'K1930010_2016042007_45hEAOtt00_scen_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} "\
            "du format XML Sandre de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "Les séries sont écrites dans un seul fichier."\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_modscen_pyspc(self):
        """
        Test export PHyC (data_obs_hydro)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandre2csv.py',
            '-I', self.dirname2,
            '-d', 'spcmo.xml',
            '-t', 'data_fcst_hydro',
            '-O', self.dir_out,
            '-s', 'Y2100020',
            '-U', 'model', '11gGRPd130',
            '-U', 'scen', 'ctl',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
#        with captured_output():
        args = _args.xmlSandre2csv()
        self.assertEqual(args.xml_filename, 'spcmo.xml')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, 'Y2100020')
        self.assertEqual(args.data_type, 'data_fcst_hydro')
        self.assertEqual(args.modscen, [['model', '11gGRPd130'],
                                        ['scen', 'ctl']])
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'Y2100020_2019111906_11gGRPd130_ctl_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} "\
            "du format XML Sandre de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "Les séries sont écrites dans un seul fichier. "\
            "L'extraction concerne uniquement le site {s} "\
            "pour les modèles-scénarios suivants: {U}."\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type, s=args.station_name, U=args.modscen,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_levelcor_pyspc(self):
        """
        Test export PHyC (data_obs_hydro)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandre2csv.py',
            '-I', self.dirname2,
            '-d', 'levelcor.xml',
            '-t', 'levelcor',
            '-O', self.dir_out,
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.xmlSandre2csv()
        self.assertEqual(args.xml_filename, 'levelcor.xml')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'levelcor')
        self.assertEqual(args.modscen, None)
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'K055001010_levelcor_HI.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} "\
            "du format XML Sandre de type ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================
