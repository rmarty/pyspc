#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Config in pyspc.model.plathynes

To run all tests just type:
    python -m unittest test_model_PLATHYNES_Config

To run only a class test:
    python -m unittest test_model_PLATHYNES_Config.TestPLATHYNES_Config

To run only a specific test:
    python -m unittest test_model_PLATHYNES_Config.TestPLATHYNES_Config.test_init

"""
# Imports
import filecmp
import os
import unittest

# Imports pyspc
from pyspc.model.plathynes import Config


class TestPLATHYNES_Config(unittest.TestCase):
    """
    PLATHYNES_Config class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'plathynes')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'plathynes_event.evt')
        datatype = 'event'
        cfg = Config(filename=filename, datatype=datatype)
        self.assertEqual(cfg.filename, filename)
        self.assertEqual(cfg.datatype, datatype)

    def test_datatypes(self):
        """
        Test des types de fichiers PRV traités par <pyspc>
        """
        valid = ['event', 'model', 'project', 'simul']
        self.assertEqual(valid, Config.get_types())

    def test_read_event(self):
        """
        Test de la lecture de configuration de type 'event'
        """
        valid = {
            'event settings': {
                "Nom de l'evenement": '8001',
                'Description': '',
            },
            'Temporal settings': {
                'Date de debut': '2019-10-16 08:00:00',
                'Date de fin': '2019-10-26 08:00:00',
                'Pas de temps de forcage': '01:00',
                'Pas de temps de calcul': '01:00',
                'Pas de temps des sorties': '01:00',
                'Pas de temps des bilans': '01:00',
            },
            'Parameters': {
                'evenementiels': {
                    ('Q0_moy_Chadrac', 'Parametre evenementiel'):
                        'U 98.9 1 Q0_moy_Chadrac'
                },
            },
            'Forcing settings': {
                'pluies': {
                    ('Ev_8001/8001_RRobs.mgr',
                     'Source de pluie'): 'Ev_8001/8001_RRobs.mgr'
                },
                'debits': {
                    ('Ev_8001/8001_1.mqi',
                     'Source de debits'): 'Ev_8001/8001_1.mqi'
                },
            },
            'Observations': {
                'observations': {
                    ('Ev_8001/8001_1.mqo',
                     "Fichier d'observation"): 'Ev_8001/8001_1.mqo'
                },
            },
        }
        filename = os.path.join(self.dirname, 'plathynes_event.evt')
        datatype = 'event'
        cfg = Config(filename=filename, datatype=datatype)
        cfg.read()
        self.assertDictEqual(cfg, valid)

    def test_read_project(self):
        """
        Test de la lecture de configuration de type 'event'
        """
        valid = {
            'Settings': {
                'Version': '1.8.1',
                'Nom': 'Chadrac_TR',
                'Repertoire racine': 'C:/Plathynes/PROJET_TR',
                'Bassin': 'Chadrac_TR',
            },
            'Tables set': {
                'tables': {
                    ('Sol_12_Classes.txt',
                     'Table'): 'Sol_12_Classes.txt',
                    ('Ods_12_Classes.txt',
                     'Table'): 'Ods_12_Classes.txt',
                    ('Ods_CorineLandCover.txt',
                     'Table'): 'Ods_CorineLandCover.txt',
                }
            },
            'Charts set': {
                'graphs': {}
            },
            'Structures set': {
                'structures': {}
            },
            'Stations': {
                'pluvio': {
                    ('LE_PUY_CHADRAC', 'Station pluviometrique'):
                        'LE_PUY_CHADRAC 43157008 722740.000000 2007270.000000',
                    ('LANDOS_CHARBON', 'Station pluviometrique'):
                        'LANDOS_CHARBON 43111002 719124.000000 1985280.000000',
                    ('LES_ESTABLES_SAPC', 'Station pluviometrique'):
                        'LES_ESTABLES_SAPC 43091005 744104.000000 1990535.000000',
                    ('MAZAN_ABBAYE_RAD', 'Station pluviometrique'):
                        'MAZAN_ABBAYE_RAD 07154005 738420.000000 1971872.000000',
                    ('FIX_ST_GENEYS', 'Station pluviometrique'):
                        'FIX_ST_GENEYS 43095001 704457.000000 2017020.000000',
                    ('LE_PUY_LOUDES', 'Station pluviometrique'):
                        'LE_PUY_LOUDES 43062001 712389.000000 2009227.000000',
                    ('LE_MONASTIER_SUR_GAZEILLE', 'Station pluviometrique'):
                        'LE_MONASTIER_SUR_GAZEILLE 43135003 730405.000000 1995324.000000',
                    ('CAYRES', 'Station pluviometrique'):
                        'CAYRES 43042002 716010.000000 1992118.000000',
                    ('GOUDET2', 'Station pluviometrique'):
                        'GOUDET2 43101002 725249.000000 1988805.000000',
                    ('MACHABERT', 'Station pluviometrique'):
                        'MACHABERT 43186003 740559.000000 1999403.000000',
                    ('LE_LAC_D_ISSARLES', 'Station pluviometrique'):
                        'LE_LAC_D_ISSARLES 07119002 736900.000000 1981781.000000',
                    ('SAINTE_EULALIE', 'Station pluviometrique'):
                        'SAINTE_EULALIE 07235005 746566.000000 1980608.000000',
                    ('Fix_Saint_Geneys_SPC', 'Station pluviometrique'):
                        'Fix_Saint_Geneys_SPC 43095003 704500.000000 2017019.000000',
                    ('FELINES_SAPC', 'Station pluviometrique'):
                        'FELINES_SAPC 43093001 710301.000000 2030537.000000',
                    ('Lanarce_SPC', 'Station pluviometrique'):
                        'Lanarce_SPC 07130003 732451.625000 1970793.875000',
                },
                'hydro': {
                    ('LaLoireChadrac', 'Station hydro'):
                        'LaLoireChadrac K0260010 723552.000000 2008719.000000 1.000000',
                    ('LaLoireChadrac', 'Debit de base'):
                        'LaLoireChadrac 1 FIRST_OBS LaLoireChadrac 1.000000',
                    ('Pont_la_Borie', 'Station hydro'):
                        'Pont_la_Borie K0030020 735667.000000 1981483.000000 227.000000',
                },
            },
            'Configurations': {
                'configurations': {
                    ('_SCS_2LR_classique', 'Configuration'):
                        '_SCS_2LR_classique ACTIVE'
                }
            },
            'Events': {
                'events': {
                    ('8001', 'Evenement'): '8001 -1 0 0.0 1',
                    ('8002', 'Evenement'): '8002 -1 0 0.0 1',
                    ('8003', 'Evenement'): '8003 -1 0 0.0 1',
                    ('8004', 'Evenement'): '8004 -1 0 0.0 1',
                    ('8005', 'Evenement'): '8005 -1 0 0.0 1',
                    ('8006', 'Evenement'): '8006 -1 0 0.0 1',
                    ('8007', 'Evenement'): '8007 -1 0 0.0 1',
                    ('8008', 'Evenement'): '8008 -1 0 0.0 1',
                    ('8009', 'Evenement'): '8009 -1 0 0.0 1',
                    ('8010', 'Evenement'): '8010 -1 0 0.0 1',
                }
            },
            'Simulations': {
                'simulations': {
                    ('SIM', 'Simulation'): 'SIM'
                }
            },
        }
        filename = os.path.join(self.dirname, 'plathynes_project.prj')
        datatype = 'project'
        cfg = Config(filename=filename, datatype=datatype)
        cfg.read()
        self.assertDictEqual(cfg, valid)

    def test_write_event(self):
        """
        Test de la lecture de configuration de type 'event'
        """
        filename = os.path.join(self.dirname, 'plathynes_event.evt')
        datatype = 'event'
        cfg = Config(filename=filename, datatype=datatype)
        cfg.read()
        tmp_filename = os.path.join('data', 'test_plathynes_event.evt')
        cfg.filename = tmp_filename
        cfg.write()
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_filename
        ))
        os.remove(tmp_filename)

    def test_write_project(self):
        """
        Test de la lecture de configuration de type 'event'
        """
        filename = os.path.join(self.dirname, 'plathynes_project.prj')
        datatype = 'project'
        cfg = Config(filename=filename, datatype=datatype)
        cfg.read()
        tmp_filename = os.path.join('data', 'test_plathynes_project.prj')
        cfg.filename = tmp_filename
        cfg.write()
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_filename
        ))
        os.remove(tmp_filename)
