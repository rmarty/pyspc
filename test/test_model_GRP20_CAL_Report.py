#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for functions in pyspc.model.grp20.cal_report

To run all tests just type:
    python -m unittest test_model_GRP20_CAL_Report

To run only a class test:
    python -m unittest test_model_GRP20_CAL_Report.TestGRP_Report

To run only a specific test:
    python -m unittest test_model_GRP20_CAL_Report.TestGRP_Report.test_index2colors

"""
# Imports
from datetime import datetime as dt
import filecmp
import os
import pandas as pnd
import unittest

from pyspc.model.grp20 import GRP_Basin, GRP_Cfg
from pyspc.model.grp20.cal_report import (index2colors, report_model,
                                          report_verif)


class TestGRP_Report(unittest.TestCase):
    """
    CAL_Report functions test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp20', 'cal', 'report')
        runs = GRP_Cfg(os.path.join(self.dirname, 'LISTE_BASSINS.DAT'))
        runs.read()
        self.run = runs[0]

    def test_index2colors(self):
        """
        Test index2colors
        """
        # ===================================================================
        valid = {
            ('SMN_TAN', None, None): 'tab:blue',
            ('SMN_RNA', None, None): 'tab:red',
            ('AMN_TAN', None, None): 'tab:cyan',
            ('AMN_RNA', None, None): 'tab:orange',
            ('1234', None, None): 'tab:brown'
        }
        # ===================================================================
        colors = index2colors(valid.keys())
        self.assertListEqual(list(valid.values()), colors)
        # ===================================================================

    def test_report_model(self):
        """
        Test report_model
        """
        # ===================================================================
        tmpfile = os.path.join('data', 'test_report_model.pdf')
        tmpfile2 = os.path.join('data', 'test_report_model_hypso.pdf')
        # ===================================================================
        # ===================================================================
        bv_info = GRP_Basin(
            filename=os.path.join(self.dirname, 'K0403010_00J01H00M.DAT'))
        bv_info.read()
        bv_info['start'] = dt(2000, 1, 1)
        bv_info['end'] = dt(2023, 1, 1)
        bv_info['mv'] = 0.05
        for v in ['P', 'T']:
            for s in bv_info[v]:
                bv_info[v][s]['start'] = bv_info['start']
                bv_info[v][s]['end'] = bv_info['end']
                bv_info[v][s]['mv'] = bv_info['mv']
        # ===================================================================
        # CAS SANS HYPSO
        report_model(tmpfile, self.run, bv_info)
        self.assertTrue(os.path.exists(tmpfile))
#        self.assertTrue(filecmp.cmp(
#            os.path.join(self.dirname, os.path.basename(tmpfile)),
#            tmpfile
#        ))
        os.remove(tmpfile)
        # ===================================================================
        # CAS AVEC HYPSO
        hypso = pnd.DataFrame(
            {'pct': [100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0],
             'Z': [925.2, 1013.9, 1045.5, 1071.3, 1096.8, 1123.5, 1160.5,
                   1195.9, 1243.7, 1303.9, 1721.4]}
        )
        hypso.pct = hypso.pct.astype(int)
        report_model(tmpfile2, self.run, bv_info, hypso, hypso_lim=(0, 2000))
        self.assertTrue(os.path.exists(tmpfile2))
        os.remove(tmpfile2)
        # ===================================================================

    def test_report_verif(self):
        """
        Test report_verif
        """
        # ===================================================================
        # CAS CALAGE (TESTS)
        filenames = report_verif(self.run, self.dirname, 'cal', 'data')
        for f in filenames:
            if f.endswith('.pdf'):
                self.assertTrue(os.path.exists(f))
                os.remove(f)
            else:
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dirname, os.path.basename(f)),
                    f
                ))
                os.remove(f)
        # ===================================================================
        # CAS CALAGE COMPLET (CALAG)
        filenames = report_verif(self.run, self.dirname, 'rtime', 'data')
        for f in filenames:
            if f.endswith('.pdf'):
                self.assertTrue(os.path.exists(f))
                os.remove(f)
            else:
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dirname, os.path.basename(f)),
                    f
                ))
                os.remove(f)
        # ===================================================================
