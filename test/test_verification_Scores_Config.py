#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Config in pyspc.verification.scores

To run all tests just type:
    python -m unittest test_verification_Scores_Config

To run only a class test:
    python -m unittest test_verification_Scores_Config.TestScores_Config

To run only a specific test:
    python -m unittest test_verification_Scores_Config.TestScores_Config.test_init

"""
# Imports
import filecmp
import os
import unittest

# Imports pyspc
from pyspc.verification.scores import Config


class TestScores_Config(unittest.TestCase):
    """
    Scores_Config class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.source = os.path.join('data', 'verification', 'scores',
                                   'config_defaut.cfg')
        self.nocomment = os.path.join('data', 'verification', 'scores',
                                      'config_nocomment.cfg')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        cfg = Config(filename=self.source)
        self.assertEqual(cfg.filename, self.source)

    def test_update(self):
        """
        Test de la mise à jour de la configuration
        """
        new_info = {
            ('GENERAL', 'ENTITE'): 'K5383020',
            ('OBSERVATION', 'DONNEES_OBSERVATION'): 'obs/fichier_1.csv, obs/fichier_2.csv',
            ('PREVISION', 'DONNEES_PREVISION'): 'fcst/fichier_1.csv, fcst/fichier_2.csv',
            ('PREVISION', 'LISTE_MODELE'): '45gGRPd000',
            ('PREVISION', 'LISTE_ECHEANCE'): '0, 6, 12, 24, 48, 72',
            ('EXPORT', 'DOSSIER_EXPORT_PREVISION_ECHEANCE_FIXE'): 'output/fcst_ltime',
            ('CALCUL', 'FICHIER_RESULTAT'): 'output/results.xml',
        }
        cfg = Config(filename=self.source)
        cfg.read()
        cfg.update_config(config=new_info)
        for k in new_info.keys():
            section = k[0]
            option = k[1]
            self.assertIn(section, cfg.keys())
            self.assertIn(option, cfg[section])
        tmpfile = os.path.join('data', 'config_nocomment.txt')
        cfg.filename = tmpfile
        cfg.write()
        self.assertTrue(filecmp.cmp(
            self.nocomment,
            tmpfile
        ))
        os.remove(tmpfile)

    def test_update_errors(self):
        """
        Test de la mise à jour de la configuration - CAS AVEC ERREURS
        """
        cfg = Config(filename=self.source)
        cfg.read()
        with self.assertRaises(ValueError):
            cfg.update_config()
        with self.assertRaises(ValueError):
            cfg.update_config(config=[])

    def test_cfg_keys(self):
        """
        Test des clés de configuration
        """
        valid = [
            ('GENERAL', 'FONCTIONNEMENT'),
            ('GENERAL', 'MODE'),
            ('GENERAL', 'ENTITE'),
            ('GENERAL', 'GRANDEUR'),
            ('GENERAL', 'TAUX_PAS_DE_TEMPS'),
            ('CSV', 'CSV_DELIMITEUR'),
            ('OBSERVATION', 'DONNEES_OBSERVATION'),
            ('OBSERVATION', 'LACUNE_EVENEMENT'),
            ('OBSERVATION', 'DETECTION_EVENEMENT'),
            ('OBSERVATION', 'LISTE_EVENEMENT'),
            ('PREVISION', 'DONNEES_PREVISION'),
            ('PREVISION', 'LISTE_MODELE'),
            ('PREVISION', 'LISTE_EXECUTION'),
            ('PREVISION', 'LISTE_ECHEANCE'),
            ('INTERPOLATION', 'METHODE_INTERPOLATION'),
            ('EXPORT', 'EXPORT_PREVISION_ECHEANCE_FIXE'),
            ('EXPORT', 'DOSSIER_EXPORT_PREVISION_ECHEANCE_FIXE'),
            ('CALCUL', 'FICHIER_RESULTAT'),
            ('CALCUL', 'LISTE_GROUPES_SCORES'),
            ('CALCUL', 'ERREURS_ABSOLUES'),
            ('CALCUL', 'DELAI_PREVISION'),
            ('CALCUL', 'RAPPORT_DE_POINTE'),
            ('CALCUL', 'RAPPORT_ECHELLE'),
            ('CALCUL', 'DISTRIBUTION_ERREUR'),
            ('CALCUL', 'METHODE_CALCUL_FRANCHISSEMENT_SEUIL'),
            ('CALCUL', 'LISTE_SEUIL'),
            ('CALCUL', 'SEUIL_MINIMAL'),
            ('CALCUL', 'TAUX_VALEURS_IGNOREES'),
            ('CALCUL', 'FENETRE_TEMPORELLE'),
            ('CALCUL', 'HYSTERESIS'),
            ('CALCUL', 'CALCUL_PAR_CLASSE'),
            ('CALCUL', 'NATURE_CLASSE'),
            ('CALCUL', 'PORTEE_CLASSE'),
            ('CALCUL', 'NOMBRE_CLASSE'),
            ('CALCUL', 'LISTE_CLASSE'),
            ('CALCUL', 'CALCUL_AVEC_REPHASAGE'),
            ('CALCUL', 'LISTE_REPHASAGE'),
            ('CALCUL', 'PHASE_DE_MONTEE'),
            ('CALCUL', 'DELTA_TEMPS_ENTREE_PHASE_MONTANTE'),
            ('CALCUL', 'DELTA_TEMPS_SORTIE_PHASE_MONTANTE')
        ]
        self.assertEqual(valid, Config.get_cfg_keys())
