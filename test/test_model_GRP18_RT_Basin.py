#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Basin in pyspc.model.grp18

To run all tests just type:
    python -m unittest test_model_GRP18_RT_Basin

To run only a class test:
    python -m unittest test_model_GRP18_RT_Basin.TestGRPRT_Basin

To run only a specific test:
    python -m unittest test_model_GRP18_RT_Basin.TestGRPRT_Basin.test_init

"""
# Imports
import collections
import os
import unittest

from pyspc.model.grp18 import GRPRT_Basin


class TestGRPRT_Basin(unittest.TestCase):
    """
    GRPRT_Basin class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp18', 'rt')
        self.valid = collections.OrderedDict()
        self.valid['B'] = 'La Capricieuse amont'
        self.valid['S'] = 31.0
        self.valid['A'] = 0.0
        self.valid['E'] = collections.OrderedDict()
        self.valid['E']['RH10585x'] = {'w': 1.0, 'n': ''}
        self.valid['F'] = 1687.95
        self.valid['P'] = collections.OrderedDict()
        self.valid['P']['90052002'] = {'w': 0.80, 'n': 'poste_90052002',
                                       't': '00J01H00M'}
        self.valid['P']['90065003'] = {'w': 0.20, 'n': 'poste_90065003',
                                       't': '00J01H00M'}
        self.valid['Q'] = collections.OrderedDict()
        self.valid['Q'] = 'RH10585x'
        self.valid['G'] = 5.00
        self.valid['L'] = '01J00H00M'

        self.valid_snow = collections.OrderedDict()
        self.valid_snow['B'] = 'La Capricieuse amont_Jour'
        self.valid_snow['S'] = 31.0
        self.valid_snow['A'] = 780.0
        self.valid_snow['E'] = collections.OrderedDict()
        self.valid_snow['E']['RH10585x'] = {'w': 1.0, 'n': ''}
        self.valid_snow['F'] = 1681.83
        self.valid_snow['P'] = collections.OrderedDict()
        self.valid_snow['P']['90052002'] = {'w': 0.80, 'n': 'poste_90052002',
                                            't': '00J01H00M'}
        self.valid_snow['P']['90065003'] = {'w': 0.20, 'n': 'poste_90065003',
                                            't': '00J01H00M'}
        self.valid_snow['Q'] = collections.OrderedDict()
        self.valid_snow['Q'] = 'RH10585x'
        self.valid_snow['G'] = 1.00
        self.valid_snow['D'] = collections.OrderedDict()
        self.valid_snow['D']['90035001'] = {'w': 0.33, 'z': 401.0, 'n': ''}
        self.valid_snow['D']['90052002'] = {'w': 0.33, 'z': 473.0, 'n': ''}
        self.valid_snow['D']['90065003'] = {'w': 0.33, 'z': 1153.0, 'n': ''}
        self.valid_snow['C'] = collections.OrderedDict()
        self.valid_snow['C']['1'] = {'snma': 114.68, 'z50': 531.0, 'n': ''}
        self.valid_snow['C']['2'] = {'snma': 159.08, 'z50': 667.0, 'n': ''}
        self.valid_snow['C']['3'] = {'snma': 207.52, 'z50': 780.0, 'n': ''}
        self.valid_snow['C']['4'] = {'snma': 273.07, 'z50': 903.0, 'n': ''}
        self.valid_snow['C']['5'] = {'snma': 364.96, 'z50': 1042.0, 'n': ''}
        self.valid_snow['K'] = 8.32
        self.valid_snow['N'] = 1.0
        self.valid_snow['L'] = '01J00H00M'

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'BASSIN.DAT')
        basin = GRPRT_Basin(filename=filename)
        self.assertEqual(basin.filename, filename)

    def test_read(self):
        """
        Test de la lecture du fichier - Cas sans température
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'BASSIN.DAT')
        basin = GRPRT_Basin(filename=filename)
        basin.read()
        self.assertDictEqual(basin, self.valid)
        # =====================================================================
        filename = os.path.join(self.dirname, 'BASSIN_snow.DAT')
        basin = GRPRT_Basin(filename=filename)
        basin.read()
        self.assertDictEqual(basin, self.valid_snow)
        # =====================================================================

    def test_write(self):
        """
        Test d'écriture du fichier - Cas sans température
        """
        filename = os.path.join(self.dirname, 'BASSIN.DAT')
        writer = GRPRT_Basin(filename=filename)
        with self.assertRaises(NotImplementedError):
            writer.write()
