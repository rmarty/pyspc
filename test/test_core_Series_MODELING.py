#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Series in pyspc.core.series

To run all tests just type:
    python -m unittest test_core_Series_MODELING

To run only a class test:
    python -m unittest test_core_Series_MODELING.TestSeries

To run only a specific test:
    python -m unittest test_core_Series_MODELING.TestSeries.test_oudin

"""
# Imports
from datetime import datetime as dt, timedelta as td
import numpy as np
import pandas as pnd
import unittest
from unittest import mock
import warnings

# Imports pyspc
from pyspc.core.serie import Serie
from pyspc.core.series import Series


class TestSeries(unittest.TestCase):
    """
    Series class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        # =====================================================================
        self.latitude1 = 44.734
        provider1 = 'SPC'
        varname1 = 'TH'
        code1 = 'K0000000'
        serie1 = pnd.DataFrame(
            {(code1, varname1): [
                17.600, 17.300, 16.500, 15.100, 15.500, 15.600, 17.500,
                20.500, 21.700, 22.700, 24.600, 25.400, 25.500, 24.500,
                24.300, 25.400, 24.300, 23.800, 23.000, 20.800, 19.800,
                19.100, 18.600, 17.600, 17.400, 16.500, 16.400, 16.000,
                15.800, 14.900, 16.900, 19.300, 19.400, 22.500, 23.200,
                24.400, 25.200]},
            index=pnd.date_range(
                dt(2018, 7, 31), dt(2018, 8, 1, 12), freq=td(hours=1)
            )
        )
        serie1.columns = [(code1, varname1)]
        self.serie1 = Serie(
            datval=serie1, code=code1, provider=provider1, varname=varname1,
        )
        # =====================================================================
        self.series = Series(datatype='obs')
        self.series.add(serie=self.serie1)
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.etp_oudin')
    def test_oudin(self, mock_method, mock_add):
        """
        Test méthode etp_oudin
        """
        # =====================================================================
        self.series.etp_oudin(latitude=self.latitude1)
        mock_method.assert_called_with(
            latitude=self.latitude1, timestep=None)
        mock_method.reset_mock()
        # =====================================================================
        self.series.etp_oudin(latitude=self.latitude1, timestep=td(days=1))
        mock_method.assert_called_with(
            latitude=self.latitude1, timestep=td(days=1))
        mock_method.reset_mock()
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.series.Series.extend')
    @mock.patch('pyspc.core.serie.Serie.apply_RatingCurves')
    def test_ratingcurves(self, mock_method, mock_ext, mock_add):
        """
        Test méthode apply_RatingCurves
        """
        # =====================================================================
        curves = mock.MagicMock(name='C')
        self.series.apply_RatingCurves(curves=curves)
        mock_method.assert_called_with(
            curves=curves,
            tocode=None,
            extrapolation=False
        )
        mock_method.reset_mock()
        # =====================================================================
        self.series.apply_RatingCurves(curves=curves, tocodes='T')
        mock_method.assert_called_with(
            curves=curves,
            tocode='T',
            extrapolation=False
        )
        mock_method.reset_mock()
        # =====================================================================
        self.series.apply_RatingCurves(
            curves=curves, tocodes='T', extrapolation='E')
        mock_method.assert_called_with(
            curves=curves,
            tocode='T',
            extrapolation='E'
        )
        mock_method.reset_mock()
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.series.Series.extend')
    @mock.patch('pyspc.core.serie.Serie.apply_Reservoir')
    def test_reservoir(self, mock_method, mock_ext, mock_add):
        """
        Test méthode apply_Reservoir
        """
        # =====================================================================
        self.series.apply_Reservoir(reservoir='R', tablename='T')
        mock_method.assert_called_with(
            reservoir='R',
            tablename='T',
            assoc=None,
            sim=False
        )
        mock_method.reset_mock()
        # =====================================================================
        self.series.apply_Reservoir(reservoir='R', tablename='T', assoc='A')
        mock_method.assert_called_with(
            reservoir='R',
            tablename='T',
            assoc='A',
            sim=False
        )
        mock_method.reset_mock()
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.series.Series.extend')
    @mock.patch('pyspc.core.serie.Serie.apply_ReservoirTable')
    def test_reservoirtable(self, mock_method, mock_ext, mock_add):
        """
        Test méthode apply_ReservoirTable
        """
        # =====================================================================
        self.series.apply_ReservoirTable(table='T')
        mock_method.assert_called_with(
            table='T',
            assoc=None,
            sim=False
        )
        mock_method.reset_mock()
        # =====================================================================
        self.series.apply_ReservoirTable(table='T', assoc='A')
        mock_method.assert_called_with(
            table='T',
            assoc='A',
            sim=False
        )
        mock_method.reset_mock()
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.series.Series.extend')
    @mock.patch('pyspc.core.serie.Serie.apply_ReservoirZ0')
    def test_reservoirz0(self, mock_method, mock_ext, mock_add):
        """
        Test méthode apply_ReservoirZ0
        """
        # =====================================================================
        self.series.apply_ReservoirZ0(reservoir='R')
        mock_method.assert_called_with(
            reservoir='R'
        )
        # =====================================================================

#    def test_sauquet(self):
#        """
#        test Sauquet
#        """
#        # ====================================================================
#        random = Random('pyspc')
#        dates = pnd.date_range(dt(2000, 1, 1), dt(2023, 7, 1), freq='D')
#        means = [1.430, 1.540, 1.700, 1.670, 1.320, 0.829,
#                 0.380, 0.241, 0.324, 0.613, 1.140, 1.440]
#        df = pnd.DataFrame(
#            {'sigma': [1] * len(dates),
#             'mu': [10 * means[d.month-1] for d in dates]},
#            index=dates
#        )
#        df['normalvariate'] = df.apply(
#            lambda row: random.normalvariate(row['mu'], row['sigma']), axis=1)
#        serie = Serie(df['normalvariate'], code='MASTATION', varname='QJ')
#        series = Series(datatype='obs')
#        series.add(serie=serie)
#        regimes = series.regime_sauquet()
#        # ====================================================================

    @mock.patch('pyspc.core.serie.Serie.regime_sauquet')
    def test_sauquet(self, mock_method):
        """
        test Sauquet
        """
        self.series.regime_sauquet()
        mock_method.assert_called_with()
        mock_method.reset_mock()

    @mock.patch('pyspc.core.series.Series.extend')
    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.sim2fcst')
    def test_sim2fcst(self, mock_method, mock_add, mock_extend):
        """
        Test méthode sim2fcst
        """
        # =====================================================================
        ref = 'ref'
        first_dtime = 'first_dtime'
        last_dtime = 'last_dtime'
        error_depth = 'error_depth'
        max_ltime = 'max_ltime'
        self.series.sim2fcst(
            ref=ref, first_dtime=first_dtime, last_dtime=last_dtime,
            error_depth=error_depth, max_ltime=max_ltime)
        mock_method.assert_called_with(
            ref=ref, first_dtime=first_dtime, last_dtime=last_dtime,
            error_depth=error_depth, max_ltime=max_ltime)
        mock_method.reset_mock()
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.socose')
    def test_socose(self, mock_method, mock_add):
        """
        Test méthode socose
        """
        # ====================================================================
        self.series.socose()
        mock_method.assert_called_with()
        # ====================================================================

    def test_socose2(self):
        """
        Test méthode socose
        """
        # ====================================================================
        code = 'K0000000'
        varname = 'QH'
        df_2024 = pnd.DataFrame(
            [23.0, 25.5, 30.6, 53.6, 92.5, 119.4, 132.4, 137.1, 134.9, 129.6,
             121.2, 111.7, 102.1, 93.7, 84.6, 76.6, 69.5, 63.9, 59.1, 55.0,
             51.0, 48.1, 45.8, 43.5, 41.9],
            index=pnd.date_range(
                dt(2024, 3, 30, 6), dt(2024, 3, 31, 6), freq='H'),
            dtype=np.float64
        )
        df_2016 = pnd.DataFrame(
            [48.1, 54.5, 62.4, 70.0, 76.3, 81.8, 86.5, 91.7, 99.8, 109.5,
             118.5, 127.0, 134.0, 140.0, 145.0, 148.0, 149.0, 148.5, 146.0,
             142.0, 137.5, 132.5, 127.5, 122.5, 117.5, 112.5, 108.0, 103.5,
             98.8, 94.4, 90.5, 86.9, 83.3, 80.1, 76.7, 73.5, 70.6, 67.9, 65.1,
             62.9, 60.8, 58.5, 56.5, 55.0, 53.2, 51.3, 49.8, 48.0, 46.4],
            index=pnd.date_range(
                dt(2016, 5, 31, 6), dt(2016, 6, 2, 6), freq='H'),
            dtype=np.float64
        )
        df_2011 = pnd.DataFrame(
            [35.8, 41.6, 51.7, 68.9, 89.6, 106.8, 118.0, 125.0, 128.5, 128.0,
             125.5, 121.5, 116.5, 111.0, 104.0, 96.4, 89.3, 82.1, 75.1, 68.9,
             63.5, 58.9, 55.0, 51.6, 48.8],
            index=pnd.date_range(
                dt(2011, 12, 16, 12), dt(2011, 12, 17, 12), freq='H'),
            dtype=np.float64
        )
        valid = {
            ('K0000000', 'QH', '2011'): {'d': 17, 'rxd': 1.2446584240214233},
            ('K0000000', 'QH', '2016'): {'d': 31, 'rxd': 1.3137834916661926},
            ('K0000000', 'QH', '2024'): {'d': 13, 'rxd': 1.2682701202590196}
        }
        # ====================================================================
        series = Series(datatype='obs')
        for df, evt in zip([df_2011, df_2016, df_2024],
                           ['2011', '2016', '2024']):
            s = Serie(datval=df, code=code, provider='SPC', varname=varname)
            series.add(serie=s, meta=evt)
        values = series.socose()
        self.assertDictEqual(values, valid)
        # ====================================================================
