#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary duplicateGrpRTCfg

To run all tests just type:
    python -m unittest test_bin_duplicateGrpRTCfg

To run only a class test:
    python -m unittest test_bin_duplicateGrpRTCfg.Test_duplicateGrpRTCfg

To run only a specific test:
    python -m unittest test_bin_duplicateGrpRTCfg.Test_duplicateGrpRTCfg.test_update

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import duplicateGrpRTCfg as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_duplicateGrpRTCfg(unittest.TestCase):
    """
    duplicateGrpRTCfg bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'duplicateGrpRTCfg')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_2016 = os.path.join('data', 'model', 'grp16', 'rt')
        cls.dir_2018 = os.path.join('data', 'model', 'grp18', 'rt')
        cls.dir_2020 = os.path.join('data', 'model', 'grp20', 'rt')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_grp16(self):
        """
        Test Mise à jour config GRP Temps Réel - version 2016
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/duplicateGrpRTCfg.py',
            '-I', self.dir_2016,
            '-D', 'Config_Prevision.txt',
            '-O', self.dir_out,
            '-t', 'grp16',
            '-c', 'Config_Prevision_2016.txt',
            '-U', 'MODFON', 'Temps_diff',
            '-U', 'INSTPR', '"2016-06-01 06:00:00"'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.duplicateGrpRTCfg()
        self.assertEqual(args.cfg_filename, 'Config_Prevision_2016.txt')
        self.assertEqual(args.duplicata_src, 'Config_Prevision.txt')
        self.assertEqual(args.data_type, 'grp16')
        self.assertEqual(args.input_dir, self.dir_2016)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
        self.assertEqual(args.update_cfg,
                         [['MODFON', 'Temps_diff'],
                          ['INSTPR', '"2016-06-01 06:00:00"']])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, args.cfg_filename),
            os.path.join(self.dir_ctl, args.cfg_filename),
        ))
        os.remove(os.path.join(self.dir_out, args.cfg_filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Dupliquer la configuration de référence de GRP v2016 "\
            "('{t}') du fichier {D} dans le répertoire {I} "\
            "vers le fichier {c} dans le répertoire {O} "\
            "pour la balise {u1k} valant {u1v}, "\
            "pour la balise {u2k} valant {u2v}."\
            "".format(
                t=args.data_type,
                I=args.input_dir, D=args.duplicata_src,
                O=args.output_dir, c=args.cfg_filename,
                u1k=args.update_cfg[0][0], u1v=args.update_cfg[0][1],
                u2k=args.update_cfg[1][0], u2v=args.update_cfg[1][1]
                )
        # =====================================================================

    def test_grp18(self):
        """
        Test Mise à jour config GRP Temps Réel - version 2018
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/duplicateGrpRTCfg.py',
            '-I', self.dir_2018,
            '-D', 'Config_Prevision.txt',
            '-O', self.dir_out,
            '-t', 'grp18',
            '-c', 'Config_Prevision_2018.txt',
            '-U', 'MODFON', 'Temps_diff',
            '-U', 'SIMULA', '1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.duplicateGrpRTCfg()
        self.assertEqual(args.cfg_filename, 'Config_Prevision_2018.txt')
        self.assertEqual(args.duplicata_src, 'Config_Prevision.txt')
        self.assertEqual(args.data_type, 'grp18')
        self.assertEqual(args.input_dir, self.dir_2018)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
        self.assertEqual(args.update_cfg,
                         [['MODFON', 'Temps_diff'], ['SIMULA', '1']])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, args.cfg_filename),
            os.path.join(self.dir_ctl, args.cfg_filename),
        ))
        os.remove(os.path.join(self.dir_out, args.cfg_filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Dupliquer la configuration de référence de GRP v2016 "\
            "('{t}') du fichier {D} dans le répertoire {I} "\
            "vers le fichier {c} dans le répertoire {O} "\
            "pour la balise {u1k} valant {u1v}, "\
            "pour la balise {u2k} valant {u2v}."\
            "".format(
                t=args.data_type,
                I=args.input_dir, D=args.duplicata_src,
                O=args.output_dir, c=args.cfg_filename,
                u1k=args.update_cfg[0][0], u1v=args.update_cfg[0][1],
                u2k=args.update_cfg[1][0], u2v=args.update_cfg[1][1]
                )
        # =====================================================================

    def test_grp20(self):
        """
        Test Mise à jour config GRP Temps Réel - version 2020
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/duplicateGrpRTCfg.py',
            '-I', self.dir_2020,
            '-D', 'config_prevision.ini',
            '-O', self.dir_out,
            '-t', 'grp20',
            '-c', 'config_prevision_2020.ini',
            '-U', 'GENERAL_MODFON', 'Temps_reel',
            '-U', 'SORTIES_SIMULA', 'NON'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.duplicateGrpRTCfg()
        self.assertEqual(args.cfg_filename, 'config_prevision_2020.ini')
        self.assertEqual(args.duplicata_src, 'config_prevision.ini')
        self.assertEqual(args.data_type, 'grp20')
        self.assertEqual(args.input_dir, self.dir_2020)
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
        self.assertEqual(
            args.update_cfg,
            [['GENERAL_MODFON', 'Temps_reel'], ['SORTIES_SIMULA', 'NON']])
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, args.cfg_filename),
            os.path.join(self.dir_ctl, args.cfg_filename),
        ))
        os.remove(os.path.join(self.dir_out, args.cfg_filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Dupliquer la configuration de référence de GRP v2016 "\
            "('{t}') du fichier {D} dans le répertoire {I} "\
            "vers le fichier {c} dans le répertoire {O} "\
            "pour la balise {u1k} valant {u1v}, "\
            "pour la balise {u2k} valant {u2v}."\
            "".format(
                t=args.data_type,
                I=args.input_dir, D=args.duplicata_src,
                O=args.output_dir, c=args.cfg_filename,
                u1k=args.update_cfg[0][0], u1v=args.update_cfg[0][1],
                u2k=args.update_cfg[1][0], u2v=args.update_cfg[1][1]
                )
        # =====================================================================
