#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Series in pyspc.core.series

To run all tests just type:
    python -m unittest test_core_Series_SCALING

To run only a class test:
    python -m unittest test_core_Series_SCALING.TestSeries

To run only a specific test:
    python -m unittest test_core_Series_SCALING.TestSeries.test_upscale

"""
# Imports
from datetime import datetime as dt, timedelta as td
import numpy as np
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest
from unittest import mock
import warnings

# Imports pyspc
from pyspc.core.serie import Serie
from pyspc.core.series import Series


class TestSeries(unittest.TestCase):
    """
    Series class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        # =====================================================================
        provider1 = 'SPC'
        varname1 = 'QH'
        code1 = 'K0000000'
        serie1 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500, 86.400, 82.300, 78.300],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie1.columns = [(code1, varname1)]
        self.serie1 = Serie(
            datval=serie1, code=code1, provider=provider1, varname=varname1,
        )
        # =====================================================================
        provider2 = 'SPC'
        varname2 = 'QH'
        code2 = 'K9999999'
        serie2 = pnd.DataFrame(
            [10, 12, 14, 17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie2.columns = [(code2, varname2)]
        self.serie2 = Serie(
            datval=serie2, code=code2, provider=provider2, varname=varname2
        )
        # =====================================================================
        provider3 = 'SPC'
        varname3 = 'QH'
        code3 = 'K3333333'
        serie3 = pnd.DataFrame(
            [17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 3), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie3.columns = [(code3, varname3)]
        self.serie3 = Serie(
            datval=serie3, code=code3, provider=provider3, varname=varname3
        )
        # =====================================================================
        provider4 = 'SPC'
        varname4 = 'QH'
        code4 = 'K4444444'
        serie4 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 9), freq=td(hours=1)
            )
        )
        serie4.columns = [(code4, varname4)]
        self.serie4 = Serie(
            datval=serie4, code=code4, provider=provider4, varname=varname4,
        )
        # =====================================================================
        self.series = Series(datatype='obs')
        self.series.add(serie=self.serie1)
        self.series.add(serie=self.serie2)
        self.series.add(serie=self.serie3)
        self.series.add(serie=self.serie4)
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.downscale')
    def test_downscale(self, mock_method, mock_add):
        """
        Test méthode downscale
        """
        # =====================================================================
        toparam = 'PH'
        # =====================================================================
        self.series.downscale(toparam=toparam)
        mock_method.assert_called_with(toparam=toparam, dayhour=6)
        mock_method.reset_mock()
        # =====================================================================
        self.series.downscale(toparam=toparam, dayhour=0)
        mock_method.assert_called_with(toparam=toparam, dayhour=0)
        mock_method.reset_mock()
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.nearlyequalscale')
    def test_nearlyequalscale(self, mock_method, mock_add):
        """
        Test méthode nearlyequalscale
        """
        # =====================================================================
        toparam = 'P5M'
        # =====================================================================
        self.series.nearlyequalscale(toparam=toparam)
        mock_method.assert_called_with(toparam=toparam, strict=True)
        mock_method.reset_mock()
        # =====================================================================
        self.series.nearlyequalscale(toparam=toparam, strict=True)
        mock_method.assert_called_with(toparam=toparam, strict=True)
        mock_method.reset_mock()
        # =====================================================================
        self.series.nearlyequalscale(toparam=toparam, strict=False)
        mock_method.assert_called_with(toparam=toparam, strict=False)
        mock_method.reset_mock()
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.regularscale')
    def test_regularscale(self, mock_method, mock_add):
        """
        Test méthode regularscale
        """
        # =====================================================================
        self.series.regularscale()
        mock_method.assert_called_with()
        # =====================================================================

    @mock.patch('pandas.concat')
    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.standardize')
    def test_standardize(self, mock_method, mock_add, mock_pnd_concat):
        """
        Test méthode standardize
        """
        # ====================================================================
        self.series.standardize()
        mock_method.assert_called_with()
        # ====================================================================

    def test_standardize2(self):
        """
        Test méthode standardize
        """
        # ====================================================================
        code = 'K0000000'
        varname = 'QH'
        df_2024 = pnd.DataFrame(
            [23.0, 25.5, 30.6, 53.6, 92.5, 119.4, 132.4, 137.1, 134.9, 129.6,
             121.2, 111.7, 102.1, 93.7, 84.6, 76.6, 69.5, 63.9, 59.1, 55.0,
             51.0, 48.1, 45.8, 43.5, 41.9],
            index=pnd.date_range(
                dt(2024, 3, 30, 6), dt(2024, 3, 31, 6), freq='H'),
            dtype=np.float64
        )
        df_2016 = pnd.DataFrame(
            [48.1, 54.5, 62.4, 70.0, 76.3, 81.8, 86.5, 91.7, 99.8, 109.5,
             118.5, 127.0, 134.0, 140.0, 145.0, 148.0, 149.0, 148.5, 146.0,
             142.0, 137.5, 132.5, 127.5, 122.5, 117.5, 112.5, 108.0, 103.5,
             98.8, 94.4, 90.5, 86.9, 83.3, 80.1, 76.7, 73.5, 70.6, 67.9, 65.1,
             62.9, 60.8, 58.5, 56.5, 55.0, 53.2, 51.3, 49.8, 48.0, 46.4],
            index=pnd.date_range(
                dt(2016, 5, 31, 6), dt(2016, 6, 2, 6), freq='H'),
            dtype=np.float64
        )
        df_2011 = pnd.DataFrame(
            [35.8, 41.6, 51.7, 68.9, 89.6, 106.8, 118.0, 125.0, 128.5, 128.0,
             125.5, 121.5, 116.5, 111.0, 104.0, 96.4, 89.3, 82.1, 75.1, 68.9,
             63.5, 58.9, 55.0, 51.6, 48.8],
            index=pnd.date_range(
                dt(2011, 12, 16, 12), dt(2011, 12, 17, 12), freq='H'),
            dtype=np.float64
        )
        # ====================================================================
        valid = pnd.DataFrame(
            {(code, varname, '2011'): [
                np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan,
                0.278599, 0.323735, 0.402335, 0.536187, 0.697276, 0.831128,
                0.918288, 0.972763, 1.000000, 0.996109, 0.976654, 0.945525,
                0.906615, 0.863813, 0.809339, 0.750195, 0.694942, 0.638911,
                0.584436, 0.536187, 0.494163, 0.458366, 0.428016, 0.401556,
                0.379767, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan,
                np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan,
                np.nan, np.nan],
             (code, varname, '2016'): [
                 0.322819, 0.365772, 0.418792, 0.469799, 0.512081, 0.548993,
                 0.580537, 0.615436, 0.669799, 0.734899, 0.795302, 0.852349,
                 0.899329, 0.939597, 0.973154, 0.993289, 1.000000, 0.996644,
                 0.979866, 0.953020, 0.922819, 0.889262, 0.855705, 0.822148,
                 0.788591, 0.755034, 0.724832, 0.694631, 0.663087, 0.633557,
                 0.607383, 0.583221, 0.559060, 0.537584, 0.514765, 0.493289,
                 0.473826, 0.455705, 0.436913, 0.422148, 0.408054, 0.392617,
                 0.379195, 0.369128, 0.357047, 0.344295, 0.334228, 0.322148,
                 0.311409],
             (code, varname, '2024'): [
                 np.nan, np.nan, np.nan, np.nan, np.nan,
                 np.nan, np.nan, np.nan, np.nan, 0.167761, 0.185996, 0.223195,
                 0.390956, 0.674690, 0.870897, 0.965718, 1.000000, 0.983953,
                 0.945295, 0.884026, 0.814734, 0.744712, 0.683443, 0.617068,
                 0.558716, 0.506929, 0.466083, 0.431072, 0.401167, 0.371991,
                 0.350839, 0.334063, 0.317287, 0.305616, np.nan, np.nan,
                 np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan,
                 np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
             },
            index=[td(hours=x) for x in range(-16, 33, 1)]
        )
        # ====================================================================
        series = Series(datatype='obs')
        for df, evt in zip([df_2011, df_2016, df_2024],
                           ['2011', '2016', '2024']):
            s = Serie(datval=df, code=code, provider='SPC', varname=varname)
            series.add(serie=s, meta=evt)
        df = series.standardize()
        assert_frame_equal(df, valid)
        # ====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.subhourlyscale')
    def test_subhourlyscale(self, mock_method, mock_add):
        """
        Test méthode subhourlyscale
        """
        # =====================================================================
        ts = 1
        how = 'interp'
        howinterp = 'linear'
        self.series.subhourlyscale(ts=ts, how=how, howinterp=howinterp)
        mock_method.assert_called_with(ts=ts, how=how, howinterp=howinterp)
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.upscale')
    def test_upscale(self, mock_method, mock_add):
        """
        Test méthode upscale
        """
        # =====================================================================
        toparam = 'QJ'
        # =====================================================================
        self.series.upscale(toparam=toparam)
        mock_method.assert_called_with(
            toparam=toparam, dayhour=6, strict=True)
        mock_method.reset_mock()
        # =====================================================================
        self.series.upscale(toparam=toparam, dayhour=0)
        mock_method.assert_called_with(
            toparam=toparam, dayhour=0, strict=True)
        mock_method.reset_mock()
        # =====================================================================
        self.series.upscale(toparam=toparam, strict=False)
        mock_method.assert_called_with(
            toparam=toparam, dayhour=6, strict=False)
        mock_method.reset_mock()
        # =====================================================================
