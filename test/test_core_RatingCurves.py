#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for RatingCurves in pyspc.core.ratingcurve

To run all tests just type:
    python -m unittest test_core_RatingCurves

To run only a class test:
    python -m unittest test_core_RatingCurves.TestRatingCurves

To run only a specific test:
    python -m unittest test_core_RatingCurves.TestRatingCurves.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import unittest

# Imports pyspc
from pyspc.core.ratingcurve import RatingCurve, RatingCurves


class TestRatingCurves(unittest.TestCase):
    """
    RatingCurves class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.curve_K01 = RatingCurve(
            code='K0000000', num='K01', provider='foo',
            valid_dt=(dt(2020, 1, 1), dt(2021, 1, 1) - td(seconds=1)),
            valid_interval=(0, 2), hq=[(0, 0), (1, 100), (2, 200)]
        )
        self.curve_K02 = RatingCurve(
            code='K0000000', num='K02', provider='foo',
            valid_dt=(dt(2021, 1, 1), dt(2022, 1, 1)),
            valid_interval=(0, 2), hq=[(0, 0), (1, 100), (2, 2000)]
        )
        self.curve_K03 = RatingCurve(
            code='K0000000', num='K03', provider='bar',
            valid_dt=(dt(2021, 1, 1), dt(2030, 1, 1)),
            valid_interval=(0, 2), hq=[(0, 0), (1, 1000), (2, 2000)]
        )
        self.curve_K99 = RatingCurve(
            code='K9999999', num='K99', provider='foo',
            valid_dt=(dt(2020, 1, 1), dt(2021, 1, 1)), valid_interval=(0, 2),
            hq=[(0, 0), (2, 200)]
        )

    def test_init(self):
        """
        Test de la création de l'instance
        """
        name = 'test_init'
        curves = RatingCurves(name=name)
        self.assertEqual(curves.name, name)
        self.assertEqual(curves.codes, [])
        self.assertEqual(curves.nums, [])
        self.assertEqual(curves.providers, [])
        self.assertEqual(curves._timeintervals, {})

    def test_add(self):
        """
        Test de la méthode add
        """
        # =====================================================================
        curves = RatingCurves()
        curves.add(self.curve_K01)
        curves.add(self.curve_K02)
        curves.add(self.curve_K03)
        curves.add(self.curve_K99)
        self.assertEqual(len(curves), 4)
        self.assertEqual(
            curves.codes, ['K0000000', 'K0000000', 'K0000000', 'K9999999'])
        self.assertEqual(curves.nums, ['K01', 'K02', 'K03', 'K99'])
        self.assertEqual(curves.providers, ['foo', 'foo', 'bar', 'foo'])
        # =====================================================================

    def test_extend(self):
        """
        Test de la méthode extend
        """
        # =====================================================================
        other = RatingCurves()
        other.add(self.curve_K01)
        other.add(self.curve_K02)
        other.add(self.curve_K03)
        # =====================================================================
        curves = RatingCurves()
        curves.add(self.curve_K99)
        self.assertEqual(len(curves), 1)
        self.assertEqual(curves.codes, ['K9999999'])
        self.assertEqual(curves.nums, ['K99'])
        self.assertEqual(curves.providers, ['foo'])
        # =====================================================================
        curves.extend(other)
        self.assertEqual(len(curves), 4)
        self.assertEqual(
            curves.codes, ['K9999999', 'K0000000', 'K0000000', 'K0000000'])
        self.assertEqual(curves.nums, ['K99', 'K01', 'K02', 'K03'])
        self.assertEqual(curves.providers, ['foo', 'foo', 'foo', 'bar'])
        # =====================================================================

    def test_select(self):
        """
        Test de la méthode select
        """
        # =====================================================================
        curves = RatingCurves()
        curves.add(self.curve_K01)
        curves.add(self.curve_K02)
        curves.add(self.curve_K03)
        curves.add(self.curve_K99)
        # =====================================================================
        select = curves.select(codes=['K000'])
        self.assertIsInstance(select, RatingCurves)
        self.assertEqual(len(select), 3)
        self.assertEqual(select.codes, ['K0000000', 'K0000000', 'K0000000'])
        self.assertEqual(select.nums, ['K01', 'K02', 'K03'])
        self.assertEqual(select.providers, ['foo', 'foo', 'bar'])
        # =====================================================================
        select = curves.select(codes=['K000'], providers=['foo'])
        self.assertIsInstance(select, RatingCurves)
        self.assertEqual(len(select), 2)
        self.assertEqual(select.codes, ['K0000000', 'K0000000'])
        self.assertEqual(select.nums, ['K01', 'K02'])
        self.assertEqual(select.providers, ['foo', 'foo'])
        # =====================================================================
        select = curves.select(codes=['K000'], nums=['K01', 'K02'])
        self.assertIsInstance(select, RatingCurves)
        self.assertEqual(len(select), 2)
        self.assertEqual(select.codes, ['K0000000', 'K0000000'])
        self.assertEqual(select.nums, ['K01', 'K02'])
        self.assertEqual(select.providers, ['foo', 'foo'])
        # =====================================================================
        select = curves.select(start=dt(2021, 1, 1), end=dt(2050, 1, 1))
        self.assertIsInstance(select, RatingCurves)
        self.assertEqual(len(select), 3)
        self.assertEqual(select.codes, ['K0000000', 'K0000000', 'K9999999'])
        self.assertEqual(select.nums, ['K02', 'K03', 'K99'])
        self.assertEqual(select.providers, ['foo', 'bar', 'foo'])
        # =====================================================================
        select = curves.select(codes=['K000'], nums=['K01', 'K02'],
                               providers=['foo'],
                               start=dt(2021, 1, 1), end=dt(2050, 1, 1))
        self.assertIsInstance(select, RatingCurves)
        self.assertEqual(len(select), 1)
        self.assertEqual(select.codes, ['K0000000'])
        self.assertEqual(select.nums, ['K02'])
        self.assertEqual(select.providers, ['foo'])
        # =====================================================================

    def test_check_overlapping(self):
        """
        Test de la méthode check_overlapping
        """
        # =====================================================================
        curves = RatingCurves()
        curves.add(self.curve_K01)
        curves.add(self.curve_K02)
        curves.add(self.curve_K03)
        curves.add(self.curve_K99)
        with self.assertRaises(ValueError):
            curves.check_overlapping(code='K0000000')
        try:
            curves.check_overlapping(code='K9999999')
        except ValueError:
            self.fail("Erreur dans le test de check_overlapping")
        # =====================================================================
        curves = RatingCurves()
        curves.add(self.curve_K02)
        curves.add(self.curve_K03)
        with self.assertRaises(ValueError):
            curves.check_overlapping(code='K0000000')
        # =====================================================================
        curves = RatingCurves()
        curves.add(self.curve_K01)
        curves.add(self.curve_K02)
        try:
            curves.check_overlapping(code='K0000000')
        except ValueError:
            self.fail("Erreur dans le test de check_overlapping")
        # =====================================================================
