#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Fcst in pyspc.model.grp16

To run all tests just type:
    python -m unittest test_model_GRP16_RT_Fcst

To run only a class test:
    python -m unittest test_model_GRP16_RT_Fcst.TestGRPRT_Fcst

To run only a specific test:
    python -m unittest test_model_GRP16_RT_Fcst.TestGRPRT_Fcst.test_init

"""
# Imports
from datetime import datetime as dt
import filecmp
import numpy as np
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.model.grp16 import GRPRT_Fcst


class TestGRPRT_Fcst(unittest.TestCase):
    """
    GRPRT_Fcst class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp16', 'rt')
        self.valid_obs = pnd.DataFrame({
            'TYP': ['OBS']*9,
            '      CODE': ['  K6173130']*9,
            '    DATE(TU)': pnd.date_range(dt(2016, 5, 31, 8),
                                           dt(2016, 5, 31, 16),
                                           freq='H'),
            '    DEBIT(m3/s)': [27.1528, 28.1086, 28.7168, 29.1512, 29.5422,
                                29.9549, 30.4328, 30.9976, 31.6710],
            '      PLUIE(mm)': [2.4533, 2.3867, 1.4400, 0.8067, 1.2000,
                                0.7067, 0.4800, 0.9067, 0.6533],
            'Temperature(°C)': [np.nan]*9,

        })
        self.valid_sim = pnd.DataFrame({
            'TYP': ['SIM']*20,
            '      CODE': ['  K6173130']*20,
            '    DATE(TU)': pnd.date_range(dt(2016, 5, 31, 17),
                                           dt(2016, 6, 1, 12),
                                           freq='H'),
            '    DEBIT(m3/s)': [25.4160, 26.1100, 26.8240, 27.5560, 28.3040,
                                29.0670, 29.8400, 30.6250, 31.4170, 32.2180,
                                33.0240, 33.8310, 34.6360, 35.4420, 36.2440,
                                37.0340, 37.8010, 38.5450, 39.2640, 39.9670],
            '      PLUIE(mm)': [1.0533, 0.8800, 0.7067, 0.4133, 0.3467, 0.3067,
                                0.1733, 0.3200, 0.2933, 0.3733, 0.5733, 0.2800,
                                0.3867, 0.7800, 0.4667, 0.0800, 0.1333, 0.1600,
                                0.0800, 0.1333],
            'Temperature(°C)': [np.nan]*20,

        })
        self.valid_fcst = pnd.DataFrame({
            'TYP': ['PRV']*20,
            '      CODE': ['  K6173130']*20,
            '    DATE(TU)': pnd.date_range(dt(2016, 5, 31, 17),
                                           dt(2016, 6, 1, 12),
                                           freq='H'),
            '    DEBIT(m3/s)': [32.2280, 32.8900, 33.5890, 34.3120, 35.0520,
                                35.8070, 36.5740, 37.3490, 38.1320, 38.9200,
                                39.7120, 40.5020, 41.2890, 42.0730, 42.8510,
                                43.6130, 44.3490, 45.0580, 45.7390, 46.4000],
            '      PLUIE(mm)': [1.0533, 0.8800, 0.7067, 0.4133, 0.3467, 0.3067,
                                0.1733, 0.3200, 0.2933, 0.3733, 0.5733, 0.2800,
                                0.3867, 0.7800, 0.4667, 0.0800, 0.1333, 0.1600,
                                0.0800, 0.1333],
            'Temperature(°C)': [np.nan]*20,

        })

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = 'GRP_Obs.txt'
        datatype = "obs"
        reader = GRPRT_Fcst(filename=filename, datatype=datatype)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.datatype, datatype)

    def test_init_error(self):
        """
        Test de la création de l'instance - Cas avec erreurs
        """
        filename = 'GRP_Obs.txt'
        with self.assertRaises(ValueError):
            GRPRT_Fcst(filename=filename)
        with self.assertRaises(ValueError):
            GRPRT_Fcst(filename=filename, datatype='toto')

    def test_read_obs_diff(self):
        """
        Test de lecture d'un fichier de prévision GRP RT - OBS DIFF
        """
        filename = os.path.join(self.dirname, 'GRP_D_Obs.txt')
        datatype = "obs_diff"
        reader = GRPRT_Fcst(filename=filename, datatype=datatype)
        df = reader.read()
        assert_frame_equal(df, self.valid_obs)

    def test_read_sim_diff(self):
        """
        Test de lecture d'un fichier de prévision GRP RT - SIM DIFF
        """
        filename = os.path.join(self.dirname, 'GRP_D_Simu_2001.txt')
        datatype = "sim_diff"
        reader = GRPRT_Fcst(filename=filename, datatype=datatype)
        df = reader.read()
        assert_frame_equal(df, self.valid_sim)

    def test_read_fcst_diff(self):
        """
        Test de lecture d'un fichier de prévision GRP RT - FCST DIFF
        """
        filename = os.path.join(self.dirname, 'GRP_D_Prev_2001.txt')
        datatype = "fcst_diff"
        reader = GRPRT_Fcst(filename=filename, datatype=datatype)
        df = reader.read()
        assert_frame_equal(df, self.valid_fcst)

    def test_write_obs(self):
        """
        Test d'écriture d'un fichier de prévision GRP Temps-Réel - OBS
        """
        datatype = "obs_diff"
        filename = os.path.join(self.dirname, 'GRP_D_Obs.txt')
        tmpfile = os.path.join('data', 'GRP_D_Obs.txt')
        writer = GRPRT_Fcst(filename=tmpfile, datatype=datatype)
        writer.write(data=self.valid_obs)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)

    def test_write_sim(self):
        """
        Test d'écriture d'un fichier de prévision GRP Temps-Réel - SIM
        """
        datatype = "sim_diff"
        filename = os.path.join(self.dirname, 'GRP_D_Simu_2001.txt')
        tmpfile = os.path.join('data', 'GRP_D_Simu_2001.txt')
        writer = GRPRT_Fcst(filename=tmpfile, datatype=datatype)
        writer.write(data=self.valid_sim)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)

    def test_write_fcst(self):
        """
        Test d'écriture d'un fichier de prévision GRP Temps-Réel - FCST
        """
        datatype = "fcst_diff"
        filename = os.path.join(self.dirname, 'GRP_D_Prev_2001.txt')
        tmpfile = os.path.join('data', 'GRP_D_Prev_2001.txt')
        writer = GRPRT_Fcst(filename=tmpfile, datatype=datatype)
        writer.write(data=self.valid_fcst)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)

    def test_fileprefix(self):
        """
        Test des préfixes des fichiers GRPRT_Fcst
        """
        valids = {
            "obs": 'GRP_Obs',
            "obs_diff": 'GRP_D_Obs',
            "sim": 'GRP_Simu_',
            "sim_diff": 'GRP_D_Simu_',
            "fcst": 'GRP_Prev_',
            "fcst_diff": 'GRP_D_Prev_'
        }
        for k, v in valids.items():
            self.assertEqual(GRPRT_Fcst.get_fileprefix(datatype=k), v)

    def test_lineprefix(self):
        """
        Test des préfixes des lignes données GRPRT_Fcst
        """
        valids = {
            "obs": 'OBS',
            "obs_diff": 'OBS',
            "sim": 'SIM',
            "sim_diff": 'SIM',
            "fcst": 'PRV',
            "fcst_diff": 'PRV'
        }
        for k, v in valids.items():
            self.assertEqual(GRPRT_Fcst.get_lineprefix(datatype=k), v)

    def test_datatype(self):
        """
        Test des types de données GRPRT_Fcst
        """
        self.assertEqual(
            ['fcst', 'fcst_diff', 'obs', 'obs_diff', 'sim', 'sim_diff'],
            GRPRT_Fcst.get_types()
        )
