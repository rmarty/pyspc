#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Serie in pyspc.core.serie - plotting

To run all tests just type:
    python -m unittest test_core_Serie_PLOTTING

To run only a class test:
    python -m unittest test_core_Serie_PLOTTING.TestSerie

To run only a specific test:
    python -m unittest test_core_Serie_PLOTTING.TestSerie.test_plot

"""
# Imports
from datetime import datetime as dt, timedelta as td
import filecmp
import numpy as np
import os.path
import pandas as pnd
import unittest
import warnings

# Imports pyspc
from pyspc.core.serie import Serie


class TestSerie(unittest.TestCase):
    """
    Serie class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        self.code = 'K0000000'
        self.provider = 'SPC'
        self.varname = 'QH'

    def test_plot(self):
        """
        Test méthode plot
        """
        # =====================================================================
        varname = self.varname
        serie = pnd.DataFrame(
            {(self.code, varname): [2.510, 2.700, 2.970, 3.430, 4.810, 8.180,
                                    12.100, 17.000, 19.600, 22.200, 24.800,
                                    29.900, 35.600, 41.200, 46.900, 62.900,
                                    85.700, 106.00, 118.00, 115.00, 103.00,
                                    92.500, 86.400, 82.300, np.nan, np.nan,
                                    np.nan, 69.600, 71.400, 76.000, 82.400,
                                    89.100, 88.900, 84.300, 74.500, 64.500,
                                    55.300, 49.600, 45.100, 42.500, 40.000,
                                    37.900, 36.700, 35.500, 34.300, 33.000,
                                    31.800, 30.600, 29.400, 28.200, 27.700,
                                    27.300, 26.800, 26.300, 25.800, 25.200,
                                    24.700, 24.200, 23.700, 23.200, 22.700,
                                    22.300, 21.800, 21.300, 20.800, 20.300,
                                    19.800, 19.500, 19.100, 18.800, 18.600,
                                    18.400, 18.200]},
            index=pnd.date_range(
                dt(2014, 11, 3, 12),
                dt(2014, 11, 6, 12),
                freq=td(hours=1)
            )
        )
        serie = Serie(
            datval=serie,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        tmpfile = serie.plot(dirname='data')
        filename = os.path.join('data', 'core', 'serie',
                                'K0000000_2014110312_2014110612_QH.png')
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
