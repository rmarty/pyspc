#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Data in pyspc.model.otamin16

To run all tests just type:
    python -m unittest test_model_OTAMIN16_Data

To run only a class test:
    python -m unittest test_model_OTAMIN16_Data.TestOtamin_Data

To run only a specific test:
    python -m unittest test_model_OTAMIN16_Data.TestOtamin_Data.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import filecmp
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.model.otamin16 import Data


class TestOtamin_Data(unittest.TestCase):
    """
    Otamin_Data class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        # =====================================================================
        self.dirname = os.path.join('data', 'model', 'otamin16')
        # =====================================================================
        self.valid = pnd.DataFrame({
            ('K0403010', 'Q', '45gGRPd000', 'OBS'):
                [22.7000, 30.2000, 41.7000, 67.8000, 136.0000, 177.0000,
                 188.0000, 172.0000, 160.0000, 186.0000, 203.0000, 186.0000,
                 174.0000, 164.0000, 145.0000, 121.0000, 97.0000, 75.2000,
                 64.5000],
            ('K0403010', 'Q', '45gGRPd000', 'PREV'):
                [25.2565, 30.3346, 42.1571, 64.2836, 94.1275, 125.9666,
                 149.4239, 159.6839, 163.2515, 164.4469, 163.0889,
                 160.1685, 158.0497, 153.9042, 133.8308, 123.6565,
                 112.3536, 85.3825, 73.2027],
            },
            index=pnd.date_range(
                dt(2008, 11, 1, 18),
                dt(2008, 11, 2, 12),
                freq='H'
            )
        )
        self.valid.index.name = '# JJ-MM-AAAA HH:MM'
        self.valid.columns.names = ['Stations', 'Grandeurs', 'Modeles', None]
        # =====================================================================

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'K0403010_45gGRPd000_012.csv')
        station = 'K0403010'
        model = '45gGRPd000'
        leadtime = 12 * td(hours=1)
        reader = Data(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.station, station)
        self.assertEqual(reader.model, model)
        self.assertEqual(reader.leadtime, leadtime)

    def test_basename(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K0403010_45gGRPd000_012.csv')
        [station, model, leadtime] = Data.split_basename(
            filename=filename)  # pylint: disable=unbalanced-tuple-unpacking
        self.assertEqual(station, 'K0403010')
        self.assertEqual(model, '45gGRPd000')
        self.assertEqual(leadtime, 12 * td(hours=1))
        # =====================================================================
        station = 'K0403010'
        model = '45gGRPd000'
        leadtime = 12 * td(hours=1)
        filename = Data.join_basename(
            station=station, model=model, leadtime=leadtime
        )
        self.assertEqual(filename, 'K0403010_45gGRPd000_012.csv')
        # =====================================================================

    def test_read(self):
        """
        Test de la lecture
        """
        filename = os.path.join(self.dirname, 'K0403010_45gGRPd000_012.csv')
        reader = Data(filename=filename)
        df = reader.read()
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(self.valid, df)

    def test_write(self):
        """
        Test de l'écriture
        """
        df = self.valid
        filename = os.path.join(self.dirname, 'K0403010_45gGRPd000_012.csv')
        tmp_file = os.path.join('data', 'K0403010_45gGRPd000_012.csv')
        writer = Data(filename=tmp_file)
        writer.write(data=df)
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
