#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Cfg in pyspc.model.grp20

To run all tests just type:
    python -m unittest test_model_GRP20_RT_Cfg

To run only a class test:
    python -m unittest test_model_GRP20_RT_Cfg.TestGRPRT_Cfg

To run only a specific test:
    python -m unittest test_model_GRP20_RT_Cfg.TestGRPRT_Cfg.test_init

"""
# Imports
import filecmp
import os
import unittest

from pyspc.model.grp20 import GRPRT_Cfg


class TestGRPRT_Cfg(unittest.TestCase):
    """
    GRPRT_Cfg class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp20', 'rt')
        self.validrun = {
            ('GENERAL', 'MODFON'): 'Temps_diff',
            ('GENERAL', 'INSTPR'): '2007-01-19 00:00:00',
            ('GENERAL', 'CONFIRM'): 'NON',
            ('CHEMINS', 'BDD'): 'D:\\00_BDDTR_Capricieuse\\Temps_Reel\\',
            ('CHEMINS', 'OBS'):
                'D:\\00_BDDTR_Capricieuse\\Temps_Reel\\Entrees\\',
            ('CHEMINS', 'SCE'):
                'D:\\00_BDDTR_Capricieuse\\Temps_Reel\\Entrees\\',
            ('CHEMINS', 'PRV'):
                'D:\\00_BDDTR_Capricieuse\\Temps_Reel\\Sorties\\',
            ('CHEMINS', 'R'): 'C:\\R\\R-3.3.2\\bin\\Rscript.exe',
            ('OBSERVATIONS', 'TYPE'): 'TXT',
            ('SCENARIOS', 'TYPE'): 'TXT',
            ('SCENARIOS', 'REF'): 'TU',
            ('SCENARIOS', 'EXTRA'): 'NON',
            ('SORTIES', 'TYPE'): 'TXT',
            ('SORTIES', 'REF'): 'TU',
            ('SORTIES', 'UNITE'): 'm3/s',
            ('SORTIES', 'CODMPP'): '2010;2011',
            ('SORTIES', 'AFFOBS'): 'NON',
            ('SORTIES', 'SIMULA'): 'OUI',
        }
        self.valid_output = {
            ('SORTIES', 'DEPASS'): 'OUI',
            ('SORTIES', 'PREVI'): 'OUI',
            ('ARCHIVES', 'HISTO_PRV_2HOR'): 'OUI',
            ('ARCHIVES', 'HISTO_PRV'): 'OUI',
            ('DERNIER_EXPORT', 'GRP_EXP'): 'OUI',
            ('FICHES_CONTROLE', 'FCH'): 'OUI',
            ('FICHES_CONTROLE', 'FCC'): 'OUI',
            ('FICHES_CONTROLE', 'FCN'): 'OUI',
            ('FICHES_CONTROLE', 'FCS'): 'OUI',
            ('INCERTITUDES', 'GRP_PRV_INC'): 'OUI',
            ('RAPPORTS', 'LIST_PB'): 'OUI',
        }

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'config_prevision.ini')
        config = GRPRT_Cfg(filename=filename)
        self.assertEqual(config.filename, filename)
        self.assertEqual(config.datatype, 'rt_config_run')
        # =====================================================================
        filename = os.path.join(self.dirname, 'Fichiers_sortie_GRP.ini')
        config = GRPRT_Cfg(filename=filename)
        self.assertEqual(config.filename, filename)
        self.assertEqual(config.datatype, 'rt_config_outputs')
        # =====================================================================

    def test_datatype(self):
        """
        Test du type de fichier de configuration
        """
        # =====================================================================
        valid = {
            'config_prevision': 'rt_config_run',
            'config_prevision_ABCDE': 'rt_config_run',
            'ABCDE_config_prevision': 'rt_config_run',
            'config_prevision123456': 'rt_config_run',
            'Fichiers_sortie_GRP': 'rt_config_outputs',
            'Fichiers_sortie_GRP_ABCDE': 'rt_config_outputs',
            'ABCDE_Fichiers_sortie_GRP': 'rt_config_outputs',
            'Fichiers_sortie_GRP123456': 'rt_config_outputs',
            'toto': None,
            None: None,
            123: None
        }
        for k, v in valid.items():
            self.assertEqual(GRPRT_Cfg.get_datatype(k), v)
        # =====================================================================

    def test_read(self):
        """
        Test de la lecture du fichier de configuration GRP *Temps Reel*
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'config_prevision.ini')
        config = GRPRT_Cfg(filename=filename)
        config.read(encoding='iso-8859-15')
        for k, v in self.validrun.items():
            self.assertEqual(config[k[0]][k[1]], v)
        # =====================================================================
        filename = os.path.join(self.dirname, 'Fichiers_sortie_GRP.ini')
        config = GRPRT_Cfg(filename=filename)
        config.read()
        for k, v in self.valid_output.items():
            self.assertEqual(config[k[0]][k[1]], v)
        # =====================================================================

    def test_update(self):
        """
        Test de la mise à jour de la configuration GRP *Temps Reel*
        """
        # =====================================================================
        old_valids = {
            ('GENERAL', 'MODFON'): 'Temps_diff',
            ('GENERAL', 'INSTPR'): '2007-01-19 00:00:00',
        }
        new_valids = {
            ('GENERAL', 'MODFON'): 'Temps_reel',
            ('GENERAL', 'INSTPR'): '2022-02-23 12:30:30',
        }
        filename = os.path.join(self.dirname, 'config_prevision.ini')
        config = GRPRT_Cfg(filename=filename)
        config.read(encoding='iso-8859-15')
        config.update_config(config=new_valids)
        for k, v in old_valids.items():
            self.assertNotEqual(config[k[0]][k[1]], v)
        for k, v in new_valids.items():
            self.assertEqual(config[k[0]][k[1]], v)
        # =====================================================================

    def test_write(self):
        """
        Test de l'écriture du fichier de configuration GRP *Temps Reel*
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'config_prevision.ini')
        testfile = os.path.join(self.dirname, 'config_prevision_nocomment.ini')
        config = GRPRT_Cfg(filename=filename)
        config.read(encoding='iso-8859-15')
        tmpfile = os.path.join('data', 'config_prevision.ini')
        config.filename = tmpfile
        config.write()
        self.assertTrue(filecmp.cmp(testfile, tmpfile))
        os.remove(tmpfile)
        # =====================================================================
        filename = os.path.join(self.dirname, 'Fichiers_sortie_GRP.ini')
        testfile = os.path.join(self.dirname,
                                'Fichiers_sortie_GRP_nocomment.ini')
        config = GRPRT_Cfg(filename=filename)
        config.read()
        tmpfile = os.path.join('data', 'Fichiers_sortie_GRP.ini')
        config.filename = tmpfile
        config.write()
        self.assertTrue(filecmp.cmp(testfile, tmpfile))
        os.remove(tmpfile)
        # =====================================================================

    def test_keys(self):
        """
        Test de la liste des clés de configuration
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'config_prevision.ini')
        self.assertListEqual(GRPRT_Cfg.get_cfg_keys(filename=filename),
                             list(self.validrun.keys()))
        # =====================================================================
        filename = os.path.join(self.dirname, 'Fichiers_sortie_GRP.ini')
        self.assertListEqual(GRPRT_Cfg.get_cfg_keys(filename=filename),
                             list(self.valid_output.keys()))
        # =====================================================================

    def test_check_keys(self):
        """
        Test de vérification des clés de configuration
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'config_prevision.ini')
        config = GRPRT_Cfg(filename=filename)
        config.read(encoding='iso-8859-15')
        try:
            config.check_cfg_keys()
        except ValueError:
            self.fail("Erreur dans le test de check_cfg_keys")
        # =====================================================================
        filename = os.path.join(self.dirname, 'config_prevision.ini')
        config = GRPRT_Cfg(filename=filename)
        config.read(encoding='iso-8859-15')
        config.update_config(config={('1', '2'): None}, strict=False)
        with self.assertRaises(ValueError):
            config.check_cfg_keys()
        # =====================================================================
        filename = os.path.join(self.dirname, 'config_prevision.ini')
        config = GRPRT_Cfg(filename=filename)
        config.read(encoding='iso-8859-15')
        del config['GENERAL']['MODFON']
        with self.assertRaises(ValueError):
            config.check_cfg_keys()
        # =====================================================================
