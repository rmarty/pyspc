#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary bdapbpjson2csv

To run all tests just type:
    python -m unittest test_bin_bdapbpjson2csv

To run only a class test:
    python -m unittest test_bin_bdapbpjson2csv.Test_bdapbpjson2csv

To run only a specific test:
    python -m unittest test_bin_bdapbpjson2csv.Test_bdapbpjson2csv.test_short

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import bdapbpjson2csv as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_bdapbpjson2csv(unittest.TestCase):
    """
    bdapbpjson2csv bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'bdapbpjson2csv')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dirname2 = os.path.join('data', 'data', 'lamedo')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_short(self):
        """
        Test de la conversion - Format Short
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdapbpjson2csv.py',
            '-I', self.dirname2,
            '-d', 'bp_short.json',
            '-O', self.dir_out,
            '-C', 'pyspc',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.bdapbpjson2csv()
        self.assertEqual(args.json_filename, 'bp_short.json')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.domain_name, None)
        self.assertEqual(args.onefile, False)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for filename in os.listdir(self.dir_out):
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dir_ctl, filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier BdApbp {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, d=args.json_filename,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_long(self):
        """
        Test de la conversion - Format Long
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdapbpjson2csv.py',
            '-I', self.dirname2,
            '-d', 'bp_long.json',
            '-O', self.dir_out,
            '-C', 'pyspc',
            '-S', '41003',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.bdapbpjson2csv()
        self.assertEqual(args.json_filename, 'bp_long.json')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.domain_name, '41003')
        self.assertEqual(args.onefile, True)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for filename in os.listdir(self.dir_out):
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dir_ctl, filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier BdApbp {d} "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Seule la zone {S} est extraite. "\
            "Les données sont exportées dans un seul fichier."\
            "".format(
                I=args.input_dir, d=args.json_filename,
                S=args.domain_name,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================
