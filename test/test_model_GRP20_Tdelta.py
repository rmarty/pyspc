#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for dtfmt, str2td, td2str in pyspc.model.grp20

To run all tests just type:
    python -m unittest test_model_GRP20_Tdelta

To run only a class test:
    python -m unittest test_model_GRP20_Tdelta.TestGRP_Tdelta

To run only a specific test:
    python -m unittest test_model_GRP20_Tdelta.TestGRP_Tdelta.test_init

"""
# Imports
from datetime import timedelta as td
import unittest

from pyspc.model.grp20 import str2td, td2str


class TestGRP_Tdelta(unittest.TestCase):
    """
    GRP_Data class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """

    def test_str2td(self):
        """
        Test str2td
        """
        # =====================================================================
        self.assertEqual(str2td(None), None)
        # =====================================================================
        self.assertEqual(str2td('00J00H15M'), td(minutes=15))
        self.assertEqual(str2td('00J01H00M'), td(hours=1))
        self.assertEqual(str2td('00J01H30M'), td(hours=1, minutes=30))
        self.assertEqual(str2td('01J00H00M'), td(days=1))
        self.assertEqual(str2td('01J12H00M'), td(hours=36))
        # =====================================================================
        with self.assertRaises(ValueError):
            str2td('00J00H00M')
        with self.assertRaises(ValueError):
            str2td('01J')
        with self.assertRaises(ValueError):
            str2td('01H')
        with self.assertRaises(ValueError):
            str2td('30M')
        # =====================================================================

    def test_td2str(self):
        """
        Test td2str
        """
        # =====================================================================
        self.assertEqual(td2str(None), None)
        # =====================================================================
        self.assertEqual(td2str(td(minutes=15)), '00J00H15M')
        self.assertEqual(td2str(td(hours=1)), '00J01H00M')
        self.assertEqual(td2str(td(hours=1, minutes=30)), '00J01H30M')
        self.assertEqual(td2str(td(days=1)), '01J00H00M')
        self.assertEqual(td2str(td(hours=36)), '01J12H00M')
        # =====================================================================
        with self.assertRaises(ValueError):
            td2str(td(0))
        # =====================================================================
