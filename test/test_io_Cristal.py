#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_Cristal in pyspc.io.cristal

To run all tests just type:
    python -m unittest test_data_Cristal

To run only a class test:
    python -m unittest test_data_Cristal.TestCristal

To run only a specific test:
    python -m unittest test_data_Cristal.TestCristal.test_init

"""
# Imports
from datetime import datetime as dt
import os
import unittest

from pyspc.core.series import Series
from pyspc.io.cristal import read_Cristal


class TestCristal(unittest.TestCase):
    """
    Cristal_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'cristal')

    def test_read_hydro(self):
        """
        Test de lecture KMHEAU -> HI, QI
        Test de lecture KMLBAR -> ZI, QI
        """
        # =====================================================================
        valid = [('K0550010', 'HI', None), ('K0550010', 'QI', None),
                 ('K0600010', 'HI', None), ('K0600010', 'QI', None),
                 ('K0600010', 'ZI', None)]
        stations = ['K0550010', 'K0600010']
        first_dtime = dt(2008, 10, 31, 18)
        last_dtime = dt(2008, 11, 1, 18)
        # =====================================================================
        series = read_Cristal(dirname=self.dirname, codes=stations,
                              first_dtime=first_dtime, last_dtime=last_dtime)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), len(valid))
        for v in valid:
            self.assertIn(v, series)
        # =====================================================================

    def test_read_meteo(self):
        """
        Test de lecture KMPLUI -> compteur pluie
        """
        # =====================================================================
        valid = [('K0559910', 'P5m', None)]
        stations = ['K0559910']
        first_dtime = dt(2008, 10, 31, 18)
        last_dtime = dt(2008, 11, 1, 18)
        # =====================================================================
        series = read_Cristal(dirname=self.dirname, codes=stations,
                              first_dtime=first_dtime, last_dtime=last_dtime)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), len(valid))
        for v in valid:
            self.assertIn(v, series)
        # =====================================================================
