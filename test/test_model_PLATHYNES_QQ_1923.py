#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Config in pyspc.model.plathynes

To run all tests just type:
    python -m unittest test_model_PLATHYNES_QQ_1923

To run only a class test:
    python -m unittest test_model_PLATHYNES_QQ_1923.TestPLATHYNES_QQ_1923

To run only a specific test:
    python -m unittest test_model_PLATHYNES_QQ_1923.TestPLATHYNES_QQ_1923.test_init

"""
# Imports
import filecmp
import os
import unittest

# Imports pyspc
from pyspc.model.plathynes import Config


class TestPLATHYNES_QQ_1923(unittest.TestCase):
    """
    PLATHYNES_QQ_1923 class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'plathynes', 'QQ_1.9.23')
        self.name = 'QQ_auto'
        self.event_name = '2021_01'
        self.simul_name = 'SIM_FINALE'
        self.model_name = 'QQ_autoMOHYS'

    def test_read_event(self):
        """
        Test de la lecture de configuration de type 'event'
        """
        valid = {
            'event settings': {
                "Nom de l'evenement": '2021_01',
                'Description': '',
            },
            'Temporal settings': {
                'Date de debut': '2020-12-20 00:00:00',
                'Date de fin': '2021-02-21 00:00:00',
                'Pas de temps de forcage': '01:00',
                'Pas de temps de calcul': '01:00',
                'Pas de temps des sorties': '01:00',
                'Pas de temps des bilans': '01:00',
            },
            'Parameters': {
                'evenementiels': {
                    ('K5133010', 'Parametre evenementiel'):
                        'U 1.0 K5133010'
                },
            },
            'Forcing settings': {
                'pluies': {},
                'debits': {
                    ('Ev_2021_01/2021_01_1.mqi',
                     'Source de debits'): 'Ev_2021_01/2021_01_1.mqi',
                    ('Ev_2021_01/2021_01_2.mqi',
                     'Source de debits'): 'Ev_2021_01/2021_01_2.mqi'
                },
            },
            'Observations': {
                'observations': {
                    ('Ev_2021_01/2021_01_1.mqo',
                     "Fichier d'observation"): 'Ev_2021_01/2021_01_1.mqo'
                },
            },
        }
        filename = os.path.join(
            self.dirname, self.name,
            f'Ev_{self.event_name}', f'{self.event_name}.evt')
        datatype = 'event'
        cfg = Config(filename=filename, datatype=datatype)
        cfg.read()
        self.assertDictEqual(cfg, valid)

    def test_read_model(self):
        """
        Test de la lecture de configuration de type 'model'
        """
        self.maxDiff = None
        valid = {
            'Network': {
                "Network file": '.\\QQ_autoMOHYS.net',
            },
            'Stations': {
                'stations': {
                    ('CHAMBONSURVOUEIZE#exu_CHA',
                     'Station'): 'CHAMBONSURVOUEIZE#exu_CHA 85.00 0.00 0.00 Q',
                    ('CHAMPAGNAT#CHA_CHA',
                     'Station'): 'CHAMPAGNAT#CHA_CHA -30.00 200.00 -1000000.00 Qin',
                    ('GOUZON#GOU_CHA',
                     'Station'): 'GOUZON#GOU_CHA 70.00 200.00 -1000000.00 Qin',
                }
            },
            'Unites modele': {
                'surface units': {
                    ('CHA_CHA', 'Surface unit'): 'CHA_CHA',
                    ('GOU_CHA', 'Surface unit'): 'GOU_CHA',
                },
                'soil units': {},
                'node units': {
                    ('exu_CHA', 'Node unit'): 'exu_CHA',
                },
            },
            'Regression functions': {
                'regression functions': {
                    ('.\\QQ_auto\\Multilines_AlphaS_CHA_1.txt',
                     'Regression function'): 'M .\\QQ_auto\\Multilines_AlphaS_CHA_1.txt',
                    ('.\\QQ_auto\\Multilines_AlphaS_GOU_2.txt',
                     'Regression function'): 'M .\\QQ_auto\\Multilines_AlphaS_GOU_2.txt',
                    ('.\\QQ_auto\\Multilines_Tp_CHA_3.txt',
                     'Regression function'): 'M .\\QQ_auto\\Multilines_Tp_CHA_3.txt',
                    ('.\\QQ_auto\\Multilines_Tp_GOU_4.txt',
                     'Regression function'): 'M .\\QQ_auto\\Multilines_Tp_GOU_4.txt',
                },
            },
            'Model functions': {
                ('1', 'Runoff function'): '1 MOHYS_TG1 U 0.0 0 T CHAMPAGNAT 1 U 120.0 0 U 0.0 0 U 0.0 0',
                ('1', 'Runoff optimisation'): '1 F 5.0 600.0 0.6 F 1.0 3.0 0.01 F 60.0 1440.0 1.44 F -300.0 300.0 0.3 F -300.0 300.0 0.3',
                ('2', 'Runoff function'): '2 MOHYS_TG1 U 0.0 0 T GOUZON 2 U 120.0 0 U 0.0 0 U 0.0 0',
                ('2', 'Runoff optimisation'): '2 F 5.0 600.0 0.6 F 0.9 4.0 0.009 F 60.0 1440.0 1.44 F -300.0 300.0 0.3 F -300.0 300.0 0.3',
            },
        }
        filename = os.path.join(self.dirname, self.name,
                                f'{self.model_name}.mwc')
        datatype = 'model'
        cfg = Config(filename=filename, datatype=datatype)
        cfg.read()
#        self.assertEqual(cfg['Model functions'],
#                         valid['Model functions'])
#        for k, v in valid.items():
#            print('='*10)
#            print(k)
#            self.assertIn(k, cfg)
#            if isinstance(v, dict):
#                for k2, v2 in v.items():
#                    print(k2)
#                    self.assertIn(k2, cfg[k])
#                    self.assertEqual(cfg[k][k2], v2)
#            self.assertEqual(cfg[k], v)
        self.assertDictEqual(cfg, valid)

    def test_read_project(self):
        """
        Test de la lecture de configuration de type 'event'
        """
        valid = {
            'Charts set': {
                'graphs': {}
            },
            'Configurations': {
                'configurations': {
                    ('MOHYS', 'Configuration'): 'MOHYS ACTIVE'
                }
            },
            'Events': {
                'events': {
                    ('2021_01', 'Evenement'): '2021_01 -1 0 0.0 1',
                    ('2021_07', 'Evenement'): '2021_07 -1 0 0.0 1',
                    ('2021_12', 'Evenement'): '2021_12 -1 0 0.0 1',
                }
            },
            'Settings': {
                'Bassin': 'QQ_auto',
                'Version': '1.9.23',
                'Nom': 'QQ',
                'Repertoire racine': '.',
            },
            'Simulations': {
                'simulations': {
                    ('VISU', 'Simulation'): 'VISU',
                    ('SIM_FINALE', 'Simulation'): 'SIM_FINALE'
                }
            },
            'Stations': {
                'pluvio': {},
                'hydro': {
                    ('CHAMPAGNAT', 'Station hydro'):
                        'CHAMPAGNAT K5133010 647664.000000 6546940.000000 202.000000',
                    ('GOUZON', 'Station hydro'):
                        'GOUZON K5143110 642950.000000 6566580.000000 144.000000',
                    ('CHAMBONSURVOUEIZE', 'Station hydro'):
                        'CHAMBONSURVOUEIZE K5183020 657173.000000 6565087.000000 850.000000',
                },
            },
            'Structures set': {
                'structures': {}
            },
            'Tables set': {
                'tables': {
                    ('Sol_12_Classes.txt',
                     'Table'): 'Sol_12_Classes.txt',
                    ('Ods_12_Classes.txt',
                     'Table'): 'Ods_12_Classes.txt',
                    ('Ods_CorineLandCover.txt',
                     'Table'): 'Ods_CorineLandCover.txt',
                }
            },
        }
        filename = os.path.join(self.dirname, f'{self.name}.prj')
        datatype = 'project'
        cfg = Config(filename=filename, datatype=datatype)
        cfg.read()
        self.assertDictEqual(cfg, valid)

    def test_read_simul(self):
        """
        Test de la lecture de configuration de type 'simul'
        """
        self.maxDiff = None
        valid = {
            'MWC file': {
                "MWC file": 'QQ_autoMOHYS.mwc',
            },
            'Events': {
                'events': {
                    ('Ev_2021_01\\2021_01.evt',
                     'Event file'): 'Ev_2021_01\\2021_01.evt',
                    ('Ev_2021_12\\2021_12.evt',
                     'Event file'): 'Ev_2021_12\\2021_12.evt'

                }
            },
            'Optimisation settings': {
                'Number of attempts': '10',
                'Maximum number of iterations': '100',
                'Maximum number of simulations': '500',
                'Convergence threshold': '1e-06',
            },
            'Cost functions': {
                'functions': {
                    ('NASH', "Cost function"): 'NASH'
                },
            },
        }
        filename = os.path.join(self.dirname, self.name,
                                f'{self.simul_name}.sim')
        datatype = 'simul'
        cfg = Config(filename=filename, datatype=datatype)
        cfg.read()
        self.assertDictEqual(cfg, valid)

    def test_write_event(self):
        """
        Test de la lecture de configuration de type 'event'
        """
        filename = os.path.join(
            self.dirname, self.name,
            f'Ev_{self.event_name}', f'{self.event_name}.evt')
        datatype = 'event'
        cfg = Config(filename=filename, datatype=datatype)
        cfg.read()
        tmp_filename = os.path.join('data', 'test_plathynes-1923_event.evt')
        cfg.filename = tmp_filename
        cfg.write()
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_filename
        ))
        os.remove(tmp_filename)

    def test_write_model(self):
        """
        Test de la lecture de configuration de type 'event'
        """
        filename = os.path.join(self.dirname, self.name,
                                f'{self.model_name}.mwc')
        datatype = 'model'
        cfg = Config(filename=filename, datatype=datatype)
        cfg.read()
        tmp_filename = os.path.join('data', 'test_plathynes-1923_model.mwc')
        cfg.filename = tmp_filename
        cfg.write()
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_filename
        ))
        os.remove(tmp_filename)

    def test_write_project(self):
        """
        Test de la lecture de configuration de type 'project'
        """
        filename = os.path.join(self.dirname, f'{self.name}.prj')
        datatype = 'project'
        cfg = Config(filename=filename, datatype=datatype)
        cfg.read()
        tmp_filename = os.path.join('data', 'test_plathynes-1923_project.prj')
        cfg.filename = tmp_filename
        cfg.write()
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_filename
        ))
        os.remove(tmp_filename)

    def test_write_simul(self):
        """
        Test de la lecture de configuration de type 'simul'
        """
        filename = os.path.join(self.dirname, self.name,
                                f'{self.simul_name}.sim')
        datatype = 'simul'
        cfg = Config(filename=filename, datatype=datatype)
        cfg.read()
        tmp_filename = os.path.join('data', 'test_plathynes-1923_simul.sim')
        cfg.filename = tmp_filename
        cfg.write()
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_filename
        ))
        os.remove(tmp_filename)
