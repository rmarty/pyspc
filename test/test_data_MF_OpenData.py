#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for MF_OpenData in pyspc.data.meteofrance

To run all tests just type:
    python -m unittest test_MF_OpenData

To run only a class test:
    python -m unittest test_MF_OpenData.TestMF_OpenData

To run only a specific test:
    python -m unittest test_MF_OpenData.TestMF_OpenData.test_init

"""
# Imports
from datetime import timedelta as td
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.data.meteofrance import MF_OpenData


class TestMF_OpenData(unittest.TestCase):
    """
    MF_OpenData class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'mf', 'open_data')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = 'Q_07_latest-2023-2024_RR-T-Vent.csv.gz'
        data = MF_OpenData(filename=filename)
        self.assertEqual(data.filename, filename)
        self.assertEqual(data.prefix, 'Q')
        self.assertEqual(data.timestep, td(days=1))
        self.assertEqual(data.dept, '07')
        self.assertEqual(data.period, 'latest-2023-2024')
        self.assertEqual(data.varname, 'RR-T-Vent')
        self.assertTrue(data.compressed)

    def test_MN(self):
        """
        Test de lecture/écriture de données MINUTES
        """
        # =====================================================================
        valid = pnd.DataFrame({
            'NUM_POSTE': ['43091005', '43091005', '43091005', '43091005',
                          '43091005', '43091005', '43091005', '43091005',
                          '43091005', '43091005', '43091005', '43091005',
                          '43091005', '43091005', '43091005', '43091005',
                          '43091005', '43091005', '43091005', '43091005',
                          '43130002', '43130002', '43130002', '43130002',
                          '43130002', '43130002', '43130002', '43130002',
                          '43130002', '43130002', '43130002', '43130002',
                          '43130002', '43130002', '43130002', '43130002',
                          '43130002', '43130002', '43130002', '43130002'],
            'NOM_USUEL': ['LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'MAZET-VOLAMONT', 'MAZET-VOLAMONT', 'MAZET-VOLAMONT',
                          'MAZET-VOLAMONT', 'MAZET-VOLAMONT', 'MAZET-VOLAMONT',
                          'MAZET-VOLAMONT', 'MAZET-VOLAMONT', 'MAZET-VOLAMONT',
                          'MAZET-VOLAMONT', 'MAZET-VOLAMONT', 'MAZET-VOLAMONT',
                          'MAZET-VOLAMONT', 'MAZET-VOLAMONT', 'MAZET-VOLAMONT',
                          'MAZET-VOLAMONT', 'MAZET-VOLAMONT', 'MAZET-VOLAMONT',
                          'MAZET-VOLAMONT', 'MAZET-VOLAMONT'],
            'LAT': [44.900500, 44.900500, 44.900500, 44.900500, 44.900500,
                    44.900500, 44.900500, 44.900500, 44.900500, 44.900500,
                    44.900500, 44.900500, 44.900500, 44.900500, 44.900500,
                    44.900500, 44.900500, 44.900500, 44.900500, 44.900500,
                    45.025000, 45.025000, 45.025000, 45.025000, 45.025000,
                    45.025000, 45.025000, 45.025000, 45.025000, 45.025000,
                    45.025000, 45.025000, 45.025000, 45.025000, 45.025000,
                    45.025000, 45.025000, 45.025000, 45.025000, 45.025000],
            'LON': [4.161000, 4.161000, 4.161000, 4.161000, 4.161000, 4.161000,
                    4.161000, 4.161000, 4.161000, 4.161000, 4.161000, 4.161000,
                    4.161000, 4.161000, 4.161000, 4.161000, 4.161000, 4.161000,
                    4.161000, 4.161000, 4.239833, 4.239833, 4.239833, 4.239833,
                    4.239833, 4.239833, 4.239833, 4.239833, 4.239833, 4.239833,
                    4.239833, 4.239833, 4.239833, 4.239833, 4.239833, 4.239833,
                    4.239833, 4.239833, 4.239833, 4.239833],
            'ALTI': [1350, 1350, 1350, 1350, 1350, 1350, 1350, 1350, 1350,
                     1350, 1350, 1350, 1350, 1350, 1350, 1350, 1350, 1350,
                     1350, 1350, 1130, 1130, 1130, 1130, 1130, 1130, 1130,
                     1130, 1130, 1130, 1130, 1130, 1130, 1130, 1130, 1130,
                     1130, 1130, 1130, 1130],
            'AAAAMMJJHHMN': ['202105101100', '202105101106', '202105101112',
                             '202105101118', '202105101124', '202105101130',
                             '202105101136', '202105101142', '202105101148',
                             '202105101154', '202105101200', '202105101206',
                             '202105101212', '202105101218', '202105101224',
                             '202105101230', '202105101236', '202105101242',
                             '202105101248', '202105101254', '202105101100',
                             '202105101106', '202105101112', '202105101118',
                             '202105101124', '202105101130', '202105101136',
                             '202105101142', '202105101148', '202105101154',
                             '202105101200', '202105101206', '202105101212',
                             '202105101218', '202105101224', '202105101230',
                             '202105101236', '202105101242', '202105101248',
                             '202105101254'],
            'RR': [0.800, 0.400, 0.600, 1.400, 1.400, 1.200, 0.600, 0.700,
                   1.000, 1.200, 2.200, 1.400, 0.800, 0.800, 0.800, 1.100,
                   1.000, 0.800, 1.000, 1.200, 0.800, 0.600, 0.600, 1.000,
                   0.800, 1.000, 1.400, 1.000, 1.200, 1.000, 1.000, 0.800,
                   1.700, 1.200, 1.400, 1.000, 0.800, 1.400, 1.200, 1.400],
            'QRR': [9]*40,
        })
        # =====================================================================
        filename = os.path.join(self.dirname,
                                'MN_43_previous-2020-2022.csv.gz')
        reader = MF_OpenData(filename=filename)
        content = reader.read()
        assert_frame_equal(content, valid)
        # =====================================================================

    def test_H(self):
        """
        Test de lecture/écriture de données HORAIRE
        """
        # =====================================================================
        valid = pnd.DataFrame({
            'NUM_POSTE': ['43091005', '43091005', '43091005', '43091005',
                          '43091005', '43091005', '43091005', '43091005',
                          '43091005', '43091005', '43130002', '43130002',
                          '43130002', '43130002', '43130002', '43130002',
                          '43130002', '43130002', '43130002', '43130002'],
            'NOM_USUEL': ['LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'LES ESTABLES_SAPC', 'LES ESTABLES_SAPC',
                          'MAZET-VOLAMONT', 'MAZET-VOLAMONT', 'MAZET-VOLAMONT',
                          'MAZET-VOLAMONT', 'MAZET-VOLAMONT', 'MAZET-VOLAMONT',
                          'MAZET-VOLAMONT', 'MAZET-VOLAMONT', 'MAZET-VOLAMONT',
                          'MAZET-VOLAMONT'],
            'LAT': [44.900500, 44.900500, 44.900500, 44.900500, 44.900500,
                    44.900500, 44.900500, 44.900500, 44.900500, 44.900500,
                    45.025000, 45.025000, 45.025000, 45.025000, 45.025000,
                    45.025000, 45.025000, 45.025000, 45.025000, 45.025000],
            'LON': [4.161000, 4.161000, 4.161000, 4.161000, 4.161000, 4.161000,
                    4.161000, 4.161000, 4.161000, 4.161000, 4.239833, 4.239833,
                    4.239833, 4.239833, 4.239833, 4.239833, 4.239833, 4.239833,
                    4.239833, 4.239833],
            'ALTI': [1350, 1350, 1350, 1350, 1350, 1350, 1350, 1350, 1350,
                     1350, 1130, 1130, 1130, 1130, 1130, 1130, 1130, 1130,
                     1130, 1130],
            'AAAAMMJJHH': ['2019112300', '2019112301', '2019112302',
                           '2019112303', '2019112304', '2019112305',
                           '2019112306', '2019112307', '2019112308',
                           '2019112309', '2019112300', '2019112301',
                           '2019112302', '2019112303', '2019112304',
                           '2019112305', '2019112306', '2019112307',
                           '2019112308', '2019112309'],
            'RR1': [5.9, 6.6, 7.9, 9.1, 11.7, 8.9, 7.8, 8.3, 8.9, 6.2, 5.2,
                    3.8, 5.4, 6.6, 6.4, 7.7, 7.0, 7.4, 9.3, 5.8],
            ' T': [5.0, 4.9, 5.6, 5.2, 5.1, 4.4, 4.0, 3.3, 3.2, 2.4, 6.4, 6.5,
                   6.4, 6.7, 6.4, 5.9, 5.4, 5.0, 4.7, 3.9, ],
        })
        # =====================================================================
        filename = os.path.join(self.dirname,
                                'H_43_2010-2019.csv.gz')
        reader = MF_OpenData(filename=filename)
        content = reader.read()
        self.assertEqual(len(content.index), 20)
        self.assertEqual(len(content.columns), 204)
        content = content[['NUM_POSTE', 'NOM_USUEL', 'LAT', 'LON', 'ALTI',
                           'AAAAMMJJHH', 'RR1', ' T']]
        assert_frame_equal(content, valid)
        # =====================================================================

    def test_Q(self):
        """
        Test de lecture/écriture de données QUOTIDIEN
        """
        # =====================================================================
        valid = pnd.DataFrame({
            'NUM_POSTE': ['07154005', '07154005', '07154005', '07154005',
                          '07154005', '07154005', '07154005', '07154005',
                          '07154005', '07154005', '07232001', '07232001',
                          '07232001', '07232001', '07232001', '07232001',
                          '07232001', '07232001', '07232001', '07232001'],
            'NOM_USUEL': ['MAZAN ABBAYE RAD', 'MAZAN ABBAYE RAD',
                          'MAZAN ABBAYE RAD', 'MAZAN ABBAYE RAD',
                          'MAZAN ABBAYE RAD', 'MAZAN ABBAYE RAD',
                          'MAZAN ABBAYE RAD', 'MAZAN ABBAYE RAD',
                          'MAZAN ABBAYE RAD', 'MAZAN ABBAYE RAD',
                          'ST-ETIENNE LUGDARES', 'ST-ETIENNE LUGDARES',
                          'ST-ETIENNE LUGDARES', 'ST-ETIENNE LUGDARES',
                          'ST-ETIENNE LUGDARES', 'ST-ETIENNE LUGDARES',
                          'ST-ETIENNE LUGDARES', 'ST-ETIENNE LUGDARES',
                          'ST-ETIENNE LUGDARES', 'ST-ETIENNE LUGDARES'],
            'LAT': [44.733833, 44.733833, 44.733833, 44.733833, 44.733833,
                    44.733833, 44.733833, 44.733833, 44.733833, 44.733833,
                    44.651333, 44.651333, 44.651333, 44.651333, 44.651333,
                    44.651333, 44.651333, 44.651333, 44.651333, 44.651333],
            'LON': [4.083833, 4.083833, 4.083833, 4.083833, 4.083833, 4.083833,
                    4.083833, 4.083833, 4.083833, 4.083833, 3.948500, 3.948500,
                    3.948500, 3.948500, 3.948500, 3.948500, 3.948500, 3.948500,
                    3.948500, 3.948500],
            'ALTI': [1240, 1240, 1240, 1240, 1240, 1240, 1240, 1240, 1240,
                     1240, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022,
                     1022, 1022],
            'AAAAMMJJ': ['20231010', '20231011', '20231012', '20231013',
                         '20231014', '20231015', '20231016', '20231017',
                         '20231018', '20231019', '20231010', '20231011',
                         '20231012', '20231013', '20231014', '20231015',
                         '20231016', '20231017', '20231018', '20231019'],
            'RR': [0.0, 0.0, 0.2, 0.0, 0.0, 0.0, 0.2, 2.8, 21.4, 130.8, 0.0,
                   0.0, 0.0, 0.0, 0.0, 0.0, 1.7, 5.9, 27.9, 83.9],
            'TM': [17.5, 16.2, 15.5, 12.9, 12, 4.4, 8.3, 9.6, 12, 12, 13, 12.2,
                   11.8, 13.4, 13.1, 6.4, 8.9, 10.7, 13.1, 13.6, ],
            'TNTXM': [18.5, 18, 16.9, 12.9, 14.1, 5.5, 7.5, 10.1, 11.3,
                      12.1, 14.7, 13.7, 13.5, 11.4, 14.1, 7.8, 7.9, 11.5,
                      12.6, 13.5],
        })
        # =====================================================================
        filename = os.path.join(self.dirname,
                                'Q_07_latest-2023-2024_RR-T-Vent.csv.gz')
        reader = MF_OpenData(filename=filename)
        content = reader.read()
        self.assertEqual(len(content.index), 20)
        self.assertEqual(len(content.columns), 56)
        content = content[['NUM_POSTE', 'NOM_USUEL', 'LAT', 'LON', 'ALTI',
                           'AAAAMMJJ', 'RR', 'TM', 'TNTXM']]
        assert_frame_equal(content, valid)
        # =====================================================================

    def test_Q_others(self):
        """
        Test de lecture/écriture de données QUOTIDIEN
        """
        # =====================================================================
        filename = os.path.join(
                self.dirname, 'Q_07_latest-2023-2024_autres-parametres.csv.gz')
        reader = MF_OpenData(filename=filename)
        content = reader.read()
        self.assertEqual(len(content.index), 20)
        self.assertEqual(len(content.columns), 88)
        # =====================================================================
