#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary bdapbp2json

To run all tests just type:
    python -m unittest test_bin_bdapbp2json

To run only a class test:
    python -m unittest test_bin_bdapbp2json.Test_bdapbp2json

To run only a specific test:
    python -m unittest test_bin_bdapbp2json.Test_bdapbp2json.test_long

"""
# Imports
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import bdapbp2json as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_bdapbp2json(unittest.TestCase):
    """
    bdapbp2json bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'bdapbp2json')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dirname2 = os.path.join('data', 'schapi', 'lamedo')
        cls.file_cfg = os.path.join('..', 'bin', 'bdapbp2json_RM.txt')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_long(self):
        """
        Test téléchargement - Format long
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdapbp2json.py',
            '-O', self.dir_out,
            '-S', '41005',
            '-r', '2020061206',
            '-t', 'long',
            '-c', self.file_cfg
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.bdapbp2json()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.domain_name, '41005')
        self.assertEqual(args.data_type, 'long')
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.runtime, '2020061206')
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = os.path.join(self.dir_out,
                                '41005_202006120600_bp-long.json')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraction des prévisions BP à la date {r} au format "\
            "{t} pour le domaine spatiale {S} dans le répertoire local {O}. "\
            "L'accès à BdApbp est défini au travers du fichier {c}".format(
                S=args.domain_name, r=args.runtime, t=args.data_type,
                O=args.output_dir, c=args.cfg_filename)
        # =====================================================================

    def test_short(self):
        """
        Test téléchargement - Format short
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdapbp2json.py',
            '-O', self.dir_out,
            '-S', '41005+41003',
            '-r', '2020061206',
            '-t', 'short',
            '-c', self.file_cfg
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.bdapbp2json()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.domain_name, '41005+41003')
        self.assertEqual(args.data_type, 'short')
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.runtime, '2020061206')
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = os.path.join(self.dir_out,
                                '41005+41003_202006120600_bp-short.json')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraction des prévisions BP à la date {r} au format "\
            "{t} pour le domaine spatiale {S} (2 zones) dans le répertoire "\
            "local {O}. L'accès à BdApbp est défini au travers du fichier "\
            "{c}".format(S=args.domain_name, r=args.runtime,
                         t=args.data_type, O=args.output_dir,
                         c=args.cfg_filename)
        # =====================================================================
