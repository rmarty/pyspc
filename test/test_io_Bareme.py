#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_Bareme and write_Bareme in pyspc.io.bareme

To run all tests just type:
    python -m unittest test_data_Bareme

To run only a class test:
    python -m unittest test_data_Bareme.TestBareme

To run only a specific test:
    python -m unittest test_data_Bareme.TestBareme.test_init

"""
# Imports
from datetime import datetime as dt
import os
import unittest

from pyspc.core.ratingcurve import RatingCurves
from pyspc.core.series import Series
from pyspc.io.bareme import read_Bareme


class TestBareme(unittest.TestCase):
    """
    Bareme_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'io', 'dbase')
        self.filename = os.path.join(self.dirname, 'bareme.mdb')

    def test_Bareme_flowmes(self):
        """
        Test Bareme - Cas flowmes
        """
        # =====================================================================
        codes = ['K055001010']
        first_dt = dt(2008, 9, 1)
        last_dt = dt(2009, 2, 1)
        hydro3 = True
        datatype = 'flowmes'
        # =====================================================================
        series = read_Bareme(
            filename=self.filename, datatype=datatype, codes=codes,
            first_dt=first_dt, last_dt=last_dt, hydro3=hydro3)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(
            ('K055001010', 'QI', 'Bareme-flowmes'), series)
        self.assertIn(
            ('K055001010', 'HI', 'Bareme-flowmes'), series)
        # =====================================================================

    def test_Bareme_levelcor(self):
        """
        Test Bareme - Cas levelcor
        """
        # =====================================================================
        codes = ['K055001010']
        first_dt = dt(2014, 1, 1)
        last_dt = dt(2015, 1, 1)
        hydro3 = True
        datatype = 'levelcor'
        # =====================================================================
        series = read_Bareme(
            filename=self.filename, datatype=datatype, codes=codes,
            first_dt=first_dt, last_dt=last_dt, hydro3=hydro3)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(
            ('K055001010', 'HI', 'Bareme-levelcor'), series)
        # =====================================================================

    def test_Bareme_ratingcurve(self):
        """
        Test Bareme - Cas ratingcurve
        """
        # =====================================================================
        codes = ['K010002010']
        first_dt = dt(2008, 9, 1)
        last_dt = dt(2010, 9, 1)
        hydro3 = True
        datatype = 'ratingcurve'
        curves = read_Bareme(
            filename=self.filename, datatype=datatype, codes=codes,
            first_dt=first_dt, last_dt=last_dt, hydro3=hydro3)
        self.assertIsInstance(curves, RatingCurves)
        self.assertEqual(len(curves), 3)
        self.assertEqual(curves.codes, ['K010002010'] * 3)
        self.assertEqual(curves.nums, ['H200708', 'H200910', 'H200809'])
        self.assertEqual(curves.providers, ['Bareme'] * 3)
        # =====================================================================
        codes = ['K055001010']
        first_dt = dt(2013, 9, 1)
        last_dt = dt(2015, 9, 1)
        hydro3 = True
        datatype = 'ratingcurve'
        curves = read_Bareme(
            filename=self.filename, datatype=datatype, codes=codes,
            first_dt=first_dt, last_dt=last_dt, hydro3=hydro3)
        self.assertIsInstance(curves, RatingCurves)
        self.assertEqual(len(curves), 4)
        self.assertEqual(curves.codes, ['K055001010'] * 4)
        self.assertEqual(
            curves.nums, ['H201314', 'H201420ex', 'H201414', 'H201420'])
        self.assertEqual(curves.providers, ['Bareme'] * 4)
        # =====================================================================
