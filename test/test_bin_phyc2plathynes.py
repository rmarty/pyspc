#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary phyc2plathynes

To run all tests just type:
    python -m unittest test_bin_phyc2plathynes

To run only a class test:
    python -m unittest test_bin_phyc2plathynes.Test_phyc2plathynes

To run only a specific test:
    python -m unittest test_bin_phyc2plathynes.Test_phyc2plathynes.test_dl

"""
# Imports
import filecmp
import os
import sys
import unittest

from pyspc.binutils.args import phyc2plathynes as _args
from pyspc.binutils.runs import phyc2plathynes as _runs
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_phyc2plathynes(unittest.TestCase):
    """
    phyc2plathynes bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'phyc2plathynes')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'plathynes')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.file_cfg = os.path.join('..', 'bin', 'phyc2xml_RM.txt')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_ctl_pla = os.path.join(cls.dir_ctl, 'plathynes')
        cls.dir_ctl_xml = os.path.join(cls.dir_ctl, 'xml')
        if not os.path.exists(cls.dir_in):
            os.makedirs(cls.dir_in)
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.stations_list_file = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)

    def test_dl(self):
        """
        Tests DOWNLOAD
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/phyc2plathynes.py',
            '-I', self.dir_in,
            '-D', self.file_cfg,
            '-O', self.dir_out,
            '-c', 'plathynes_project.prj',
            '-M', 'dl'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.phyc2plathynes()
        self.assertEqual(args.processing_method, ['dl'])
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.varname, ['P', 'Q'])
        self.assertEqual(args.projet_filename, 'plathynes_project.prj')
        self.assertEqual(args.events, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            _ = _runs.phyc2plathynes(args)
        for filename in os.listdir(self.dir_in):
            self.assertTrue(os.path.exists(os.path.join(
                self.dir_ctl_xml, os.path.basename(filename))))
            os.remove(os.path.join(self.dir_in, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données nécessaires à PLATHYNES depuis "\
            "la PHyC ({M}) et enregistrer les fichiers xmls dans le "\
            "répertoire {I}. Le projet PLATHYNES est {c} dans le répertoire "\
            "{O}. Tous les "\
            "événements sont considérés. La configuration de la connexion à "\
            "la PHyC est spécifiée dans le fichier {D}.".format(
                M=args.processing_method[0], c=args.projet_filename,
                D=args.cfg_filename,
                I=args.input_dir, O=args.output_dir)
        # =====================================================================

    def test_dl_event(self):
        """
        Tests DOWNLOAD - SELECTED EVENTS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/phyc2plathynes.py',
            '-I', self.dir_in,
            '-D', self.file_cfg,
            '-O', self.dir_out,
            '-c', 'plathynes_project.prj',
            '-S', '201911',
            '-M', 'dl'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.phyc2plathynes()
        self.assertEqual(args.processing_method, ['dl'])
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.varname, ['P', 'Q'])
        self.assertEqual(args.projet_filename, 'plathynes_project.prj')
        self.assertEqual(args.events, ['201911'])
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            _ = _runs.phyc2plathynes(args)
        # =====================================================================
        for filename in os.listdir(self.dir_in):
            self.assertTrue(os.path.exists(os.path.join(
                self.dir_ctl_xml, os.path.basename(filename))))
            os.remove(os.path.join(self.dir_in, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données nécessaires à PLATHYNES depuis "\
            "la PHyC ({M}) et enregistrer les fichiers xml dans le "\
            "répertoire {I}. Le projet PLATHYNES est {c} dans le répertoire "\
            "{O}. Seul "\
            "l'événement {S} est considéré. La configuration de la connexion "\
            "à la PHyC est spécifiée dans le fichier {D}.".format(
                M=args.processing_method[0], c=args.projet_filename,
                S=args.events[0], D=args.cfg_filename,
                I=args.input_dir, O=args.output_dir)
        # =====================================================================

    def test_cv(self):
        """
        Tests CONVERT
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/phyc2plathynes.py',
            '-I', self.dir_ctl_xml,
            '-O', self.dir_out,
            '-c', 'plathynes_project.prj',
            '-s', 'K0030020',
            '-M', 'cv'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.phyc2plathynes()
        self.assertEqual(args.processing_method, ['cv'])
        self.assertEqual(args.input_dir, self.dir_ctl_xml)
        self.assertEqual(args.cfg_filename, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.varname, ['P', 'Q'])
        self.assertEqual(args.projet_filename, 'plathynes_project.prj')
        self.assertEqual(args.events, None)
        self.assertEqual(args.station_name, 'K0030020')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            _ = _runs.phyc2plathynes(args)
        # =====================================================================
        for evt in ['201910', '201911']:
            evt_dir = os.path.join(
                self.dir_out,
                'plathynes_project',
                'Ev_{}'.format(evt)
            )
            for filename in os.listdir(evt_dir):
                if not filename.endswith('.evt'):
                    self.assertTrue(filecmp.cmp(
                        os.path.join(evt_dir, filename),
                        os.path.join(self.dir_ctl_pla, filename),
                    ))
                    os.remove(os.path.join(evt_dir, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données nécessaires à PLATHYNES depuis "\
            "la PHyC ({M}) et enregistrer les fichiers xml dans le "\
            "répertoire {I}. Le projet PLATHYNES est {c} dans le répertoire "\
            "{O}: la station {s} est une station d'injection. Tous les "\
            "événements sont considérés.".format(
                M=args.processing_method[0], c=args.projet_filename,
                s=args.station_name,
                I=args.input_dir, O=args.output_dir)
        # =====================================================================

    def test_cv_onlyP(self):
        """
        Tests CONVERT
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/phyc2plathynes.py',
            '-I', self.dir_ctl_xml,
            '-O', self.dir_out,
            '-c', 'plathynes_project.prj',
            '-s', 'K0030020',
            '-M', 'cv',
            '-n', 'P'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.phyc2plathynes()
        self.assertEqual(args.processing_method, ['cv'])
        self.assertEqual(args.input_dir, self.dir_ctl_xml)
        self.assertEqual(args.cfg_filename, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.varname, ['P'])
        self.assertEqual(args.projet_filename, 'plathynes_project.prj')
        self.assertEqual(args.events, None)
        self.assertEqual(args.station_name, 'K0030020')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            _ = _runs.phyc2plathynes(args)
        # =====================================================================
        for evt in ['201910', '201911']:
            evt_dir = os.path.join(
                self.dir_out,
                'plathynes_project',
                'Ev_{}'.format(evt)
            )
            for filename in os.listdir(evt_dir):
                if not filename.endswith('.evt'):
                    self.assertTrue(filecmp.cmp(
                        os.path.join(evt_dir, filename),
                        os.path.join(self.dir_ctl_pla, filename),
                    ))
                    os.remove(os.path.join(evt_dir, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données nécessaires à PLATHYNES depuis "\
            "la PHyC ({M}) et enregistrer les fichiers xml dans le "\
            "répertoire {I}. Le projet PLATHYNES est {c} dans le répertoire "\
            "{O}: la station {s} est une station d'injection. Tous les "\
            "événements sont considérés. Seules les données '{n}' sont "\
            "exportées.".format(
                M=args.processing_method[0], c=args.projet_filename,
                s=args.station_name,
                I=args.input_dir, O=args.output_dir,
                n=args.varname[0])
        # =====================================================================

    def test_cv_events(self):
        """
        Tests CONVERT - SELECTED EVENTS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/phyc2plathynes.py',
            '-I', self.dir_ctl_xml,
            '-O', self.dir_out,
            '-c', 'plathynes_project.prj',
            '-s', 'K0030020',
            '-S', '201911',
            '-M', 'cv'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.phyc2plathynes()
        self.assertEqual(args.processing_method, ['cv'])
        self.assertEqual(args.input_dir, self.dir_ctl_xml)
        self.assertEqual(args.cfg_filename, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.varname, ['P', 'Q'])
        self.assertEqual(args.projet_filename, 'plathynes_project.prj')
        self.assertEqual(args.events, ['201911'])
        self.assertEqual(args.station_name, 'K0030020')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            _ = _runs.phyc2plathynes(args)
        # =====================================================================
        evt_dir = os.path.join(
            self.dir_out, 'plathynes_project', 'Ev_{}'.format(args.events[0]))
        for filename in os.listdir(evt_dir):
            if not filename.endswith('.evt'):
                self.assertTrue(filecmp.cmp(
                    os.path.join(evt_dir, filename),
                    os.path.join(self.dir_ctl_pla, filename),
                ))
                os.remove(os.path.join(evt_dir, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données nécessaires à PLATHYNES depuis "\
            "la PHyC ({M}) et enregistrer les fichiers xml dans le "\
            "répertoire {I}. Le projet PLATHYNES est {c} dans le répertoire "\
            "{O}: la station {s} est une station d'injection. Seul "\
            "l'événement {S} est considéré.".format(
                M=args.processing_method[0], c=args.projet_filename,
                s=args.station_name, S=args.events[0],
                I=args.input_dir, O=args.output_dir)
        # =====================================================================
