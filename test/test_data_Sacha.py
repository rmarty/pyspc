#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Sacha in pyspc.data.sacha

To run all tests just type:
    python -m unittest test_data_Sacha

To run only a class test:
    python -m unittest test_data_Sacha.TestSacha

To run only a specific test:
    python -m unittest test_data_Sacha.TestSacha.test_init

"""
# Imports
from datetime import datetime as dt
import filecmp
import os
import shutil
import numpy as np
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.data.sacha import Sacha


class TestSacha(unittest.TestCase):
    """
    Sacha class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'io', 'dbase')
        self.source = os.path.join(self.dirname, 'sacha_montpezat.mdb')
        self.source2 = os.path.join('data', 'sacha_test_insert.mdb')
        shutil.copy2(
            os.path.join('..', 'resources', 'dbase', 'sacha.mdb'),
            self.source2
        )
        self.loc_hydro2 = {
             1: {'altitude': None,
                 'bv': None,
                 'code_bareme': None,
                 'codeh': None,
                 'codep': 'K0009910',
                 'codeq': None,
                 'codet': None,
                 'comment': None,
                 'courdo': 'Loire Amont',
                 'nom': 'Sainte-Eulalie',
                 'nosta': 1,
                 'xlambert': None,
                 'ylambert': None},
             2: {'altitude': None,
                 'bv': None,
                 'code_bareme': 'K0010020',
                 'codeh': 'K0010020',
                 'codep': None,
                 'codeq': 'K0010020',
                 'codet': None,
                 'comment': None,
                 'courdo': 'Loire',
                 'nom': 'Barrage-de-la-Palisse',
                 'nosta': 2,
                 'xlambert': None,
                 'ylambert': None},
             3: {'altitude': None,
                 'bv': None,
                 'code_bareme': 'K0018720',
                 'codeh': 'K0018720',
                 'codep': None,
                 'codeq': 'K0018720',
                 'codet': None,
                 'comment': None,
                 'courdo': 'Loire',
                 'nom': 'Barrage-du-Peyron',
                 'nosta': 3,
                 'xlambert': None,
                 'ylambert': None},
             4: {'altitude': None,
                 'bv': None,
                 'code_bareme': None,
                 'codeh': None,
                 'codep': 'K0029910',
                 'codeq': None,
                 'codet': None,
                 'comment': None,
                 'courdo': 'Loire Amont',
                 'nom': "Lac d Issarlès",
                 'nosta': 4,
                 'xlambert': None,
                 'ylambert': None},
             5: {'altitude': None,
                 'bv': 229.0,
                 'code_bareme': 'K0030020',
                 'codeh': 'K0030020',
                 'codep': 'K0030020',
                 'codeq': 'K0030020',
                 'codet': None,
                 'comment': None,
                 'courdo': 'Loire',
                 'nom': 'Pont-de-la-Borie',
                 'nosta': 5,
                 'xlambert': None,
                 'ylambert': None},
             6: {'altitude': None,
                 'bv': 432.0,
                 'code_bareme': None,
                 'codeh': 'K0100020',
                 'codep': 'K0100020',
                 'codeq': 'K0100020',
                 'codet': None,
                 'comment': 'Périodes de retour des seuils réglementaires:\n'
                            '-JAUNE: 1 à 2 ans.\n'
                            '-ORANGE: 15 ans.\n'
                            '-ROUGE: supérieur à 20 ans.\n',
                 'courdo': 'Loire',
                 'nom': 'Goudet',
                 'nosta': 6,
                 'xlambert': None,
                 'ylambert': None},
             7: {'altitude': None,
                 'bv': None,
                 'code_bareme': None,
                 'codeh': None,
                 'codep': '07154005',
                 'codeq': None,
                 'codet': '07154005',
                 'comment': None,
                 'courdo': 'Haut Bassin Loire',
                 'nom': "Mazan-L Abbaye MF",
                 'nosta': 7,
                 'xlambert': None,
                 'ylambert': None}
        }
        self.loc_hydro3 = {
            '1_2_07235005': {'nature': 2, 'nosta': 1, 'ordre': 0, 'tr': 1,
                             'valeur': '07235005'},
            '2_0_K001002010': {'nature': 0, 'nosta': 2, 'ordre': 0, 'tr': 1,
                               'valeur': 'K001002010'},
            '2_1_K001002010': {'nature': 1, 'nosta': 2, 'ordre': 0, 'tr': 1,
                               'valeur': 'K001002010'},
            '3_0_K001872010': {'nature': 0, 'nosta': 3, 'ordre': 0, 'tr': 1,
                               'valeur': 'K001872010'},
            '3_1_K001872010': {'nature': 1, 'nosta': 3, 'ordre': 0, 'tr': 1,
                               'valeur': 'K001872010'},
            '4_2_07119002': {'nature': 2, 'nosta': 4, 'ordre': 0, 'tr': 1,
                             'valeur': '07119002'},
            '5_0_K003002010': {'nature': 0, 'nosta': 5, 'ordre': 0, 'tr': 1,
                               'valeur': 'K003002010'},
            '5_1_K003002010': {'nature': 1, 'nosta': 5, 'ordre': 0, 'tr': 1,
                               'valeur': 'K003002010'},
            '6_0_K010002010': {'nature': 0, 'nosta': 6, 'ordre': 0, 'tr': 1,
                               'valeur': 'K010002010'},
            '6_1_K010002010': {'nature': 1, 'nosta': 6, 'ordre': 0, 'tr': 1,
                               'valeur': 'K010002010'},
            '6_1_K010002011': {'nature': 1, 'nosta': 6, 'ordre': 1, 'tr': 0,
                               'valeur': 'K010002011'},
            '6_2_43101002': {'nature': 2, 'nosta': 6, 'ordre': 0, 'tr': 1,
                             'valeur': '43101002'},
            '7_2_07154005': {'nature': 2, 'nosta': 7, 'ordre': 0, 'tr': 1,
                             'valeur': '07154005'}
        }

    def tearDown(self):
        """
        Applied after test
        """
        if os.path.exists(self.source2):
            os.remove(self.source2)

    def test_init(self):
        """
        Test de la création de l'instance
        """
        reader = Sacha(filename=self.source)
        self.assertEqual(reader.filename, self.source)
        self.assertIsNone(reader._dbase_connect)
        self.assertIsNone(reader._dbase_cursor)
        self.assertIsNone(reader.sql)
        self.assertIsNone(reader._tables)

    def test_datatypes(self):
        """
        Test des types de données SACHA
        """
        dtypes = ['sacha', 'sacha_TR']
        self.assertEqual(Sacha.get_datatypes(), dtypes)

    def test_get_locations_hydro2(self):
        """
        Test de la récupération des entités d'une base
        """
        reader = Sacha(filename=self.source)
        locs = reader.get_locations()
        self.assertDictEqual(locs, self.loc_hydro2)
        locs = reader.get_locations(hydro_version='hydro2')
        self.assertDictEqual(locs, self.loc_hydro2)

    def test_get_locations_hydro3(self):
        """
        Test de la récupération des entités d'une base
        """
        reader = Sacha(filename=self.source)
        locs = reader.get_locations(hydro_version='hydro3')
        self.assertDictEqual(locs, self.loc_hydro3)

    def test_insert_locations(self):
        """
        Test de l'ajout des entités d'une base
        """
        # ===================================================================
        writer = Sacha(filename=self.source2)
        locs = writer.get_locations()
        self.assertDictEqual(locs, {})
        # ===================================================================
        valid = {1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7}
        rows = writer.insert_locations(
            locations=self.loc_hydro2, hydro_version='hydro2')
        self.assertDictEqual(rows, valid)
        # ===================================================================
        valid = {
            '1_2_07235005': 1,
            '2_0_K001002010': 2,
            '2_1_K001002010': 3,
            '3_0_K001872010': 4,
            '3_1_K001872010': 5,
            '4_2_07119002': 6,
            '5_0_K003002010': 7,
            '5_1_K003002010': 8,
            '6_0_K010002010': 9,
            '6_1_K010002010': 10,
            '6_1_K010002011': 11,
            '6_2_43101002': 12,
            '7_2_07154005': 13
        }
        rows = writer.insert_locations(
            locations=self.loc_hydro3, hydro_version='hydro3')
        self.assertDictEqual(rows, valid)
        # ===================================================================

    def test_get_datacoverage(self):
        """
        Test de la récupération de la couverture des données d'une base
        """
        valids = {
            (2, False, 'HH', None): [
                {'end': dt(2008, 11, 5, 0, 0), 'length': 145,
                 'length_ratio': 1.0, 'missing_length': 0,
                 'missing_ratio': 0.0, 'start': dt(2008, 10, 30, 0, 0),
                 'value_max': 1.51, 'value_min': -18.52}],
            (1, False, 'PH', 'gauge'): [
                {'end': dt(2008, 11, 5, 0, 0), 'length': 145,
                 'length_ratio': 1.0, 'missing_length': 0,
                 'missing_ratio': 0.0, 'start': dt(2008, 10, 30, 0, 0),
                 'value_max': 30.0, 'value_min': 0.0}],
            (1, False, 'PH', 'radar'): [
                {'end': None, 'length': 0, 'length_ratio': np.nan,
                 'missing_length': np.nan, 'missing_ratio': np.nan,
                 'start': None, 'value_max': np.nan, 'value_min': np.nan}],
            (6, False, 'QH', None): [
                {'end': dt(2008, 11, 5, 0, 0), 'length': 145,
                 'length_ratio': 1.0, 'missing_length': 0,
                 'missing_ratio': 0.0, 'start': dt(2008, 10, 30, 0, 0),
                 'value_max': 959.0, 'value_min': 2.5899999141693115}],
            (7, False, 'TH', None): [
                {'end': dt(2008, 11, 5, 0, 0), 'length': 145,
                 'length_ratio': 1.0, 'missing_length': 0,
                 'missing_ratio': 0.0, 'start': dt(2008, 10, 30, 0, 0),
                 'value_max': 9.700000000000001, 'value_min': -5.9}],
        }
        reader = Sacha(filename=self.source)
        for k, v in valids.items():
            coverage = reader.get_datacoverage(
                nosta=k[0], realtime=k[1], varname=k[2], prcp_src=k[3])
            self.assertIsInstance(coverage, list)
            self.assertEqual(len(coverage), len(v))
            for c, t in zip(coverage, v):
                self.assertDictEqual(c, t)

    def test_get_inventory(self):
        """
        Test de la récupération de l'inventaire d'une base
        """
        reader = Sacha(filename=self.source)
        exports = reader.get_inventory(dirname='data')
        for f in exports:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dirname, os.path.basename(f))))
            os.remove(f)

    def test_prcp_src(self):
        """
        Test de la liste des sources
        """
        self.assertEqual(Sacha.get_prcp_src(), ['gauge', 'radar'])

    def test_read_P_hydro2(self):
        """
        Test de la lecture de données de précipitations sol
        """
        # ====================================================================
        valid = pnd.DataFrame(
            {('07154005', 'PH'): [16.40, 13.40, 12.30, 8.00],
             ('K0009910', 'PH'): [15.80, 7.00, 6.80, 3.20],
             ('K0029910', 'PH'): [12.80, 10.80, 13.80, 8.40],
             ('K0100020', 'PH'): [9.00, 7.00, 6.40, 10.00]},
            index=pnd.date_range(
                dt(2008, 11, 1, 23), dt(2008, 11, 2, 2), freq='H')
        )
        # ====================================================================
        stations = ['K0009910', 'K0029910', 'K0100020', '07154005']
        varname = 'PH'
        first_dt = dt(2008, 11, 1, 23)
        last_dt = dt(2008, 11, 2, 2)
        reader = Sacha(filename=self.source)
        # ====================================================================
        content = reader.read(
            codes=stations,
            varname=varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=False,
            prcp_src='gauge',
            hydro_version='hydro2'
        )
        assert_frame_equal(content, valid)
        # ====================================================================

    def test_read_P_hydro3(self):
        """
        Test de la lecture de données de précipitations sol
        """
        # ====================================================================
        valid = pnd.DataFrame(
            {('07119002', 'PH'): [12.80, 10.80, 13.80, 8.40],
             ('07154005', 'PH'): [16.40, 13.40, 12.30, 8.00],
             ('07235005', 'PH'): [15.80, 7.00, 6.80, 3.20],
             ('43101002', 'PH'): [9.00, 7.00, 6.40, 10.00]},
            index=pnd.date_range(
                dt(2008, 11, 1, 23), dt(2008, 11, 2, 2), freq='H')
        )
        # ====================================================================
        stations = ['07235005', '07119002', '43101002', '07154005']
        varname = 'PH'
        first_dt = dt(2008, 11, 1, 23)
        last_dt = dt(2008, 11, 2, 2)
        reader = Sacha(filename=self.source)
        # ====================================================================
        content = reader.read(
            codes=stations,
            varname=varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=False,
            prcp_src='gauge',
            hydro_version='hydro3'
        )
        assert_frame_equal(content, valid)
        # ====================================================================

    def test_read_Pradar(self):
        """
        Test de la lecture de données de précipitations radar
        """
        # ====================================================================
        valid = pnd.DataFrame(
            {('K0030020', 'PH'): [8.80, 9.40, 11.70, 6.70],
             ('K0100020', 'PH'): [7.80, 8.40, 10.70, 8.60]},
            index=pnd.date_range(
                dt(2008, 11, 1, 23), dt(2008, 11, 2, 2), freq='H')
        )
        # ====================================================================
        stations = ['K0030020', 'K0100020']
        varname = 'PH'
        first_dt = dt(2008, 11, 1, 23)
        last_dt = dt(2008, 11, 2, 2)
        reader = Sacha(filename=self.source)
        # ====================================================================
        content = reader.read(
            codes=stations,
            varname=varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=False,
            prcp_src='radar',
            hydro_version='hydro2'
        )
        assert_frame_equal(content, valid)
        # ====================================================================

    def test_read_Q(self):
        """
        Test de la lecture de données de débit
        """
        # ====================================================================
        valid = pnd.DataFrame(
            {('K0030020', 'QH'): [206.0, 288.0, 280.0, 137.0],
             ('K0100020', 'QH'): [222.0, 195.0, 677.0, 771.0]},
            index=pnd.date_range(
                dt(2008, 11, 1, 23), dt(2008, 11, 2, 2), freq='H')
        )
        # ====================================================================
        stations = ['K0030020', 'K0100020']
        varname = 'QH'
        first_dt = dt(2008, 11, 1, 23)
        last_dt = dt(2008, 11, 2, 2)
        reader = Sacha(filename=self.source)
        # ====================================================================
        content = reader.read(
            codes=stations,
            varname=varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=False,
            hydro_version='hydro2'
        )
        assert_frame_equal(content, valid)
        # ====================================================================
        content = reader.read(
            codes=stations,
            varname=varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=False,
            hydro_version='hydro3'
        )
        assert_frame_equal(content, valid)
        # ====================================================================

    def test_read_Q_realtime(self):
        """
        Test de la lecture de données de débit - TEMPS REEL
        """
        # ====================================================================
        valid = pnd.DataFrame(
            {('K0030020', 'QH'): [431.0, 448.0, 439.0, 338.0],
             ('K0100020', 'QH'): [412.0, 480.0, 581.0, 602.0]},
            index=pnd.date_range(
                dt(2016, 11, 22, 15), dt(2016, 11, 22, 18), freq='H')
        )
        # ====================================================================
        stations = ['K0030020', 'K0100020']
        varname = 'QH'
        first_dt = dt(2016, 11, 22, 15)
        last_dt = dt(2016, 11, 22, 18)
        reader = Sacha(filename=self.source)
        # ====================================================================
        content = reader.read(
            codes=stations,
            varname=varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=True,
            hydro_version='hydro2'
        )
        assert_frame_equal(content, valid)
        # ====================================================================
        content = reader.read(
            codes=stations,
            varname=varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=True,
            hydro_version='hydro3'
        )
        assert_frame_equal(content, valid)
        # ====================================================================

    def test_read_H_hydro2(self):
        """
        Test de la lecture de données de débit
        """
        # ====================================================================
        valid = pnd.DataFrame(
            {('K0010020', 'HH'): [1.49, 1.51, 1.43, 1.28],
             ('K0018720', 'HH'): [-11.03, -10.23, -9.36, -9.24]},
            index=pnd.date_range(
                dt(2008, 11, 1, 23), dt(2008, 11, 2, 2), freq='H')
        )
        # ====================================================================
        stations = ['K0010020', 'K0018720']
        varname = 'HH'
        first_dt = dt(2008, 11, 1, 23)
        last_dt = dt(2008, 11, 2, 2)
        reader = Sacha(filename=self.source)
        # ====================================================================
        content = reader.read(
            codes=stations,
            varname=varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=False,
            hydro_version='hydro2'
        )
        assert_frame_equal(content, valid)
        # ====================================================================

    def test_read_H_hydro3(self):
        """
        Test de la lecture de données de débit
        """
        # ====================================================================
        valid = pnd.DataFrame(
            {('K001002010', 'HH'): [1.49, 1.51, 1.43, 1.28],
             ('K001872010', 'HH'): [-11.03, -10.23, -9.36, -9.24]},
            index=pnd.date_range(
                dt(2008, 11, 1, 23), dt(2008, 11, 2, 2), freq='H')
        )
        # ====================================================================
        stations = ['K001002010', 'K001872010']
        varname = 'HH'
        first_dt = dt(2008, 11, 1, 23)
        last_dt = dt(2008, 11, 2, 2)
        reader = Sacha(filename=self.source)
        # ====================================================================
        content = reader.read(
            codes=stations,
            varname=varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=False,
            hydro_version='hydro3'
        )
        assert_frame_equal(content, valid)
        # ====================================================================

    def test_read_T(self):
        """
        Test de la lecture de données de température
        """
        # ====================================================================
        valid = pnd.DataFrame(
            {('07154005', 'TH'): [7.80, 8.40, 8.60, 8.50]},
            index=pnd.date_range(
                dt(2008, 11, 1, 23), dt(2008, 11, 2, 2), freq='H')
        )
        # ====================================================================
        station = '07154005'
        varname = 'TH'
        first_dt = dt(2008, 11, 1, 23)
        last_dt = dt(2008, 11, 2, 2)
        reader = Sacha(filename=self.source)
        # ====================================================================
        content = reader.read(
            codes=[station],
            varname=varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=False,
            hydro_version='hydro2'
        )
        assert_frame_equal(content, valid)
        # ====================================================================
