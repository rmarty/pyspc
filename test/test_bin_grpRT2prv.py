#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary grpRT2prv

To run all tests just type:
    python -m unittest test_bin_grpRT2prv

To run only a class test:
    python -m unittest test_bin_grpRT2prv.Test_grpRT2prv

To run only a specific test:
    python -m unittest test_bin_grpRT2prv.Test_grpRT2prv.test_rtime_scores

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import grpRT2prv as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_grpRT2prv(unittest.TestCase):
    """
    grpRT2prv bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'grpRT2prv')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_grp16 = os.path.join('data', 'model', 'grp16', 'rt')
        cls.dir_grp18 = os.path.join('data', 'model', 'grp18', 'rt')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_diff_grp16_otamin16(self):
        """
        Test export Prévisions Temps Différé v2016 -> OTAMIN v2016
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpRT2prv.py',
            '-I', self.dir_grp16,
            '-O', self.dir_out,
            '-t', 'grp16_rt_fcst_diff', 'otamin16_fcst',
            '-U', 'Previ', '45gGRPd000'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.grpRT2prv()
        self.assertEqual(args.input_dir, self.dir_grp16)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type,
                         ['grp16_rt_fcst_diff', 'otamin16_fcst'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
#        basename = 'GRP_B_20210416_1407.prv'
#        basename = 'GRP_B_20210416_1507.prv'
        basename = 'GRP_B_20211216_0858.prv'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, 'grp16', basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les prévisions de type {t1} "\
            "situées dans le répertoire {I} "\
            "au format prv de type '{t2}' dans le répertoire {O}. "\
            "".format(
                I=args.input_dir,
                t1=args.data_type[0], t2=args.data_type[1],
                O=args.output_dir)
        # =====================================================================

    def test_diff_grp18_otamin18(self):
        """
        Test export Prévisions Temps Différé v2018 -> OTAMIN v2018
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpRT2prv.py',
            '-I', self.dir_grp18,
            '-O', self.dir_out,
            '-t', 'grp18_rt_fcst_diff', 'otamin18_fcst',
            '-U', 'Previ', '45gGRPd000'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.grpRT2prv()
        self.assertEqual(args.input_dir, self.dir_grp18)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type,
                         ['grp18_rt_fcst_diff', 'otamin18_fcst'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
#        basename = 'GRP_B_20210528_0819.prv'
#        basename = 'GRP_B_20210528_0919.prv'
        basename = 'GRP_B_20211216_0858.prv'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, 'grp18', basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les prévisions de type {t1} "\
            "situées dans le répertoire {I} "\
            "au format prv de type '{t2}' dans le répertoire {O}. "\
            "".format(
                I=args.input_dir,
                t1=args.data_type[0], t2=args.data_type[1],
                O=args.output_dir)
        # =====================================================================

    def test_diff_grp16_scores(self):
        """
        Test export Prévisions Temps Différé v2016 -> OTAMIN v2016
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpRT2prv.py',
            '-I', self.dir_grp16,
            '-O', self.dir_out,
            '-t', 'grp16_rt_fcst_diff', 'scores_fcst',
            '-U', 'Previ', '45gGRPd000'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.grpRT2prv()
        self.assertEqual(args.input_dir, self.dir_grp16)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type,
                         ['grp16_rt_fcst_diff', 'scores_fcst'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
#        basename = 'GRP_B_20210416_1407.csv'
#        basename = 'GRP_B_20210416_1507.csv'
        basename = 'GRP_B_20211216_0858.csv'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, 'grp16', basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les prévisions de type {t1} "\
            "situées dans le répertoire {I} "\
            "au format prv de type '{t2}' dans le répertoire {O}. "\
            "".format(
                I=args.input_dir,
                t1=args.data_type[0], t2=args.data_type[1],
                O=args.output_dir)
        # =====================================================================

    def test_diff_grp18_scores(self):
        """
        Test export Prévisions Temps Différé v2018 -> OTAMIN v2018
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpRT2prv.py',
            '-I', self.dir_grp18,
            '-O', self.dir_out,
            '-t', 'grp18_rt_fcst_diff', 'scores_fcst',
            '-U', 'Previ', '45gGRPd000'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.grpRT2prv()
        self.assertEqual(args.input_dir, self.dir_grp18)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type,
                         ['grp18_rt_fcst_diff', 'scores_fcst'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
#        basename = 'GRP_B_20210528_0819.csv'
#        basename = 'GRP_B_20210528_0919.csv'
        basename = 'GRP_B_20211216_0858.csv'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, 'grp18', basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les prévisions de type {t1} "\
            "situées dans le répertoire {I} "\
            "au format prv de type '{t2}' dans le répertoire {O}. "\
            "".format(
                I=args.input_dir,
                t1=args.data_type[0], t2=args.data_type[1],
                O=args.output_dir)
        # =====================================================================
