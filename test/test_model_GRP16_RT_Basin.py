#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Basin in pyspc.model.grp16

To run all tests just type:
    python -m unittest test_model_GRP16_RT_Basin

To run only a class test:
    python -m unittest test_model_GRP16_RT_Basin.TestGRPRT_Basin

To run only a specific test:
    python -m unittest test_model_GRP16_RT_Basin.TestGRPRT_Basin.test_init

"""
# Imports
import collections
import filecmp
import os
import unittest

from pyspc.model.grp16 import GRPRT_Basin


class TestGRPRT_Basin(unittest.TestCase):
    """
    GRPRT_Basin class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp16', 'rt')
        self.valid = collections.OrderedDict()
        self.valid['B'] = 'Chadron'
        self.valid['S'] = 102.0
        self.valid['A'] = 0.0
        self.valid['T'] = collections.OrderedDict()
        self.valid['T']['P'] = 0
        self.valid['T']['Q'] = 0
        self.valid['E'] = collections.OrderedDict()
        self.valid['E']['K0114030'] = {'w': 1.0, 'n': ''}
        self.valid['F'] = 912.37
        self.valid['P'] = collections.OrderedDict()
        self.valid['P']['43157008'] = {'w': 0.05, 'n': 'LE PUY-EN-VELAY-SPC'}
        self.valid['P']['43135005'] = {'w': 0.30,
                                       'n': 'LE MONASTIER-SUR-GAZEILLE2-SPC'}
        self.valid['P']['43101002'] = {'w': 0.30, 'n': 'GOUDET 2-SPC'}
        self.valid['P']['43091005'] = {'w': 0.05, 'n': 'LES ESTABLES_SAPC'}
        self.valid['P']['43186003'] = {'w': 0.30, 'n': 'SAINT-FRONT-SPC'}
        self.valid['Q'] = collections.OrderedDict()
        self.valid['Q']['K0114030'] = {'w': 1.0, 'n': ''}
        self.valid['G'] = 60.0

        self.valid_snow = collections.OrderedDict()
        self.valid_snow['B'] = 'Morge a Cote Rouge'
        self.valid_snow['S'] = 713.0
        self.valid_snow['A'] = 467.0
        self.valid_snow['T'] = collections.OrderedDict()
        self.valid_snow['T']['P'] = 0
        self.valid_snow['T']['Q'] = 0
        self.valid_snow['E'] = collections.OrderedDict()
        self.valid_snow['E']['K2783010'] = {'w': 1.0, 'n': ''}
        self.valid_snow['F'] = 749.53
        self.valid_snow['P'] = collections.OrderedDict()
        self.valid_snow['P']['03061001'] = {'w': 0.13, 'n': 'Charmes MF'}
        self.valid_snow['P']['63113001'] = {'w': 0.37,
                                            'n': 'Clermont Ferrand MF'}
        self.valid_snow['P']['63263005'] = {'w': 0.17, 'n': 'Orcines MF'}
        self.valid_snow['P']['63417001'] = {'w': 0.33, 'n': 'Sayat MF'}
        self.valid_snow['Q'] = collections.OrderedDict()
        self.valid_snow['Q']['K2783010'] = {'w': 1.0, 'n': ''}
        self.valid_snow['G'] = 30.0
        self.valid_snow['D'] = collections.OrderedDict()
        self.valid_snow['D']['63263005'] = {'w': 1.0, 'n': '', 'z': 971.00}
        self.valid_snow['C'] = collections.OrderedDict()
        self.valid_snow['C']['1'] = {'sn': 15.04, 'z': 315.00}
        self.valid_snow['C']['2'] = {'sn': 16.22, 'z': 340.00}
        self.valid_snow['C']['3'] = {'sn': 24.15, 'z': 467.00}
        self.valid_snow['C']['4'] = {'sn': 39.58, 'z': 626.00}
        self.valid_snow['C']['5'] = {'sn': 63.19, 'z': 797.00}
        self.valid_snow['K'] = 10.53

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'BASSIN.DAT')
        basin = GRPRT_Basin(filename=filename)
        self.assertEqual(basin.filename, filename)

    def test_read(self):
        """
        Test de la lecture du fichier - Cas sans température
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'BASSIN.DAT')
        basin = GRPRT_Basin(filename=filename)
        basin.read()
        self.assertDictEqual(basin, self.valid)
        # =====================================================================
        filename = os.path.join(self.dirname, 'BASSIN_NEIGE.DAT')
        basin = GRPRT_Basin(filename=filename)
        basin.read()
        self.assertDictEqual(basin, self.valid_snow)
        # =====================================================================

    def test_write(self):
        """
        Test d'écriture du fichier - Cas sans température
        """
        filename = os.path.join(self.dirname, 'BASSIN.DAT')
        tmpfile = os.path.join('data', 'BASSIN.DAT')
        basin = GRPRT_Basin(filename=tmpfile)
        basin.update(self.valid)
        basin.write()
        self.assertTrue(filecmp.cmp(
            tmpfile,
            filename
        ))
        os.remove(tmpfile)
