#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Serie in pyspc.core.serie - instanciation

To run all tests just type:
    python -m unittest test_core_Serie_EVENT

To run only a class test:
    python -m unittest test_core_Serie_EVENT.TestSerie

To run only a specific test:
    python -m unittest test_core_Serie_EVENT.TestSerie.test_EVENT_list_str

"""
# Imports
from datetime import datetime as dt, timedelta as td
import filecmp
import os.path
import pandas as pnd
import unittest
from unittest import mock
import warnings

# Imports pyspc
from pyspc.core.serie import Serie


class TestSerie(unittest.TestCase):
    """
    Serie class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        self.dirname = os.path.join('data', 'core', 'serie')
        self.code = 'K0000000'
        self.provider = 'SPC'
        self.varname = 'QJ'
        self.serie = pnd.DataFrame(
            {(self.code, self.varname): [
                14.494, 13.842, 12.497, 11.220, 10.097, 10.069, 9.273, 8.578,
                7.851, 7.583, 7.273, 6.953, 6.639, 6.627, 6.179, 6.269, 6.437,
                5.695, 5.285, 5.026, 4.956, 4.841, 4.670, 4.580, 4.423, 5.648,
                5.640, 5.214, 4.692, 4.401, 4.334, 4.236, 5.175, 5.483, 4.392,
                5.433, 8.873, 7.571, 6.623, 5.956, 4.865, 4.709, 4.914, 4.430,
                4.412, 4.150, 5.702, 6.104, 5.252, 4.614, 4.423, 4.214, 4.020,
                3.893, 4.056, 3.809, 5.472, 22.788, 30.702, 110.418, 129.002,
                51.897, 54.262, 22.252, 18.094, 15.749, 13.831, 12.618, 11.108,
                10.022, 9.658, 9.392, 8.707, 8.193, 7.983, 7.693, 7.407, 7.099,
                6.823, 6.472, 6.330, 8.287, 182.574, 88.658, 53.842, 35.992,
                26.956, 31.630, 30.770, 23.494, 32.264, 24.546, 65.602,
                114.373, 70.916, 49.665, 30.653, 26.361, 22.971, 21.625,
                20.378, 18.774, 17.389, 17.760, 18.165, 22.682, 61.387, 55.575,
                45.393, 37.727, 32.181, 32.849, 38.203, 33.301, 32.345, 31.796,
                29.675, 26.911, 24.656, 27.883, 26.878, 33.760, 32.075, 27.314,
                24.291, 26.955, 38.293, 30.852, 27.837, 24.617, 22.301, 20.165,
                18.577, 18.041, 16.935, 17.425, 16.534, 14.572, 14.307, 13.381,
                12.173, 12.295, 15.051, 19.554, 15.642, 13.758, 13.704, 13.315,
                12.726, 12.627, 12.759, 11.665, 11.151, 11.221]},
            index=pnd.date_range(
                dt(2014, 8, 14), dt(2015, 1, 14), freq=td(days=1))
        )
        self.serie = Serie(
            datval=self.serie,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )

    @mock.patch('pyspc.core.serie.Serie.events_basic')
    @mock.patch('pyspc.core.serie.Serie.events_scipy')
    def test_events(self, mock_scipy, mock_basic):
        """
        Test méthode events
        """
        # =====================================================================

        # =====================================================================
        self.serie.events(threshold=100, filename='filename.png')
        mock_basic.assert_called_with(threshold=100, filename='filename.png')
        mock_scipy.assert_not_called()
        mock_basic.reset_mock()
        mock_scipy.reset_mock()
        # =====================================================================
        self.serie.events(threshold=100, engine='basic')
        mock_basic.assert_called_with(threshold=100, filename=None)
        mock_scipy.assert_not_called()
        mock_basic.reset_mock()
        mock_scipy.reset_mock()
        # =====================================================================
        self.serie.events(
            engine='scipy',
            threshold=50,
            prominence=20,
            width=2,
            before=td(days=-7),
            after=td(days=7),
            filename='filename.png'
        )
        mock_scipy.assert_called_with(
            threshold=50,
            prominence=20,
            width=2,
            before=td(days=-7),
            after=td(days=7),
            filename='filename.png'
        )
        mock_basic.assert_not_called()
        # =====================================================================

    def test_basic(self):
        """
        Test méthode events_basic
        """
        # =====================================================================
        valid_events = {
            '20141013': {
                'name': '20141013',
                'max': 129.002,
                'maxdt': dt(2014, 10, 13),
                'firstdt': dt(2014, 10, 12),
                'lastdt': dt(2014, 10, 15),
                'length': 4
            },
            '20141104': {
                'name': '20141104',
                'max': 182.574,
                'maxdt': dt(2014, 11, 4),
                'firstdt': dt(2014, 11, 4),
                'lastdt': dt(2014, 11, 6),
                'length': 3
            },
            '20141115': {
                'name': '20141115',
                'max': 114.373,
                'maxdt': dt(2014, 11, 15),
                'firstdt': dt(2014, 11, 14),
                'lastdt': dt(2014, 11, 16),
                'length': 3
            },
            '20141128': {
                'name': '20141128',
                'max': 61.387,
                'maxdt': dt(2014, 11, 28),
                'firstdt': dt(2014, 11, 28),
                'lastdt': dt(2014, 11, 29),
                'length': 2
            },
        }
        # =====================================================================
        fig_filename = os.path.join(
            self.dirname, '{}_{}_basic.png'.format(self.code, self.varname))
        tmpfile = os.path.join('data', os.path.basename(fig_filename))
        events = self.serie.events_basic(threshold=50, filename=tmpfile)
        # =====================================================================
        for k, v in valid_events.items():
            self.assertIn(k, events.keys())
            for k2, v2 in v.items():
                self.assertIn(k2, events[k].keys())
                if k2 == 'max':
                    self.assertAlmostEqual(v2, events[k][k2])
                else:
                    self.assertEqual(v2, events[k][k2])
        self.assertTrue(filecmp.cmp(
            fig_filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_scipy(self):
        """
        Test méthode events_scipy
        """
        # =====================================================================
        valid_events = {
            '20141013': {
                'name': '20141013',
                'max': 129.002,
                'maxdt': dt(2014, 10, 13),
                'firstdt': dt(2014, 10, 8),
                'lastdt': dt(2014, 11, 3),
                'length': 27
            },
            '20141104': {
                'name': '20141104',
                'max': 182.574,
                'maxdt': dt(2014, 11, 4),
                'firstdt': dt(2014, 11, 3),
                'lastdt': dt(2014, 11, 11),
                'length': 9
            },
            '20141115': {
                'name': '20141115',
                'max': 114.373,
                'maxdt': dt(2014, 11, 15),
                'firstdt': dt(2014, 11, 11),
                'lastdt': dt(2014, 11, 24),
                'length': 14
            },
            '20141128': {
                'name': '20141128',
                'max': 61.387,
                'maxdt': dt(2014, 11, 28),
                'firstdt': dt(2014, 11, 24),
                'lastdt': dt(2015, 1, 14),
                'length': 52
            },
        }
        # =====================================================================
        fig_filename = os.path.join(
            self.dirname, '{}_{}_scipy.png'.format(self.code, self.varname))
        tmpfile = os.path.join('data', os.path.basename(fig_filename))
        events = self.serie.events_scipy(
            threshold=50,
            prominence=20,
            width=2,
            filename=tmpfile
        )
        # =====================================================================
        for k, v in valid_events.items():
            self.assertIn(k, events.keys())
            for k2, v2 in v.items():
                self.assertIn(k2, events[k].keys())
                if k2 == 'max':
                    self.assertAlmostEqual(v2, events[k][k2])
                else:
                    self.assertEqual(v2, events[k][k2])
        self.assertTrue(filecmp.cmp(
            fig_filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_scipy_beforeafter(self):
        """
        Test méthode events_scipy
        """
        # =====================================================================
        valid_events = {
            '20141013': {
                'name': '20141013',
                'max': 129.002,
                'maxdt': dt(2014, 10, 13),
                'firstdt': dt(2014, 10, 8),
                'lastdt': dt(2014, 10, 20),
                'length': 13
            },
            '20141104': {
                'name': '20141104',
                'max': 182.574,
                'maxdt': dt(2014, 11, 4),
                'firstdt': dt(2014, 11, 3),
                'lastdt': dt(2014, 11, 11),
                'length': 9
            },
            '20141115': {
                'name': '20141115',
                'max': 114.373,
                'maxdt': dt(2014, 11, 15),
                'firstdt': dt(2014, 11, 11),
                'lastdt': dt(2014, 11, 22),
                'length': 12
            },
            '20141128': {
                'name': '20141128',
                'max': 61.387,
                'maxdt': dt(2014, 11, 28),
                'firstdt': dt(2014, 11, 24),
                'lastdt': dt(2014, 12, 5),
                'length': 12
            },
        }
        # =====================================================================
        fig_filename = os.path.join(
            self.dirname, '{}_{}_scipy-beforeafter.png'.format(
                self.code, self.varname))
        tmpfile = os.path.join('data', os.path.basename(fig_filename))
        events = self.serie.events_scipy(
            threshold=50,
            prominence=20,
            width=2,
            before=td(days=-7),
            after=td(days=7),
            filename=tmpfile
        )
        # =====================================================================
        for k, v in valid_events.items():
            self.assertIn(k, events.keys())
            for k2, v2 in v.items():
                self.assertIn(k2, events[k].keys())
                if k2 == 'max':
                    self.assertAlmostEqual(v2, events[k][k2])
                else:
                    self.assertEqual(v2, events[k][k2])
        self.assertTrue(filecmp.cmp(
            fig_filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
