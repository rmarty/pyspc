#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Config in pyspc.plotting

To run all tests just type:
    python -m unittest test_plotting_regime

To run only a class test:
    python -m unittest test_plotting_regime.Testregime

To run only a specific test:
    python -m unittest test_plotting_regime.Testregime.test_plot

"""
# Imports
import filecmp
import os.path
import pandas as pnd
import unittest

from pyspc.plotting.regime import plot_regime


class Testregime(unittest.TestCase):
    """
    Colormap class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'plotting', 'regime')
        self.data = pnd.DataFrame(
            {('MASTATION', 'TJ', 'amin'): [
                -10.987349, -20.927502, -28.532033, -28.076151, -36.541390,
                -29.483204, -24.401203, -16.569580, -8.038793, -2.843159,
                0.709528, -2.465142],
             ('MASTATION', 'TJ', 'mean'): [
                9.775277, 7.647808, 2.447466, -2.284140, -7.150944, -9.808500,
                -9.630296, -6.969922, -2.551329, 2.679328, 6.937878, 9.774600],
             ('MASTATION', 'TJ', 'amax'): [
                30.366130, 31.096103, 29.276437, 22.403386, 18.317250,
                8.399533, 5.242768, 0.025375, 2.903838, 8.982568, 15.738727,
                22.267054],
             ('MASTATION', 'TJ', 'q10'): [
                2.175369, -2.161437, -9.260649, -13.540367, -17.218375,
                -17.139951, -15.005319, -10.138419, -4.974818, 0.252474,
                3.912094, 5.040022],
             ('MASTATION', 'TJ', 'q25'): [
                5.483506, 2.162670, -3.558794, -8.314738, -12.848633,
                -13.776696, -12.217287, -8.672136, -3.823146, 1.242906,
                5.228819, 7.199803],
             ('MASTATION', 'TJ', 'q50'): [
                9.881925, 7.702300, 2.684536, -2.430664, -7.027587, -9.963173,
                -9.512707, -6.874644, -2.513706, 2.467920, 6.599372, 9.765345],
             ('MASTATION', 'TJ', 'q75'): [
                13.756946, 13.467442, 8.463805, 3.609519, -1.690938, -5.798924,
                -7.089044, -5.273049, -1.179915, 3.998118, 8.564590, 12.172330
                ],
             ('MASTATION', 'TJ', 'q90'): [
                17.887767, 17.929788, 14.078995, 9.348240, 3.763987, -2.059937,
                -4.420848, -3.783082, -0.131121, 5.349232, 10.420266,
                14.699299],
             }, index=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        )
        self.data.index.name = 'month'

    def test_plot(self):
        """
        Test création figure regime
        """
        # =====================================================================
        f = os.path.join('data', 'regime.png')
        tmp_file = plot_regime(data=self.data, fig_filename=f)
        self.assertTrue(tmp_file, f)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(tmp_file)),
            tmp_file
        ))
        os.remove(f)
        # =====================================================================

    def test_plot_fill(self):
        """
        Test création figure regime fill
        """
        # =====================================================================
        f = os.path.join('data', 'regime_fill.png')
        tmp_file = plot_regime(data=self.data, fig_filename=f, fill=True)
        self.assertTrue(tmp_file, f)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(tmp_file)),
            tmp_file
        ))
        os.remove(f)
        # =====================================================================

    def test_plot_boxplot(self):
        """
        Test création figure regime boxplot
        """
        # =====================================================================
        f = os.path.join('data', 'regime_boxplot.png')
        tmp_file = plot_regime(data=self.data, fig_filename=f, boxplot=True)
        self.assertTrue(tmp_file, f)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(tmp_file)),
            tmp_file
        ))
        os.remove(f)
        # =====================================================================

    def test_plot_boxplot_fill(self):
        """
        Test création figure regime boxplot_fill
        """
        # =====================================================================
        f = os.path.join('data', 'regime_boxplot_fill.png')
        tmp_file = plot_regime(data=self.data, fig_filename=f, boxplot=True,
                               fill=True)
        self.assertTrue(tmp_file, f)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(tmp_file)),
            tmp_file
        ))
        os.remove(f)
        # =====================================================================
