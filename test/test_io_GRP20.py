#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_GRP20 in pyspc.io.grp20

To run all tests just type:
    python -m unittest test_io_GRP20

To run only a class test:
    python -m unittest test_io_GRP20.TestGRP20

To run only a specific test:
    python -m unittest test_io_GRP20.TestGRP20.test_init

"""
# Imports
from datetime import datetime as dt
import os
import unittest

from pyspc.core.series import Series
from pyspc.io.grp20 import read_GRP20


class TestGRP20(unittest.TestCase):
    """
    GRP20_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname_cal = os.path.join('data', 'model', 'grp20', 'cal')
        self.dirname_rt = os.path.join('data', 'model', 'grp20', 'rt')

    def test_cal_data(self):
        """
        CALAGE - Cas de données d'observation
        """
        # =====================================================================
        datatype = 'grp20_cal_data'
        valid = {
            ('90065003', 'PH', None): os.path.join(
                self.dirname_cal, '90065003_P_00J01H00M.txt'),
            ('RH10585x', 'EJ', None): os.path.join(
                self.dirname_cal, 'RH10585x_E_01J00H00M.txt'),
            ('RH10585x', 'QI', None): os.path.join(
                self.dirname_cal, 'RH10585x_Q.txt'),
            ('90035001', 'TI', None): os.path.join(
                self.dirname_cal, '90035001_T.txt')
        }
        for k, v in valid.items():
            series = read_GRP20(filename=v, datatype=datatype)
            self.assertIsInstance(series, Series)
            self.assertEqual(len(series), 1)
            self.assertIn(k, series)
        # =====================================================================
        series = read_GRP20(
            datatype=datatype, dirname=self.dirname_cal,
            stations=['RH10585x', '90065003', '90035001'],
            varnames=['PH', 'EJ', 'QI', 'TI'])
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 4)
        for k in valid.keys():
            self.assertIn(k, series)
        # =====================================================================

    def test_cal_event(self):
        """
        CALAGE - Cas de données d'événements
        """
        # =====================================================================
        datatype = 'grp20_cal_event'
        # =====================================================================
        filename = os.path.join(self.dirname_cal, 'RH10585x-EV0001.DAT')
        series = read_GRP20(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(('RH10585x', 'PH', 'EV0001'), series)
        self.assertIn(('RH10585x', 'QH', 'EV0001'), series)
        # =====================================================================
        filename = os.path.join(self.dirname_cal, 'RH10585x-EV0001jour.DAT')
        series = read_GRP20(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(('RH10585x', 'PJ', 'EV0001jour'), series)
        self.assertIn(('RH10585x', 'QJ', 'EV0001jour'), series)
        # =====================================================================

    def test_cal_fcst(self):
        """
        CALAGE - Cas de données de prévision
        """
        # =====================================================================
        datatype = 'grp20_cal_fcst'
        filename = os.path.join(
            self.dirname_cal,
            'H_RH10585x_GRP_SMN_TAN_PDT_00J01H00M_HOR_00J03H00M_'
            'Scal_5d00_PP_P0P0.TXT')
        series = read_GRP20(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        cols = ['OBS00J00H00M', 'OBS00J01H00M', 'PRV00J01H00M', 'OBS00J03H00M',
                'PRV00J03H00M', 'OBS00J06H00M', 'PRV00J06H00M', 'OBS00J09H00M',
                'PRV00J09H00M', 'OBS00J12H00M', 'PRV00J12H00M', 'OBS00J18H00M',
                'PRV00J18H00M', 'OBS01J00H00M', 'PRV01J00H00M', 'OBS01J06H00M',
                'PRV01J06H00M', 'OBS01J12H00M', 'PRV01J12H00M', 'OBS01J18H00M',
                'PRV01J18H00M', 'OBS02J00H00M', 'PRV02J00H00M', 'OBS02J06H00M',
                'PRV02J06H00M', 'OBS02J12H00M', 'PRV02J12H00M', 'OBS02J18H00M',
                'PRV02J18H00M', 'OBS03J00H00M', 'PRV03J00H00M', 'OBS05J00H00M',
                'PRV05J00H00M']
        self.assertEqual(len(series), len(cols))
        for x in cols:
            self.assertIn(('RH10585x', 'QH', x), series)
        # =====================================================================

    def test_rt_archive(self):
        """
        TEMPS-REEL - Cas de données d'archive
        """
        # =====================================================================
        datatype = 'grp20_rt_archive'
        filename = os.path.join(self.dirname_rt, 'PV_00J01H00M_2007.DAT')
        series = read_GRP20(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('90065003', 'PH', None), series)
        # =====================================================================
        filename = os.path.join(self.dirname_rt, 'QV_2007.DAT')
        series = read_GRP20(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('RH10585x', 'QI', None), series)
        # =====================================================================

    def test_rt_data(self):
        """
        TEMPS-REEL - Cas de données d'observation
        """
        # =====================================================================
        datatype = 'grp20_rt_data'
        filename = os.path.join(self.dirname_rt, 'Debit.txt')
        series = read_GRP20(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('RH10585x', 'QI', None), series)
        # =====================================================================
        filename = os.path.join(self.dirname_rt, 'Pluie_00J01H00M.txt')
        series = read_GRP20(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(('90052002', 'PH', None), series)
        self.assertIn(('90065003', 'PH', None), series)
        # =====================================================================

    def test_rt_metscen(self):
        """
        TEMPS-REEL - Cas de scénarios météorologiques
        """
        # =====================================================================
        datatype = 'grp20_rt_metscen'
        filename = os.path.join(
            self.dirname_rt, 'Scen_0001_PluSY_00J01H00M.txt')
        series = read_GRP20(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(
            ('RH10585x', 'PH', (dt(2021, 5, 31, 1), 'grp20', '0001', None)),
            series)
        # =====================================================================

    def test_rt_fcst(self):
        """
        TEMPS-REEL - Cas d'export d'observation
        TEMPS-REEL - Cas de prévision sans assimilation
        TEMPS-REEL - Cas de prévision avec assimilation
        """
        # =====================================================================
        datatype = 'grp20_rt_obs_diff'
        filename = os.path.join(self.dirname_rt, 'GRP_D_Obs.txt')
        series = read_GRP20(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 4)
        self.assertIn(('RH10585x', 'QH', 'grp20'), series)
        self.assertIn(('RH10585x', 'PH', 'grp20'), series)
        self.assertIn(('RH10585x', 'QJ', 'grp20'), series)
        self.assertIn(('RH10585x', 'PJ', 'grp20'), series)
        # =====================================================================
        datatype = 'grp20_rt_sim_diff'
        filename = os.path.join(self.dirname_rt, 'GRP_D_Simu_2001.txt')
        series = read_GRP20(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 4)
        self.assertIn(
            ('RH10585x', 'QH',
             (dt(2007, 1, 19), 'grp20-sim', '2001', None)), series)
        self.assertIn(
            ('RH10585x', 'PH',
             (dt(2007, 1, 19), 'grp20-sim', '2001', None)), series)
        self.assertIn(
            ('RH10585x', 'QJ',
             (dt(2007, 1, 19), 'grp20-sim', '2001', None)), series)
        self.assertIn(
            ('RH10585x', 'PJ',
             (dt(2007, 1, 19), 'grp20-sim', '2001', None)), series)
        # =====================================================================
        datatype = 'grp20_rt_fcst_diff'
        filename = os.path.join(self.dirname_rt, 'GRP_D_Prev_2001.txt')
        series = read_GRP20(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 4)
        self.assertIn(
            ('RH10585x', 'QH',
             (dt(2007, 1, 19), 'grp20', '2001', None)), series)
        self.assertIn(
            ('RH10585x', 'PH',
             (dt(2007, 1, 19), 'grp20', '2001', None)), series)
        self.assertIn(
            ('RH10585x', 'QJ',
             (dt(2007, 1, 19), 'grp20', '2001', None)), series)
        self.assertIn(
            ('RH10585x', 'PJ',
             (dt(2007, 1, 19), 'grp20', '2001', None)), series)
        # =====================================================================

    def test_rt_intern(self):
        """
        TEMPS-REEL - Cas de données internes au modèle
        """
        # =====================================================================
        datatype = 'grp20_rt_intern_diff'
        filename = os.path.join(
            self.dirname_rt, 'intern', 'PQE_1A_D.DAT')
        series = read_GRP20(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 4)
        self.assertIn(('RH10585x', 'QH', None), series)
        self.assertIn(('RH10585x', 'QH', 'grp20'), series)
        self.assertIn(('RH10585x', 'PH', None), series)
        self.assertIn(('RH10585x', 'EH', None), series)
        # =====================================================================
        filename = os.path.join(
            self.dirname_rt, 'intern', 'PQE_1A_D_journalier.DAT')
        series = read_GRP20(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 4)
        self.assertIn(('RH10585x', 'QJ', None), series)
        self.assertIn(('RH10585x', 'QJ', 'grp20'), series)
        self.assertIn(('RH10585x', 'PJ', None), series)
        self.assertIn(('RH10585x', 'EJ', None), series)
        # =====================================================================
