#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Sandre in pyspc.data.sandre

To run all tests just type:
    python -m unittest test_data_Sandre

To run only a class test:
    python -m unittest test_data_Sandre.TestSandre

To run only a specific test:
    python -m unittest test_data_Sandre.TestSandre.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import filecmp
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal, assert_series_equal
import unittest

from pyspc.data.sandre import Sandre


class TestSandre(unittest.TestCase):
    """
    Sandre class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'sandre')
        self.vigicrues = os.path.join('data', 'webservice', 'vigicrues')

    def test_init(self):
        """
        Test des méthodes de lecture
        """
        # =====================================================================
        filename = os.path.join(
            self.vigicrues, 'vigicrues_obs_202101291400.xml')
        sandre = Sandre(filename=filename)
        self.assertEqual(sandre.filename, filename)
        # =====================================================================

    def test_concat(self):
        """
        Test de la méthode de concaténation de fichier
        """
        # =====================================================================
        filenames = [os.path.join(self.dirname, x)
                     for x in ['dataobs_hydro_Q.xml',
                               'dataobs_hydro_H.xml',
                               'dataobs_meteo.xml']]
        tmp_file = os.path.join('data', "test_concat.xml")
        sender = 'me'
        user = 'org'
        target = 'you'
        runtime = dt(2023, 8, 30)
        # =====================================================================
        tmp_file = Sandre.concat(
            filename=tmp_file, filenames=filenames, runtime=runtime,
            sender=sender, user=user, target=target)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(tmp_file)),
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_read_dataobs_hydro_Q(self):
        """
        Test des méthodes de lecture - dataobs_hydro Q
        """
        # =====================================================================
        valid = {
            ('K5183020', 'Q'): pnd.DataFrame(
                {
                    'res': [45267.5, 47105.4, 48497.3, 50470.6, 50964.0,
                            51950.6, 52444.0, 52444.0, 52937.3, 53430.6,
                            53924.0, 54417.3, 54417.3, 55404.0, 56390.6,
                            57377.3, 58364.0, 59350.6, 60540.0, 60540.0,
                            60540.0, 60540.0, 60540.0, 59350.6, 58857.3,
                            58364.0, 57870.6, 56884.0, 56390.6, 55404.0,
                            54417.3, 53430.6, 52937.3, 50470.6, 52444.0,
                            50964.0, 49484.0, 48990.6, 48497.3, 48024.3,
                            47105.4, 46186.4, 45727.0, 44808.1, 44348.6,
                            43429.7, 42510.8],
                    'mth': [8]*47,
                    'qal': [16]*47,
                    'cnt': [0]*47,
                    'statut': [4]*47,
                },
                index=pnd.date_range(
                    dt(2016, 4, 17, 1), dt(2016, 4, 18, 23), freq='H')
            )
        }
        valid_toconcat = {
            ('K5183020', 'Q'): []
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'dataobs_hydro_Q.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        for s in content.serieshydro:
            self.assertIn((s.entite.code, s.grandeur), valid)
            self.assertIn((s.entite.code, s.grandeur), valid_toconcat)
            valid_toconcat[(s.entite.code, s.grandeur)].append(s.observations)
        for k in valid_toconcat:
            df = pnd.concat(valid_toconcat[k])
            v = valid[k]
            v.index.name = 'dte'
            v = v.astype({'mth': 'int8', 'qal': 'int8', 'cnt': 'int8',
                          'statut': 'int8'})
            assert_frame_equal(df, v, check_less_precise=2)
        # =====================================================================

    def test_read_dataobs_hydro_Q_elab(self):
        """
        Test des méthodes de lecture - dataobs_hydro Q - données élaborées
        """
        # =====================================================================
        valid = {
            ('K0550010', 'QmnJ'): pnd.DataFrame(
                {
                    'res': [11100.0, 11600.0, 9600.0, 11600.0, 12200.0,
                            12700.0, 10600.0, 10600.0, 9600.0, 8650.0, 8650.0,
                            7750.0, 7750.0, 7750.0, 5750.0, 770000.0, 392000.0,
                            120000.0, 81000.0, 56500.0, 47400.0, 41500.0,
                            34500.0, 30900.0, 84500.0, 26200.0, 24400.0,
                            23800.0, 23200.0, 23200.0, 22600],
                    'mth': [12]*31,
                    'qal': [20]*31,
                    'cnt': [0]*31,
                    'statut': [16]*31,
                },
                index=pnd.date_range(
                    dt(1980, 9, 6), dt(1980, 10, 6), freq='D')
            )
        }
        valid_toconcat = {
            ('K0550010', 'QmnJ'): []
        }
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'K0550010_198009060000_198010060000_Q_obs.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        for s in content.seriesobselab:
            self.assertIn((s.entite.code, s.typegrd), valid)
            self.assertIn((s.entite.code, s.typegrd), valid_toconcat)
            valid_toconcat[(s.entite.code, s.typegrd)].append(s.observations)
        for k in valid_toconcat:
            df = pnd.concat(valid_toconcat[k])
            v = valid[k]
            v.index.name = 'dte'
            v = v.astype({'mth': 'int8', 'qal': 'int8', 'cnt': 'int8',
                          'statut': 'int8'})
            assert_frame_equal(df, v, check_less_precise=2)
        # =====================================================================

    def test_read_dataobs_hydro_H(self):
        """
        Test des méthodes de lecture - dataobs_hydro H
        """
        # =====================================================================
        valid = {
            ('K518302001', 'H'): pnd.DataFrame(
                {
                    'res': [410.0, 450.0, 480.0, 520.0, 530.0, 550.0, 560.0,
                            560.0, 570.0, 580.0, 590.0, 600.0, 600.0, 620.0,
                            640.0, 660.0, 680.0, 700.0, 720.0, 720.0, 720.0,
                            720.0, 720.0, 700.0, 690.0, 680.0, 670.0, 650.0,
                            640.0, 620.0, 600.0, 580.0, 570.0, 520.0, 560.0,
                            530.0, 500.0, 490.0, 480.0, 470.0, 450.0, 430.0,
                            420.0, 400.0, 390.0, 370.0, 350.0],
                    'mth': [0]*47,
                    'qal': [16]*47,
                    'cnt': [0]*47,
                    'statut': [4]*47,
                },
                index=pnd.date_range(
                    dt(2016, 4, 17, 1), dt(2016, 4, 18, 23), freq='H')
            )
        }
        valid_toconcat = {
            ('K518302001', 'H'): []
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'dataobs_hydro_H.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        for s in content.serieshydro:
            self.assertIn((s.entite.code, s.grandeur), valid)
            self.assertIn((s.entite.code, s.grandeur), valid_toconcat)
            valid_toconcat[(s.entite.code, s.grandeur)].append(s.observations)
        for k in valid_toconcat:
            df = pnd.concat(valid_toconcat[k])
            v = valid[k]
            v.index.name = 'dte'
            v = v.astype({'mth': 'int8', 'qal': 'int8', 'cnt': 'int8',
                          'statut': 'int8'})
            assert_frame_equal(df, v, check_less_precise=2)
        # =====================================================================

    def test_read_datafcst_hydro(self):
        """
        Test des méthodes de lecture - datafcst_hydro
        """
        # =====================================================================
        valid = {
            ('K1930010', 'Q', dt(2016, 4, 20, 7), '45hEAOtt00'): pnd.Series(
                {
                    (dt(2016, 4, 20, 6), 'moy'): 657905.029297,
                    (dt(2016, 4, 20, 6), 'min'): 651244.995117,
                    (dt(2016, 4, 20, 6), 'max'): 661364.013672,
                    (dt(2016, 4, 20, 7), 'moy'): 654122.985840,
                    (dt(2016, 4, 20, 7), 'min'): 647578.979492,
                    (dt(2016, 4, 20, 7), 'max'): 657557.983398,
                    (dt(2016, 4, 20, 8), 'moy'): 648411.010742,
                    (dt(2016, 4, 20, 8), 'min'): 637543.029785,
                    (dt(2016, 4, 20, 8), 'max'): 654010.009766,
                    (dt(2016, 4, 20, 9), 'moy'): 644525.024414,
                    (dt(2016, 4, 20, 9), 'min'): 629979.980469,
                    (dt(2016, 4, 20, 9), 'max'): 652531.005859,
                    (dt(2016, 4, 20, 10), 'moy'): 636773.986816,
                    (dt(2016, 4, 20, 10), 'min'): 619005.004883,
                    (dt(2016, 4, 20, 10), 'max'): 646870.971680,
                    (dt(2016, 4, 20, 11), 'moy'): 634590.026855,
                    (dt(2016, 4, 20, 11), 'min'): 614572.021484,
                    (dt(2016, 4, 20, 11), 'max'): 646533.020020,
                    (dt(2016, 4, 20, 12), 'moy'): 621318.969727,
                    (dt(2016, 4, 20, 12), 'min'): 600044.982910,
                    (dt(2016, 4, 20, 12), 'max'): 634541.992188,
                },
                index=None,
                name='res'
            )
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'datafcst_hydro.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        for s in content.simulations:
            k = (s.entite.code, s.grandeur, s.dtprod, s.modeleprevision.code)
            self.assertIn(k, valid)
            v = valid[k]
            v.index.names = ['dte', 'tend']
            assert_series_equal(s.previsions_tend, v)
        # =====================================================================

    def test_read_datafcst_hydro_comsimul(self):
        """
        Test des méthodes de lecture - datafcst_hydro - comsimul
        """
        # =====================================================================
        valid = {
            ('Y2100020', 'Q', dt(2019, 11, 19, 6), '11gGRPd130', 'ctl'):
                pnd.Series({
                    (dt(2019, 11, 22, 0), 'moy'): 1296.497,
                    (dt(2019, 11, 22, 1), 'moy'): 1305.520,
                    (dt(2019, 11, 22, 2), 'moy'): 1314.609,
                    (dt(2019, 11, 22, 3), 'moy'): 1323.763,
                    (dt(2019, 11, 22, 4), 'moy'): 1332.980,
                    (dt(2019, 11, 22, 5), 'moy'): 1342.261,
                    (dt(2019, 11, 22, 6), 'moy'): 1351.603,
                },
                index=None,
                name='res'
            )
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'spcmo.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        for s in content.simulations:
            comment = sandre.process_comsim(s.commentaire)
            k = (s.entite.code, s.grandeur, comment.runtime,
                 s.modeleprevision.code, comment.code)
            if k in valid:
                v = valid[k]
                v.index.names = ['dte', 'tend']
                assert_series_equal(s.previsions_tend, v)
        # =====================================================================

    def test_read_dataobs_meteo(self):
        """
        Test des méthodes de lecture - dataobs_meteo
        """
        # =====================================================================
        valid = {
            ('023209001', 'RR'): pnd.DataFrame(
                {
                    'res': [0.0, 0.0, 0.0, 4.0, 8.0, 8.0, 2.0, 4.0, 2.0, 2.0,
                            38.0, 10.0, 24.0, 12.0, 2.0, 2.0, 0.0, 8.0, 14.0],
                    'mth': [0]*19,
                    'qal': [16]*19,
                    'qua': [100.0]*19,
                    'ctxt': [0]*19,
                    'statut': [4]*19,
                },
                index=pnd.date_range(
                    dt(2016, 4, 15, 12), dt(2016, 4, 16, 6), freq='H')
            )
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'dataobs_meteo.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        for s in content.seriesmeteo:
            self.assertIn(
                (s.grandeur.sitemeteo.code, s.grandeur.typemesure), valid)
            v = valid[(s.grandeur.sitemeteo.code, s.grandeur.typemesure)]
            v.index.name = 'dte'
            v = v.astype({'mth': 'int8', 'qal': 'int8', 'ctxt': 'int8',
                          'statut': 'int8'})
            assert_frame_equal(s.observations, v)
        # =====================================================================

    def test_read_levelcor(self):
        """
        Test des méthodes de lecture - levelcor
        """
        # =====================================================================
        valid = {
            'K055001010': [
                (dt(2014, 4, 23, 13, 40), 0.0),
                (dt(2014, 5, 22, 8, 20), -30.0),
                (dt(2014, 6, 5, 8, 35), -30.0),
                (dt(2014, 6, 18, 6, 20), -30.0),
                (dt(2014, 7, 1, 9, 50), -30.0),
                (dt(2014, 7, 6, 20), -30.0),
                (dt(2014, 7, 9, 8), 0.0),
                (dt(2014, 8, 30), 0.0),
            ]
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'levelcor.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        for s in content.courbescorrection:
            self.assertIn(s.station.code, valid)
            levelcor = [(p.dte, p.deltah) for p in s.pivots]
            self.assertEqual(levelcor, valid[s.station.code])
        # =====================================================================

#    def test_read_flowmes(self):
#        """
#        Test des méthodes de lecture - flowmes
#        """

    def test_read_vigicrues(self):
        """
        Test des méthodes de lecture - vigicrues
        """
        # =====================================================================
        valid = {
            ('K055001010', 'Q'): pnd.DataFrame(
                {
                    'res': [51000.0, 50700.0, 50600.0, 50600.0, 50400.0,
                            50700.0, 50300.0, 50700.0, 49400.0, 48300.0,
                            48800.0, 52800.0, 54100.0, 54100.0, 54100.0,
                            54100.0, 54100.0, 54100.0, 54100.0, 54100.0,
                            54100.0, 54100.0, 54100.0, 54100.0, 54100.0,
                            54100.0, 54100.0, 54100.0, 54100.0, 54100.0,
                            54100.0, 54100.0, 54100.0, 54100.0, 54100.0],
                    'mth': [0]*35,
                    'qal': [16]*35,
                    'cnt': [0]*35,
                    'statut': [4]*35,
                },
                index=pnd.date_range(
                    dt(2021, 1, 27, 12), dt(2021, 1, 27, 23), freq='H').union(
                        pnd.date_range(dt(2021, 1, 28), dt(2021, 1, 28, 0, 35),
                                       freq='5Min')).union(
                            pnd.date_range(
                                dt(2021, 1, 28, 0, 50), dt(2021, 1, 28, 2),
                                freq='5Min'))
            )
        }
        # =====================================================================
        filename = os.path.join(
            self.vigicrues, 'vigicrues_obs_202101291400.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        for s in content.serieshydro:
            self.assertIn((s.entite.code, s.grandeur), valid)
            v = valid[(s.entite.code, s.grandeur)]
            v.index.name = 'dte'
            v = v.astype({'mth': 'int8', 'qal': 'int8', 'cnt': 'int8',
                          'statut': 'int8'})
            assert_frame_equal(s.observations, v)
        # =====================================================================

    def test_write_datafcst_hydro_comsimul(self):
        """
        Test des méthodes d'écriture - data_fcst_hydro - comsimul
        """
        # =====================================================================
        datatype = 'data_fcst_hydro'
        sender = 'me'
        user = 'org'
        target = 'you'
        runtime = dt(2020, 2, 3, 12)
        # =====================================================================
        key1 = ('A6701210', 'Q', runtime, '57gGRPd000', '2001', 'moy')
        key2 = ('A6701210', 'Q', runtime, '57gGRPd000', '2001', 'min')
        key3 = ('A6701210', 'Q', runtime, '57gGRPd000', '2001', 'max')
        key4 = ('A6701210', 'Q', runtime, '57gGRPd000', '2002', 'moy')
        key5 = ('A6701210', 'Q', runtime, '57gGRPd999', '', 'moy')
        data = pnd.DataFrame({
            key1: [41.689, 43.426, 44.285, 44.395, 44.109, 43.340, 42.448,
                   41.470, 40.312, 39.044, 37.754, 36.612, 36.184, 36.708,
                   38.304, 40.523, 43.099, 45.781, 48.223, 50.064, 50.648,
                   50.161, 48.963, 47.745],
            key2: [41.614, 43.161, 43.699, 43.438, 42.775, 41.659, 40.459,
                   39.230, 37.941, 36.614, 35.292, 34.060, 33.290, 33.133,
                   33.660, 34.575, 35.706, 36.908, 37.971, 38.681, 38.654,
                   38.023, 37.008, 36.014],
            key3: [41.784, 43.760, 45.025, 45.649, 45.949, 45.793, 45.491,
                   45.023, 44.198, 43.123, 41.936, 40.888, 40.909, 42.427,
                   45.723, 50.083, 55.065, 60.224, 64.957, 68.613, 70.013,
                   69.503, 67.750, 66.030],
            key4: [41.689, 43.426, 44.285, 44.395, 44.109, 43.340, 42.448,
                   41.470, 40.312, 39.044, 37.754, 36.612, 36.184, 36.708,
                   38.304, 40.523, 43.099, 45.781, 48.223, 50.064, 50.648,
                   50.161, 48.963, 47.745],
            key5: [41.689, 43.426, 44.285, 44.395, 44.109, 43.340, 42.448,
                   41.470, 40.312, 39.044, 37.754, 36.612, 36.184, 36.708,
                   38.304, 40.523, 43.099, 45.781, 48.223, 50.064, 50.648,
                   50.161, 48.963, 47.745],
            },
            index=pnd.date_range(
                dt(2020, 2, 3, 13), dt(2020, 2, 4, 12), freq='H')
        )
        data.columns.set_names(
            ['Location', 'Varname', 'Runtime', 'Model', 'Scenario', 'Prob'],
            level=[0, 1, 2, 3, 4, 5],
            inplace=True
        )
        # =====================================================================
        tmp_file = os.path.join('data', 'A6701210_2020020312_57gGRPd000.xml')
        writer = Sandre(filename=tmp_file)
        writer.write(
            data=data, datatype=datatype,
            sender=sender, user=user, target=target, runtime=runtime
        )
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(tmp_file)),
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_write_dataobs_hydro(self):
        """
        Test des méthodes d'écriture - data_obs_hydro
        """
        # =====================================================================
        datatype = 'data_obs_hydro'
        sender = 'me'
        user = 'org'
        target = 'you'
        runtime = dt(2016, 4, 19)
        # =====================================================================
        data = pnd.DataFrame({
            ('K5183020', 'Q'): [
                45267.5, 47105.4, 48497.3, 50470.6, 50964.0,
                51950.6, 52444.0, 52444.0, 52937.3, 53430.6,
                53924.0, 54417.3, 54417.3, 55404.0, 56390.6,
                57377.3, 58364.0, 59350.6, 60540.0, 60540.0,
                60540.0, 60540.0, 60540.0, 59350.6, 58857.3,
                58364.0, 57870.6, 56884.0, 56390.6, 55404.0,
                54417.3, 53430.6, 52937.3, 50470.6, 52444.0,
                50964.0, 49484.0, 48990.6, 48497.3, 48024.3,
                47105.4, 46186.4, 45727.0, 44808.1, 44348.6,
                43429.7, 42510.8],
            ('K518302001', 'H'): [
                410.0, 450.0, 480.0, 520.0, 530.0, 550.0, 560.0,
                560.0, 570.0, 580.0, 590.0, 600.0, 600.0, 620.0,
                640.0, 660.0, 680.0, 700.0, 720.0, 720.0, 720.0,
                720.0, 720.0, 700.0, 690.0, 680.0, 670.0, 650.0,
                640.0, 620.0, 600.0, 580.0, 570.0, 520.0, 560.0,
                530.0, 500.0, 490.0, 480.0, 470.0, 450.0, 430.0,
                420.0, 400.0, 390.0, 370.0, 350.0],
            }, index=pnd.date_range(
                dt(2016, 4, 17, 1), dt(2016, 4, 18, 23), freq='H')
        )
        data.columns.set_names(
            ['Location', 'Varname'], level=[0, 1], inplace=True)
        # =====================================================================
        tmp_file = os.path.join('data', 'test_write_HQ.xml')
        writer = Sandre(filename=tmp_file)
        writer.write(
            data=data, datatype=datatype,
            timedelta=td(hours=1),
            sender=sender, user=user, target=target, runtime=runtime
        )
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(tmp_file)),
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_write_dataobs_meteo(self):
        """
        Test des méthodes d'écriture - data_obs_meteo
        """
        # =====================================================================
        datatype = 'data_obs_meteo'
        sender = 'me'
        user = 'org'
        target = 'you'
        runtime = dt(2016, 4, 16, 6)
        # =====================================================================
        data = pnd.DataFrame({
            ('23209001', 'RR'): [
                0.0, 0.0, 0.0, 4.0, 8.0, 8.0, 2.0, 4.0, 2.0, 2.0,
                38.0, 10.0, 24.0, 12.0, 2.0, 2.0, 0.0, 8.0, 14.0],
            ('23209002', 'RR'): [
                0.0, 0.0, 0.0, 4.0, 8.0, 8.0, 2.0, 4.0, 2.0, 2.0,
                38.0, 10.0, 24.0, 12.0, 2.0, 2.0, 0.0, 8.0, 14.0],
            }, index=pnd.date_range(
                dt(2016, 4, 15, 12), dt(2016, 4, 16, 6), freq='H')
        )
        data.columns.set_names(
            ['Location', 'Varname'], level=[0, 1], inplace=True)
        # =====================================================================
        tmp_file = os.path.join('data', 'test_write_RR.xml')
        writer = Sandre(filename=tmp_file)
        writer.write(
            data=data, datatype=datatype,
            timedelta=td(hours=1),
            sender=sender, user=user, target=target, runtime=runtime
        )
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(tmp_file)),
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_types(self):
        """
        Test des types de fichier XML
        """
        valid = ['data_fcst_hydro', 'data_obs_hydro', 'data_obs_meteo',
                 'flowmes', 'levelcor']
        self.assertEqual(valid, Sandre.get_types())
