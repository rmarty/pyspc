#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Serie in pyspc.core.serie - scaling

To run all tests just type:
    python -m unittest test_core_Serie_SCALING

To run only a class test:
    python -m unittest test_core_Serie_SCALING.TestSerie

To run only a specific test:
    python -m unittest test_core_Serie_SCALING.TestSerie.test_fvi_lvi

"""
# Imports
from datetime import datetime as dt, timedelta as td
import numpy as np
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest
import warnings

# Imports pyspc
from pyspc.core.serie import Serie


class TestSerie(unittest.TestCase):
    """
    Serie class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        self.code = 'K0000000'
        self.provider = 'SPC'

    def test_downscale_JH(self):
        """
        Test downscale - Journalier vers Horaire
        """
        # =====================================================================
        varname = 'QJ'
        serie = pnd.DataFrame(
            {(self.code, varname): [27.382, 71.262, 26.112]},
            index=pnd.date_range(
                dt(2014, 11, 3),
                dt(2014, 11, 5),
                freq=td(days=1)
            )
        )
        serie = Serie(
            datval=serie,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid = []
        valid.extend([27.382]*24)
        valid.extend([71.262]*24)
        valid.extend([26.112]*24)
        valid_varname = 'QH'
        valid = pnd.DataFrame(
            {(self.code, valid_varname): valid},
            index=pnd.date_range(
                dt(2014, 11, 2, 7),
                dt(2014, 11, 5, 6),
                freq=td(hours=1)
            )
        )
        valid.columns = [(self.code, valid_varname)]
        dn = serie.downscale(toparam=valid_varname, dayhour=6)
        assert_frame_equal(dn.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================
        valid = []
        valid.extend([27.382]*24)
        valid.extend([71.262]*24)
        valid.extend([26.112]*24)
        valid_varname = 'QH'
        valid = pnd.DataFrame(
            {(self.code, valid_varname): valid},
            index=pnd.date_range(
                dt(2014, 11, 2, 1),
                dt(2014, 11, 5, 0),
                freq=td(hours=1)
            )
        )
        valid.columns = [(self.code, valid_varname)]
        dn = serie.downscale(toparam=valid_varname, dayhour=0)
        assert_frame_equal(dn.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================

    def test_downscale_JH_cumule(self):
        """
        Test downscale - Journalier vers Horaire - Version cumulée
        """
        # =====================================================================
        varname = 'PJ'
        serie = pnd.DataFrame(
            {(self.code, varname): [80, np.nan, 25]},
            index=pnd.date_range(
                dt(2014, 11, 3),
                dt(2014, 11, 5),
                freq=td(days=1)
            )
        )
        serie = Serie(
            datval=serie,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid = []
        valid.extend([3.333]*24)
        valid.extend([np.nan]*24)
        valid.extend([1.042]*24)
        valid_varname = 'PH'
        valid = pnd.DataFrame(
            {(self.code, valid_varname): valid},
            index=pnd.date_range(
                dt(2014, 11, 2, 7),
                dt(2014, 11, 5, 6),
                freq=td(hours=1)
            )
        )
        valid.columns = [(self.code, valid_varname)]
        dn = serie.downscale(toparam=valid_varname, dayhour=6)
        assert_frame_equal(dn.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================

    def test_downscale_H15m_cumule(self):
        """
        Test downscale - Horaire vers 15 minutes - Version cumulée
        """
        # =====================================================================
        varname = 'PH'
        serie = pnd.DataFrame(
            {(self.code, varname): [1.2, 4.9, 34.2]},
            index=pnd.date_range(
                dt(2017, 6, 13, 18),
                dt(2017, 6, 13, 20),
                freq=td(hours=1)
            )
        )
        serie = Serie(
            datval=serie,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid = []
        valid.extend([0.300]*4)
        valid.extend([1.225]*4)
        valid.extend([8.550]*4)
        valid_varname = 'P15m'
        valid = pnd.DataFrame(
            {(self.code, valid_varname): valid},
            index=pnd.date_range(
                dt(2017, 6, 13, 17, 15),
                dt(2017, 6, 13, 20),
                freq=td(minutes=15)
            )
        )
        valid.columns = [(self.code, valid_varname)]
        dn = serie.downscale(toparam=valid_varname)
        assert_frame_equal(dn.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================

    def test_nearlyequalscale_56(self):
        """
        Test nearlyequalscale - 5min vers 6min
        """
        varname = 'P5m'
        dtval = [
            [dt(2017, 6, 13, 17, 0), 5.8],
            [dt(2017, 6, 13, 17, 5), 4.6],
            [dt(2017, 6, 13, 17, 10), 1.8],
            [dt(2017, 6, 13, 17, 15), 2.4],
            [dt(2017, 6, 13, 17, 20), 2.2],
            [dt(2017, 6, 13, 17, 25), 9.4],
            [dt(2017, 6, 13, 17, 30), 9.6],
            [dt(2017, 6, 13, 17, 35), 1.2],
            [dt(2017, 6, 13, 17, 40), 4.8],
            [dt(2017, 6, 13, 17, 45), 6.4],
            [dt(2017, 6, 13, 17, 50), 7.8],
            [dt(2017, 6, 13, 17, 55), 8.2],
            [dt(2017, 6, 13, 18, 0), 2.4],
        ]
        # =====================================================================
        valid_varname = 'P6m'
        valid = [
            [dt(2017, 6, 13, 17, 0), 5.800],
            [dt(2017, 6, 13, 17, 6), 4.960],
            [dt(2017, 6, 13, 17, 12), 2.400],
            [dt(2017, 6, 13, 17, 18), 2.760],
            [dt(2017, 6, 13, 17, 24), 8.400],
            [dt(2017, 6, 13, 17, 30), 11.480],
            [dt(2017, 6, 13, 17, 36), 2.160],
            [dt(2017, 6, 13, 17, 42), 6.400],
            [dt(2017, 6, 13, 17, 48), 8.520],
            [dt(2017, 6, 13, 17, 54), 9.680],
            [dt(2017, 6, 13, 18, 0), 4.040],
        ]
        valid = pnd.DataFrame([x[1] for x in valid],
                              index=[x[0] for x in valid],
                              dtype=np.float64)
        valid.columns = [(self.code, valid_varname)]
        valid_strict = [
            [dt(2017, 6, 13, 17, 6), 4.960],
            [dt(2017, 6, 13, 17, 12), 2.400],
            [dt(2017, 6, 13, 17, 18), 2.760],
            [dt(2017, 6, 13, 17, 24), 8.400],
            [dt(2017, 6, 13, 17, 30), 11.480],
            [dt(2017, 6, 13, 17, 36), 2.160],
            [dt(2017, 6, 13, 17, 42), 6.400],
            [dt(2017, 6, 13, 17, 48), 8.520],
            [dt(2017, 6, 13, 17, 54), 9.680],
            [dt(2017, 6, 13, 18, 0), 4.040],
        ]
        valid_strict = pnd.DataFrame([x[1] for x in valid_strict],
                                     index=[x[0] for x in valid_strict],
                                     dtype=np.float64)
        valid_strict.columns = [(self.code, valid_varname)]
        # =====================================================================
        serie = Serie(
            datval=dtval,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        nes = serie.nearlyequalscale(toparam=valid_varname, strict=False)
        self.assertEqual(nes.code, self.code)
        self.assertEqual(nes.varname, valid_varname)
        self.assertEqual(nes.spc_varname, valid_varname)
        self.assertEqual(nes.firstdt, dt(2017, 6, 13, 17, 0))
        self.assertEqual(nes.lastdt, dt(2017, 6, 13, 18, 0))
        assert_frame_equal(nes.data_frame, valid)
        # =====================================================================
        nes = serie.nearlyequalscale(toparam=valid_varname, strict=True)
        self.assertEqual(nes.code, self.code)
        self.assertEqual(nes.varname, valid_varname)
        self.assertEqual(nes.spc_varname, valid_varname)
        self.assertEqual(nes.firstdt, dt(2017, 6, 13, 17, 6))
        self.assertEqual(nes.lastdt, dt(2017, 6, 13, 18, 0))
        assert_frame_equal(nes.data_frame, valid_strict)
        # =====================================================================

    def test_nearlyequalscale_65(self):
        """
        Test nearlyequalscale - 6min vers 5min
        """
        varname = 'P6m'
        dtval = [
            [dt(2017, 6, 13, 17, 0), 0.0],
            [dt(2017, 6, 13, 17, 6), 1.0],
            [dt(2017, 6, 13, 17, 12), 0.2],
            [dt(2017, 6, 13, 17, 18), 0.4],
            [dt(2017, 6, 13, 17, 24), 0.4],
            [dt(2017, 6, 13, 17, 30), 0.4],
            [dt(2017, 6, 13, 17, 36), 2.2],
            [dt(2017, 6, 13, 17, 42), 11.7],
            [dt(2017, 6, 13, 17, 48), 13.3],
            [dt(2017, 6, 13, 17, 54), 8.2],
            [dt(2017, 6, 13, 18, 0), 6.3],
        ]
        # =====================================================================
        valid_varname = 'P5m'
        valid = [
            [dt(2017, 6, 13, 16, 55), 0.000],
            [dt(2017, 6, 13, 17, 0), 0.000],
            [dt(2017, 6, 13, 17, 5), 0.833],
            [dt(2017, 6, 13, 17, 10), 0.300],
            [dt(2017, 6, 13, 17, 15), 0.267],
            [dt(2017, 6, 13, 17, 20), 0.333],
            [dt(2017, 6, 13, 17, 25), 0.333],
            [dt(2017, 6, 13, 17, 30), 0.333],
            [dt(2017, 6, 13, 17, 35), 1.833],
            [dt(2017, 6, 13, 17, 40), 8.167],
            [dt(2017, 6, 13, 17, 45), 10.550],
            [dt(2017, 6, 13, 17, 50), 9.383],
            [dt(2017, 6, 13, 17, 55), 6.517],
            [dt(2017, 6, 13, 18, 0), 5.250],
        ]
        valid = pnd.DataFrame([x[1] for x in valid],
                              index=[x[0] for x in valid],
                              dtype=np.float64)
        valid.columns = [(self.code, valid_varname)]
        valid_strict = [
            [dt(2017, 6, 13, 17, 0), 0.000],
            [dt(2017, 6, 13, 17, 5), 0.833],
            [dt(2017, 6, 13, 17, 10), 0.300],
            [dt(2017, 6, 13, 17, 15), 0.267],
            [dt(2017, 6, 13, 17, 20), 0.333],
            [dt(2017, 6, 13, 17, 25), 0.333],
            [dt(2017, 6, 13, 17, 30), 0.333],
            [dt(2017, 6, 13, 17, 35), 1.833],
            [dt(2017, 6, 13, 17, 40), 8.167],
            [dt(2017, 6, 13, 17, 45), 10.550],
            [dt(2017, 6, 13, 17, 50), 9.383],
            [dt(2017, 6, 13, 17, 55), 6.517],
            [dt(2017, 6, 13, 18, 0), 5.250],
        ]
        valid_strict = pnd.DataFrame([x[1] for x in valid_strict],
                                     index=[x[0] for x in valid_strict],
                                     dtype=np.float64)
        valid_strict.columns = [(self.code, valid_varname)]
        # =====================================================================
        serie = Serie(
            datval=dtval,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        nes = serie.nearlyequalscale(toparam=valid_varname, strict=False)
        self.assertEqual(nes.code, self.code)
        self.assertEqual(nes.varname, valid_varname)
        self.assertEqual(nes.spc_varname, valid_varname)
        self.assertEqual(nes.firstdt, dt(2017, 6, 13, 16, 55))
        self.assertEqual(nes.lastdt, dt(2017, 6, 13, 18, 0))
        assert_frame_equal(nes.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================
        nes = serie.nearlyequalscale(toparam=valid_varname, strict=True)
        self.assertEqual(nes.code, self.code)
        self.assertEqual(nes.varname, valid_varname)
        self.assertEqual(nes.spc_varname, valid_varname)
        self.assertEqual(nes.firstdt, dt(2017, 6, 13, 17, 0))
        self.assertEqual(nes.lastdt, dt(2017, 6, 13, 18, 0))
        assert_frame_equal(nes.data_frame, valid_strict,
                           check_less_precise=1, check_exact=False)
        # =====================================================================

    def test_regularscale(self):
        """
        Test méthode to_regularscale
        """
        # =====================================================================
        varname = 'QI'
        valid_varname = 'QH'
        dtval = [
            [dt(2014, 11, 3, 23, 55), 35.600],
            [dt(2014, 11, 4, 0, 15), 41.200],
            [dt(2014, 11, 4, 2, 45), 46.900],
            [dt(2014, 11, 4, 3, 30), 62.900],
            [dt(2014, 11, 4, 4, 12), 85.700],
            [dt(2014, 11, 4, 5, 58), 106.000],
            [dt(2014, 11, 4, 6, 2), 118.000],
            [dt(2014, 11, 4, 7, 59), 115.000],
            [dt(2014, 11, 4, 8, 1), 103.000],
            [dt(2014, 11, 4, 9, 30), 92.500],
            [dt(2014, 11, 4, 10, 30), 86.400],
            [dt(2014, 11, 4, 11, 45), 82.300],
            [dt(2014, 11, 4, 12, 15), 78.300]
        ]
        valid = [
            [dt(2014, 11, 3, 23), np.nan],
            [dt(2014, 11, 4, 0), 36.700],
            [dt(2014, 11, 4, 1), 42.910],
            [dt(2014, 11, 4, 2), 45.190],
            [dt(2014, 11, 4, 3), 52.233],
            [dt(2014, 11, 4, 4), 79.186],
            [dt(2014, 11, 4, 5), 94.892],
            [dt(2014, 11, 4, 6), 112.000],
            [dt(2014, 11, 4, 7), 116.513],
            [dt(2014, 11, 4, 8), 109.000],
            [dt(2014, 11, 4, 9), 96.039],
            [dt(2014, 11, 4, 10), 89.450],
            [dt(2014, 11, 4, 11), 84.760],
            [dt(2014, 11, 4, 12), 80.300],
            [dt(2014, 11, 4, 13), np.nan]
        ]
        valid = pnd.DataFrame([x[1] for x in valid],
                              index=[x[0] for x in valid],
                              dtype=np.float64)
        valid.columns = [(self.code, valid_varname)]
        # =====================================================================
        serie = Serie(
            datval=dtval,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        regularserie = serie.regularscale()
        self.assertEqual(regularserie.code, self.code)
        self.assertEqual(regularserie.varname, valid_varname)
        self.assertEqual(regularserie.spc_varname, valid_varname)
        self.assertEqual(regularserie.firstdt, dt(2014, 11, 3, 23))
        self.assertEqual(regularserie.lastdt, dt(2014, 11, 4, 13))
        assert_frame_equal(regularserie.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================

    def test_standardize(self):
        """
        Test méthode standardize
        """
        # =====================================================================
        varname = 'QH'
        df = pnd.DataFrame(
            [23.0, 25.5, 30.6, 53.6, 92.5, 119.4, 132.4, 137.1, 134.9, 129.6,
             121.2, 111.7, 102.1, 93.7, 84.6, 76.6, 69.5, 63.9, 59.1, 55.0,
             51.0, 48.1, 45.8, 43.5, 41.9],
            index=pnd.date_range(
                dt(2024, 3, 30, 6), dt(2024, 3, 31, 6), freq='H'),
            dtype=np.float64
        )
        valid = pnd.DataFrame(
            [0.167761, 0.185996, 0.223195, 0.390956, 0.674690, 0.870897,
             0.965718, 1.000000, 0.983953, 0.945295, 0.884026, 0.814734,
             0.744712, 0.683443, 0.617068, 0.558716, 0.506929, 0.466083,
             0.431072, 0.401167, 0.371991, 0.350839, 0.334063, 0.317287,
             0.305616],
            index=[td(hours=x) for x in range(-7, 18, 1)]
        )
        valid.columns = [(self.code, varname)]
        # =====================================================================
        serie = Serie(datval=df, code=self.code, provider=self.provider,
                      varname=varname)
        df = serie.standardize()
        assert_frame_equal(df, valid)
        # =====================================================================

    def test_subhourlyscale_BFILL(self):
        """
        Test méthode subhourlyscale
        """
        # =====================================================================
        varname = 'QH'
        ts = td(seconds=60*30)
        dtval = [
            [dt(2014, 11, 4, 0), np.nan],
            [dt(2014, 11, 4, 1), np.nan],
            [dt(2014, 11, 4, 2), 46.900],
            [dt(2014, 11, 4, 3), 62.900],
            [dt(2014, 11, 4, 4), 85.700],
            [dt(2014, 11, 4, 5), 106.000],
            [dt(2014, 11, 4, 6), np.nan],
            [dt(2014, 11, 4, 7), np.nan],
            [dt(2014, 11, 4, 8), 103.000],
            [dt(2014, 11, 4, 9), np.nan],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 11), np.nan],
            [dt(2014, 11, 4, 12), np.nan]
        ]
        serie = Serie(
            datval=dtval,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        valid_varname = 'QI'
        # =====================================================================
        # CAS 1 : BACKFILL / BFILL
        # ------------------------
        valid = [
            [dt(2014, 11, 4, 0, 0), np.nan],
            [dt(2014, 11, 4, 0, 30), np.nan],
            [dt(2014, 11, 4, 1, 0), np.nan],
            [dt(2014, 11, 4, 1, 30), 46.900],
            [dt(2014, 11, 4, 2, 0), 46.900],
            [dt(2014, 11, 4, 2, 30), 62.900],
            [dt(2014, 11, 4, 3, 0), 62.900],
            [dt(2014, 11, 4, 3, 30), 85.700],
            [dt(2014, 11, 4, 4, 0), 85.700],
            [dt(2014, 11, 4, 4, 30), 106.0],
            [dt(2014, 11, 4, 5, 0), 106.0],
            [dt(2014, 11, 4, 5, 30), np.nan],
            [dt(2014, 11, 4, 6, 0), np.nan],
            [dt(2014, 11, 4, 6, 30), np.nan],
            [dt(2014, 11, 4, 7, 0), np.nan],
            [dt(2014, 11, 4, 7, 30), 103.0],
            [dt(2014, 11, 4, 8, 0), 103.0],
            [dt(2014, 11, 4, 8, 30), np.nan],
            [dt(2014, 11, 4, 9, 0), np.nan],
            [dt(2014, 11, 4, 9, 30), 86.400],
            [dt(2014, 11, 4, 10, 0), 86.400],
            [dt(2014, 11, 4, 10, 30), np.nan],
            [dt(2014, 11, 4, 11, 0), np.nan],
            [dt(2014, 11, 4, 11, 30), np.nan],
            [dt(2014, 11, 4, 12, 0), np.nan],
        ]
        valid = pnd.DataFrame([x[1] for x in valid],
                              index=[x[0] for x in valid],
                              dtype=np.float32)
        valid.columns = [(self.code, valid_varname)]
        subhourlyserie = serie.subhourlyscale(ts=ts, how='bfill')
        self.assertEqual(subhourlyserie.code, self.code)
        self.assertEqual(subhourlyserie.varname, valid_varname)
        self.assertEqual(subhourlyserie.spc_varname, valid_varname)
        self.assertEqual(subhourlyserie.firstdt, dt(2014, 11, 4, 0, 0))
        self.assertEqual(subhourlyserie.lastdt, dt(2014, 11, 4, 12, 0))
        assert_frame_equal(subhourlyserie.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================

    def test_subhourlyscale_FFILL(self):
        """
        Test méthode subhourlyscale
        """
        # =====================================================================
        varname = 'QH'
        ts = td(seconds=60*30)
        dtval = [
            [dt(2014, 11, 4, 0), np.nan],
            [dt(2014, 11, 4, 1), np.nan],
            [dt(2014, 11, 4, 2), 46.900],
            [dt(2014, 11, 4, 3), 62.900],
            [dt(2014, 11, 4, 4), 85.700],
            [dt(2014, 11, 4, 5), 106.000],
            [dt(2014, 11, 4, 6), np.nan],
            [dt(2014, 11, 4, 7), np.nan],
            [dt(2014, 11, 4, 8), 103.000],
            [dt(2014, 11, 4, 9), np.nan],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 11), np.nan],
            [dt(2014, 11, 4, 12), np.nan]
        ]
        serie = Serie(
            datval=dtval,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        valid_varname = 'QI'
        # =====================================================================
        # CAS 2 : FORWARD / FFILL
        # ------------------------
        valid = [
            [dt(2014, 11, 4, 0, 0), np.nan],
            [dt(2014, 11, 4, 0, 30), np.nan],
            [dt(2014, 11, 4, 1, 0), np.nan],
            [dt(2014, 11, 4, 1, 30), np.nan],
            [dt(2014, 11, 4, 2, 0), 46.900],
            [dt(2014, 11, 4, 2, 30), 46.900],
            [dt(2014, 11, 4, 3, 0), 62.900],
            [dt(2014, 11, 4, 3, 30), 62.900],
            [dt(2014, 11, 4, 4, 0), 85.700],
            [dt(2014, 11, 4, 4, 30), 85.700],
            [dt(2014, 11, 4, 5, 0), 106.000],
            [dt(2014, 11, 4, 5, 30), 106.000],
            [dt(2014, 11, 4, 6, 0), np.nan],
            [dt(2014, 11, 4, 6, 30), np.nan],
            [dt(2014, 11, 4, 7, 0), np.nan],
            [dt(2014, 11, 4, 7, 30), np.nan],
            [dt(2014, 11, 4, 8, 0), 103.000],
            [dt(2014, 11, 4, 8, 30), 103.000],
            [dt(2014, 11, 4, 9, 0), np.nan],
            [dt(2014, 11, 4, 9, 30), np.nan],
            [dt(2014, 11, 4, 10, 0), 86.400],
            [dt(2014, 11, 4, 10, 30), 86.400],
            [dt(2014, 11, 4, 11, 0), np.nan],
            [dt(2014, 11, 4, 11, 30), np.nan],
            [dt(2014, 11, 4, 12, 0), np.nan],
        ]
        valid = pnd.DataFrame([x[1] for x in valid],
                              index=[x[0] for x in valid],
                              dtype=np.float32)
        valid.columns = [(self.code, valid_varname)]
        subhourlyserie = serie.subhourlyscale(ts=ts, how='ffill')
        self.assertEqual(subhourlyserie.code, self.code)
        self.assertEqual(subhourlyserie.varname, valid_varname)
        self.assertEqual(subhourlyserie.spc_varname, valid_varname)
        self.assertEqual(subhourlyserie.firstdt, dt(2014, 11, 4, 0, 0))
        self.assertEqual(subhourlyserie.lastdt, dt(2014, 11, 4, 12, 0))
        assert_frame_equal(subhourlyserie.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================

    def test_subhourlyscale_NEAREST(self):
        """
        Test méthode subhourlyscale
        """
        # =====================================================================
        varname = 'QH'
        ts = td(seconds=60*15)
        dtval = [
            [dt(2014, 11, 4, 0), np.nan],
            [dt(2014, 11, 4, 1), np.nan],
            [dt(2014, 11, 4, 2), 46.900],
            [dt(2014, 11, 4, 3), 62.900],
            [dt(2014, 11, 4, 4), 85.700],
            [dt(2014, 11, 4, 5), 106.000],
            [dt(2014, 11, 4, 6), np.nan],
            [dt(2014, 11, 4, 7), np.nan],
            [dt(2014, 11, 4, 8), 103.000],
            [dt(2014, 11, 4, 9), np.nan],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 11), np.nan],
            [dt(2014, 11, 4, 12), np.nan]
        ]
        serie = Serie(
            datval=dtval,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        valid_varname = 'QI'
        # =====================================================================
        # CAS 3 : NEAREST
        # ---------------
        valid = [
            [dt(2014, 11, 4, 0, 0), np.nan],
            [dt(2014, 11, 4, 0, 15), np.nan],
            [dt(2014, 11, 4, 0, 30), np.nan],
            [dt(2014, 11, 4, 0, 45), np.nan],
            [dt(2014, 11, 4, 1, 0), np.nan],
            [dt(2014, 11, 4, 1, 15), np.nan],
            [dt(2014, 11, 4, 1, 30), 46.900],
            [dt(2014, 11, 4, 1, 45), 46.900],
            [dt(2014, 11, 4, 2, 0), 46.900],
            [dt(2014, 11, 4, 2, 15), 46.900],
            [dt(2014, 11, 4, 2, 30), 62.900],
            [dt(2014, 11, 4, 2, 45), 62.900],
            [dt(2014, 11, 4, 3, 0), 62.900],
            [dt(2014, 11, 4, 3, 15), 62.900],
            [dt(2014, 11, 4, 3, 30), 85.700],
            [dt(2014, 11, 4, 3, 45), 85.700],
            [dt(2014, 11, 4, 4, 0), 85.700],
            [dt(2014, 11, 4, 4, 15), 85.700],
            [dt(2014, 11, 4, 4, 30), 106.000],
            [dt(2014, 11, 4, 4, 45), 106.000],
            [dt(2014, 11, 4, 5, 0), 106.000],
            [dt(2014, 11, 4, 5, 15), 106.000],
            [dt(2014, 11, 4, 5, 30), np.nan],
            [dt(2014, 11, 4, 5, 45), np.nan],
            [dt(2014, 11, 4, 6, 0), np.nan],
            [dt(2014, 11, 4, 6, 15), np.nan],
            [dt(2014, 11, 4, 6, 30), np.nan],
            [dt(2014, 11, 4, 6, 45), np.nan],
            [dt(2014, 11, 4, 7, 0), np.nan],
            [dt(2014, 11, 4, 7, 15), np.nan],
            [dt(2014, 11, 4, 7, 30), 103.000],
            [dt(2014, 11, 4, 7, 45), 103.000],
            [dt(2014, 11, 4, 8, 0), 103.000],
            [dt(2014, 11, 4, 8, 15), 103.000],
            [dt(2014, 11, 4, 8, 30), np.nan],
            [dt(2014, 11, 4, 8, 45), np.nan],
            [dt(2014, 11, 4, 9, 0), np.nan],
            [dt(2014, 11, 4, 9, 15), np.nan],
            [dt(2014, 11, 4, 9, 30), 86.400],
            [dt(2014, 11, 4, 9, 45), 86.400],
            [dt(2014, 11, 4, 10, 0), 86.400],
            [dt(2014, 11, 4, 10, 15), 86.400],
            [dt(2014, 11, 4, 10, 30), np.nan],
            [dt(2014, 11, 4, 10, 45), np.nan],
            [dt(2014, 11, 4, 11, 0), np.nan],
            [dt(2014, 11, 4, 11, 15), np.nan],
            [dt(2014, 11, 4, 11, 30), np.nan],
            [dt(2014, 11, 4, 11, 45), np.nan],
            [dt(2014, 11, 4, 12, 0), np.nan],
        ]
        valid = pnd.DataFrame([x[1] for x in valid],
                              index=[x[0] for x in valid],
                              dtype=np.float32)
        valid.columns = [(self.code, valid_varname)]
        subhourlyserie = serie.subhourlyscale(ts=ts, how='nearest')
        self.assertEqual(subhourlyserie.code, self.code)
        self.assertEqual(subhourlyserie.varname, valid_varname)
        self.assertEqual(subhourlyserie.spc_varname, valid_varname)
        self.assertEqual(subhourlyserie.firstdt, dt(2014, 11, 4, 0, 0))
        self.assertEqual(subhourlyserie.lastdt, dt(2014, 11, 4, 12, 0))
        assert_frame_equal(subhourlyserie.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================

    def test_subhourlyscale_INTERP(self):
        """
        Test méthode subhourlyscale
        """
        # =====================================================================
        varname = 'QH'
        ts = td(seconds=60*15)
        dtval = [
            [dt(2014, 11, 4, 0), np.nan],
            [dt(2014, 11, 4, 1), np.nan],
            [dt(2014, 11, 4, 2), 46.900],
            [dt(2014, 11, 4, 3), 62.900],
            [dt(2014, 11, 4, 4), 85.700],
            [dt(2014, 11, 4, 5), 106.000],
            [dt(2014, 11, 4, 6), np.nan],
            [dt(2014, 11, 4, 7), np.nan],
            [dt(2014, 11, 4, 8), 103.000],
            [dt(2014, 11, 4, 9), np.nan],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 11), np.nan],
            [dt(2014, 11, 4, 12), np.nan]
        ]
        serie = Serie(
            datval=dtval,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        valid_varname = 'QI'
        # =====================================================================
        # CAS 4 : INTERPOLATE
        # -------------------
        valid = [
            [dt(2014, 11, 4, 0, 0), np.nan],
            [dt(2014, 11, 4, 0, 15), np.nan],
            [dt(2014, 11, 4, 0, 30), np.nan],
            [dt(2014, 11, 4, 0, 45), np.nan],
            [dt(2014, 11, 4, 1, 0), np.nan],
            [dt(2014, 11, 4, 1, 15), np.nan],
            [dt(2014, 11, 4, 1, 30), np.nan],
            [dt(2014, 11, 4, 1, 45), np.nan],
            [dt(2014, 11, 4, 2, 0), 46.900],
            [dt(2014, 11, 4, 2, 15), 50.900],
            [dt(2014, 11, 4, 2, 30), 54.900],
            [dt(2014, 11, 4, 2, 45), 58.900],
            [dt(2014, 11, 4, 3, 0), 62.900],
            [dt(2014, 11, 4, 3, 15), 68.600],
            [dt(2014, 11, 4, 3, 30), 74.300],
            [dt(2014, 11, 4, 3, 45), 80.000],
            [dt(2014, 11, 4, 4, 0), 85.700],
            [dt(2014, 11, 4, 4, 15), 90.775],
            [dt(2014, 11, 4, 4, 30), 95.850],
            [dt(2014, 11, 4, 4, 45), 100.925],
            [dt(2014, 11, 4, 5, 0), 106.000],
            [dt(2014, 11, 4, 5, 15), np.nan],
            [dt(2014, 11, 4, 5, 30), np.nan],
            [dt(2014, 11, 4, 5, 45), np.nan],
            [dt(2014, 11, 4, 6, 0), np.nan],
            [dt(2014, 11, 4, 6, 15), np.nan],
            [dt(2014, 11, 4, 6, 30), np.nan],
            [dt(2014, 11, 4, 6, 45), np.nan],
            [dt(2014, 11, 4, 7, 0), np.nan],
            [dt(2014, 11, 4, 7, 15), np.nan],
            [dt(2014, 11, 4, 7, 30), np.nan],
            [dt(2014, 11, 4, 7, 45), np.nan],
            [dt(2014, 11, 4, 8, 0), 103.000],
            [dt(2014, 11, 4, 8, 15), np.nan],
            [dt(2014, 11, 4, 8, 30), np.nan],
            [dt(2014, 11, 4, 8, 45), np.nan],
            [dt(2014, 11, 4, 9, 0), np.nan],
            [dt(2014, 11, 4, 9, 15), np.nan],
            [dt(2014, 11, 4, 9, 30), np.nan],
            [dt(2014, 11, 4, 9, 45), np.nan],
            [dt(2014, 11, 4, 10, 0), 86.400],
            [dt(2014, 11, 4, 10, 15), np.nan],
            [dt(2014, 11, 4, 10, 30), np.nan],
            [dt(2014, 11, 4, 10, 45), np.nan],
            [dt(2014, 11, 4, 11, 0), np.nan],
            [dt(2014, 11, 4, 11, 15), np.nan],
            [dt(2014, 11, 4, 11, 30), np.nan],
            [dt(2014, 11, 4, 11, 45), np.nan],
            [dt(2014, 11, 4, 12, 0), np.nan],
        ]
        valid = pnd.DataFrame([x[1] for x in valid],
                              index=[x[0] for x in valid],
                              dtype=np.float32)
        valid.columns = [(self.code, valid_varname)]
        subhourlyserie = serie.subhourlyscale(ts=ts, how='interpolate',
                                              howinterp='linear')
        self.assertEqual(subhourlyserie.code, self.code)
        self.assertEqual(subhourlyserie.varname, valid_varname)
        self.assertEqual(subhourlyserie.spc_varname, valid_varname)
        self.assertEqual(subhourlyserie.firstdt, dt(2014, 11, 4, 0, 0))
        self.assertEqual(subhourlyserie.lastdt, dt(2014, 11, 4, 12, 0))
        assert_frame_equal(subhourlyserie.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================

    def test_upscale_JM_cumule(self):
        """
        Test upscale - Journalier vers Mensuel - Version cumulée
        """
        # =====================================================================
        varname = 'PJ'
        serie = pnd.DataFrame(
            {(self.code, varname): [0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
                                    0.000, 0.000, 0.000, 0.000, 0.000, 18.100,
                                    18.800, 13.900, 0.000, 0.000, 0.000, 0.000,
                                    0.000, 0.000, 0.000, 28.500, 124.100,
                                    10.100, 0.000, 0.000, 19.300, 8.700, 3.000,
                                    0.000, 0.000, 0.000, 0.000, 0.000, 12.800,
                                    11.700, 15.900, 0.000, 0.000, 0.000, 0.000,
                                    0.000, 0.000, 0.000, 1.200, 0.000, 0.000,
                                    8.500, 0.200, 0.000, 0.000, 0.000, 2.400,
                                    0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
                                    0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
                                    0.000, 0.000, 0.000, 0.000, 21.200, 0.000,
                                    3.000, 0.200, 0.000, 0.000, 0.000, 0.000,
                                    0.000, 1.200, 0.400, 0.000, 0.000, 0.400,
                                    3.200, 0.000, 0.000, 5.000, 0.000, 0.000,
                                    0.000, 0.000, 0.000, 31.700, 0.000]},
            index=pnd.date_range(
                dt(2019, 7, 15),
                dt(2019, 10, 16),
                freq=td(days=1)
            )
        )
        serie = Serie(
            datval=serie,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid_varname = 'PM'
        valid = pnd.DataFrame(
            {(self.code, valid_varname): [50.800, 243.800, 27.000, 41.900]},
            index=pnd.date_range(
                dt(2019, 7, 1),
                dt(2019, 10, 1),
                freq='MS'
            )
        )
        valid.columns = [(self.code, valid_varname)]
        up = serie.upscale(toparam=valid_varname, strict=False)
        assert_frame_equal(up.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================
        valid_varname = 'PM'
        valid = pnd.DataFrame(
            {(self.code, valid_varname): [243.800, 27.000]},
            index=pnd.date_range(
                dt(2019, 8, 1),
                dt(2019, 9, 1),
                freq='MS'
            )
        )
        valid.columns = [(self.code, valid_varname)]
        up = serie.upscale(toparam=valid_varname, strict=True)
        assert_frame_equal(up.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================

    def test_upscale_3HJ_cumule(self):
        """
        Test upscale - Tri-Horaire vers Journalier - Version cumulée
        """
        # =====================================================================
        varname = 'P3H'
        serie = pnd.DataFrame(
            {(self.code, varname): [4.200, 3.200, 1.100, 2.200, 10.600, 14.300,
                                    14.300, 14.300, 14.300, 3.100, 3.100,
                                    0.400, 0.400, 0.000, 0.000, 0.000, 0.000]},
            index=pnd.date_range(
                dt(2016, 11, 21, 12),
                dt(2016, 11, 23, 12),
                freq=td(hours=3)
            )
        )
        serie = Serie(
            datval=serie,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid_varname = 'PJ'
        valid = pnd.DataFrame(
            {(self.code, valid_varname): [49.900, 35.600, 0.000]},
            index=pnd.date_range(
                dt(2016, 11, 21),
                dt(2016, 11, 23),
                freq=td(days=1)
            )
        )
        valid.columns = [(self.code, valid_varname)]
        up = serie.upscale(toparam=valid_varname, dayhour=6, strict=False)
        assert_frame_equal(up.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================
        valid_varname = 'PJ'
        valid = pnd.DataFrame(
            {(self.code, valid_varname): [21.300, 64.200, 0.000]},
            index=pnd.date_range(
                dt(2016, 11, 21),
                dt(2016, 11, 23),
                freq=td(days=1)
            )
        )
        valid.columns = [(self.code, valid_varname)]
        up = serie.upscale(toparam=valid_varname, dayhour=0, strict=False)
        assert_frame_equal(up.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================
        valid_varname = 'PJ'
        valid = pnd.DataFrame(
            {(self.code, valid_varname): [64.200]},
            index=pnd.date_range(
                dt(2016, 11, 22),
                dt(2016, 11, 22),
                freq=td(days=1)
            )
        )
        valid.columns = [(self.code, valid_varname)]
        up = serie.upscale(toparam=valid_varname, dayhour=0, strict=True)
        assert_frame_equal(up.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================

    def test_upscale_HJ(self):
        """
        Test upscale - Horaire vers Journalier
        """
        # =====================================================================
        varname = 'QH'
        serie = pnd.DataFrame(
            {(self.code, varname): [2.510, 2.700, 2.970, 3.430, 4.810, 8.180,
                                    12.100, 17.000, 19.600, 22.200, 24.800,
                                    29.900, 35.600, 41.200, 46.900, 62.900,
                                    85.700, 106.00, 118.00, 115.00, 103.00,
                                    92.500, 86.400, 82.300, 78.300, 74.400,
                                    70.600, 69.600, 71.400, 76.000, 82.400,
                                    89.100, 88.900, 84.300, 74.500, 64.500,
                                    55.300, 49.600, 45.100, 42.500, 40.000,
                                    37.900, 36.700, 35.500, 34.300, 33.000,
                                    31.800, 30.600, 29.400, 28.200, 27.700,
                                    27.300, 26.800, 26.300, 25.800, 25.200,
                                    24.700, 24.200, 23.700, 23.200, 22.700,
                                    22.300, 21.800, 21.300, 20.800, 20.300,
                                    19.800, 19.500, 19.100, 18.800, 18.600,
                                    18.400, 18.200]},
            index=pnd.date_range(
                dt(2014, 11, 3, 12),
                dt(2014, 11, 6, 12),
                freq=td(hours=1)
            )
        )
        serie = Serie(
            datval=serie,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid_varname = 'QJ'
        valid = pnd.DataFrame(
            {(self.code, valid_varname): [34.026, 71.263, 26.112, 18.767]},
            index=pnd.date_range(
                dt(2014, 11, 3),
                dt(2014, 11, 6),
                freq=td(days=1)
            )
        )
        valid.columns = [(self.code, valid_varname)]
        up = serie.upscale(toparam=valid_varname, dayhour=6, strict=False)
        assert_frame_equal(up.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================
        valid_varname = 'QJ'
        valid = pnd.DataFrame(
            {(self.code, valid_varname): [14.292, 79.967, 31.342, 19.908]},
            index=pnd.date_range(
                dt(2014, 11, 3),
                dt(2014, 11, 6),
                freq=td(days=1)
            )
        )
        valid.columns = [(self.code, valid_varname)]
        up = serie.upscale(toparam=valid_varname, dayhour=0, strict=False)
        assert_frame_equal(up.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================
        valid_varname = 'QJ'
        valid = pnd.DataFrame(
            {(self.code, valid_varname): [79.967, 31.342]},
            index=pnd.date_range(
                dt(2014, 11, 4),
                dt(2014, 11, 5),
                freq=td(days=1)
            )
        )
        valid.columns = [(self.code, valid_varname)]
        up = serie.upscale(toparam=valid_varname, dayhour=0, strict=True)
        assert_frame_equal(up.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================

    def test_upscale_15mH_cumule(self):
        """
        Test upscale
        """
        # =====================================================================
        varname = 'P15m'
        serie = pnd.DataFrame(
            {(self.code, varname): [9.150, 16.390, 32.610, 33.030, 23.750,
                                    19.600, 9.530, 10.730]},
            index=pnd.date_range(
                dt(2017, 6, 13, 17, 45),
                dt(2017, 6, 13, 19, 30),
                freq=td(minutes=15)
            )
        )
        serie = Serie(
            datval=serie,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid_varname = 'PH'
        valid = pnd.DataFrame(
            {(self.code, valid_varname): [25.540, 108.990, 20.260]},
            index=pnd.date_range(
                dt(2017, 6, 13, 18),
                dt(2017, 6, 13, 20),
                freq=td(hours=1)
            )
        )
        valid.columns = [(self.code, valid_varname)]
        up = serie.upscale(toparam=valid_varname, strict=False)
        assert_frame_equal(up.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================
        valid_varname = 'PH'
        valid = pnd.DataFrame(
            {(self.code, valid_varname): [108.990]},
            index=pnd.date_range(
                dt(2017, 6, 13, 19),
                dt(2017, 6, 13, 19),
                freq=td(hours=1)
            )
        )
        valid.columns = [(self.code, valid_varname)]
        up = serie.upscale(toparam=valid_varname, strict=True)
        assert_frame_equal(up.data_frame, valid,
                           check_less_precise=1, check_exact=False)
        # =====================================================================
