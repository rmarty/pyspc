#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary grpRT2csv

To run all tests just type:
    python -m unittest test_bin_grpRT2csv

To run only a class test:
    python -m unittest test_bin_grpRT2csv.Test_grpRT2csv

To run only a specific test:
    python -m unittest test_bin_grpRT2csv.Test_grpRT2csv.test_data

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import grpRT2csv as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_grpRT2csv(unittest.TestCase):
    """
    grpRT2csv bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'grpRT2csv')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_grp16 = os.path.join('data', 'model', 'grp16', 'rt')
        cls.dir_grp18 = os.path.join('data', 'model', 'grp18', 'rt')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_data_grp16_grp16(self):
        """
        Test fichier d'observations v2016
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpRT2csv.py',
            '-I', self.dir_grp16,
            '-O', self.dir_out,
            '-d', 'Debit.txt',
            '-t', 'grp16_rt_data',
            '-C', 'grp16'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.grpRT2csv()
        self.assertEqual(args.input_dir, self.dir_grp16)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'grp16_rt_data')
        self.assertEqual(args.data_filename, 'Debit.txt')
        self.assertEqual(args.csv_type, 'grp16')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        with subprocess.Popen(
                processArgs, universal_newlines=True, shell=True,  # nosec
                stdout=sys.stdout, stderr=sys.stderr):  # nosec
            pass
        # =====================================================================
        basenames = ['K0114020_Q.txt', 'K0114030_Q.txt']
        for basename in basenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, basename),
                os.path.join(self.dir_ctl, basename),
            ))
            os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les données d'observation de GRP v2016 ({t}) "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, t=args.data_type, d=args.data_filename,
                O=args.output_dir, C=args.csv_type,
                )
        # =====================================================================

    def test_data_grp18_grp18(self):
        """
        Test fichier d'observations v2018
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpRT2csv.py',
            '-I', self.dir_grp18,
            '-O', self.dir_out,
            '-d', 'Pluie_00J01H00M.txt',
            '-t', 'grp18_rt_data',
            '-C', 'grp18'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.grpRT2csv()
        self.assertEqual(args.input_dir, self.dir_grp18)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'grp18_rt_data')
        self.assertEqual(args.data_filename, 'Pluie_00J01H00M.txt')
        self.assertEqual(args.csv_type, 'grp18')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        with subprocess.Popen(
                processArgs, universal_newlines=True, shell=True,  # nosec
                stdout=sys.stdout, stderr=sys.stderr):  # nosec
            pass
        # =====================================================================
        basenames = ['90052002_P_00J01H00M.txt', '90065003_P_00J01H00M.txt']
        for basename in basenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, basename),
                os.path.join(self.dir_ctl, basename),
            ))
            os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les données d'observation de GRP v2018 ({t}) "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, t=args.data_type, d=args.data_filename,
                O=args.output_dir, C=args.csv_type,
                )
        # =====================================================================

    def test_scenmeteo_grp16_pyspc(self):
        """
        Test fichier de scénarios météo v2016
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpRT2csv.py',
            '-I', self.dir_grp16,
            '-O', self.dir_out,
            '-d', 'Scen_006_PluMA.txt',
            '-t', 'grp16_rt_data',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.grpRT2csv()
        self.assertEqual(args.input_dir, self.dir_grp16)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'grp16_rt_data')
        self.assertEqual(args.data_filename, 'Scen_006_PluMA.txt')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        with subprocess.Popen(
                processArgs, universal_newlines=True, shell=True,  # nosec
                stdout=sys.stdout, stderr=sys.stderr):  # nosec
            pass
        # =====================================================================
        basenames = ['K0114020_2017061312_grp16_006_PH.txt',
                     'K0114030_2017061312_grp16_006_PH.txt']
        for basename in basenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, basename),
                os.path.join(self.dir_ctl, basename),
            ))
            os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les scénarios météo de GRP v2016 ({t}) "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, t=args.data_type, d=args.data_filename,
                O=args.output_dir, C=args.csv_type,
                )
        # =====================================================================

    def test_scenmeteo_grp18_pyspc(self):
        """
        Test fichier de scénarios météo v2018
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpRT2csv.py',
            '-I', self.dir_grp18,
            '-O', self.dir_out,
            '-d', 'Scen_001_PluRR_00J01H00M.txt',
            '-t', 'grp18_rt_metscen',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.grpRT2csv()
        self.assertEqual(args.input_dir, self.dir_grp18)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'grp18_rt_metscen')
        self.assertEqual(args.data_filename, 'Scen_001_PluRR_00J01H00M.txt')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        with subprocess.Popen(
                processArgs, universal_newlines=True, shell=True,  # nosec
                stdout=sys.stdout, stderr=sys.stderr):  # nosec
            pass
        # =====================================================================
        basenames = ['RH10585x_2021053101_grp18_001_PH.txt']
        for basename in basenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, basename),
                os.path.join(self.dir_ctl, basename),
            ))
            os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les scénarios météo de GRP v2018 ({t}) "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, t=args.data_type, d=args.data_filename,
                O=args.output_dir, C=args.csv_type,
                )
        # =====================================================================

    def test_fcst_grp16_pyspc(self):
        """
        Test fichier de prévisions hydro v2016
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpRT2csv.py',
            '-I', self.dir_grp16,
            '-O', self.dir_out,
            '-d', 'GRP_D_Prev_2001.txt',
            '-t', 'grp16_rt_fcst_diff',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.grpRT2csv()
        self.assertEqual(args.input_dir, self.dir_grp16)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'grp16_rt_fcst_diff')
        self.assertEqual(args.data_filename, 'GRP_D_Prev_2001.txt')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
#        print(' '.join(processArgs))
        with subprocess.Popen(
                processArgs, universal_newlines=True, shell=True,  # nosec
                stdout=sys.stdout, stderr=sys.stderr):  # nosec
            pass
        # =====================================================================
#        basenames = ['K6173130_2016053116_grp16_2001_PH.txt']
        basenames = ['GRP16_PH.txt']
        for basename in basenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, basename),
                os.path.join(self.dir_ctl, basename),
            ))
            os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les prévisions hydro de GRP v2016 ({t}) "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "Un seul fichier est créé."\
            "".format(
                I=args.input_dir, t=args.data_type, d=args.data_filename,
                O=args.output_dir, C=args.csv_type,
                )
        # =====================================================================

    def test_fcst_grp18_pyspc(self):
        """
        Test fichier de prévisions hydro v2018
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpRT2csv.py',
            '-I', self.dir_grp18,
            '-O', self.dir_out,
            '-d', 'GRP_D_Prev_2001.txt',
            '-t', 'grp18_rt_fcst_diff',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.grpRT2csv()
        self.assertEqual(args.input_dir, self.dir_grp18)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'grp18_rt_fcst_diff')
        self.assertEqual(args.data_filename, 'GRP_D_Prev_2001.txt')
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        with subprocess.Popen(
                processArgs, universal_newlines=True, shell=True,  # nosec
                stdout=sys.stdout, stderr=sys.stderr):  # nosec
            pass
        # =====================================================================
        basenames = ['RH10585x_2007011900_grp18_2001_PH.txt',
                     'RH10585x_2007011900_grp18_2001_PJ.txt',
                     'RH10585x_2007011900_grp18_2001_QH.txt',
                     'RH10585x_2007011900_grp18_2001_QJ.txt',
                     'RH10585x_2007011900_grp18_2001_TJ.txt',
                     ]
        for basename in basenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, basename),
                os.path.join(self.dir_ctl, basename),
            ))
            os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les prévisions hydro de GRP v2018 ({t}) "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, t=args.data_type, d=args.data_filename,
                O=args.output_dir, C=args.csv_type,
                )
        # =====================================================================

    def test_archive_grp16_grp16(self):
        """
        Test fichier d'archive v2016
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpRT2csv.py',
            '-I', self.dir_grp16,
            '-O', self.dir_out,
            '-d', 'PV_10A.DAT',
            '-t', 'grp16_rt_archive',
            '-C', 'grp16'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.grpRT2csv()
        self.assertEqual(args.input_dir, self.dir_grp16)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'grp16_rt_archive')
        self.assertEqual(args.data_filename, 'PV_10A.DAT')
        self.assertEqual(args.csv_type, 'grp16')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        with subprocess.Popen(
                processArgs, universal_newlines=True, shell=True,  # nosec
                stdout=sys.stdout, stderr=sys.stderr):  # nosec
            pass
        # =====================================================================
        basenames = ['43091005_P.txt']
        for basename in basenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, basename),
                os.path.join(self.dir_ctl, basename),
            ))
            os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les données d'archive de GRP v2016 ({t}) "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, t=args.data_type, d=args.data_filename,
                O=args.output_dir, C=args.csv_type,
                )
        # =====================================================================

    def test_archive_grp18_grp18(self):
        """
        Test fichier d'archive v2018
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/grpRT2csv.py',
            '-I', self.dir_grp18,
            '-O', self.dir_out,
            '-d', 'QV_10A.DAT',
            '-t', 'grp18_rt_archive',
            '-C', 'grp18'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.grpRT2csv()
        self.assertEqual(args.input_dir, self.dir_grp18)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'grp18_rt_archive')
        self.assertEqual(args.data_filename, 'QV_10A.DAT')
        self.assertEqual(args.csv_type, 'grp18')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        with subprocess.Popen(
                processArgs, universal_newlines=True, shell=True,  # nosec
                stdout=sys.stdout, stderr=sys.stderr):  # nosec
            pass
        # =====================================================================
        basenames = ['RH10585x_Q.txt']
        for basename in basenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, basename),
                os.path.join(self.dir_ctl, basename),
            ))
            os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les données d'archive de GRP v2018 ({t}) "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, t=args.data_type, d=args.data_filename,
                O=args.output_dir, C=args.csv_type,
                )
        # =====================================================================

    def test_intern_grp16_pyspc(self):
        """
        Test fichier de données internes v2016
        """
        # =====================================================================
        filename = os.path.join(self.dir_grp16, 'intern', 'PQE_1A_D.DAT')
        processArgs = [
            'python',
            '../bin/grpRT2csv.py',
            '-I', os.path.dirname(filename),
            '-O', self.dir_out,
            '-d', os.path.basename(filename),
            '-t', 'grp16_rt_intern_diff',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.grpRT2csv()
        self.assertEqual(args.input_dir, os.path.dirname(filename))
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'grp16_rt_intern_diff')
        self.assertEqual(args.data_filename, os.path.basename(filename))
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
#        print(' '.join(processArgs))
        with subprocess.Popen(
                processArgs, universal_newlines=True, shell=True,  # nosec
                stdout=sys.stdout, stderr=sys.stderr):  # nosec
            pass
        # =====================================================================
#        basenames = ['K1251810_QH.txt']
        basenames = ['GRP16_QH.txt']
        for basename in basenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, basename),
                os.path.join(self.dir_ctl, basename),
            ))
            os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les données internes de GRP v2016 ({t}) "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "Un seul fichier est créé."\
            "".format(
                I=args.input_dir, t=args.data_type, d=args.data_filename,
                O=args.output_dir, C=args.csv_type,
                )
        # =====================================================================

    def test_intern_grp18_pyspc(self):
        """
        Test fichier de données internes v2018
        """
        # =====================================================================
        filename = os.path.join(self.dir_grp18, 'intern', 'PQE_1A_D.DAT')
        processArgs = [
            'python',
            '../bin/grpRT2csv.py',
            '-I', os.path.dirname(filename),
            '-O', self.dir_out,
            '-d', os.path.basename(filename),
            '-t', 'grp18_rt_intern_diff',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.grpRT2csv()
        self.assertEqual(args.input_dir, os.path.dirname(filename))
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.data_type, 'grp18_rt_intern_diff')
        self.assertEqual(args.data_filename, os.path.basename(filename))
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
#        print(' '.join(processArgs))
        with subprocess.Popen(
                processArgs, universal_newlines=True, shell=True,  # nosec
                stdout=sys.stdout, stderr=sys.stderr):  # nosec
            pass
        # =====================================================================
#        basenames = ['RH10585x_QH.txt']
        basenames = ['GRP18_QH.txt']
        for basename in basenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, basename),
                os.path.join(self.dir_ctl, basename),
            ))
            os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les données internes de GRP v2018 ({t}) "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "Un seul fichier est créé."\
            "".format(
                I=args.input_dir, t=args.data_type, d=args.data_filename,
                O=args.output_dir, C=args.csv_type,
                )
        # =====================================================================
