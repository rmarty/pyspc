#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for BdApbp in pyspc.webservice.lamedo

To run all tests just type:
    python -m unittest test_webservice_BdApbp

To run only a class test:
    python -m unittest test_webservice_BdApbp.TestBdApbp

To run only a specific test:
    python -m unittest test_webservice_BdApbp.TestBdApbp.test_init

"""
# Imports
from datetime import datetime as dt
import json
import os
import unittest

from pyspc.webservice.lamedo import BdApbp


class TestBdApbp(unittest.TestCase):
    """
    BdApbp class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'webservice', 'lamedo')

    def test_init(self):
        """
        Test initialisation de l'instance
        """
        # =====================================================================
        proxies = {}
        bdapbp = BdApbp(proxies=proxies)
        self.assertEqual(bdapbp.proxies, proxies)
        self.assertIsNone(bdapbp.filename)
        self.assertIsNone(bdapbp.url)
        # =====================================================================
        proxies = None
        bdapbp = BdApbp(proxies=proxies)
        self.assertEqual(bdapbp.proxies, {})
        self.assertIsNone(bdapbp.filename)
        self.assertIsNone(bdapbp.url)
        # =====================================================================

    def test_urllib(self):
        """
        Test téléchargement - DTYPE short/long
        """
        # =====================================================================
        zones = ['43003', '43005']
        date = dt(2024, 10, 16)
        bp = BdApbp()
        # =====================================================================
        valid = {
            "data": [
                [43005, "202410160000", 17, None],
                [43005, "202410180000", 300, None],
                [43003, "202410170000", 150, 600],
                [43003, "202410160000", 30, None],
                [43003, "202410180000", 400, None],
                [43005, "202410170000", 17, None]],
            "statut": 0}
        dtype = 'short'
        basename = '43003-43005_202410160000_bp-short.json'
        bp.set_url(zones=zones, date=date, dtype=dtype)
        tmp_filename = os.path.join('data', basename)
        bp.set_filename(zones=zones, date=date, dtype=dtype, dirname='data')
        self.assertEqual(bp.filename, tmp_filename)
        bp.retrieve_byurllib()
        with open(tmp_filename, 'r', encoding='utf-8') as j:
            content = json.load(j)
        for k, v in valid.items():
            self.assertIn(k, content)
            self.assertEqual(v, content[k])
        os.remove(bp.filename)
        # =====================================================================
        valid = {
            "data": [
                [43005, "Lignon Forez Mare", "202410160000",
                 17, 12, None, None, "202410151032"],
                [43005, "Lignon Forez Mare", "202410180000",
                 300, 100, None, None, "202410151032"],
                [43003, "Lignon Velay Dunieres", "202410170000",
                 150, 50, 600, None, "202410151032"],
                [43003, "Lignon Velay Dunieres", "202410160000",
                 30, 20, None, None, "202410151032"],
                [43003, "Lignon Velay Dunieres", "202410180000",
                 400, 100, None, None, "202410151032"],
                [43005, "Lignon Forez Mare", "202410170000",
                 17, 12, None, None, "202410151032"]],
            "statut": 0}
        dtype = 'long'
        basename = '43003-43005_202410160000_bp-long.json'
        bp.set_url(zones=zones, date=date, dtype=dtype)
        tmp_filename = os.path.join('data', basename)
        bp.set_filename(zones=zones, date=date, dtype=dtype, dirname='data')
        self.assertEqual(bp.filename, tmp_filename)
        bp.retrieve_byurllib()
        with open(tmp_filename, 'r', encoding='utf-8') as j:
            content = json.load(j)
        for k, v in valid.items():
            self.assertIn(k, content)
            self.assertEqual(v, content[k])
        os.remove(bp.filename)
        # =====================================================================

    def test_datatype(self):
        """
        Test des types de données ApBp au format JSON
        """
        valid = ['long', 'short']
        self.assertEqual(valid, BdApbp.get_types())
