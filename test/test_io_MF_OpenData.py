#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_MF_OpenData in pyspc.io.meteofrance

To run all tests just type:
    python -m unittest test_data_MF_OpenData

To run only a class test:
    python -m unittest test_data_MF_OpenData.TestMF_OpenData

To run only a specific test:
    python -m unittest test_data_MF_OpenData.TestMF_OpenData.test_init

"""
# Imports
from datetime import datetime as dt
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.core.series import Series
from pyspc.io.meteofrance import read_MF_OpenData


class TestMF_OpenData(unittest.TestCase):
    """
    MF_OpenData_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'mf', 'open_data')

    def test_MN(self):
        """
        Test de lecture/écriture de données RR6/P6M
        """
        # =====================================================================
        dates = pnd.date_range(
            dt(2021, 5, 10, 11), dt(2021, 5, 10, 12, 54), freq='6min')
        valid = {
            ('43091005', 'P6m'): pnd.DataFrame(
                [0.800, 0.400, 0.600, 1.400, 1.400, 1.200, 0.600, 0.700,
                 1.000, 1.200, 2.200, 1.400, 0.800, 0.800, 0.800, 1.100,
                 1.000, 0.800, 1.000, 1.200],
                index=dates
            ),
            ('43130002', 'P6m'): pnd.DataFrame(
                [0.800, 0.600, 0.600, 1.000,
                 0.800, 1.000, 1.400, 1.000, 1.200, 1.000, 1.000, 0.800,
                 1.700, 1.200, 1.400, 1.000, 0.800, 1.400, 1.200, 1.400],
                index=dates
            ),
        }
        # =====================================================================
        filename = os.path.join(self.dirname,
                                'MN_43_previous-2020-2022.csv.gz')
        series = read_MF_OpenData(filename=filename)
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            kcv = (c, v)
            self.assertIn(kcv, valid)
            valid[kcv].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[kcv],
                               check_less_precise=3)
        # =====================================================================

    def test_H(self):
        """
        Test de lecture/écriture de données RR1/PH + T/TH
        """
        # =====================================================================
        dates = pnd.date_range(dt(2019, 11, 23), dt(2019, 11, 23, 9), freq='H')
        valid = {
            ('43091005', 'PH'): pnd.DataFrame(
                [5.9, 6.6, 7.9, 9.1, 11.7, 8.9, 7.8, 8.3, 8.9, 6.2],
                index=dates
            ),
            ('43091005', 'TH'): pnd.DataFrame(
                [5.0, 4.9, 5.6, 5.2, 5.1, 4.4, 4.0, 3.3, 3.2, 2.4],
                index=dates
            ),
            ('43130002', 'PH'): pnd.DataFrame(
                [5.2, 3.8, 5.4, 6.6, 6.4, 7.7, 7.0, 7.4, 9.3, 5.8],
                index=dates
            ),
            ('43130002', 'TH'): pnd.DataFrame(
                [6.4, 6.5, 6.4, 6.7, 6.4, 5.9, 5.4, 5.0, 4.7, 3.9],
                index=dates
            ),
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'H_43_2010-2019.csv.gz')
        series = read_MF_OpenData(filename=filename)
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            kcv = (c, v)
            self.assertIn(kcv, valid)
            valid[kcv].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[kcv],
                               check_less_precise=3)
        # =====================================================================

    def test_Q(self):
        """
        Test de lecture/écriture de données RR/PJ
        """
        # =====================================================================
        dates = pnd.date_range(dt(2023, 10, 10), dt(2023, 10, 19), freq='D')
        valid = {
            ('07154005', 'PJ'): pnd.DataFrame(
                [0.0, 0.0, 0.2, 0.0, 0.0, 0.0, 0.2, 2.8, 21.4, 130.8],
                index=dates
            ),
            ('07232001', 'PJ'): pnd.DataFrame(
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.7, 5.9, 27.9, 83.9],
                index=dates
            ),
            ('07154005', 'TJ'): pnd.DataFrame(
                [18.5, 18.0, 16.9, 12.9, 14.1, 5.5, 7.5, 10.1, 11.3, 12.1],
                index=dates
            ),
            ('07232001', 'TJ'): pnd.DataFrame(
                [14.7, 13.7, 13.5, 11.4, 14.1, 7.8, 7.9, 11.5, 12.6, 13.5],
                index=dates
            ),
        }
        # =====================================================================
        filename = os.path.join(self.dirname,
                                'Q_07_latest-2023-2024_RR-T-Vent.csv.gz')
        series = read_MF_OpenData(filename=filename)
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            kcv = (c, v)
            self.assertIn(kcv, valid)
            valid[kcv].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[kcv],
                               check_less_precise=3)
        # =====================================================================

    def test_Q_select(self):
        """
        Test de lecture/écriture de données RR/PJ
        """
        # =====================================================================
        dates = pnd.date_range(dt(2023, 10, 10), dt(2023, 10, 19), freq='D')
        valid = {
            ('07154005', 'PJ'): pnd.DataFrame(
                [0.0, 0.0, 0.2, 0.0, 0.0, 0.0, 0.2, 2.8, 21.4, 130.8],
                index=dates
            ),
            ('07154005', 'TJ'): pnd.DataFrame(
                [18.5, 18.0, 16.9, 12.9, 14.1, 5.5, 7.5, 10.1, 11.3, 12.1],
                index=dates
            ),
        }
        # =====================================================================
        filename = os.path.join(self.dirname,
                                'Q_07_latest-2023-2024_RR-T-Vent.csv.gz')
        series = read_MF_OpenData(filename=filename, codes=['07154005'])
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            kcv = (c, v)
            self.assertIn(kcv, valid)
            valid[kcv].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[kcv],
                               check_less_precise=3)
        # =====================================================================
