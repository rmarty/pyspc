#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary dbase2plathynes

To run all tests just type:
    python -m unittest test_bin_dbase2plathynes

To run only a class test:
    python -m unittest test_bin_dbase2plathynes.Test_dbase2plathynes

To run only a specific test:
    python -m unittest test_bin_dbase2plathynes.Test_dbase2plathynes.test_project

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import dbase2plathynes as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_dbase2plathynes(unittest.TestCase):
    """
    dbase2plathynes bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'dbase2plathynes')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'plathynes')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.stations_list_file = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)

    def test_project(self):
        """
        Tests PROJET PLATHYNES
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2plathynes.py',
            '-I', self.dir_in,
            '-d', 'plathynes_debitpluie.mdb', 'plathynes_pluie.mdb',
            '-t', 'sacha',
            '-O', self.dir_out,
            '-c', 'plathynes_project.prj',
            '-s', 'K0030020',
            '-3'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbase2plathynes()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(
            args.dbase_filename,
            ['plathynes_debitpluie.mdb', 'plathynes_pluie.mdb'])
        self.assertEqual(args.data_type, 'sacha')
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.varname, ['P', 'Q'])
        self.assertEqual(args.projet_filename, 'plathynes_project.prj')
        self.assertEqual(args.events, None)
        self.assertEqual(args.station_name, 'K0030020')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for evt in ['201910', '201911']:
            evt_dir = os.path.join(
                self.dir_out,
                'plathynes_project',
                'Ev_{}'.format(evt)
            )
            for filename in os.listdir(evt_dir):
                if not filename.endswith('.evt'):
                    self.assertTrue(filecmp.cmp(
                        os.path.join(evt_dir, filename),
                        os.path.join(self.dir_ctl, filename),
                    ))
                    os.remove(os.path.join(evt_dir, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données nécessaires à PLATHYNES depuis "\
            "les bases de données de type {t} : {d}, situées dans le "\
            "répertoire {I}. Le projet PLATHYNES est {c} dans le répertoire "\
            "{O}: la station {s} est une station d'injection. Tous les "\
            "événements sont considérés. Les codes stations suivent la "\
            "convention Hydro3".format(
                t=args.data_type, c=args.projet_filename,
                s=args.station_name, d=args.dbase_filename,
                I=args.input_dir, O=args.output_dir)
        # =====================================================================

    def test_project_stations_list_file(self):
        """
        Tests PROJET PLATHYNES
        """
        self.stations_list_file = os.path.join(
            self.dir_in, 'list_injection.txt')
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2plathynes.py',
            '-I', self.dir_in,
            '-d', 'plathynes_debitpluie.mdb', 'plathynes_pluie.mdb',
            '-t', 'sacha',
            '-O', self.dir_out,
            '-c', 'plathynes_project.prj',
            '-l', self.stations_list_file,
            '-3'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbase2plathynes()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(
            args.dbase_filename,
            ['plathynes_debitpluie.mdb', 'plathynes_pluie.mdb'])
        self.assertEqual(args.data_type, 'sacha')
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.varname, ['P', 'Q'])
        self.assertEqual(args.projet_filename, 'plathynes_project.prj')
        self.assertEqual(args.events, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for evt in ['201910', '201911']:
            evt_dir = os.path.join(
                self.dir_out,
                'plathynes_project',
                'Ev_{}'.format(evt)
            )
            for filename in os.listdir(evt_dir):
                if not filename.endswith('.evt'):
                    self.assertTrue(filecmp.cmp(
                        os.path.join(evt_dir, filename),
                        os.path.join(self.dir_ctl, filename),
                    ))
                    os.remove(os.path.join(evt_dir, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données nécessaires à PLATHYNES depuis "\
            "les bases de données de type {t} : {d}, situées dans le "\
            "répertoire {I}. Le projet PLATHYNES est {c} dans le répertoire "\
            "{O}: la station {s} est une station d'injection. Tous les "\
            "événements sont considérés. Les codes stations suivent la "\
            "convention Hydro3".format(
                t=args.data_type, c=args.projet_filename,
                s=args.station_name, d=args.dbase_filename,
                I=args.input_dir, O=args.output_dir)
        # =====================================================================

    def test_project_events(self):
        """
        Tests PROJET PLATHYNES - Sous-sélection d'événement
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2plathynes.py',
            '-I', self.dir_in,
            '-d', 'plathynes_debitpluie.mdb', 'plathynes_pluie.mdb',
            '-t', 'sacha',
            '-O', self.dir_out,
            '-c', 'plathynes_project.prj',
            '-s', 'K0030020',
            '-S', '201911',
            '-3'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbase2plathynes()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(
            args.dbase_filename,
            ['plathynes_debitpluie.mdb', 'plathynes_pluie.mdb'])
        self.assertEqual(args.data_type, 'sacha')
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.varname, ['P', 'Q'])
        self.assertEqual(args.projet_filename, 'plathynes_project.prj')
        self.assertEqual(args.events, ['201911'])
        self.assertEqual(args.station_name, 'K0030020')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        evt_dir = os.path.join(
            self.dir_out,
            'plathynes_project',
            'Ev_{}'.format(args.events[0])
        )
        for filename in os.listdir(evt_dir):
            if not filename.endswith('.evt'):
                self.assertTrue(filecmp.cmp(
                    os.path.join(evt_dir, filename),
                    os.path.join(self.dir_ctl, filename),
                ))
                os.remove(os.path.join(evt_dir, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données nécessaires à PLATHYNES depuis "\
            "les bases de données de type {t} : {d}, situées dans le "\
            "répertoire {I}. Le projet PLATHYNES est {c} dans le répertoire "\
            "{O}: la station {s} est une station d'injection. Seul "\
            "l'événement {S} est considéré. Les codes stations suivent la "\
            "convention Hydro3".format(
                t=args.data_type, c=args.projet_filename, S=args.events[0],
                s=args.station_name, d=args.dbase_filename,
                I=args.input_dir, O=args.output_dir)
        # =====================================================================

    def test_project_only_Q(self):
        """
        Tests PROJET PLATHYNES
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2plathynes.py',
            '-I', self.dir_in,
            '-d', 'plathynes_debitpluie.mdb', 'plathynes_pluie.mdb',
            '-t', 'sacha',
            '-O', self.dir_out,
            '-c', 'plathynes_project.prj',
            '-s', 'K0030020',
            '-n', 'Q',
            '-3'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbase2plathynes()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(
            args.dbase_filename,
            ['plathynes_debitpluie.mdb', 'plathynes_pluie.mdb'])
        self.assertEqual(args.data_type, 'sacha')
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.varname, ['Q'])
        self.assertEqual(args.projet_filename, 'plathynes_project.prj')
        self.assertEqual(args.events, None)
        self.assertEqual(args.station_name, 'K0030020')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for evt in ['201910', '201911']:
            evt_dir = os.path.join(
                self.dir_out,
                'plathynes_project',
                'Ev_{}'.format(evt)
            )
            for filename in os.listdir(evt_dir):
                if not filename.endswith('.evt'):
                    self.assertTrue(filecmp.cmp(
                        os.path.join(evt_dir, filename),
                        os.path.join(self.dir_ctl, filename),
                    ))
                    os.remove(os.path.join(evt_dir, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données nécessaires à PLATHYNES depuis "\
            "les bases de données de type {t} : {d}, situées dans le "\
            "répertoire {I}. Le projet PLATHYNES est {c} dans le répertoire "\
            "{O}: la station {s} est une station d'injection. Tous les "\
            "événements sont considérés. Les codes stations suivent la "\
            "convention Hydro3. Seules les données '{n}' sont exportées"\
            "".format(
                t=args.data_type, c=args.projet_filename,
                s=args.station_name, d=args.dbase_filename,
                I=args.input_dir, O=args.output_dir,
                n=args.varname[0])
        # =====================================================================

    def test_project_only_P(self):
        """
        Tests PROJET PLATHYNES
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbase2plathynes.py',
            '-I', self.dir_in,
            '-d', 'plathynes_debitpluie.mdb', 'plathynes_pluie.mdb',
            '-t', 'sacha',
            '-O', self.dir_out,
            '-c', 'plathynes_project.prj',
            '-s', 'K0030020',
            '-n', 'P',
            '-3'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbase2plathynes()
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
        self.assertEqual(
            args.dbase_filename,
            ['plathynes_debitpluie.mdb', 'plathynes_pluie.mdb'])
        self.assertEqual(args.data_type, 'sacha')
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.varname, ['P'])
        self.assertEqual(args.projet_filename, 'plathynes_project.prj')
        self.assertEqual(args.events, None)
        self.assertEqual(args.station_name, 'K0030020')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for evt in ['201910', '201911']:
            evt_dir = os.path.join(
                self.dir_out,
                'plathynes_project',
                'Ev_{}'.format(evt)
            )
            for filename in os.listdir(evt_dir):
                if not filename.endswith('.evt'):
                    self.assertTrue(filecmp.cmp(
                        os.path.join(evt_dir, filename),
                        os.path.join(self.dir_ctl, filename),
                    ))
                    os.remove(os.path.join(evt_dir, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données nécessaires à PLATHYNES depuis "\
            "les bases de données de type {t} : {d}, situées dans le "\
            "répertoire {I}. Le projet PLATHYNES est {c} dans le répertoire "\
            "{O}: la station {s} est une station d'injection. Tous les "\
            "événements sont considérés. Les codes stations suivent la "\
            "convention Hydro3. Seules les données '{n}' sont exportées"\
            "".format(
                t=args.data_type, c=args.projet_filename,
                s=args.station_name, d=args.dbase_filename,
                I=args.input_dir, O=args.output_dir,
                n=args.varname[0])
        # =====================================================================
