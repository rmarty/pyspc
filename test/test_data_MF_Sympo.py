#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Sympo_Data in pyspc.data.meteofrance

To run all tests just type:
    python -m unittest test_data_MF_Sympo

To run only a class test:
    python -m unittest test_data_MF_Sympo.TestSympo_Data

To run only a specific test:
    python -m unittest test_data_MF_Sympo.TestSympo_Data.test_init

"""
# Imports
from datetime import datetime as dt
import filecmp
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.data.meteofrance import Sympo_Data


class TestSympo_Data(unittest.TestCase):
    """
    Sympo_Data class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'mf')
        self.rr3_filename = os.path.join(self.dirname, 'rr3_200811010425')
        self.sympo_filename = os.path.join(self.dirname, 'sympo_201611200658')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        rr3 = Sympo_Data(filename=self.rr3_filename)
        self.assertEqual(rr3.filename, self.rr3_filename)
        self.assertEqual(rr3.version, 'rr3')
        self.assertEqual(rr3.date_prod, dt(2008, 11, 1, 4, 25))
        # =====================================================================
        sympo = Sympo_Data(filename=self.sympo_filename)
        self.assertEqual(sympo.filename, self.sympo_filename)
        self.assertEqual(sympo.version, 'sympo')
        self.assertEqual(sympo.date_prod, dt(2016, 11, 20, 6, 58))
        # =====================================================================
        with self.assertRaises(ValueError):
            Sympo_Data(filename='abcd_200811010425')
        # =====================================================================

    def test_versions(self):
        """
        Test des méta-data des versions RR3/SYMPO
        """
        self.assertEqual(Sympo_Data.get_versions(), ['rr3', 'sympo'])

    def test_rr3(self):
        """
        Test de la lecture d'un fichier RR3
        """
        # =====================================================================
        valid = pnd.DataFrame({
            'ZONE': sorted(['0708', '0709']*6),
            'DATE': list(pnd.date_range(
                dt(2008, 11, 1, 6), dt(2008, 11, 1, 21), freq='3H'))*2,
            'RR3': [9, 14, 14, 14, 14, 14, 5, 11, 11, 11, 11, 11],
            'INTERVAL': ['7/15', '10/20', '10/20', '10/20', '10/20', '10/20',
                         '3/10', '7/15', '7/15', '7/15', '7/15', '7/15'],
        })
        rr3 = Sympo_Data(filename=self.rr3_filename)
        content = rr3.read()
        assert_frame_equal(content, valid)
        # =====================================================================
        tmpfile = os.path.join('data', os.path.basename(self.rr3_filename))
        writer = Sympo_Data(filename=tmpfile)
        writer.write(valid)
        self.assertTrue(filecmp.cmp(
            self.rr3_filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_sympo(self):
        """
        Test de la lecture d'un fichier RR3
        """
        # =====================================================================
        valid = pnd.DataFrame({
            'ZONE': sorted(['0708', '0709']*12),
            'DATE': list(pnd.date_range(
                dt(2016, 11, 21), dt(2016, 11, 22, 9), freq='3H'))*2,
            'RR3': [10.4, 12.8, 27.0, 27.0, 30.0, 28.1, 28.1, 28.1, 31.4, 33.8,
                    33.8, 29.7, 5.3, 7.2, 12.2, 12.2, 15.4, 14.0, 14.0, 14.0,
                    13.4, 17.6, 17.6, 16.5],
            'LPN': [1800, 1900, 2200, 2400, 2400, 2400, 2500, 2500, 2500,
                    2400, 2400, 2300, 1900, 2200, 2400, 2500, 2500, 2600,
                    2700, 2600, 2600, 2600, 2600, 2400],
            'ISO-0C': [2100, 2200, 2500, 2700, 2700, 2700, 2800, 2800, 2800,
                       2700, 2700, 2600, 2200, 2500, 2700, 2800, 2800, 2900,
                       3000, 2900, 2900, 2900, 2900, 2700],
        })
        rr3 = Sympo_Data(filename=self.sympo_filename)
        content = rr3.read()
        assert_frame_equal(content, valid)
        # =====================================================================
        tmpfile = os.path.join('data', os.path.basename(self.sympo_filename))
        writer = Sympo_Data(filename=tmpfile)
        writer.write(valid)
        self.assertTrue(filecmp.cmp(
            self.sympo_filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
