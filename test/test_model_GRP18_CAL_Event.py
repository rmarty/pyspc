#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP_Event in pyspc.model.grp18

To run all tests just type:
    python -m unittest test_model_GRP18_CAL_Event

To run only a class test:
    python -m unittest test_model_GRP18_CAL_Event.TestGRP_Event

To run only a specific test:
    python -m unittest test_model_GRP18_CAL_Event.TestGRP_Event.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import filecmp
import numpy as np
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.model.grp18 import GRP_Event


class TestGRP_Event(unittest.TestCase):
    """
    GRP_Event class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp18', 'cal')
        self.valid_hor = pnd.DataFrame(
            {'Date': pnd.date_range(
                dt(2007, 1, 18, 18), dt(2007, 1, 19, 18), freq='H'),
             'Pluie(mm/h)': [
                 8.48, 3.72, 7.60, 6.32, 9.44, 6.64, 7.88, 8.44, 5.32, 6.64,
                 5.96, 6.72, 7.24, 7.44, 6.60, 6.48, 5.36, 2.96, 1.32, 1.08,
                 0.48, 1.60, 1.00, 0.72, 0.80],
             'Debit(m3/s)': [
                 3.537, 5.150, 7.864, 10.192, 12.464, 14.695, 16.625, 18.250,
                 19.710, 20.432, 21.912, 23.425, 23.550, 24.913, 29.306,
                 33.850, 36.100, 36.367, 34.750, 30.500, 26.928, 24.993,
                 23.685, 22.642, 21.467]},
        )
        self.valid_jour = pnd.DataFrame(
            {'Date': pnd.date_range(
                dt(2007, 1, 14), dt(2007, 1, 26), freq='D'),
             'Pluie(mm/h)': [
                 1.60, 6.88, 0.00, 0.40, 7.84, 70.08, 82.96, 10.48, 9.00, 2.04,
                 1.12, 2.96, 0.04],
             'Debit(m3/s)': [
                 np.nan, 3.5680, 2.9220, np.nan, 2.1830, 4.8860, 24.4490,
                 11.9240, 8.0950, 5.1400, 3.8190, 2.9690, 2.2720]},
        )

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'RH10585x-EVhor1.DAT')
        reader = GRP_Event(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.station, 'RH10585x')
        self.assertEqual(reader.event, 'EVhor1')
        # =====================================================================
        filename = os.path.join(self.dirname, 'RH10585x-EVjour1.DAT')
        reader = GRP_Event(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.station, 'RH10585x')
        self.assertEqual(reader.event, 'EVjour1')
        # =====================================================================

    def test_read_hor(self):
        """
        Test de la lecture d'un fichier GRP Event - Cas HORAIRE
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'RH10585x-EVhor1.DAT')
        reader = GRP_Event(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_hor, df)
        # =====================================================================

    def test_read_jour(self):
        """
        Test de la lecture d'un fichier GRP Event - Cas JOURNALIER
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'RH10585x-EVjour1.DAT')
        reader = GRP_Event(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_jour, df)
        # =====================================================================

    def test_write_hor(self):
        """
        Test de l'écriture d'un fichier GRP_Event - Cas HORAIRE
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'RH10585x-EVhor1.DAT')
        tmpfile = os.path.join('data', 'RH10585x-EVhor1.DAT')
        writer = GRP_Event(filename=tmpfile)
        writer.write(data=self.valid_hor, tdelta=td(hours=1))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_write_jour(self):
        """
        Test de l'écriture d'un fichier GRP_Event - Cas JOURNALIER
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'RH10585x-EVjour1.DAT')
        tmpfile = os.path.join('data', 'RH10585x-EVjour1.DAT')
        writer = GRP_Event(filename=tmpfile)
        writer.write(data=self.valid_jour, tdelta=td(days=1))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
