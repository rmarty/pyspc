#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary dbaseInfo

To run all tests just type:
    python -m unittest test_bin_dbaseInfo

To run only a class test:
    python -m unittest test_bin_dbaseInfo.Test_dbaseInfo

To run only a specific test:
    python -m unittest test_bin_dbaseInfo.Test_dbaseInfo.test_loc_meteo

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import dbaseInfo as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_dbaseInfo(unittest.TestCase):
    """
    dbaseInfo bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'dbaseInfo')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.filename = os.path.join(
            'data', 'metadata', 'refspc', 'BDD_light.sqlite')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.stations_list_file = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)

    def test_loc_meteo(self):
        """
        Test extraction info sur site meteo
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'liste_meteo.txt')
        processArgs = [
            'python',
            '../bin/dbaseInfo.py',
            '-I', os.path.dirname(self.filename),
            '-d', os.path.basename(self.filename),
            '-c', os.path.join(self.dir_out, 'loc_meteo.csv'),
            '-l', self.stations_list_file,
            '-t', 'loc_meteo',
            '-3'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbaseInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'loc_meteo.csv'))
        self.assertEqual(args.db_filename, os.path.basename(self.filename))
        self.assertEqual(args.input_dir, os.path.dirname(self.filename))
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'loc_meteo')
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' des éléments "\
            "listés dans {l} issues du "\
            "référentiel SPC contenues dans le fichier {d} situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.db_filename,
                t=args.data_type, c=args.output_filename,
                l=args.stations_list_file
                )
        # =====================================================================

    def test_stat_meteo(self):
        """
        Test extraction stat sur site meteo
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'liste_meteo.txt')
        processArgs = [
            'python',
            '../bin/dbaseInfo.py',
            '-I', os.path.dirname(self.filename),
            '-d', os.path.basename(self.filename),
            '-c', os.path.join(self.dir_out, 'stat_meteo.csv'),
            '-l', self.stations_list_file,
            '-t', 'stat_meteo',
            '-3'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbaseInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'stat_meteo.csv'))
        self.assertEqual(args.db_filename, os.path.basename(self.filename))
        self.assertEqual(args.input_dir, os.path.dirname(self.filename))
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'stat_meteo')
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' des éléments "\
            "listés dans {l} issues du "\
            "référentiel SPC contenues dans le fichier {d} situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.db_filename,
                t=args.data_type, c=args.output_filename,
                l=args.stations_list_file
                )
        # =====================================================================

    def test_reach(self):
        """
        Test extraction info sur troncon - HYDRO3
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'liste_reach.txt')
        processArgs = [
            'python',
            '../bin/dbaseInfo.py',
            '-I', os.path.dirname(self.filename),
            '-d', os.path.basename(self.filename),
            '-c', os.path.join(self.dir_out, 'reach.csv'),
            '-l', self.stations_list_file,
            '-t', 'reach',
            '-3'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbaseInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'reach.csv'))
        self.assertEqual(args.db_filename, os.path.basename(self.filename))
        self.assertEqual(args.input_dir, os.path.dirname(self.filename))
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'reach')
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' des éléments "\
            "listés dans {l} issues du "\
            "référentiel SPC contenues dans le fichier {d} situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.db_filename,
                t=args.data_type, c=args.output_filename,
                l=args.stations_list_file
                )
        # =====================================================================

    def test_reach_hydro2(self):
        """
        Test extraction info sur troncon - HYDRO2
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'liste_reach.txt')
        processArgs = [
            'python',
            '../bin/dbaseInfo.py',
            '-I', os.path.dirname(self.filename),
            '-d', os.path.basename(self.filename),
            '-c', os.path.join(self.dir_out, 'reach2.csv'),
            '-l', self.stations_list_file,
            '-t', 'reach',
            '-2'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbaseInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'reach2.csv'))
        self.assertEqual(args.db_filename, os.path.basename(self.filename))
        self.assertEqual(args.input_dir, os.path.dirname(self.filename))
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'reach')
        self.assertEqual(args.hydro2, True)
        self.assertEqual(args.hydro3, False)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' des éléments "\
            "listés dans {l} issues du "\
            "référentiel SPC contenues dans le fichier {d} situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.db_filename,
                t=args.data_type, c=args.output_filename,
                l=args.stations_list_file
                )
        # =====================================================================

    def test_stat_hydro(self):
        """
        Test extraction stat sur site hydro - HYDRO3
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/dbaseInfo.py',
            '-I', os.path.dirname(self.filename),
            '-d', os.path.basename(self.filename),
            '-c', os.path.join(self.dir_out, 'stat_hydro.csv'),
            '-s', 'K0260010',
            '-t', 'stat_hydro',
            '-3'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbaseInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'stat_hydro.csv'))
        self.assertEqual(args.db_filename, os.path.basename(self.filename))
        self.assertEqual(args.input_dir, os.path.dirname(self.filename))
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, 'K0260010')
        self.assertEqual(args.data_type, 'stat_hydro')
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' de l'élément "\
            "{s} issues du "\
            "référentiel SPC contenues dans le fichier {d} situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.db_filename,
                t=args.data_type, c=args.output_filename,
                s=args.station_name
                )
        # =====================================================================

    def test_loc_hydro_site(self):
        """
        Test extraction info sur site hydro - HYDRO3
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'liste_site.txt')
        processArgs = [
            'python',
            '../bin/dbaseInfo.py',
            '-I', os.path.dirname(self.filename),
            '-d', os.path.basename(self.filename),
            '-c', os.path.join(self.dir_out, 'loc_hydro_site.csv'),
            '-l', self.stations_list_file,
            '-t', 'loc_hydro',
            '-3'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbaseInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'loc_hydro_site.csv'))
        self.assertEqual(args.db_filename, os.path.basename(self.filename))
        self.assertEqual(args.input_dir, os.path.dirname(self.filename))
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'loc_hydro')
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' des éléments "\
            "listés dans {l} issues du "\
            "référentiel SPC contenues dans le fichier {d} situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.db_filename,
                t=args.data_type, c=args.output_filename,
                l=args.stations_list_file
                )
        # =====================================================================

    def test_loc_hydro_station(self):
        """
        Test extraction info sur station hydro - HYDRO3
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'liste_station.txt')
        processArgs = [
            'python',
            '../bin/dbaseInfo.py',
            '-I', os.path.dirname(self.filename),
            '-d', os.path.basename(self.filename),
            '-c', os.path.join(self.dir_out, 'loc_hydro_station.csv'),
            '-l', self.stations_list_file,
            '-t', 'loc_hydro',
            '-3'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbaseInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'loc_hydro_station.csv'))
        self.assertEqual(args.db_filename, os.path.basename(self.filename))
        self.assertEqual(args.input_dir, os.path.dirname(self.filename))
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'loc_hydro')
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' des éléments "\
            "listés dans {l} issues du "\
            "référentiel SPC contenues dans le fichier {d} situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.db_filename,
                t=args.data_type, c=args.output_filename,
                l=args.stations_list_file
                )
        # =====================================================================

    def test_loc_hydro_capteur(self):
        """
        Test extraction info sur capteur hydro - HYDRO3
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in,
                                               'liste_capteur.txt')
        processArgs = [
            'python',
            '../bin/dbaseInfo.py',
            '-I', os.path.dirname(self.filename),
            '-d', os.path.basename(self.filename),
            '-c', os.path.join(self.dir_out, 'loc_hydro_capteur.csv'),
            '-l', self.stations_list_file,
            '-t', 'loc_hydro',
            '-3'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbaseInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'loc_hydro_capteur.csv'))
        self.assertEqual(args.db_filename, os.path.basename(self.filename))
        self.assertEqual(args.input_dir, os.path.dirname(self.filename))
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'loc_hydro')
        self.assertEqual(args.hydro2, False)
        self.assertEqual(args.hydro3, True)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' des éléments "\
            "listés dans {l} issues du "\
            "référentiel SPC contenues dans le fichier {d} situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.db_filename,
                t=args.data_type, c=args.output_filename,
                l=args.stations_list_file
                )
        # =====================================================================

    def test_loc_hydro_2(self):
        """
        Test extraction info sur site hydro - HYDRO2
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'liste_hydro2.txt')
        processArgs = [
            'python',
            '../bin/dbaseInfo.py',
            '-I', os.path.dirname(self.filename),
            '-d', os.path.basename(self.filename),
            '-c', os.path.join(self.dir_out, 'loc_hydro2.csv'),
            '-l', self.stations_list_file,
            '-t', 'loc_hydro',
            '-2'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.dbaseInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'loc_hydro2.csv'))
        self.assertEqual(args.db_filename, os.path.basename(self.filename))
        self.assertEqual(args.input_dir, os.path.dirname(self.filename))
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'loc_hydro')
        self.assertEqual(args.hydro2, True)
        self.assertEqual(args.hydro3, False)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' des éléments "\
            "listés dans {l} issues du "\
            "référentiel SPC contenues dans le fichier {d} situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.db_filename,
                t=args.data_type, c=args.output_filename,
                l=args.stations_list_file
                )
        # =====================================================================

#    def test_sacha(self):
#        """
#        Test extraction info sur les entités contenues dans base SACHA
#        """
#        # =====================================================================
#        filename = os.path.join('data', 'io', 'dbase', 'sacha_montpezat.mdb')
#        processArgs = [
#            'python',
#            '../bin/dbaseInfo.py',
#            '-I', os.path.dirname(filename),
#            '-d', os.path.basename(filename),
#            '-t', 'sacha',
#            '-v'
#        ]
#        # =====================================================================
#        cline = ' '.join(processArgs).replace('python', '').strip()
#        sys.argv = processArgs[1:]
#        with captured_output():
#            args = _args.dbaseInfo()
#        self.assertEqual(args.output_filename,
#                         os.path.join(self.dir_out, 'loc_hydro2.csv'))
#        self.assertEqual(args.db_filename, os.path.basename(self.filename))
#        self.assertEqual(args.input_dir, os.path.dirname(self.filename))
#        self.assertEqual(args.stations_list_file,
#                         os.path.join(self.dir_in, 'liste_hydro2.txt'))
#        self.assertEqual(args.station_name, None)
#        self.assertEqual(args.data_type, 'loc_hydro')
#        self.assertEqual(args.hydro2, True)
#        self.assertEqual(args.hydro3, False)
##        self.assertEqual(args.verbose, True)
#        # =====================================================================
#        processRun = subprocess.Popen(
#            processArgs, universal_newlines=True,
#            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
#        processRun.wait()
#        # =====================================================================
#        self.assertTrue(filecmp.cmp(
#            args.output_filename,
#            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
#        ))
#        os.remove(args.output_filename)
#        # =====================================================================
##        print(cline)
#        self.cline = cline
#        self.label = "Extraire les méta-données de type '{t}' des éléments "\
#            "listés dans {l} issues du "\
#            "référentiel SPC contenues dans le fichier {d} situé "\
#            "dans le répertoire {I}. Ces informations sont écrites dans le "\
#            "fichier {c}."\
#            "".format(
#                I=args.input_dir, d=args.db_filename,
#                t=args.data_type, c=args.output_filename,
#                l=args.stations_list_file
#                )
#        # =====================================================================
