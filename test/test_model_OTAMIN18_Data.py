#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Data in pyspc.model.otamin18

To run all tests just type:
    python -m unittest test_model_OTAMIN18_Data

To run only a class test:
    python -m unittest test_model_OTAMIN18_Data.TestOtamin_Data

To run only a specific test:
    python -m unittest test_model_OTAMIN18_Data.TestOtamin_Data.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import filecmp
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.model.otamin18 import Data


class TestOtamin_Data(unittest.TestCase):
    """
    Otamin_Data class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        # =====================================================================
        self.dirname = os.path.join('data', 'model', 'otamin18')
        # =====================================================================
        self.valid = pnd.DataFrame({
            ('A6701210', 'Q', '57gGRPd000', 'OBS'):
                [86.7140, 88.6180, 85.9730, 80.4380, 73.7120, 66.0520, 59.5280,
                 52.7420, 48.9270, 46.2150, 43.3490, 41.4390, 39.4690, 36.8390,
                 34.1760, 31.3990, 28.4790, 27.0970, 26.0860, 24.9090, 23.9730,
                 23.6450, 23.2440, 22.8440, ],
            ('A6701210', 'Q', '57gGRPd000', 'PREV'):
                [72.1220, 71.3140, 67.6682, 62.7951, 61.7039, 57.8520, 53.7458,
                 50.3575, 47.0560, 44.7905, 41.7438, 39.4583, 38.2434, 36.7917,
                 35.8940, 34.6435, 34.8361, 33.4631, 31.3334, 29.5052, 27.6166,
                 26.4756, 26.6687, 27.3146],
            },
            index=pnd.date_range(
                dt(2013, 10, 16, 0),
                dt(2013, 10, 16, 23),
                freq='H'
            )
        )
        self.valid.index.name = '# JJ-MM-AAAA HH:MM'
        self.valid.columns.names = ['Stations', 'Grandeurs', 'Modeles', None]
        # =====================================================================

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'A6701210_57gGRPd000_024H.csv')
        station = 'A6701210'
        model = '57gGRPd000'
        leadtime = 24 * td(hours=1)
        reader = Data(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.station, station)
        self.assertEqual(reader.model, model)
        self.assertEqual(reader.leadtime, leadtime)

    def test_basename(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'A6701210_57gGRPd000_024H.csv')
        [station, model, leadtime] = Data.split_basename(
            filename=filename)  # pylint: disable=unbalanced-tuple-unpacking
        self.assertEqual(station, 'A6701210')
        self.assertEqual(model, '57gGRPd000')
        self.assertEqual(leadtime, 24 * td(hours=1))
        # =====================================================================
        station = 'A6701210'
        model = '57gGRPd000'
        leadtime = 24 * td(hours=1)
        filename = Data.join_basename(
            station=station, model=model,
            leadtime=leadtime, timestep=td(hours=1)
        )
        self.assertEqual(filename, 'A6701210_57gGRPd000_024H.csv')
        # =====================================================================

    def test_read(self):
        """
        Test de la lecture
        """
        filename = os.path.join(self.dirname, 'A6701210_57gGRPd000_024H.csv')
        reader = Data(filename=filename)
        df = reader.read()
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(self.valid, df)

    def test_write(self):
        """
        Test de l'écriture
        """
        df = self.valid
        filename = os.path.join(self.dirname, 'A6701210_57gGRPd000_024H.csv')
        tmp_file = os.path.join('data', 'A6701210_57gGRPd000_024H.csv')
        writer = Data(filename=tmp_file)
        writer.write(data=df)
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
