#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_BP in pyspc.io.meteofrance

To run all tests just type:
    python -m unittest test_MF_BP

To run only a class test:
    python -m unittest test_MF_BP.TestMF_BP

To run only a specific test:
    python -m unittest test_MF_BP.TestMF_BP.test_init

"""
# Imports
from datetime import datetime as dt
import os
import unittest

from pyspc.core.series import Series
from pyspc.io.meteofrance import read_BP


class TestMF_BP(unittest.TestCase):
    """
    MF_BP class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'mf')

    def test_read_prevision(self):
        """
        Lecture BP XML version 2016 - prévision
        """
        # =====================================================================
        zones = ['71002', '71005']
        scens = ['MoyInf', 'MoySup', 'LocInf', 'LocSup']
        runtimes = [dt(2016, 5, 31, 5, 2, 5)]
        valid = [(z, 'PJ', (r, 'BP', s, None))
                 for z in zones for s in scens for r in runtimes]
        # =====================================================================
        filename = os.path.join(self.dirname, 'bp_ic_201605310503.xml')
        series = read_BP(filename=filename, zones=zones)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), len(valid))
        for v in valid:
            self.assertIn(v, series)
        # =====================================================================

    def test_read_observation(self):
        """
        Lecture BP XML version 2016 - observation
        """
        # =====================================================================
        zones = ['71002', '71005']
        sims = ['AVGRR', 'MAXRR']
        valid = [(z, 'PJ', s) for z in zones for s in sims]
        # =====================================================================
        filename = os.path.join(self.dirname, 'bp_ic_201605310503.xml')
        series = read_BP(filename=filename, zones=zones, observation=True)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), len(valid))
        for v in valid:
            self.assertIn(v, series)
        # =====================================================================
