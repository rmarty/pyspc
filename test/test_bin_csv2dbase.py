#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary csv2dbase

To run all tests just type:
    python -m unittest test_bin_csv2dbase

To run only a class test:
    python -m unittest test_bin_csv2dbase.Test_csv2dbase

To run only a specific test:
    python -m unittest test_bin_csv2dbase.Test_csv2dbase.test_previ19

"""
# Imports
import os
import shutil
import subprocess
import sys
import unittest

from pyspc.binutils.args import csv2dbase as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_csv2dbase(unittest.TestCase):
    """
    csv2dbase bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'csv2dbase')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_pyspc = os.path.join('data', 'core', 'csv')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.source2 = os.path.join('data', 'prevision_2019.mdb')
        if not os.path.exists(self.source2):
            src_filename = os.path.join(
                os.path.dirname(__file__), '..',
                'resources', 'dbase', 'Prevision_2019.mdb'
            )
            shutil.copy2(src_filename, self.source2)
        self.stations_list_file = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)
        if os.path.exists(self.source2):
            os.remove(self.source2)

    def test_previ19_mdb(self):
        """
        Test Base Prévision 2019 - BRUT
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'list_previ19.txt')
        processArgs = [
            'python',
            '../bin/csv2dbase.py',
            '-I', self.dir_pyspc,
            '-d', os.path.basename(self.source2),
            '-O', os.path.dirname(self.source2),
            '-n', 'QH',
            '-l', self.stations_list_file,
            '-t', 'previ19'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2dbase()
        self.assertEqual(args.dbase_filename, os.path.basename(self.source2))
        self.assertEqual(args.stations_list_file,
                         os.path.join(self.dir_in, 'list_previ19.txt'))
        self.assertEqual(args.varname, ['QH'])
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'previ19')
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.output_dir, os.path.dirname(self.source2))
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        self.assertEqual(os.stat(self.source2).st_size, 331776)
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        self.assertEqual(os.stat(self.source2).st_size, 335872)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Intégration des prévisions de débit horaire ({n}) "\
            "dont les identifiants sont listés dans {l}. "\
            "Les séries sont lues depuis le répertoire {I} "\
            "et sont ajoutées à la base de type '{t}' nommée {d} "\
            "dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, l=args.stations_list_file, n=args.varname,
                O=args.output_dir, d=args.dbase_filename, t=args.data_type,
            )
        # =====================================================================
