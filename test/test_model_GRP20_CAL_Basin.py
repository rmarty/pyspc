#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP_Basin in pyspc.model.grp20

To run all tests just type:
    python -m unittest test_model_GRP20_CAL_Basin

To run only a class test:
    python -m unittest test_model_GRP20_CAL_Basin.TestGRP_Basin

To run only a specific test:
    python -m unittest test_model_GRP20_CAL_Basin.TestGRP_Basin.test_init

"""
# Imports
import filecmp
import os
import unittest

from pyspc.model.grp20 import GRP_Basin


class TestGRP_Basin(unittest.TestCase):
    """
    GRP_Basin class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp20', 'cal')
        self.valid = {
            'E': {
                'RH10585x': {
                        'n': 'Code et ponderation poste ETP   1', 'w': 1.0},
            },
            'P': {
                '90052002': {
                    'n': 'Code, ponderation et pas de temps poste pluvio 1',
                    'w': 0.8, 't': '00J01H00M'},
                '90065003': {
                    'n': 'Code, ponderation et pas de temps poste pluvio 2',
                    'w': 0.2, 't': '00J01H00M'},
            },
            'T': {
                '90035001': {
                    'n': 'Code, ponderation et altitude du poste temperature'
                    ' 1', 'w': 1.0, 'z': 401.0},
                '90052002': {
                    'n': 'Code, ponderation et altitude du poste temperature'
                    ' 2', 'w': 1.0, 'z': 473.0},
                '90065003': {
                    'n': 'Code, ponderation et altitude du poste temperature'
                    ' 3', 'w': 1.0, 'z': 1153.0},
            },
            'N': 1.0,
            'L': {'t': '01J00H00M',
                  'n': 'Duree seuil pour les lacunes (format nnJnnHnnM)'}
        }

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'RH10585x_00J01H00M.DAT')
        basin = GRP_Basin(filename=filename)
        self.assertEqual(basin.filename, filename)
        self.assertEqual(basin.location, 'RH10585x')
        self.assertEqual(basin.timestep, '00J01H00M')

    def test_read(self):
        """
        Test de la lecture du fichier
        """
        filename = os.path.join(self.dirname, 'RH10585x_00J01H00M.DAT')
        basin = GRP_Basin(filename=filename)
        basin.read()
        for tag in self.valid:
            self.assertIn(tag, basin)
            if tag in ['N']:
                self.assertEqual(basin[tag], self.valid[tag])
            elif tag in ['L']:
                self.assertDictEqual(basin[tag], self.valid[tag])
            else:
                for code in self.valid[tag]:
                    self.assertIn(code, basin[tag])
                    self.assertDictEqual(basin[tag][code],
                                         self.valid[tag][code])

    def test_write(self):
        """
        Test de l'écriture du fichier
        """
        filename = os.path.join(self.dirname, 'RH10585x_00J01H00M.DAT')
        tmpfile = os.path.join('data', 'RH10585x_00J01H00M.DAT')
        basin = GRP_Basin(filename=tmpfile)
        basin.update(self.valid)
        basin.write()
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
