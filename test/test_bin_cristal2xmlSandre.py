#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary cristal2xmlSandre

To run all tests just type:
    python -m unittest test_bin_cristal2xmlSandre

To run only a class test:
    python -m unittest test_bin_cristal2xmlSandre.Test_cristal2xmlSandre

To run only a specific test:
    python -m unittest test_bin_cristal2xmlSandre.Test_cristal2xmlSandre.test_data_obs_hydro

"""
# Imports
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import cristal2xmlSandre as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_cristal2xmlSandre(unittest.TestCase):
    """
    cristal2xmlSandre bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'cristal2xmlSandre')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dirname2 = os.path.join('data', 'data', 'cristal')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.stations_list_file = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)

    def test_data_obs_hydro(self):
        """
        Test conversion data_obs_hydro
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/cristal2xmlSandre.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-F', '2008103118',
            '-L', '2008110200',
            '-s', 'K0550010',
            '-U', 'sender', 'me',
            '-U', 'user', 'org',
            '-U', 'target', 'you',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.cristal2xmlSandre()
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.first_dtime, '2008103118')
        self.assertEqual(args.last_dtime, '2008110200')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0550010')
        self.assertEqual(
            args.user_sandre,
            dict([('sender', 'me'), ('user', 'org'), ('target', 'you')]))
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for v in ['HI', 'QI']:
            basename = 'K0550010_data_obs_hydro_{}.xml'.format(v)
#            self.assertTrue(filecmp.cmp(
#                os.path.join(self.dir_out, basename),
#                os.path.join(self.dir_ctl, basename),
#            ))
            self.assertTrue(
                os.path.exists(os.path.join(self.dir_out, basename)))
            os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données de la station {s} "\
            "depuis les archives CRISTAL "\
            "situé dans le répertoire {I} au format xml Sandre "\
            "dans le répertoire {O}. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "Le scénario xml Sandre correspond aux informations utilisateur: "\
            "{U}"\
            "".format(
                I=args.input_dir, s=args.station_name,
                U=args.user_sandre,
                F=args.first_dtime, L=args.last_dtime,
                O=args.output_dir)
        # =====================================================================

    def test_data_obs_meteo(self):
        """
        Test conversion data_obs_meteo
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'pluvios.txt')
        processArgs = [
            'python',
            '../bin/cristal2xmlSandre.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-F', '2008103118',
            '-L', '2008110200',
            '-l', self.stations_list_file
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.cristal2xmlSandre()
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.first_dtime, '2008103118')
        self.assertEqual(args.last_dtime, '2008110200')
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(
            args.user_sandre,
            dict([('sender', 'me'), ('user', 'org'), ('target', 'you')]))
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for v in ['P5m']:
            basename = 'pluvios_data_obs_meteo_{}.xml'.format(v)
#            self.assertTrue(filecmp.cmp(
#                os.path.join(self.dir_out, basename),
#                os.path.join(self.dir_ctl, basename),
#            ))
            self.assertTrue(
                os.path.exists(os.path.join(self.dir_out, basename)))
            os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les données des stations listés dans {l} "\
            "depuis les archives CRISTAL "\
            "situé dans le répertoire {I} au format xml Sandre "\
            "dans le répertoire {O}. "\
            "L'extraction concerne la période du {F} au {L}. "\
            "Le scénario xml Sandre correspond aux informations utilisateur: "\
            "{U}"\
            "".format(
                I=args.input_dir, l=args.stations_list_file,
                U=args.user_sandre,
                F=args.first_dtime, L=args.last_dtime,
                O=args.output_dir)
        # =====================================================================
