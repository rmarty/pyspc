#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_MF_Data in pyspc.io.meteofrance

To run all tests just type:
    python -m unittest test_data_MF_Data

To run only a class test:
    python -m unittest test_data_MF_Data.TestMF_Data

To run only a specific test:
    python -m unittest test_data_MF_Data.TestMF_Data.test_init

"""
# Imports
from datetime import datetime as dt
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.core.series import Series
from pyspc.io.meteofrance import read_MF_Data


class TestMF_Data(unittest.TestCase):
    """
    MF_Data_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'mf')

    def test_RR6(self):
        """
        Test de lecture/écriture de données RR6/P6M
        """
        # =====================================================================
        valid = {
            '43111002': pnd.DataFrame(
                [0.0, 1.0, 0.2, 0.4, 0.4, 0.4, 2.2, 11.7, 13.3, 8.2, 6.3, 13.4,
                 14.2, 12.9, 21.5, 14.1, 17.0, 17.0, 8.0, 2.8, 1.8, 6.1, 11.2,
                 6.6, 3.5, 1.6, 0.4, 0.6, 5.0, 1.8],
                index=pnd.date_range(dt(2017, 6, 13, 17),
                                     dt(2017, 6, 13, 19, 54), freq='6min')
            ),
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'RR6.data')
        series = read_MF_Data(filename=filename)
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            self.assertIn(c, valid)
            valid[c].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[c],
                               check_less_precise=3)
#        print(series)
#        for k in series:
#            s = series[k]
#            print(s.firstdt)
#            print(s.lastdt)
#            print(s.timestep)
#            print("'{}': pnd.DataFrame(".format(s.code))
#            print("    {},".format([int(v*1000)/1000.
#                                    for v in s.data_frame.values]))
#            print("    index=pnd.date_range(dt({}), dt({}), freq='XXXX')"
#                  "".format(
#                      s.firstdt.strftime('%Y, %m, %d, %H, %M'),
#                      s.lastdt.strftime('%Y, %m, %d, %H, %M'),
#                  ))
#            print("),")
#            series[k].data_frame.to_csv(
#                "data/{}_{}.txt".format(k[0], k[1]),
#                sep=';',
#                float_format='%.3f',
#                index=True,
#                index_label='AAAAMMJJHHMM',
#                header=["{}_{}".format(k[0], k[1])],
#                date_format='%Y%m%d%H%M',
#                line_terminator='\n'
#            )
        # =====================================================================

    def test_RR1(self):
        """
        Test de lecture/écriture de données RR1/PH
        """
        # =====================================================================
        valid = {
            '42039003': pnd.DataFrame(
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5.0, 1.4, 1.2, 1.2, 1.6,
                 5.5, 5.2, 6.4, 4.8, 3.6, 4.9, 5.8, 3.6, 1.6, 0.8, 1.2, 0.8,
                 0.2],
                index=pnd.date_range(
                    dt(2010, 11, 14, 12), dt(2010, 11, 15, 12),
                    freq='H')
            ),
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'RR1.data')
        series = read_MF_Data(filename=filename)
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            self.assertIn(c, valid)
            valid[c].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[c],
                               check_less_precise=3)
        # =====================================================================

    def test_RR(self):
        """
        Test de lecture/écriture de données RR/PJ
        """
        # =====================================================================
        valid = {
            '07075001': pnd.DataFrame(
                [0.0, 0.0, 0.0, 0.0, 3.6, 37.0, 2.5, 23.5, 71.0, 0.0],
                index=pnd.date_range(dt(2013, 5, 10), dt(2013, 5, 19),
                                     freq='D')
            ),
            '07105001': pnd.DataFrame(
                [0.2, 0.1, 0.0, 0.2, 2.2, 28.8, 1.6, 26.7, 65.7, 0.1],
                index=pnd.date_range(dt(2013, 5, 10), dt(2013, 5, 19),
                                     freq='D')
            ),
            '07154005': pnd.DataFrame(
                [0.0, 0.0, 0.0, 0.0, 4.6, 42.2, 2.6, 30.0, 74.3, 1.0],
                index=pnd.date_range(dt(2013, 5, 10), dt(2013, 5, 19),
                                     freq='D')
            ),
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'RR.data')
        series = read_MF_Data(filename=filename)
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            self.assertIn(c, valid)
            valid[c].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[c],
                               check_less_precise=3)
        # =====================================================================

    def test_RR_select(self):
        """
        Test de lecture/écriture de données RR/PJ
        """
        # =====================================================================
        valid = {
            '07075001': pnd.DataFrame(
                [0.0, 0.0, 0.0, 0.0, 3.6, 37.0, 2.5, 23.5, 71.0, 0.0],
                index=pnd.date_range(dt(2013, 5, 10), dt(2013, 5, 19),
                                     freq='D')
            ),
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'RR.data')
        series = read_MF_Data(filename=filename, codes=list(valid.keys()))
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            self.assertIn(c, valid)
            valid[c].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[c],
                               check_less_precise=3)
        # =====================================================================

    def test_T(self):
        """
        Test de lecture/écriture de données T/TH
        """
        # =====================================================================
        valid = {
            '21567001': pnd.DataFrame(
                [1.9, 1.2, 0.3, -1.0, -1.4, -1.4, -2.6, -2.6, -2.7, -1.9, -0.5,
                 2.2, 6.0, 7.6, 8.2, 7.4, 6.1, 4.7, 3.7, 2.6, 2.3, 1.8, 0.5,
                 0.1],
                index=pnd.date_range(dt(2000, 2, 4), dt(2000, 2, 4, 23),
                                     freq='H')
            ),
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'T.data')
        series = read_MF_Data(filename=filename)
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            self.assertIn(c, valid)
            valid[c].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[c],
                               check_less_precise=3)
        # =====================================================================

    def test_TNTXM(self):
        """
        Test de lecture/écriture de données TNTXM/TJ
        """
        # =====================================================================
        valid = {
            '03060001': pnd.DataFrame(
                [16.2, 15.1, 9.9, 11.9, 14.3, 12.8, 10.8, 16.4, 15.3],
                index=pnd.date_range(dt(2019, 10, 1), dt(2019, 10, 9),
                                     freq='D')
            ),
            '18125004': pnd.DataFrame(
                [15.9, 15.3, 8.4, 11.3, 15.6, 16.0, 12.3, 16.2, 14.3],
                index=pnd.date_range(dt(2019, 10, 1), dt(2019, 10, 9),
                                     freq='D')
            ),
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'TNTXM.data')
        series = read_MF_Data(filename=filename)
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            self.assertIn(c, valid)
            valid[c].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[c],
                               check_less_precise=3)
        # =====================================================================

    def test_ETPMON(self):
        """
        Test de lecture/écriture de données ETPMON/EJ
        """
        # =====================================================================
        valid = {
            '43111002': pnd.DataFrame(
                [6.5, 5.4, 4.3, 4.8, 3.3, 2.5, 5.2, 4.9, 5.4],
                index=pnd.date_range(dt(2018, 7, 1), dt(2018, 7, 9), freq='D')
            ),
            '63113001': pnd.DataFrame(
                [7.6, 6.4, 5.7, 4.2, 3.9, 4.3, 5.6, 5.9, 5.9],
                index=pnd.date_range(dt(2018, 7, 1), dt(2018, 7, 9), freq='D')
            ),
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'ETPMON.data')
        series = read_MF_Data(filename=filename)
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            self.assertIn(c, valid)
            valid[c].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[c],
                               check_less_precise=3)
        # =====================================================================
