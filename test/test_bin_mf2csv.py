#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary mf2csv

To run all tests just type:
    python -m unittest test_bin_mf2csv

To run only a class test:
    python -m unittest test_bin_mf2csv.Test_mf2csv

To run only a specific test:
    python -m unittest test_bin_mf2csv.Test_mf2csv.test_data_PJ

"""
# Imports
import filecmp
import os
import sys
import unittest

from pyspc.binutils.args import mf2csv as _args
from pyspc.binutils.runs import mf2csv as _runs
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_mf2csv(unittest.TestCase):
    """
    mf2csv bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'mf2csv')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dirname2 = os.path.join('data', 'data', 'mf')
        cls.dirname3 = os.path.join('data', 'data', 'mf', 'open_data')
        cls.dirname4 = os.path.join('data', 'data', 'mf', 'open_api')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_openapi_pyspc_MN(self):
        """
        Test conversion Données Minutes
        """
        # ===================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname4,
            '-O', self.dir_out,
            '-d', '43091005_MN_202410170500_202410170700.csv',
            '-t', 'MF_OpenAPI',
            '-C', 'pyspc',
            '-v'
        ]
        # ===================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename,
                         '43091005_MN_202410170500_202410170700.csv')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'MF_OpenAPI')
        self.assertEqual(args.input_dir, self.dirname4)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # ===================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, 'open_api', basename)))
            os.remove(f)
        # ===================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} de l'api de Météo-France "\
            "('{t}') situé dans le répertoire {I} au format csv de type "\
            "'{C}' dans le répertoire {O}. Un seul fichier est créé. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # ===================================================================

    def test_openapi_pyspc_H(self):
        """
        Test conversion Données Horaires
        """
        # ===================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname4,
            '-O', self.dir_out,
            '-d', '43091005_H_202410170000_202410171200.csv',
            '-t', 'MF_OpenAPI',
            '-C', 'pyspc',
            '-v'
        ]
        # ===================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename,
                         '43091005_H_202410170000_202410171200.csv')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'MF_OpenAPI')
        self.assertEqual(args.input_dir, self.dirname4)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # ===================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, 'open_api', basename)))
            os.remove(f)
        # ===================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} de l'api de Météo-France "\
            "('{t}') situé dans le répertoire {I} au format csv de type "\
            "'{C}' dans le répertoire {O}. Un seul fichier est créé. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # ===================================================================

    def test_openapi_pyspc_Q(self):
        """
        Test conversion Données Quotidiennes
        """
        # ===================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname4,
            '-O', self.dir_out,
            '-d', '43091005_Q_202410150000_202410190000.csv',
            '-t', 'MF_OpenAPI',
            '-C', 'pyspc',
            '-v'
        ]
        # ===================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename,
                         '43091005_Q_202410150000_202410190000.csv')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'MF_OpenAPI')
        self.assertEqual(args.input_dir, self.dirname4)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # ===================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, 'open_api', basename)))
            os.remove(f)
        # ===================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} de l'api de Météo-France "\
            "('{t}') situé dans le répertoire {I} au format csv de type "\
            "'{C}' dans le répertoire {O}. Un seul fichier est créé. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # ===================================================================

    def test_opendata_pyspc_MN(self):
        """
        Test conversion Données Minutes
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname3,
            '-O', self.dir_out,
            '-d', 'MN_43_previous-2020-2022.csv.gz',
            '-t', 'MF_OpenData',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename,
                         'MN_43_previous-2020-2022.csv.gz')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'MF_OpenData')
        self.assertEqual(args.input_dir, self.dirname3)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, basename)))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} du site meteo.data.gouv.fr "\
            "('{t}') situé dans le répertoire {I} au format csv de type "\
            "'{C}' dans le répertoire {O}. Un seul fichier est créé. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_opendata_pyspc_H(self):
        """
        Test conversion Données Horaires
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname3,
            '-O', self.dir_out,
            '-d', 'H_43_2010-2019.csv.gz',
            '-t', 'MF_OpenData',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename, 'H_43_2010-2019.csv.gz')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'MF_OpenData')
        self.assertEqual(args.input_dir, self.dirname3)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, basename)))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} du site meteo.data.gouv.fr "\
            "('{t}') situé dans le répertoire {I} au format csv de type "\
            "'{C}' dans le répertoire {O}. Un seul fichier est créé. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_opendata_pyspc_Q(self):
        """
        Test conversion Données Journalières
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname3,
            '-O', self.dir_out,
            '-d', 'Q_07_latest-2023-2024_RR-T-Vent.csv.gz',
            '-t', 'MF_OpenData',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename,
                         'Q_07_latest-2023-2024_RR-T-Vent.csv.gz')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'MF_OpenData')
        self.assertEqual(args.input_dir, self.dirname3)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, basename)))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} du site meteo.data.gouv.fr "\
            "('{t}') situé dans le répertoire {I} au format csv de type "\
            "'{C}' dans le répertoire {O}. Un seul fichier est créé. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_opendata_pyspc_Qselect(self):
        """
        Test conversion Données Journalières avec sélection station
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname3,
            '-O', self.dir_out,
            '-d', 'Q_07_latest-2023-2024_RR-T-Vent.csv.gz',
            '-s', '07154005',
            '-t', 'MF_OpenData',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename,
                         'Q_07_latest-2023-2024_RR-T-Vent.csv.gz')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, '07154005')
        self.assertEqual(args.data_type, 'MF_OpenData')
        self.assertEqual(args.input_dir, self.dirname3)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, basename)))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} du site meteo.data.gouv.fr "\
            "('{t}') situé dans le répertoire {I} au format csv de type "\
            "'{C}' dans le répertoire {O}. Un seul fichier est créé et "\
            "seule la station {s} est extraite. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type, s=args.station_name,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_data_pyspc_PJ(self):
        """
        Test conversion Pluie Journalière (RR)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'RR.data',
            '-t', 'data',
            '-s', '07075001',
            '-C', 'pyspc',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename, 'RR.data')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, '07075001')
        self.assertEqual(args.data_type, 'data')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, basename)))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} de la Publithèque ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Un seul fichier est créé et "\
            "seule la station {s} est extraite. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type, s=args.station_name,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_data_pyspc_TJ(self):
        """
        Test conversion Température Journalière (RR)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'TNTXM.data',
            '-t', 'data',
            '-s', '03060001',
            '-C', 'pyspc',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename, 'TNTXM.data')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, '03060001')
        self.assertEqual(args.data_type, 'data')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, basename)))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} de la Publithèque ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Un seul fichier est créé et "\
            "seule la station {s} est extraite. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type, s=args.station_name,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_data_grp16_PH(self):
        """
        Test conversion Pluie Horaire (RR1)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'RR1.data',
            '-t', 'data',
            '-s', '42039003',
            '-C', 'grp16',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename, 'RR1.data')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, '42039003')
        self.assertEqual(args.data_type, 'data')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'grp16')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, basename)))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} de la Publithèque ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Un seul fichier est créé et "\
            "seule la station {s} est extraite. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type, s=args.station_name,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_data_grp16_TH(self):
        """
        Test conversion Température Horaire (RR)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'T.data',
            '-t', 'data',
            '-s', '21567001',
            '-C', 'grp16',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename, 'T.data')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, '21567001')
        self.assertEqual(args.data_type, 'data')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'grp16')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, basename)))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} de la Publithèque ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Un seul fichier est créé et "\
            "seule la station {s} est extraite. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type, s=args.station_name,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_data_grp18_P6m(self):
        """
        Test conversion Pluie 6-min (RR6)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'RR6.data',
            '-t', 'data',
            '-s', '43111002',
            '-C', 'grp18',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename, 'RR6.data')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, '43111002')
        self.assertEqual(args.data_type, 'data')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'grp18')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, basename)))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} de la Publithèque ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Un seul fichier est créé et "\
            "seule la station {s} est extraite. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type, s=args.station_name,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_data_grp18_EJ(self):
        """
        Test conversion ETP Journalière (ETPMON)
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'ETPMON.data',
            '-t', 'data',
            '-s', '63113001',
            '-C', 'grp18',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename, 'ETPMON.data')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, '63113001')
        self.assertEqual(args.data_type, 'data')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'grp18')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, basename)))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} de la Publithèque ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Un seul fichier est créé et "\
            "seule la station {s} est extraite. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type, s=args.station_name,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_bp_00(self):
        """
        Test conversion archive BP
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'bp_ic_201605310503.xml',
            '-t', 'bp',
            '-s', '71005',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename, 'bp_ic_201605310503.xml')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, '71005')
        self.assertEqual(args.data_type, 'bp')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, basename)))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} d'archive BP ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Un seul fichier est créé et "\
            "seule la zone {s} est extraite. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type, s=args.station_name,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_bp_66(self):
        """
        Test conversion archive BP
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'bp_ly_200811010718.xml',
            '-t', 'bp',
            '-s', '403',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename, 'bp_ly_200811010718.xml')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, '403')
        self.assertEqual(args.data_type, 'bp')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, basename)))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} d'archive BP ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Un seul fichier est créé et "\
            "seule la zone {s} est extraite. "\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type, s=args.station_name,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_sympo(self):
        """
        Test conversion archive SYMPO
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/mf2csv.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'sympo_201611200658',
            '-t', 'sympo',
            '-s', '0708',
            '-C', 'pyspc',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2csv()
        self.assertEqual(args.mfdata_filename, 'sympo_201611200658')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, '0708')
        self.assertEqual(args.data_type, 'sympo')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2csv(args)
        for f in filenames:
            basename = os.path.basename(f)
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, basename)))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir le fichier {d} d'archive SYMPO ('{t}') "\
            "situé dans le répertoire {I} au format csv de type '{C}' "\
            "dans le répertoire {O}. Seule la zone {s} est extraite."\
            "".format(
                I=args.input_dir, d=args.mfdata_filename,
                t=args.data_type, s=args.station_name,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================
