#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyspc>.
# Copyright (C) 2013-2021  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Config in pyspc.core.config

To run all tests just type:
    python -m unittest test_core_Config

To run only a class test:
    python -m unittest test_core_Config.TestConfig

To run only a specific test:
    python -m unittest test_core_Config.TestConfig.test_init

"""
# Imports
import collections
from datetime import datetime as dt
from functools import partial
import filecmp
import os
import unittest

# Imports pyspc
from pyspc.core.config import Config


class TestConfig(unittest.TestCase):
    """
    Config class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'core', 'config')
        self.source = os.path.join(self.dirname, 'scores_133.cfg')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        cfg = Config(filename=self.source)
        self.assertEqual(cfg.filename, self.source)
        self.assertEqual(cfg, collections.OrderedDict())

    def test_convert(self):
        """
        test conversion
        """
        # =====================================================================
        str_values = {
            ('barrage', 'code'): 'K001002010',
            ('barrage', 'nom'): 'Barrage-de-la-Palisse',
            ('barrage', 'Z0'): '1010',
            ('barrage', 'bareme_fichier'): 'data/bareme/llp_light.txt',
            ('barrage', 'bareme_cols'): 'Z;Qd;Vd1;V;V+0.5*Vd1',
            ('scs', 'lameobs'): '07235005,0.5;07154005,0.5',
            ('scs', 'lameprv'): 'K0000000,1.39',
            ('scs', 'qini_prof'): '24',
            ('scs', 'qini_code'): 'K0010010',
            ('scs', 'qini_min'): '0.100',
            ('scs', 'P0_auto'): 'int(-30.46*math.log(qini)+68.67)',
            ('scs', 'P0_min'): '0',
            ('scs', 'P0_max'): '120',
            ('scs', 'Smax'): '400',
            ('scs', 'Tbase'): '5',
            ('scs', 'Surface'): '130',
            ('stations', 'in'): '07235005;07154005;K0010010;K001002010',
            ('stations', 'mod'): 'K0010020;K001002010',
            ('stations', 'out'): 'K001002010',
            ('stations', 'delta_scen_plu'): '-3000',
            ('stations', 'delta_scen_brut'): '100',
            ('stations', 'delta_abaq_plu'): '-30000',
            ('stations', 'delta_abaq_brut'): '1000',
            ('transfert', 'modeles_amont'): 'LLP;GMP',
            ('transfert', 'Tbase'): '3',
            ('transfert', 'nom'): 'Pont de la Borie EDF',
        }
        # =====================================================================
        conv_values = {
            ('barrage', 'Z0'): float,
            ('barrage', 'bareme_fichier'): partial(Config.to_path, ''),
            ('barrage', 'bareme_cols'): Config.to_listofstr,
            ('scs', 'lameobs'): Config.to_dictoffloat,
            ('scs', 'lameprv'): Config.to_dictoffloat,
            ('scs', 'qini_prof'): float,
            ('scs', 'qini_min'): float,
            ('scs', 'P0_min'): Config.to_int,
            ('scs', 'P0_max'): Config.to_int,
            ('scs', 'Smax'): Config.to_int,
            ('scs', 'Tbase'): Config.to_int,
            ('scs', 'Surface'): float,
            ('stations', 'in'): Config.to_listofstr,
            ('stations', 'mod'): Config.to_listofstr,
            ('stations', 'out'): Config.to_listofstr,
            ('stations', 'delta_scen_plu'): Config.to_int,
            ('stations', 'delta_scen_brut'): Config.to_int,
            ('stations', 'delta_abaq_plu'): Config.to_int,
            ('stations', 'delta_abaq_brut'): Config.to_int,
            ('transfert', 'modeles_amont'): Config.to_listofstr,
            ('transfert', 'Tbase'): Config.to_int
        }
        # =====================================================================
        valid_values = {
            ('barrage', 'code'): 'K001002010',
            ('barrage', 'nom'): 'Barrage-de-la-Palisse',
            ('barrage', 'Z0'): 1010,
            ('barrage', 'bareme_fichier'): 'data/bareme/llp_light.txt',
            ('barrage', 'bareme_cols'): ['Z', 'Qd', 'Vd1', 'V', 'V+0.5*Vd1'],
            ('scs', 'lameobs'): {'07235005': 0.5, '07154005': 0.5},
            ('scs', 'lameprv'): {'K0000000': 1.39},
            ('scs', 'qini_prof'): 24,
            ('scs', 'qini_code'): 'K0010010',
            ('scs', 'qini_min'): 0.100,
            ('scs', 'P0_auto'): 'int(-30.46*math.log(qini)+68.67)',
            ('scs', 'P0_min'): 0,
            ('scs', 'P0_max'): 120,
            ('scs', 'Smax'): 400,
            ('scs', 'Tbase'): 5,
            ('scs', 'Surface'): 130.0,
            ('stations', 'in'): ['07235005', '07154005',
                                 'K0010010', 'K001002010'],
            ('stations', 'mod'): ['K0010020', 'K001002010'],
            ('stations', 'out'): ['K001002010'],
            ('stations', 'delta_scen_plu'): -3000,
            ('stations', 'delta_scen_brut'): 100,
            ('stations', 'delta_abaq_plu'): -30000,
            ('stations', 'delta_abaq_brut'): 1000,
            ('transfert', 'modeles_amont'): ['LLP', 'GMP'],
            ('transfert', 'Tbase'): 3,
            ('transfert', 'nom'): 'Pont de la Borie EDF',
        }
        # =====================================================================
        config = Config()
        with self.assertRaises(ValueError):
            config.convert()
        # =====================================================================
        for key, value in str_values.items():
            section = key[0]
            option = key[1]
            config.setdefault(section, {})
            config[section].setdefault(option, value)
        # =====================================================================
        with self.assertRaises(ValueError):
            config.convert()
        config.convert(functions=conv_values)
        for k, v in valid_values.items():
            self.assertEqual(
                config[k[0]][k[1]],
                v
            )
        # =====================================================================

    def test_list_so(self):
        """
        Test liste Sections / Options
        """
        # =====================================================================
        values = {
            ('barrage', 'code'): 'K001002010',
            ('barrage', 'nom'): 'Barrage-de-la-Palisse',
            ('barrage', 'Z0'): '1010',
            ('barrage', 'bareme_fichier'): '../bareme/llp_light.txt',
            ('barrage', 'bareme_cols'): 'Z;Qd;Vd1;V;V+0.5*Vd1',
        }
        config = Config()
        # =====================================================================
        for key, value in values.items():
            section = key[0]
            option = key[1]
            config.setdefault(section, {})
            config[section].setdefault(option, value)
        keys = config.list_sections_options()
        self.assertTrue(keys)
        self.assertListEqual(sorted(list(values.keys())), sorted(keys))
        # =====================================================================

    def test_read(self):
        """
        Test de lecture
        """
        valid_values = {
            ('GENERAL', 'ENTITE'): 'K5433020',
            ('GENERAL', 'GRANDEUR'): 'Q',
            ('OBSERVATION', 'DONNEES_OBSERVATION'):
                'obs/fichier_10.csv, obs/fichier_20.csv',
            ('PREVISION', 'LISTE_ECHEANCE'): '0, 6, 12, 24, 30, 36, 48, 72',
        }
        cfg = Config(filename=self.source)
        cfg.read()
        for k, v in valid_values.items():
            self.assertEqual(
                cfg[k[0]][k[1]],
                v
            )

    def test_update_dict(self):
        """Test update"""
        # =====================================================================
        new = {
            ('barrage', 'code'): 'abcde',
            ('barrage', 'nom_court'): 'LLP'
        }
        values = {
            ('barrage', 'code'): 'K001002010',
            ('barrage', 'nom'): 'Barrage-de-la-Palisse',
            ('barrage', 'Z0'): '1010',
            ('barrage', 'bareme_fichier'): '../bareme/llp_light.txt',
            ('barrage', 'bareme_cols'): 'Z;Qd;Vd1;V;V+0.5*Vd1',
        }
        config = Config()
        for key, value in values.items():
            section = key[0]
            option = key[1]
            config.setdefault(section, {})
            config[section].setdefault(option, value)
        # =====================================================================
        valid = {
            ('barrage', 'code'): 'abcde',
            ('barrage', 'nom'): 'Barrage-de-la-Palisse',
            ('barrage', 'Z0'): '1010',
            ('barrage', 'bareme_fichier'): '../bareme/llp_light.txt',
            ('barrage', 'bareme_cols'): 'Z;Qd;Vd1;V;V+0.5*Vd1',
        }
        config.update_config(config=new, strict=True)
        for k, v in valid.items():
            self.assertEqual(
                config[k[0]][k[1]],
                v
            )
        # =====================================================================
        valid = {
            ('barrage', 'code'): 'abcde',
            ('barrage', 'nom'): 'Barrage-de-la-Palisse',
            ('barrage', 'Z0'): '1010',
            ('barrage', 'bareme_fichier'): '../bareme/llp_light.txt',
            ('barrage', 'bareme_cols'): 'Z;Qd;Vd1;V;V+0.5*Vd1',
            ('barrage', 'nom_court'): 'LLP'
        }
        config.update_config(config=new, strict=False)
        for k, v in valid.items():
            self.assertEqual(
                config[k[0]][k[1]],
                v
            )
        # =====================================================================

    def test_update_Config(self):
        """Test maj"""
        # =====================================================================
        values = {
            ('barrage', 'code'): 'K001002010',
            ('barrage', 'nom'): 'Barrage-de-la-Palisse',
            ('barrage', 'Z0'): '1010',
            ('barrage', 'bareme_fichier'): '../bareme/llp_light.txt',
            ('barrage', 'bareme_cols'): 'Z;Qd;Vd1;V;V+0.5*Vd1',
        }
        config = Config()
        for key, value in values.items():
            section = key[0]
            option = key[1]
            config.setdefault(section, {})
            config[section].setdefault(option, value)
        # =====================================================================
        new = {
            ('barrage', 'code'): 'abcde',
            ('barrage', 'nom_court'): 'LLP'
        }
        newconfig = Config()
        for key, value in new.items():
            section = key[0]
            option = key[1]
            newconfig.setdefault(section, {})
            newconfig[section].setdefault(option, value)
        # =====================================================================
        valid = {
            ('barrage', 'code'): 'abcde',
            ('barrage', 'nom'): 'Barrage-de-la-Palisse',
            ('barrage', 'Z0'): '1010',
            ('barrage', 'bareme_fichier'): '../bareme/llp_light.txt',
            ('barrage', 'bareme_cols'): 'Z;Qd;Vd1;V;V+0.5*Vd1',
        }
        config.update_config(config=newconfig, strict=True)
        for k, v in valid.items():
            self.assertEqual(
                config[k[0]][k[1]],
                v
            )
        # =====================================================================
        valid = {
            ('barrage', 'code'): 'abcde',
            ('barrage', 'nom'): 'Barrage-de-la-Palisse',
            ('barrage', 'Z0'): '1010',
            ('barrage', 'bareme_fichier'): '../bareme/llp_light.txt',
            ('barrage', 'bareme_cols'): 'Z;Qd;Vd1;V;V+0.5*Vd1',
            ('barrage', 'nom_court'): 'LLP'
        }
        config.update_config(config=newconfig, strict=False)
        for k, v in valid.items():
            self.assertEqual(
                config[k[0]][k[1]],
                v
            )
        # =====================================================================

    def test_write(self):
        """
        Test de l'écriture
        """
        tmpfile = os.path.join('data', 'scores_133.cfg')
        cfg = Config(filename=self.source)
        cfg.read()
        cfg2 = Config(filename=tmpfile)
        for k, v in cfg.items():
            cfg2.setdefault(k, v)
        cfg2.write()
        self.assertTrue(filecmp.cmp(
            self.source,
            tmpfile
        ))
        os.remove(tmpfile)

    def test_write_func(self):
        """
        Test de l'écriture avec fonction de conversion en str
        """
        tmpfile = os.path.join('data', 'write_func.cfg')
        cfg = Config(filename=tmpfile)
        cfg.setdefault(('a', 'b'), {'1': 1, 2.1: '2b'})
        cfg.setdefault(('a', 'c'), {'1': 1, 2.1: '2c'})
        cfg.write(func_sec=lambda x: '_'.join(list(x)),
                  func_opt=lambda x: str(int(x)))
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(tmpfile)),
            tmpfile
        ))
        os.remove(tmpfile)

    def test_csv(self):
        """
        Test lecture/écriture CSV
        """
        # =====================================================================
        valid = {
            '31008': {
                'n_id_bap': '31008',
                'lib_ap': 'Indrois Fouzon',
                'id_dir': 'rn',
                'id_spc': '10',
            },
            '41005': {
                'n_id_bap': '41005',
                'lib_ap': 'Source Loire',
                'id_dir': 'ly',
                'id_spc': '10',
            },
            '21018': {
                'n_id_bap': '21018',
                'lib_ap': 'Arroux amont',
                'id_dir': 'ne',
                'id_spc': '10',
            },
        }
        sectionname = 'n_id_bap'
        # =====================================================================
        csvfile = os.path.join(self.dirname, 'csv', 'test_from_csv.csv')
        cfg = Config.from_csv(filename=csvfile, sectionname=sectionname)
        self.assertIsInstance(cfg, Config)
        self.assertEqual(len(cfg), 3)
        for k, v in valid.items():
            self.assertIn(k, cfg)
            self.assertEqual(cfg[k], v)
        # =====================================================================
        tmpfile = os.path.join('data', os.path.basename(csvfile))
        cfg.to_csv(filename=tmpfile)
        self.assertTrue(filecmp.cmp(
            csvfile,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        for s in cfg:
            del cfg[s][sectionname]
        cfg.to_csv(filename=tmpfile, sectionname=sectionname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, 'csv', 'test_to_csv.csv'),
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_from_multitxt(self):
        """
        Test lecture/écriture Multi-TXT
        """
        # =====================================================================
        dirname = os.path.join(self.dirname, 'multitxt')
        # =====================================================================
        valid_values = {
            ('barrage', 'code'): 'K001002010',
            ('barrage', 'nom'): 'Barrage-de-la-Palisse',
            ('barrage', 'Z0'): '1010',
            ('barrage', 'bareme_fichier'): '../bareme/llp_light.txt',
            ('barrage', 'bareme_cols'): 'Z;Qd;Vd1;V;V+0.5*Vd1',
        }
        filenames = os.path.join(dirname, 'barrage.txt')
        config = Config.from_multitxt(filenames=filenames)
        self.assertIsInstance(config, Config)
        for k, v in valid_values.items():
            self.assertEqual(
                config[k[0]][k[1]],
                v
            )
        # =====================================================================
        valid_values = {
            ('barrage', 'code'): 'K001002010',
            ('barrage', 'nom'): 'Barrage-de-la-Palisse',
            ('barrage', 'Z0'): '1010',
            ('barrage', 'bareme_fichier'): '../bareme/llp_light.txt',
            ('barrage', 'bareme_cols'): 'Z;Qd;Vd1;V;V+0.5*Vd1',
            ('scs', 'lameobs'): '07235005,0.5;07154005,0.5',
            ('scs', 'lameprv'): 'K0000000,1.39',
            ('scs', 'qini_prof'): '24',
            ('scs', 'qini_code'): 'K0010010',
            ('scs', 'qini_min'): '0.100',
            ('scs', 'P0_auto'): 'int(-30.46*math.log(qini)+68.67)',
            ('scs', 'P0_min'): '0',
            ('scs', 'P0_max'): '120',
            ('scs', 'Smax'): '400',
            ('scs', 'Tbase'): '5',
            ('scs', 'Surface'): '130',
            ('stations', 'in'): '07235005;07154005;K0010010;K001002010',
            ('stations', 'mod'): 'K0010020;K001002010',
            ('stations', 'out'): 'K001002010',
            ('stations', 'delta_scen_plu'): '-3000',
            ('stations', 'delta_scen_brut'): '100',
            ('stations', 'delta_abaq_plu'): '-30000',
            ('stations', 'delta_abaq_brut'): '1000',
            ('transfert', 'modeles_amont'): 'LLP;GMP',
            ('transfert', 'Tbase'): '3',
            ('transfert', 'nom'): 'Pont de la Borie EDF',
        }
        filenames = [
            os.path.join(dirname, 'barrage.txt'),
            os.path.join(dirname, 'scs.txt'),
            os.path.join(dirname, 'stations.txt'),
            os.path.join(dirname, 'transfert.txt')
        ]
        config = Config.from_multitxt(filenames=filenames)
        self.assertIsInstance(config, Config)
        for k, v in valid_values.items():
            self.assertEqual(
                config[k[0]][k[1]],
                v
            )
        # =====================================================================
        filenames = {
            'barrage': os.path.join(dirname, 'barrage.txt'),
            'scs': os.path.join(dirname, 'scs.txt'),
            'stations': os.path.join(dirname, 'stations.txt'),
            'transfert': os.path.join(dirname, 'transfert.txt')
        }
        config = Config.from_multitxt(filenames=filenames)
        self.assertIsInstance(config, Config)
        for k, v in valid_values.items():
            self.assertEqual(
                config[k[0]][k[1]],
                v
            )
        # =====================================================================
        tmpfiles = config.to_multitxt(dirname='data')
        for tf in tmpfiles:
            self.assertTrue(filecmp.cmp(
                os.path.join(dirname, os.path.basename(tf)),
                tf
            ))
            os.remove(tf)
        # =====================================================================


class Test_Functions(unittest.TestCase):
    """Test _config > Fonctions"""

    def test_listof(self):
        """Test Liste de ..."""
        # =====================================================================
        self.assertEqual(Config.to_listofint('1;2.0;3'), [1, 2, 3])
        self.assertEqual(Config.to_listofint('1,2.0,3', sep=','), [1, 2, 3])
        self.assertEqual(Config.to_listoffloat('1.0;2;3.5'), [1.0, 2.0, 3.5])
        self.assertEqual(Config.to_listofstr('1;2.0;3'), ['1', '2.0', '3'])
        self.assertEqual(
            Config.to_listofintorstr('Z;H;20;40;60;100;150;200'),
            ['Z', 'H', 20, 40, 60, 100, 150, 200]
        )
        # =====================================================================
        self.assertEqual(Config.from_listofint([1, 2, 3]), '1;2;3')
        self.assertEqual(Config.from_listofint([1, 2, 3], sep=','), '1,2,3')
        self.assertEqual(
            Config.from_listoffloat([1.0, 2.0, 3.5]), '1.0;2.0;3.5')
        self.assertEqual(Config.from_listofstr(['1', '2.0', '3']), '1;2.0;3')
        self.assertEqual(
            Config.from_listofintorstr(['Z', 'H', 20, 40, 60, 100, 150, 200]),
            'Z;H;20;40;60;100;150;200'
        )
        # =====================================================================

    def test_dictof(self):
        """Test Dictionnaire de ..."""
        # =====================================================================
        self.assertEqual(
            Config.to_dictofint('a,1;b,2.0;c,3'),
            {'a': 1, 'b': 2, 'c': 3}
        )
        self.assertEqual(
            Config.to_dictoffloat('a,1.0;b,2;c,3.5'),
            {'a': 1.0, 'b': 2.0, 'c': 3.5}
        )
        self.assertEqual(
            Config.to_dictofstr('a,1.0;b,2;c,3.5'),
            {'a': '1.0', 'b': '2', 'c': '3.5'}
        )
        # =====================================================================
        self.assertEqual(
            Config.from_dictofint({'a': 1, 'b': 2, 'c': 3}),
            'a,1;b,2;c,3'
        )
        self.assertEqual(
            Config.from_dictoffloat({'a': 1.0, 'b': 2.0, 'c': 3.5}),
            'a,1.0;b,2.0;c,3.5'
        )
        self.assertEqual(
            Config.from_dictofstr({'a': '1.0', 'b': '2', 'c': '3.5'}),
            'a,1.0;b,2;c,3.5'
        )
        # =====================================================================

    def test_bool(self):
        """Test booléen"""
        # =====================================================================
        self.assertTrue(Config.to_bool('True'))
        self.assertTrue(Config.to_bool('true'))
        self.assertTrue(Config.to_bool('TRUE'))
        self.assertTrue(Config.to_bool('Yes'))
        self.assertTrue(Config.to_bool('yes'))
        self.assertTrue(Config.to_bool('YES'))
        self.assertTrue(Config.to_bool('Oui'))
        self.assertTrue(Config.to_bool('oui'))
        self.assertTrue(Config.to_bool('OUI'))
        self.assertFalse(Config.to_bool('toto'))
        self.assertFalse(Config.to_bool('123'))
        self.assertFalse(Config.to_bool('ouioui'))
        # =====================================================================
        self.assertEqual(Config.from_bool(True), 'True')
        # =====================================================================

    def test_numeric(self):
        """Test numerique"""
        # =====================================================================
        self.assertEqual(Config.to_int('1'), 1)
        self.assertEqual(Config.to_int('1.00'), 1)
        self.assertEqual(Config.to_int('01'), 1)
        self.assertEqual(Config.to_int('01.00'), 1)
        self.assertEqual(Config.to_float('1'), 1.0)
        self.assertEqual(Config.to_float('1.00'), 1.0)
        self.assertEqual(Config.to_float('01'), 1.0)
        self.assertEqual(Config.to_float('01.00'), 1.0)
        # =====================================================================
        self.assertEqual(Config.from_int(1), '1')
        self.assertEqual(Config.from_int(1, fmt='{0:03d}'), '001')
        self.assertEqual(Config.from_float(1.2345), '1.2345')
        self.assertEqual(Config.from_float(1.2345, fmt='{0:.2f}'), '1.23')
        # =====================================================================

    def test_path(self):
        """Test chemin"""
        # =====================================================================
        self.assertEqual(Config.to_path('a', '1'), os.path.join('a', '1'))
        # =====================================================================

    def test_datetime(self):
        """Test datetime"""
        # =====================================================================
        self.assertEqual(
            Config.to_datetime('%Y%m%d%H', '2008110201'),
            dt(2008, 11, 2, 1)
        )
        self.assertIsNone(Config.to_datetime('%Y%m%d%H', None))
        # =====================================================================
        self.assertEqual(
            Config.from_datetime(dt(2008, 11, 2, 1)),
            '2008110201'
        )
        self.assertEqual(
            Config.from_datetime(dt(2008, 11, 2, 1), fmt='%Y%m%d'),
            '20081102'
        )
        # =====================================================================
