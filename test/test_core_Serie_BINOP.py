#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Serie in pyspc.core.serie - binary operators

To run all tests just type:
    python -m unittest test_core_Serie_BINOP

To run only a class test:
    python -m unittest test_core_Serie_BINOP.TestSerie

To run only a specific test:
    python -m unittest test_core_Serie_BINOP.TestSerie.test_add

"""
# Imports
from datetime import datetime as dt
import numpy as np
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest
import warnings

# Imports pyspc
from pyspc.core.serie import Serie


class TestSerie(unittest.TestCase):
    """
    Serie class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        self.code = 'K0000000'
        self.provider = 'SPC'
        self.varname = 'QH'
        self.list_dtval = [
            [dt(2014, 11, 4, 0), 35.600],
            [dt(2014, 11, 4, 1), 41.200],
            [dt(2014, 11, 4, 2), 46.900],
            [dt(2014, 11, 4, 3), 62.900],
            [dt(2014, 11, 4, 4), 85.700],
            [dt(2014, 11, 4, 5), 106.000],
            [dt(2014, 11, 4, 6), 118.000],
            [dt(2014, 11, 4, 7), 115.000],
            [dt(2014, 11, 4, 8), 103.000],
            [dt(2014, 11, 4, 9), 92.500],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 11), 82.300],
            [dt(2014, 11, 4, 12), 78.300]
        ]

    def test_add(self):
        """
        Test addition
        """
        # =====================================================================
        code_add = '({0})a(1)'.format(self.code)
        code_addserie = '({0})a({0})'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([1 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [('1', self.varname)]
        serie_1 = Serie(
            datval=valid_1,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        valid_add = pnd.DataFrame([x[1]+1 for x in self.list_dtval],
                                  index=[x[0] for x in self.list_dtval],
                                  dtype=np.float32)
        valid_add.columns = [(code_add, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie + 1
        assert_frame_equal(serie.data_frame, valid_add)
        self.assertEqual(serie.code, code_add)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = 1 + serie
        assert_frame_equal(serie.data_frame, valid_add)
        self.assertEqual(serie.code, code_add)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie + serie_1
        valid_add.columns = [(code_addserie, self.varname)]
        assert_frame_equal(serie.data_frame, valid_add)
        self.assertEqual(serie.code, '({0})a({0})'.format(self.code))
        # =====================================================================

    def test_sub(self):
        """
        Test soustraction
        """
        # =====================================================================
        code_sub = '({0})s(1)'.format(self.code)
        code_sub2 = '(1)s({0})'.format(self.code)
        code_subserie = '({0})s({0})'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([1 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [('-1', self.varname)]
        serie_1 = Serie(
            datval=valid_1,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        valid_sub = pnd.DataFrame([x[1]-1 for x in self.list_dtval],
                                  index=[x[0] for x in self.list_dtval],
                                  dtype=np.float32)
        valid_sub.columns = [(code_sub, self.varname)]
        valid_sub2 = pnd.DataFrame([1-x[1] for x in self.list_dtval],
                                   index=[x[0] for x in self.list_dtval],
                                   dtype=np.float32)
        valid_sub2.columns = [(code_sub2, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie - 1
        assert_frame_equal(serie.data_frame, valid_sub)
        self.assertEqual(serie.code, code_sub)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = 1 - serie
        assert_frame_equal(serie.data_frame, valid_sub2)
        self.assertEqual(serie.code, code_sub2)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie - serie_1
        valid_sub.columns = [(code_subserie, self.varname)]
        assert_frame_equal(serie.data_frame, valid_sub)
        self.assertEqual(serie.code, code_subserie)
        # =====================================================================

    def test_mul(self):
        """
        Test multiplication
        """
        # =====================================================================
        code_mul = '({0})m(2)'.format(self.code)
        code_mulserie = '({0})m({0})'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([2 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [('2', self.varname)]
        serie_1 = Serie(
            datval=valid_1,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        valid_mul = pnd.DataFrame([x[1]*2 for x in self.list_dtval],
                                  index=[x[0] for x in self.list_dtval],
                                  dtype=np.float32)
        valid_mul.columns = [(code_mul, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie * 2
        assert_frame_equal(serie.data_frame, valid_mul)
        self.assertEqual(serie.code, code_mul)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = 2 * serie
        assert_frame_equal(serie.data_frame, valid_mul)
        self.assertEqual(serie.code, code_mul)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie * serie_1
        valid_mul.columns = [(code_mulserie, self.varname)]
        assert_frame_equal(serie.data_frame, valid_mul)
        self.assertEqual(serie.code, code_mulserie)
        # =====================================================================

    def test_truediv(self):
        """
        Test division
        """
        # =====================================================================
        code_div = '({0})d(2)'.format(self.code)
        code_div2 = '(2)d({0})'.format(self.code)
        code_divserie = '({0})d({0})'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([2 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [('1', self.varname)]
        serie_1 = Serie(
            datval=valid_1,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        valid_div = pnd.DataFrame([x[1]/2 for x in self.list_dtval],
                                  index=[x[0] for x in self.list_dtval],
                                  dtype=np.float32)
        valid_div.columns = [(code_div, self.varname)]
        valid_div2 = pnd.DataFrame([2/x[1] for x in self.list_dtval],
                                   index=[x[0] for x in self.list_dtval],
                                   dtype=np.float32)
        valid_div2.columns = [(code_div2, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie / 2
        assert_frame_equal(serie.data_frame, valid_div)
        self.assertEqual(serie.code, code_div)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = 2 / serie
        assert_frame_equal(serie.data_frame, valid_div2)
        self.assertEqual(serie.code, code_div2)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie / serie_1
        valid_div.columns = [(code_divserie, self.varname)]
        assert_frame_equal(serie.data_frame, valid_div)
        self.assertEqual(serie.code, code_divserie)
        # =====================================================================

    def test_pow(self):
        """
        Test puissance
        """
        # =====================================================================
        code_pow = '({0})p(2)'.format(self.code)
        code_powserie = '({0})p({0})'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([2 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [('1', self.varname)]
        serie_1 = Serie(
            datval=valid_1,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        valid_pow = pnd.DataFrame([x[1]**2 for x in self.list_dtval],
                                  index=[x[0] for x in self.list_dtval],
                                  dtype=np.float32)
        valid_pow.columns = [(code_pow, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie ** 2
        assert_frame_equal(serie.data_frame, valid_pow)
        self.assertEqual(serie.code, code_pow)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie ** serie_1
        valid_pow.columns = [(code_powserie, self.varname)]
        assert_frame_equal(serie.data_frame, valid_pow)
        self.assertEqual(serie.code, code_powserie)
        # =====================================================================

    def test_abs(self):
        """
        Test abs
        """
        # =====================================================================
        valabs = [
            [dt(2014, 11, 4, 0), -35.600],
            [dt(2014, 11, 4, 1), -41.200],
            [dt(2014, 11, 4, 2), -46.900],
            [dt(2014, 11, 4, 3), -62.900],
            [dt(2014, 11, 4, 4), -85.700],
            [dt(2014, 11, 4, 5), 106.000],
            [dt(2014, 11, 4, 6), 118.000],
            [dt(2014, 11, 4, 7), 115.000],
            [dt(2014, 11, 4, 8), 103.000],
            [dt(2014, 11, 4, 9), -92.500],
            [dt(2014, 11, 4, 10), -86.400],
            [dt(2014, 11, 4, 11), -82.300],
            [dt(2014, 11, 4, 12), -78.300]
        ]
        # =====================================================================
        code_abs = 'abs({0})'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in valabs],
                              index=[x[0] for x in valabs],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([abs(x[1]) for x in valabs],
                                index=[x[0] for x in valabs],
                                dtype=np.float32)
        valid_1.columns = [(code_abs, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = abs(serie)
        assert_frame_equal(serie.data_frame, valid_1)
        self.assertEqual(serie.code, code_abs)
        # =====================================================================

    def test_mod(self):
        """
        Test modulo
        """
        # =====================================================================
        valmod = [
            [dt(2014, 11, 4, 0), 10],
            [dt(2014, 11, 4, 1), 12],
            [dt(2014, 11, 4, 2), 14],
            [dt(2014, 11, 4, 3), 17],
            [dt(2014, 11, 4, 4), 21],
            [dt(2014, 11, 4, 5), 24],
            [dt(2014, 11, 4, 6), 28],
            [dt(2014, 11, 4, 7), 31],
            [dt(2014, 11, 4, 8), 34],
            [dt(2014, 11, 4, 9), 37],
            [dt(2014, 11, 4, 10), 39],
            [dt(2014, 11, 4, 11), 41],
            [dt(2014, 11, 4, 12), 42]
        ]
        # =====================================================================
        code_mod = '({0})mod(2)'.format(self.code)
        code_modserie = '({0})mod({0})'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in valmod],
                              index=[x[0] for x in valmod],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([x[1] % 2 for x in valmod],
                                index=[x[0] for x in valmod],
                                dtype=np.float32)
        valid_1.columns = [(code_mod, self.varname)]
        valid_2 = pnd.DataFrame([2 for x in valmod],
                                index=[x[0] for x in valmod],
                                dtype=np.float32)
        valid_2.columns = [(code_mod, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie % 2
        assert_frame_equal(serie.data_frame, valid_1)
        self.assertEqual(serie.code, code_mod)
        # =====================================================================
        serie_2 = Serie(
            datval=valid_2,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie % serie_2
        valid_1.columns = [(code_modserie, self.varname)]
        assert_frame_equal(serie.data_frame, valid_1)
        self.assertEqual(serie.code, code_modserie)
        # =====================================================================

    def test_lt(self):
        """
        Test <
        """
        # =====================================================================
        code_op = '({0})lt(100)'.format(self.code)
        code_opserie = '({0})lt({0})'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([100 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [('100', self.varname)]
        serie_1 = Serie(
            datval=valid_1,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        valid_comp = pnd.DataFrame([x[1] < 100 for x in self.list_dtval],
                                   index=[x[0] for x in self.list_dtval],
                                   dtype=np.bool)
        valid_comp.columns = [(code_op, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie < 100
        assert_frame_equal(serie.data_frame, valid_comp)
        self.assertEqual(serie.code, code_op)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie < serie_1
        valid_comp.columns = [(code_opserie, self.varname)]
        assert_frame_equal(serie.data_frame, valid_comp)
        self.assertEqual(serie.code, code_opserie)
        # =====================================================================

    def test_le(self):
        """
        Test <=
        """
        # =====================================================================
        code_op = '({0})le(100)'.format(self.code)
        code_opserie = '({0})le({0})'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([100 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [('100', self.varname)]
        serie_1 = Serie(
            datval=valid_1,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        valid_comp = pnd.DataFrame([x[1] <= 100 for x in self.list_dtval],
                                   index=[x[0] for x in self.list_dtval],
                                   dtype=np.bool)
        valid_comp.columns = [(code_op, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie <= 100
        assert_frame_equal(serie.data_frame, valid_comp)
        self.assertEqual(serie.code, code_op)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie <= serie_1
        valid_comp.columns = [(code_opserie, self.varname)]
        assert_frame_equal(serie.data_frame, valid_comp)
        self.assertEqual(serie.code, code_opserie)
        # =====================================================================

    def test_eq(self):
        """
        Test ==
        """
        # =====================================================================
        code_op = '({0})eq(118.0)'.format(self.code)
        code_opserie = '({0})eq({0})'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([118.0 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [('1', self.varname)]
        serie_1 = Serie(
            datval=valid_1,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        valid_comp = pnd.DataFrame([x[1] == 118.0 for x in self.list_dtval],
                                   index=[x[0] for x in self.list_dtval],
                                   dtype=np.bool)
        valid_comp.columns = [(code_op, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie == 118.0
        assert_frame_equal(serie.data_frame, valid_comp)
        self.assertEqual(serie.code, code_op)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie == serie_1
        valid_comp.columns = [(code_opserie, self.varname)]
        assert_frame_equal(serie.data_frame, valid_comp)
        self.assertEqual(serie.code, code_opserie)
        # =====================================================================

    def test_ne(self):
        """
        Test !=
        """
        # =====================================================================
        code_op = '({0})ne(118.0)'.format(self.code)
        code_opserie = '({0})ne({0})'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([118.0 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [('1', self.varname)]
        serie_1 = Serie(
            datval=valid_1,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        valid_comp = pnd.DataFrame([x[1] != 118.0 for x in self.list_dtval],
                                   index=[x[0] for x in self.list_dtval],
                                   dtype=np.bool)
        valid_comp.columns = [(code_op, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie != 118.0
        assert_frame_equal(serie.data_frame, valid_comp)
        self.assertEqual(serie.code, code_op)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie != serie_1
        valid_comp.columns = [(code_opserie, self.varname)]
        assert_frame_equal(serie.data_frame, valid_comp)
        self.assertEqual(serie.code, code_opserie)
        # =====================================================================

    def test_ge(self):
        """
        Test >=
        """
        # =====================================================================
        code_op = '({0})ge(100)'.format(self.code)
        code_opserie = '({0})ge({0})'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([100 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [('1', self.varname)]
        serie_1 = Serie(
            datval=valid_1,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        valid_comp = pnd.DataFrame([x[1] >= 100 for x in self.list_dtval],
                                   index=[x[0] for x in self.list_dtval],
                                   dtype=np.bool)
        valid_comp.columns = [(code_op, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie >= 100
        assert_frame_equal(serie.data_frame, valid_comp)
        self.assertEqual(serie.code, code_op)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie >= serie_1
        valid_comp.columns = [(code_opserie, self.varname)]
        assert_frame_equal(serie.data_frame, valid_comp)
        self.assertEqual(serie.code, code_opserie)
        # =====================================================================

    def test_gt(self):
        """
        Test >
        """
        # =====================================================================
        code_op = '({0})gt(100)'.format(self.code)
        code_opserie = '({0})gt({0})'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([100 for x in self.list_dtval],
                                index=[x[0] for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [('1', self.varname)]
        serie_1 = Serie(
            datval=valid_1,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        valid_comp = pnd.DataFrame([x[1] > 100 for x in self.list_dtval],
                                   index=[x[0] for x in self.list_dtval],
                                   dtype=np.bool)
        valid_comp.columns = [(code_op, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie > 100
        assert_frame_equal(serie.data_frame, valid_comp)
        self.assertEqual(serie.code, code_op)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie > serie_1
        valid_comp.columns = [(code_opserie, self.varname)]
        assert_frame_equal(serie.data_frame, valid_comp)
        self.assertEqual(serie.code, code_opserie)
        # =====================================================================
