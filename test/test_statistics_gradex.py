#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for functions in pyspc.statistics.gradex

To run all tests just type:
    python -m unittest test_statistics_gradex

To run only a class test:
    python -m unittest test_statistics_gradex.TestGradex

To run only a specific test:
    python -m unittest test_statistics_gradex.TestGradex.test_rr2q

"""
# Imports
from datetime import timedelta as td
import os
import unittest

from pyspc.statistics.gradex import apply, compute, rr2q, montana


class TestGradex(unittest.TestCase):
    """
    statistics class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'metadata', 'statistics')

    def test_apply(self):
        """
        Test de l'application du gradex
        """
        pivot_value = 74
        pivot_period = 10
        target_period = 100
        gradex = 23.2
        target_value = apply(pivot_value, pivot_period, target_period, gradex)
        self.assertAlmostEqual(target_value, 128.515, places=2)

    def test_compute(self):
        """
        Test du calcul du gradex
        """
        val1 = 127
        t1 = 10
        val2 = 204
        t2 = 100
        self.assertAlmostEqual(compute(val1, t1, val2, t2), 32.77, places=2)

    def test_rr2q(self):
        """
        Test de la conversion d'un gradex P -> Q
        """
        g = 12.1
        area = 17570
        duration = td(days=4)
        rij = 1.5
        gq = rr2q(g, area, duration, rij)
        self.assertAlmostEqual(gq, 922.73, places=2)

    def test_montana(self):
        """
        Test de l'application de Montana
        https://publitheque.meteo.fr/Exemples/MONTANA-exemple.pdf
        """
        a = 8.929
        b = 0.766
        duration = td(hours=1)
        self.assertAlmostEqual(montana(duration, a, b), 23.275, places=2)
