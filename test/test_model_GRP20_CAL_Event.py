#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP_Event in pyspc.model.grp20

To run all tests just type:
    python -m unittest test_model_GRP20_CAL_Event

To run only a class test:
    python -m unittest test_model_GRP20_CAL_Event.TestGRP_Event

To run only a specific test:
    python -m unittest test_model_GRP20_CAL_Event.TestGRP_Event.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import filecmp
import numpy as np
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.model.grp20 import GRP_Event


class TestGRP_Event(unittest.TestCase):
    """
    GRP_Event class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp20', 'cal')
        self.valid_hor = pnd.DataFrame(
            {'Date': pnd.date_range(
                dt(2007, 1, 18, 12), dt(2007, 1, 19, 18), freq='H'),
             'Pluie(mm/h)': [
                 0.16, 0.12, 0.20, 3.04, 3.36, 4.16, 8.48, 3.72, 7.60, 6.32,
                 9.44, 6.64, 7.88, 8.44, 5.32, 6.64, 5.96, 6.72, 7.24, 7.44,
                 6.60, 6.48, 5.36, 2.96, 1.32, 1.08, 0.48, 1.60, 1.00, 0.72,
                 0.80],
             'Debit(m3/s)': [
                 2.8230, 2.7738, 2.7246, 2.7100, 2.7300, 2.8962, 3.5375,
                 5.1501, 7.8640, 10.1920, 12.4640, 14.6950, 16.6250, 18.2500,
                 19.7100, 20.4315, 21.9115, 23.4250, 23.5500, 24.9125, 29.3062,
                 33.8499, 36.1000, 36.3666, 34.7500, 30.4996, 26.9284, 24.9933,
                 23.6850, 22.6416, 21.4666]},
        )
        self.valid_jour = pnd.DataFrame(
            {'Date': pnd.date_range(
                dt(2007, 1, 14), dt(2007, 1, 26), freq='D'),
             'Pluie(mm/h)': [
                 1.60, 6.88, 0.00, 0.40, 7.84, 70.08, 82.96, 10.48, 9.00, 2.04,
                 1.12, 2.96, 0.04],
             'Debit(m3/s)': [
                 np.nan, 3.5675, 2.9224, np.nan, 2.1828, 4.8858, 24.4488,
                 11.9242, 8.0948, 5.1401, 3.8193, 2.9686, 2.2722]},
        )

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'RH10585x-EV0001.DAT')
        reader = GRP_Event(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.station, 'RH10585x')
        self.assertEqual(reader.event, 'EV0001')
        # =====================================================================
        filename = os.path.join(self.dirname, 'RH10585x-EV0001jour.DAT')
        reader = GRP_Event(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.station, 'RH10585x')
        self.assertEqual(reader.event, 'EV0001jour')
        # =====================================================================

    def test_read_hor(self):
        """
        Test de la lecture d'un fichier GRP Event - Cas HORAIRE
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'RH10585x-EV0001.DAT')
        reader = GRP_Event(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_hor, df)
        # =====================================================================

    def test_read_jour(self):
        """
        Test de la lecture d'un fichier GRP Event - Cas JOURNALIER
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'RH10585x-EV0001jour.DAT')
        reader = GRP_Event(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_jour, df)
        # =====================================================================

    def test_write_hor(self):
        """
        Test de l'écriture d'un fichier GRP_Event - Cas HORAIRE
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'RH10585x-EV0001.DAT')
        tmpfile = os.path.join('data', 'RH10585x-EV0001.DAT')
        writer = GRP_Event(filename=tmpfile)
        writer.write(data=self.valid_hor, tdelta=td(hours=1))
        self.assertTrue(filecmp.cmp(filename, tmpfile))
        os.remove(tmpfile)
        # =====================================================================

    def test_write_jour(self):
        """
        Test de l'écriture d'un fichier GRP_Event - Cas JOURNALIER
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'RH10585x-EV0001jour.DAT')
        tmpfile = os.path.join('data', 'RH10585x-EV0001jour.DAT')
        writer = GRP_Event(filename=tmpfile)
        writer.write(data=self.valid_jour, tdelta=td(days=1))
        self.assertTrue(filecmp.cmp(filename, tmpfile))
        os.remove(tmpfile)
        # =====================================================================
