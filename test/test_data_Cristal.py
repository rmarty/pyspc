#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Cristal in pyspc.data.cristal

To run all tests just type:
    python -m unittest test_data_Cristal

To run only a class test:
    python -m unittest test_data_Cristal.TestCristal

To run only a specific test:
    python -m unittest test_data_Cristal.TestCristal.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import numpy as np
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.data.cristal import Cristal


class TestCristal(unittest.TestCase):
    """
    Cristal class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'cristal', '2008')
        self.src_oct = os.path.join(self.dirname, 'ARCHIVE_2008_10.csv')
        self.data_oct = Cristal(filename=self.src_oct)
        self.src_nov = os.path.join(self.dirname, 'ARCHIVE_2008_11.csv')
        self.data_nov = Cristal(filename=self.src_nov)

    def test_init(self):
        """
        Test de la création de l'instance
        """
        self.assertEqual(self.data_nov.filename, self.src_nov)

    def test_read_KMHEAU(self):
        """
        Test de lecture KMHEAU -> HI, QI
        """
        # =====================================================================
        stations = 'K0550010'
        longnames = 'KMHEAU'
        # =====================================================================
        valid = {
            (stations, longnames, 'VALUE_MES'): pnd.DataFrame(
                {'VALUE': [-0.71, -0.71, -0.71, -0.71, -0.71, -0.71, -0.7,
                           -0.7, -0.7, -0.7, -0.69, -0.7, -0.69, -0.69, -0.7,
                           -0.7, -0.7, -0.7, -0.71, -0.71, -0.71, -0.72, -0.72,
                           -0.72, -0.72, -0.73, -0.73, -0.73, -0.74, -0.74,
                           -0.74, -0.74, -0.74, -0.75, -0.74, -0.74, -0.74,
                           -0.74, -0.74, -0.73, -0.72, -0.71, -0.71, -0.7,
                           -0.7, -0.7, -0.7, -0.71, -0.7, -0.7, -0.7, -0.7,
                           -0.7, -0.7, -0.7, -0.7, -0.7, -0.7, -0.7, -0.7,
                           -0.7, -0.7, -0.69, -0.69, -0.69, -0.69, -0.69,
                           -0.69, -0.69, -0.69, -0.68, -0.68, -0.67, -0.67,
                           -0.67, -0.67, -0.67, -0.66, -0.66, -0.65, -0.65,
                           -0.65, -0.65, -0.65, -0.64, -0.64, -0.64, -0.64,
                           -0.64, -0.64, -0.64, -0.63, -0.63, -0.63, -0.63,
                           -0.63, -0.63, -0.62, -0.62, -0.61, -0.61, -0.6,
                           -0.6, -0.59, -0.59, -0.59, -0.59, -0.58, -0.58,
                           -0.57, -0.56, -0.55, -0.55, -0.54, -0.52, -0.51,
                           -0.5, -0.49, -0.47, -0.46, -0.44, -0.42, -0.39,
                           -0.36, -0.33, -0.29, -0.26, -0.22, -0.18, -0.15,
                           -0.11, -0.08, -0.05, -0.01, 0.02, 0.05, 0.07, 0.1,
                           0.13]},
                index=pnd.date_range(
                    dt(2008, 11, 1), dt(2008, 11, 1, 23), freq=td(minutes=10)
                )
            ),
            (stations, longnames, 'VALUE_CONV'): pnd.DataFrame(
                {'VALUE': [57.15, 57.15, 57.15, 57.15, 57.15, 57.15, 58.53,
                           58.53, 58.53, 58.53, 59.91, 58.53, 59.91, 59.91,
                           58.53, 58.53, 58.53, 58.53, 57.15, 57.15, 57.15,
                           55.78, 55.78, 55.78, 55.78, 54.4, 54.4, 54.4, 53.15,
                           53.15, 53.15, 53.15, 53.15, 51.9, 53.15, 53.15,
                           53.15, 53.15, 53.15, 54.4, 55.78, 57.15, 57.15,
                           58.53, 58.53, 58.53, 58.53, 57.15, 58.53, 58.53,
                           58.53, 58.53, 58.53, 58.53, 58.53, 58.53, 58.53,
                           58.53, 58.53, 58.53, 58.53, 58.53, 59.91, 59.91,
                           59.91, 59.91, 59.91, 59.91, 59.91, 59.91, 61.29,
                           61.29, 62.66, 62.66, 62.66, 62.66, 62.66, 64.04,
                           64.04, 65.42, 65.42, 65.42, 65.42, 65.42, 66.8,
                           66.8, 66.8, 66.8, 66.8, 66.8, 66.8, 68.17, 68.17,
                           68.17, 68.17, 68.17, 68.17, 69.55, 69.55, 70.93,
                           70.93, 72.3, 72.3, 73.68, 73.68, 73.68, 73.68,
                           75.06, 75.06, 76.44, 77.81, 79.19, 79.19, 80.57,
                           83.32, 84.7, 86.21, 87.73, 90.76, 92.27, 95.3,
                           98.53, 103.38, 108.0, 114.0, 122.0, 128.0, 139.0,
                           145.0, 154.14, 162.71, 169.14, 175.57, 184.7,
                           192.8, 200.9, 206.3, 214.4, 222.5]},
                index=pnd.date_range(
                    dt(2008, 11, 1), dt(2008, 11, 1, 23), freq=td(minutes=10)
                )
            )
        }
        for v in valid.values():
            v.index.name = 'DATE'
        # =====================================================================
        content = self.data_nov.read(
            stations=[stations],
            longnames=[longnames]
        )
        for k in valid:
            self.assertIn(k, content)
            assert_frame_equal(content[k], valid[k])
        # =====================================================================

    def test_read_KMLBAR(self):
        """
        Test de lecture KMLBAR -> ZI, QI
        """
        # =====================================================================
        stations = 'K0600010'
        longnames = 'KMLBAR'
        # =====================================================================
        valid = {
            (stations, longnames, 'VALUE_MES'): pnd.DataFrame(
                {'VALUE': [419.05, 419.05, 419.05, 419.05, 419.05, 419.04,
                           419.04, 419.04, 419.04, 419.04, 419.04, 419.04,
                           419.04, 419.03, 419.03, 419.03, 419.03, 419.03,
                           419.03, 419.03, 419.02, 419.02, 419.02, 419.02,
                           419.02, 419.02, 419.02, 419.01, 419.01, 419.01,
                           419.01, 419.01, 419.01, 419.01, 419.01, 419.01,
                           419.01, 419.0, 419.0, 419.0, 419.0, 419.0, 419.0,
                           419.0, 419.0, 418.99, 418.99, 418.99, 418.99,
                           418.98, 418.98, 418.99, 418.98, 418.98, 418.97,
                           418.97, 418.97, 418.97, 418.98, 418.98, 418.97,
                           418.97, 418.97, 418.97, 418.97, 418.97, 418.96,
                           418.96, 418.96, 418.96, 418.96, 418.96, 418.96,
                           418.96, 418.95, 418.95, 418.95, 418.95, 418.95,
                           418.94, 418.94, 418.94, 418.94, 418.93, 418.93,
                           418.93, 418.93, 418.93, 418.93, 418.92, 418.92,
                           418.92, 418.92, 418.92, 418.92, 418.92, 418.92,
                           418.92, 418.93, 418.93, 418.93, 418.94, 418.94,
                           418.95, 418.95, 418.96, 418.97, 418.98, 418.99,
                           418.99, 419.0, 419.01, 419.02, 419.03, 419.04,
                           419.05, 419.07, 419.08, 419.09, 419.1, 419.12,
                           419.13, 419.15, 419.16, 419.18, 419.2, 419.22,
                           419.23, 419.25, 419.27, 419.3, 419.32, 419.34,
                           419.37, 419.39, 419.42, 419.45, 419.48, 419.51,
                           419.55, 419.58, 419.61, 419.63, 419.65]},
                index=pnd.date_range(
                    dt(2008, 11, 1), dt(2008, 11, 1, 23, 50),
                    freq=td(minutes=10)
                )
            ),
            (stations, longnames, 'VALUE_CONV'): pnd.DataFrame(
                {'VALUE': [np.nan]*144},
                index=pnd.date_range(
                    dt(2008, 11, 1), dt(2008, 11, 1, 23, 50),
                    freq=td(minutes=10)
                )
            )
        }
        for v in valid.values():
            v.index.name = 'DATE'
        # =====================================================================
        content = self.data_nov.read(
            stations=[stations],
            longnames=[longnames]
        )
        for k in valid:
            self.assertIn(k, content)
            assert_frame_equal(content[k], valid[k])
        # =====================================================================

    def test_read_KMPLUI(self):
        """
        Test de lecture KMPLUI -> compteur pluie
        """
        # =====================================================================
        stations = 'K0559910'
        longnames = 'KMPLUI'
        # =====================================================================
        valid = {
            (stations, longnames, 'VALUE_MES'): pnd.DataFrame(
                {'VALUE': [3292.8, 3292.8, 3292.8, 3292.8, 3292.8, 3292.8,
                           3292.8, 3292.8, 3292.8, 3292.8, 3292.8, 3292.8,
                           3292.8, 3292.8, 3292.8, 3292.8, 3292.8, 3292.8,
                           3292.8, 3292.8, 3292.8, 3292.8, 3292.8, 3292.8,
                           3292.8, 3292.8, 3292.8, 3292.8, 3292.8, 3292.8,
                           3292.8, 3292.8, 3292.8, 3292.8, 3292.8, 3292.8,
                           3292.8, 3292.8, 3292.8, 3292.8, 3292.8, 3292.8,
                           3292.8, 3292.8, 3292.8, 3292.8, 3292.8, 3292.8,
                           3292.8, 3292.8, 3292.8, 3292.8, 3292.8, 3292.8,
                           3292.8, 3292.8, 3292.8, 3292.8, 3292.8, 3292.8,
                           3292.8, 3292.8, 3292.8, 3292.8, 3292.8, 3292.8,
                           3292.8, 3292.8, 3292.8, 3292.8, 3292.8, 3292.8,
                           3292.8, 3292.8, 3292.8, 3292.8, 3292.8, 3292.8,
                           3292.8, 3292.8, 3292.8, 3292.8, 3292.8, 3292.8,
                           3292.8, 3292.8, 3292.8, 3293.0, 3293.0, 3293.2,
                           3293.2, 3293.4, 3294.8, 3296.2, 3298.0, 3299.2,
                           3301.0, 3302.8, 3303.6, 3305.6, 3307.8, 3309.2,
                           3310.0, 3311.8, 3313.4, 3314.6, 3315.8, 3316.8,
                           3317.6, 3318.8, 3319.4, 3321.4, 3323.0, 3324.2,
                           3325.4, 3325.8, 3326.8, 3327.6, 3328.8, 3329.4,
                           3330.2, 3330.4, 3331.4, 3332.4, 3333.4, 3334.0,
                           3334.4, 3334.8, 3335.8, 3337.2, 3338.2, 3339.2,
                           3340.0, 3340.8, 3341.0, 3341.0, 3341.8, 3343.0,
                           3344.0, 3345.6, 3346.8, 3347.8, 3349.0, 3350.0]},
                index=pnd.date_range(
                    dt(2008, 11, 1), dt(2008, 11, 1, 23, 50),
                    freq=td(minutes=10)
                )
            ),
            (stations, longnames, 'VALUE_CONV'): pnd.DataFrame(
                {'VALUE': [np.nan]*144},
                index=pnd.date_range(
                    dt(2008, 11, 1), dt(2008, 11, 1, 23, 50),
                    freq=td(minutes=10)
                )
            )
        }
        for v in valid.values():
            v.index.name = 'DATE'
        # =====================================================================
        content = self.data_nov.read(
            stations=[stations],
            longnames=[longnames]
        )
#        for k, v in content.items():
#            v.to_csv(
#                os.path.join('data', '{}_{}_{}.csv'.format(*k))
#            )
        for k in valid:
            self.assertIn(k, content)
            assert_frame_equal(content[k], valid[k])
        # =====================================================================

    def test_write(self):
        """
        Test de l'écriture d'un fichier Cristal
        Fonctionnalité pas encore implémenté dans <pySPC>
        """
        with self.assertRaises(NotImplementedError):
            self.data_nov.write()
