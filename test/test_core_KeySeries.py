#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Series in pyspc.core.series

To run all tests just type:
    python -m unittest test_core_KeySeries

To run only a class test:
    python -m unittest test_core_KeySeries.TestKeySeries

To run only a specific test:
    python -m unittest test_core_KeySeries.TestKeySeries.str2tuple

"""
# Imports
from datetime import datetime as dt
import unittest

# Imports pyspc
from pyspc.core.keyseries import tuple2str, str2tuple


class TestKeySeries(unittest.TestCase):
    """
    Series class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """

    def test_str2tuple(self):
        """
        Test str2tuple
        """
        # =====================================================================
        valid = {
            'ID_GRD': ('ID', 'GRD', None),
            'ID_MDL_GRD': ('ID', 'GRD', 'MDL'),
            'ID_20210301_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1), None, None, None)),
            'ID_20210301_MDL_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1), 'MDL', None, None)),
            'ID_20210301_MDL_SCN_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1), 'MDL', 'SCN', None)),
            'ID_20210301_MDL_SCN_INC_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1), 'MDL', 'SCN', 'INC')),
            'ID_2021030112_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12), None, None, None)),
            'ID_2021030112_MDL_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12), 'MDL', None, None)),
            'ID_2021030112_MDL_SCN_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12), 'MDL', 'SCN', None)),
            'ID_2021030112_MDL_SCN_INC_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12), 'MDL', 'SCN', 'INC')),
            'ID_202103011230_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12, 30), None, None, None)),
            'ID_202103011230_MDL_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12, 30), 'MDL', None, None)),
            'ID_202103011230_MDL_SCN_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12, 30), 'MDL', 'SCN', None)),
            'ID_202103011230_MDL_SCN_INC_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12, 30), 'MDL', 'SCN', 'INC')),
        }
        # =====================================================================
        for k, v in valid.items():
            self.assertEqual(str2tuple(k), v)
#            print("    >>> str2tuple('{}', forcesim=True)".format(k))
#            print("    {}".format(v))
#            print()
        # =====================================================================

    def test_str2tuple_forceobs(self):
        """
        Test str2tuple
        """
        # =====================================================================
        valid = {
            'ID_GRD': ('ID', 'GRD', None),
            'ID_MDL_GRD': ('ID_MDL', 'GRD', None),
            'ID_20210301_GRD':
                ('ID_20210301', 'GRD', None),
            'ID_20210301_MDL_GRD':
                ('ID_20210301_MDL', 'GRD', None),
            'ID_20210301_MDL_SCN_GRD':
                ('ID_20210301_MDL_SCN', 'GRD', None),
            'ID_20210301_MDL_SCN_INC_GRD':
                ('ID_20210301_MDL_SCN_INC', 'GRD', None),
            'ID_2021030112_GRD':
                ('ID_2021030112', 'GRD', None),
            'ID_2021030112_MDL_GRD':
                ('ID_2021030112_MDL', 'GRD', None),
            'ID_2021030112_MDL_SCN_GRD':
                ('ID_2021030112_MDL_SCN', 'GRD', None),
            'ID_2021030112_MDL_SCN_INC_GRD':
                ('ID_2021030112_MDL_SCN_INC', 'GRD', None),
            'ID_202103011230_GRD':
                ('ID_202103011230', 'GRD', None),
            'ID_202103011230_MDL_GRD':
                ('ID_202103011230_MDL', 'GRD', None),
            'ID_202103011230_MDL_SCN_GRD':
                ('ID_202103011230_MDL_SCN', 'GRD', None),
            'ID_202103011230_MDL_SCN_INC_GRD':
                ('ID_202103011230_MDL_SCN_INC', 'GRD', None),
        }
        # =====================================================================
        for k, v in valid.items():
            self.assertEqual(str2tuple(k, forceobs=True), v)
        # =====================================================================

    def test_str2tuple_forcesim(self):
        """
        Test str2tuple
        """
        # =====================================================================
        valid = {
            'ID_GRD': ('ID', 'GRD', None),
            'ID_MDL_GRD': ('ID', 'GRD', 'MDL'),
            'ID_20210301_GRD':
                ('ID', 'GRD', '20210301'),
            'ID_20210301_MDL_GRD':
                ('ID', 'GRD', '20210301_MDL'),
            'ID_20210301_MDL_SCN_GRD':
                ('ID', 'GRD', '20210301_MDL_SCN'),
            'ID_20210301_MDL_SCN_INC_GRD':
                ('ID', 'GRD', '20210301_MDL_SCN_INC'),
            'ID_2021030112_GRD':
                ('ID', 'GRD', '2021030112'),
            'ID_2021030112_MDL_GRD':
                ('ID', 'GRD', '2021030112_MDL'),
            'ID_2021030112_MDL_SCN_GRD':
                ('ID', 'GRD', '2021030112_MDL_SCN'),
            'ID_2021030112_MDL_SCN_INC_GRD':
                ('ID', 'GRD', '2021030112_MDL_SCN_INC'),
            'ID_202103011230_GRD':
                ('ID', 'GRD', '202103011230'),
            'ID_202103011230_MDL_GRD':
                ('ID', 'GRD', '202103011230_MDL'),
            'ID_202103011230_MDL_SCN_GRD':
                ('ID', 'GRD', '202103011230_MDL_SCN'),
            'ID_202103011230_MDL_SCN_INC_GRD':
                ('ID', 'GRD', '202103011230_MDL_SCN_INC'),
        }
        # =====================================================================
        for k, v in valid.items():
            self.assertEqual(str2tuple(k, forcesim=True), v)
        # =====================================================================

    def test_tuple2str(self):
        """
        Test tuple2str
        """
        # =====================================================================
        valid = {
            'ID_GRD': ('ID', 'GRD', None),
            'ID_MDL_GRD': ('ID', 'GRD', 'MDL'),
            'ID_2021030100_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1), None, None, None)),
            'ID_2021030100_MDL_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1), 'MDL', None, None)),
            'ID_2021030100_MDL_SCN_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1), 'MDL', 'SCN', None)),
            'ID_2021030100_MDL_SCN_INC_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1), 'MDL', 'SCN', 'INC')),
            'ID_2021030112_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12), None, None, None)),
            'ID_2021030112_MDL_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12), 'MDL', None, None)),
            'ID_2021030112_MDL_SCN_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12), 'MDL', 'SCN', None)),
            'ID_2021030112_MDL_SCN_INC_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12), 'MDL', 'SCN', 'INC')),
            'ID_2021030113_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 13, 30), None, None, None)),
            'ID_2021030113_MDL_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 13, 30), 'MDL', None, None)),
            'ID_2021030113_MDL_SCN_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 13, 30), 'MDL', 'SCN', None)),
            'ID_2021030113_MDL_SCN_INC_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 13, 30), 'MDL', 'SCN', 'INC')),
        }
        # =====================================================================
        for k, v in valid.items():
            self.assertEqual(tuple2str(v), k)
#            print("    >>> tuple2str({})".format(v))
#            print("    {}".format(k))
#            print()
        # =====================================================================

    def test_tuple2str_dtfmt(self):
        """
        Test tuple2str
        """
        # =====================================================================
        valid = {
            'ID_GRD': ('ID', 'GRD', None),
            'ID_MDL_GRD': ('ID', 'GRD', 'MDL'),
            'ID_202103010000_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1), None, None, None)),
            'ID_202103010000_MDL_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1), 'MDL', None, None)),
            'ID_202103010000_MDL_SCN_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1), 'MDL', 'SCN', None)),
            'ID_202103010000_MDL_SCN_INC_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1), 'MDL', 'SCN', 'INC')),
            'ID_202103011200_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12), None, None, None)),
            'ID_202103011200_MDL_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12), 'MDL', None, None)),
            'ID_202103011200_MDL_SCN_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12), 'MDL', 'SCN', None)),
            'ID_202103011200_MDL_SCN_INC_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12), 'MDL', 'SCN', 'INC')),
            'ID_202103011230_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12, 30), None, None, None)),
            'ID_202103011230_MDL_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12, 30), 'MDL', None, None)),
            'ID_202103011230_MDL_SCN_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12, 30), 'MDL', 'SCN', None)),
            'ID_202103011230_MDL_SCN_INC_GRD':
                ('ID', 'GRD', (dt(2021, 3, 1, 12, 30), 'MDL', 'SCN', 'INC')),
        }
        # =====================================================================
        for k, v in valid.items():
            self.assertEqual(tuple2str(v, dtfmt='%Y%m%d%H%M'), k)
#            print("    >>> tuple2str({}, ".format(v))
#            print("                   dtfmt='%Y%m%d%H%M')")
#            print("    {}".format(k))
#            print()
        # =====================================================================
