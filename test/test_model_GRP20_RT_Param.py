#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Param in pyspc.model.grp20

To run all tests just type:
    python -m unittest test_model_GRP20_RT_Param

To run only a class test:
    python -m unittest test_model_GRP20_RT_Param.TestGRPRT_Param

To run only a specific test:
    python -m unittest test_model_GRP20_RT_Param.TestGRPRT_Param.test_init

"""
# Imports
import collections
import os
import unittest

from pyspc.model.grp20 import GRPRT_Param


class TestGRPRT_Param(unittest.TestCase):
    """
    GRPRT_Param class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp20', 'rt')
        self.valid = collections.OrderedDict()
        self.valid['P'] = collections.OrderedDict()
        self.valid['P']['INTER (mm)'] = 1.750
        self.valid['P']['ROUT (mm)'] = 1844.567
        self.valid['P']['CORR (-)'] = 1.139
        self.valid['P']['TB (h)'] = 2.713
        self.valid['M'] = 'TANGARA'
        self.valid['C'] = 0.450

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'PARAM.DAT')
        param = GRPRT_Param(filename=filename)
        self.assertEqual(param.filename, filename)

    def test_read(self):
        """
        Test de la lecture du fichier - Cas sans température
        """
        filename = os.path.join(self.dirname, 'PARAM.DAT')
        param = GRPRT_Param(filename=filename)
        param.read()
        self.assertDictEqual(param, self.valid)

    def test_write(self):
        """
        Test d'écriture du fichier - Cas sans température
        """
        tmpfile = os.path.join('data', 'PARAM.DAT')
        param = GRPRT_Param(filename=tmpfile)
        param.update(self.valid)
        with self.assertRaises(NotImplementedError):
            param.write()
