#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for CsvFile in pyspc.core.csvfile

To run all tests just type:
    python -m unittest test_core_CsvFile

To run only a class test:
    python -m unittest test_core_CsvFile.TestCsvFile

To run only a specific test:
    python -m unittest test_core_CsvFile.TestCsvFile.test_init

"""
# Imports
from datetime import datetime as dt
import os
import unittest

from pyspc.io.pyspcfile import read_PyspcFile
from pyspc.core.series import Series


class TestPyspcFile(unittest.TestCase):
    """
    CsvFile class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'core', 'csv')

    def test_read_mono(self):
        """
        Test de la lecture d'un fichier avec une série unique
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K1321810_QH.txt')
        series = read_PyspcFile(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('K1321810', 'QH', None), series)
        # =====================================================================
        filename = os.path.join(self.dirname, 'K1321810_mohys_QH.txt')
        series = read_PyspcFile(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('K1321810', 'QH', 'mohys'), series)
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'K1251810_2018010412_2001_brut_QH.txt')
        series = read_PyspcFile(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('K1251810', 'QH',
                       (dt(2018, 1, 4, 12), '2001', 'brut', None)), series)
        # =====================================================================

    def test_read_multicol(self):
        """
        Test de la lecture d'un fichier avec plusieurs séries
        """
        # =====================================================================
        filename = os.path.join(self.dirname,
                                'LaLoireChadrac_plathynes_QH.txt')
        series = read_PyspcFile(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 4)
        self.assertIn(('LaLoireChadrac', 'PH', None), series)
        self.assertIn(('LaLoireChadrac', 'QH', None), series)
        self.assertIn(('LaLoireChadrac', 'QH', 'plathynes'), series)
        self.assertIn(('LaLoireChadrac', 'QH', 'pers'), series)
        # =====================================================================
        filename = os.path.join(self.dirname, 'LaLoireChadrac_QH.txt')
        series = read_PyspcFile(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 3)
        self.assertIn(('LaLoireChadrac', 'PH', None), series)
        self.assertIn(('LaLoireChadrac', 'QH', None), series)
        self.assertIn(('LaLoireChadrac', 'QH', 'plathynes'), series)
        # =====================================================================
        filename = os.path.join(self.dirname,
                                'K0253030_2020061200_GR6J_QJ.txt')
        series = read_PyspcFile(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 11)
        self.assertIn(('K0253030', 'QJ',
                       (dt(2020, 6, 12), 'GR6J', 'CEPcf', None)), series)
        self.assertIn(('K0253030', 'QJ',
                       (dt(2020, 6, 12), 'GR6J', 'CEPpf1', None)), series)
        self.assertIn(('K0253030', 'QJ',
                       (dt(2020, 6, 12), 'GR6J', 'CEPpf2', None)), series)
        self.assertIn(('K0253030', 'QJ',
                       (dt(2020, 6, 12), 'GR6J', 'CEPpf3', None)), series)
        self.assertIn(('K0253030', 'QJ',
                       (dt(2020, 6, 12), 'GR6J', 'CEPpf4', None)), series)
        self.assertIn(('K0253030', 'QJ',
                       (dt(2020, 6, 12), 'GR6J', 'CEPpf5', None)), series)
        self.assertIn(('K0253030', 'QJ',
                       (dt(2020, 6, 12), 'GR6J', 'CEPpf6', None)), series)
        self.assertIn(('K0253030', 'QJ',
                       (dt(2020, 6, 12), 'GR6J', 'CEPpf7', None)), series)
        self.assertIn(('K0253030', 'QJ',
                       (dt(2020, 6, 12), 'GR6J', 'CEPpf8', None)), series)
        self.assertIn(('K0253030', 'QJ',
                       (dt(2020, 6, 12), 'GR6J', 'CEPpf9', None)), series)
        self.assertIn(('K0253030', 'QJ',
                       (dt(2020, 6, 12), 'GR6J', 'CEPpf10', None)), series)
        # =====================================================================

    def test_read_multifiles_sim(self):
        """
        Test de la lecture de plusieurs fichiers avec série unique
        """
        # =====================================================================
        series = read_PyspcFile(
            dirname=self.dirname,
            stations='K1321810', varnames='QH', simulations=[None, 'mohys']
        )
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(('K1321810', 'QH', None), series)
        self.assertIn(('K1321810', 'QH', 'mohys'), series)
        # =====================================================================
        series = read_PyspcFile(
            dirname=self.dirname,
            stations='K1251810', varnames='QH',
            runtimes=dt(2018, 1, 4, 12), models=['2001', '2011'],
            scens=['brut', 'pilote'], uncerts=[None, '10', '50', '90']
        )
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 8)
        self.assertIn(('K1251810', 'QH',
                       (dt(2018, 1, 4, 12), '2001', 'brut', None)), series)
        self.assertIn(('K1251810', 'QH',
                       (dt(2018, 1, 4, 12), '2001', 'brut', '10')), series)
        self.assertIn(('K1251810', 'QH',
                       (dt(2018, 1, 4, 12), '2001', 'brut', '50')), series)
        self.assertIn(('K1251810', 'QH',
                       (dt(2018, 1, 4, 12), '2001', 'brut', '90')), series)
        self.assertIn(('K1251810', 'QH',
                       (dt(2018, 1, 4, 12), '2001', 'pilote', '10')), series)
        self.assertIn(('K1251810', 'QH',
                       (dt(2018, 1, 4, 12), '2001', 'pilote', '50')), series)
        self.assertIn(('K1251810', 'QH',
                       (dt(2018, 1, 4, 12), '2001', 'pilote', '90')), series)
        self.assertIn(('K1251810', 'QH',
                       (dt(2018, 1, 4, 12), '2011', 'brut', None)), series)
        # =====================================================================
