#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Hydroportail in pyspc.webservice.hydroportail

To run all tests just type:
    python -m unittest test_webservice_Hydroportail

To run only a class test:
    python -m unittest test_webservice_Hydroportail.TestHydroportail

To run only a specific test:
    python -m unittest test_webservice_Hydroportail.TestHydroportail.test_init

"""
# Imports
import csv
import filecmp
import os
import pandas as pnd
import unittest
import warnings

from pyspc.webservice.hydroportail import Hydroportail


class TestHydroportail(unittest.TestCase):
    """
    Hydroportail class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'webservice', 'hydroportail')
        self.dirouahs = os.path.join(
            'data', 'metadata', 'hydroportail', 'ouahs')
        warnings.filterwarnings(action="ignore", category=ResourceWarning)

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # ===================================================================
        hostname = 'hydro.eaufrance.fr'
        proxies = {'x': 'y'}
        timeout = 10
        hydro = Hydroportail(hostname=hostname, proxies=proxies,
                             timeout=timeout)
        self.assertEqual(hydro.hostname, hostname)
        self.assertDictEqual(hydro.proxies, proxies)
        self.assertEqual(hydro.timeout, timeout)
        self.assertIsNone(hydro.url)
        self.assertIsNone(hydro.filename)
        self.assertIsNone(hydro.verify)
        # ===================================================================
        hydro = Hydroportail(proxies={})
        self.assertEqual(hydro.hostname, 'https://hydro.eaufrance.fr')
        self.assertDictEqual(hydro.proxies, {})
        self.assertEqual(hydro.timeout, 300)
        self.assertIsNone(hydro.url)
        self.assertIsNone(hydro.filename)
        self.assertIsNone(hydro.verify)
        # ===================================================================

    def test_get(self):
        """
        Test de la création de l'instance
        """
        # ===================================================================
        datatype = 'DEBCLA'
        code = 'K0550010'
        hydro = Hydroportail()
        res = hydro.get(code=code, datatype=datatype)
        self.assertIsInstance(res.text, str)
        # ===================================================================

    def test_retrieve_debcla(self):
        """
        Test de la création de l'instance
        """
        # ===================================================================
        datatype = 'DEBCLA'
        codes = ['K0550010']
        valid = {('K0550010', 'DEBCLA'): [r'data\K0550010_DEBCLA_0.csv']}
        hydro = Hydroportail()
        filenames = hydro.retrieve(
            codes=codes, datatype=datatype, dirname='data')
        self.assertEqual(filenames, valid)
        for fs in filenames.values():
            for f in fs:
                os.remove(f)
        # ===================================================================

    def test_retrieve_stats(self):
        """
        Test de la création de l'instance
        """
        # ===================================================================
        datatype = 'Q-X'
        codes = ['K0550010']
        valid = {
            ('K0550010', 'Q-X'): [r'data\K0550010_Q-X_seasons.csv',
                                  r'data\K0550010_Q-X_seasons-ouahs.csv',
                                  r'data\K0550010_Q-X_result-quantile.csv',
                                  r'data\K0550010_Q-X_result-empirical.csv',
                                  r'data\K0550010_Q-X_result-parameters.csv',
                                  r'data\K0550010_Q-X_result-observations.csv',
                                  r'data\K0550010_Q-X_result-test.csv',
                                  r'data\K0550010_Q-X_analysis.csv',
                                  r'data\K0550010_Q-X_descriptiveStats.csv']}
        hydro = Hydroportail()
        filenames = hydro.retrieve(
            codes=codes, datatype=datatype, dirname='data', ouahs=True)
        self.assertEqual(filenames, valid)
        for fs in filenames.values():
            for f in fs:
                os.remove(f)
        # ===================================================================

    def test_seasons2ouahs(self):
        """
        Test conversion seasons to ouahs sample
        """
        # ===================================================================
        source = os.path.join(self.dirouahs, 'K7202610_Q-X_seasons.csv')
        filename = os.path.join(
            self.dirouahs, 'K7202610_Q-X_seasons-ouahs.csv')
        tmp_file = os.path.join('data', 'K7202610_Q-X_seasons-ouahs.csv')
        # ===================================================================
        seasons = pnd.read_csv(source, sep=';')
        ouahs = Hydroportail.seasons2ouahs(seasons)
        ouahs.to_csv(tmp_file, sep=',', index=False, quoting=csv.QUOTE_ALL)
        self.assertTrue(filecmp.cmp(filename, tmp_file))
        os.remove(tmp_file)
        # ===================================================================

    def test_datatypes(self):
        """
        Test liste des pages en ligne disponibles
        """
        valid = ['DEBCLA', 'Q-X', 'Q3J-N', 'QJ-X', 'QJ-annuel', 'QM-N']
        self.assertEqual(valid, Hydroportail.get_datatypes())
