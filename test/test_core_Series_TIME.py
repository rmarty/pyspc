#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Series in pyspc.core.series - time series

To run all tests just type:
    python -m unittest test_core_Series_TIME

To run only a class test:
    python -m unittest test_core_Series_TIME.TestSeries

To run only a specific test:
    python -m unittest test_core_Series_TIME.TestSeries.test_fill_cst

"""
# Imports
from datetime import datetime as dt, timedelta as td
import pandas as pnd
import unittest
from unittest import mock
import warnings

# Imports pyspc
from pyspc.core.serie import Serie
from pyspc.core.series import Series


class TestSeries(unittest.TestCase):
    """
    Series class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        # =====================================================================
        provider1 = 'SPC'
        varname1 = 'QH'
        code1 = 'K0000000'
        serie1 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500, 86.400, 82.300, 78.300],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie1.columns = [(code1, varname1)]
        self.serie1 = Serie(
            datval=serie1, code=code1, provider=provider1, varname=varname1,
        )
        # =====================================================================
        provider2 = 'SPC'
        varname2 = 'QH'
        code2 = 'K9999999'
        serie2 = pnd.DataFrame(
            [10, 12, 14, 17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie2.columns = [(code2, varname2)]
        self.serie2 = Serie(
            datval=serie2, code=code2, provider=provider2, varname=varname2
        )
        # =====================================================================
        provider3 = 'SPC'
        varname3 = 'QH'
        code3 = 'K3333333'
        serie3 = pnd.DataFrame(
            [17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 3), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie3.columns = [(code3, varname3)]
        self.serie3 = Serie(
            datval=serie3, code=code3, provider=provider3, varname=varname3
        )
        # =====================================================================
        provider4 = 'SPC'
        varname4 = 'QH'
        code4 = 'K4444444'
        serie4 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 9), freq=td(hours=1)
            )
        )
        serie4.columns = [(code4, varname4)]
        self.serie4 = Serie(
            datval=serie4, code=code4, provider=provider4, varname=varname4,
        )
        # =====================================================================
        self.model = 'PLATHYNES'
        self.series = Series(datatype='obs')
        self.series.add(serie=self.serie1)
        self.series.add(serie=self.serie2)
        self.series.add(serie=self.serie3)
        self.series.add(serie=self.serie4)
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.fill_constant')
    def test_fill_cst(self, mock_method, mock_add):
        """
        Test méthode fill_constant
        """
        # =====================================================================
        valid_arg = {
            ('K0000000', 'QH', None): 1,
            ('K9999999', 'QH', None): 2,
            ('K3333333', 'QH', None): 3,
            ('K4444444', 'QH', None): 4,
        }
        # =====================================================================
        self.series.fill_constant(1, inplace=False)
        mock_method.assert_called_with(1, inplace=False)
        # =====================================================================
        self.series.fill_constant(valid_arg, inplace=False)
        for v in valid_arg.values():
            mock_method.assert_any_call(v, inplace=False)
        # =====================================================================
        self.series.fill_constant(1, inplace=True)
        mock_method.assert_called_with(1, inplace=True)
        # =====================================================================
        self.series.fill_constant(valid_arg, inplace=True)
        for v in valid_arg.values():
            mock_method.assert_any_call(v, inplace=True)
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.fill_linear_interpolation')
    def test_fill_linear(self, mock_method, mock_add):
        """
        Test méthode fill_linear_interpolation
        """
        # =====================================================================
        x = self.series.fill_linear_interpolation()
        mock_method.assert_called_with()
        self.assertIsInstance(x, Series)
        # =====================================================================
        x = self.series.fill_linear_interpolation(inplace=False)
        mock_method.assert_called_with()
        self.assertIsInstance(x, Series)
        # =====================================================================
        x = self.series.fill_linear_interpolation(inplace=True)
        mock_method.assert_called_with()
        self.assertIsNone(x)
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.set_timezone')
    def test_timezone(self, mock_method, mock_add):
        """
        Test méthode shift
        """
        # =====================================================================
        valid_arg = {
            ('K0000000', 'QH', None): 1,
            ('K9999999', 'QH', None): 2,
            ('K3333333', 'QH', None): 3,
            ('K4444444', 'QH', None): 4,
        }
        # =====================================================================
        self.series.set_timezone(timezone=1, inplace=False)
        mock_method.assert_called_with(timezone=1, inplace=False)
        # =====================================================================
        self.series.set_timezone(timezone=valid_arg, inplace=False)
        for v in valid_arg.values():
            mock_method.assert_any_call(timezone=v, inplace=False)
        # =====================================================================
        self.series.set_timezone(timezone=1, inplace=True)
        mock_method.assert_called_with(timezone=1, inplace=True)
        # =====================================================================
        self.series.set_timezone(timezone=valid_arg, inplace=True)
        for v in valid_arg.values():
            mock_method.assert_any_call(timezone=v, inplace=True)
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.shift')
    def test_shift(self, mock_method, mock_add):
        """
        Test méthode shift
        """
        # =====================================================================
        valid_arg = {
            ('K0000000', 'QH', None): 1,
            ('K9999999', 'QH', None): 2,
            ('K3333333', 'QH', None): 3,
            ('K4444444', 'QH', None): 4,
        }
        # =====================================================================
        self.series.shift(periods=1, freq=2, inplace=False)
        mock_method.assert_called_with(periods=1, freq=2, inplace=False)
        # =====================================================================
        self.series.shift(periods=valid_arg, freq=valid_arg, inplace=False)
        for v in valid_arg.values():
            mock_method.assert_any_call(periods=v, freq=v, inplace=False)
        # =====================================================================
        self.series.shift(periods=1, freq=2, inplace=True)
        mock_method.assert_called_with(periods=1, freq=2, inplace=True)
        # =====================================================================
        self.series.shift(periods=valid_arg, freq=valid_arg, inplace=True)
        for v in valid_arg.values():
            mock_method.assert_any_call(periods=v, freq=v, inplace=True)
        # =====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.timelag')
    def test_timelag(self, mock_method, mock_add):
        """
        Test méthode timelag
        """
        # =====================================================================
        valid_arg = {
            ('K0000000', 'QH', None): 1,
            ('K9999999', 'QH', None): 2,
            ('K3333333', 'QH', None): 3,
            ('K4444444', 'QH', None): 4,
        }
        # =====================================================================
        self.series.timelag(1, inplace=False)
        mock_method.assert_called_with(1, inplace=False)
        # =====================================================================
        self.series.timelag(valid_arg, inplace=False)
        for v in valid_arg.values():
            mock_method.assert_any_call(v, inplace=False)
        # =====================================================================
        self.series.timelag(1, inplace=True)
        mock_method.assert_called_with(1, inplace=True)
        # =====================================================================
        self.series.timelag(valid_arg, inplace=True)
        for v in valid_arg.values():
            mock_method.assert_any_call(v, inplace=True)
        # =====================================================================
