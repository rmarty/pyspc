#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Sdb in pyspc.io.dbase.sdb

To run all tests just type:
    python -m unittest test_io_dbase_Sdb

To run only a class test:
    python -m unittest test_io_dbase_Sdb.TestSdb

To run only a specific test:
    python -m unittest test_io_dbase_Sdb.TestSdb.test_init

"""
# Imports
import os
import sqlite3
import unittest
import pyspc.io.dbase.sdb as _dbase


class TestSdb(unittest.TestCase):
    """
    Sdb class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
#        self.dirname = os.path.join('data', 'io', 'dbase')
        self.dirname = os.path.join('data', 'metadata', 'refspc')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        with self.assertRaises(ValueError):
            _dbase.Sdb()
        filename = os.path.join(self.dirname, 'BDD_light.sqlite')
        reader = _dbase.Sdb(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertIsNone(reader._dbase_connect)
        self.assertIsNone(reader._dbase_cursor)
        self.assertIsNone(reader.sql)
        self.assertIsNone(reader._tables)

    def test_connect_error(self):
        """
        Test de la connexion, fermeture de la base
        """
        filename = os.path.join(self.dirname, 'toto.sqlite')
        with self.assertRaises(ValueError):
            _dbase.Sdb(filename=filename)

    def test_connect_close(self):
        """
        Test de la connexion, fermeture de la base
        """
        filename = os.path.join(self.dirname, 'BDD_light.sqlite')
        reader = _dbase.Sdb(filename=filename)
        self.assertIsNone(reader._dbase_connect)
        self.assertIsNone(reader._dbase_cursor)
        reader.connect()
        self.assertIsNotNone(reader._dbase_connect)
        self.assertIsNotNone(reader._dbase_cursor)
        self.assertIsInstance(reader._dbase_connect, sqlite3.Connection)
        self.assertIsInstance(reader._dbase_cursor, sqlite3.Cursor)
        reader.close()
        self.assertIsNone(reader._dbase_connect)
        self.assertIsNone(reader._dbase_cursor)

    def test_connect_execute_close(self):
        """
        Test de la connexion, lecture et fermeture de la base
        """
        valid = [
            (8, 'La Palisse [barrage]'),
            (11, 'Pont de la Borie'),
            (13, 'Goudet'),
            (21, 'Coubon'),
            (32, 'Chadrac-sur-Loire'),
            (34, 'Brives-Charensac'),
            (61, 'Bas-en-Basset')
        ]
        sql = """
            SELECT "id_site_hydro", "nom_site_phyc"
            FROM "SITE_HYDRO"
            WHERE "nom_riviere" IS 'Loire'
            ORDER BY "id_site_hydro"
        """
        filename = os.path.join(self.dirname, 'BDD_light.sqlite')
        reader = _dbase.Sdb(filename=filename)
        reader.connect()
        with self.assertRaises(ValueError):
            reader.execute()
        reader.sql = 1
        with self.assertRaises(ValueError):
            reader.execute()
        reader.sql = sql
        data = reader.execute()
        for d, v in zip(data, valid):
            self.assertEqual(list(d), list(v))
        reader.close()
