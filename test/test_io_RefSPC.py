#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_RefSPC in pyspc.io.refspc

To run all tests just type:
    python -m unittest test_io_RefSPC

To run only a class test:
    python -m unittest test_io_RefSPC.TestRefSPC

To run only a specific test:
    python -m unittest test_io_RefSPC.TestRefSPC.test_init

"""
# Imports
import os.path
import unittest

from pyspc.core.location import Locations
from pyspc.core.reach import Reaches
from pyspc.io.refspc import read_RefSPC


class TestRefSPC(unittest.TestCase):
    """
    RefSPC_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'metadata', 'refspc')
        self.filename = os.path.join(self.dirname, 'BDD_light.sqlite')

    def test_reach(self):
        """
        Test des méthodes de lecture - reach
        """
        # =====================================================================
        codes = ['LC105', 'LC134', 'LC265']
        datatype = 'reach'
        reaches = read_RefSPC(self.filename, codes=codes, datatype=datatype)
        self.assertIsInstance(reaches, Reaches)
        self.assertEqual(len(reaches), 3)
        self.assertIn('LC105', reaches)
        self.assertIn('LC134', reaches)
        self.assertIn('LC265', reaches)
        # =====================================================================

    def test_loc_hydro(self):
        """
        Test des méthodes de lecture - loc_hydro
        """
        # =====================================================================
        datatype = 'loc_hydro'
        # =====================================================================
        codes = ['K0010020', 'K0114020', 'K0550010']
        locs = read_RefSPC(self.filename, codes=codes, datatype=datatype)
        self.assertIsInstance(locs, Locations)
        self.assertEqual(len(locs), 3)
        self.assertIn('K0010020', locs)
        self.assertIn('K0114020', locs)
        self.assertIn('K0550010', locs)
        # =====================================================================
        codes = ['K010002010', 'K026001002', 'K055001010']
        locs = read_RefSPC(self.filename, codes=codes, datatype=datatype)
        self.assertEqual(len(locs), 3)
        self.assertIn('K010002010', locs)
        self.assertIn('K026001002', locs)
        self.assertIn('K055001010', locs)
        # =====================================================================
        codes = ['K01000201001', 'K05500101001', 'K05500101002']
        locs = read_RefSPC(self.filename, codes=codes, datatype=datatype)
        self.assertIsInstance(locs, Locations)
        self.assertEqual(len(locs), 3)
        self.assertIn('K01000201001', locs)
        self.assertIn('K05500101001', locs)
        self.assertIn('K05500101002', locs)
        # =====================================================================
        codes = ['K0100020', 'K0100030', 'K0260020', 'K0260030']
        locs = read_RefSPC(self.filename, codes=codes, datatype=datatype,
                           hydro3=False)
        self.assertIsInstance(locs, Locations)
        self.assertEqual(len(locs), 4)
        self.assertIn('K0100020', locs)
        self.assertIn('K0100030', locs)
        self.assertIn('K0260020', locs)
        self.assertIn('K0260030', locs)
        # =====================================================================

    def test_loc_meteo(self):
        """
        Test des méthodes de lecture - loc_meteo
        """
        # =====================================================================
        codes = ['07235005', '43042002', '43091005']
        datatype = 'loc_meteo'
        locs = read_RefSPC(self.filename, codes=codes, datatype=datatype)
        self.assertIsInstance(locs, Locations)
        self.assertEqual(len(locs), 3)
        self.assertIn('07235005', locs)
        self.assertIn('43042002', locs)
        self.assertIn('43091005', locs)
        # =====================================================================
