#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for OnlineReport in pyspc.webservice.report

To run all tests just type:
    python -m unittest test_webservice_Report

To run only a class test:
    python -m unittest test_webservice_Report.TestReport

To run only a specific test:
    python -m unittest test_webservice_Report.TestReport.test_init

"""
# Imports
from datetime import datetime as dt
import filecmp
import os
import unittest
import warnings

# Imports pyspc
from pyspc.webservice.report import OnlineReport


class TestReport(unittest.TestCase):
    """
    Report class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'webservice', 'report')
        warnings.filterwarnings(action="ignore", category=ResourceWarning)

    def test_init(self):
        """
        Test de la création de l'instance
        """
        hostname = 'https://hydrogr.github.io/BDD-HydroClim/images/fr/'
        reporttype = 'inrae_hydroclim'
        proxies = {'x': 'y'}
        timeout = 10
        report = OnlineReport(reporttype=reporttype, proxies=proxies,
                              timeout=timeout)
        self.assertEqual(report.reporttype, reporttype)
        self.assertEqual(report.hostname, hostname)
        self.assertDictEqual(report.proxies, proxies)
        self.assertEqual(report.timeout, timeout)
        self.assertIsNone(report.url)
        self.assertIsNone(report.filename)
        self.assertIsNone(report.verify)

    def test_inrae_explore2070(self):
        """
        Test téléchargement de fichier - inrae_explore2070
        """
        # =====================================================================
        reporttype = 'inrae_explore2070'
        code = '652'
        basename = 'explore2070_652.pdf'
        filename = os.path.join(self.dirname, basename)
        url = r'https://piece-jointe-carto.developpement-durable.gouv.fr'\
            r'/NAT007/Explore2070/652.pdf'
        report = OnlineReport(reporttype=reporttype)
        # =====================================================================
        report.set_url(code=code)
        self.assertEqual(report.url, url)
        # =====================================================================
        tmp_filename = os.path.join('data', basename)
        report.set_filename(code=code, dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        # =====================================================================
        report.retrieve_byrequests()
        self.assertTrue(filecmp.cmp(filename, report.filename))
        os.remove(report.filename)
        # =====================================================================

    def test_inrae_hydroclim(self):
        """
        Test téléchargement de fichier - inrae_hydroclim
        """
        # =====================================================================
        reporttype = 'inrae_hydroclim'
        code = 'K153301001'
        basename = f'{code}_INRAE_BDD-HydroClim_fact_sheet_FR.png'
        filename = os.path.join(self.dirname, basename)
        url = f'https://hydrogr.github.io/BDD-HydroClim/images/fr/{basename}'
        report = OnlineReport(reporttype=reporttype)
        # =====================================================================
        report.set_url(code=code)
        self.assertEqual(report.url, url)
        # =====================================================================
        tmp_filename = os.path.join('data', basename)
        report.set_filename(code=code, dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        # =====================================================================
        report.retrieve_byrequests()
        self.assertTrue(filecmp.cmp(filename, report.filename))
        os.remove(report.filename)
        # =====================================================================

    def test_inrae_premhyce(self):
        """
        Test téléchargement de fichier - inrae_premhyce
        """
        # =====================================================================
        reporttype = 'inrae_premhyce'
        code = 'DREAL_Centre_H3203310_Presages_CEP'
        basename = 'DREAL_Centre_H3203310_Presages_CEP.pdf'
        filename = os.path.join(self.dirname, basename)
        report = OnlineReport(reporttype=reporttype)
        # =====================================================================
        report.set_url(code=code)
        tmp_filename = os.path.join('data', basename)
        report.set_filename(code=code, dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        # =====================================================================
        report.retrieve_byrequests()
        self.assertTrue(filecmp.cmp(filename, report.filename))
        os.remove(report.filename)
        # =====================================================================

    def test_inrae_shyreg(self):
        """
        Test téléchargement de fichier - inrae_shyreg
        """
        # =====================================================================
        reporttype = 'inrae_shyreg'
        code = 'LO2228'
        basename = 'BNBV_LO2228.pdf'
        filename = os.path.join(self.dirname, basename)
        report = OnlineReport(reporttype=reporttype)
        # =====================================================================
        report.set_url(code=code)
        tmp_filename = os.path.join('data', basename)
        report.set_filename(code=code, dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        report.retrieve_byrequests()
        self.assertTrue(filecmp.cmp(filename, report.filename))
        os.remove(report.filename)
        # =====================================================================

    def test_inrae_shyreg_bnbv(self):
        """
        Test téléchargement de fichier - inrae_shyreg_bnbv
        """
        # =====================================================================
        reporttype = 'inrae_shyreg_bnbv'
        code = 'LO2228'
        basename = 'BNBV_LO2228.pdf'
        filename = os.path.join(self.dirname, basename)
        report = OnlineReport(reporttype=reporttype)
        # =====================================================================
        report.set_url(code=code)
        tmp_filename = os.path.join('data', basename)
        report.set_filename(code=code, dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        report.retrieve_byrequests()
        self.assertTrue(filecmp.cmp(filename, report.filename))
        os.remove(report.filename)
        # =====================================================================

    def test_inrae_shyreg_hydro(self):
        """
        Test téléchargement de fichier - inrae_shyreg_hydro
        """
        # =====================================================================
        reporttype = 'inrae_shyreg_hydro'
        code = 'K0403010'
        basename = 'HYDRO_K0403010.pdf'
        filename = os.path.join(self.dirname, basename)
        report = OnlineReport(reporttype=reporttype)
        # =====================================================================
        report.set_url(code=code)
        tmp_filename = os.path.join('data', basename)
        report.set_filename(code=code, dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        report.retrieve_byrequests()
        self.assertTrue(filecmp.cmp(filename, report.filename))
        os.remove(report.filename)
        # =====================================================================

    def test_mf_station(self):
        """
        Test téléchargement de fichier - mf_station
        """
        # =====================================================================
        url = r'https://donneespubliques.meteofrance.fr/'\
            'metadonnees_publiques/fiches/fiche_07154005.pdf'
        reporttype = 'mf_station'
        code = '07154005'
        basename = 'fiche_07154005.pdf'
        filename = os.path.join(self.dirname, basename)
        report = OnlineReport(reporttype=reporttype)
        # =====================================================================
        report.set_url(code=code)
        self.assertEqual(report.url, url)
        # =====================================================================
        tmp_filename = os.path.join('data', basename)
        report.set_filename(code=code, dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        # =====================================================================
        report.retrieve_byrequests()
        self.assertTrue(filecmp.cmp(filename, report.filename))
        os.remove(report.filename)
        # =====================================================================

    def test_mf_station_geojson(self):
        """
        Test téléchargement de fichier - mf_station
        """
        # =====================================================================
        url = r'https://donneespubliques.meteofrance.fr/'\
            'metadonnees_publiques/fiches/fiches.json'
        reporttype = 'mf_station_geojson'
        basename = 'fiches.json'
        filename = os.path.join(self.dirname, basename)
        report = OnlineReport(reporttype=reporttype)
        # =====================================================================
        report.set_url()
        self.assertEqual(report.url, url)
        # =====================================================================
        tmp_filename = os.path.join('data', basename)
        report.set_filename(dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        # =====================================================================
        report.retrieve_byrequests()
        self.assertTrue(filecmp.cmp(filename, report.filename))
        os.remove(report.filename)
        # =====================================================================

    def test_mf_clim(self):
        """
        Test téléchargement de fichier - mf_clim
        """
        # =====================================================================
        url = r'https://donneespubliques.meteofrance.fr/FichesClim/'\
            'FICHECLIM_03155003.pdf'
        reporttype = 'mf_clim'
        code = '03155003'
        basename = 'FICHECLIM_03155003.pdf'
        filename = os.path.join(self.dirname, basename)
        report = OnlineReport(reporttype=reporttype)
        # =====================================================================
        report.set_url(code=code)
        self.assertEqual(report.url, url)
        # =====================================================================
        tmp_filename = os.path.join('data', basename)
        report.set_filename(code=code, dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        # =====================================================================
        report.retrieve_byrequests()
        self.assertTrue(filecmp.cmp(filename, report.filename))
        os.remove(report.filename)
        # =====================================================================

    def test_mf_climdata(self):
        """
        Test téléchargement de fichier - mf_climdata
        """
        # =====================================================================
        url = r'https://donneespubliques.meteofrance.fr/FichesClim/'\
            'FICHECLIM_03155003.data'
        reporttype = 'mf_climdata'
        code = '03155003'
        basename = 'FICHECLIM_03155003.data'
        filename = os.path.join(self.dirname, basename)
        report = OnlineReport(reporttype=reporttype)
        # =====================================================================
        report.set_url(code=code)
        self.assertEqual(report.url, url)
        # =====================================================================
        tmp_filename = os.path.join('data', basename)
        report.set_filename(code=code, dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        # =====================================================================
        report.retrieve_byrequests()
        self.assertTrue(filecmp.cmp(filename, report.filename))
        os.remove(report.filename)
        # =====================================================================

    def test_mf_warning(self):
        """
        Test téléchargement de fichier - mf_warning
        """
        # =====================================================================
        url = r'http://vigilance-public.meteo.fr/telechargement.php'\
            '?dateVigi={}&base={}'
        reporttype = 'mf_warning'
        report = OnlineReport(reporttype=reporttype)
        # =====================================================================
        valid = {
            dt(2008, 11, 1): 'vigilance1a3',
            dt(2011, 9, 30): 'vigilance1a3',
            dt(2011, 10, 1): 'vigilance4',
            dt(2020, 1, 1): 'vigilance4',
            dt(2021, 1, 1): 'vigilance4'
        }
        for d, b in valid.items():
            f = os.path.join(
                'data',
                "vigilance_{}.zip".format(d.strftime('%Y_%m_%d'))
            )
            u = url.format(d.strftime('%Y-%m-%d'), b)
            report.set_url(date=d)
            self.assertEqual(report.url, u)
            report.set_filename(date=d, dirname='data')
            self.assertEqual(report.filename, f)
        # =====================================================================

    def test_mf_dailyreport(self):
        """
        Test téléchargement de fichier - mf_dailyreport
        """
        # =====================================================================
        url = r'https://donneespubliques.meteofrance.fr/donnees_libres/'\
            'bulletins/BQA/20170613.pdf'
        reporttype = 'mf_dailyreport'
        date = dt(2017, 6, 13)
        basename = '20170613.pdf'
        filename = os.path.join(self.dirname, basename)
        report = OnlineReport(reporttype=reporttype)
        # =====================================================================
        report.set_url(date=date)
        self.assertEqual(report.url, url)
        # =====================================================================
        tmp_filename = os.path.join('data', basename)
        report.set_filename(date=date, dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        # =====================================================================
        report.retrieve_byrequests()
        self.assertTrue(filecmp.cmp(filename, report.filename))
        os.remove(report.filename)
        # =====================================================================

    def test_mf_monthlyreport(self):
        """
        Test téléchargement de fichier - mf_monthlyreport
        """
        # =====================================================================
        reporttype = 'mf_monthlyreport'
        report = OnlineReport(reporttype=reporttype)
        hostname = 'https://donneespubliques.meteofrance.fr/donnees_libres/'\
            'bulletins/'
        # =====================================================================
        cases = [
            ('43', dt(2008, 11, 1), 'BCMD', 'BCMD_43_200811.pdf'),  # Hte-Loire
            ('00', dt(2019, 11, 1), 'BCM', '201911.pdf'),  # France
            ('03', dt(2019, 11, 1), 'BCMR', 'BCMR_03_201911.pdf'),  # Auvergne
            ('01', dt(2020, 6, 1), 'BCMR', 'BCMR_01_202006.pdf'),  # Aura
        ]
        for case in cases:
            code = case[0]
            date = case[1]
            subdir = case[2]
            basename = case[3]
            filename = os.path.join(self.dirname, basename)
            url = '{}{}/{}'.format(hostname, subdir, basename)
            report.set_url(code=code, date=date)
            self.assertEqual(report.url, url)
            tmp_filename = os.path.join('data', basename)
            report.set_filename(
                code=code, date=date, dirname='data')
            self.assertEqual(report.filename, tmp_filename)
            report.retrieve_byrequests()
            self.assertTrue(filecmp.cmp(filename, report.filename))
            os.remove(report.filename)
        # =====================================================================

    def test_vigicrues_fcst(self):
        """
        Test téléchargement de fichier - vigicrues_fcst
        """
        # =====================================================================
        reporttype = 'vigicrues_fcst'
        code = 'K055001010'
        varname = 'Q'
        basename = 'vigicrues_fcst_{}_{}.json'.format(code, varname)
        report = OnlineReport(reporttype=reporttype)
        url = 'https://www.vigicrues.gouv.fr/services/previsions.json/'\
            '?CdStationHydro=K055001010&GrdSimul=Q&FormatDate=iso'
        # =====================================================================
        report.set_url(code=code, varname=varname)
        self.assertEqual(report.url, url)
        # =====================================================================
        tmp_filename = os.path.join('data', basename)
        report.set_filename(code=code, varname=varname, dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        # =====================================================================
#        report.retrieve_byrequests()
        # =====================================================================

    def test_vigicrues_loc(self):
        """
        Test téléchargement de fichier - vigicrues_loc
        """
        # =====================================================================
        reporttype = 'vigicrues_loc'
        code = 'K055001010'
        basename = 'vigicrues_loc_{}.json'.format(code)
        report = OnlineReport(reporttype=reporttype)
        url = 'https://www.vigicrues.gouv.fr/services/station.json/'\
            '?CdStationHydro=K055001010'
        # =====================================================================
        report.set_url(code=code)
        self.assertEqual(report.url, url)
        # =====================================================================
        tmp_filename = os.path.join('data', basename)
        report.set_filename(code=code, dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        # =====================================================================
#        report.retrieve_byrequests()
        # =====================================================================

    def test_vigicrues_obs(self):
        """
        Test téléchargement de fichier - vigicrues_obs
        """
        # =====================================================================
        reporttype = 'vigicrues_obs'
        code = 'K055001010'
        varname = 'Q'
        basename = 'vigicrues_obs_{}_{}.json'.format(code, varname)
        report = OnlineReport(reporttype=reporttype)
        url = 'https://www.vigicrues.gouv.fr/services/observations.json/'\
            '?CdStationHydro=K055001010&GrdSerie=Q&FormatDate=iso'
        # =====================================================================
        report.set_url(code=code, varname=varname)
        self.assertEqual(report.url, url)
        # =====================================================================
        tmp_filename = os.path.join('data', basename)
        report.set_filename(code=code, varname=varname, dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        # =====================================================================
#        report.retrieve_byrequests()
        # =====================================================================

    def test_vigicrues_sandre(self):
        """
        Test téléchargement de fichier - vigicrues_sandre
        """
        # =====================================================================
        reporttype = 'vigicrues_sandre'
        code = 'K055001010'
        varname = 'Q'
        basename = 'vigicrues_sandre_{}_{}.xml'.format(code, varname)
        report = OnlineReport(reporttype=reporttype)
        url = 'https://www.vigicrues.gouv.fr/services/observations.xml/'\
            '?CdStationHydro=K055001010&GrdSerie=Q'
        # =====================================================================
        report.set_url(code=code, varname=varname)
        self.assertEqual(report.url, url)
        # =====================================================================
        tmp_filename = os.path.join('data', basename)
        report.set_filename(code=code, varname=varname, dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        # =====================================================================
#        report.retrieve_byrequests()
        # =====================================================================

    def test_vigicrues_reach(self):
        """
        Test téléchargement de fichier - vigicrues_reach
        """
        # =====================================================================
        reporttype = 'vigicrues_reach'
        basename = 'vigicrues_reaches_{}.json'.format(
            dt.utcnow().strftime('%Y%m%d%H%M'))
        report = OnlineReport(reporttype=reporttype)
        url = 'https://www.vigicrues.gouv.fr/services/vigicrues.geojson'
        # =====================================================================
        report.set_url()
        self.assertEqual(report.url, url)
        # =====================================================================
        tmp_filename = os.path.join('data', basename)
        report.set_filename(dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        # =====================================================================
#        report.retrieve_byrequests()
        # =====================================================================

    def test_vigicrues1_geoinfo(self):
        """
        Test téléchargement de fichier - vigicrues-1_geoinfo
        """
        # =====================================================================
        reporttype = 'vigicrues-1_geoinfo'
        basename = 'vigicrues-1_geoinfo_{}.geojson'.format(
            dt.utcnow().strftime('%Y%m%d%H%M'))
        report = OnlineReport(reporttype=reporttype)
        url = 'https://www.vigicrues.gouv.fr/services/1/InfoVigiCru.geojson'
        # =====================================================================
        report.set_url()
        self.assertEqual(report.url, url)
        # =====================================================================
        tmp_filename = os.path.join('data', basename)
        report.set_filename(dirname='data')
        self.assertEqual(report.filename, tmp_filename)
        # =====================================================================
#        report.retrieve_byrequests()
        # =====================================================================

    def test_vigicrues1_info(self):
        """
        Test téléchargement de fichier - vigicrues-1_info
        """
        # =====================================================================
        reporttype = 'vigicrues-1_info'
        date = dt.utcnow()
        datestr = date.strftime('%Y%m%d%H%M')
        basename = 'vigicrues-1_info_{}-{}_{}.json'
        report = OnlineReport(reporttype=reporttype)
        baseurl = 'https://www.vigicrues.gouv.fr/services/1/'
        valid = [('30', '5'), ('LC165', '8')]
        # =====================================================================
        for v in valid:

            url = baseurl + 'InfoVigiCru.jsonld/?CdEntVigiCru={}'\
                '&TypEntVigiCru={}'.format(*v)
            bname = basename.format(*v, datestr)
            report.set_url(code=v[0], codetype=v[1])
            self.assertEqual(report.url, url)
            tmp_filename = os.path.join('data', bname)
            report.set_filename(dirname='data', code=v[0], codetype=v[1],
                                date=date)
            self.assertEqual(report.filename, tmp_filename)
#            report.retrieve_byrequests()
        # =====================================================================

    def test_vigicrues1_domain(self):
        """
        Test téléchargement de fichier - vigicrues-1_domain
        """
        # =====================================================================
        reporttype = 'vigicrues-1_domain'
        date = dt.utcnow()
        datestr = date.strftime('%Y%m%d%H%M')
        basename = 'vigicrues-1_domain_{}-{}_{}.json'
        report = OnlineReport(reporttype=reporttype)
        baseurl = 'https://www.vigicrues.gouv.fr/services/1/'
        valid = [('30', '5')]
        # =====================================================================
        for v in valid:

            url = baseurl + 'TerEntVigiCru.jsonld/?CdEntVigiCru={}'\
                '&TypEntVigiCru={}'.format(*v)
            bname = basename.format(*v, datestr)
            report.set_url(code=v[0], codetype=v[1])
            self.assertEqual(report.url, url)
            tmp_filename = os.path.join('data', bname)
            report.set_filename(dirname='data', code=v[0], codetype=v[1],
                                date=date)
            self.assertEqual(report.filename, tmp_filename)
#            report.retrieve_byrequests()
        # =====================================================================

    def test_vigicrues1_reach(self):
        """
        Test téléchargement de fichier - vigicrues-1_reach
        """
        # =====================================================================
        reporttype = 'vigicrues-1_reach'
        date = dt.utcnow()
        datestr = date.strftime('%Y%m%d%H%M')
        basename = 'vigicrues-1_reach_{}-{}_{}.json'
        report = OnlineReport(reporttype=reporttype)
        baseurl = 'https://www.vigicrues.gouv.fr/services/1/'
        valid = [('LC165', '8')]
        # =====================================================================
        for v in valid:

            url = baseurl + 'TronEntVigiCru.jsonld/?CdEntVigiCru={}'\
                '&TypEntVigiCru={}'.format(*v)
            bname = basename.format(*v, datestr)
            report.set_url(code=v[0], codetype=v[1])
            self.assertEqual(report.url, url)
            tmp_filename = os.path.join('data', bname)
            report.set_filename(dirname='data', code=v[0], codetype=v[1],
                                date=date)
            self.assertEqual(report.filename, tmp_filename)
#            report.retrieve_byrequests()
        # =====================================================================

    def test_vigicrues1_loc(self):
        """
        Test téléchargement de fichier - vigicrues-1_loc
        """
        # =====================================================================
        reporttype = 'vigicrues-1_loc'
        date = dt.utcnow()
        datestr = date.strftime('%Y%m%d%H%M')
        basename = 'vigicrues-1_loc_{}-{}_{}.json'
        report = OnlineReport(reporttype=reporttype)
        baseurl = 'https://www.vigicrues.gouv.fr/services/1/'
        valid = [('K400001010', '7')]
        # =====================================================================
        for v in valid:

            url = baseurl + 'StaEntVigiCru.jsonld/?CdEntVigiCru={}'\
                '&TypEntVigiCru={}'.format(*v)
            bname = basename.format(*v, datestr)
            report.set_url(code=v[0], codetype=v[1])
            self.assertEqual(report.url, url)
            tmp_filename = os.path.join('data', bname)
            report.set_filename(dirname='data', code=v[0], codetype=v[1],
                                date=date)
            self.assertEqual(report.filename, tmp_filename)
#            report.retrieve_byrequests()
        # =====================================================================

    def test_reporttypes(self):
        """
        Test liste des documents en ligne disponibles
        """
        valid = [
            'inrae_explore2070',
            'inrae_hydroclim',
            'inrae_premhyce',
            'inrae_shyreg',
            'inrae_shyreg_bnbv',
            'inrae_shyreg_hydro',
            'mf_clim',
            'mf_climdata',
            'mf_dailyreport',
            'mf_monthlyreport',
            'mf_station',
            'mf_station_geojson',
            'mf_warning',
            'vigicrues-1_domain',  # service version 1.1 (ajout 2024)
            'vigicrues-1_geoinfo',  # service version 1.1 (ajout 2024)
            'vigicrues-1_info',  # service version 1.1 (ajout 2024)
            'vigicrues-1_loc',  # service version 1.1 (ajout 2024)
            'vigicrues-1_reach',  # service version 1.1 (ajout 2024)
            'vigicrues_fcst',
            'vigicrues_loc',
            'vigicrues_obs',  # version json
            'vigicrues_reach',
            'vigicrues_sandre',  # anciennement vigicrues_obs
        ]
        self.assertEqual(valid, OnlineReport.get_reporttypes())
