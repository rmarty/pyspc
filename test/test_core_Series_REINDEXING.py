#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Series in pyspc.core.series

To run all tests just type:
    python -m unittest test_core_Series_REINDEXING

To run only a class test:
    python -m unittest test_core_Series_REINDEXING.TestSeries

To run only a specific test:
    python -m unittest test_core_Series_REINDEXING.TestSeries.test_REINDEXING

"""
# Imports
from datetime import datetime as dt, timedelta as td
import numpy as np
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest
from unittest import mock
import warnings

# Imports pyspc
from pyspc.core.serie import Serie
from pyspc.core.series import Series


class TestSeries(unittest.TestCase):
    """
    Series class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        # ====================================================================
        provider1 = 'SPC'
        varname1 = 'QH'
        code1 = 'K0000000'
        serie1 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500, 86.400, 82.300, 78.300],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie1.columns = [(code1, varname1)]
        self.serie1 = Serie(
            datval=serie1, code=code1, provider=provider1, varname=varname1,
        )
        # ====================================================================
        provider2 = 'SPC'
        varname2 = 'QH'
        code2 = 'K9999999'
        serie2 = pnd.DataFrame(
            [10, 12, 14, 17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie2.columns = [(code2, varname2)]
        self.serie2 = Serie(
            datval=serie2, code=code2, provider=provider2, varname=varname2
        )
        # ====================================================================
        provider3 = 'SPC'
        varname3 = 'QH'
        code3 = 'K3333333'
        serie3 = pnd.DataFrame(
            [17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 3), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie3.columns = [(code3, varname3)]
        self.serie3 = Serie(
            datval=serie3, code=code3, provider=provider3, varname=varname3
        )
        # ====================================================================
        provider4 = 'SPC'
        varname4 = 'QH'
        code4 = 'K4444444'
        serie4 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 9), freq=td(hours=1)
            )
        )
        serie4.columns = [(code4, varname4)]
        self.serie4 = Serie(
            datval=serie4, code=code4, provider=provider4, varname=varname4,
        )
        # ====================================================================
        self.model = 'PLATHYNES'
        self.series = Series(datatype='obs')
        self.series.add(serie=self.serie1)
        self.series.add(serie=self.serie2)
        self.series.add(serie=self.serie3)
        self.series.add(serie=self.serie4)
        # ====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.between_dates')
    def test_bwd(self, mock_method, mock_add):
        """
        Test méthode between_dates
        """
        # ====================================================================
        fdt_arg = {
            ('K0000000', 'QH', None): dt(2016, 5, 31, 1),
            ('K9999999', 'QH', None): dt(2016, 5, 31, 2),
            ('K3333333', 'QH', None): dt(2016, 5, 31, 3),
            ('K4444444', 'QH', None): dt(2016, 5, 31, 4),
        }
        ldt_arg = {
            ('K0000000', 'QH', None): dt(2016, 6, 1, 1),
            ('K9999999', 'QH', None): dt(2016, 6, 1, 2),
            ('K3333333', 'QH', None): dt(2016, 6, 1, 3),
            ('K4444444', 'QH', None): dt(2016, 6, 1, 4),
        }
        # ====================================================================
        self.series.between_dates(
            dt(2016, 5, 31, 1), dt(2016, 6, 1, 1), inplace=False)
        mock_method.assert_called_with(
            dt(2016, 5, 31, 1), dt(2016, 6, 1, 1), inplace=False)
        # ====================================================================
        self.series.between_dates(fdt_arg, ldt_arg, inplace=False)
        for f, l in zip(fdt_arg.values(), ldt_arg.values()):
            mock_method.assert_any_call(f, l, inplace=False)
        # ====================================================================
        self.series.between_dates(
            dt(2016, 5, 31, 1), dt(2016, 6, 1, 1), inplace=True)
        mock_method.assert_called_with(
            dt(2016, 5, 31, 1), dt(2016, 6, 1, 1), inplace=True)
        # ====================================================================
        self.series.between_dates(fdt_arg, ldt_arg, inplace=True)
        for f, l in zip(fdt_arg.values(), ldt_arg.values()):
            mock_method.assert_any_call(f, l, inplace=True)
        # ====================================================================

    def test_concat(self):
        """
        Test de la méthode concat
        """
        # ====================================================================
        valid = pnd.DataFrame(
            {
                ('K0000000', 'QH', None): [
                    35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000,
                    115.000, 103.000, 92.500, 86.400, 82.300, 78.300],
                ('K9999999', 'QH', None): [
                    10, 12, 14, 17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
                ('K3333333', 'QH', None): [
                    np.nan, np.nan, np.nan,
                    17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
                ('K4444444', 'QH', None): [
                    35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000,
                    115.000, 103.000, 92.500, np.nan, np.nan, np.nan],
            },
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
#        valid.columns = valid.columns.to_flat_index()
        valid.columns = [
            ('K0000000', 'QH', None),
            ('K9999999', 'QH', None),
            ('K3333333', 'QH', None),
            ('K4444444', 'QH', None),
        ]
        # ====================================================================
        df = self.series.concat()
        assert_frame_equal(df, valid)
        # ====================================================================

    @mock.patch('pyspc.core.series.Series.extend')
    @mock.patch('pyspc.core.serie.Serie.split')
    def test_split(self, mock_method, mock_extend):
        """
        Test de la méthode concat
        """
        # ====================================================================
        self.series.split(value=3, method='chunks')
        mock_method.assert_called_with(value=3, method='chunks')
        # ====================================================================
        values = {
            ('K0000000', 'QH', None): 1,
            ('K9999999', 'QH', None): 2,
            ('K3333333', 'QH', None): 3,
            ('K4444444', 'QH', None): 4,
        }
        methods = {
            ('K0000000', 'QH', None): 'chunks',
            ('K9999999', 'QH', None): 'chunk_size',
            ('K3333333', 'QH', None): 'days',
            ('K4444444', 'QH', None): 'dates',
        }
        self.series.split(value=values, method=methods)
        for v, m in zip(values.values(), methods.values()):
            mock_method.assert_any_call(value=v, method=m)
        # ====================================================================

    @mock.patch('pyspc.core.series.Series.add')
    @mock.patch('pyspc.core.serie.Serie.stripna')
    def test_stripna(self, mock_method, mock_add):
        """
        Test méthode stripna
        """
        # ====================================================================
        x = self.series.stripna()
        mock_method.assert_called_with()
        self.assertIsInstance(x, Series)
        # ====================================================================
        x = self.series.stripna(inplace=False)
        mock_method.assert_called_with()
        self.assertIsInstance(x, Series)
        # ====================================================================
        x = self.series.stripna(inplace=True)
        mock_method.assert_called_with()
        self.assertIsNone(x)
        # ====================================================================
