#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Vigicrues_Reach in pyspc.metadata.vigicrues

To run all tests just type:
    python -m unittest test_metadata_Vigicrues

To run only a class test:
    python -m unittest test_metadata_Vigicrues.TestVigicrues_Reach

To run only a specific test:
    python -m unittest test_metadata_Vigicrues.TestVigicrues_Reach.test_init

"""
# Imports
from datetime import datetime as dt
import os
import unittest

from pyspc.metadata.vigicrues import (
    Vigicrues_Reach, Vigicrues_Location, Service)


class TestVigicrues_Reach(unittest.TestCase):
    """
    Vigicrues class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'metadata', 'vigicrues')

    def test_init(self):
        """
        Test des méthodes de lecture - Utile si unittest + mock sur urllib
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'troncons.json')
        vigi = Vigicrues_Reach(filename=filename)
        self.assertEqual(vigi.filename, filename)
        # =====================================================================

    def test_read(self):
        """
        Test des méthodes de lecture
        """
        valid = \
            {"type": "FeatureCollection",
             "crs": {"type": "name",
                     "properties": {"name": "urn:ogc:def:crs:EPSG::2154"}},
             "VersionFlux": 1,
             "CdIntervenant": "id.eaufrance.fr/int/1537",
             "UriScenarioSandre": "id.eaufrance.fr/scn/pcru_geojson/1",
             "RefInfoVigiCru": "22092018_10",
             "DtHrInfoVigiCru": dt(2018, 9, 22, 7, 50),
             "features": [
                 {"type": "Feature",
                  "properties": {"gid": 117,
                                 "CdEntVigiCru": "LC110",
                                 "NomEntVigiCru": "Haut bassin de la Loire",
                                 "CdTCC": 10,
                                 "NivSituVigiCruEnt": 1},
                  "geometry": {"type": "MultiLineString", "coordinates": []},
                  "id":117},
                 {"type": "Feature",
                  "properties": {"gid": 118,
                                 "CdEntVigiCru": "LC120",
                                 "NomEntVigiCru": "Loire forézienne",
                                 "CdTCC": 10,
                                 "NivSituVigiCruEnt": 1},
                  "geometry": {"type": "MultiLineString", "coordinates": []},
                  "id": 118}
              ]}
        # =====================================================================
        filename = os.path.join(self.dirname, 'troncons.json')
        vigi = Vigicrues_Reach(filename=filename)
        content = vigi.read()
        self.assertDictEqual(content, valid)
        # =====================================================================


class TestVigicrues_Location(unittest.TestCase):
    """
    Vigicrues class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'metadata', 'vigicrues')

    def test_init(self):
        """
        Test des méthodes de lecture - Utile si unittest + mock sur urllib
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K118001010_station.json')
        vigi = Vigicrues_Location(filename=filename)
        self.assertEqual(vigi.filename, filename)
        # =====================================================================

    def test_read(self):
        """
        Test des méthodes de lecture
        """
        valid = {
            'VersionFlux': 'Beta 0.3k',
            'CdStationHydro': 'K118001010',
            'LbStationHydro': 'Digoin [Pont canal]',
            'LbCoursEau': 'Loire',
            'CdStationHydroAncienRef': 'K1180010',
            'CoordStationHydro': {'CoordXStationHydro': '775417',
                                  'CoordYStationHydro': '6598045'},
            'CdCommune': '71176',
            'Evenement': [],
            'VigilanceCrues': {
                'StationPrevision': 't', 'Photo': 't', 'ZoomInitial': '3',
                'PereBoitEntVigiCru': {
                    'CdEntVigiCru': '10',
                    'Link': 'https://www.vigicrues.gouv.fr/services/bulletin.json?CdEntVigiCru=10'},
                'CruesHistoriques': [
                    {'LbUsuel': 'Crue du 03/11/2008', 'ValHauteur': '4.95',
                     'ValDebit': '1850.00'},
                    {'LbUsuel': 'Crue du 04/12/2003', 'ValHauteur': '4.88',
                     'ValDebit': '1800.00'},
                    {'LbUsuel': 'Crue du 20/05/2013', 'ValHauteur': '3.14',
                     'ValDebit': '675.00'}],
                'StationsBassin': [
                    {'CdStationHydro': 'K117321001', 'LbStationHydro': "Montceaux-l'Étoile", 'LbCoursEau': 'Arconce', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K117321001'},
                    {'CdStationHydro': 'K113000201', 'LbStationHydro': 'Mornay [Villorbaine]', 'LbCoursEau': 'Arconce', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K113000201'},
                    {'CdStationHydro': 'K091003010', 'LbStationHydro': 'Commelle-Vernay [Barrage de Villerest]', 'LbCoursEau': 'Loire', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K091003010'},
                    {'CdStationHydro': 'K091001010', 'LbStationHydro': 'Villerest [Aval]', 'LbCoursEau': 'Loire', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K091001010'},
                    {'CdStationHydro': 'K091001011', 'LbStationHydro': 'Villerest [Pont de Villerest]', 'LbCoursEau': 'Loire', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K091001011'},
                    {'CdStationHydro': 'K094301001', 'LbStationHydro': 'Amplepuis', 'LbCoursEau': 'Rhins', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K094301001'},
                    {'CdStationHydro': 'K098301101', 'LbStationHydro': 'St-Vincent-de-Boisset [Pont Maréchal]', 'LbCoursEau': 'Rhins', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K098301101'},
                    {'CdStationHydro': 'K106301002', 'LbStationHydro': 'Pouilly-sous-Charlieu', 'LbCoursEau': 'Sornin', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K106301002'},
                    {'CdStationHydro': 'K108402001', 'LbStationHydro': 'Bénisson-Dieu', 'LbCoursEau': 'Teyssonne', 'idspc': '10', 'Link': 'https://www.vigicrues.gouv.fr/services/station.json?CdStationHydro=K108402001'}],
                'FluxDonnees': {
                    'Observations': {
                        'Hauteurs': 'https://www.vigicrues.gouv.fr/services/observations.json?CdStationHydro=K118001010&FormatDate=iso',
                        'Debits': 'https://www.vigicrues.gouv.fr/services/observations.json?CdStationHydro=K118001010&FormatDate=iso&GrdSerie=Q'},
                    'Previsions': {
                        'Hauteurs': 'https://www.vigicrues.gouv.fr/services/previsions.json?CdStationHydro=K118001010&FormatDate=iso',
                        'Debits': 'https://www.vigicrues.gouv.fr/services/previsions.json?CdStationHydro=K118001010&FormatDate=iso&GrdSimul=Q'}
                }
            }
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'K118001010_station.json')
        vigi = Vigicrues_Location(filename=filename)
        content = vigi.read()
        self.assertDictEqual(content, valid)
        # =====================================================================


class TestVigicrues_Service(unittest.TestCase):
    """
    Vigicrues class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'metadata', 'vigicrues')

    def test_init(self):
        """
        Test des méthodes de lecture - Utile si unittest + mock sur urllib
        """
        # =====================================================================
        filename = os.path.join(self.dirname,
                                'vigicrues-1_info_30-5_202402140927.json')
        vigi = Service(filename=filename)
        self.assertEqual(vigi.filename, filename)
        # =====================================================================

    def test_read_info(self):
        """
        Test des méthodes de lecture - flux InfoVigiCru.jsonld
        """
        valid = {
            '@context': {'mat': 'http://id.eaufrance.fr/ddd/mat/3.1/',
                         'int': 'http://id.eaufrance.fr/ddd/int/2/',
                         'vic': 'http://id.eaufrance.fr/ddd/vic/1.1/',
                         'com': 'http://id.eaufrance.fr/ddd/com/4/',
                         'hyd': 'http://id.eaufrance.fr/ddd/hyd/2.3/',
                         'eth': 'http://id.eaufrance.fr/ddd/eth/2/',
                         'zon': 'http://id.eaufrance.fr/ddd/zon/2.2/',
                         'mdo': 'http://id.eaufrance.fr/ddd/mdo/1.4/'},
            'mat:Scenario': {'Flux': {'Version': 'béta',
                                      'DateRevision': '2019-09-16T11:00:00'},
                             'mat:CodeScenario': 'VICGeo',
                             'mat:VersionScenario': '1beta',
                             'mat:NomScenario': 'Vigilance crues aux formats géo',
                             'DateHeureCreationFichier': '2024-02-14T09:27:12+00:00',
                             'Emetteur': {'@id': 'http://id.eaufrance.fr/int/1537',
                                          'int:CdIntervenant': '1537'}},
            'vic:InfoVigiCru': {'vic:RefInfoVigiCru': '14022024_10',
                                'vic:TypInfoVigiCru': '1',
                                'vic:DtHrInfoVigiCru': '2024-02-14T08:17:00+0100',
                                'vic:DtHrSuivInfoVigiCru': '2024-02-14T16:00:00+0100',
                                'vic:NivInfoVigiCru': '1',
                                'vic:EstNivCalInfoVigiCru': True,
                                'vic:StInfoVigiCru': '4',
                                'vic:SituActuInfoVigiCru': 'Pas de vigilance particulière requise.',
                                'vic:ConsInfoVigiCru': '',
                                'vic:ConseqInfoVigiCru': '',
                                'vic:porteSurLEntite': {'@id': 'http://id.eaufrance.fr/TerEntVigiCru/30',
                                                        'vic:CdEntVigiCru': '30', 'vic:TypEntVigiCru': '5'},
                                'vic:aEteProduitePar': {'@id': 'http://id.eaufrance.fr/int/ARenseigner',
                                                        'vic:CdIntervenant': 'ARenseigner',
                                                        'vic:LbIntervenant': 'Service de Prévision des Crues Loire-Allier-Cher-Indre'}}}

        # ====================================================================
        filename = os.path.join(self.dirname,
                                'vigicrues-1_info_30-5_202402140927.json')
        vigi = Vigicrues_Location(filename=filename)
        content = vigi.read()
        self.assertDictEqual(content, valid)
        # ====================================================================

    def test_read_geoinfo(self):
        """
        Test des méthodes de lecture - flux InfoVigiCru.geojson
        """
        valid = {
            "type": "FeatureCollection",
            "name": "InfoVigiCru",
            "bbox": [-4.1486, 42.4018, 8.2333, 50.8871],
            "features": [
                {"type": "Feature", "properties":
                    {"id": 796, "CdEntCru": "VT9", "TypEntCru": "8",
                     "LbEntCru": "Creuse médiane", "AcroEntCru": "VT9",
                     "CdDiEnt_1": "FRG",
                     "DhCEntCru": "2006/07/05 09:00:00.000",
                     "DhMEntCru": "2006/07/05 09:00:00.000",
                     "StEntCru": "Validé", "CdTCC": "TCC12", "cdensup_1": "31",
                     "TypEnSup_1": "5", "CdInt": "2116", "NivInfViCr": 1},
                 "geometry": {"type": "MultiLineString",
                              "coordinates": [
                                  [[1.613095000000876, 46.45409300000237],
                                   [1.612932999995557, 46.45490500000421],
                                   [1.612860999996313, 46.455313999999724],
                                   [1.61253200000463, 46.455534999996885],
                                   [0.829596999994649, 46.78484099999706],
                                   [0.829563000004122, 46.78489600000406]]]},
                 "id": None}],
            "RefInfoVigiCru": "13022024_16",
            "DtHrInfoVigiCru ": "2024-02-13T14:56:11+00:00"}
        # ====================================================================
        filename = os.path.join(self.dirname,
                                'vigicrues-1_geoinfo_202402140826.geojson')
        vigi = Vigicrues_Location(filename=filename)
        content = vigi.read()
        self.assertDictEqual(content, valid)
        # ====================================================================

    def test_read_domain(self):
        """
        Test des méthodes de lecture - flux TerEntVigiCru.jsonld
        """
        valid = {
            "@context": {
                "mat": "http://id.eaufrance.fr/ddd/mat/3.1/",
                "int": "http://id.eaufrance.fr/ddd/int/2/",
                "vic": "http://id.eaufrance.fr/ddd/vic/1.1/",
                "com": "http://id.eaufrance.fr/ddd/com/4/",
                "hyd": "http://id.eaufrance.fr/ddd/hyd/2.3/",
                "eth": "http://id.eaufrance.fr/ddd/eth/2/",
                "zon": "http://id.eaufrance.fr/ddd/zon/2.2/",
                "mdo": "http://id.eaufrance.fr/ddd/mdo/1.4/"
            },
            "mat:Scenario": {
                "Flux": {
                    "Version": "béta",
                    "DateRevision": "2019-09-16T11:30:00"
                },
                "mat:CodeScenario": "VICGeo",
                "mat:VersionScenario": "1beta",
                "mat:NomScenario": "Vigilance crues aux formats géo",
                "DateHeureCreationFichier": "2024-02-14T09:24:00+00:00",
                "Emetteur": {
                    "@id": "http://id.eaufrance.fr/int/1537",
                    "int:CdIntervenant": "1537"
                }
            },
            "count_TerEntVigiCru": 1,
            "vic:TerEntVigiCru": {
                "@id": "http://id.eaufrance.fr/TerEntVigiCru/30",
                "vic:CdEntVigiCru": "30",
                "vic:TypEntVigiCru": "5",
                "vic:LbEntVigiCru": "Loire-Allier-Cher-Indre",
                "vic:DtHrCreatEntVigiCru": "",
                "vic:DtHrMajEntVigiCru": "",
                "vic:StEntVigiCru": "Validé",
                "vic:administre": {
                    "@id": "http://id.eaufrance.fr/int/ARenseigner",
                    "int:CdIntervenant": ""
                },
                "vic:aPourTCC": {
                    "@id": "http://id.eaufrance.fr/TCC/ARenseigner",
                    "vic:CdTCC": ""
                },
                "vic:aPourDistrictHydro": {
                    "@id": "http://id.eaufrance.fr/mdo/ARenseigner",
                    "mdo:CdEuDistrict": ""
                },
                "count_aNMoinsUn": 41,
                "vic:aNMoinsUn": [
                    {
                        "@id": "http://id.eaufrance.fr/TronEntVigiCru/AL5",
                        "vic.CdEntVigiCru": "AL5",
                        "vic.TypEntVigiCru": "8",
                        "vic.LbEntVigiCru": "Allier entre Dore et Sioule",
                        "LinkEntity": "https://www.vigicrues.gouv.fr/services/1/TronEntVigiCru.jsonld?CdEntVigiCru=AL5&TypEntVigiCru=8",
                        "LinkInfoCru": "https://www.vigicrues.gouv.fr/services/1/InfoVigiCru.jsonld?CdEntVigiCru=AL5&TypEntVigiCru=8"
                    },
                    {
                        "@id": "http://id.eaufrance.fr/TronEntVigiCru/AL8",
                        "vic.CdEntVigiCru": "AL8",
                        "vic.TypEntVigiCru": "8",
                        "vic.LbEntVigiCru": "Allier à l'aval de la Sioule",
                        "LinkEntity": "https://www.vigicrues.gouv.fr/services/1/TronEntVigiCru.jsonld?CdEntVigiCru=AL8&TypEntVigiCru=8",
                        "LinkInfoCru": "https://www.vigicrues.gouv.fr/services/1/InfoVigiCru.jsonld?CdEntVigiCru=AL8&TypEntVigiCru=8"
                    },
                     {
                        "@id": "http://id.eaufrance.fr/TronEntVigiCru/LC130",
                        "vic.CdEntVigiCru": "LC130",
                        "vic.TypEntVigiCru": "8",
                        "vic.LbEntVigiCru": "Loire charollaise",
                        "LinkEntity": "https://www.vigicrues.gouv.fr/services/1/TronEntVigiCru.jsonld?CdEntVigiCru=LC130&TypEntVigiCru=8",
                        "LinkInfoCru": "https://www.vigicrues.gouv.fr/services/1/InfoVigiCru.jsonld?CdEntVigiCru=LC130&TypEntVigiCru=8"
                    },
                    {
                        "@id": "http://id.eaufrance.fr/TronEntVigiCru/LC140",
                        "vic.CdEntVigiCru": "LC140",
                        "vic.TypEntVigiCru": "8",
                        "vic.LbEntVigiCru": "Loire nivernaise",
                        "LinkEntity": "https://www.vigicrues.gouv.fr/services/1/TronEntVigiCru.jsonld?CdEntVigiCru=LC140&TypEntVigiCru=8",
                        "LinkInfoCru": "https://www.vigicrues.gouv.fr/services/1/InfoVigiCru.jsonld?CdEntVigiCru=LC140&TypEntVigiCru=8"
                    },
                    {
                        "@id": "http://id.eaufrance.fr/TronEntVigiCru/LC165",
                        "vic.CdEntVigiCru": "LC165",
                        "vic.TypEntVigiCru": "8",
                        "vic.LbEntVigiCru": "Loire giennoise",
                        "LinkEntity": "https://www.vigicrues.gouv.fr/services/1/TronEntVigiCru.jsonld?CdEntVigiCru=LC165&TypEntVigiCru=8",
                        "LinkInfoCru": "https://www.vigicrues.gouv.fr/services/1/InfoVigiCru.jsonld?CdEntVigiCru=LC165&TypEntVigiCru=8"
                    },
                    {
                        "@id": "http://id.eaufrance.fr/TronEntVigiCru/LC175",
                        "vic.CdEntVigiCru": "LC175",
                        "vic.TypEntVigiCru": "8",
                        "vic.LbEntVigiCru": "Loire orléanaise",
                        "LinkEntity": "https://www.vigicrues.gouv.fr/services/1/TronEntVigiCru.jsonld?CdEntVigiCru=LC175&TypEntVigiCru=8",
                        "LinkInfoCru": "https://www.vigicrues.gouv.fr/services/1/InfoVigiCru.jsonld?CdEntVigiCru=LC175&TypEntVigiCru=8"
                    },
                    {
                        "@id": "http://id.eaufrance.fr/TronEntVigiCru/LC185",
                        "vic.CdEntVigiCru": "LC185",
                        "vic.TypEntVigiCru": "8",
                        "vic.LbEntVigiCru": "Loire blaisoise",
                        "LinkEntity": "https://www.vigicrues.gouv.fr/services/1/TronEntVigiCru.jsonld?CdEntVigiCru=LC185&TypEntVigiCru=8",
                        "LinkInfoCru": "https://www.vigicrues.gouv.fr/services/1/InfoVigiCru.jsonld?CdEntVigiCru=LC185&TypEntVigiCru=8"
                    },
                    {
                        "@id": "http://id.eaufrance.fr/TronEntVigiCru/LC195",
                        "vic.CdEntVigiCru": "LC195",
                        "vic.TypEntVigiCru": "8",
                        "vic.LbEntVigiCru": "Loire tourangelle",
                        "LinkEntity": "https://www.vigicrues.gouv.fr/services/1/TronEntVigiCru.jsonld?CdEntVigiCru=LC195&TypEntVigiCru=8",
                        "LinkInfoCru": "https://www.vigicrues.gouv.fr/services/1/InfoVigiCru.jsonld?CdEntVigiCru=LC195&TypEntVigiCru=8"
                    }
                ],
                "LinkInfoCru": "https://www.vigicrues.gouv.fr/services/1/InfoVigiCru.jsonld?CdEntVigiCru=30&TypEntVigiCru=5"
            }
        }
        # ====================================================================
        filename = os.path.join(self.dirname,
                                'vigicrues-1_domain_30-5_202402140924.json')
        vigi = Vigicrues_Location(filename=filename)
        content = vigi.read()
        self.assertDictEqual(content, valid)
        # ====================================================================

    def test_read_reach(self):
        """
        Test des méthodes de lecture - flux TronEntVigiCru.jsonld
        """
        valid = {
            "@context": {
                "mat": "http://id.eaufrance.fr/ddd/mat/3.1/",
                "int": "http://id.eaufrance.fr/ddd/int/2/",
                "vic": "http://id.eaufrance.fr/ddd/vic/1.1/",
                "com": "http://id.eaufrance.fr/ddd/com/4/",
                "hyd": "http://id.eaufrance.fr/ddd/hyd/2.3/",
                "eth": "http://id.eaufrance.fr/ddd/eth/2/",
                "zon": "http://id.eaufrance.fr/ddd/zon/2.2/",
                "mdo": "http://id.eaufrance.fr/ddd/mdo/1.4/"
            },
            "mat:Scenario": {
                "Flux": {
                    "Version": "béta",
                    "DateRevision": "2019-09-16T11:30:00"
                },
                "mat:CodeScenario": "VICGeo",
                "mat:VersionScenario": "1beta",
                "mat:NomScenario": "Vigilance crues aux formats géo",
                "DateHeureCreationFichier": "2024-02-14T09:26:39+00:00",
                "Emetteur": {
                    "@id": "http://id.eaufrance.fr/int/1537",
                    "int:CdIntervenant": "1537"
                }
            },
            "count_TronEntVigiCru": 1,
            "vic:TronEntVigiCru": {
                "@id": "http://id.eaufrance.fr/TronEntVigiCru/LC165",
                "vic:CdEntVigiCru": "LC165",
                "vic:TypEntVigiCru": "8",
                "vic:LbEntVigiCru": "Loire giennoise",
                "vic:DtHrCreatEntVigiCru": "",
                "vic:DtHrMajEntVigiCru": "",
                "vic:StEntVigiCru": "Validé",
                "vic:administre": {
                    "@id": "http://id.eaufrance.fr/int/ARenseigner",
                    "int:CdIntervenant": ""
                },
                "vic:aPourTCC": {
                    "@id": "http://id.eaufrance.fr/TCC/ARenseigner",
                    "vic:CdTCC": ""
                },
                "vic:aPourDistrictHydro": {
                    "@id": "http://id.eaufrance.fr/mdo/ARenseigner",
                    "mdo:CdEuDistrict": ""
                },
                "count_aNPlusUn": 1,
                "vic:aNPlusUn": {
                    "@id": "http://id.eaufrance.fr/TerEntVigiCru/30",
                    "vic:CdEntVigiCru": "30",
                    "vic:TypEntVigiCru": "5",
                    "vic:LbEntVigiCru": "",
                    "LinkEntity": "https://www.vigicrues.gouv.fr/services/1/TerEntVigiCru.jsonld?CdEntVigiCru=30&TypEntVigiCru=",
                    "LinkInfoCru": "https://www.vigicrues.gouv.fr/services/1/InfoVigiCru.jsonld?CdEntVigiCru=30&TypEntVigiCru="
                },
                "count_aNMoinsUn": 9,
                "vic:aNMoinsUn": [
                    {
                        "@id": "http://id.eaufrance.fr/StaEntVigiCru/K400001010",
                        "vic:CdEntVigiCru": "K400001010",
                        "vic:TypEntVigiCru": "7",
                        "vic:LbEntVigiCru": "Cours-les-Barres [Givry]",
                        "LinkEntity": "https://www.vigicrues.gouv.fr/services/1/StaEntVigiCru.jsonld?CdEntVigiCru=K400001010&TypEntVigiCru=7"
                    },
                    {
                        "@id": "http://id.eaufrance.fr/StaEntVigiCru/K418001010",
                        "vic:CdEntVigiCru": "K418001010",
                        "vic:TypEntVigiCru": "7",
                        "vic:LbEntVigiCru": "Gien [Vieux Pont]",
                        "LinkEntity": "https://www.vigicrues.gouv.fr/services/1/StaEntVigiCru.jsonld?CdEntVigiCru=K418001010&TypEntVigiCru=7"
                    },
                    {
                        "@id": "http://id.eaufrance.fr/StaEntVigiCru/K412301001",
                        "vic:CdEntVigiCru": "K412301001",
                        "vic:TypEntVigiCru": "7",
                        "vic:LbEntVigiCru": "Arquian",
                        "LinkEntity": "https://www.vigicrues.gouv.fr/services/1/StaEntVigiCru.jsonld?CdEntVigiCru=K412301001&TypEntVigiCru=7"
                    }
                ],
                "LinkInfoCru": "https://www.vigicrues.gouv.fr/services/1/InfoVigiCru.jsonld?CdEntVigiCru=LC165&TypEntVigiCru=8"
            }
        }
        # ====================================================================
        filename = os.path.join(self.dirname,
                                'vigicrues-1_reach_LC165-8_202402140927.json')
        vigi = Vigicrues_Location(filename=filename)
        content = vigi.read()
        self.assertDictEqual(content, valid)
        # ====================================================================

    def test_read_loc(self):
        """
        Test des méthodes de lecture - flux StaEntVigiCru.jsonld
        """
        valid = {
            "@context": {
                "mat": "http://id.eaufrance.fr/ddd/mat/3.1/",
                "int": "http://id.eaufrance.fr/ddd/int/2/",
                "vic": "http://id.eaufrance.fr/ddd/vic/1.1/",
                "com": "http://id.eaufrance.fr/ddd/com/4/",
                "hyd": "http://id.eaufrance.fr/ddd/hyd/2.3/",
                "eth": "http://id.eaufrance.fr/ddd/eth/2/",
                "zon": "http://id.eaufrance.fr/ddd/zon/2.2/",
                "mdo": "http://id.eaufrance.fr/ddd/mdo/1.4/"
            },
            "mat:Scenario": {
                "Flux": {
                    "Version": "béta",
                    "DateRevision": "2019-09-16T11:30:00"
                },
                "mat:CodeScenario": "VICGeo",
                "mat:VersionScenario": "1beta",
                "mat:NomScenario": "Vigilance crues aux formats géo",
                "DateHeureCreationFichier": "2024-02-14T09:24:00+00:00",
                "Emetteur": {
                    "@id": "http://id.eaufrance.fr/int/1537",
                    "int:CdIntervenant": "1537"
                }
            },
            "vic:StaEntVigiCru": {
                "@id": "http://id.eaufrance.fr/StaEntVigiCru/K400001010",
                "vic:CdEntVigiCru": "K400001010",
                "vic:TypEntVigiCru": "7",
                "vic:LbEntVigiCru": "Cours-les-Barres [Givry]",
                "vic:DtHrCreatEntVigiCru": "",
                "vic:DtHrMajEntVigiCru": "",
                "vic:StEntVigiCru": "Validé",
                "vic:administre": {
                    "@id": "http://id.eaufrance.fr/int/ARenseigner",
                    "int:CdIntervenant": ""
                },
                "vic:aPourTCC": {
                    "@id": "http://id.eaufrance.fr/TCC/ARenseigner",
                    "vic:CdTCC": ""
                },
                "vic:aPourDistrictHydro": {
                    "@id": "http://id.eaufrance.fr/mdo/ARenseigner",
                    "mdo:CdEuDistrict": ""
                },
                "vic:correspondA": {
                    "hyd:CdStationHydro": "K400001010",
                    "hyd:CdStationHydroAncienRef": "0",
                    "hyd:LbStationHydro": "Cours-les-Barres [Givry]",
                    "hyd:TypStationHydro": "",
                    "hyd:estSitueeSur": {
                        "@id": "http://id.eaufrance.fr/com/18075",
                        "com:CdCommune": "18075"
                    },
                    "hyd:CoordXStationHydro": "705478",
                    "hyd:CoordYStationHydro": "6657310",
                    "hyd:ProjCoordStationHydro": ""
                },
                "vic:aNPlusUn": {
                    "@id": "http://id.eaufrance.fr/TronEntVigiCru/ARenseigner",
                    "vic:CdEntVigiCru": "",
                    "vic:TypEntVigiCru": "8",
                    "vic:LbEntVigiCru": "",
                    "LinkEntity": "",
                    "LinkInfoCru": "",
                    "vic:aNPlusUn": {
                        "@id": "http://id.eaufrance.fr/TerEntVigiCru/ARenseigner",
                        "vic:CdEntVigiCru": "",
                        "vic:TypEntVigiCru": "5",
                        "vic:LbEntVigiCru": "",
                        "LinkEntity": "",
                        "LinkInfoCru": ""
                    }
                },
                "VigilanceCrues": {
                    "StationPrevision": "t"
                },
                "LinkObsHaut": "https://www.vigicrues.gouv.fr/services/observations.json?CdStationHydro=K400001010&FormatDate=iso",
                "LinkObsDeb": "https://www.vigicrues.gouv.fr/services/observations.json?CdStationHydro=K400001010&FormatDate=iso&GrdSerie=Q",
                "LinkPrevHaut": "https://www.vigicrues.gouv.fr/services/previsions.json?CdEntVigiCru=K400001010&TypEntVigiCru=7&FormatDate=iso",
                "LinkPrevDeb": "https://www.vigicrues.gouv.fr/services/previsions.json?CdEntVigiCru=K400001010&TypEntVigiCru=7&FormatDate=iso&GrdSimul=Q"
            }
        }
        # ====================================================================
        filename = os.path.join(
            self.dirname,
            'vigicrues-1_loc_K400001010-7_202402140924.json')
        vigi = Vigicrues_Location(filename=filename)
        content = vigi.read()
        self.assertDictEqual(content, valid)
        # ====================================================================
