#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary csv2csv

To run all tests just type:
    python -m unittest test_bin_csv2csv

To run only a class test:
    python -m unittest test_bin_csv2csv.Test_csv2csv

To run only a specific test:
    python -m unittest test_bin_csv2csv.Test_csv2csv.test_ath

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import csv2csv as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_csv2csv(unittest.TestCase):
    """
    csv2csv bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'csv2csv')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_reservoir = os.path.join('data', 'core', 'reservoir')
        cls.dir_grp16 = os.path.join('data', 'model', 'grp16', 'cal')
        cls.dir_grp18 = os.path.join('data', 'model', 'grp18', 'cal')
        cls.dir_dbase = os.path.join('data', 'io', 'dbase')
        cls.dir_pyspc = os.path.join('data', 'io', 'pyspcfile')
        cls.dir_cat = os.path.join(cls.dir_in, 'cat')
        cls.dir_dsc = os.path.join(cls.dir_in, 'dsc')
        cls.dir_f2s = os.path.join(cls.dir_in, 'f2s')
        cls.dir_nsc = os.path.join(cls.dir_in, 'nsc')
        cls.dir_pct = os.path.join(cls.dir_in, 'pct')
        cls.dir_rtc = os.path.join(cls.dir_in, 'rtc')
        cls.dir_s2f = os.path.join(cls.dir_in, 's2f')
        cls.dir_shs = os.path.join(cls.dir_in, 'shs')
        cls.dir_usc = os.path.join(cls.dir_in, 'usc')
        cls.dir_wav = os.path.join(cls.dir_in, 'wav')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.stations_list_file = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)

    def test_ath(self):
        """
        Test above threshold
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_pyspc,
            '-O', self.dir_out,
            '-n', 'QH',
            '-s', 'K0000000',
            '-M', 'ath', '100'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0000000')
        self.assertEqual(args.processing_method, ['ath', '100'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format(args.station_name, args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Construction d'une série de valeurs > 100 à la station "\
            "{s}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_bth(self):
        """
        Test below threshold
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_pyspc,
            '-O', self.dir_out,
            '-n', 'QH',
            '-s', 'K0000000',
            '-M', 'bth', '100'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0000000')
        self.assertEqual(args.processing_method, ['bth', '100'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format(args.station_name, args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Construction d'une série de valeurs < 100 à la station "\
            "{s}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_bwd(self):
        """
        Test between dates
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_pyspc,
            '-O', self.dir_out,
            '-n', 'QH',
            '-s', 'K0000000',
            '-M', 'bwd', '2014110406', '2014110410'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0000000')
        self.assertEqual(args.processing_method,
                         ['bwd', '2014110406', '2014110410'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format(args.station_name, args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Construction d'une série entre 2 dates à la station "\
            "{s}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_cat(self):
        """
        Test Concaténation de séries
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_cat,
                                               'K000000_concat.txt')
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_cat,
            '-O', self.dir_out,
            '-n', 'QH',
            '-l',  self.stations_list_file,
            '-M', 'cat'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_cat)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.processing_method, ['cat'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format('K000000_concat', args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Concaténer les séries listées dans {l} en une seule. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') aux stations listées dans {l} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                l=args.stations_list_file, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_ccd(self):
        """
        Test change code serie
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_pyspc,
            '-O', self.dir_out,
            '-n', 'QH',
            '-s', 'K0000000',
            '-M', 'ccd', 'K1234567'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0000000')
        self.assertEqual(args.processing_method, ['ccd', 'K1234567'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format('K1234567', args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Changement de code d'une série de données. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_cpy(self):
        """
        Test duplication année
        """
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_dsc,
            '-O', self.dir_out,
            '-n', 'PJ',
            '-s', '43130002',
            '-M', 'cpy', '43130002', '2014', '20151103', '20151110'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_dsc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'PJ')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, '43130002')
        self.assertEqual(args.processing_method, ['cpy', '43130002', '2014',
                                                  '20151103', '20151110'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format(args.station_name, args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Compléter des données par un bloc annuel d'une autre "\
            "série. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_etp(self):
        """
        Test ETP Oudin
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_usc,
            '-O', self.dir_out,
            '-n', 'TH',
            '-s', '43130002',
            '-M', 'etp', 'hourly', '44.9',
            '-C', 'grp16', 'grp16'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_usc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'TH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, '43130002')
        self.assertEqual(args.processing_method, ['etp', 'hourly', '44.9'])
        self.assertEqual(args.csv_type, ['grp16', 'grp16'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format(args.station_name, 'EH')
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Construction d'une série d'ETP horaire à la station "\
            "{s}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_f2s(self):
        """
        Test forecast converter to sim
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_f2s, 'grp.txt')
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_f2s,
            '-O', self.dir_out,
            '-n', 'QH',
            '-l', self.stations_list_file,
            '-M', 'f2s', '6', '24', '48',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]  # cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_f2s)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.processing_method, ['f2s', '6', '24', '48'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        self.assertEqual(args.onefile, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'K1251810_{}.txt'.format(args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Création de simulations à partir de prévisions "\
            "listées dans {l}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') des séries listées dans {l} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                l=args.stations_list_file, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_fct(self):
        """
        Test filling by constant
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', os.path.join(self.dir_ctl, 'ath'),
            '-O', self.dir_out,
            '-n', 'QH',
            '-s', 'K0000000',
            '-M', 'fct', '0'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, os.path.join(self.dir_ctl, 'ath'))
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0000000')
        self.assertEqual(args.processing_method, ['fct', '0'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format(args.station_name, args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Remplacement des valeurs manquantes par une constante "\
            "à la station {s}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_fli(self):
        """
        Test linear filling
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', os.path.join(self.dir_ctl, 'bth'),
            '-O', self.dir_out,
            '-n', 'QH',
            '-s', 'K0000000',
            '-M', 'fli',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, os.path.join(self.dir_ctl, 'bth'))
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0000000')
        self.assertEqual(args.processing_method, ['fli'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format(args.station_name, args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Remplacement des valeurs manquantes par interpolation "\
            "à la station {s}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_lrg(self):
        """
        Test linear reg
        """
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_pyspc,
            '-O', self.dir_out,
            '-n', 'QH',
            '-s', 'K0000000',
            '-M', 'lrg', '2', '10', '6'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0000000')
        self.assertEqual(args.processing_method, ['lrg', '2', '10', '6'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format(args.station_name, args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Construction d'une série de valeurs > 100 à la station "\
            "{s}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_pct(self):
        """
        Test du calcul des percentiles
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_pct, 'pearp.txt')
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_pct,
            '-O', self.dir_out,
            '-n', 'QH',
            '-l', self.stations_list_file,
            '-M', 'pct', '10-25-50-75-90',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]  # cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_pct)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.processing_method, ['pct', '10-25-50-75-90'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        self.assertEqual(args.onefile, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'K6373020_2016053006_pearp_percentile_{}.txt'.format(
            args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Calcul des percentiles (fréquences: {M2}) "\
            "à partir des séries listées dans {l}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') aux stations listées dans {l} "\
            "vers le format '{C2}' dans le répertoire {O}. "\
            "Un seul fichier est créé"\
            "".format(
                I=args.input_dir,
                l=args.stations_list_file, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_res(self):
        """
        Test reservoir
        """
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_reservoir,
            '-O', self.dir_out,
            '-n', 'HH',
            '-s', 'K001002010',
            '-M', 'res', os.path.join(
                self.dir_reservoir, 'reservoir.txt'), 'llp', 'V:V,Q:Qd'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_reservoir)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'HH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K001002010')
        self.assertEqual(args.processing_method,
                         ['res',
                          os.path.join(self.dir_reservoir, 'reservoir.txt'),
                          'llp', 'V:V,Q:Qd'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for v in ['QH', 'VH', 'ZH']:
            filename = '{}_{}.txt'.format(args.station_name, v)
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dir_ctl, args.processing_method[0],
                             filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Construction de séries à la station {s} "\
            "à partir d'un barème de réservoir. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_rtc_bareme(self):
        """
        Test apply_RatingCurves - BAREME
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_rtc,
            '-O', self.dir_out,
            '-n', 'QH',
            '-s', 'K0100020',
            '-M', 'rtc', os.path.join(self.dir_dbase, 'bareme.mdb')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_rtc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0100020')
        self.assertEqual(args.processing_method,
                         ['rtc', os.path.join(self.dir_dbase, 'bareme.mdb')])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=subprocess.DEVNULL, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_Bareme_{}.txt'.format(args.station_name, 'HH')
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Construction de séries à la station {s} "\
            "à partir d'un barème d'une base Bareme. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_rtc_bareme_code(self):
        """
        Test apply_RatingCurves - BAREME
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_rtc,
            '-O', self.dir_out,
            '-n', 'QH',
            '-s', 'K0100020',
            '-M', 'rtc', os.path.join(self.dir_dbase, 'bareme.mdb'), 'H200809'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_rtc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0100020')
        self.assertEqual(args.processing_method,
                         ['rtc',
                          os.path.join(self.dir_dbase, 'bareme.mdb'),
                          'H200809'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=subprocess.DEVNULL, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_Bareme-{}_{}.txt'.format(
            args.station_name, args.processing_method[-1], 'HH')
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Construction de séries à la station {s} "\
            "à partir d'un bareme particulier d'une base Bareme. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_rtc_phyc(self):
        """
        Test apply_RatingCurves - PHyC
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_rtc,
            '-O', self.dir_out,
            '-n', 'QH',
            '-s', 'K0550010',
            '-M', 'rtc', os.path.join(self.dir_rtc, 'RatingCurves.xml')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_rtc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0550010')
        self.assertEqual(args.processing_method,
                         ['rtc',
                          os.path.join(self.dir_rtc, 'RatingCurves.xml')])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=subprocess.DEVNULL, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_Sandre_{}.txt'.format(args.station_name, 'HH')
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Construction de séries à la station {s} "\
            "à partir d'un barème d'un fichier XML Sandre. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_rtc_phyc_levelcor(self):
        """
        Test apply_RatingCurves - PHyC - Avec Courbes de correction
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_rtc,
            '-O', self.dir_out,
            '-n', 'QH',
            '-s', 'K0550010',
            '-M', 'rtc', os.path.join(self.dir_rtc, 'RatingCurves.xml'),
            os.path.join(self.dir_rtc, 'levelcor.xml'),
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_rtc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0550010')
        self.assertEqual(args.processing_method,
                         ['rtc',
                          os.path.join(self.dir_rtc, 'RatingCurves.xml'),
                          os.path.join(self.dir_rtc, 'levelcor.xml')])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=subprocess.DEVNULL, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_Sandre_{}.txt'.format(args.station_name, 'HH')
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Construction de séries à la station {s} "\
            "à partir d'un barème et d'une courbe de correction de fichiers. "\
            "XML Sandre. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_s2f(self):
        """
        Test sim to forecast
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_s2f, 'mohys.txt')
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_s2f,
            '-O', self.dir_out,
            '-n', 'QH',
            '-l', self.stations_list_file,
            '-M', 's2f', '36', '48', '2018010412', '2018010412',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]  # cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_s2f)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.processing_method,
                         ['s2f', '36', '48', '2018010412', '2018010412'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'K1321810_2018010412_mohys_036_{}.txt'.format(args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Création de prévisions à partir de simulations et "\
            "observations listées dans {l}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') des séries listées dans {l} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                l=args.stations_list_file, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_tlg(self):
        """
        Test timelag
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_pyspc,
            '-O', self.dir_out,
            '-n', 'QH',
            '-s', 'K0000000',
            '-M', 'tlg', '" -2"'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]  # cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0000000')
        self.assertEqual(args.processing_method, ['tlg', '" -2"'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format(args.station_name, args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Décalage temporel (-2h) d'une série à la station "\
            "{s}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_wav(self):
        """
        Test weighted average
        """
        # =====================================================================
        cases = ['HteLoire.txt', 'HteLoireN.txt']
        # =====================================================================
        for case in cases:
            self.stations_list_file = os.path.join(self.dir_wav, case)
            processArgs = [
                'python',
                '../bin/csv2csv.py',
                '-I', self.dir_wav,
                '-O', self.dir_out,
                '-n', 'PH',
                '-l', self.stations_list_file,
                '-M', 'wav', os.path.join(self.dir_wav, 'ponderation.txt'),
                '-C', 'grp16', 'pyspc'
            ]
            # =================================================================
            cline = ' '.join(processArgs).replace('python', '').strip()
            sys.argv = cline.split(' ')
            with captured_output():
                args = _args.csv2csv()
            self.assertEqual(args.input_dir, self.dir_wav)
            self.assertEqual(args.stations_list_file, self.stations_list_file)
            self.assertEqual(args.varname, 'PH')
            self.assertEqual(args.output_dir, self.dir_out)
            self.assertEqual(args.station_name, None)
            self.assertEqual(
                args.processing_method,
                ['wav', os.path.join(self.dir_wav, 'ponderation.txt')])
            self.assertEqual(args.csv_type, ['grp16', 'pyspc'])
            # =================================================================
            processRun = subprocess.Popen(
                processArgs, universal_newlines=True,
                shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
            processRun.wait()
            # =================================================================
            filename = '{}-ponderation_{}.txt'.format(
                case.replace('.txt', ''), args.varname)
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dir_ctl, args.processing_method[0],
                             filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
            # =================================================================
    #        print(cline)
            self.cline = cline
            self.label = "Construction d'une série à partir de la somme "\
                "pondérée de plusieurs séries listées dans {l}. "\
                "Application de la méthode {M1} "\
                "avec les éléments utilisateurs {M2} "\
                "à partir de données au format '{C1}' dans le répertoire {I} "\
                "de la grandeur ('{n}') aux stations listées dans {l} "\
                "vers le format '{C2}' dans le répertoire {O}"\
                ""\
                "".format(
                    I=args.input_dir,
                    l=args.stations_list_file, n=args.varname,
                    O=args.output_dir,
                    M1=args.processing_method[0],
                    M2=args.processing_method[1:],
                    C1=args.csv_type[0],
                    C2=args.csv_type[-1])
            # =================================================================

    def test_dsc(self):
        """
        Test downscale
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_dsc,
            '-O', self.dir_out,
            '-n', 'PJ',
            '-s', '43130002',
            '-M', 'dsc', 'PH', '66'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_dsc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'PJ')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, '43130002')
        self.assertEqual(args.processing_method, ['dsc', 'PH', '66'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format(args.station_name, 'PH')
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Construction d'une série horaire à partir de données "\
            "journalières à la station {s}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_nsc(self):
        """
        Test nearlyequalscale
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_nsc,
            '-O', self.dir_out,
            '-n', 'P5m',
            '-s', 'K0109910',
            '-M', 'nsc', 'P6m', 'o'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_nsc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'P5m')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0109910')
        self.assertEqual(args.processing_method, ['nsc', 'P6m', 'o'])
        self.assertEqual(args.csv_type, ['pyspc', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format(args.station_name, 'P6m')
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Construction d'une série 6-min à partir de données "\
            "5-min à la station {s}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_rsc(self):
        """
        Test regularscale
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_grp18,
            '-O', self.dir_out,
            '-n', 'QI',
            '-s', 'RH10585x',
            '-M', 'rsc',
            '-C', 'grp18', 'grp16'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_grp18)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'QI')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'RH10585x')
        self.assertEqual(args.processing_method, ['rsc'])
        self.assertEqual(args.csv_type, ['grp18', 'grp16'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format(args.station_name, 'Q')
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Construction d'une série horaire à partir de données "\
            "instantanées à la station {s}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================

    def test_shs(self):
        """
        Test subhourlyscale
        """
        # =====================================================================
        cases = {
            'bfill': 'K040301b',
            'ffill': 'K040301f',
            'nearest': 'K040301n',
            'interpolate': 'K040301i',
        }
        for km, ks in cases.items():
            # =================================================================
            processArgs = [
                'python',
                '../bin/csv2csv.py',
                '-I', self.dir_shs,
                '-O', self.dir_out,
                '-n', 'QH',
                '-s', ks,
                '-M', 'shs', '900', km,
                '-C', 'grp16', 'pyspc'
            ]
            # =================================================================
            cline = ' '.join(processArgs).replace('python', '').strip()
            sys.argv = cline.split(' ')
            with captured_output():
                args = _args.csv2csv()
            self.assertEqual(args.input_dir, self.dir_shs)
            self.assertEqual(args.stations_list_file, None)
            self.assertEqual(args.varname, 'QH')
            self.assertEqual(args.output_dir, self.dir_out)
            self.assertEqual(args.station_name, ks)
            self.assertEqual(args.processing_method, ['shs', '900', km])
            self.assertEqual(args.csv_type, ['grp16', 'pyspc'])
            # =================================================================
#            print(' '.join(processArgs))
            processRun = subprocess.Popen(
                processArgs, universal_newlines=True,
                shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
            processRun.wait()
            # =================================================================
            filename = '{}_{}.txt'.format(args.station_name, 'QI')
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dir_ctl, args.processing_method[0],
                             filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
            # =================================================================
    #        print(cline)
            self.cline = cline
            self.label = "Construction d'une série infra-horaire à partir de "\
                "données horaires selon l'approche {km} à la station {s}. "\
                "Application de la méthode {M1} "\
                "avec les éléments utilisateurs {M2} "\
                "à partir de données au format '{C1}' dans le répertoire {I} "\
                "de la grandeur ('{n}') à la station {s} "\
                "vers le format '{C2}' dans le répertoire {O}"\
                ""\
                "".format(
                    km=km,
                    I=args.input_dir,
                    s=args.station_name, n=args.varname,
                    O=args.output_dir,
                    M1=args.processing_method[0],
                    M2=args.processing_method[1:],
                    C1=args.csv_type[0],
                    C2=args.csv_type[-1])
            # =================================================================

    def test_usc(self):
        """
        Test upscale
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2csv.py',
            '-I', self.dir_usc,
            '-O', self.dir_out,
            '-n', 'TH',
            '-s', '43130002',
            '-M', 'usc', 'TJ', 'o', '00',
            '-C', 'grp16', 'pyspc',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2csv()
        self.assertEqual(args.input_dir, self.dir_usc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'TH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, '43130002')
        self.assertEqual(args.processing_method, ['usc', 'TJ', 'o', '00'])
        self.assertEqual(args.csv_type, ['grp16', 'pyspc'])
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.txt'.format(args.station_name, 'TJ')
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, args.processing_method[0], filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Construction d'une série journalière à partir de "\
            "données horaires à la station {s}. "\
            "Application de la méthode {M1} "\
            "avec les éléments utilisateurs {M2} "\
            "à partir de données au format '{C1}' dans le répertoire {I} "\
            "de la grandeur ('{n}') à la station {s} "\
            "vers le format '{C2}' dans le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir,
                s=args.station_name, n=args.varname,
                O=args.output_dir,
                M1=args.processing_method[0],
                M2=args.processing_method[1:],
                C1=args.csv_type[0],
                C2=args.csv_type[-1])
        # =====================================================================
