#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for PyspcFile in pyspc.core.csvfile

To run all tests just type:
    python -m unittest test_core_PyspcFile

To run only a class test:
    python -m unittest test_core_PyspcFile.TestPyspcFile

To run only a specific test:
    python -m unittest test_core_PyspcFile.TestPyspcFile.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import filecmp
import numpy as np
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.core.pyspcfile import PyspcFile


class TestPyspcFile(unittest.TestCase):
    """
    PyspcFile class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'core', 'csv')
        self.valid_P6m = pnd.DataFrame(  # 43111002_P6m.txt
            {'43111002_P6m': [
                0.0, 1.0, 0.2, 0.4, 0.4, 0.4, 2.2, 11.7, 13.3, 8.2, 6.3, 13.4,
                14.2, 12.9, 21.5, 14.1, 17.0, 17.0, 8.0, 2.8, 1.8, 6.1, 11.2,
                6.6, 3.5, 1.6, 0.4, 0.6, 5.0, 1.8]},
            index=pnd.date_range(
                dt(2017, 6, 13, 17, 0),
                dt(2017, 6, 13, 19, 54),
                freq=td(seconds=6*60)
            )
        )
        self.valid_P6m.index.name = 'AAAAMMJJHHMM'
        self.valid_QI = pnd.DataFrame(  # K1341810_QI.txt
            {'K1341810_QI': [87.4, 90.6, 107.0, 117.0, 121.0, 124.0, 139.0,
                             146.0, 151.0, 151.0, 160.0, 170.0, 205.0, 213.0,
                             217.0, 219.0, 220.0, 219.0, 220.0, 220.0, 221.0,
                             221.0, 219.0, 220.0, 219.0, 243.0, 244.0, 244.0,
                             238.0, 236.0]},
            index=[dt(2020, 3, 5, 12, 0), dt(2020, 3, 5, 13, 0),
                   dt(2020, 3, 5, 16, 0), dt(2020, 3, 5, 17, 35),
                   dt(2020, 3, 5, 18, 20), dt(2020, 3, 5, 18, 50),
                   dt(2020, 3, 5, 21, 55), dt(2020, 3, 5, 23, 25),
                   dt(2020, 3, 6, 0, 10), dt(2020, 3, 6, 0, 22),
                   dt(2020, 3, 6, 1, 45), dt(2020, 3, 6, 3, 20),
                   dt(2020, 3, 6, 10, 30), dt(2020, 3, 6, 13, 0),
                   dt(2020, 3, 6, 14, 5), dt(2020, 3, 6, 15, 20),
                   dt(2020, 3, 6, 16, 0), dt(2020, 3, 6, 16, 20),
                   dt(2020, 3, 6, 16, 50), dt(2020, 3, 6, 17, 45),
                   dt(2020, 3, 6, 18, 25), dt(2020, 3, 6, 19, 10),
                   dt(2020, 3, 6, 22, 0), dt(2020, 3, 6, 22, 15),
                   dt(2020, 3, 6, 23, 35), dt(2020, 3, 7, 6, 15),
                   dt(2020, 3, 7, 7, 10), dt(2020, 3, 7, 8, 5),
                   dt(2020, 3, 7, 11, 20), dt(2020, 3, 7, 12, 0)]
        )
        self.valid_QI.index.name = 'AAAAMMJJHHMM'
        self.valid_QH = pnd.DataFrame(  # K1321810_mohys_QH.txt
            {'K1321810_mohys_QH': [
                118.9, 120.3, 121.4, 122.2, 122.4, 122.2, 121.6, 120.7, 119.7,
                118.8, 118.3, 118.8, 120.5, 123.9, 129.5, 137.2, 147.3, 159.8,
                172.7, 187.1, 200.7, 213.4, 224.8, 235.8, 245.6, 254.7, 266.9,
                273.6, 277.8, 280.1, 280.9, 280.7, 280.2, 280.0, 279.9, 280.1,
                280.4, 280.7, 280.9, 280.9, 280.6, 279.9, 278.7, 277.1, 275.5,
                273.9, 272.4, 271.0, 269.8]},
            index=pnd.date_range(
                dt(2018, 1, 3, 12),
                dt(2018, 1, 5, 12),
                freq='H'
            )
        )
        self.valid_QH.index.name = 'AAAAMMJJHH'
        self.valid_PJ = pnd.DataFrame(  # 41005_202006120526_BP_PJ.txt
            {'MoyInf': [100.0, 7.0, 0.0],
             'MoySup': [130.0, 15.0, 0.0],
             'LocInf': [200.0, np.nan, np.nan],
             'LocSup': [200.0, np.nan, np.nan],
             },
            index=pnd.date_range(
                dt(2020, 6, 12),
                dt(2020, 6, 14),
                freq='D'
            )
        )
        self.valid_PJ.index.name = 'AAAAMMJJ'
        self.valid_QM = pnd.DataFrame(  # K1341810_QM.txt
            {'K1341810_QM': [1.4, 2.1, 1.4, 5.5, 53.6, 156.0, 59.8, 60.9, 27.8,
                             9.5, 11.1, 2.6, 1.1]},
            index=pnd.date_range(
                dt(2017, 8, 1),
                dt(2018, 8, 1),
                freq='MS'
            )
        )
        self.valid_QM.index.name = 'AAAAMM'

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K1321810_QH.txt')
        reader = PyspcFile(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.station, 'K1321810')
        self.assertEqual(reader.varname, 'QH')
        self.assertEqual(reader.tdelta, td(hours=1))
        self.assertEqual(reader.simulation, None)
        self.assertEqual(reader.runtime, None)
        self.assertEqual(reader.model, None)
        self.assertEqual(reader.scen, None)
        self.assertEqual(reader.uncert, None)
        # =====================================================================
        filename = os.path.join(self.dirname, 'K1321810_mohys_QH.txt')
        reader = PyspcFile(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.station, 'K1321810')
        self.assertEqual(reader.varname, 'QH')
        self.assertEqual(reader.tdelta, td(hours=1))
        self.assertEqual(reader.simulation, 'mohys')
        self.assertEqual(reader.runtime, None)
        self.assertEqual(reader.model, None)
        self.assertEqual(reader.scen, None)
        self.assertEqual(reader.uncert, None)
        # =====================================================================
        filename = os.path.join(self.dirname,
                                'K0253030_2020061200_GR6J_QJ.txt')
        reader = PyspcFile(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.station, 'K0253030')
        self.assertEqual(reader.varname, 'QJ')
        self.assertEqual(reader.tdelta, td(days=1))
        self.assertEqual(reader.simulation, None)
        self.assertEqual(reader.runtime, dt(2020, 6, 12))
        self.assertEqual(reader.model, 'GR6J')
        self.assertEqual(reader.scen, None)
        self.assertEqual(reader.uncert, None)
        # =====================================================================

    def test_basename(self):
        """
        Test de la récupération d'information à partir du nom du fichier
        """
        valid = {
            'K1321810_QH.txt':
                ('K1321810', 'QH', None, None, None, None, None),
            'K1321810_mohys_QH.txt':
                ('K1321810', 'QH', 'mohys', None, None, None, None),
            'K0253030_2020061200_GR6J_QJ.txt':
                ('K0253030', 'QJ', None, dt(2020, 6, 12), 'GR6J', None, None),
            'K1251810_2018010412_2001_brut_QH.txt':
                ('K1251810', 'QH', None, dt(2018, 1, 4, 12),
                 '2001', 'brut', None),
            'K1251810_2018010412_2001_brut_50_QH.txt':
                ('K1251810', 'QH', None, dt(2018, 1, 4, 12),
                 '2001', 'brut', '50'),
        }
        for k, v in valid.items():
            self.assertEqual(PyspcFile.split_basename(k), v)
            self.assertEqual(PyspcFile.join_basename(*v), k)

    def test_read_minutes(self):
        """
        Test de la lecture d'un fichier en minutes
        """
        # =====================================================================
        filename = os.path.join(self.dirname, '43111002_P6m.txt')
        reader = PyspcFile(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_P6m, df)
        # =====================================================================
        filename = os.path.join(self.dirname, 'K1341810_QI.txt')
        reader = PyspcFile(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_QI, df)
        # =====================================================================

    def test_read_hours(self):
        """
        Test de la lecture d'un fichier en heures
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K1321810_mohys_QH.txt')
        reader = PyspcFile(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_QH, df)
        # =====================================================================

    def test_read_days(self):
        """
        Test de la lecture d'un fichier en jours
        """
        # =====================================================================
        filename = os.path.join(self.dirname, '41005_202006120526_BP_PJ.txt')
        reader = PyspcFile(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_PJ, df)
        # =====================================================================

    def test_read_months(self):
        """
        Test de la lecture d'un fichier en mois
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K1341810_QM.txt')
        reader = PyspcFile(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_QM, df)
        # =====================================================================

    def test_write_minutes(self):
        """
        Test de l'écriture d'un fichier en minutes
        """
        # =====================================================================
        filename = os.path.join(self.dirname, '43111002_P6m.txt')
        tmpfile = os.path.join('data', os.path.basename(filename))
        writer = PyspcFile(filename=tmpfile)
        writer.write(data=self.valid_P6m)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        filename = os.path.join(self.dirname, 'K1341810_QI.txt')
        tmpfile = os.path.join('data', os.path.basename(filename))
        writer = PyspcFile(filename=tmpfile)
        writer.write(data=self.valid_QI)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_write_hours(self):
        """
        Test de l'écriture d'un fichier en heures
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K1321810_mohys_QH.txt')
        tmpfile = os.path.join('data', os.path.basename(filename))
        writer = PyspcFile(filename=tmpfile)
        writer.write(data=self.valid_QH)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_write_days(self):
        """
        Test de l'écriture d'un fichier en jours
        """
        # =====================================================================
        filename = os.path.join(self.dirname, '41005_202006120526_BP_PJ.txt')
        tmpfile = os.path.join('data', os.path.basename(filename))
        writer = PyspcFile(filename=tmpfile)
        writer.write(data=self.valid_PJ)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_write_months(self):
        """
        Test de l'écriture d'un fichier en minutes
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K1341810_QM.txt')
        tmpfile = os.path.join('data', os.path.basename(filename))
        writer = PyspcFile(filename=tmpfile)
        writer.write(data=self.valid_QM)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_varname(self):
        """
        Test de la correspondance des variables selon GRP et selon SPC_LCI
        """
        valid = ['EH', 'EJ',
                 'HH', 'HI',
                 'HU2J',
                 'P15m', 'P3H', 'P5m', 'P6m', 'PH', 'PJ', 'PM',
                 'QH', 'QI', 'QJ', 'QM',
                 'TH', 'TI', 'TJ',
                 'VH', 'VI',
                 'ZH', 'ZI']
        self.assertEqual(PyspcFile.get_varnames(), valid)
