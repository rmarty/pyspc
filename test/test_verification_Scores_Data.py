#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Data in pyspc.verification.scores

To run all tests just type:
    python -m unittest test_verification_Scores_Data

To run only a class test:
    python -m unittest test_verification_Scores_Data.TestScores_Data

To run only a specific test:
    python -m unittest test_verification_Scores_Data.TestScores_Data.test_init

"""
# Imports
from datetime import datetime as dt
import filecmp
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.verification.scores import Data


class TestScores_Data(unittest.TestCase):
    """
    Scores_Data class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        # =====================================================================
        self.dirname = os.path.join('data', 'verification', 'scores')
        # =====================================================================
        self.valid_obs = pnd.DataFrame({
            ('K6373020', 'Q', 'obs'): [79.30, 85.70, 89.60, 91.80, 93.20,
                                       94.80, 96.00, 96.40, 96.40, 95.60,
                                       94.80, 94.00, 93.20, 92.40, 91.60,
                                       90.80, 90.00, 88.60, 87.30]},
            index=pnd.date_range(
                dt(2016, 5, 31, 12),
                dt(2016, 6, 1, 6),
                freq='H'
            )
        )
        self.valid_obs.columns.set_names(
            ['Stations', 'Grandeurs', 'IdSeries'],
            level=[0, 1, 2],
            inplace=True
        )
        # =====================================================================
        self.valid_sim = pnd.DataFrame({
            ('K6373020', 'Q', 'sim'): [71.02, 73.67, 76.08, 78.20, 80.04,
                                       81.72, 83.16, 84.08, 84.69, 84.94,
                                       84.75, 84.10, 83.06, 81.82, 80.44,
                                       78.95, 77.34, 75.61, 73.79]},
            index=pnd.date_range(
                dt(2016, 5, 31, 12),
                dt(2016, 6, 1, 6),
                freq='H'
            )
        )
        self.valid_sim.columns.set_names(
            ['Stations', 'Grandeurs', 'IdSeries'],
            level=[0, 1, 2],
            inplace=True
        )
        # =====================================================================
        key = ('45hEAOtt00', '2007', '31-05-2016 12:00', '50',
               'K6373020', 'Q', '45hEAOtt00_2007_50')
        self.valid_fcst = pnd.DataFrame({
            key: [71.02, 73.67, 76.08, 78.20, 80.04, 81.72, 83.16, 84.08,
                  84.69, 84.94, 84.75, 84.10, 83.06, 81.82, 80.44, 78.95,
                  77.34, 75.61, 73.79]},
            index=pnd.date_range(
                dt(2016, 5, 31, 12),
                dt(2016, 6, 1, 6),
                freq='H'
            )
        )
        self.valid_fcst.columns.set_names(
            ['# Modeles', '# Scenarios', '# DtDerObs', '# Probas',
             'Stations', 'Grandeurs', 'IdSeries'],
            level=[0, 1, 2, 3, 4, 5, 6],
            inplace=True
        )
        # =====================================================================

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'K6373020_Q.csv')
        datatype = 'obs'
        reader = Data(
            filename=filename,
            datatype=datatype
        )
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.datatype, datatype)

    def test_read_obs(self):
        """
        Test de la lecture - CAS OBSERVATION
        """
        filename = os.path.join(self.dirname, 'K6373020_Q.csv')
        datatype = 'obs'
        reader = Data(
            filename=filename,
            datatype=datatype
        )
        df = reader.read()
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(self.valid_obs, df)

    def test_read_sim(self):
        """
        Test de la lecture - CAS SIMULATION
        """
        filename = os.path.join(self.dirname, 'K6373020_Q_sim.csv')
        datatype = 'sim'
        reader = Data(
            filename=filename,
            datatype=datatype
        )
        df = reader.read()
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(self.valid_sim, df)

    def test_read_fcst(self):
        """
        Test de la lecture - CAS PREVISION SCORES
        """
        filename = os.path.join(self.dirname, 'K6373020_Q_9.csv')
        datatype = 'fcst'
        reader = Data(
            filename=filename,
            datatype=datatype
        )
        df = reader.read()
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(self.valid_fcst, df)

    def test_read_error(self):
        """
        Test de la lecture - CAS avec ERREURS
        """
#        filename0 = os.path.join(self.dirname, 'K6373020_Q_error_data.csv')
        filename1 = os.path.join(self.dirname, 'K6373020_Q_error_meta1.csv')
        filename2 = os.path.join(self.dirname, 'K6373020_Q_error_meta2.csv')
        datatype = 'obs'
#        reader0 = Data(
#            filename=filename0,
#            datatype=datatype
#        )
        reader1 = Data(
            filename=filename1,
            datatype=datatype
        )
        reader2 = Data(
            filename=filename2,
            datatype=datatype
        )
#        with self.assertRaises(ValueError):
#            reader0.read()
        with self.assertRaises(ValueError):
            reader1.read()
        with self.assertRaises(ValueError):
            reader2.read()

    def test_write_obs(self):
        """
        Test de la lecture - CAS OBSERVATION
        """
        df = self.valid_obs
        filename = os.path.join(self.dirname, 'K6373020_Q.csv')
        datatype = 'obs'
        tmp_file = os.path.join('data', 'K6373020_Q.csv')
        writer = Data(
            filename=tmp_file,
            datatype=datatype
        )
        writer.write(data=df)
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)

    def test_write_sim(self):
        """
        Test de la lecture - CAS SIMULATION
        """
        df = self.valid_sim
        filename = os.path.join(self.dirname, 'K6373020_Q_sim.csv')
        datatype = 'sim'
        tmp_file = os.path.join('data', 'K6373020_Q_sim.csv')
        writer = Data(
            filename=tmp_file,
            datatype=datatype
        )
        writer.write(data=df)
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)

    def test_write_fcst(self):
        """
        Test de la lecture - CAS PREVISION SCORES
        """
        df = self.valid_fcst
        filename = os.path.join(self.dirname, 'K6373020_Q_9.csv')
        datatype = 'fcst'
        tmp_file = os.path.join('data', 'K6373020_Q_9.csv')
        writer = Data(
            filename=tmp_file,
            datatype=datatype
        )
        writer.write(data=df)
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)

    def test_datatypes(self):
        """
        Test des types de fichiers PRV SCORES traités par <pyspc>
        """
        valid = ['obs', 'sim', 'fcst']
        self.assertEqual(valid, Data.get_types())
