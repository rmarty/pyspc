44 Manning Nomenclature
1 0.03 Continuous urban fabric
2 0.03 Discontinuous urban fabric
3 0.03 Industrial or commercial units
4 0.03 Road and rail networks and associated land
5 0.03 Port areas
6 0.03 Airports
7 0.03 Mineral extraction sites
8 0.03 Dump sites
9 0.03 Construction sites
10 0.03 Green urban areas
11 0.03 Sport and leisure facilities
12 0.03 Non-irrigated arable land
13 0.035 Permanently irrigated land
14 0.035 Rice fields
15 0.03 Vineyards
16 0.04 Fruit trees and berry plantations
17 0.04 Olive groves
18 0.03 Pastures
19 0.035 Annual crops associated with permanent crops
20 0.035 Complex cultivation patterns
21 0.04 Land principally occupied by agriculture, with significant areas of natural vegetation
22 0.07 Agro-forestry areas
23 0.1 Broad-leaved forest
24 0.1 Coniferous forest
25 0.1 Mixed forest
26 0.05 Natural grasslands
27 0.05 Moors and heathland
28 0.05 Sclerophyllous vegetation
29 0.05 Transitional woodland-shrub
30 0.04 Beaches, dunes, sands
31 0.045 Bare rocks
32 0.05 Sparsely vegetated areas
33 0.03 Burnt areas
34 0.05 Glaciers and perpetual snow
35 0.02 Inland marshes
36 0.02 Peat bogs
37 0.02 Salt marshes
38 0.02 Salines
39 0.02 Intertidal flats
40 0.02 Water courses
41 0.02 Water bodies
42 0.02 Coastal lagoons
43 0.02 Estuaries
44 0.02 Sea and ocean
