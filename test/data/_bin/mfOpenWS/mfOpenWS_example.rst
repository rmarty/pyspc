
Exemples
--------

.. note:: Les exemples sont issus des tests unitaires.



:blue:`Récupérer depuis l'API des données météorologiques (MF_OpenAPI) pour la grandeur PJ pour les lieux listés dans data/_bin/mfOpenWS/cfg/sitesmeteo_openapi.txt entre les dates 2016-05-29 00:00:00 et 2016-06-03 00:00:00 et enregistrer les fichiers csv dans le répertoire data/_bin/mfOpenWS/out. Les éléments de connexion à l'API sont renseignés dans ../bin/mfOpenWS_RM.txt.`

.. container:: cmdimg

   .. container:: cmdline

      mfOpenWS.py -l data/_bin/mfOpenWS/cfg/sitesmeteo_openapi.txt -c ../bin/mfOpenWS_RM.txt -t MF_OpenAPI -n PJ -F 20160529 -L 20160603 -O data/_bin/mfOpenWS/out -v


.. code-block:: cfg

   [data_obs_meteo]
   appid = TTR3dzY0SGxFVXhNZXhrTmJkV0E4aWZXTXBVYTpiTHBqRnFpNl9OODFSbjJGczJSalBoek10ZHNh
   apikey = eyJ4NXQiOiJZV0kxTTJZNE1qWTNOemsyTkRZeU5XTTRPV014TXpjek1UVmhNbU14T1RSa09ETXlOVEE0Tnc9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJyZW5hdWQubWFydHlAY2FyYm9uLnN1cGVyIiwiYXBwbGljYXRpb24iOnsib3duZXIiOiJyZW5hdWQubWFydHkiLCJ0aWVyUXVvdGFUeXBlIjpudWxsLCJ0aWVyIjoiVW5saW1pdGVkIiwibmFtZSI6IkRlZmF1bHRBcHBsaWNhdGlvbiIsImlkIjoxODAsInV1aWQiOiIzOTdlOTAzMC1lZDkyLTQ1YTQtOWM5OS04YzQ1YzhmMmY2ZDMifSwiaXNzIjoiaHR0cHM6XC9cL3BvcnRhaWwtYXBpLm1ldGVvZnJhbmNlLmZyOjQ0M1wvb2F1dGgyXC90b2tlbiIsInRpZXJJbmZvIjp7IjUwUGVyTWluIjp7InRpZXJRdW90YVR5cGUiOiJyZXF1ZXN0Q291bnQiLCJncmFwaFFMTWF4Q29tcGxleGl0eSI6MCwiZ3JhcGhRTE1heERlcHRoIjowLCJzdG9wT25RdW90YVJlYWNoIjp0cnVlLCJzcGlrZUFycmVzdExpbWl0IjowLCJzcGlrZUFycmVzdFVuaXQiOiJzZWMifX0sImtleXR5cGUiOiJQUk9EVUNUSU9OIiwic3Vic2NyaWJlZEFQSXMiOlt7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJjYXJib24uc3VwZXIiLCJuYW1lIjoiQVJQRUdFIiwiY29udGV4dCI6IlwvcHVibGljXC9hcnBlZ2VcLzEuMCIsInB1Ymxpc2hlciI6ImFkbWluX21mIiwidmVyc2lvbiI6IjEuMCIsInN1YnNjcmlwdGlvblRpZXIiOiI1MFBlck1pbiJ9LHsic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImNhcmJvbi5zdXBlciIsIm5hbWUiOiJEb25uZWVzUHVibGlxdWVzQ2xpbWF0b2xvZ2llIiwiY29udGV4dCI6IlwvcHVibGljXC9EUENsaW1cL3YxIiwicHVibGlzaGVyIjoiYWRtaW5fbWYiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiNTBQZXJNaW4ifV0sImV4cCI6MTc2NjQ4MDMxNSwidG9rZW5fdHlwZSI6ImFwaUtleSIsImlhdCI6MTczNDk0NDMxNSwianRpIjoiMzIyYjk3ZmItZTFhNS00Mjc1LWEyNmYtMjc2ZTM3ZmE5NjdjIn0=.p0pAVXH-uFJL84g8vm00nM-nGzYjzbrbdQveihV2tuIHabAzYh0OUQ8pNpgPlFwNVIeX6_rgRFJso9Mi-dQsqqN9ZXVqvmE2TNMcGVGp8FwEodO0V1KIe06MWnDBz9MTdQZFGS4N2nL3iiDOmM5Y7Ofcbo0ISFMgZKRMILIXIWdtROwqH0fGtyz3JkO3wTMuBt9Ve6RHQFHtssgu5SCQebzqyumTmqXyI3B_enxIMM5VwePhNZnSfbHpC843US4kDJqivEzS7pzQFSkebbfcnKb1WaemYbJhmv6HZndAQF4sBif6hie3RsHCRkMKYT_fD4ABRftjKsxyNmM5gQSecg==


Fichier de stations : data/_bin/mfOpenWS/cfg/sitesmeteo_openapi.txt

.. code-block:: text

   45004001
   45069001
   45187001
   45340002



:blue:`Récupérer des données météorologiques (MF_OpenData) pour la grandeur PJ pour les lieux listés dans data/_bin/mfOpenWS/cfg/sitesmeteo.txt entre les dates 2019-09-01 00:00:00 et 2025-02-11 00:00:00 et enregistrer les fichiers csv.gz dans le répertoire data/_bin/mfOpenWS/out.`

.. container:: cmdimg

   .. container:: cmdline

      mfOpenWS.py -l data/_bin/mfOpenWS/cfg/sitesmeteo.txt -t MF_OpenData -n PJ -F 20190901 -L 20250211 -O data/_bin/mfOpenWS/out -v


Fichier de stations : data/_bin/mfOpenWS/cfg/sitesmeteo.txt

.. code-block:: text

   07075001
   07105001
   07154005
   43111002


