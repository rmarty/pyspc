[figure]
plottype        = hydro
dirname         = data/_bin/plotCsvData/out
filename        = hydro
verbose         = False
dpi             = 150
format          = png
title           = Hydrogrammes
xtitle          = temps
xlim            = 2008103100,2008110323
xfmt            = d/m/YnH:M
xminor          = 0,25,6
ytitle          = Hauteur (m)
legend          = 2
lfontsize       = 8
xfontsize       = 8
yfontsize       = 8
tfontsize       = 12

[defaut]
inputdir        = data/_bin/plotCsvData/in/hydro
firstdatetime   = 2008103100
lastdatetime    = 2008110323
color           = 0.0,0.0,0.0
linewidth       = 2
linestyle       = -
marker          = 
markersize      = 4

[K0260020_QH]
label       = Chadrac
color       = 0.4,0.6,1.0

[K0550010_QH]
label       = Bas-en-Basset
color       = 0.0,0.0,0.4
