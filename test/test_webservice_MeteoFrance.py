#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for pyspc.webservice.meteofrance

To run all tests just type:
    python -m unittest test_webservice_MeteoFrance

To run only a class test:
    python -m unittest test_webservice_MeteoFrance.TestOpenData

To run only a specific test:
    python -m unittest test_webservice_MeteoFrance.TestOpenData.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import os
import pandas as pnd
import unittest
import warnings

from pyspc.webservice.meteofrance import OpenAPI, OpenData


class TestOpenAPI(unittest.TestCase):
    """
    OpenAPI class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.simplefilter("ignore", ResourceWarning)
        warnings.filterwarnings('ignore', message='Unverified HTTPS request')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # ===================================================================
        hostname = 'https://public-api.meteofrance.fr/public'
        proxies = {'x': 'y'}
        timeout = 10
        oapi = OpenAPI(hostname=hostname, proxies=proxies, timeout=timeout)
        self.assertEqual(oapi.hostname, hostname)
        self.assertDictEqual(oapi.proxies, proxies)
        self.assertEqual(oapi.timeout, timeout)
        self.assertIsNone(oapi.url)
        self.assertIsNone(oapi.filename)
        # ===================================================================

    def test_get_data(self):
        """
        Test de la méthode get_data
        """
        # ===================================================================
        datatype = 'data_obs_meteo'
        loc = '43091005'
        timestep = td(days=1)
        start = dt(2024, 10, 15)
        end = dt(2024, 10, 19)
        oapi = OpenAPI()
        # ===================================================================
        content = oapi.get(
            datatype=datatype, codes=[loc], timestep=timestep,
            start=start, end=end)
        self.assertIsInstance(content, dict)
        self.assertEqual(len(content), 1)
        self.assertIn((loc, timestep, start, end), list(content.keys()))
        df = content[(loc, timestep, start, end)]
        self.assertIsInstance(df, pnd.DataFrame)
        self.assertEqual(len(df.index), 5)
        self.assertEqual(len(df.columns), 69)
        self.assertIn('POSTE', df.columns)
        self.assertEqual(list(df['POSTE'].unique()), [43091005])
        # ===================================================================

    def test_get_loc(self):
        """
        Test de la méthode get_loc
        """
        # ===================================================================
        datatype = 'loc_meteo'
        loc = '45'
        timestep = td(hours=1)
        oapi = OpenAPI()
        # ===================================================================
        content = oapi.get(datatype=datatype, codes=[loc], timestep=timestep)
        self.assertIsInstance(content, dict)
        self.assertEqual(len(content), 1)
        self.assertIn((loc, timestep), list(content.keys()))
        self.assertEqual(len(content[(loc, timestep)]), 17)
        # ===================================================================

    def test_get_loc_meta(self):
        """
        Test de la méthode get_loc_meta
        """
        # ===================================================================
        datatype = 'loc_meteo_meta'
        loc = '43091005'
        oapi = OpenAPI()
        # ===================================================================
        content = oapi.get(datatype=datatype, codes=[loc])
        self.assertIsInstance(content, dict)
        self.assertEqual(len(content), 1)
        self.assertIn(loc, list(content.keys()))
        # ===================================================================

    def test_retrieve_data(self):
        """
        Test de la méthode retrieve_data
        """
        # ===================================================================
        dirname = 'data'
        datatype = 'data_obs_meteo'
        loc = '43091005'
        timestep = td(days=1)
        start = dt(2024, 10, 15)
        end = dt(2024, 10, 19)
        valid = ['data\\43091005_Q_202410150000_202410190000.csv']
        oapi = OpenAPI()
        # ===================================================================
        filenames = oapi.retrieve(
            dirname=dirname, datatype=datatype,
            codes=[loc], timestep=timestep,
            start=start, end=end)
        self.assertListEqual(filenames, valid)
        for f in filenames:
            os.remove(f)
        # ===================================================================

    def test_retrieve_loc(self):
        """
        Test de la méthode retrieve_loc
        """
        # ===================================================================
        dirname = 'data'
        datatype = 'loc_meteo'
        loc = '45'
        timestep = td(hours=1)
        valid = ['data\\liste-stations_45_H.csv']
        oapi = OpenAPI()
        # ===================================================================
        filenames = oapi.retrieve(
            dirname=dirname, datatype=datatype, codes=[loc], timestep=timestep)
        self.assertListEqual(filenames, valid)
        for f in filenames:
            os.remove(f)
        # ===================================================================

    def test_retrieve_loc_full(self):
        """
        Test de la méthode retrieve_loc
        """
        # ===================================================================
        dirname = 'data'
        datatype = 'loc_meteo'
        loc = '43'
        timestep = td(hours=1)
        valid = [
            'data\\liste-stations_43_H.csv',
            'data\\43003001_metadata.json',
            'data\\43012001_metadata.json',
            'data\\43014001_metadata.json',
            'data\\43020002_metadata.json',
            'data\\43037001_metadata.json',
            'data\\43046001_metadata.json',
            'data\\43062001_metadata.json',
            'data\\43067001_metadata.json',
            'data\\43071001_metadata.json',
            'data\\43091002_metadata.json',
            'data\\43091005_metadata.json',
            'data\\43093001_metadata.json',
            'data\\43095001_metadata.json',
            'data\\43096001_metadata.json',
            'data\\43103001_metadata.json',
            'data\\43104001_metadata.json',
            'data\\43111002_metadata.json',
            'data\\43118001_metadata.json',
            'data\\43130002_metadata.json',
            'data\\43137003_metadata.json',
            'data\\43150001_metadata.json',
            'data\\43157002_metadata.json',
            'data\\43157004_metadata.json',
            'data\\43162001_metadata.json',
            'data\\43178003_metadata.json',
            'data\\43212002_metadata.json',
            'data\\43215001_metadata.json',
            'data\\43223001_metadata.json',
            'data\\43234005_metadata.json',
            'data\\43241002_metadata.json',
            'data\\43244003_metadata.json',
            'data\\43246001_metadata.json',
            'data\\43268004_metadata.json',
            'data\\43268005_metadata.json',
        ]
        oapi = OpenAPI()
        # ===================================================================
        filenames = oapi.retrieve(
            dirname=dirname, datatype=datatype, codes=[loc], timestep=timestep,
            meta=True)
        self.maxDiff = None
        self.assertListEqual(filenames, valid)
        for f in filenames:
            os.remove(f)
        # ===================================================================

    def test_retrieve_loc_meta(self):
        """
        Test de la méthode retrieve_loc_meta
        """
        # ===================================================================
        dirname = 'data'
        datatype = 'loc_meteo_meta'
        loc = '43091005'
        valid = ['data\\43091005_metadata.json']
        oapi = OpenAPI()
        # ===================================================================
        filenames = oapi.retrieve(
            dirname=dirname, datatype=datatype, codes=[loc])
        self.assertListEqual(filenames, valid)
        for f in filenames:
            os.remove(f)
        # ===================================================================


class TestOpenData(unittest.TestCase):
    """
    OpenData class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # ===================================================================
        hostname = 'meteo.data.gouv.fr'
        proxies = {'x': 'y'}
        timeout = 10
        odata = OpenData(hostname=hostname, proxies=proxies, timeout=timeout)
        self.assertEqual(odata.hostname, hostname)
        self.assertDictEqual(odata.proxies, proxies)
        self.assertEqual(odata.timeout, timeout)
        self.assertIsNone(odata.url)
        self.assertIsNone(odata.filename)
        self.assertIsNone(odata.verify)
        # ===================================================================
        odata = OpenData(proxies={})
        self.assertEqual(
            odata.hostname,
            'https://object.files.data.gouv.fr/meteofrance/data/synchro_ftp'
            '/BASE')
        self.assertDictEqual(odata.proxies, {})
        self.assertEqual(odata.timeout, 300)
        self.assertIsNone(odata.url)
        self.assertIsNone(odata.filename)
        self.assertIsNone(odata.verify)
        # ===================================================================

    def test_get(self):
        """
        Test Requête Quotidien
        """
        # ===================================================================
        remotedir = 'QUOT'
        remotefile = 'Q_descriptif_champs_RR-T-Vent.csv'
        odata = OpenData()
        odata.login()
        res = odata.get(remotedir=remotedir, remotefile=remotefile)
        self.assertEqual(
            odata.url,
            'https://object.files.data.gouv.fr/meteofrance/data/synchro_ftp/'
            'BASE/QUOT/Q_descriptif_champs_RR-T-Vent.csv'
        )
        self.assertIsInstance(res.text, str)
        odata.logout()
        # ===================================================================

    def test_retrieve_quot(self):
        """
        Test Export Quotidien
        """
        # ===================================================================
        codes = ['7075001', '07105001', '07154005', '43111002']
        start = dt(2019, 9, 1)
        end = dt.utcnow()
        timestep = td(days=1)
        dirname = 'data'
        # ===================================================================
        valid = ['Q_07_latest-2024-2025_RR-T-Vent.csv.gz',
                 'Q_07_previous-1950-2023_RR-T-Vent.csv.gz',
                 'Q_43_latest-2024-2025_RR-T-Vent.csv.gz',
                 'Q_43_previous-1950-2023_RR-T-Vent.csv.gz']
        valid = [os.path.join(dirname, v) for v in valid]
        odata = OpenData()
        odata.login()
        filenames = odata.retrieve(
            codes=codes, start=start, end=end, timestep=timestep,
            dirname=dirname, desc=False
        )
        self.assertListEqual(filenames, valid)
        for f in filenames:
            os.remove(f)
        # ===================================================================
        valid = ['Q_07_latest-2024-2025_RR-T-Vent.csv.gz',
                 'Q_07_previous-1950-2023_RR-T-Vent.csv.gz',
                 'Q_descriptif_champs_RR-T-Vent.csv']
        valid = [os.path.join(dirname, v) for v in valid]
        filenames = odata.retrieve(
            codes=codes[:2], start=start, end=end, timestep=timestep,
            dirname=dirname, desc=True
        )
        self.assertListEqual(filenames, valid)
        for f in filenames:
            os.remove(f)
        odata.logout()
        # ===================================================================

    def test_retrieve_quot_others(self):
        """
        Test Export Quotidien - rr_t_uv=False
        """
        # ===================================================================
        codes = ['7075001', '07105001', '07154005', '43111002']
        start = dt(2019, 9, 1)
        end = dt.utcnow()
        timestep = td(days=1)
        dirname = 'data'
        # ===================================================================
        valid = ['Q_07_latest-2024-2025_autres-parametres.csv.gz',
                 'Q_07_previous-1950-2023_autres-parametres.csv.gz',
                 'Q_43_latest-2024-2025_autres-parametres.csv.gz',
                 'Q_43_previous-1950-2023_autres-parametres.csv.gz']
        valid = [os.path.join(dirname, v) for v in valid]
        odata = OpenData()
        odata.login()
        filenames = odata.retrieve(
            codes=codes, start=start, end=end, timestep=timestep,
            dirname=dirname, desc=False, rr_t_uv=False
        )
        self.assertListEqual(filenames, valid)
        for f in filenames:
            os.remove(f)
        # ===================================================================
        valid = ['Q_07_latest-2024-2025_autres-parametres.csv.gz',
                 'Q_07_previous-1950-2023_autres-parametres.csv.gz',
                 'Q_descriptif_champs_autres-parametres.csv']
        valid = [os.path.join(dirname, v) for v in valid]
        filenames = odata.retrieve(
            codes=codes[:2], start=start, end=end, timestep=timestep,
            dirname=dirname, desc=True, rr_t_uv=False
        )
        self.assertListEqual(filenames, valid)
        for f in filenames:
            os.remove(f)
        odata.logout()
        # ===================================================================

    def test_retrieve_hor(self):
        """
        Test Export Horaire
        """
        # ===================================================================
        codes = ['43111002']
        start = dt(2019, 9, 1)
        end = dt.utcnow()
        timestep = td(hours=1)
        dirname = 'data'
        # ===================================================================
        valid = ['H_43_2010-2019.csv.gz',
                 'H_43_latest-2024-2025.csv.gz',
                 'H_43_previous-2020-2023.csv.gz',
                 'H_descriptif_champs.csv']
        valid = [os.path.join(dirname, v) for v in valid]
        odata = OpenData()
        odata.login()
        filenames = odata.retrieve(
            codes=codes, start=start, end=end, timestep=timestep,
            dirname=dirname, desc=True
        )
        self.assertListEqual(filenames, valid)
        for f in filenames:
            os.remove(f)
        # ===================================================================

    def test_retrieve_min(self):
        """
        Test Export Minute
        """
        # ===================================================================
        codes = ['43111002']
        start = dt(2019, 9, 1)
        end = dt.utcnow()
        timestep = td(minutes=6)
        dirname = 'data'
        # ===================================================================
        valid = ['MN_43_2010-2019.csv.gz',
                 'MN_43_latest-2024-2025.csv.gz',
                 'MN_43_previous-2020-2023.csv.gz',
                 'MN_descriptif_champs.csv']
        valid = [os.path.join(dirname, v) for v in valid]
        odata = OpenData()
        odata.login()
        filenames = odata.retrieve(
            codes=codes, start=start, end=end, timestep=timestep,
            dirname=dirname, desc=True
        )
        self.assertListEqual(filenames, valid)
        for f in filenames:
            os.remove(f)
        # ===================================================================

    def test_filenames(self):
        """
        Test Liste des fichiers
        """
        # ===================================================================
        odata = OpenData()
        valid = {
            (43, dt(1910, 1, 1), td(days=1), True):
                'Q_43_1865-1949_RR-T-Vent.csv.gz',
            (43, dt(1910, 1, 1), td(days=1), False):
                'Q_43_1865-1949_autres-parametres.csv.gz',
            (43, dt(1949, 12, 31), td(days=1), True):
                'Q_43_1865-1949_RR-T-Vent.csv.gz',
            (43, dt(1950, 1, 1), td(days=1), True):
                'Q_43_previous-1950-2023_RR-T-Vent.csv.gz',
            (43, dt(2023, 12, 31), td(days=1), True):
                'Q_43_previous-1950-2023_RR-T-Vent.csv.gz',
            (43, dt(2024, 1, 1), td(days=1), True):
                'Q_43_latest-2024-2025_RR-T-Vent.csv.gz',
            (43, dt(2024, 12, 31), td(days=1), True):
                'Q_43_latest-2024-2025_RR-T-Vent.csv.gz',
            (43, dt(2025, 5, 1), td(days=1), True):
                'Q_43_latest-2024-2025_RR-T-Vent.csv.gz',

            (15, dt(1920, 1, 1), td(hours=1), True):
                'H_15_1920-1929.csv.gz',
            (15, dt(1958, 12, 31), td(hours=1), True):
                'H_15_1950-1959.csv.gz',
            (15, dt(2000, 1, 1), td(hours=1), True):
                'H_15_2000-2009.csv.gz',
            (15, dt(2009, 12, 31), td(hours=1), True):
                'H_15_2000-2009.csv.gz',
            (15, dt(2010, 1, 1), td(hours=1), True):
                'H_15_2010-2019.csv.gz',
            (15, dt(2019, 12, 31), td(hours=1), True):
                'H_15_2010-2019.csv.gz',
            (15, dt(2020, 1, 1), td(hours=1), True):
                'H_15_previous-2020-2023.csv.gz',
            (15, dt(2023, 12, 31), td(hours=1), True):
                'H_15_previous-2020-2023.csv.gz',
            (15, dt(2024, 1, 1), td(hours=1), True):
                'H_15_latest-2024-2025.csv.gz',
            (15, dt(2025, 5, 1), td(hours=1), True):
                'H_15_latest-2024-2025.csv.gz',

            (7, dt(2000, 1, 1), td(minutes=6), True):
                'MN_07_2000-2009.csv.gz',
            (7, dt(2009, 12, 31), td(minutes=6), True):
                'MN_07_2000-2009.csv.gz',
            (7, dt(2010, 1, 1), td(minutes=6), True):
                'MN_07_2010-2019.csv.gz',
            (7, dt(2019, 12, 31), td(minutes=6), True):
                'MN_07_2010-2019.csv.gz',
            (7, dt(2020, 1, 1), td(minutes=6), True):
                'MN_07_previous-2020-2023.csv.gz',
            (7, dt(2023, 12, 31), td(minutes=6), True):
                'MN_07_previous-2020-2023.csv.gz',
            (7, dt(2024, 1, 1), td(minutes=6), True):
                'MN_07_latest-2024-2025.csv.gz',
            (7, dt(2025, 5, 1), td(minutes=6), True):
                'MN_07_latest-2024-2025.csv.gz',

        }
        for k, v in valid.items():
            self.assertEqual(odata.set_basename(*k), v)
        # ===================================================================

    def test_timesteps(self):
        """
        Test liste des pages en ligne disponibles
        """
        valid = [td(seconds=360), td(seconds=3600), td(days=1)]
        self.assertEqual(valid, OpenData.get_timesteps())
