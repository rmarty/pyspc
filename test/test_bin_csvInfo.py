#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary csvInfo

To run all tests just type:
    python -m unittest test_bin_csvInfo

To run only a class test:
    python -m unittest test_bin_csvInfo.Test_csvInfo

To run only a specific test:
    python -m unittest test_bin_csvInfo.Test_csvInfo.test_diff

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import csvInfo as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_csvInfo(unittest.TestCase):
    """
    csvInfo bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'csvInfo')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.stations_list_file = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)

    def test_diff(self):
        """
        Test comparaison de séries
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csvInfo.py',
            '-I', self.dir_in,
            '-n', 'QH',
            '-s', 'K040301mvl',
            '-M', 'diff', 'K0403010', self.dir_in,
            '-c', os.path.join(self.dir_out, 'K0403010_diff.txt')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csvInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'K0403010_diff.txt'))
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.processing_method,
                         ['diff', 'K0403010', self.dir_in])
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.station_name, 'K040301mvl')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, False)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            os.path.join(args.output_filename),
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(os.path.join(args.output_filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Comparaison de la série {s} lue dans le répertoire {I} "\
            "avec la référence {M1} lue dans {M2} pour la grandeur {n}. "\
            "La comparaison est exportée dans le fichier {c}"\
            ""\
            "".format(
                I=args.input_dir, c=args.output_filename,
                s=args.station_name, n=args.varname,
                M1=args.processing_method[1],
                M2=args.processing_method[2])
        # =====================================================================

    def test_diff_list(self):
        """
        Test comparaison de séries
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'Chambon.txt')
        processArgs = [
            'python',
            '../bin/csvInfo.py',
            '-I', self.dir_in,
            '-n', 'QH',
            '-l', self.stations_list_file,
            '-M', 'diff',
            '-c', os.path.join(self.dir_out, 'Chambon_diff.txt')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csvInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'Chambon_diff.txt'))
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.processing_method, ['diff'])
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
#        self.assertEqual(args.verbose, False)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            os.path.join(args.output_filename),
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(os.path.join(args.output_filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Comparaison des séries listées dans {l} "\
            "et lues dans le répertoire {I} pour la grandeur {n}. "\
            "La référence est la première listée dans {l}. "\
            "La comparaison est exportée dans le fichier {c}"\
            ""\
            "".format(
                I=args.input_dir, c=args.output_filename,
                l=args.stations_list_file, n=args.varname)
        # =====================================================================

    def test_evt_basic(self):
        """
        Test extraction evenements
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csvInfo.py',
            '-I', self.dir_in,
            '-n', 'QJ',
            '-s', 'K1251810',
            '-M', 'evt', '60',
            '-c', os.path.join(self.dir_out, 'K1251810_evt.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csvInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'K1251810_evt.csv'))
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.processing_method, ['evt', '60'])
        self.assertEqual(args.varname, 'QJ')
        self.assertEqual(args.station_name, 'K1251810')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, False)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            os.path.join(args.output_filename),
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(os.path.join(args.output_filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Etablir la liste des événements contenus dans la "\
            "série {s} lue dans le répertoire {I} pour la grandeur {n}. "\
            "L'algorithme 'basic' est utilisé, en appliquant le seuil {M1}. "\
            "L'analyse est exportée dans le fichier {c}"\
            ""\
            "".format(
                I=args.input_dir, c=args.output_filename,
                M1=args.processing_method[1],
                s=args.station_name, n=args.varname)
        # =====================================================================

    def test_evt_scipy(self):
        """
        Test extraction evenements
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csvInfo.py',
            '-I', self.dir_in,
            '-n', 'QJ',
            '-s', 'K0260010',
            '-M', 'evt', '50', 'scipy', '20', '2', '-7', '7',
            '-c', os.path.join(self.dir_out, 'K0260010_evt.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csvInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'K0260010_evt.csv'))
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.processing_method,
                         ['evt', '50', 'scipy', '20', '2', '-7', '7'])
        self.assertEqual(args.varname, 'QJ')
        self.assertEqual(args.station_name, 'K0260010')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, False)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        f = args.output_filename
        self.assertTrue(filecmp.cmp(
            f, os.path.join(self.dir_ctl, os.path.basename(f))))
        os.remove(f)
        f = f.replace('.csv', '.png')
        self.assertTrue(filecmp.cmp(
            f, os.path.join(self.dir_ctl, os.path.basename(f))))
        os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Etablir la liste des événements contenus dans la "\
            "série {s} lue dans le répertoire {I} pour la grandeur {n}. "\
            "L'algorithme {M2} est utilisé, en appliquant le seuil {M1} "\
            "et les autres pamraètres ({Mx}). "\
            "L'analyse est exportée dans le fichier {c}"\
            ""\
            "".format(
                I=args.input_dir, c=args.output_filename,
                M1=args.processing_method[1], M2=args.processing_method[2],
                Mx=' '.join(args.processing_method[3:]),
                s=args.station_name, n=args.varname)
        # =====================================================================

    def test_max(self):
        """
        Test extraction maximas annuels
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csvInfo.py',
            '-I', self.dir_in,
            '-n', 'QJ',
            '-s', 'K1251810',
            '-M', 'max',
            '-c', os.path.join(self.dir_out, 'K1251810_max.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csvInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'K1251810_max.csv'))
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.processing_method, ['max'])
        self.assertEqual(args.varname, 'QJ')
        self.assertEqual(args.station_name, 'K1251810')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, False)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            os.path.join(args.output_filename),
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(os.path.join(args.output_filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraction des maximas annuels de la série {s} "\
            "lue dans le répertoire {I} pour la grandeur {n}. "\
            "L'analyse est exportée dans le fichier {c}"\
            ""\
            "".format(
                I=args.input_dir, c=args.output_filename,
                s=args.station_name, n=args.varname)
        # =====================================================================

    def test_mvl(self):
        """
        Test Valeurs manquantes
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csvInfo.py',
            '-I', self.dir_in,
            '-n', 'QH',
            '-s', 'K040301mvl',
            '-M', 'mvl',
            '-c', os.path.join(self.dir_out, 'K040301mvl.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csvInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'K040301mvl.csv'))
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.processing_method, ['mvl'])
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.station_name, 'K040301mvl')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, False)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            os.path.join(args.output_filename),
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(os.path.join(args.output_filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Etablir la complétude des données de la série {s} "\
            "lue dans le répertoire {I} pour la grandeur {n}. "\
            "L'analyse est exportée dans le fichier {c}"\
            ""\
            "".format(
                I=args.input_dir, c=args.output_filename,
                s=args.station_name, n=args.varname)
        # =====================================================================

    def test_mvl_subperiod(self):
        """
        Test Valeurs manquantes - Dans une sous-période
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csvInfo.py',
            '-I', self.dir_in,
            '-n', 'QH',
            '-s', 'K040301mvl',
            '-M', 'mvl',
            '-c', os.path.join(self.dir_out, 'K040301mvl_sub.csv'),
            '-F', '2008110120',
            '-L', '2008110222'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csvInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'K040301mvl_sub.csv'))
        self.assertEqual(args.first_dtime, '2008110120')
        self.assertEqual(args.last_dtime, '2008110222')
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.processing_method, ['mvl'])
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.station_name, 'K040301mvl')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, False)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            os.path.join(args.output_filename),
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(os.path.join(args.output_filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Etablir la complétude des données de la série {s} "\
            "lue dans le répertoire {I} pour la grandeur {n}. "\
            "Les données sont censurées à la période {F} - {L}. "\
            "L'analyse est exportée dans le fichier {c}"\
            ""\
            "".format(
                I=args.input_dir, c=args.output_filename,
                F=args.first_dtime, L=args.last_dtime,
                s=args.station_name, n=args.varname)
        # =====================================================================

    def test_mvs(self):
        """
        Test Valeurs manquantes - Suppression des np.nan en début/fin de séries
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csvInfo.py',
            '-I', self.dir_in,
            '-n', 'QH',
            '-s', 'K040301mvl',
            '-M', 'mvs',
            '-c', os.path.join(self.dir_out, 'K040301mvs.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csvInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'K040301mvs.csv'))
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.processing_method, ['mvs'])
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.station_name, 'K040301mvl')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, False)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            os.path.join(args.output_filename),
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(os.path.join(args.output_filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Etablir la complétude des données de la série {s} "\
            "lue dans le répertoire {I} pour la grandeur {n}. "\
            "Les données manquantes en début et fin de série sont ignorées. "\
            "L'analyse est exportée dans le fichier {c}"\
            ""\
            "".format(
                I=args.input_dir, c=args.output_filename,
                s=args.station_name, n=args.varname)
        # =====================================================================

    def test_regime(self):
        """
        Test regime hydrologique
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csvInfo.py',
            '-I', self.dir_in,
            '-n', 'QJ',
            '-s', 'K1251810',
            '-M', 'reg',
            'month',  # Groupe
            '0.10,0.25,0.50,0.75,0.90',  # Freqs
            '6',  # Dayhour
            'False',  # Strict
            'False',  # Echelle ignorée
            self.dir_out,  # Répertoire,
            'Oui',  # Boxplot
            'yes',  # Fill
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csvInfo()
        self.assertEqual(args.output_filename, None)
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.processing_method,
                         ['reg', 'month', '0.10,0.25,0.50,0.75,0.90', '6',
                          'False', 'False', self.dir_out, 'Oui', 'yes'])
        self.assertEqual(args.varname, 'QJ')
        self.assertEqual(args.station_name, 'K1251810')
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.verbose, False)
        self.assertEqual(args.csv_type, 'pyspc')
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        for b in ['K1251810_QJ_regime-month.csv',
                  'K1251810_QJ_regime-month.png']:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, b),
                os.path.join(self.dir_ctl, b),
            ))
            os.remove(os.path.join(self.dir_out, b))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Etablir le régime hydrologique de la "\
            "série {s} lue dans le répertoire {I} pour la grandeur {n}. "\
            "L'algorithme est utilisé, en appliquant les paramètres: \n"\
            "- 'Groupe' = {M1}\n"\
            "- 'Freqs' = {M2}\n"\
            "- 'Dayhour' = {M3}\n"\
            "- 'Strict' = {M4}\n"\
            "- 'Echelle' = {M5}\n"\
            "- 'Répertoire' = {M6}\n"\
            "- 'Boxplot' = {M7}\n"\
            "- 'Fill' = {M8}\n"\
            "".format(
                I=args.input_dir,
                M1=args.processing_method[1],
                M2=args.processing_method[2],
                M3=args.processing_method[3],
                M4=args.processing_method[4],
                M5=args.processing_method[5],
                M6=args.processing_method[6],
                M7=args.processing_method[7],
                M8=args.processing_method[8],
                s=args.station_name, n=args.varname)
        # =====================================================================
