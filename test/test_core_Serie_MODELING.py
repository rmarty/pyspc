#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Serie in pyspc.core.serie - modeling

To run all tests just type:
    python -m unittest test_core_Serie_MODELING

To run only a class test:
    python -m unittest test_core_Serie_MODELING.TestSerie

To run only a specific test:
    python -m unittest test_core_Serie_MODELING.TestSerie.test_plot

"""
# Imports
from datetime import datetime as dt, timedelta as td
import numpy as np
import pandas as pnd
from pandas.util.testing import assert_frame_equal
from random import Random
import unittest
import warnings

# Imports pyspc
from pyspc.core.serie import Serie
from pyspc.core.ratingcurve import RatingCurve, RatingCurves
from pyspc.core.reservoir import Reservoir, Table


class TestSerie(unittest.TestCase):
    """
    Serie class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        self.code = 'K0000000'
        self.provider = 'SPC'

    def test_oudin_THEJ(self):
        """
        Test méthode oudin : TH -> EJ
        """
        # =====================================================================
        varname = 'TH'
        src = pnd.DataFrame(
            {(self.code, varname): [
                17.600, 17.300, 16.500, 15.100, 15.500, 15.600, 17.500,
                20.500, 21.700, 22.700, 24.600, 25.400, 25.500, 24.500,
                24.300, 25.400, 24.300, 23.800, 23.000, 20.800, 19.800,
                19.100, 18.600, 17.600, 17.400, 16.500, 16.400, 16.000,
                15.800, 14.900, 16.900, 19.300, 19.400, 22.500, 23.200,
                24.400, 25.200]},
            index=pnd.date_range(
                dt(2018, 7, 31), dt(2018, 8, 1, 12), freq=td(hours=1)
            )
        )
        src = Serie(
            datval=src,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid_varname = 'EJ'
        valid_serie = pnd.DataFrame(
            {(self.code, valid_varname): [4.126]},
            index=pnd.date_range(
                dt(2018, 7, 31), dt(2018, 7, 31), freq=td(days=1)
            )
        )
        valid_serie = Serie(
            datval=valid_serie,
            code=self.code,
            provider=self.provider,
            varname=valid_varname,
        )
        # =====================================================================
        serie = src.etp_oudin(latitude=44.734, timestep=td(days=1))
        assert_frame_equal(
            serie.data_frame, valid_serie.data_frame, check_less_precise=3)
        # =====================================================================

    def test_oudin_TJEJ(self):
        """
        Test méthode oudin : TJ -> EJ
        """
        # =====================================================================
        varname = 'TJ'
        src = pnd.DataFrame(
            {(self.code, varname): [1.404, 1.03, 5.012]},
            index=pnd.date_range(
                dt(2014, 11, 5), dt(2014, 11, 7), freq=td(days=1)
            )
        )
        src = Serie(
            datval=src,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid_varname = 'EJ'
        valid_serie = pnd.DataFrame(
            {(self.code, valid_varname): [0.403172, 0.374730, 0.614209]},
            index=pnd.date_range(
                dt(2014, 11, 5), dt(2014, 11, 7), freq=td(days=1)
            )
        )
        valid_serie = Serie(
            datval=valid_serie,
            code=self.code,
            provider=self.provider,
            varname=valid_varname,
        )
        # =====================================================================
        serie = src.etp_oudin(latitude=45.025)
        assert_frame_equal(serie.data_frame, valid_serie.data_frame)
        # =====================================================================

    def test_oudin_TJEH(self):
        """
        Test méthode oudin : TJ -> EH
        """
        # =====================================================================
        varname = 'TJ'
        src = pnd.DataFrame(
            {(self.code, varname): [5.012]},
            index=pnd.date_range(
                dt(2014, 11, 7), dt(2014, 11, 7), freq=td(days=1)
            )
        )
        src = Serie(
            datval=src,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid_varname = 'EH'
        valid_serie = pnd.DataFrame(
            {(self.code, valid_varname): [
                0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                0.000000, 0.021497, 0.038081, 0.048523, 0.059578, 0.067563,
                0.071863, 0.071863, 0.067563, 0.059578, 0.048523, 0.038081,
                0.021497, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000]},
            index=pnd.date_range(
                dt(2014, 11, 7), dt(2014, 11, 7, 23), freq=td(hours=1)
            )
        )
        valid_serie = Serie(
            datval=valid_serie,
            code=self.code,
            provider=self.provider,
            varname=valid_varname,
        )
        # =====================================================================
        serie = src.etp_oudin(latitude=45.025, timestep=td(hours=1))
        assert_frame_equal(
            serie.data_frame, valid_serie.data_frame, check_less_precise=3)
        # =====================================================================

    def test_ratingcurves(self):
        """
        Test Convertir par RatingCurves
        """
        # =====================================================================
        levelcor = pnd.DataFrame({
                'h': [0, -1, -1, 0]
            },
            index=[dt(2020, 12, 31, 18), dt(2020, 12, 31, 21),
                   dt(2021, 1, 1, 1), dt(2021, 1, 1, 3)]
        )
        curve_K01 = RatingCurve(
            code='K0000000', num='K01', provider='foo',
            valid_dt=(dt(2020, 1, 1), dt(2021, 1, 1) - td(seconds=1)),
            valid_interval=(0, 2), hq=[(0, 0), (1, 100), (2, 200)]
        )
        curve_K02 = RatingCurve(
            code='K0000000', num='K02', provider='foo',
            valid_dt=(dt(2021, 1, 1), dt(2022, 1, 1)),
            valid_interval=(0, 2), hq=[(0, 0), (1, 100), (2, 2000)]
        )
        curve_K03 = RatingCurve(
            code='K0000000', num='K03', provider='bar',
            valid_dt=(dt(2021, 1, 1), dt(2030, 1, 1)),
            valid_interval=(0, 2), hq=[(0, 0), (1, 1000), (2, 2000)]
        )
        curve_K99 = RatingCurve(
            code='K9999999', num='K99', provider='foo',
            valid_dt=(dt(2020, 1, 1), dt(2021, 1, 1)), valid_interval=(0, 2),
            hq=[(0, 0), (2, 200)]
        )
        curve_KL1 = RatingCurve(
            code='K0000000', num='K01', provider='foo',
            valid_dt=(dt(2020, 1, 1), dt(2021, 1, 1) - td(seconds=1)),
            valid_interval=(0, 2), hq=[(0, 0), (1, 100), (2, 200)],
            levelcor=levelcor
        )
        curve_KL2 = RatingCurve(
            code='K0000000', num='K02', provider='foo',
            valid_dt=(dt(2021, 1, 1), dt(2022, 1, 1)),
            valid_interval=(0, 2), hq=[(0, 0), (1, 100), (2, 2000)],
            levelcor=levelcor
        )
        # =====================================================================
        code = 'K0000000'
        data = pnd.DataFrame(
            {'QH': [0, 50, 75, 100, 150, 200,
                    2000, 2000, 1050, 100, 75, 50, 0],
             'HH': [0.00, 0.50, 0.75, 1.00, 1.50, 2.00,
                    2.00, 2.00, 1.50, 1.00, 0.75, 0.50, 0.00]},
            index=pnd.date_range(
                dt(2020, 12, 31, 18), dt(2021, 1, 1, 6), freq='H'),
            dtype=np.float64
        )
        datalev = pnd.DataFrame(
            {'QH': [0, 16.67, 8.33, 0, 50, 100, 100, 100, 100, 100, 75, 50, 0],
             'HH': [0.00, 0.833, 1.417, 2.00, 2.50, 3.00, 3.00, 3.00, 2.00,
                    1.00, 0.75, 0.50, 0.00]},
            index=pnd.date_range(
                dt(2020, 12, 31, 18), dt(2021, 1, 1, 6), freq='H'),
            dtype=np.float64
        )
        h = Serie(datval=data['HH'].to_frame(), code=code, varname='HH')
        q = Serie(datval=data['QH'].to_frame(), code=code, varname='QH')
        hlev = Serie(datval=datalev['HH'].to_frame(), code=code, varname='HH')
        qlev = Serie(datval=datalev['QH'].to_frame(), code=code, varname='QH')
        # =====================================================================
        # Cas SANS CT à la station / au site
        # ---------------------------------------------------------------------
        curves = RatingCurves()
        curves.add(curve_K99)
        with self.assertRaises(ValueError):
            h.apply_RatingCurves(curves=curves)
        # =====================================================================
        # Cas AVEC période commune à deux CT
        # ---------------------------------------------------------------------
        curves = RatingCurves()
        curves.add(curve_K01)
        curves.add(curve_K02)
        curves.add(curve_K03)
        with self.assertRaises(ValueError):
            h.apply_RatingCurves(curves=curves)
        # =====================================================================
        # Cas SANS période commune à deux CT - H vers Q
        # ---------------------------------------------------------------------
        curves = RatingCurves()
        curves.add(curve_K01)
        curves.add(curve_K02)
        curves.add(curve_K99)
        serie = h.apply_RatingCurves(curves=curves)
        self.assertEqual(serie.code, code)
        self.assertEqual(serie.spc_varname, 'QH')
        assert_frame_equal(
            serie.data_frame, q.data_frame, check_less_precise=3)
        # =====================================================================
        # Cas SANS période commune à deux CT - Q vers H
        # ---------------------------------------------------------------------
        serie = q.apply_RatingCurves(curves=curves)
        self.assertEqual(serie.code, code)
        self.assertEqual(serie.spc_varname, 'HH')
        assert_frame_equal(
            serie.data_frame, h.data_frame, check_less_precise=3)
        # =====================================================================
        # Cas SANS période commune à deux CT - H vers Q + levelcor
        # ---------------------------------------------------------------------
        curves = RatingCurves()
        curves.add(curve_KL1)
        curves.add(curve_KL2)
        serie = h.apply_RatingCurves(curves=curves)
        self.assertEqual(serie.code, code)
        self.assertEqual(serie.spc_varname, 'QH')
        assert_frame_equal(
            serie.data_frame, qlev.data_frame, check_less_precise=3)
        # =====================================================================
        # Cas SANS période commune à deux CT - Q vers H + levelcor
        # ---------------------------------------------------------------------
        curves = RatingCurves()
        curves.add(curve_KL1)
        curves.add(curve_KL2)
        serie = q.apply_RatingCurves(curves=curves)
        self.assertEqual(serie.code, code)
        self.assertEqual(serie.spc_varname, 'HH')
        assert_frame_equal(
            serie.data_frame, hlev.data_frame, check_less_precise=3)
        # =====================================================================

    def test_reservoir(self):
        """
        Test Convertir par Réservoir
        """
        # =====================================================================
        classic = {
            'Z': [
                280.80, 281.80, 282.80, 283.80, 284.80, 285.80, 286.80,
                287.80, 288.80, 289.80, 290.80, 291.80, 292.80, 293.80,
                294.80, 295.80, 296.80, 297.80, 298.80, 299.80, 300.80,
                301.80, 302.80
            ],
            'Qd': [
                0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
                0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
                0.000, 0.000, 0.564, 226.062, 630.306, 1179.923, 1874.914
            ],
            'Vd1': [
                0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
                0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
                0.000, 0.000, 0.002, 0.814, 2.269, 4.248, 6.750
            ],
            'V': [
                4.155, 4.663, 5.214, 5.809, 6.448, 7.130, 7.856, 8.626,
                9.440, 10.298, 11.199, 12.144, 13.133, 14.165, 15.241,
                16.361, 17.525, 18.732, 19.983, 21.278, 22.617, 23.999, 25.425
            ],
            'V+0.5*Vd1': [
                4.155, 4.663, 5.214, 5.809, 6.448, 7.130, 7.856, 8.626,
                9.440, 10.298, 11.199, 12.144, 13.133, 14.165, 15.241,
                16.361, 17.525, 18.732, 19.984, 21.685, 23.751, 26.123, 28.800
            ]
        }
        classic = pnd.DataFrame(classic)
        ratios = {
            'Vd1': 1000000,
            'V': 1000000,
            'V+0.5*Vd1': 1000000
        }
        # =====================================================================
        code = 'K0000000'
        provider = 'SPC'
        bareme = Table(datatype='table', ratios=ratios)
        bareme._name = 'classic'
        bareme._table = classic
        bareme._cols = classic.columns
        bareme._assoc = {(c[0] if isinstance(c, str) else c): c
                         for c in bareme.cols}
        name = 'Barrage'
        Z0 = 298.78
        reservoir = Reservoir(code=code, name=name, Z0=Z0)
        reservoir._tables = {'classic': bareme}
        # =====================================================================
        data = pnd.DataFrame(
            {'ZH': [298.250, 298.390, 298.560, 298.670, 298.770, 298.980,
                    299.020, 299.150, 299.170, 299.120, 299.130, 299.150,
                    299.160, 299.320, 299.330],
             'HH': [-0.53, -0.39, -0.22, -0.11, -0.01, 0.20, 0.24, 0.37, 0.39,
                    0.34, 0.35, 0.37, 0.38, 0.54, 0.55],
             'QH': [0.25, 0.33, 0.43, 0.49, 0.55, 41.16, 50.17, 79.49, 84.00,
                    72.72, 74.98, 79.49, 81.74, 117.82, 120.07]},
            index=pnd.date_range(dt(2016, 6, 1), dt(2016, 6, 1, 14), freq='H'),
            dtype=np.float64
        )
        z = Serie(
            datval=data['ZH'].to_frame(),
            code=code,
            provider=provider,
            varname='ZH',
        )
        h = Serie(
            datval=data['HH'].to_frame(),
            code=code,
            provider=provider,
            varname='HH',
        )
        q = Serie(
            datval=data['QH'].to_frame(),
            code=code,
            provider=provider,
            varname='QH',
        )
        # =====================================================================
        series = z.apply_ReservoirTable(table=bareme, assoc={'V': 'V'})
        self.assertEqual(len(series), 2)
        self.assertEqual(
            sorted(list(series.keys())),
            sorted([('K0000000', 'VH', None), ('K0000000', 'QH', None)])
        )
        assert_frame_equal(
            q.data_frame,
            series[('K0000000', 'QH', None)].data_frame,
            check_less_precise=1
        )
        # =====================================================================
        series = z.apply_ReservoirTable(table=bareme, assoc={'V': 'V'},
                                        sim=True)
        self.assertEqual(len(series), 2)
        self.assertEqual(
            sorted(list(series.keys())),
            sorted([('K0000000', 'VH', 'classic'),
                    ('K0000000', 'QH', 'classic')])
        )
        assert_frame_equal(
            q.data_frame,
            series[('K0000000', 'QH', 'classic')].data_frame,
            check_less_precise=1
        )
        # =====================================================================
        sh = z.apply_ReservoirZ0(reservoir=reservoir)
        assert_frame_equal(
            sh.data_frame,
            h.data_frame,
            check_less_precise=2
        )
        # =====================================================================
        # METTRE ICI z.apply_Reservoir
        sz = z.apply_Reservoir(
            reservoir=reservoir, tablename='classic', assoc={'V': 'V'})
        self.assertEqual(len(sz), 3)
        self.assertEqual(
            sorted(list(sz.keys())),
            sorted([('K0000000', 'HH', None),
                    ('K0000000', 'VH', None),
                    ('K0000000', 'QH', None)])
        )
        assert_frame_equal(
            q.data_frame,
            sz[('K0000000', 'QH', None)].data_frame,
            check_less_precise=1
        )
        assert_frame_equal(
            h.data_frame,
            sz[('K0000000', 'HH', None)].data_frame,
            check_less_precise=2
        )
        # =====================================================================
        # METTRE ICI q.apply_Reservoir
        sq = q.apply_Reservoir(
            reservoir=reservoir,
            tablename='classic',
            assoc={'V': 'V'}
        )
        self.assertEqual(len(sq), 3)
        self.assertEqual(
            sorted(list(sq.keys())),
            sorted([('K0000000', 'VH', None),
                    ('K0000000', 'ZH', None),
                    ('K0000000', 'HH', None)])
        )
        assert_frame_equal(
            z.data_frame,
            sq[('K0000000', 'ZH', None)].data_frame,
            check_less_precise=2
        )
        assert_frame_equal(
            h.data_frame,
            sq[('K0000000', 'HH', None)].data_frame,
            check_less_precise=-1
        )
        # =====================================================================
        # METTRE ICI h.apply_Reservoir
        sh = h.apply_Reservoir(
            reservoir=reservoir,
            tablename='classic',
            assoc={'V': 'V'}
        )
        self.assertEqual(len(sh), 3)
        self.assertEqual(
            sorted(list(sh.keys())),
            sorted([('K0000000', 'ZH', None),
                    ('K0000000', 'VH', None),
                    ('K0000000', 'QH', None)])
        )
        assert_frame_equal(
            z.data_frame,
            sh[('K0000000', 'ZH', None)].data_frame,
            check_less_precise=2
        )
        assert_frame_equal(
            q.data_frame,
            sh[('K0000000', 'QH', None)].data_frame,
            check_less_precise=1
        )
        # =====================================================================

    def test_sauquet(self):
        """
        test Sauquet
        """
        # =====================================================================
        random = Random('pyspc')
        # =====================================================================
        dates = pnd.date_range(dt(2000, 1, 1), dt(2023, 7, 1), freq='D')
        means = [1.430, 1.540, 1.700, 1.670, 1.320, 0.829,
                 0.380, 0.241, 0.324, 0.613, 1.140, 1.440]
        df = pnd.DataFrame(
            {'sigma': [1] * len(dates),
             'mu': [10 * means[d.month-1] for d in dates]},
            index=dates
        )
        df['normalvariate'] = df.apply(
            lambda row: random.normalvariate(row['mu'], row['sigma']), axis=1)
        serie = Serie(df['normalvariate'], code='MASTATION', varname='QJ')
#        print(serie.df)
#        print(serie.df)
        # =====================================================================
        norm, regimes = serie.regime_sauquet()
        valid_norm = pnd.DataFrame(
            {'MASTATION': [0.695665, 0.903210, 1.175648, 1.129008, 0.481835,
                           -0.416628, -1.228527, -1.480077, -1.333583,
                           -0.791470, 0.150593, 0.714325]},
            index=range(1, 13))
        valid_norm.index.name = 'month'
        valid_regimes = pnd.DataFrame(
            {'source': ['MASTATION']*12,
             'id_regime': range(12),
             'lbl_regime': ['nivo_glaciaire', 'nival', 'nival_transition',
                            'nivo_pluvial', 'pluvial_1', 'pluvial_2',
                            'pluvial_3', 'pluvial_4', 'pluvial_5', 'pluvial_6',
                            'pluvial_7', 'pluvial_8'],
             'distance': [6.368610030131479, 5.550304409204043,
                          5.221587778828889, 3.156538772299115,
                          1.0256567961026546, 1.2115383020901043,
                          0.8487595341026603, 1.8074353856575152,
                          2.0124516342098997, 1.273911529079778,
                          2.7013963884929217, 1.2201797801437109]},
            index=range(12)
        )
        assert_frame_equal(norm, valid_norm)
        assert_frame_equal(regimes, valid_regimes)
        # =====================================================================

    def test_sim2fcst(self):
        """
        Test méthode sim2fcst
        """
        # =====================================================================
        first_rtime = dt(2014, 11, 4, 1)
        last_rtime = dt(2014, 11, 4, 6)
        error_depth = 2
        max_ltime = 3
        # =====================================================================
        varname = 'QH'
        sim_serie = pnd.DataFrame(
            {(self.code, varname): [1, 2, 3, 4, 5, 6]},
            index=pnd.date_range(
                dt(2014, 11, 4, 1), dt(2014, 11, 4, 6), freq=td(hours=1)
            )
        )
        sim_serie = Serie(
            datval=sim_serie,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        ref_serie = pnd.DataFrame(
            {(self.code, varname): [3, 3, 3, 3, 3, 3]},
            index=pnd.date_range(
                dt(2014, 11, 4, 3), dt(2014, 11, 4, 8), freq=td(hours=1)
            )
        )
        ref_serie = Serie(
            datval=ref_serie,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        with self.assertRaises(ValueError):
            sim_serie.sim2fcst(ref=1)
        with self.assertRaises(ValueError):
            sim_serie.sim2fcst(ref=sim_serie, first_dtime='a')
        with self.assertRaises(ValueError):
            sim_serie.sim2fcst(ref=sim_serie, first_dtime=first_rtime,
                               last_dtime='b')
        with self.assertRaises(ValueError):
            sim_serie.sim2fcst(ref=sim_serie, first_dtime=first_rtime,
                               last_dtime=last_rtime, error_depth='1')
        with self.assertRaises(ValueError):
            sim_serie.sim2fcst(ref=sim_serie, first_dtime=first_rtime,
                               last_dtime=last_rtime, error_depth=error_depth,
                               max_ltime='3')
        # =====================================================================
        series = sim_serie.sim2fcst(
            ref=ref_serie,
            first_dtime=first_rtime,
            last_dtime=last_rtime,
            error_depth=error_depth,
            max_ltime=max_ltime
        )
        self.assertEqual(len(series), 4)
        self.assertIn(
            (self.code, varname, (dt(2014, 11, 4, 3), 'model', '002', None)),
            series)
        self.assertIn(
            (self.code, varname, (dt(2014, 11, 4, 4), 'model', '002', None)),
            series)
        self.assertIn(
            (self.code, varname, (dt(2014, 11, 4, 5), 'model', '002', None)),
            series)
        self.assertIn(
            (self.code, varname, (dt(2014, 11, 4, 6), 'model', '002', None)),
            series)
        # =====================================================================
        valid_4 = pnd.DataFrame(
            {('K0000000_2014110404_model_002', varname): [3, 4.5, 6, np.nan]},
            index=pnd.date_range(
                dt(2014, 11, 4, 4), dt(2014, 11, 4, 7), freq=td(hours=1)
            ),
            dtype=np.float32
        )
        valid_4.columns = [('K0000000_2014110404_model_002', varname)]
        key_4 = (self.code, varname,
                 (dt(2014, 11, 4, 4), 'model', '002', None))
        assert_frame_equal(series[key_4].data_frame, valid_4)
        # =====================================================================

    def test_socose(self):
        """
        Test méthode socose
        """
        # =====================================================================
        code = 'K0000000'
        varname = 'QH'
        df = pnd.DataFrame(
            [23.0, 25.5, 30.6, 53.6, 92.5, 119.4, 132.4, 137.1, 134.9, 129.6,
             121.2, 111.7, 102.1, 93.7, 84.6, 76.6, 69.5, 63.9, 59.1, 55.0,
             51.0, 48.1, 45.8, 43.5, 41.9],
            index=pnd.date_range(
                dt(2024, 3, 30, 6), dt(2024, 3, 31, 6), freq='H'),
            dtype=np.float64
        )
        valid = {('K0000000', 'QH'): {'d': 13, 'rxd': 1.2682701202590196}}
        # =====================================================================
        serie = Serie(datval=df, code=code, provider=self.provider,
                      varname=varname)
        values = serie.socose()
        self.assertDictEqual(values, valid)
        # =====================================================================
