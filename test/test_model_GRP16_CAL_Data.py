#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP_Data in pyspc.model.grp16

To run all tests just type:
    python -m unittest test_model_GRP16_CAL_Data

To run only a class test:
    python -m unittest test_model_GRP16_CAL_Data.TestGRP_Data

To run only a specific test:
    python -m unittest test_model_GRP16_CAL_Data.TestGRP_Data.test_init

"""
# Imports
from datetime import datetime as dt
import filecmp
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.model.grp16 import GRP_Data


class TestGRP_Data(unittest.TestCase):
    """
    GRP_Data class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp16', 'cal')
        self.valid_E = pnd.DataFrame(
            {'ETP(mm)': [0.481, 0.481, 0.452, 0.398, 0.325, 0.255,
                         0.144, 0.000, 0.000, 0.000, 0.000, 0.000]},
            index=pnd.date_range(
                dt(2017, 6, 13, 12),
                dt(2017, 6, 13, 23),
                freq='H'
            )
        )
        self.valid_E.index.name = 'AAAAMMJJHH'
        self.valid_P = pnd.DataFrame(
            {'P(mm)': [0.000, 0.000, 0.000, 23.800, 2.600, 2.200,
                       1.200, 4.900, 34.200, 0.200, 0.000, 0.000]},
            index=pnd.date_range(
                dt(2017, 6, 13, 12),
                dt(2017, 6, 13, 23),
                freq='H'
            )
        )
        self.valid_P.index.name = 'AAAAMMJJHH'
        self.valid_Q = pnd.DataFrame(
            {'Q(m3/s)': [0.570, 0.570, 0.560, 0.590, 0.580, 0.700,
                         11.200, 121.000, 203.000, 159.000, 92.800, 53.800]},
            index=pnd.date_range(
                dt(2017, 6, 13, 12),
                dt(2017, 6, 13, 23),
                freq='H'
            )
        )
        self.valid_Q.index.name = 'AAAAMMJJHH'
        self.valid_T = pnd.DataFrame(
            {'T(°C)': [25.700, 22.800, 22.600, 17.500, 18.800, 18.200,
                       17.400, 15.700, 16.300, 16.400, 17.000, 17.400]},
            index=pnd.date_range(
                dt(2017, 6, 13, 12),
                dt(2017, 6, 13, 23),
                freq='H'
            )
        )
        self.valid_T.index.name = 'AAAAMMJJHH'

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, '43091005_P.txt')
        reader = GRP_Data(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'P')
        self.assertEqual(reader.station, '43091005')

    def test_read_E(self):
        """
        Test de la lecture d'un fichier GRP Data - Cas E
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K0114030_EH.txt')
        reader = GRP_Data(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_E, df)
        # =====================================================================

    def test_read_P(self):
        """
        Test de la lecture d'un fichier GRP Data - Cas P
        """
        # =====================================================================
        filename = os.path.join(self.dirname, '43091005_P.txt')
        reader = GRP_Data(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_P, df)
        # =====================================================================

    def test_read_Q(self):
        """
        Test de la lecture d'un fichier GRP Data - Cas Q
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K0114030_Q.txt')
        reader = GRP_Data(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_Q, df)
        # =====================================================================

    def test_read_T(self):
        """
        Test de la lecture d'un fichier GRP Data - Cas T
        """
        # =====================================================================
        filename = os.path.join(self.dirname, '43091005_T.txt')
        reader = GRP_Data(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_T, df)
        # =====================================================================

    def test_write_E(self):
        """
        Test de l'écriture d'un fichier GRP_Data - Cas E
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K0114030_EH.txt')
        tmpfile = os.path.join('data', 'K0114030_EH.txt')
        writer = GRP_Data(filename=tmpfile)
        writer.write(data=self.valid_E)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_write_P(self):
        """
        Test de l'écriture d'un fichier GRP_Data - Cas P
        """
        # =====================================================================
        filename = os.path.join(self.dirname, '43091005_P.txt')
        tmpfile = os.path.join('data', '43091005_P.txt')
        writer = GRP_Data(filename=tmpfile)
        writer.write(data=self.valid_P)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_write_Q(self):
        """
        Test de l'écriture d'un fichier GRP_Data - Cas Q
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K0114030_Q.txt')
        tmpfile = os.path.join('data', 'K0114030_Q.txt')
        writer = GRP_Data(filename=tmpfile)
        writer.write(data=self.valid_Q)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_write_T(self):
        """
        Test de l'écriture d'un fichier GRP_Data - Cas T
        """
        # =====================================================================
        filename = os.path.join(self.dirname, '43091005_T.txt')
        tmpfile = os.path.join('data', '43091005_T.txt')
        writer = GRP_Data(filename=tmpfile)
        writer.write(data=self.valid_T)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_varname(self):
        """
        Test de la correspondance des variables selon GRP et selon SPC_LCI
        """
        valid = ['EH', 'P', 'Q', 'T']
        self.assertEqual(GRP_Data.get_varnames(), valid)
