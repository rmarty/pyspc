#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Reach in pyspc.core.reach

To run all tests just type:
    python -m unittest test_core_Reach

To run only a class test:
    python -m unittest test_core_Reach.TestReach

To run only a specific test:
    python -m unittest test_core_Reach.TestReach.test_init

"""
# Imports
from datetime import datetime as dt
import unittest

# Imports pyspc
from pyspc.core.reach import Reach


class TestReach(unittest.TestCase):
    """
    Reach class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """

    def test_init(self):
        """
        Test de la création de l'instance
        """
        code = 'LC110'
        name = 'Haut-bassin de la Loire'
        reach = Reach(code=code, name=name)
        self.assertEqual(reach.code, code)
        self.assertEqual(reach.name, name)
        self.assertIsNone(reach.status)
        self.assertTrue(isinstance(reach.status_dt, dt)
                        or reach.status_dt is None)
        self.assertIsNone(reach.locations)
