#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for _Basic_webservice in pyspc.webservice._basic

To run all tests just type:
    python -m unittest test_webservice_Basic

To run only a class test:
    python -m unittest test_webservice_Basic.TestBasic

To run only a specific test:
    python -m unittest test_webservice_Basic.TestBasic.test_init

"""
# Imports
import filecmp
import os
import unittest
from unittest import mock
import warnings

# Imports pyspc
from pyspc.webservice._basic import _Basic_webservice


class TestBasic(unittest.TestCase):
    """
    Basic class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'webservice', 'basic')
        warnings.filterwarnings(action="ignore", category=ResourceWarning)

    def test_init(self):
        """
        Test de la création de l'instance
        """
        hostname = 'https://donneespubliques.meteofrance.fr'
        proxies = {'x': 'y'}
        timeout = 10
        report = _Basic_webservice(hostname=hostname, proxies=proxies,
                                   timeout=timeout)
        self.assertEqual(report.hostname, hostname)
        self.assertDictEqual(report.proxies, proxies)
        self.assertEqual(report.timeout, timeout)
        self.assertIsNone(report.url)
        self.assertIsNone(report.filename)
        self.assertIsNone(report.verify)

    @mock.patch('pyspc.webservice._basic._Basic_webservice'
                '.retrieve_byrequests')
    @mock.patch('pyspc.webservice._basic._Basic_webservice'
                '.retrieve_byurllib')
    def test_retrieve(self, mock_u, mock_r):
        """
        Test retrieve
        """
        # =====================================================================
        hostname = 'https://donneespubliques.meteofrance.fr'
        proxies = {'x': 'y'}
        timeout = 10
        report = _Basic_webservice(hostname=hostname, proxies=proxies,
                                   timeout=timeout)
        # =====================================================================
        report.retrieve(engine='urllib')
        mock_u.assert_called()
        mock_r.assert_not_called()
        mock_u.reset_mock()
        mock_r.reset_mock()
        # =====================================================================
        report.retrieve(engine='requests')
        mock_u.assert_not_called()
        mock_r.assert_called()
        # =====================================================================

    def test_retrieve_byrequests(self):
        """
        Test retrieve - by requests
        """
        url = r'https://donneespubliques.meteofrance.fr/donnees_libres/'\
            'bulletins/BCMR/BCMR_03_201706.pdf'
        filename = os.path.join(self.dirname, 'BCMR_03_201706.pdf')
        report = _Basic_webservice()
        report.url = url
        report.filename = os.path.join('data', os.path.basename(filename))
        report.retrieve_byrequests()
        self.assertTrue(filecmp.cmp(filename, report.filename))
        os.remove(report.filename)

    def test_retrieve_byurllib(self):
        """
        Test retrieve - by urllib
        """
        url = r'https://donneespubliques.meteofrance.fr/donnees_libres/'\
            'bulletins/BCMR/BCMR_03_201706.pdf'
        filename = os.path.join(self.dirname, 'BCMR_03_201706.pdf')
        report = _Basic_webservice()
        report.url = url
        report.filename = os.path.join('data', os.path.basename(filename))
        report.retrieve_byurllib()
        self.assertTrue(filecmp.cmp(filename, report.filename))
        os.remove(report.filename)
