#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary onlineReport

To run all tests just type:
    python -m unittest test_bin_onlineReport

To run only a class test:
    python -m unittest test_bin_onlineReport.Test_onlineReport

To run only a specific test:
    python -m unittest test_bin_onlineReport.Test_onlineReport.test_monthly

"""
# Imports
import filecmp
import glob
import os
import sys
import unittest

from pyspc.binutils.args import onlineReport as _args
from pyspc.binutils.runs import onlineReport as _runs
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_onlineReport(unittest.TestCase):
    """
    onlineReport bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'onlineReport')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dirname2 = os.path.join('data', 'webservice', 'report')
        cls.dirname3 = os.path.join('data', 'metadata', 'shyreg')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.stations_list_file = None

    def tearDown(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)

    def test_inrae_explore2070(self):
        """
        Test Fiche bassin
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-s', '652',
            '-O', self.dir_out,
            '-t', 'inrae_explore2070'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, '652')
        self.assertEqual(args.varname, None)
        self.assertEqual(args.data_type, 'inrae_explore2070')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filename = 'explore2070_652.pdf'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dirname2, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement du document de type '{t}' vers le "\
            "répertoire {O} pour l'entité {s}".format(
                t=args.data_type, s=args.station_name, O=args.output_dir)
        # =====================================================================

    def test_inrae_explore2070_list(self):
        """
        Test Fiche bassin
        """
        # =====================================================================
        self.stations_list_file = os.path.join(
            self.dir_in, 'liste_explore2070.txt')
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-l',  self.stations_list_file,
            '-O', self.dir_out,
            '-t', 'inrae_explore2070',
            '-v'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file,  self.stations_list_file)
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.data_type, 'inrae_explore2070')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        for code in ['653', '654', '655']:
            filename = f'explore2070_{code}.pdf'
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, filename),
                os.path.join(self.dirname2, filename),
            ))
            os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement du document de type '{t}' vers le "\
            "répertoire {O} pour les entités listé dans {l}".format(
                t=args.data_type, l=args.stations_list_file, O=args.output_dir)
        # =====================================================================

    def test_inrae_hydroclim(self):
        """
        Test Fiche bassin
        """
        code = 'K153301001'
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-s', code,
            '-O', self.dir_out,
            '-t', 'inrae_hydroclim'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, code)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.data_type, 'inrae_hydroclim')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filename = f'{code}_INRAE_BDD-HydroClim_fact_sheet_FR.png'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dirname2, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement du document de type '{t}' vers le "\
            "répertoire {O} pour l'entité {s}".format(
                t=args.data_type, s=args.station_name, O=args.output_dir)
        # =====================================================================

    def test_inrae_shyreg(self):
        """
        Test Fiche SHYREG - Exutoire BNBV
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-O', self.dir_out,
            '-t', 'inrae_shyreg',
            '-s', 'LO2228'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'LO2228')
        self.assertEqual(args.varname, None)
        self.assertEqual(args.data_type, 'inrae_shyreg')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filename = 'BNBV_LO2228.pdf'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dirname2, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        for f in ['BNBV_LO2228.csv', 'BNBV_LO2228_interv80.csv']:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, f),
                os.path.join(self.dirname3, f),
            ))
            os.remove(os.path.join(self.dir_out, f))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement du document de type '{t}' vers le "\
            "répertoire {O} pour l'entité {s}".format(
                t=args.data_type, s=args.station_name, O=args.output_dir)
        # =====================================================================

    def test_inrae_shyreg_bnbv(self):
        """
        Test Fiche SHYREG - Exutoire BNBV
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-O', self.dir_out,
            '-t', 'inrae_shyreg_bnbv',
            '-s', 'LO2228'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'LO2228')
        self.assertEqual(args.varname, None)
        self.assertEqual(args.data_type, 'inrae_shyreg_bnbv')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filename = 'BNBV_LO2228.pdf'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dirname2, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        for f in ['BNBV_LO2228.csv', 'BNBV_LO2228_interv80.csv']:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, f),
                os.path.join(self.dirname3, f),
            ))
            os.remove(os.path.join(self.dir_out, f))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement du document de type '{t}' vers le "\
            "répertoire {O} pour l'entité {s}".format(
                t=args.data_type, s=args.station_name, O=args.output_dir)
        # =====================================================================

    def test_inrae_shyreg_hydro(self):
        """
        Test Fiche SHYREG - Station Hydro
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-O', self.dir_out,
            '-t', 'inrae_shyreg_hydro',
            '-s', 'K0403010'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K0403010')
        self.assertEqual(args.varname, None)
        self.assertEqual(args.data_type, 'inrae_shyreg_hydro')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filename = 'HYDRO_K0403010.pdf'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dirname2, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        for f in ['HYDRO_K0403010.csv', 'HYDRO_K0403010_interv80.csv']:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dir_out, f),
                os.path.join(self.dirname3, f),
            ))
            os.remove(os.path.join(self.dir_out, f))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement du document de type '{t}' vers le "\
            "répertoire {O} pour l'entité {s}".format(
                t=args.data_type, s=args.station_name, O=args.output_dir)
        # =====================================================================

    def test_mf_monthlyreport(self):
        """
        Test Rapport climatologique mensuel
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-s', '01',
            '-O', self.dir_out,
            '-t', 'mf_monthlyreport',
            '-r', '20200612'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, '20200612')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, '01')
        self.assertEqual(args.varname, None)
        self.assertEqual(args.data_type, 'mf_monthlyreport')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filename = 'BCMR_01_202006.pdf'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dirname2, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement du document de type '{t}' vers le "\
            "répertoire {O} pour l'entité {s} à la date {r}".format(
                t=args.data_type, s=args.station_name, O=args.output_dir,
                r=args.runtime)
        # =====================================================================

    def test_mf_dailyreport(self):
        """
        Test Rapport météo journalier
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-O', self.dir_out,
            '-t', 'mf_dailyreport',
            '-r', '20170613'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, '20170613')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'FR')
        self.assertEqual(args.varname, None)
        self.assertEqual(args.data_type, 'mf_dailyreport')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filename = '20170613.pdf'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dirname2, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement du document de type '{t}' vers le "\
            "répertoire {O} pour l'entité {s} (forcé par pyspc) à la "\
            "date {r}".format(
                t=args.data_type, s=args.station_name, O=args.output_dir,
                r=args.runtime)
        # =====================================================================

    def test_mf_station(self):
        """
        Test Rapport météo journalier
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-O', self.dir_out,
            '-t', 'mf_station',
            '-s', '07154005'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, '07154005')
        self.assertEqual(args.varname, None)
        self.assertEqual(args.data_type, 'mf_station')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filename = 'fiche_07154005.pdf'
        self.assertTrue(filecmp.cmp(
             os.path.join(self.dir_out, filename),
             os.path.join(self.dirname2, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement du document de type '{t}' vers le "\
            "répertoire {O} pour l'entité {s} à la date {r}".format(
                t=args.data_type, s=args.station_name, O=args.output_dir,
                r=args.runtime)
        # =====================================================================

    def test_mf_clim(self):
        """
        Test fiche station climatologique PDF
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-O', self.dir_out,
            '-t', 'mf_clim',
            '-s', '03155003'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, '03155003')
        self.assertEqual(args.varname, None)
        self.assertEqual(args.data_type, 'mf_clim')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filename = 'FICHECLIM_03155003.pdf'
#        self.assertTrue(filecmp.cmp(
#            os.path.join(self.dir_out, filename),
#            os.path.join(self.dirname2, filename),
#        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement du document de type '{t}' vers le "\
            "répertoire {O} pour l'entité {s}".format(
                t=args.data_type, s=args.station_name, O=args.output_dir)
        # =====================================================================

    def test_mf_climdata(self):
        """
        Test fiche station climatologique DATA
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-O', self.dir_out,
            '-t', 'mf_climdata',
            '-s', '03155003'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, '03155003')
        self.assertEqual(args.varname, None)
        self.assertEqual(args.data_type, 'mf_climdata')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filename = 'FICHECLIM_03155003.data'
#        self.assertTrue(filecmp.cmp(
#            os.path.join(self.dir_out, filename),
#            os.path.join(self.dirname2, filename),
#        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement du document de type '{t}' vers le "\
            "répertoire {O} pour l'entité {s}".format(
                t=args.data_type, s=args.station_name, O=args.output_dir)
        # =====================================================================

    def test_mf_warning(self):
        """
        Test archive vigilance
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-O', self.dir_out,
            '-t', 'mf_warning',
            '-r', '20191123'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, '20191123')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.data_type, 'mf_warning')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filename = 'vigilance_2019_11_23.zip'
        self.assertTrue(os.path.exists(os.path.join(self.dir_out, filename)))
#        self.assertTrue(filecmp.cmp(
#            os.path.join(self.dir_out, filename),
#            os.path.join(self.dirname2, filename),
#        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement du document de type '{t}' vers le "\
            "répertoire {O} à la date {r}".format(
                t=args.data_type, O=args.output_dir, r=args.runtime)
        # =====================================================================

    def test_vigicrues_fcst(self):
        """
        Test Vigicrues Fcst
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-O', self.dir_out,
            '-t', 'vigicrues_fcst',
            '-s', 'K118001010',
            '-n', 'Q'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K118001010')
        self.assertEqual(args.varname, 'Q')
        self.assertEqual(args.data_type, 'vigicrues_fcst')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filenames = glob.glob(os.path.join(
            self.dir_out, 'vigicrues_fcst_*.json'))
        self.assertGreater(len(filenames), 0)
        for f in filenames:
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement des informations Vigicrues de type "\
            "'{t}' vers le répertoire {O} pour l'entité {s} et la "\
            "grandeur {n}".format(
                t=args.data_type, s=args.station_name, O=args.output_dir,
                n=args.varname)
        # =====================================================================

    def test_vigicrues_obs(self):
        """
        Test Vigicrues Obs
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-O', self.dir_out,
            '-t', 'vigicrues_obs',
            '-s', 'K118001010',
            '-n', 'Q'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K118001010')
        self.assertEqual(args.varname, 'Q')
        self.assertEqual(args.data_type, 'vigicrues_obs')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filenames = glob.glob(os.path.join(
            self.dir_out, 'vigicrues_obs_*.json'))
        self.assertGreater(len(filenames), 0)
        for f in filenames:
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement des informations Vigicrues de type "\
            "'{t}' vers le répertoire {O} pour l'entité {s} et la grandeur "\
            "{n}".format(
                t=args.data_type, s=args.station_name, O=args.output_dir,
                n=args.varname)
        # =====================================================================

    def test_vigicrues_loc(self):
        """
        Test Vigicrues Location
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-O', self.dir_out,
            '-t', 'vigicrues_loc',
            '-s', 'K118001010'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K118001010')
        self.assertEqual(args.varname, None)
        self.assertEqual(args.data_type, 'vigicrues_loc')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filenames = glob.glob(os.path.join(
            self.dir_out, 'vigicrues_loc_*.json'))
        self.assertGreater(len(filenames), 0)
        for f in filenames:
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement des informations Vigicrues de type "\
            "'{t}' vers le répertoire {O} pour l'entité {s}.".format(
                t=args.data_type, s=args.station_name, O=args.output_dir)
        # =====================================================================
#
#    def test_vigicrues_reach(self):
#        """
#        Test Vigicrues Reaches
#        """
#        # =====================================================================
#        processArgs = [
#            'python',
#            '../bin/onlineReport.py',
#            '-O', self.dir_out,
#            '-t', 'vigicrues_reach'
#        ]
#        # =====================================================================
#        cline = ' '.join(processArgs).replace('python', '').strip()
#        sys.argv = cline.split(' ')
#        with captured_output():
#            args = _args.onlineReport()
#        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.runtime, None)
#        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.station_name, None)
#        self.assertEqual(args.varname, None)
#        self.assertEqual(args.data_type, 'vigicrues_reach')
#        # =====================================================================
#        with captured_output():
#            _ = _runs.onlineReport(args)
#        filenames = glob.glob(os.path.join(
#            self.dir_out, 'vigicrues_reaches_*.json'))
#        self.assertGreater(len(filenames), 0)
#        for f in filenames:
#            os.remove(f)
#        # =====================================================================
##        print(cline)
#        self.cline = cline
#        self.label = "Téléchargement des informations Vigicrues de type "\
#            "'{t}' vers le répertoire {O}.".format(
#                t=args.data_type, O=args.output_dir)
#        # =====================================================================

    def test_vigicrues_sandre(self):
        """
        Test Vigicrues Obs Sandre
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-O', self.dir_out,
            '-t', 'vigicrues_sandre',
            '-s', 'K118001010',
            '-n', 'Q'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K118001010')
        self.assertEqual(args.varname, 'Q')
        self.assertEqual(args.data_type, 'vigicrues_sandre')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filenames = glob.glob(os.path.join(
            self.dir_out, 'vigicrues_sandre_*.xml'))
        self.assertGreater(len(filenames), 0)
        for f in filenames:
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement des informations Vigicrues de type "\
            "'{t}' vers le répertoire {O} pour l'entité {s} et la grandeur "\
            "{n}".format(
                t=args.data_type, s=args.station_name, O=args.output_dir,
                n=args.varname)
        # =====================================================================

    def test_vigicrues1_geoinfo(self):
        """
        Test téléchargement de fichier - vigicrues-1_geoinfo
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/onlineReport.py',
            '-O', self.dir_out,
            '-t', 'vigicrues-1_geoinfo'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.onlineReport()
        self.assertEqual(args.stations_list_file, None)
#        self.assertEqual(args.runtime, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.data_type, 'vigicrues-1_geoinfo')
        # =====================================================================
        with captured_output():
            _ = _runs.onlineReport(args)
        filenames = glob.glob(os.path.join(
            self.dir_out, 'vigicrues-1_geoinfo_*.geojson'))
        self.assertGreater(len(filenames), 0)
        for f in filenames:
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Téléchargement des informations Vigicrues "\
            "(service 1.1) de type '{t}' vers le répertoire {O} au format"\
            " geoJSON.".format(t=args.data_type, O=args.output_dir)
        # =====================================================================
#
#    def test_vigicrues1_info(self):
#        """
#        Test téléchargement de fichier - vigicrues-1_info
#        """
#        # =====================================================================
#        processArgs = [
#            'python',
#            '../bin/onlineReport.py',
#            '-O', self.dir_out,
#            '-t', 'vigicrues-1_info',
#            '-s', '30,5'
#        ]
#        # =====================================================================
#        cline = ' '.join(processArgs).replace('python', '').strip()
#        sys.argv = cline.split(' ')
#        with captured_output():
#            args = _args.onlineReport()
#        self.assertEqual(args.stations_list_file, None)
##        self.assertEqual(args.runtime, None)
#        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.station_name, '30,5')
#        self.assertEqual(args.varname, None)
#        self.assertEqual(args.data_type, 'vigicrues-1_info')
#        # =====================================================================
#        with captured_output():
#            _ = _runs.onlineReport(args)
#        filenames = glob.glob(os.path.join(
#            self.dir_out, 'vigicrues-1_info*.json'))
#        self.assertGreater(len(filenames), 0)
#        for f in filenames:
#            os.remove(f)
#        # =====================================================================
##        print(cline)
#        self.cline = cline
#        self.label = "Téléchargement au format json des informations "\
#            "Vigicrues (service 1.1) de type '{t}' pour l'entité '{s}' "\
#            "(1e valeur: code, 2e valeur: type d'entité) vers le répertoire "\
#            "{O}.".format(t=args.data_type, O=args.output_dir,
#                          s=args.station_name)
#        # =====================================================================
#
#    def test_vigicrues1_domain(self):
#        """
#        Test téléchargement de fichier - vigicrues-1_domain
#        """
#        # =====================================================================
#        processArgs = [
#            'python',
#            '../bin/onlineReport.py',
#            '-O', self.dir_out,
#            '-t', 'vigicrues-1_domain',
#            '-s', '30,5'
#        ]
#        # =====================================================================
#        cline = ' '.join(processArgs).replace('python', '').strip()
#        sys.argv = cline.split(' ')
#        with captured_output():
#            args = _args.onlineReport()
#        self.assertEqual(args.stations_list_file, None)
##        self.assertEqual(args.runtime, None)
#        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.station_name, '30,5')
#        self.assertEqual(args.varname, None)
#        self.assertEqual(args.data_type, 'vigicrues-1_domain')
#        # =====================================================================
#        with captured_output():
#            _ = _runs.onlineReport(args)
#        filenames = glob.glob(os.path.join(
#            self.dir_out, 'vigicrues-1_domain*.json'))
#        self.assertGreater(len(filenames), 0)
#        for f in filenames:
#            os.remove(f)
#        # =====================================================================
##        print(cline)
#        self.cline = cline
#        self.label = "Téléchargement au format json des informations "\
#            "Vigicrues (service 1.1) de type '{t}' pour l'entité '{s}' "\
#            "(1e valeur: code, 2e valeur: type d'entité) vers le répertoire "\
#            "{O}.".format(t=args.data_type, O=args.output_dir,
#                          s=args.station_name)
#        # =====================================================================
#
#    def test_vigicrues1_reach(self):
#        """
#        Test téléchargement de fichier - vigicrues-1_reach
#        """
#        # =====================================================================
#        processArgs = [
#            'python',
#            '../bin/onlineReport.py',
#            '-O', self.dir_out,
#            '-t', 'vigicrues-1_reach',
#            '-s', 'LC165,8'
#        ]
#        # =====================================================================
#        cline = ' '.join(processArgs).replace('python', '').strip()
#        sys.argv = cline.split(' ')
#        with captured_output():
#            args = _args.onlineReport()
#        self.assertEqual(args.stations_list_file, None)
##        self.assertEqual(args.runtime, None)
#        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.station_name, 'LC165,8')
#        self.assertEqual(args.varname, None)
#        self.assertEqual(args.data_type, 'vigicrues-1_reach')
#        # =====================================================================
#        with captured_output():
#            _ = _runs.onlineReport(args)
#        filenames = glob.glob(os.path.join(
#            self.dir_out, 'vigicrues-1_reach*.json'))
#        self.assertGreater(len(filenames), 0)
#        for f in filenames:
#            os.remove(f)
#        # =====================================================================
##        print(cline)
#        self.cline = cline
#        self.label = "Téléchargement au format json des informations "\
#            "Vigicrues (service 1.1) de type '{t}' pour l'entité '{s}' "\
#            "(1e valeur: code, 2e valeur: type d'entité) vers le répertoire "\
#            "{O}.".format(t=args.data_type, O=args.output_dir,
#                          s=args.station_name)
#        # =====================================================================
#
#    def test_vigicrues1_loc(self):
#        """
#        Test téléchargement de fichier - vigicrues-1_loc
#        """
#        # =====================================================================
#        processArgs = [
#            'python',
#            '../bin/onlineReport.py',
#            '-O', self.dir_out,
#            '-t', 'vigicrues-1_loc',
#            '-s', 'K400001010,7'
#        ]
#        # =====================================================================
#        cline = ' '.join(processArgs).replace('python', '').strip()
#        sys.argv = cline.split(' ')
#        with captured_output():
#            args = _args.onlineReport()
#        self.assertEqual(args.stations_list_file, None)
##        self.assertEqual(args.runtime, None)
#        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.station_name, 'K400001010,7')
#        self.assertEqual(args.varname, None)
#        self.assertEqual(args.data_type, 'vigicrues-1_loc')
#        # =====================================================================
#        with captured_output():
#            _ = _runs.onlineReport(args)
#        filenames = glob.glob(os.path.join(
#            self.dir_out, 'vigicrues-1_loc*.json'))
#        self.assertGreater(len(filenames), 0)
#        for f in filenames:
#            os.remove(f)
#        # =====================================================================
##        print(cline)
#        self.cline = cline
#        self.label = "Téléchargement au format json des informations "\
#            "Vigicrues (service 1.1) de type '{t}' pour l'entité '{s}' "\
#            "(1e valeur: code, 2e valeur: type d'entité) vers le répertoire "\
#            "{O}.".format(t=args.data_type, O=args.output_dir,
#                          s=args.station_name)
#        # =====================================================================
