#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for RT_Data in pyspc.model.otamin18

To run all tests just type:
    python -m unittest test_model_OTAMIN18_RTData

To run only a class test:
    python -m unittest test_model_OTAMIN18_RTData.TestOtamin_RTData

To run only a specific test:
    python -m unittest test_model_OTAMIN18_RTData.TestOtamin_RTData.test_init

"""
# Imports
from datetime import datetime as dt
import filecmp
import numpy as np
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.model.otamin18 import RT_Data


class TestOtamin_RTData(unittest.TestCase):
    """
    Otamin_RTData class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        # =====================================================================
        self.dirname = os.path.join('data', 'model', 'otamin16')
#        self.dirname = os.path.join('data', 'model', 'otamin18')
        # =====================================================================
        key1 = ('A6701210', 'Q', '57gGRPd000_2001',
                '57gGRPd000', '2001', '03-02-2020 12:00')
        key2 = ('A6701210', 'Q', '57gGRPd000_2002',
                '57gGRPd000', '2002', '03-02-2020 12:00')
        key3 = ('A6701210', 'Q', '57gGRPd000_2003',
                '57gGRPd000', '2003', '03-02-2020 12:00')
        self.valid_fcst = pnd.DataFrame({
            key1: [41.689, 43.426, 44.285, 44.395, 44.109, 43.340, 42.448,
                   41.470, 40.312, 39.044, 37.754, 36.612, 36.184, 36.708,
                   38.304, 40.523, 43.099, 45.781, 48.223, 50.064, 50.648,
                   50.161, 48.963, 47.745],
            key2: [41.614, 43.161, 43.699, 43.438, 42.775, 41.659, 40.459,
                   39.230, 37.941, 36.614, 35.292, 34.060, 33.290, 33.133,
                   33.660, 34.575, 35.706, 36.908, 37.971, 38.681, 38.654,
                   38.023, 37.008, 36.014],
            key3: [41.784, 43.760, 45.025, 45.649, 45.949, 45.793, 45.491,
                   45.023, 44.198, 43.123, 41.936, 40.888, 40.909, 42.427,
                   45.723, 50.083, 55.065, 60.224, 64.957, 68.613, 70.013,
                   69.503, 67.750, 66.030],
            },
            index=pnd.date_range(
                dt(2020, 2, 3, 13),
                dt(2020, 2, 4, 12),
                freq='H'
            )
        )
        self.valid_fcst.columns.set_names(
            ['Stations', 'Grandeurs', 'IdSeries',
             '# Modeles', '# Scenarios', '# DtDerObs'],
            level=[0, 1, 2, 3, 4, 5],
            inplace=True
        )
        # =====================================================================
        key1 = ('A6701210', 'RR', '57gGRPd000_2001',
                '57gGRPd000', '2001', '03-02-2020 12:00')
        key2 = ('A6701210', 'TA', '57gGRPd000_2001',
                '57gGRPd000', '2001', '03-02-2020 12:00')
        key3 = ('A6701210', 'RR', '57gGRPd000_2002',
                '57gGRPd000', '2002', '03-02-2020 12:00')
        key4 = ('A6701210', 'TA', '57gGRPd000_2002',
                '57gGRPd000', '2002', '03-02-2020 12:00')
        key5 = ('A6701210', 'RR', '57gGRPd000_2003',
                '57gGRPd000', '2003', '03-02-2020 12:00')
        key6 = ('A6701210', 'TA', '57gGRPd000_2003',
                '57gGRPd000', '2003', '03-02-2020 12:00')
        self.valid_da = pnd.DataFrame({
            key1: [1.100, 1.100, 1.100, 0.200, 0.200, 0.200, 0.300, 0.300,
                   0.300, 0.400, 0.400, 0.400, 3.600, 3.600, 3.600, 0.800,
                   0.800, 0.800, 0.400, 0.400, 0.400, 0.700, 0.700, 0.700],
            key2: [np.nan]*24,
            key3: [0.700, 0.700, 0.700, 0.200, 0.200, 0.200, 0.200, 0.200,
                   0.200, 0.200, 0.200, 0.200, 2.400, 2.400, 2.400, 0.500,
                   0.500, 0.500, 0.200, 0.200, 0.200, 0.500, 0.500, 0.500],
            key4: [np.nan]*24,
            key5: [1.600, 1.600, 1.600, 0.400, 0.400, 0.400, 0.400, 0.400,
                   0.400, 0.600, 0.600, 0.600, 5.400, 5.400, 5.400, 1.100,
                   1.100, 1.100, 0.600, 0.600, 0.600, 1.100, 1.100, 1.100],
            key6: [np.nan]*24,
            },
            index=pnd.date_range(
                dt(2020, 2, 3, 13),
                dt(2020, 2, 4, 12),
                freq='H'
            )
        )
        self.valid_da.columns.set_names(
            ['Stations', 'Grandeurs', 'IdSeries',
             '# Modeles', '# Scenarios', '# DtDerObs'],
            level=[0, 1, 2, 3, 4, 5],
            inplace=True
        )
        # =====================================================================
        key1 = ('A6701210', 'Q', '57gGRPd000_2001_-1',
                '57gGRPd000', '2001', '03-02-2020 12:00', '-1')
        key2 = ('A6701210', 'Q', '57gGRPd000_2001_10',
                '57gGRPp000', '2001', '03-02-2020 12:00', '10')
        key3 = ('A6701210', 'Q', '57gGRPd000_2001_50',
                '57gGRPp000', '2001', '03-02-2020 12:00', '50')
        key4 = ('A6701210', 'Q', '57gGRPd000_2001_90',
                '57gGRPp000', '2001', '03-02-2020 12:00', '90')
        self.valid_trend = pnd.DataFrame({
            key1: [41.689, 43.426, 44.285, 44.395, 44.109, 43.340, 42.448,
                   41.470, 40.312, 39.044, 37.754, 36.612, 36.184, 36.708,
                   38.304, 40.523, 43.099, 45.781, 48.223, 50.064, 50.648,
                   50.161, 48.963, 47.745],
            key2: [40.329, 40.748, 40.268, 39.131, 37.651, 35.788, 34.241,
                   32.661, 30.979, 29.568, 28.169, 26.907, 26.425, 26.638,
                   27.619, 29.032, 30.678, 32.376, 34.042, 35.280, 35.628,
                   35.223, 34.320, 33.407],
            key3: [41.769, 43.599, 44.553, 44.658, 44.364, 43.584, 42.599,
                   41.531, 40.287, 38.977, 37.648, 36.470, 36.022, 36.521,
                   38.086, 40.268, 42.802, 45.438, 47.823, 49.609, 50.147,
                   49.625, 48.401, 47.158],
            key4: [42.977, 46.273, 48.725, 50.069, 50.962, 51.269, 51.050,
                   50.691, 50.070, 49.208, 48.272, 47.480, 47.263, 48.290,
                   50.748, 54.066, 57.906, 61.938, 65.412, 68.085, 69.058,
                   68.571, 67.106, 65.605],
            },
            index=pnd.date_range(
                dt(2020, 2, 3, 13),
                dt(2020, 2, 4, 12),
                freq='H'
            )
        )
        self.valid_trend.columns.set_names(
            ['Stations', 'Grandeurs', 'IdSeries',
             '# Modeles', '# Scenarios', '# DtDerObs', '# Probas'],
            level=[0, 1, 2, 3, 4, 5, 6],
            inplace=True
        )
        # =====================================================================

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'GRP_B_20200911_1515_2.prv')
        datatype = 'fcst'
        reader = RT_Data(
            filename=filename,
            datatype=datatype
        )
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.datatype, datatype)

    def test_read_fcst(self):
        """
        Test de la lecture - CAS PREVISION OTAMIN
        """
        filename = os.path.join(self.dirname, 'GRP_B_20200911_1515_2.prv')
        datatype = 'fcst'
        reader = RT_Data(
            filename=filename,
            datatype=datatype
        )
        df = reader.read()
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(self.valid_fcst, df)

    def test_read_fcst_DA(self):
        """
        Test de la lecture - CAS PREVISION OTAMIN - CAS RR,TA
        """
        filename = os.path.join(self.dirname, 'GRP_B_20200911_1515_DA_2.prv')
        datatype = 'fcst'
        reader = RT_Data(
            filename=filename,
            datatype=datatype
        )
        df = reader.read()
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(self.valid_da, df)

    def test_read_trend(self):
        """
        Test de la lecture - CAS TENDANCE OTAMIN
        """
        filename = os.path.join(self.dirname, 'GRP_B_20200203_1200_2.prv')
        datatype = 'trend'
        reader = RT_Data(
            filename=filename,
            datatype=datatype
        )
        df = reader.read()
        self.assertIsInstance(df, pnd.DataFrame)
        assert_frame_equal(self.valid_trend, df)

    def test_write_fcst(self):
        """
        Test de la lecture - CAS PREVISION OTAMIN
        """
        df = self.valid_fcst
        filename = os.path.join(self.dirname, 'GRP_B_20200911_1515_2.prv')
        datatype = 'fcst'
        tmp_file = os.path.join('data', 'GRP_B_20200911_1515_2.prv')
        writer = RT_Data(
            filename=tmp_file,
            datatype=datatype
        )
        writer.write(data=df)
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)

    def test_write_fcst_DA(self):
        """
        Test de la lecture - CAS PREVISION OTAMIN - CAS RR,TA
        """
        df = self.valid_da
        filename = os.path.join(self.dirname, 'GRP_B_20200911_1515_DA_2.prv')
        datatype = 'fcst'
        tmp_file = os.path.join('data', 'GRP_B_20200911_1515_DA_2.prv')
        writer = RT_Data(
            filename=tmp_file,
            datatype=datatype
        )
        writer.write(data=df)
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)

    def test_write_trend(self):
        """
        Test de la lecture - CAS TENDANCE OTAMIN
        """
        df = self.valid_trend
        filename = os.path.join(self.dirname, 'GRP_B_20200203_1200_2.prv')
        datatype = 'trend'
        tmp_file = os.path.join('data', 'GRP_B_20200203_1200_2.prv')
        writer = RT_Data(
            filename=tmp_file,
            datatype=datatype
        )
        writer.write(data=df)
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)

    def test_datatypes(self):
        """
        Test des types de fichiers PRV OTAMIN traités par pyspc
        """
        valid = ['fcst', 'trend']
        self.assertEqual(valid, RT_Data.get_types())
