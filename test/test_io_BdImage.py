#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_BdImage in pyspc.io.lamedo

To run all tests just type:
    python -m unittest test_data_BdImage

To run only a class test:
    python -m unittest test_data_BdImage.TestBdImage

To run only a specific test:
    python -m unittest test_data_BdImage.TestBdImage.test_init

"""
# Imports
from datetime import datetime as dt
import os
import unittest

from pyspc.core.series import Series
from pyspc.io.lamedo import read_BdImage


class TestBdImage(unittest.TestCase):
    """
    BdImage_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'lamedo')

    def test_read_getObsStatsByZones(self):
        """
        Test de lecture - getObsStatsByZones
        """
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getObsStatsByZones_antilope-j1-rr_000100.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in ['med', 'moy', 'q40', 'min', 'max', 'etyp', 'cvar', 'q90',
                  'q20', 'q80', 'q30', 'q10', 'q60', 'q70']:
            self.assertIn(
                ('LO8060', 'PH', 'antilope,j1,rr,{}'.format(s)), series)
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getObsStatsByZones_antilope-j1-rr_010000.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in ['med', 'moy', 'q40', 'min', 'max', 'etyp', 'cvar', 'q90',
                  'q20', 'q80', 'q30', 'q10', 'q60', 'q70']:
            self.assertIn(
                ('LO8060', 'PJ', 'antilope,j1,rr,{}'.format(s)), series)
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getObsStatsByZones_antilope-j1-rr_010000_RATIO.xml')
        series = read_BdImage(filename=filename,
                              ratio_image=0.8, ratio_stats=0.8)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in ['med', 'moy', 'q40', 'min', 'max', 'etyp', 'cvar', 'q90',
                  'q20', 'q80', 'q30', 'q10', 'q60', 'q70']:
            self.assertIn(
                ('LO8060', 'PJ', 'antilope,j1,rr,{}'.format(s)), series)
        # =====================================================================

    def test_read_getObsStatsByPixels(self):
        """
        Test de lecture - getObsStatsByPixels
        """
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getObsStatsByPixels_sim-rr-total_010000.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in ['med', 'moy', 'q40', 'min', 'max', 'etyp', 'cvar', 'q90',
                  'q20', 'q80', 'q30', 'q10', 'q60', 'q70']:
            self.assertIn(
                ('pixels', 'PJ', 'sim,rr,total,{}'.format(s)), series)
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getObsStatsByPixels_sim-t-t.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in ['med', 'moy', 'q40', 'min', 'max', 'etyp', 'cvar', 'q90',
                  'q20', 'q80', 'q30', 'q10', 'q60', 'q70']:
            self.assertIn(
                ('pixels', 'TI', 'sim,t,t,{}'.format(s)), series)
#        for s in series.values():
#            print(s)
#            print(s.data_frame)
        # =====================================================================

    def test_read_getObsValuesByBBox(self):
        """
        Test de lecture - getObsValuesByBBox
        """
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getObsValuesByBBox_antilope-j1-rr_010000.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 4)
        self.assertIn(
            ('793500.0,6412500.0', 'PJ', 'antilope,j1,rr,val'), series)
        self.assertIn(
            ('793500.0,6413500.0', 'PJ', 'antilope,j1,rr,val'), series)
        self.assertIn(
            ('794500.0,6412500.0', 'PJ', 'antilope,j1,rr,val'), series)
        self.assertIn(
            ('794500.0,6413500.0', 'PJ', 'antilope,j1,rr,val'), series)
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getObsValuesByBBox_sim-t-t.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 6)
        self.assertIn(
            ('779000.0,6404000.0', 'TI', 'sim,t,t,val'), series)
        self.assertIn(
            ('779000.0,6412000.0', 'TI', 'sim,t,t,val'), series)
        self.assertIn(
            ('779000.0,6420000.0', 'TI', 'sim,t,t,val'), series)
        self.assertIn(
            ('787000.0,6404000.0', 'TI', 'sim,t,t,val'), series)
        self.assertIn(
            ('787000.0,6412000.0', 'TI', 'sim,t,t,val'), series)
        self.assertIn(
            ('787000.0,6420000.0', 'TI', 'sim,t,t,val'), series)
        # =====================================================================

    def test_read_getPrevByNetworkStatsByZones(self):
        """
        Test de lecture - getPrevByNetworkStatsByZones
        """
        # =====================================================================
        trends = ['med', 'moy', 'q40', 'min', 'max', 'etyp', 'cvar', 'q90',
                  'q20', 'q80', 'q30', 'q10', 'q60', 'q70']
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getPrevByNetworkStatsByZones_sympo-rr-rr.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in trends:
            self.assertIn(
                ('LO8060', 'P3H', (dt(2021, 5, 10, 6), 'sympo', 'rr', s)),
                series)
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getPrevByNetworkStatsByZones_sympo-t-t.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in trends:
            self.assertIn(
                ('LO8060', 'TI', (dt(2021, 5, 10, 6), 'sympo', 't', s)),
                series)
        # ====================================================================
        filename = os.path.join(
            self.dirname, 'getPrevByNetworkStatsByZones_arpege-rr-total.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in trends:
            self.assertIn(
                ('LO8060', 'P3H', (dt(2022, 3, 31, 6), 'arpege', 'rr', s)),
                series)
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getPrevByNetworkStatsByZones_arpege-t-t.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in trends:
            self.assertIn(
                ('LO8060', 'TI', (dt(2022, 3, 31, 6), 'arpege', 't', s)),
                series)
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getPrevByNetworkStatsByZones_arome-rr-total.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in trends:
            self.assertIn(
                ('LO8060', 'PH', (dt(2022, 3, 31, 6), 'arome', 'rr', s)),
                series)
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getPrevByNetworkStatsByZones_arome-t-t.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in trends:
            self.assertIn(
                ('LO8060', 'TI', (dt(2022, 3, 31, 6), 'arome', 't', s)),
                series)
        # =====================================================================
        filename = os.path.join(
            self.dirname,
            'getPrevByNetworkStatsByZones_arome-ifs-rr-total.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in trends:
            self.assertIn(
                ('LO8060', 'PH', (dt(2022, 3, 31, 12), 'arome-ifs', 'rr', s)),
                series)
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getPrevByNetworkStatsByZones_arome-ifs-t-t.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in trends:
            self.assertIn(
                ('LO8060', 'TI', (dt(2022, 3, 31, 12), 'arome-ifs', 't', s)),
                series)
        # =====================================================================
        filename = os.path.join(
            self.dirname,
            'getPrevByNetworkStatsByZones_arome-pi-rr-total.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        for s in ['moy']:
            self.assertIn(
                ('LO808', 'P15m', (dt(2023, 8, 31, 9), 'arome-pi', 'rr', s)),
                series)
        # =====================================================================
        filename = os.path.join(
            self.dirname,
            'getPrevByNetworkStatsByZones_piaf-france-antilope-15mn-rr.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        for s in ['moy']:
            self.assertIn(
                ('LO808', 'P15m', (dt(2023, 8, 31, 10, 15), 'piaf',
                                   'france-antilope-15mn', s)),
                series)
        # =====================================================================

    def test_read_getPrevByNetworkStatsByPixels(self):
        """
        Test de lecture - getPrevByNetworkStatsByPixels
        """
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getPrevByNetworkStatsByPixels_sympo-rr-rr.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in ['med', 'moy', 'q40', 'min', 'max', 'etyp', 'cvar', 'q90',
                  'q20', 'q80', 'q30', 'q10', 'q60', 'q70']:
            self.assertIn(
                ('pixels', 'P3H', (dt(2021, 5, 10, 6), 'sympo', 'rr', s)),
                series)
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getPrevByNetworkStatsByPixels_sympo-t-t.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 14)
        for s in ['med', 'moy', 'q40', 'min', 'max', 'etyp', 'cvar', 'q90',
                  'q20', 'q80', 'q30', 'q10', 'q60', 'q70']:
            self.assertIn(
                ('pixels', 'TI', (dt(2021, 5, 10, 6), 'sympo', 't', s)),
                series)
        # =====================================================================

    def test_read_getPrevByNetworkValuesByBBox(self):
        """
        Test de lecture - getPrevByNetworkValuesByBBox
        """
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getPrevByNetworkValuesByBBox_sympo-rr-rr.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 4)
        self.assertIn(
            ('793500.0,6412500.0', 'P3H',
             (dt(2021, 5, 10, 6), 'sympo', 'rr', 'val')),
            series)
        self.assertIn(
            ('793500.0,6413500.0', 'P3H',
             (dt(2021, 5, 10, 6), 'sympo', 'rr', 'val')),
            series)
        self.assertIn(
            ('794500.0,6412500.0', 'P3H',
             (dt(2021, 5, 10, 6), 'sympo', 'rr', 'val')),
            series)
        self.assertIn(
            ('794500.0,6413500.0', 'P3H',
             (dt(2021, 5, 10, 6), 'sympo', 'rr', 'val')),
            series)
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'getPrevByNetworkValuesByBBox_sympo-t-t.xml')
        series = read_BdImage(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 4)
        self.assertIn(
            ('793500.0,6412500.0', 'TI',
             (dt(2021, 5, 10, 6), 'sympo', 't', 'val')),
            series)
        self.assertIn(
            ('793500.0,6413500.0', 'TI',
             (dt(2021, 5, 10, 6), 'sympo', 't', 'val')),
            series)
        self.assertIn(
            ('794500.0,6412500.0', 'TI',
             (dt(2021, 5, 10, 6), 'sympo', 't', 'val')),
            series)
        self.assertIn(
            ('794500.0,6413500.0', 'TI',
             (dt(2021, 5, 10, 6), 'sympo', 't', 'val')),
            series)
        # =====================================================================
