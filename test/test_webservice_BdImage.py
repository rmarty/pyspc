#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for BdImage in pyspc.webservice.lamedo

To run all tests just type:
    python -m unittest test_webservice_BdImage

To run only a class test:
    python -m unittest test_webservice_BdImage.TestBdImage

To run only a specific test:
    python -m unittest test_webservice_BdImage.TestBdImage.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import glob
import os
import re
import unittest
import warnings

from libbdimage.bdiws import Client
from libbdimage.bdipixel import Nppixels

from pyspc.webservice.lamedo import BdImage
from pyspc.webservice.lamedo.bdimage import split_domains, BBox


class TestBdImage(unittest.TestCase):
    """
    BdImage class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'webservice', 'lamedo')
        self.antilope = ('antilope', 'j1', 'rr')
        self.sim_rr = ('sim', 'rr', 'total')
        self.sim_tt = ('sim', 't', 't')
        self.sympo_rr = ('sympo', 'rr', 'rr')
        self.sympo_tt = ('sympo', 't', 't')
        self.arpege_rr = ('arpege', 'rr', 'total')
        self.arpege_tt = ('arpege', 't', 't')
        self.arome_rr = ('arome', 'rr', 'total')
        self.arome_tt = ('arome', 't', 't')
        self.aromeifs_rr = ('arome-ifs', 'rr', 'total')
        self.aromeifs_tt = ('arome-ifs', 't', 't')
        self.zones = 'LO8060'  # Ste-Eulalie
        self.zones_sim = 'LO8087'  # Goudet
        self.pixels = '793000.0,6412000.0+793000.0,6413000.0'\
            '+793000.0,6414000.0+794000.0,6412000.0+794000.0,6413000.0'\
            '+794000.0,6414000.0+795000.0,6412000.0+795000.0,6413000.0'\
            '+795000.0,6414000.0'  # Ste-Eulalie
        self.pixels_sim = '776180.0,6407789.0+776180.0,6415782.0'\
            '+776180.0,6423775.0+784173.0,6407789.0+784173.0,6415782.0'\
            '+784173.0,6423775.0+792166.0,6407789.0+792166.0,6415782.0'\
            '+792166.0,6423775.0'
        self.box = 'Bx793000,6414000,795000,6412000'  # Ste-Eulalie
        self.box_sim = 'Bx775000,6430000,795000,6400000'
        self.event_anthor = (dt(2021, 5, 10), dt(2021, 5, 11, 6), td(hours=1))
        self.event_antjr = (dt(2021, 5, 9, 6), dt(2021, 5, 12, 6), td(days=1))
        self.event_sim_rr = (dt(2021, 5, 9, 6), dt(2021, 5, 12, 6),
                             td(days=1))
        self.event_sim_tt = (dt(2021, 5, 9, 6), dt(2021, 5, 12, 6),
                             td(seconds=0))
        self.event_sympo_rr = (dt(2021, 5, 10, 6), dt(2021, 5, 12),
                               td(hours=3))
        self.event_sympo_tt = (dt(2021, 5, 10, 6), dt(2021, 5, 12),
                               td(seconds=0))
        self.re = re
        self.event_arpege_rr = (dt(2022, 3, 31, 6), dt(2022, 4, 3, 6),
                                td(hours=3))
        self.event_arpege_tt = (dt(2022, 3, 31, 6), dt(2022, 4, 3, 6),
                                td(seconds=0))
        self.event_arome_rr = (dt(2022, 3, 31, 6), dt(2022, 4, 2),
                               td(hours=1))
        self.event_arome_tt = (dt(2022, 3, 31, 6), dt(2022, 4, 2),
                               td(seconds=0))
        self.event_aromeifs_rr = (dt(2022, 3, 31, 12), dt(2022, 4, 2, 12),
                                  td(hours=1))
        self.event_aromeifs_tt = (dt(2022, 3, 31, 12), dt(2022, 4, 2, 12),
                                  td(seconds=0))
        warnings.filterwarnings(action="ignore", category=ResourceWarning)

    def test_init(self):
        """
        Test initialisation de l'instance
        """
        # ====================================================================
        bdimage = BdImage()
        self.assertIsNone(bdimage.proxies)
        self.assertIsNone(bdimage.filename)
        self.assertIsNone(bdimage.url)
        self.assertIsInstance(bdimage.client, Client)
        # ====================================================================

#    def test_getobsstatsbypixels(self):
#        """
#        Test requête OBS / STATS / PIXELS
#        """
#        # ====================================================================
#        bdimage = BdImage()
#        # ====================================================================
#        # SIM RR / JOURNALIER / STATS='complete'
#        # --------------------------------------------------------------------
#        key = (self.pixels_sim, self.sim_rr,
#               self.event_sim_rr[0], self.event_sim_rr[1], None,
#               self.event_sim_rr[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "stats": "complete",
#            "duree": "010000",  # JOURNALIER
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "pixels": r".*pixels.*",
#            "epsg": "2154",
#            "request": r"getObsStatsByPixels\w*",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            domains=key[0], stats=valid['stats'])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
#            self.assertTrue(rm)
#        # ====================================================================
#        # SIM TT / INSTANTANE / STATS='complete'
#        # --------------------------------------------------------------------
#        key = (self.pixels_sim, self.sim_tt,
#               self.event_sim_tt[0], self.event_sim_tt[1], None,
#               self.event_sim_tt[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "stats": "complete",
#            "duree": "000000",  # INSTANTANE
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "pixels": r".*pixels.*",
#            "epsg": "2154",
#            "request": r"getObsStatsByPixels\w*",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            domains=key[0], stats=valid['stats'])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
##            print(str(content[key]))
#            self.assertTrue(rm)
#        # ====================================================================
#
#    def test_getobsstatsbyzones(self):
#        """
#        Test requête OBS / STATS / ZONES
#        """
#        # ====================================================================
#        bdimage = BdImage()
#        # ====================================================================
#        # ANTILOPE / HORAIRE / STATS='complete'
#        # --------------------------------------------------------------------
#        key = (self.zones, self.antilope,
#               self.event_anthor[0], self.event_anthor[1], None,
#               self.event_anthor[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "stats": "complete",
#            "duree": "000100",  # HORAIRE
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "zones": r".*zones.*",
#            "request": r"getObsStatsByZones\w*",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            domains=key[0], stats=valid['stats'])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
#            self.assertTrue(rm)
#        # ====================================================================
#        # ANTILOPE / JOURNALIER / PRECISION='forte'
#        # --------------------------------------------------------------------
#        key = (self.zones, self.antilope,
#               self.event_antjr[0], self.event_antjr[1], None,
#               self.event_antjr[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "precision": "forte",
#            "duree": "010000",  # JOURNALIER
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "zones": r".*zones.*",
#            "request": r"getObsStatsByZones\w*",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            domains=key[0], precision=valid['precision'])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
##            print(str(content[key]))
#            self.assertTrue(rm)
#        # ====================================================================
#
#    def test_getobsvaluesbybbox(self):
#        """
#        Test requête OBS / VALUES / BBOX
#        """
#        # ====================================================================
#        bdimage = BdImage()
#        # ====================================================================
#        # ANTILOPE / JOURNALIER
#        # --------------------------------------------------------------------
#        key = (self.box, self.antilope,
#               self.event_antjr[0], self.event_antjr[1], None,
#               self.event_antjr[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "duree": "010000",  # JOURNALIER
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "ul": r"793000\.\d*,6414000\.\d*",
#            "lr": r"795000\.\d*,6412000\.\d*",
#            "request": r"getObsValuesByBBox\w*",
#            "epsg": "2154",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            domains=key[0])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
##            print(str(content[key]))
#            self.assertTrue(rm)
#        # ====================================================================
#        # SIM TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.box_sim, self.sim_tt,
#               self.event_sim_tt[0], self.event_sim_tt[1], None,
#               self.event_sim_tt[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "duree": "000000",  # INSTANTANE
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "ul": r"775000\.\d*,6430000\.\d*",
#            "lr": r"795000\.\d*,6400000\.\d*",
#            "epsg": "2154",
#            "request": r"getObsValuesByBBox\w*",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            domains=key[0])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
##            print(str(content[key]))
#            self.assertTrue(rm)
#        # ====================================================================
#
#    def test_getobsvaluesbypixels(self):
#        """
#        Test requête OBS / VALUES / PIXELS
#        """
#        # ====================================================================
#        bdimage = BdImage()
#        # ====================================================================
#        # ANTILOPE / JOURNALIER
#        # --------------------------------------------------------------------
#        key = (self.pixels, self.antilope,
#               self.event_antjr[0], self.event_antjr[1], None,
#               self.event_antjr[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "duree": "010000",  # JOURNALIER
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "pixels": r".*pixels.*",
#            "coord": "True",
#            "request": r"getObsValuesByPixels\w*",
#            "epsg": "2154",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            domains=key[0])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
##            print(str(content[key]))
#            self.assertTrue(rm)
#        # ====================================================================
#        # SIM TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.pixels_sim, self.sim_tt,
#               self.event_sim_tt[0], self.event_sim_tt[1], None,
#               self.event_sim_tt[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "duree": "000000",  # INSTANTANE
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "pixels": r".*pixels.*",
#            "coord": "True",
#            "epsg": "2154",
#            "request": r"getObsValuesByPixels\w*",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            domains=key[0])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
##            print(str(content[key]))
#            self.assertTrue(rm)
#        # ====================================================================
#
#    def test_getobsvaluesbyzones(self):
#        """
#        Test requête OBS / VALUES / ZONES
#        """
#        # ====================================================================
#        bdimage = BdImage()
#        # ====================================================================
#        # ANTILOPE / JOURNALIER
#        # --------------------------------------------------------------------
#        key = (self.zones, self.antilope,
#               self.event_antjr[0], self.event_antjr[1], None,
#               self.event_antjr[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "duree": "010000",  # JOURNALIER
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "zones": r".*zones.*",
#            "request": r"getObsValuesByZones\w*",
#            "epsg": "2154",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            domains=key[0])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
##            print(str(content[key]))
#            self.assertTrue(rm)
#        # ====================================================================
#        # SIM TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.zones_sim, self.sim_tt,
#               self.event_sim_tt[0], self.event_sim_tt[1], None,
#               self.event_sim_tt[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "duree": "000000",  # INSTANTANE
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "zones": r".*zones.*",
#            "epsg": "2154",
#            "request": r"getObsValuesByZones\w*",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            domains=key[0])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
##            print(str(content[key]))
#            self.assertTrue(rm)
#        # ====================================================================
#
#    def test_getprevbynetworkstatsbypixels(self):
#        """
#        Test requête PREV / STATS / PIXELS
#        """
#        # ====================================================================
#        bdimage = BdImage()
#        # ====================================================================
#        # SYMPO RR / 3-HR
#        # --------------------------------------------------------------------
#        key = (self.pixels, self.sympo_rr,
#               self.event_sympo_rr[0], self.event_sympo_rr[1],
#               self.event_sympo_rr[0], self.event_sympo_rr[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "stats": "complete",
#            "duree": "000300",  # 3-HR
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "dateNetwork": key[4].strftime('%Y%m%d%H%M'),
#            "pixels": r".*pixels.*",
#            "epsg": "2154",
#            "request": r"getPrevByNetworkStatsByPixels\w*",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            runtime=key[4], domains=key[0], stats=valid['stats'])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
#            self.assertTrue(rm)
#        # ====================================================================
#        # SYMPO TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.pixels, self.sympo_tt,
#               self.event_sympo_tt[0], self.event_sympo_tt[1],
#               self.event_sympo_tt[0], self.event_sympo_tt[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "stats": "complete",
#            "duree": "000000",  # INSTANTANE
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "dateNetwork": key[4].strftime('%Y%m%d%H%M'),
#            "pixels": r".*pixels.*",
#            "epsg": "2154",
#            "request": r"getPrevByNetworkStatsByPixels\w*",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            runtime=key[4], domains=key[0], stats=valid['stats'])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
#            self.assertTrue(rm)
#        # ====================================================================
#
#    def test_getprevbynetworkstatsbyzones(self):
#        """
#        Test requête PREV / STATS / ZONES
#        """
#        # ====================================================================
#        bdimage = BdImage()
#        # ====================================================================
#        # SYMPO RR / 3-HR
#        # --------------------------------------------------------------------
#        key = (self.zones, self.sympo_rr,
#               self.event_sympo_rr[0], self.event_sympo_rr[1],
#               self.event_sympo_rr[0], self.event_sympo_rr[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "stats": "complete",
#            "duree": "000300",  # 3-HR
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "dateNetwork": key[4].strftime('%Y%m%d%H%M'),
#            "zones": r".*zones.*",
#            "request": r"getPrevByNetworkStatsByZones\w*",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            runtime=key[4], domains=key[0], stats=valid['stats'])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
#            self.assertTrue(rm)
#        # ====================================================================
#        # SYMPO TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.zones, self.sympo_tt,
#               self.event_sympo_tt[0], self.event_sympo_tt[1],
#               self.event_sympo_tt[0], self.event_sympo_tt[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "stats": "complete",
#            "duree": "000000",  # INSTANTANE
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "dateNetwork": key[4].strftime('%Y%m%d%H%M'),
#            "zones": r".*zones.*",
#            "request": r"getPrevByNetworkStatsByZones\w*",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            runtime=key[4], domains=key[0], stats=valid['stats'])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
#            self.assertTrue(rm)
#        # ====================================================================
#
#    def test_getprevbynetworkvaluesbybbox(self):
#        """
#        Test requête PREV / VALUES / BBOX
#        """
#        # ====================================================================
#        bdimage = BdImage()
#        # ====================================================================
#        # SYMPO RR / 3-HR
#        # --------------------------------------------------------------------
#        key = (self.box, self.sympo_rr,
#               self.event_sympo_rr[0], self.event_sympo_rr[1],
#               self.event_sympo_rr[0], self.event_sympo_rr[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "duree": "000300",  # 3-HR
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "dateNetwork": key[4].strftime('%Y%m%d%H%M'),
#            "ul": r"793000\.\d*,6414000\.\d*",
#            "lr": r"795000\.\d*,6412000\.\d*",
#            "epsg": "2154",
#            "request": r"getPrevByNetworkValuesByBBox\w*",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            runtime=key[4], domains=key[0])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
#            self.assertTrue(rm)
#        # ====================================================================
#        # SYMPO TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.box, self.sympo_tt,
#               self.event_sympo_tt[0], self.event_sympo_tt[1],
#               self.event_sympo_tt[0], self.event_sympo_tt[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "duree": "000000",  # INSTANTANE
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "dateNetwork": key[4].strftime('%Y%m%d%H%M'),
#            "ul": r"793000\.\d*,6414000\.\d*",
#            "lr": r"795000\.\d*,6412000\.\d*",
#            "epsg": "2154",
#            "request": r"getPrevByNetworkValuesByBBox\w*",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            runtime=key[4], domains=key[0])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
#            self.assertTrue(rm)
#        # ====================================================================
#
#    def test_getprevbynetworkvaluesbypixels(self):
#        """
#        Test requête PREV / VALUES / PIXELS
#        """
#        # ====================================================================
#        bdimage = BdImage()
#        # ====================================================================
#        # SYMPO RR / 3-HR
#        # --------------------------------------------------------------------
#        key = (self.pixels, self.sympo_rr,
#               self.event_sympo_rr[0], self.event_sympo_rr[1],
#               self.event_sympo_rr[0], self.event_sympo_rr[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "duree": "000300",  # 3-HR
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "dateNetwork": key[4].strftime('%Y%m%d%H%M'),
#            "pixels": r".*pixels.*",
#            "coord": "True",
#            "epsg": "2154",
#            "request": r"getPrevByNetworkValuesByPixels\w*",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            runtime=key[4], domains=key[0])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
#            self.assertTrue(rm)
#        # ====================================================================
#        # SYMPO TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.pixels, self.sympo_tt,
#               self.event_sympo_tt[0], self.event_sympo_tt[1],
#               self.event_sympo_tt[0], self.event_sympo_tt[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "duree": "000000",  # INSTANTANE
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "dateNetwork": key[4].strftime('%Y%m%d%H%M'),
#            "pixels": r".*pixels.*",
#            "coord": "True",
#            "epsg": "2154",
#            "request": r"getPrevByNetworkValuesByPixels\w*",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            runtime=key[4], domains=key[0])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
#            self.assertTrue(rm)
#        # ====================================================================
#
#    def test_getprevbynetworkvaluesbyzones(self):
#        """
#        Test requête PREV / VALUES / ZONES
#        """
#        # ====================================================================
#        bdimage = BdImage()
#        # ====================================================================
#        # SYMPO RR / 3-HR
#        # --------------------------------------------------------------------
#        key = (self.zones, self.sympo_rr,
#               self.event_sympo_rr[0], self.event_sympo_rr[1],
#               self.event_sympo_rr[0], self.event_sympo_rr[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "duree": "000300",  # 3-HR
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "dateNetwork": key[4].strftime('%Y%m%d%H%M'),
#            "zones": r".*zones.*",
#            "request": r"getPrevByNetworkValuesByZones\w*",
#            "epsg": "2154",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            runtime=key[4], domains=key[0])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
#            self.assertTrue(rm)
#        # ====================================================================
#        # SYMPO TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.zones, self.sympo_tt,
#               self.event_sympo_tt[0], self.event_sympo_tt[1],
#               self.event_sympo_tt[0], self.event_sympo_tt[2])
#        valid = {
#            "typeImage": key[1][0],
#            "sousTypeImage": key[1][1],
#            "bandes": key[1][2],
#            "duree": "000000",  # INSTANTANE
#            "dateDeb": key[2].strftime('%Y%m%d%H%M'),
#            "dateFin": key[3].strftime('%Y%m%d%H%M'),
#            "dateNetwork": key[4].strftime('%Y%m%d%H%M'),
#            "zones": r".*zones.*",
#            "request": r"getPrevByNetworkValuesByZones\w*",
#            "epsg": "2154",
#            "version": "2016",
#        }
#        content = bdimage.get(
#            image=key[1], tdelta=key[5], first_dtime=key[2], last_dtime=key[3],
#            runtime=key[4], domains=key[0])
#        self.assertIsInstance(content, dict)
#        self.assertIn(key, content)
#        for k, v in valid.items():
#            rm = re.search(r'<requete>(.*)<parametre nom="' + k + r'">' + v +
#                           r'</parametre>(.*)</requete>',
#                           str(content[key]), re.DOTALL)
##            print(k, v, rm)
#            self.assertTrue(rm)
#        # ====================================================================

    def test_precision(self):
        """
        Test du contrôle Stat
        """
        # ====================================================================
        bdimage = BdImage()
        self.assertEqual(['forte', 'standard'], bdimage.get_precision())
        try:
            bdimage.check_precision(precision='standard')
        except ValueError:
            self.fail("Erreur dans le contrôle de la précision")
        try:
            bdimage.check_precision(precision='forte')
        except ValueError:
            self.fail("Erreur dans le contrôle de la précision")
        # ====================================================================

    def test_split_domains(self):
        """
        Test de la définition des zones, pixels et bounding-boxes
        """
        # ====================================================================
        test_zones = 'TT9394-TT8000+TT238+TT4502-TT4523-TT4618'
        test_pixels = '791530,6408819+802412.50,6440673.50'
        test_bbox = 'Bx610722,6627518,667454.50,6590924.50'
        valid_zones = ['TT9394-TT8000', 'TT238', 'TT4502-TT4523-TT4618']
        valid_pixels = Nppixels(
            [(791530, 6408819), (802412.50, 6440673.50)], epsg='2154')
        test_domains = test_zones + '+' + test_pixels + '+' + test_bbox
        # ====================================================================
        splitted_domains = split_domains(domains=test_zones)
        self.assertIsInstance(splitted_domains, dict)
        self.assertIn('zones', splitted_domains)
        self.assertEqual(splitted_domains['zones'], valid_zones)
        self.assertIn('pixels', splitted_domains)
        self.assertIsNone(splitted_domains['pixels'])
        self.assertIn('bbox', splitted_domains)
        self.assertIsNone(splitted_domains['bbox'])
        # ====================================================================
        splitted_domains = split_domains(domains=test_pixels)
        self.assertIsInstance(splitted_domains, dict)
        self.assertIn('zones', splitted_domains)
        self.assertIsNone(splitted_domains['zones'])
        self.assertIn('pixels', splitted_domains)
        self.assertEqual(splitted_domains['pixels'], valid_pixels)
        self.assertIn('bbox', splitted_domains)
        self.assertIsNone(splitted_domains['bbox'])
        # ====================================================================
        splitted_domains = split_domains(domains=test_bbox)
        self.assertIsInstance(splitted_domains, dict)
        self.assertIn('zones', splitted_domains)
        self.assertIsNone(splitted_domains['zones'])
        self.assertIn('pixels', splitted_domains)
        self.assertIsNone(splitted_domains['pixels'])
        self.assertIn('bbox', splitted_domains)
        self.assertIsInstance(splitted_domains['bbox'], BBox)
        self.assertEqual(splitted_domains['bbox'].ul, (610722, 6627518))
        self.assertEqual(splitted_domains['bbox'].lr, (667454.50, 6590924.50))
        self.assertEqual(splitted_domains['bbox'].asstr, test_bbox)
        # ====================================================================
        splitted_domains = split_domains(domains=test_domains)
        self.assertIsInstance(splitted_domains, dict)
        self.assertIn('zones', splitted_domains)
        self.assertEqual(splitted_domains['zones'], valid_zones)
        self.assertIn('pixels', splitted_domains)
        self.assertEqual(splitted_domains['pixels'], valid_pixels)
        self.assertIn('bbox', splitted_domains)
        self.assertIsInstance(splitted_domains['bbox'], BBox)
        self.assertEqual(splitted_domains['bbox'].ul, (610722, 6627518))
        self.assertEqual(splitted_domains['bbox'].lr, (667454.50, 6590924.50))
        self.assertEqual(splitted_domains['bbox'].asstr, test_bbox)
        # ====================================================================
        unvalid_bbox = 'Bx610722,6627518,667454.50,6590924.50'\
            '+Bx610722,6627518,667454.50,6590924.50'
        unvalid_all = test_zones + '+' + test_pixels + '+' + unvalid_bbox
        with self.assertRaises(ValueError):
            split_domains(domains=unvalid_bbox)
        with self.assertRaises(ValueError):
            split_domains(domains=unvalid_all)
        # ====================================================================

    def test_retrieve(self):
        """
        Test de l'enregistrement XML
        """
#        # ====================================================================
#        bdimage = BdImage()
#        # ====================================================================
#        # getObsStatsByPixels       SIM RR / JOURNALIER / STATS='complete'
#        # --------------------------------------------------------------------
#        key = (self.pixels_sim, self.sim_rr,
#               self.event_sim_rr[0], self.event_sim_rr[1], None,
#               self.event_sim_rr[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], domains=key[0], stats='complete')
#        self.assertListEqual(
#            ['data\\SourceLoire-pixels_sim-rr-total_'
#             '202105090600-202105120600_010000.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getObsStatsByPixels      SIM TT / INSTANTANE / STATS='complete'
#        # --------------------------------------------------------------------
#        key = (self.pixels_sim, self.sim_tt,
#               self.event_sim_tt[0], self.event_sim_tt[1], None,
#               self.event_sim_tt[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], domains=key[0], stats='complete')
#        self.assertListEqual(
#            ['data\\SourceLoire-pixels_sim-t-t_'
#             '202105090600-202105120600_000000.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getObsStatsByZones       ANTILOPE / HORAIRE / STATS='complete'
#        # --------------------------------------------------------------------
#        key = (self.zones, self.antilope,
#               self.event_anthor[0], self.event_anthor[1], None,
#               self.event_anthor[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], domains=key[0], stats='complete')
#        self.assertListEqual(
#            ['data\\SourceLoire-zones_antilope-j1-rr_'
#             '202105100000-202105110600_000100.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getObsStatsByZones       ANTILOPE / JOURNALIER / PRECISION='forte'
#        # --------------------------------------------------------------------
#        key = (self.zones, self.antilope,
#               self.event_antjr[0], self.event_antjr[1], None,
#               self.event_antjr[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], domains=key[0], stats='complete')
#        self.assertListEqual(
#            ['data\\SourceLoire-zones_antilope-j1-rr_'
#             '202105090600-202105120600_010000.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getObsValuesByBBox       ANTILOPE / JOURNALIER
#        # --------------------------------------------------------------------
#        key = (self.box, self.antilope,
#               self.event_antjr[0], self.event_antjr[1], None,
#               self.event_antjr[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], domains=key[0])
#        self.assertListEqual(
#            ['data\\SourceLoire-bbox_antilope-j1-rr_'
#             '202105090600-202105120600_010000.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getObsValuesByBBox       SIM TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.box_sim, self.sim_tt,
#               self.event_sim_tt[0], self.event_sim_tt[1], None,
#               self.event_sim_tt[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], domains=key[0])
#        self.assertListEqual(
#            ['data\\SourceLoire-bbox_sim-t-t_'
#             '202105090600-202105120600_000000.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getObsValuesByPixels     ANTILOPE / JOURNALIER
#        # --------------------------------------------------------------------
#        key = (self.pixels, self.antilope,
#               self.event_antjr[0], self.event_antjr[1], None,
#               self.event_antjr[2])
#        # ====================================================================
#        # getObsValuesByPixels     SIM TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.pixels_sim, self.sim_tt,
#               self.event_sim_tt[0], self.event_sim_tt[1], None,
#               self.event_sim_tt[2])
#        # ====================================================================
#        # getObsValuesByZones      ANTILOPE / JOURNALIER
#        # --------------------------------------------------------------------
#        key = (self.zones, self.antilope,
#               self.event_antjr[0], self.event_antjr[1], None,
#               self.event_antjr[2])
#        # ====================================================================
#        # getObsValuesByZones      SIM TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.zones_sim, self.sim_tt,
#               self.event_sim_tt[0], self.event_sim_tt[1], None,
#               self.event_sim_tt[2])
#        # ====================================================================
#        # getPrevByNetworkStatsByPixels    SYMPO RR / 3-HR
#        # --------------------------------------------------------------------
#        key = (self.pixels, self.sympo_rr,
#               self.event_sympo_rr[0], self.event_sympo_rr[1],
#               self.event_sympo_rr[0], self.event_sympo_rr[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], runtime=key[4], domains=key[0],
#            stats='complete')
#        self.assertListEqual(
#            ['data\\SourceLoire-pixels_sympo-rr-rr_'
#             '202105100600-202105100600-202105120000_000300.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getPrevByNetworkStatsByPixels    SYMPO TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.pixels, self.sympo_tt,
#               self.event_sympo_tt[0], self.event_sympo_tt[1],
#               self.event_sympo_tt[0], self.event_sympo_tt[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], runtime=key[4], domains=key[0],
#            stats='complete')
#        self.assertListEqual(
#            ['data\\SourceLoire-pixels_sympo-t-t_'
#             '202105100600-202105100600-202105120000_000000.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getPrevByNetworkStatsByZones     SYMPO RR / 3-HR
#        # --------------------------------------------------------------------
#        key = (self.zones, self.sympo_rr,
#               self.event_sympo_rr[0], self.event_sympo_rr[1],
#               self.event_sympo_rr[0], self.event_sympo_rr[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], runtime=key[4], domains=key[0],
#            stats='complete')
#        self.assertListEqual(
#            ['data\\SourceLoire-zones_sympo-rr-rr_'
#             '202105100600-202105100600-202105120000_000300.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getPrevByNetworkStatsByZones     SYMPO TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.zones, self.sympo_tt,
#               self.event_sympo_tt[0], self.event_sympo_tt[1],
#               self.event_sympo_tt[0], self.event_sympo_tt[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], runtime=key[4], domains=key[0],
#            stats='complete')
#        self.assertListEqual(
#            ['data\\SourceLoire-zones_sympo-t-t_'
#             '202105100600-202105100600-202105120000_000000.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getPrevByNetworkStatsByZones     ARPEGE RR / 3-HR
#        # --------------------------------------------------------------------
#        key = (self.zones, self.arpege_rr,
#               self.event_arpege_rr[0], self.event_arpege_rr[1],
#               self.event_arpege_rr[0], self.event_arpege_rr[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2]+key[5],
#            last_dtime=key[3], runtime=key[4], domains=key[0],
#            stats='complete')
#        self.assertListEqual(
#            ['data\\SourceLoire-zones_arpege-rr-total_'
#             '202203310600-202203310900-202204030600_000300.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getPrevByNetworkStatsByZones     ARPEGE TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.zones, self.arpege_tt,
#               self.event_arpege_tt[0], self.event_arpege_tt[1],
#               self.event_arpege_tt[0], self.event_arpege_tt[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], runtime=key[4], domains=key[0],
#            stats='complete')
#        self.assertListEqual(
#            ['data\\SourceLoire-zones_arpege-t-t_'
#             '202203310600-202203310600-202204030600_000000.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getPrevByNetworkStatsByZones     AROME RR / 1-HR
#        # --------------------------------------------------------------------
#        key = (self.zones, self.arome_rr,
#               self.event_arome_rr[0], self.event_arome_rr[1],
#               self.event_arome_rr[0], self.event_arome_rr[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2]+key[5],
#            last_dtime=key[3], runtime=key[4], domains=key[0],
#            stats='complete')
#        self.assertListEqual(
#            ['data\\SourceLoire-zones_arome-rr-total_'
#             '202203310600-202203310700-202204020000_000100.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getPrevByNetworkStatsByZones     AROME TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.zones, self.arome_tt,
#               self.event_arome_tt[0], self.event_arome_tt[1],
#               self.event_arome_tt[0], self.event_arome_tt[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], runtime=key[4], domains=key[0],
#            stats='complete')
#        self.assertListEqual(
#            ['data\\SourceLoire-zones_arome-t-t_'
#             '202203310600-202203310600-202204020000_000000.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getPrevByNetworkStatsByZones     AROME-IFS RR / 1-HR
#        # --------------------------------------------------------------------
#        key = (self.zones, self.aromeifs_rr,
#               self.event_aromeifs_rr[0], self.event_aromeifs_rr[1],
#               self.event_aromeifs_rr[0], self.event_aromeifs_rr[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2]+key[5],
#            last_dtime=key[3], runtime=key[4], domains=key[0],
#            stats='complete')
#        self.assertListEqual(
#            ['data\\SourceLoire-zones_arome-ifs-rr-total_'
#             '202203311200-202203311300-202204021200_000100.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getPrevByNetworkStatsByZones     AROME-IFS TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.zones, self.aromeifs_tt,
#               self.event_aromeifs_tt[0], self.event_aromeifs_tt[1],
#               self.event_aromeifs_tt[0], self.event_aromeifs_tt[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], runtime=key[4], domains=key[0],
#            stats='complete')
#        self.assertListEqual(
#            ['data\\SourceLoire-zones_arome-ifs-t-t_'
#             '202203311200-202203311200-202204021200_000000.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getPrevByNetworkValuesByBBox     SYMPO RR / 3-HR
#        # --------------------------------------------------------------------
#        key = (self.box, self.sympo_rr,
#               self.event_sympo_rr[0], self.event_sympo_rr[1],
#               self.event_sympo_rr[0], self.event_sympo_rr[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], runtime=key[4], domains=key[0])
#        self.assertListEqual(
#            ['data\\SourceLoire-bbox_sympo-rr-rr_'
#             '202105100600-202105100600-202105120000_000300.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getPrevByNetworkValuesByBBox     SYMPO TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.box, self.sympo_tt,
#               self.event_sympo_tt[0], self.event_sympo_tt[1],
#               self.event_sympo_tt[0], self.event_sympo_tt[2])
#        filenames = bdimage.retrieve(
#            dirname='data', domainname="SourceLoire",
#            image=key[1], tdelta=key[5], first_dtime=key[2],
#            last_dtime=key[3], runtime=key[4], domains=key[0])
#        self.assertListEqual(
#            ['data\\SourceLoire-bbox_sympo-t-t_'
#             '202105100600-202105100600-202105120000_000000.xml'],
#            filenames
#        )
#        # ====================================================================
#        # getPrevByNetworkValuesByPixels   SYMPO RR / 3-HR
#        # --------------------------------------------------------------------
#        key = (self.pixels, self.sympo_rr,
#               self.event_sympo_rr[0], self.event_sympo_rr[1],
#               self.event_sympo_rr[0], self.event_sympo_rr[2])
#        # ====================================================================
#        # getPrevByNetworkValuesByPixels   SYMPO TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.pixels, self.sympo_tt,
#               self.event_sympo_tt[0], self.event_sympo_tt[1],
#               self.event_sympo_tt[0], self.event_sympo_tt[2])
#        # ====================================================================
#        # getPrevByNetworkValuesByZones    SYMPO RR / 3-HR
#        # --------------------------------------------------------------------
#        key = (self.zones, self.sympo_rr,
#               self.event_sympo_rr[0], self.event_sympo_rr[1],
#               self.event_sympo_rr[0], self.event_sympo_rr[2])
#        # ====================================================================
#        # getPrevByNetworkValuesByZones    SYMPO TT / INSTANTANE
#        # --------------------------------------------------------------------
#        key = (self.zones, self.sympo_tt,
#               self.event_sympo_tt[0], self.event_sympo_tt[1],
#               self.event_sympo_tt[0], self.event_sympo_tt[2])
#        # ====================================================================
        for f in glob.glob(os.path.join('data', 'SourceLoire*.xml')):
            os.remove(f)
        # ====================================================================

    def test_start(self):
        """
        Test de la première date
        """
        # ====================================================================
        date1 = dt(2000, 1, 1)
        date2 = dt(2006, 7, 1, 6)
        date3 = dt(2006, 7, 1, 7)
        date4 = dt(2021, 1, 1)
        bdimage = BdImage()
        # ====================================================================
        with self.assertRaises(ValueError):
            bdimage.check_start(image=None)
        with self.assertRaises(ValueError):
            bdimage.check_start(image=('antilope', 'j1', 'rr'), date=None)
        with self.assertRaises(ValueError):
            bdimage.check_start(image=('antilope', 'j1', 'rr'), date=date1)
        with self.assertRaises(ValueError):
            bdimage.check_start(image=('antilope', 'j1', 'rr'), date=date2)
        try:
            bdimage.check_start(image=('antilope', 'j1', 'rr'), date=date3)
            bdimage.check_start(image=('antilope', 'j1', 'rr'), date=date4)
        except ValueError:
            self.fail("Erreur dans le contrôle de la date de début")
        # ====================================================================

    def test_stats(self):
        """
        Test du contrôle Stat
        """
        # ====================================================================
        bdimage = BdImage()
        self.assertEqual(['complete', 'standard'], bdimage.get_stats())
        try:
            bdimage.check_stats(stats='standard')
        except ValueError:
            self.fail("Erreur dans le contrôle de la stat")
        try:
            bdimage.check_stats(stats='complete')
        except ValueError:
            self.fail("Erreur dans le contrôle de la stat")
        # ====================================================================

    def test_types(self):
        """
        Test du contrôle d'une image
        """
        # ====================================================================
        valid_str = [
            'antilope_france-td-15mn_qualite', 'antilope_france-td-15mn_rr',
            'antilope_france-td-60mn_qualite', 'antilope_france-td-60mn_rr',
            'antilope_france-tr-15mn_qualite', 'antilope_france-tr-15mn_rr',
            'antilope_france-tr-60mn_qualite', 'antilope_france-tr-60mn_rr',
            'antilope_j1_rr', 'antilope_temps-reel_rr', 'antilope_v1_rr',
            'arome-ifs_iso0_altitude', 'arome-ifs_lpn_altitude',
            'arome-ifs_rr_graupel', 'arome-ifs_rr_liquide',
            'arome-ifs_rr_neige', 'arome-ifs_rr_total', 'arome-ifs_t_t',
            'arome-pi_rr_total',
            'arome_iso0_altitude', 'arome_lpn_altitude',
            'arome_rr_graupel', 'arome_rr_liquide', 'arome_rr_neige',
            'arome_rr_total',  'arome_t_t',
            'arpege_iso0_altitude', 'arpege_lpn_altitude',
            'arpege_rr_liquide', 'arpege_rr_solide', 'arpege_rr_total',
            'arpege_t_t',
            'comephore_france_rr',
            'modis-snow_albedo_albedo', 'modis-snow_fsc_fsc',
            'panthere_france-iq_qualite', 'panthere_france-iq_rr',
            'panthere_france_rr',
            'piaf_france-antilope-15mn_rr',
            'sim_evap_rr', 'sim_hra_humidite', 'sim_hu_brut',
            'sim_hu_ref-clim', 'sim_hu_ref-jour', 'sim_hu_wg',
            'sim_neige_hauteur', 'sim_neige_rr', 'sim_ray_infrarouge',
            'sim_ray_solaire', 'sim_rr_ecoulement', 'sim_rr_fonte',
            'sim_rr_liquide', 'sim_rr_solide', 'sim_rr_total', 'sim_t_t',
            'sim_vent_vitesse',
            'sympo_iso0_altitude', 'sympo_lpn_altitude',
            'sympo_rr_rr', 'sympo_t_t',
        ]
        valid_tuple = [
            ('antilope', 'france-td-15mn', 'qualite'),
            ('antilope', 'france-td-15mn', 'rr'),
            ('antilope', 'france-td-60mn', 'qualite'),
            ('antilope', 'france-td-60mn', 'rr'),
            ('antilope', 'france-tr-15mn', 'qualite'),
            ('antilope', 'france-tr-15mn', 'rr'),
            ('antilope', 'france-tr-60mn', 'qualite'),
            ('antilope', 'france-tr-60mn', 'rr'),
            ('antilope', 'j1', 'rr'), ('antilope', 'temps-reel', 'rr'),
            ('antilope', 'v1', 'rr'),
            ('arome', 'iso0', 'altitude'), ('arome', 'lpn', 'altitude'),
            ('arome', 'rr', 'graupel'), ('arome', 'rr', 'liquide'),
            ('arome', 'rr', 'neige'), ('arome', 'rr', 'total'),
            ('arome', 't', 't'),
            ('arome-ifs', 'iso0', 'altitude'),
            ('arome-ifs', 'lpn', 'altitude'),
            ('arome-ifs', 'rr', 'graupel'), ('arome-ifs', 'rr', 'liquide'),
            ('arome-ifs', 'rr', 'neige'), ('arome-ifs', 'rr', 'total'),
            ('arome-ifs', 't', 't'),
            ('arome-pi', 'rr', 'total'),
            ('arpege', 'iso0', 'altitude'), ('arpege', 'lpn', 'altitude'),
            ('arpege', 'rr', 'liquide'), ('arpege', 'rr', 'solide'),
            ('arpege', 'rr', 'total'), ('arpege', 't', 't'),
            ('comephore', 'france', 'rr'),
            ('modis-snow', 'albedo', 'albedo'), ('modis-snow', 'fsc', 'fsc'),
            ('panthere', 'france', 'rr'), ('panthere', 'france-iq', 'qualite'),
            ('panthere', 'france-iq', 'rr'),
            ('piaf', 'france-antilope-15mn', 'rr'),
            ('sim', 'evap', 'rr'), ('sim', 'hra', 'humidite'),
            ('sim', 'hu', 'brut'), ('sim', 'hu', 'ref-clim'),
            ('sim', 'hu', 'ref-jour'), ('sim', 'hu', 'wg'),
            ('sim', 'neige', 'hauteur'), ('sim', 'neige', 'rr'),
            ('sim', 'ray', 'infrarouge'), ('sim', 'ray', 'solaire'),
            ('sim', 'rr', 'ecoulement'), ('sim', 'rr', 'fonte'),
            ('sim', 'rr', 'liquide'), ('sim', 'rr', 'solide'),
            ('sim', 'rr', 'total'), ('sim', 't', 't'),
            ('sim', 'vent', 'vitesse'),
            ('sympo', 'iso0', 'altitude'), ('sympo', 'lpn', 'altitude'),
            ('sympo', 'rr', 'rr'), ('sympo', 't', 't'),
        ]
        # ====================================================================
        bdimage = BdImage()
        self.assertEqual(valid_str, bdimage.get_datatypes(asstr=True))
        self.assertEqual(valid_tuple, bdimage.get_datatypes())
        for v in valid_str:
            try:
                BdImage.check_image(image=v)
            except ValueError:
                self.fail("Erreur dans le contrôle de l'image")
        for v in valid_tuple:
            try:
                BdImage.check_image(image=v)
            except ValueError:
                self.fail("Erreur dans le contrôle de l'image")
        # ====================================================================
