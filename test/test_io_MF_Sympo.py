#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_MF_Sympo in pyspc.io.meteofrance

To run all tests just type:
    python -m unittest test_data_MF_Sympo

To run only a class test:
    python -m unittest test_data_MF_Sympo.TestMF_Sympo

To run only a specific test:
    python -m unittest test_data_MF_Sympo.TestMF_Sympo.test_init

"""
# Imports
from datetime import datetime as dt
import os
import unittest

from pyspc.core.series import Series
from pyspc.io.meteofrance import read_Sympo


class TestMF_Sympo(unittest.TestCase):
    """
    MF_Sympo_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'mf')
        self.rr3_filename = os.path.join(self.dirname, 'rr3_200811010425')
        self.sympo_filename = os.path.join(self.dirname, 'sympo_201611200658')

    def test_rr3(self):
        """
        Test de la lecture d'un fichier RR3
        """
        # =====================================================================
        zones = ['0708', '0709']
        # =====================================================================
        series = read_Sympo(filename=self.rr3_filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        for s in zones:
            self.assertIn(
                (s, 'P3H', (dt(2008, 11, 1, 4, 25), 'Sympo', 'RR3', None)),
                series)
        # =====================================================================

    def test_sympo(self):
        """
        Test de la lecture d'un fichier RR3
        """
        # =====================================================================
        zones = ['0708', '0709']
        # =====================================================================
        series = read_Sympo(filename=self.sympo_filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        for s in zones:
            self.assertIn(
                (s, 'P3H', (dt(2016, 11, 20, 6, 58), 'Sympo', 'RR3', None)),
                series)
        # =====================================================================

    def test_sympo_zones(self):
        """
        Test de la lecture d'un fichier RR3
        """
        # =====================================================================
        zones = ['0708']
        # =====================================================================
        series = read_Sympo(filename=self.sympo_filename, zones=zones)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        for s in zones:
            self.assertIn(
                (s, 'P3H', (dt(2016, 11, 20, 6, 58), 'Sympo', 'RR3', None)),
                series)
        # =====================================================================
