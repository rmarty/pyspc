#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for ETP by Oudin in pyspc.model.oudin

To run all tests just type:
    python -m unittest test_model_Oudin

To run only a class test:
    python -m unittest test_model_Oudin.TestOudin

To run only a specific test:
    python -m unittest test_model_Oudin.TestOudin.test_oudin_etpj

"""
# Imports
from datetime import datetime as dt
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
import pyspc.model.oudin as _oudin


class TestOudin(unittest.TestCase):
    """
    Functions class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """

    def test_oudin_etpj(self):
        """
        Test TJ -> EJ
        """
        # =====================================================================
        valid_ej = pnd.DataFrame(
            {'Kxxx_EJ': [1.10308, 0.97756, 0.92200]},
            index=pnd.date_range(
                dt(2014, 11, 1),
                dt(2014, 11, 3),
                freq='D'
            )
        )
        # =====================================================================
        latitude = 45.025
        df_tj = pnd.DataFrame(
            {'Kxxx_EJ': [11.625, 9.929, 9.267]},
            index=pnd.date_range(
                dt(2014, 11, 1),
                dt(2014, 11, 3),
                freq='D'
            )
        )
        df_ej = _oudin.daily_ETP(
            data_tj=df_tj,
            latitude=latitude
        )
        self.assertIsInstance(df_ej, pnd.DataFrame)
        assert_frame_equal(valid_ej, df_ej)
        # =====================================================================

    def test_oudin_daily2hourly(self):
        """
        Test EJ -> EH
        """
        # =====================================================================
        valid_eh = pnd.DataFrame({
            'Kxxx_EJ': [0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.031500, 0.055800, 0.071100,
                        0.087300, 0.099000, 0.105300, 0.105300, 0.099000,
                        0.087300, 0.071100, 0.055800, 0.031500, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.018200, 0.032240, 0.041080, 0.050440,
                        0.057200, 0.060840, 0.060840, 0.057200, 0.050440,
                        0.041080, 0.032240, 0.018200, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
                        0.013160, 0.023312, 0.029704, 0.036472, 0.041360,
                        0.043992, 0.043992, 0.041360, 0.036472, 0.029704,
                        0.023312, 0.013160, 0.000000, 0.000000, 0.000000,
                        0.000000, 0.000000]},
            index=pnd.date_range(
                dt(2014, 11, 3, 0),
                dt(2014, 11, 5, 23),
                freq='H'
            )
        )
        # =====================================================================
        df_ej = pnd.DataFrame(
            {'Kxxx_EJ': [0.9, 0.520, 0.376]},
            index=pnd.date_range(
                dt(2014, 11, 3),
                dt(2014, 11, 5),
                freq='D'
            )
        )
        df_eh = _oudin.daily2hourly(data_ej=df_ej)
        self.assertIsInstance(df_eh, pnd.DataFrame)
        assert_frame_equal(valid_eh, df_eh)
        # =====================================================================
