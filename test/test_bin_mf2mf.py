#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary mf2mf

To run all tests just type:
    python -m unittest test_bin_mf2mf

To run only a class test:
    python -m unittest test_bin_mf2mf.Test_mf2mf

To run only a specific test:
    python -m unittest test_bin_mf2mf.Test_mf2mf.test_PJ

"""
# Imports
import filecmp
import os
import sys
import unittest

from pyspc.binutils.args import mf2mf as _args
from pyspc.binutils.runs import mf2mf as _runs
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_mf2mf_Data(unittest.TestCase):
    """
    mf2mf bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'mf2mf')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dirname2 = os.path.join('data', 'data', 'mf')
        cls.dirname3 = os.path.join('data', 'data', 'mf', 'open_data')
        cls.dirname4 = os.path.join('data', 'data', 'mf', 'open_api')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_EJ(self):
        """
        Test conversion ETP Journalière (ETPMON)
        """
        # =====================================================================
        datatype = 'MF_Data'
        processArgs = [
            'python',
            '../bin/mf2mf.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'ETPMON.data',
            '-t', datatype
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2mf()
        self.assertEqual(args.mfdata_filename, 'ETPMON.data')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.datatype, datatype)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2mf(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dirname2, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Re-écriture du fichier de données Météo-France {d} "\
            "de type '{t}' "\
            "situé dans le répertoire {I} vers le répertoire {O}".format(
                t=args.datatype,
                I=args.input_dir, d=args.mfdata_filename, O=args.output_dir)
        # =====================================================================

    def test_P6M(self):
        """
        Test conversion Pluie 6-min (RR6)
        """
        # =====================================================================
        datatype = 'MF_Data'
        processArgs = [
            'python',
            '../bin/mf2mf.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'RR6.data',
            '-t', datatype
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2mf()
        self.assertEqual(args.mfdata_filename, 'RR6.data')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.datatype, datatype)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2mf(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dirname2, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Re-écriture du fichier de données Météo-France {d} "\
            "de type '{t}' "\
            "situé dans le répertoire {I} vers le répertoire {O}".format(
                t=args.datatype,
                I=args.input_dir, d=args.mfdata_filename, O=args.output_dir)
        # =====================================================================

    def test_PH(self):
        """
        Test conversion Pluie Horaire (RR1)
        """
        # =====================================================================
        datatype = 'MF_Data'
        processArgs = [
            'python',
            '../bin/mf2mf.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'RR1.data',
            '-t', datatype
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2mf()
        self.assertEqual(args.mfdata_filename, 'RR1.data')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.datatype, datatype)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2mf(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dirname2, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Re-écriture du fichier de données Météo-France {d} "\
            "de type '{t}' "\
            "situé dans le répertoire {I} vers le répertoire {O}".format(
                t=args.datatype,
                I=args.input_dir, d=args.mfdata_filename, O=args.output_dir)
        # =====================================================================

    def test_PJ(self):
        """
        Test conversion Pluie Journalière (RR)
        """
        # =====================================================================
        datatype = 'MF_Data'
        processArgs = [
            'python',
            '../bin/mf2mf.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'RR.data',
            '-t', datatype
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2mf()
        self.assertEqual(args.mfdata_filename, 'RR.data')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.datatype, datatype)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2mf(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dirname2, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Re-écriture du fichier de données Météo-France {d} "\
            "de type '{t}' "\
            "situé dans le répertoire {I} vers le répertoire {O}".format(
                t=args.datatype,
                I=args.input_dir, d=args.mfdata_filename, O=args.output_dir)
        # =====================================================================

    def test_TH(self):
        """
        Test conversion Température Horaire (T)
        """
        # =====================================================================
        datatype = 'MF_Data'
        processArgs = [
            'python',
            '../bin/mf2mf.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'T.data',
            '-t', datatype
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2mf()
        self.assertEqual(args.mfdata_filename, 'T.data')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.datatype, datatype)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2mf(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dirname2, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Re-écriture du fichier de données Météo-France {d} "\
            "de type '{t}' "\
            "situé dans le répertoire {I} vers le répertoire {O}".format(
                t=args.datatype,
                I=args.input_dir, d=args.mfdata_filename, O=args.output_dir)
        # =====================================================================

    def test_TJ(self):
        """
        Test conversion Température Journalière (TNTXM)
        """
        # =====================================================================
        datatype = 'MF_Data'
        processArgs = [
            'python',
            '../bin/mf2mf.py',
            '-I', self.dirname2,
            '-O', self.dir_out,
            '-d', 'TNTXM.data',
            '-t', datatype
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2mf()
        self.assertEqual(args.mfdata_filename, 'TNTXM.data')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.datatype, datatype)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2mf(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dirname2, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Re-écriture du fichier de données Météo-France {d} "\
            "de type '{t}' "\
            "situé dans le répertoire {I} vers le répertoire {O}".format(
                t=args.datatype,
                I=args.input_dir, d=args.mfdata_filename, O=args.output_dir)
        # =====================================================================

    def test_openapi_MN(self):
        """
        Test de lecture/écriture de données RR6/P6M
        """
        # =====================================================================
        datatype = 'MF_OpenAPI'
        basename = '43091005_MN_202410170500_202410170700.csv'
        processArgs = [
            'python',
            '../bin/mf2mf.py',
            '-I', self.dirname4,
            '-O', self.dir_out,
            '-d', basename,
            '-t', datatype,
            '-v'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2mf()
        self.assertEqual(args.mfdata_filename, basename)
        self.assertEqual(args.input_dir, self.dirname4)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.datatype, datatype)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2mf(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Re-écriture du fichier OpenAPI de Météo-France {d} "\
            "de type '{t}' "\
            "situé dans le répertoire {I} vers le répertoire {O}".format(
                t=args.datatype,
                I=args.input_dir, d=args.mfdata_filename, O=args.output_dir)
        # =====================================================================

    def test_openapi_H(self):
        """
        Test de lecture/écriture de données RR1/PH
        """
        # =====================================================================
        datatype = 'MF_OpenAPI'
        basename = '43091005_H_202410170000_202410171200.csv'
        processArgs = [
            'python',
            '../bin/mf2mf.py',
            '-I', self.dirname4,
            '-O', self.dir_out,
            '-d', basename,
            '-t', datatype,
            '-v'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2mf()
        self.assertEqual(args.mfdata_filename, basename)
        self.assertEqual(args.input_dir, self.dirname4)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.datatype, datatype)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2mf(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Re-écriture du fichier OpenAPI de Météo-France {d} "\
            "de type '{t}' "\
            "situé dans le répertoire {I} vers le répertoire {O}".format(
                t=args.datatype,
                I=args.input_dir, d=args.mfdata_filename, O=args.output_dir)
        # =====================================================================

    def test_openapi_Q(self):
        """
        Test de lecture/écriture de données RR/PJ
        """
        # =====================================================================
        datatype = 'MF_OpenAPI'
        basename = '43091005_Q_202410150000_202410190000.csv'
        processArgs = [
            'python',
            '../bin/mf2mf.py',
            '-I', self.dirname4,
            '-O', self.dir_out,
            '-d', basename,
            '-t', datatype,
            '-v'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2mf()
        self.assertEqual(args.mfdata_filename, basename)
        self.assertEqual(args.input_dir, self.dirname4)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.datatype, datatype)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2mf(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Re-écriture du fichier OpenAPI de Météo-France {d} "\
            "de type '{t}' "\
            "situé dans le répertoire {I} vers le répertoire {O}".format(
                t=args.datatype,
                I=args.input_dir, d=args.mfdata_filename, O=args.output_dir)
        # =====================================================================

    def test_opendata_MN(self):
        """
        Test de lecture/écriture de données RR6/P6M
        """
        # =====================================================================
        datatype = 'MF_OpenData'
        basename = 'MN_43_previous-2020-2022.csv.gz'
        processArgs = [
            'python',
            '../bin/mf2mf.py',
            '-I', self.dirname3,
            '-O', self.dir_out,
            '-d', basename,
            '-t', datatype,
            '-v'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2mf()
        self.assertEqual(args.mfdata_filename, basename)
        self.assertEqual(args.input_dir, self.dirname3)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.datatype, datatype)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2mf(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Re-écriture du fichier OpenData de Météo-France {d} "\
            "de type '{t}' "\
            "situé dans le répertoire {I} vers le répertoire {O}".format(
                t=args.datatype,
                I=args.input_dir, d=args.mfdata_filename, O=args.output_dir)
        # =====================================================================

    def test_opendata_H(self):
        """
        Test de lecture/écriture de données RR1/PH + T/TH
        """
        # =====================================================================
        datatype = 'MF_OpenData'
        basename = 'H_43_2010-2019.csv.gz'
        processArgs = [
            'python',
            '../bin/mf2mf.py',
            '-I', self.dirname3,
            '-O', self.dir_out,
            '-d', basename,
            '-t', datatype,
            '-v'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2mf()
        self.assertEqual(args.mfdata_filename, basename)
        self.assertEqual(args.input_dir, self.dirname3)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.datatype, datatype)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2mf(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Re-écriture du fichier OpenData de Météo-France {d} "\
            "de type '{t}' "\
            "situé dans le répertoire {I} vers le répertoire {O}".format(
                t=args.datatype,
                I=args.input_dir, d=args.mfdata_filename, O=args.output_dir)
        # =====================================================================

    def test_opendata_Q(self):
        """
        Test de lecture/écriture de données RR/PJ
        """
        # =====================================================================
        datatype = 'MF_OpenData'
        basename = 'Q_07_latest-2023-2024_RR-T-Vent.csv.gz'
        processArgs = [
            'python',
            '../bin/mf2mf.py',
            '-I', self.dirname3,
            '-O', self.dir_out,
            '-d', basename,
            '-t', datatype,
            '-v'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.mf2mf()
        self.assertEqual(args.mfdata_filename, basename)
        self.assertEqual(args.input_dir, self.dirname3)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.datatype, datatype)
        # =====================================================================
        with captured_output():
            filenames = _runs.mf2mf(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Re-écriture du fichier OpenData de Météo-France {d} "\
            "de type '{t}' "\
            "situé dans le répertoire {I} vers le répertoire {O}".format(
                t=args.datatype,
                I=args.input_dir, d=args.mfdata_filename, O=args.output_dir)
        # =====================================================================
