#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Archive in pyspc.model.grp18

To run all tests just type:
    python -m unittest test_model_GRP18_RT_Archive

To run only a class test:
    python -m unittest test_model_GRP18_RT_Archive.TestGRPRT_Archive

To run only a specific test:
    python -m unittest test_model_GRP18_RT_Archive.TestGRPRT_Archive.test_init

"""
# Imports
from datetime import datetime as dt
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.model.grp18 import GRPRT_Archive


class TestGRPRT_Archive(unittest.TestCase):
    """
    GRPRT_Archive class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp18', 'rt')
        self.valid_P = pnd.DataFrame(
            {'90065003': [
                0.8, 0.6, 1.0, 4.8, 7.2, 8.0, 11.2, 7.4, 7.6, 6.8, 8.8, 7.6,
                5.8, 7.8, 8.2, 5.2, 6.6, 7.2, 9.0, 6.0, 5.0, 3.6, 2.8, 2.0,
                1.8]},
            index=pnd.date_range(
                dt(2007, 1, 18, 12),
                dt(2007, 1, 19, 12),
                freq='H'
            )
        )
        self.valid_P.index.name = 'Date (TU)'
        self.valid_Q = pnd.DataFrame(
            {'RH10585x': [
                6.56, 7.92, 13.60, 13.70, 15.80, 16.70, 17.30, 19.20, 19.80,
                20.40, 21.50, 23.00, 23.60, 23.40, 23.60, 24.70, 28.60, 30.60,
                35.10, 36.60, 35.90, 32.10, 28.60, 26.70, 25.30, 23.50, 22.80,
                20.80, 20.40, 19.20, 19.30, 19.10, 18.60, 18.80, 17.90]},
            index=[
                dt(2007, 1, 18, 19, 0, 0), dt(2007, 1, 18, 19, 30, 0),
                dt(2007, 1, 18, 22, 0, 0), dt(2007, 1, 18, 22, 3, 0),
                dt(2007, 1, 18, 23, 0, 0), dt(2007, 1, 18, 23, 30, 0),
                dt(2007, 1, 19, 0, 0, 0), dt(2007, 1, 19, 1, 0, 0),
                dt(2007, 1, 19, 1, 30, 0), dt(2007, 1, 19, 2, 45, 0),
                dt(2007, 1, 19, 3, 24, 0), dt(2007, 1, 19, 4, 0, 0),
                dt(2007, 1, 19, 4, 30, 0), dt(2007, 1, 19, 5, 30, 0),
                dt(2007, 1, 19, 6, 0, 0), dt(2007, 1, 19, 6, 30, 0),
                dt(2007, 1, 19, 7, 30, 0), dt(2007, 1, 19, 7, 45, 0),
                dt(2007, 1, 19, 8, 30, 0), dt(2007, 1, 19, 10, 0, 0),
                dt(2007, 1, 19, 11, 30, 0), dt(2007, 1, 19, 12, 15, 0),
                dt(2007, 1, 19, 12, 49, 0), dt(2007, 1, 19, 13, 30, 0),
                dt(2007, 1, 19, 14, 15, 0), dt(2007, 1, 19, 15, 30, 0),
                dt(2007, 1, 19, 16, 30, 0), dt(2007, 1, 19, 18, 0, 0),
                dt(2007, 1, 19, 18, 30, 0), dt(2007, 1, 19, 19, 30, 0),
                dt(2007, 1, 19, 20, 0, 0), dt(2007, 1, 19, 20, 30, 0),
                dt(2007, 1, 19, 21, 0, 0), dt(2007, 1, 19, 22, 0, 0),
                dt(2007, 1, 19, 23, 0, 0)]
        )
        self.valid_Q.index.name = 'Date (TU)'

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'QV_10A.DAT')
        reader = GRPRT_Archive(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'QV')
        self.assertIsNone(reader.timestep)
        # =====================================================================
        filename = os.path.join(self.dirname, 'PV_10A_00J01H00M.DAT')
        reader = GRPRT_Archive(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'PV')
        self.assertEqual(reader.timestep, '00J01H00M')
        # =====================================================================

    def test_read_PV(self):
        """
        Test de lecture d'un fichier Archive de GRP Temps Réel - Pluie hor.
        """
        filename = os.path.join(self.dirname, 'PV_10A_00J01H00M.DAT')
        reader = GRPRT_Archive(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_P, df)

    def test_read_QV(self):
        """
        Test de lecture d'un fichier Archive de GRP Temps Réel - Débit inst.
        """
        filename = os.path.join(self.dirname, 'QV_10A.DAT')
        reader = GRPRT_Archive(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_Q, df)

    def test_write(self):
        """
        Test de l'écriture d'un fichier Archive de GRP Temps Réel
        """
        filename = os.path.join(self.dirname, 'QV_10A.DAT')
        writer = GRPRT_Archive(filename=filename)
        with self.assertRaises(NotImplementedError):
            writer.write()

    def test_basename(self):
        """
        Test nom de base
        """
        valid = {
            ('QV', None): 'QV_10A.DAT',
            ('PV', '00J01H00M'): 'PV_10A_00J01H00M.DAT'
        }
        for k, v in valid.items():
            self.assertEqual(
                 GRPRT_Archive.split_basename(filename=v),
                 k
            )

    def test_varname(self):
        """
        Test de la correspondance des variables selon GRP et selon SPC_LCI
        """
        valid = ['PV', 'QV']
        self.assertEqual(GRPRT_Archive.get_varnames(), valid)
