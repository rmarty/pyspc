#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Serie in pyspc.core.serie - time series

To run all tests just type:
    python -m unittest test_core_Serie_TIME

To run only a class test:
    python -m unittest test_core_Serie_TIME.TestSerie

To run only a specific test:
    python -m unittest test_core_Serie_TIME.TestSerie.test_fvi_lvi

"""
# Imports
from datetime import datetime as dt, timedelta as td
import numpy as np
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import pytz
import unittest
import warnings

# Imports pyspc
from pyspc.core.serie import Serie


class TestSerie(unittest.TestCase):
    """
    Serie class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        self.code = 'K0000000'
        self.provider = 'SPC'
        self.varname = 'QH'
        self.list_dtval = [
            [dt(2014, 11, 4, 0), 35.600],
            [dt(2014, 11, 4, 1), 41.200],
            [dt(2014, 11, 4, 2), 46.900],
            [dt(2014, 11, 4, 3), 62.900],
            [dt(2014, 11, 4, 4), 85.700],
            [dt(2014, 11, 4, 5), 106.000],
            [dt(2014, 11, 4, 6), 118.000],
            [dt(2014, 11, 4, 7), 115.000],
            [dt(2014, 11, 4, 8), 103.000],
            [dt(2014, 11, 4, 9), 92.500],
            [dt(2014, 11, 4, 10), 86.400],
            [dt(2014, 11, 4, 11), 82.300],
            [dt(2014, 11, 4, 12), 78.300]
        ]

    def test_fill_cst(self):
        """
        Test méthode fill_constant
        """
        # =====================================================================
        code_valid = '{0}_fct-1'.format(self.code)
        valdata = [
            [dt(2014, 11, 4, 0), 10],
            [dt(2014, 11, 4, 1), 12],
            [dt(2014, 11, 4, 2), -1],
            [dt(2014, 11, 4, 3), -1],
            [dt(2014, 11, 4, 4), -1],
            [dt(2014, 11, 4, 5), -1],
            [dt(2014, 11, 4, 6), 28],
            [dt(2014, 11, 4, 7), 31],
            [dt(2014, 11, 4, 8), 34],
            [dt(2014, 11, 4, 9), -1],
            [dt(2014, 11, 4, 10), -1],
            [dt(2014, 11, 4, 11), 41],
            [dt(2014, 11, 4, 12), 42]
        ]
        mvdata = [
            [dt(2014, 11, 4, 0), 10],
            [dt(2014, 11, 4, 1), 12],
            [dt(2014, 11, 4, 2), np.nan],
            [dt(2014, 11, 4, 3), np.nan],
            [dt(2014, 11, 4, 4), np.nan],
            [dt(2014, 11, 4, 5), np.nan],
            [dt(2014, 11, 4, 6), 28],
            [dt(2014, 11, 4, 7), 31],
            [dt(2014, 11, 4, 8), 34],
            [dt(2014, 11, 4, 9), np.nan],
            [dt(2014, 11, 4, 10), np.nan],
            [dt(2014, 11, 4, 11), 41],
            [dt(2014, 11, 4, 12), 42]
        ]
        # =====================================================================
        valid = pnd.DataFrame([x[1] for x in mvdata],
                              index=[x[0] for x in mvdata],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([x[1] for x in valdata],
                                index=[x[0] for x in valdata],
                                dtype=np.float32)
        valid_1.columns = [(code_valid, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie.fill_constant(constant=-1)
        assert_frame_equal(serie.data_frame, valid_1)
        self.assertEqual(serie.code, code_valid)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie.fill_constant(constant=-1, inplace=True)
        valid_1.columns = [(self.code, self.varname)]
        assert_frame_equal(serie.data_frame, valid_1)
        self.assertEqual(serie.code, self.code)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        with self.assertRaises(ValueError):
            serie.fill_constant()
        with self.assertRaises(ValueError):
            serie.fill_constant(constant=None)
        # =====================================================================

    def test_fill_linear(self):
        """
        Test méthode fill_linear_interpolation
        """
        # =====================================================================
        code_valid = '{0}_fli'.format(self.code)
        valdata = [
            [dt(2014, 11, 4, 0), np.nan],
            [dt(2014, 11, 4, 1), 12],
            [dt(2014, 11, 4, 2), 15.2],
            [dt(2014, 11, 4, 3), 18.4],
            [dt(2014, 11, 4, 4), 21.6],
            [dt(2014, 11, 4, 5), 24.8],
            [dt(2014, 11, 4, 6), 28],
            [dt(2014, 11, 4, 7), 31],
            [dt(2014, 11, 4, 8), 34],
            [dt(2014, 11, 4, 9), 36],
            [dt(2014, 11, 4, 10), 38],
            [dt(2014, 11, 4, 11), 40],
            [dt(2014, 11, 4, 12), np.nan]
        ]
        mvdata = [
            [dt(2014, 11, 4, 0), np.nan],
            [dt(2014, 11, 4, 1), 12],
            [dt(2014, 11, 4, 2), np.nan],
            [dt(2014, 11, 4, 3), np.nan],
            [dt(2014, 11, 4, 4), np.nan],
            [dt(2014, 11, 4, 5), np.nan],
            [dt(2014, 11, 4, 6), 28],
            [dt(2014, 11, 4, 7), 31],
            [dt(2014, 11, 4, 8), 34],
            [dt(2014, 11, 4, 9), np.nan],
            [dt(2014, 11, 4, 10), np.nan],
            [dt(2014, 11, 4, 11), 40],
            [dt(2014, 11, 4, 12), np.nan]
        ]
        # =====================================================================
        valid = pnd.DataFrame([x[1] for x in mvdata],
                              index=[x[0] for x in mvdata],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([x[1] for x in valdata],
                                index=[x[0] for x in valdata],
                                dtype=np.float32)
        valid_1.columns = [(code_valid, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie.fill_linear_interpolation()
        assert_frame_equal(serie.data_frame, valid_1)
        self.assertEqual(serie.code, code_valid)
        # =====================================================================

    def test_fvi_lvi(self):
        """
        Test Valid Index
        """
        # =====================================================================
        fvi = dt(2014, 11, 4, 5)
        lvi = dt(2014, 11, 4, 8)
        valid = pnd.DataFrame([x[1] if x[1] > 100 else np.nan
                               for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [self.varname]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        self.assertEqual(serie.first_valid_index(), fvi)
        self.assertEqual(serie.last_valid_index(), lvi)
        # =====================================================================

    def test_tzone(self):
        """
        Test méthode set_timezone
        """
        # =====================================================================
        timezone = pytz.timezone('Europe/Paris')
        # =====================================================================
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [self.varname]
        valid_1 = pnd.DataFrame([x[1] for x in self.list_dtval],
                                index=[x[0] + td(hours=1)
                                       for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [self.varname]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie.set_timezone(timezone=timezone)
        self.assertEqual(serie.timezone, timezone)
        # =====================================================================

    def test_shift(self):
        """
        Test méthode shift
        """
        # =====================================================================
        code_valid = '{0}_shifted'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([x[1] for x in self.list_dtval],
                                index=[x[0] + td(hours=1)
                                       for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [(code_valid, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie.shift(periods=1, freq=td(hours=1), inplace=False)
        assert_frame_equal(serie.data_frame, valid_1)
        self.assertEqual(serie.code, code_valid)
        # =====================================================================

    def test_tlag(self):
        """
        Test méthode timelag
        """
        # =====================================================================
        code_valid = '{0}_tlag1'.format(self.code)
        valid = pnd.DataFrame([x[1] for x in self.list_dtval],
                              index=[x[0] for x in self.list_dtval],
                              dtype=np.float32)
        valid.columns = [(self.code, self.varname)]
        valid_1 = pnd.DataFrame([x[1] for x in self.list_dtval],
                                index=[x[0] + td(hours=1)
                                       for x in self.list_dtval],
                                dtype=np.float32)
        valid_1.columns = [(code_valid, self.varname)]
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie.timelag(tlag=td(hours=1), inplace=False)
        assert_frame_equal(serie.data_frame, valid_1)
        self.assertEqual(serie.code, code_valid)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        serie = serie.timelag(tlag=1, inplace=False)
        assert_frame_equal(serie.data_frame, valid_1)
        self.assertEqual(serie.code, code_valid)
        # =====================================================================
        serie = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=self.varname,
        )
        with self.assertRaises(ValueError):
            serie.timelag()
        with self.assertRaises(ValueError):
            serie.timelag(tlag=None)
        with self.assertRaises(ValueError):
            serie.timelag(tlag='1')
        # =====================================================================
