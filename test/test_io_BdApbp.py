#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_BdApbp in pyspc.io.lamedo

To run all tests just type:
    python -m unittest test_data_BdApbp

To run only a class test:
    python -m unittest test_data_BdApbp.TestBdApbp

To run only a specific test:
    python -m unittest test_data_BdApbp.TestBdApbp.test_init

"""
# Imports
from datetime import datetime as dt
import os
import unittest

from pyspc.core.series import Series
from pyspc.io.lamedo import read_BdApbp


class TestBdApbp(unittest.TestCase):
    """
    BdApbp_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'lamedo')

    def test_read_short(self):
        """
        Test de lecture - Format de retour 'short'
        """
        # =====================================================================
        zones = ['41003', '41005']
        filename = os.path.join(self.dirname, 'bp_short.json')
        series = read_BdApbp(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 8)
        for s in zones:
            self.assertIn(
                (s, 'PJ', (dt(2020, 6, 12, 6), 'BdApbp', 'MoyInf', None)),
                series)
            self.assertIn(
                (s, 'PJ', (dt(2020, 6, 12, 6), 'BdApbp', 'MoySup', None)),
                series)
            self.assertIn(
                (s, 'PJ', (dt(2020, 6, 12, 6), 'BdApbp', 'LocInf', None)),
                series)
            self.assertIn(
                (s, 'PJ', (dt(2020, 6, 12, 6), 'BdApbp', 'LocSup', None)),
                series)
        # =====================================================================

    def test_read_short_zones(self):
        """
        Test de lecture - Format de retour 'short'
        """
        # =====================================================================
        zones = ['41005']
        filename = os.path.join(self.dirname, 'bp_short.json')
        series = read_BdApbp(filename=filename, zones=zones)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 4)
        for s in zones:
            self.assertIn(
                (s, 'PJ', (dt(2020, 6, 12, 6), 'BdApbp', 'MoyInf', None)),
                series)
            self.assertIn(
                (s, 'PJ', (dt(2020, 6, 12, 6), 'BdApbp', 'MoySup', None)),
                series)
            self.assertIn(
                (s, 'PJ', (dt(2020, 6, 12, 6), 'BdApbp', 'LocInf', None)),
                series)
            self.assertIn(
                (s, 'PJ', (dt(2020, 6, 12, 6), 'BdApbp', 'LocSup', None)),
                series)
        # =====================================================================

    def test_read_long(self):
        """
        Test de lecture - Format de retour 'long'
        """
        # =====================================================================
        zones = ['41003', '41005']
        filename = os.path.join(self.dirname, 'bp_long.json')
        series = read_BdApbp(filename=filename)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 8)
        for s in zones:
            self.assertIn(
                (s, 'PJ', (dt(2020, 6, 12, 5, 26), 'BdApbp', 'MoyInf', None)),
                series)
            self.assertIn(
                (s, 'PJ', (dt(2020, 6, 12, 5, 26), 'BdApbp', 'MoySup', None)),
                series)
            self.assertIn(
                (s, 'PJ', (dt(2020, 6, 12, 5, 26), 'BdApbp', 'LocInf', None)),
                series)
            self.assertIn(
                (s, 'PJ', (dt(2020, 6, 12, 5, 26), 'BdApbp', 'LocSup', None)),
                series)
        # =====================================================================
#        zones = ['41005']
#        series = read_BdApbp(filename=filename, zones=zones)
#        import pandas as pnd
#        dfs = pnd.concat([series[k].data_frame for k in series], axis=1)
#        dfs.columns = ['MoyInf', 'MoySup', 'LocInf', 'LocSup']
#        print(dfs)
#        dfs.to_csv(
#            "data/41005_202006120526_BP_PJ.txt",
#            sep=';',
#            float_format='%.3f',
#            index=True,
#            index_label='AAAAMMJJ',
#            date_format='%Y%m%d',
#            line_terminator='\n'
#        )
        # =====================================================================
