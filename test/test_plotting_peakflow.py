#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Config in pyspc.plotting

To run all tests just type:
    python -m unittest test_plotting_peakflow

To run only a class test:
    python -m unittest test_plotting_peakflow.TestPeakFlow

To run only a specific test:
    python -m unittest test_plotting_peakflow.TestPeakFlow.test_plot

"""
# Imports
from datetime import datetime as dt
import filecmp
import os.path
import unittest

from pyspc.core.parameter import Parameter
from pyspc.plotting.peakflow import plot_peakflow_analysis


class TestPeakFlow(unittest.TestCase):
    """
    Colormap class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'plotting', 'peakflow')

    def test_plot_byzone(self):
        """
        Test création figure BP vs PRCP vs RADAR - 1 image par zone
        """
        # =====================================================================
        data = {
            (dt(2017, 12, 18, 17), dt(2017, 12, 18, 21)): (15.4, 36.7),
            (dt(1994, 11, 19, 6), dt(1994, 11, 19, 9)): (16.0, 39.4),
            (dt(1995, 1, 11, 18), dt(1995, 1, 11, 20)): (16.4, 52.9),
            (dt(2002, 11, 10, 12), dt(2002, 11, 10, 15)): (16.6, 42.2),
            (dt(2014, 12, 18, 12), dt(2014, 12, 18, 14)): (17.1, 49.1),
            (dt(1999, 2, 9, 13), dt(1999, 2, 9, 15)): (17.5, 47.9),
            (dt(2011, 12, 21, 22), dt(2011, 12, 22, 16)): (17.6, 44.9),
            (dt(2010, 7, 22, 5), dt(2010, 7, 22, 7)): (17.9, 55.4),
            (dt(2018, 1, 4, 12), dt(2018, 1, 4, 15)): (18.8, 57.7),
            (dt(2016, 11, 22, 20), dt(2016, 11, 22, 22)): (18.9, 47.2),
            (dt(2016, 6, 2, 17), dt(2016, 6, 2, 20)): (19.6, 48.8),
            (dt(2004, 11, 4, 3), dt(2004, 11, 4, 1)): (23.1, 39.0),
            (dt(2007, 6, 14, 20), dt(2007, 6, 15, 0)): (23.2, 49.9),
            (dt(2003, 2, 4, 1), dt(2003, 2, 4, 4)): (23.6, 52.8),
            (dt(2011, 12, 31, 15), dt(2011, 12, 31, 11)): (23.6, 53.2),
            (dt(2012, 5, 22, 15), dt(2012, 5, 22, 18)): (24.7, 97.7),
            (dt(2005, 4, 17, 10), dt(2005, 4, 17, 12)): (26.7, 70.4),
            (dt(2003, 12, 2, 12), dt(2003, 12, 2, 18)): (28.9, 73.9),
            (dt(2004, 10, 30, 2), dt(2004, 10, 30, 7)): (30.1, 46.5),
            (dt(2016, 5, 28, 23), dt(2016, 5, 28, 23)): (30.4, 52.0),
            (dt(2001, 5, 5, 6), dt(2001, 5, 6, 6)): (33.1, 61.9),
            (dt(2007, 9, 28, 11), dt(2007, 9, 28, 15)): (33.1, 78.7),
            (dt(2008, 7, 3, 7), dt(2008, 7, 3, 11)): (33.1, 82.9),
            (dt(2008, 7, 3, 1), dt(2008, 7, 3, 11)): (34.0, 82.9),
        }
        f = os.path.join('data', 'peakflow_K152-K153.png')
        # =====================================================================
        valid = {
            'slope': 1.650,
            'intercept': 18.387,
            'r_value': 0.657,
            'r_squared': 0.432,
            'p_value': 0.00048,
            'std_err': 0.403,
            'confidence_pct': 80,
            'confidence_interval': 15.142,
        }
        # =====================================================================
        infos = plot_peakflow_analysis(
            data=data,
            fig_filename=f,
            upsta='K1524010',
            downsta='K1533010',
            param=Parameter(varname='QH'))
        self.assertTrue(filecmp.cmp(
            os.path.join('data', 'plotting', 'peakflow', os.path.basename(f)),
            f
        ))
        os.remove(f)
        for k, v in valid.items():
            self.assertAlmostEqual(infos[k], v, places=2)
        # =====================================================================
