#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary prv2csv

To run all tests just type:
    python -m unittest test_bin_prv2csv

To run only a class test:
    python -m unittest test_bin_prv2csv.Test_prv2csv

To run only a specific test:
    python -m unittest test_bin_prv2csv.Test_prv2csv.test_obs

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import prv2csv as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_prv2csv(unittest.TestCase):
    """
    prv2csv bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'prv2csv')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.otamin = os.path.join('data', 'model', 'otamin16')
        cls.scores = os.path.join('data', 'verification', 'scores')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_scores_obs_grp16(self):
        """
        Test série SCORES OBS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/prv2csv.py',
            '-I', self.scores,
            '-d', 'K6373020_Q.csv',
            '-O', self.dir_out,
            '-t', 'scores_obs',
            '-C', 'grp16'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.prv2csv()
        self.assertEqual(args.prv_filename, 'K6373020_Q.csv')
        self.assertEqual(args.data_type, 'scores_obs')
        self.assertEqual(args.input_dir, self.scores)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'grp16')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'K6373020_Q.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les données du format prv de type {t} "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, d=args.prv_filename, t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_scores_sim_pyspc(self):
        """
        Test série SCORES SIM
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/prv2csv.py',
            '-I', self.scores,
            '-d', 'K6373020_Q_sim.csv',
            '-O', self.dir_out,
            '-t', 'scores_sim',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.prv2csv()
        self.assertEqual(args.prv_filename, 'K6373020_Q_sim.csv')
        self.assertEqual(args.data_type, 'scores_sim')
        self.assertEqual(args.input_dir, self.scores)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'K6373020_sim_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les données du format prv de type {t} "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, d=args.prv_filename, t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_scores_fcst_pyspc(self):
        """
        Test série SCORES FCST
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/prv2csv.py',
            '-I', self.scores,
            '-d', 'K6373020_Q_9.csv',
            '-O', self.dir_out,
            '-t', 'scores_fcst',
            '-C', 'pyspc'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.prv2csv()
        self.assertEqual(args.prv_filename, 'K6373020_Q_9.csv')
        self.assertEqual(args.data_type, 'scores_fcst')
        self.assertEqual(args.input_dir, self.scores)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, False)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'K6373020_2016053112_45hEAOtt00_2007_50_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les données du format prv de type {t} "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "".format(
                I=args.input_dir, d=args.prv_filename, t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_otamin16_fcst_pyspc(self):
        """
        Test série SCORES FCST
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/prv2csv.py',
            '-I', self.otamin,
            '-d', 'GRP_B_20200911_1515_2.prv',
            '-O', self.dir_out,
            '-t', 'otamin16_fcst',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.prv2csv()
        self.assertEqual(args.prv_filename, 'GRP_B_20200911_1515_2.prv')
        self.assertEqual(args.data_type, 'otamin16_fcst')
        self.assertEqual(args.input_dir, self.otamin)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'A6701210_2020020312_57gGRPd000_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les données du format prv de type {t} "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "Un seul fichier de sortie est créé. "\
            "".format(
                I=args.input_dir, d=args.prv_filename, t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_otamin16_fcst_RRTA_pyspc(self):
        """
        Test série SCORES FCST
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/prv2csv.py',
            '-I', self.otamin,
            '-d', 'GRP_B_20200911_1515_DA_2.prv',
            '-O', self.dir_out,
            '-t', 'otamin16_fcst',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.prv2csv()
        self.assertEqual(args.prv_filename, 'GRP_B_20200911_1515_DA_2.prv')
        self.assertEqual(args.data_type, 'otamin16_fcst')
        self.assertEqual(args.input_dir, self.otamin)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
#        basename = 'A6701210_2020020312_57gGRPd000_TH.txt'
        basename = 'otamin16fcst_TH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les données du format prv de type {t} "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "Un seul fichier de sortie est créé. "\
            "".format(
                I=args.input_dir, d=args.prv_filename, t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================

    def test_otamin16_trend_pyspc(self):
        """
        Test série SCORES FCST
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/prv2csv.py',
            '-I', self.otamin,
            '-d', 'GRP_B_20200203_1200_2.prv',
            '-O', self.dir_out,
            '-t', 'otamin16_trend',
            '-C', 'pyspc',
            '-1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.prv2csv()
        self.assertEqual(args.prv_filename, 'GRP_B_20200203_1200_2.prv')
        self.assertEqual(args.data_type, 'otamin16_trend')
        self.assertEqual(args.input_dir, self.otamin)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
        self.assertEqual(args.onefile, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'A6701210_2020020312_QH.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(self.dir_out, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Convertir les données du format prv de type {t} "\
            "du fichier {d} situé dans le répertoire {I} "\
            "au format csv de type '{C}' dans le répertoire {O}. "\
            "Un seul fichier de sortie est créé. "\
            "".format(
                I=args.input_dir, d=args.prv_filename, t=args.data_type,
                O=args.output_dir, C=args.csv_type)
        # =====================================================================
