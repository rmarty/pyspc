#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary hydroExport

To run all tests just type:
    python -m unittest test_bin_hydroExport

To run only a class test:
    python -m unittest test_bin_hydroExport.Test_hydroExport

To run only a specific test:
    python -m unittest test_bin_hydroExport.Test_hydroExport.test_QTVAR

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import hydroExport as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_hydroExport(unittest.TestCase):
    """
    hydroExport bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'hydroExport')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_hydro2 = os.path.join('data', 'webservice', 'hydro2')
#        for filename in os.listdir(self.dir_out):
#            os.remove(os.path.join(self.dir_out, filename))
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.stations_list_file = None

    def tearDown(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)

    def test_QTVAR(self):
        """
        Test export QTVAR
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/hydroExport.py',
            '-s', 'K1321810',
            '-O', self.dir_out,
            '-t', 'QTVAR',
            '-F', '2020030506',
            '-L', '2020030706'
        ]
#        processArgs.append('-v')
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydroExport()
        self.assertEqual(args.first_dtime, '202003050600')
        self.assertEqual(args.last_dtime, '202003070600')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K1321810')
        self.assertEqual(args.data_type, 'QTVAR')
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        ctl_filename = os.path.join(
            self.dir_hydro2,
            '{}_{}.txt'.format(args.data_type, args.station_name))
        for filename in os.listdir(self.dir_out):
            if args.data_type in filename:
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dir_out, filename),
                    ctl_filename
                ))
                os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Création du fichier d'export Hydro2 correspondant à "\
            "la procédure {t} pour la station {s} sur la période {F} - {L} "\
            "dans le répertoire {o}".format(
                s=args.station_name, o=args.output_dir, t=args.data_type,
                F=args.first_dtime, L=args.last_dtime)
        # =====================================================================

    def test_QTFIX(self):
        """
        Test export QTFIX
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/hydroExport.py',
            '-s', 'K1321810',
            '-O', self.dir_out,
            '-t', 'QTFIX',
            '-F', '2020030506',
            '-L', '2020030706'
        ]
#        processArgs.append('-v')
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydroExport()
        self.assertEqual(args.first_dtime, '202003050600')
        self.assertEqual(args.last_dtime, '202003070600')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K1321810')
        self.assertEqual(args.data_type, 'QTFIX')
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        ctl_filename = os.path.join(
            self.dir_hydro2,
            '{}_{}.txt'.format(args.data_type, args.station_name))
        for filename in os.listdir(self.dir_out):
            if args.data_type in filename:
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dir_out, filename),
                    ctl_filename
                ))
                os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Création du fichier d'export Hydro2 correspondant à "\
            "la procédure {t} pour la station {s} sur la période {F} - {L} "\
            "dans le répertoire {o}".format(
                s=args.station_name, o=args.output_dir, t=args.data_type,
                F=args.first_dtime, L=args.last_dtime)
        # =====================================================================

    def test_HTEMPS(self):
        """
        Test export H-TEMPS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/hydroExport.py',
            '-s', 'K1321810',
            '-O', self.dir_out,
            '-t', 'H-TEMPS',
            '-F', '2020030506',
            '-L', '2020030706'
        ]
#        processArgs.append('-v')
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydroExport()
        self.assertEqual(args.first_dtime, '202003050600')
        self.assertEqual(args.last_dtime, '202003070600')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K1321810')
        self.assertEqual(args.data_type, 'H-TEMPS')
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        ctl_filename = os.path.join(
            self.dir_hydro2,
            '{}_{}.txt'.format(args.data_type, args.station_name))
        for filename in os.listdir(self.dir_out):
            if args.data_type in filename:
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dir_out, filename),
                    ctl_filename
                ))
                os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Création du fichier d'export Hydro2 correspondant à "\
            "la procédure {t} pour la station {s} sur la période {F} - {L} "\
            "dans le répertoire {o}".format(
                s=args.station_name, o=args.output_dir, t=args.data_type,
                F=args.first_dtime, L=args.last_dtime)
        # =====================================================================

    def test_QJM(self):
        """
        Test export QJM
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/hydroExport.py',
            '-s', 'K1321810',
            '-O', self.dir_out,
            '-t', 'QJM',
            '-F', '2020030506',
            '-L', '2020030706'
        ]
#        processArgs.append('-v')
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydroExport()
        self.assertEqual(args.first_dtime, '202003050600')
        self.assertEqual(args.last_dtime, '202003070600')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'K1321810')
        self.assertEqual(args.data_type, 'QJM')
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        ctl_filename = os.path.join(
            self.dir_hydro2,
            '{}_{}.txt'.format(args.data_type, args.station_name))
        for filename in os.listdir(self.dir_out):
            if args.data_type in filename:
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dir_out, filename),
                    ctl_filename
                ))
                os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Création du fichier d'export Hydro2 correspondant à "\
            "la procédure {t} pour la station {s} sur la période {F} - {L} "\
            "dans le répertoire {o}".format(
                s=args.station_name, o=args.output_dir, t=args.data_type,
                F=args.first_dtime, L=args.last_dtime)
        # =====================================================================

    def test_DEBCLA(self):
        """
        Test export DEBCLA
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_cfg, 'Arroux.txt')
        processArgs = [
            'python',
            '../bin/hydroExport.py',
            '-l', self.stations_list_file,
            '-O', self.dir_out,
            '-t', 'DEBCLA',
            '-F', '19480901',
            '-L', '20200831',
            '-U', 'onefile', 'debcla1.txt'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydroExport()
        self.assertEqual(args.first_dtime, '194809010000')
        self.assertEqual(args.last_dtime, '202008310000')
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'DEBCLA')
        self.assertEqual(args.user, [['onefile', 'debcla1.txt']])
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        ctl_filename = os.path.join(
            self.dir_hydro2,
            '{}_{}'.format(args.data_type,
                           os.path.basename(args.stations_list_file)))
        for filename in os.listdir(self.dir_out):
            if args.data_type in filename:
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dir_out, filename),
                    ctl_filename
                ))
                os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Création du fichier d'export Hydro2 correspondant à "\
            "la procédure {t} pour les stations listées dans le fichier {l} "\
            "sur la période {F} - {L} dans le répertoire {o}. La procédure "\
            "est appliquée une seule fois grâce à l'usage de {U}.".format(
                l=args.stations_list_file, o=args.output_dir, t=args.data_type,
                F=args.first_dtime, L=args.last_dtime, U=args.user[0])
        # =====================================================================

    def test_CRUCAL(self):
        """
        Test export CRUCAL
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_cfg, 'Arroux.txt')
        processArgs = [
            'python',
            '../bin/hydroExport.py',
            '-l', self.stations_list_file,
            '-O', self.dir_out,
            '-t', 'CRUCAL',
            '-F', '19480901',
            '-L', '20200831',
            '-U', 'onefile', 'crucal1.txt'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydroExport()
        self.assertEqual(args.first_dtime, '194809010000')
        self.assertEqual(args.last_dtime, '202008310000')
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'CRUCAL')
        self.assertEqual(args.user, [['onefile', 'crucal1.txt']])
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        ctl_filename = os.path.join(
            self.dir_hydro2,
            '{}_{}'.format(args.data_type,
                           os.path.basename(args.stations_list_file)))
        for filename in os.listdir(self.dir_out):
            if args.data_type in filename:
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dir_out, filename),
                    ctl_filename
                ))
                os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Création du fichier d'export Hydro2 correspondant à "\
            "la procédure {t} pour les stations listées dans le fichier {l} "\
            "sur la période {F} - {L} dans le répertoire {o}. La procédure "\
            "est appliquée une seule fois grâce à l'usage de {U}.".format(
                l=args.stations_list_file, o=args.output_dir, t=args.data_type,
                F=args.first_dtime, L=args.last_dtime, U=args.user[0])
        # =====================================================================

    def test_SYNTHESE(self):
        """
        Test export SYNTHESE
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_cfg, 'Arroux.txt')
        processArgs = [
            'python',
            '../bin/hydroExport.py',
            '-l', self.stations_list_file,
            '-O', self.dir_out,
            '-t', 'SYNTHESE',
            '-F', '19480901',
            '-L', '20200831',
            '-U', 'onefile', 'synth1.txt'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydroExport()
        self.assertEqual(args.first_dtime, '194809010000')
        self.assertEqual(args.last_dtime, '202008310000')
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'SYNTHESE')
        self.assertEqual(args.user, [['onefile', 'synth1.txt']])
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        ctl_filename = os.path.join(
            self.dir_hydro2,
            '{}_{}'.format(args.data_type,
                           os.path.basename(args.stations_list_file)))
        for filename in os.listdir(self.dir_out):
            if args.data_type in filename:
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dir_out, filename),
                    ctl_filename
                ))
                os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Création du fichier d'export Hydro2 correspondant à "\
            "la procédure {t} pour les stations listées dans le fichier {l} "\
            "sur la période {F} - {L} dans le répertoire {o}. La procédure "\
            "est appliquée une seule fois grâce à l'usage de {U}.".format(
                l=args.stations_list_file, o=args.output_dir, t=args.data_type,
                F=args.first_dtime, L=args.last_dtime, U=args.user[0])
        # =====================================================================

    def test_TOUSMOIS(self):
        """
        Test export TOUSMOIS
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_cfg, 'Arroux.txt')
        processArgs = [
            'python',
            '../bin/hydroExport.py',
            '-l', self.stations_list_file,
            '-O', self.dir_out,
            '-t', 'TOUSMOIS',
            '-F', '20180101',
            '-L', '20201231',
            '-U', 'onefile', 'tm1prec1.txt',
            '-U', 'precision', '1'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.hydroExport()
        self.assertEqual(args.first_dtime, '201801010000')
        self.assertEqual(args.last_dtime, '202012310000')
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'TOUSMOIS')
        self.assertEqual(args.user, [['onefile', 'tm1prec1.txt'],
                                     ['precision', '1']])
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        ctl_filename = os.path.join(
            self.dir_hydro2, '{}_precision1.txt'.format(args.data_type,))
        for filename in os.listdir(self.dir_out):
            if args.data_type in filename:
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dir_out, filename),
                    ctl_filename
                ))
                os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Création du fichier d'export Hydro2 correspondant à "\
            "la procédure {t} pour les stations listées dans le fichier {l} "\
            "sur la période {F} - {L} dans le répertoire {o}. La procédure "\
            "est appliquée une seule fois grâce à l'usage de {U}. "\
            "La précision de l'export est définie par {U1}.".format(
                l=args.stations_list_file, o=args.output_dir, t=args.data_type,
                F=args.first_dtime, L=args.last_dtime, U=args.user[0],
                U1=args.user[1])
        # =====================================================================
