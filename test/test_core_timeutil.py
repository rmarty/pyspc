#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for functions designed for times, dates, events

To run all tests just type:
    python -m unittest test_core_timeutil

To run only a class test:
    python -m unittest test_core_timeutil.TestTimeUtil

To run only a specific test:
    python -m unittest test_core_timeutil.TestTimeUtil.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import unittest

from pyspc.core.timeutil import (dtfmt, dtheader, lenstr2dtfmt, str2dt,
                                 overlap, group_events, group_dates)


class TestTimeUtil(unittest.TestCase):
    """
    TimeUtil functions test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """

    def test_lenstr2dtfmt(self):
        """
        Test lenstr2dtfmt
        """
        # =====================================================================
        self.assertEqual(lenstr2dtfmt('2018'), '%Y')
        self.assertEqual(lenstr2dtfmt('201801'), '%Y%m')
        self.assertEqual(lenstr2dtfmt('20180104'), '%Y%m%d')
        self.assertEqual(lenstr2dtfmt('2018010412'), '%Y%m%d%H')
        self.assertEqual(lenstr2dtfmt('201801041230'), '%Y%m%d%H%M')
        # =====================================================================

    def test_dtfmt(self):
        """
        Test dtfmt
        """
        # =====================================================================
        self.assertEqual(dtfmt(None), '%Y%m%d%H%M')
        # =====================================================================
        self.assertEqual(dtfmt(td(days=1)), '%Y%m%d')
        self.assertEqual(dtfmt(td(days=2)), '%Y%m%d')
        self.assertEqual(dtfmt(td(hours=24)), '%Y%m%d')
        self.assertEqual(dtfmt(td(hours=48)), '%Y%m%d')
        # =====================================================================
        self.assertEqual(dtfmt(td(hours=1)), '%Y%m%d%H')
        self.assertEqual(dtfmt(td(hours=3)), '%Y%m%d%H')
        self.assertEqual(dtfmt(td(hours=12)), '%Y%m%d%H')
        self.assertEqual(dtfmt(td(hours=36)), '%Y%m%d%H')
        # =====================================================================
        self.assertEqual(dtfmt(td(minutes=5)), '%Y%m%d%H%M')
        self.assertEqual(dtfmt(td(minutes=15)), '%Y%m%d%H%M')
        self.assertEqual(dtfmt(td(minutes=30)), '%Y%m%d%H%M')
        self.assertEqual(dtfmt(td(minutes=45)), '%Y%m%d%H%M')
        # =====================================================================

    def test_dtheader(self):
        """
        Test dtheader
        """
        # =====================================================================
        self.assertEqual(dtheader(None), 'AAAAMMJJHHMM')
        # =====================================================================
        self.assertEqual(dtheader(td(days=1)), 'AAAAMMJJ')
        self.assertEqual(dtheader(td(days=2)), 'AAAAMMJJ')
        self.assertEqual(dtheader(td(hours=24)), 'AAAAMMJJ')
        self.assertEqual(dtheader(td(hours=48)), 'AAAAMMJJ')
        # =====================================================================
        self.assertEqual(dtheader(td(hours=1)), 'AAAAMMJJHH')
        self.assertEqual(dtheader(td(hours=3)), 'AAAAMMJJHH')
        self.assertEqual(dtheader(td(hours=12)), 'AAAAMMJJHH')
        self.assertEqual(dtheader(td(hours=36)), 'AAAAMMJJHH')
        # =====================================================================
        self.assertEqual(dtheader(td(minutes=5)), 'AAAAMMJJHHMM')
        self.assertEqual(dtheader(td(minutes=15)), 'AAAAMMJJHHMM')
        self.assertEqual(dtheader(td(minutes=30)), 'AAAAMMJJHHMM')
        self.assertEqual(dtheader(td(minutes=45)), 'AAAAMMJJHHMM')
        # =====================================================================

    def test_str2dt(self):
        """
        Test str2dt
        """
        # =====================================================================
        self.assertEqual(str2dt(None), None)
        # =====================================================================
        self.assertEqual(str2dt('200811'), dt(2008, 11, 1))
        self.assertEqual(str2dt('20081101'), dt(2008, 11, 1))
        self.assertEqual(str2dt('2008110112'), dt(2008, 11, 1, 12))
        self.assertEqual(str2dt('200811011215'), dt(2008, 11, 1, 12, 15))
        # =====================================================================

    def test_overlap(self):
        """
        Test overlap
        """
        # =====================================================================
        self.assertTrue(overlap(dt(2001, 2, 1), dt(2001, 2, 10),
                                dt(2001, 1, 25), dt(2001, 2, 5)))
        self.assertFalse(overlap(dt(2001, 1, 1), dt(2001, 1, 10),
                                 dt(2001, 1, 25), dt(2001, 2, 5)))
        # =====================================================================

    def test_group_events(self):
        """
        Test group_events
        """
        # =====================================================================
        events = [
            (dt(2001, 1, 1), dt(2001, 1, 10)),
            (dt(2001, 2, 1), dt(2001, 2, 10)),
            (dt(2001, 1, 25), dt(2001, 2, 5)),
            (dt(2001, 2, 2), dt(2001, 2, 2)),
            (dt(2001, 2, 10), dt(2001, 2, 12)),
            (dt(2002, 1, 1), dt(2002, 1, 10)),
        ]
        # =====================================================================
        valid = [(dt(2001, 1, 1), dt(2001, 1, 10)),
                 (dt(2001, 1, 25), dt(2001, 2, 12)),
                 (dt(2002, 1, 1), dt(2002, 1, 10))]
        groups_of_event = group_events(events=events)
        self.assertEqual(groups_of_event, valid)
        # =====================================================================
        valid = [(dt(2001, 1, 1), dt(2001, 2, 12)),
                 (dt(2002, 1, 1), dt(2002, 1, 10))]
        groups_of_event = group_events(events=events, tol=td(days=15))
        self.assertEqual(groups_of_event, valid)
        # =====================================================================

    def test_group_dates(self):
        """
        Test group_dates
        """
        # =====================================================================
        dates = [
            dt(2001, 1, 1), dt(2001, 1, 2), dt(2001, 1, 3), dt(2001, 1, 4),
            dt(2001, 1, 5), dt(2001, 1, 6), dt(2001, 1, 7), dt(2001, 1, 8),
            dt(2001, 1, 9), dt(2001, 1, 10),
            dt(2001, 2, 1), dt(2001, 2, 2), dt(2001, 2, 3), dt(2001, 2, 4),
            dt(2001, 2, 5), dt(2001, 2, 6), dt(2001, 2, 7), dt(2001, 2, 8),
            dt(2001, 2, 9), dt(2001, 2, 10), dt(2001, 2, 11), dt(2001, 2, 12),
            dt(2001, 1, 25), dt(2001, 1, 26), dt(2001, 1, 27), dt(2001, 1, 28),
            dt(2001, 1, 29), dt(2001, 1, 30), dt(2001, 1, 31),
            dt(2002, 1, 1), dt(2002, 1, 2), dt(2002, 1, 3), dt(2002, 1, 4),
            dt(2002, 1, 5), dt(2002, 1, 6), dt(2002, 1, 7), dt(2002, 1, 8),
            dt(2002, 1, 9), dt(2002, 1, 10)
        ]
        valid = [
            [dt(2001, 1, 1), dt(2001, 1, 2), dt(2001, 1, 3), dt(2001, 1, 4),
             dt(2001, 1, 5), dt(2001, 1, 6), dt(2001, 1, 7), dt(2001, 1, 8),
             dt(2001, 1, 9), dt(2001, 1, 10)],
            [dt(2001, 1, 25), dt(2001, 1, 26), dt(2001, 1, 27),
             dt(2001, 1, 28), dt(2001, 1, 29), dt(2001, 1, 30),
             dt(2001, 1, 31), dt(2001, 2, 1), dt(2001, 2, 2), dt(2001, 2, 3),
             dt(2001, 2, 4), dt(2001, 2, 5), dt(2001, 2, 6), dt(2001, 2, 7),
             dt(2001, 2, 8), dt(2001, 2, 9), dt(2001, 2, 10), dt(2001, 2, 11),
             dt(2001, 2, 12)],
            [dt(2002, 1, 1), dt(2002, 1, 2), dt(2002, 1, 3), dt(2002, 1, 4),
             dt(2002, 1, 5), dt(2002, 1, 6), dt(2002, 1, 7), dt(2002, 1, 8),
             dt(2002, 1, 9), dt(2002, 1, 10)]]
        # =====================================================================
        group_of_dates = group_dates(dates=dates, tol=td(days=1))
        self.assertEqual(group_of_dates, valid)
        # =====================================================================
