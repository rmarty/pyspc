#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for ETP by Oudin in pyspc.model.oudin

To run all tests just type:
    python -m unittest test_model_Oudin

To run only a class test:
    python -m unittest test_model_Oudin.TestOudin

To run only a specific test:
    python -m unittest test_model_Oudin.TestOudin.test_oudin_etpj

"""
# Imports
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
import pyspc.model.sauquet as _sauquet


class TestSauquet(unittest.TestCase):
    """
    Functions class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """

    def test_sauquet(self):
        """
        Test K2163110
        """
        # =====================================================================
        df = pnd.DataFrame(
            {'K2163110': [1.430, 1.540, 1.700, 1.670, 1.320, 0.829,
                          0.380, 0.241, 0.324, 0.613, 1.140, 1.440]},
            index=range(1, 13))
        valid_norm = pnd.DataFrame(
            {'K2163110': [0.690228, 0.891221, 1.183574, 1.128758, 0.489235,
                          -0.407924, -1.228341, -1.482323, -1.330664,
                          -0.802601, 0.160338, 0.7085005]},
            index=range(1, 13))
        valid_regimes = pnd.DataFrame(
            {'source': ['K2163110']*12,
             'id_regime': range(12),
             'lbl_regime': ['nivo_glaciaire', 'nival', 'nival_transition',
                            'nivo_pluvial', 'pluvial_1', 'pluvial_2',
                            'pluvial_3', 'pluvial_4', 'pluvial_5', 'pluvial_6',
                            'pluvial_7', 'pluvial_8'],
             'distance': [6.364468, 5.542722, 5.212776, 3.144169, 1.033849,
                          1.226321, 0.860899, 1.820628, 2.027160, 1.290835,
                          2.705927, 1.214482]},
            index=range(12)
        )
        norm, regimes = _sauquet.compute_distance(df)
        assert_frame_equal(norm, valid_norm)
        assert_frame_equal(regimes, valid_regimes)
        # =====================================================================
