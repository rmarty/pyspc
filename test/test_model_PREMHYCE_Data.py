#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Data in pyspc.model.premhyce

To run all tests just type:
    python -m unittest test_model_PREMHYCE_Data

To run only a class test:
    python -m unittest test_model_PREMHYCE_Data.TestPREMHYCE_Data

To run only a specific test:
    python -m unittest test_model_PREMHYCE_Data.TestPREMHYCE_Data.test_init

"""
# Imports
from datetime import datetime as dt
import filecmp
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
import pyspc.model.premhyce as _model


class TestPREMHYCE_Data(unittest.TestCase):
    """
    PREMHYCE_Data class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'premhyce')
        self.df = pnd.DataFrame(
            {'TYP': ['DEB']*12,
             'CODE': ['K0253030', 'K0260020'] * 6,
             'DATE': sorted([dt(2020, 6, 9), dt(2020, 6, 10),
                             dt(2020, 6, 11), dt(2020, 6, 12),
                             dt(2020, 6, 13), dt(2020, 6, 14)] * 2),
             'DEBIT(l/s)': [1070, 4340, 984, 5220, 919, 5220, 53500, 207000,
                            37800, 219000, 13300, 86800]}
        )

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'Debits.txt')
        reader = _model.Data(filename=filename)
        self.assertEqual(reader.filename, filename)

    def test_read(self):
        """
        Test de lecture d'un fichier de données QMJ pour PREMHYCE
        """
        filename = os.path.join(self.dirname, 'Debits.txt')
        reader = _model.Data(filename=filename)
        df = reader.read()
        assert_frame_equal(df, self.df)

    def test_write(self):
        """
        Test d'écriture d'un fichier de données QMJ pour PREMHYCE
        """
        filename = os.path.join(self.dirname, 'Debits.txt')
        tmpfile = os.path.join('data', 'Debits.txt')
        writer = _model.Data(filename=tmpfile)
        writer.write(self.df)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
