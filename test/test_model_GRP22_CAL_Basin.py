#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP_Basin in pyspc.model.grp22

To run all tests just type:
    python -m unittest test_model_GRP22_CAL_Basin

To run only a class test:
    python -m unittest test_model_GRP22_CAL_Basin.TestGRP_Basin

To run only a specific test:
    python -m unittest test_model_GRP22_CAL_Basin.TestGRP_Basin.test_init

"""
# Imports
import filecmp
import os
import unittest

from pyspc.model.grp22 import GRP_Basin


class TestGRP_Basin(unittest.TestCase):
    """
    GRP_Basin class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp22', 'cal')
        self.valid_values = {
            ('RH10585x', 'etp_pond'): 1.00,
            ('90035001', 'ta_pond'): 0.33,
            ('90035001', 'ta_alti'): 401.00,
            ('90052002', 'rr_pond'): 0.80,
            ('90052002', 'rr_pdt'): '00J01H00M',
            ('90052002', 'ta_pond'): 0.33,
            ('90052002', 'ta_alti'): 473.00,
            ('90065003', 'rr_pond'): 0.20,
            ('90065003', 'rr_pdt'): '00J01H00M',
            ('90065003', 'ta_pond'): 0.33,
            ('90065003', 'ta_alti'): 1153.00,
            ('MODELISATION', 'facteur_neige'): 1.00,
            ('MODELISATION', 'duree_lacune'): '01J00H00M',
        }

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'RH10585x_00J01H00M.ini')
        basin = GRP_Basin(filename=filename)
        self.assertEqual(basin.filename, filename)
        self.assertEqual(basin.location, 'RH10585x')
        self.assertEqual(basin.timestep, '00J01H00M')

    def test_read(self):
        """
        Test de la lecture du fichier
        """
        filename = os.path.join(self.dirname, 'RH10585x_00J01H00M.ini')
        basin = GRP_Basin(filename=filename)
        basin.read()
        for k, v in self.valid_values.items():
            self.assertEqual(basin[k[0]][k[1]], v)

    def test_write(self):
        """
        Test de l'écriture du fichier
        """
        filename = os.path.join(self.dirname, 'RH10585x_00J01H00M.ini')
        tmpfile = os.path.join('data', 'RH10585x_00J01H00M.ini')
        basin = GRP_Basin(filename=tmpfile)
        basin.update_config(self.valid_values, strict=False)
        basin.write()
        self.assertTrue(filecmp.cmp(filename, tmpfile))
        os.remove(tmpfile)
