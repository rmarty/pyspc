#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Fcst in pyspc.model.grp20

To run all tests just type:
    python -m unittest test_model_GRP20_RT_Fcst

To run only a class test:
    python -m unittest test_model_GRP20_RT_Fcst.TestGRPRT_Fcst

To run only a specific test:
    python -m unittest test_model_GRP20_RT_Fcst.TestGRPRT_Fcst.test_init

"""
# Imports
from datetime import datetime as dt
import numpy as np
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.model.grp20 import GRPRT_Fcst


class TestGRPRT_Fcst(unittest.TestCase):
    """
    GRPRT_Fcst class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp20', 'rt')
        self.valid_obs = pnd.DataFrame({
            'TYP': ['OBS']*18,
            '     CODE': [' RH10585x']*18,
            '       PDT': [' 01J00H00M', ' 01J00H00M', ' 01J00H00M',
                           ' 01J00H00M', ' 01J00H00M', ' 00J01H00M',
                           ' 00J01H00M', ' 00J01H00M', ' 00J01H00M',
                           ' 00J01H00M', ' 00J01H00M', ' 00J01H00M',
                           ' 00J01H00M', ' 00J01H00M', ' 00J01H00M',
                           ' 00J01H00M', ' 00J01H00M', ' 00J01H00M'],
            '    DATE(TU)': [
                dt(2007, 1, 15, 0, 0, 0), dt(2007, 1, 16, 0, 0, 0),
                dt(2007, 1, 17, 0, 0, 0), dt(2007, 1, 18, 0, 0, 0),
                dt(2007, 1, 19, 0, 0, 0), dt(2007, 1, 18, 12, 0, 0),
                dt(2007, 1, 18, 13, 0, 0), dt(2007, 1, 18, 14, 0, 0),
                dt(2007, 1, 18, 15, 0, 0), dt(2007, 1, 18, 16, 0, 0),
                dt(2007, 1, 18, 17, 0, 0), dt(2007, 1, 18, 18, 0, 0),
                dt(2007, 1, 18, 19, 0, 0), dt(2007, 1, 18, 20, 0, 0),
                dt(2007, 1, 18, 21, 0, 0), dt(2007, 1, 18, 22, 0, 0),
                dt(2007, 1, 18, 23, 0, 0), dt(2007, 1, 19, 0, 0, 0)],
            '    DEBIT(m3/s)': [
                3.5675, 2.9225, np.nan, 2.1828, 4.8858, 2.8227, 2.7736, 2.7246,
                2.7099, 2.7297, 2.8959, 3.5374, 5.1503, 7.8637, 10.1921,
                12.4637, 14.6949, 16.6246],
            '      PLUIE(mm)': [
                6.88, 0.00, 0.40, 7.84, 70.08, 0.16, 0.12, 0.20, 3.04, 3.36,
                4.16, 8.48, 3.72, 7.60, 6.32, 9.44, 6.64, 7.88],
            'Temperature(°C)': [
                np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan,
                np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan,
                np.nan, np.nan],

        })
        self.valid_sim = pnd.DataFrame({
            'TYP': ['SIM']*23,
            '     CODE': [' RH10585x']*23,
            '       PDT': [
                ' 01J00H00M', ' 01J00H00M', ' 01J00H00M', ' 01J00H00M',
                ' 01J00H00M', ' 00J01H00M', ' 00J01H00M', ' 00J01H00M',
                ' 00J01H00M', ' 00J01H00M', ' 00J01H00M', ' 00J01H00M',
                ' 00J01H00M', ' 00J01H00M', ' 00J01H00M', ' 00J01H00M',
                ' 00J01H00M', ' 00J01H00M', ' 00J01H00M', ' 00J01H00M',
                ' 00J01H00M', ' 00J01H00M', ' 00J01H00M'],
            '    DATE(TU)': [
                dt(2007, 1, 20, 0, 0, 0), dt(2007, 1, 21, 0, 0, 0),
                dt(2007, 1, 22, 0, 0, 0), dt(2007, 1, 23, 0, 0, 0),
                dt(2007, 1, 24, 0, 0, 0), dt(2007, 1, 19, 1, 0, 0),
                dt(2007, 1, 19, 2, 0, 0), dt(2007, 1, 19, 3, 0, 0),
                dt(2007, 1, 19, 4, 0, 0), dt(2007, 1, 19, 5, 0, 0),
                dt(2007, 1, 19, 6, 0, 0), dt(2007, 1, 19, 7, 0, 0),
                dt(2007, 1, 19, 8, 0, 0), dt(2007, 1, 19, 9, 0, 0),
                dt(2007, 1, 19, 10, 0, 0), dt(2007, 1, 19, 11, 0, 0),
                dt(2007, 1, 19, 12, 0, 0), dt(2007, 1, 19, 13, 0, 0),
                dt(2007, 1, 19, 14, 0, 0), dt(2007, 1, 19, 15, 0, 0),
                dt(2007, 1, 19, 16, 0, 0), dt(2007, 1, 19, 17, 0, 0),
                dt(2007, 1, 19, 18, 0, 0)],
            '    DEBIT(m3/s)': [
                22.0030, 17.6740, 8.9080, 5.4410, 3.4880, 24.4500, 27.4260,
                29.7600, 31.7800, 33.8920, 36.1960, 38.7650, 41.1230, 43.0090,
                44.2150, 44.1250, 42.3980, 39.7300, 36.8450, 34.3670, 32.3170,
                30.4140, 28.5610],
            '      PLUIE(mm)': [
                82.96, 10.48, 9.00, 2.04, 1.12, 8.44, 5.32, 6.64, 5.96, 6.72,
                7.24, 7.44, 6.60, 6.48, 5.36, 2.96, 1.32, 1.08, 0.48, 1.60,
                1.00, 0.72, 0.80],
            'Temperature(°C)': [
                np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan,
                np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan,
                np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
        })
        self.valid_fcst = pnd.DataFrame({
            'TYP': ['PRV']*23,
            '     CODE': [' RH10585x']*23,
            '       PDT': [
                ' 01J00H00M', ' 01J00H00M', ' 01J00H00M', ' 01J00H00M',
                ' 01J00H00M', ' 00J01H00M', ' 00J01H00M', ' 00J01H00M',
                ' 00J01H00M', ' 00J01H00M', ' 00J01H00M', ' 00J01H00M',
                ' 00J01H00M', ' 00J01H00M', ' 00J01H00M', ' 00J01H00M',
                ' 00J01H00M', ' 00J01H00M', ' 00J01H00M', ' 00J01H00M',
                ' 00J01H00M', ' 00J01H00M', ' 00J01H00M'],
            '    DATE(TU)': [
                dt(2007, 1, 20, 0, 0, 0), dt(2007, 1, 21, 0, 0, 0),
                dt(2007, 1, 22, 0, 0, 0), dt(2007, 1, 23, 0, 0, 0),
                dt(2007, 1, 24, 0, 0, 0), dt(2007, 1, 19, 1, 0, 0),
                dt(2007, 1, 19, 2, 0, 0), dt(2007, 1, 19, 3, 0, 0),
                dt(2007, 1, 19, 4, 0, 0), dt(2007, 1, 19, 5, 0, 0),
                dt(2007, 1, 19, 6, 0, 0), dt(2007, 1, 19, 7, 0, 0),
                dt(2007, 1, 19, 8, 0, 0), dt(2007, 1, 19, 9, 0, 0),
                dt(2007, 1, 19, 10, 0, 0), dt(2007, 1, 19, 11, 0, 0),
                dt(2007, 1, 19, 12, 0, 0), dt(2007, 1, 19, 13, 0, 0),
                dt(2007, 1, 19, 14, 0, 0), dt(2007, 1, 19, 15, 0, 0),
                dt(2007, 1, 19, 16, 0, 0), dt(2007, 1, 19, 17, 0, 0),
                dt(2007, 1, 19, 18, 0, 0)],
            '    DEBIT(m3/s)': [
                19.7650, 17.1220, 8.7760, 5.3950, 3.4690, 19.3090, 22.2510,
                24.6650, 26.8170, 29.0650, 31.5000, 34.1880, 36.6890, 38.7490,
                40.1620, 40.3390, 38.9430, 36.6230, 34.0620, 31.8590, 30.0370,
                28.3350, 26.6640],
            '      PLUIE(mm)': [
                82.96, 10.48, 9.00, 2.04, 1.12, 8.44, 5.32, 6.64, 5.96, 6.72,
                7.24, 7.44, 6.60, 6.48, 5.36, 2.96, 1.32, 1.08, 0.48, 1.60,
                1.00, 0.72, 0.80],
            'Temperature(°C)': [
                np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan,
                np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan,
                np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
        })

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = 'GRP_Obs.txt'
        datatype = "obs"
        reader = GRPRT_Fcst(filename=filename, datatype=datatype)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.datatype, datatype)

    def test_init_error(self):
        """
        Test de la création de l'instance - Cas avec erreurs
        """
        filename = 'GRP_Obs.txt'
        with self.assertRaises(ValueError):
            GRPRT_Fcst(filename=filename)
        with self.assertRaises(ValueError):
            GRPRT_Fcst(filename=filename, datatype='toto')

    def test_read_obs_diff(self):
        """
        Test de lecture d'un fichier de prévision GRP RT - OBS DIFF
        """
        filename = os.path.join(self.dirname, 'GRP_D_Obs.txt')
        datatype = "obs_diff"
        reader = GRPRT_Fcst(filename=filename, datatype=datatype)
        df = reader.read()
        assert_frame_equal(df, self.valid_obs)

    def test_read_sim_diff(self):
        """
        Test de lecture d'un fichier de prévision GRP RT - SIM DIFF
        """
        filename = os.path.join(self.dirname, 'GRP_D_Simu_2001.txt')
        datatype = "sim_diff"
        reader = GRPRT_Fcst(filename=filename, datatype=datatype)
        df = reader.read()
        assert_frame_equal(df, self.valid_sim)

    def test_read_fcst_diff(self):
        """
        Test de lecture d'un fichier de prévision GRP RT - FCST DIFF
        """
        filename = os.path.join(self.dirname, 'GRP_D_Prev_2001.txt')
        datatype = "fcst_diff"
        reader = GRPRT_Fcst(filename=filename, datatype=datatype)
        df = reader.read()
        assert_frame_equal(df, self.valid_fcst)

#    def test_write_obs(self):
#        """
#        Test d'écriture d'un fichier de prévision GRP Temps-Réel - OBS
#        """
#
#    def test_write_sim(self):
#        """
#        Test d'écriture d'un fichier de prévision GRP Temps-Réel - SIM
#        """
#
#    def test_write_fcst(self):
#        """
#        Test d'écriture d'un fichier de prévision GRP Temps-Réel - FCST
#        """

    def test_fileprefix(self):
        """
        Test des préfixes des fichiers GRPRT_Fcst
        """
        valids = {
            "obs": 'GRP_Obs', "obs_diff": 'GRP_D_Obs',
            "sim": 'GRP_Simu_', "sim_diff": 'GRP_D_Simu_',
            "fcst": 'GRP_Prev_', "fcst_diff": 'GRP_D_Prev_'
        }
        for k, v in valids.items():
            self.assertEqual(GRPRT_Fcst.get_fileprefix(datatype=k), v)

    def test_lineprefix(self):
        """
        Test des préfixes des lignes données GRPRT_Fcst
        """
        valids = {
            "obs": 'OBS', "obs_diff": 'OBS',
            "sim": 'SIM', "sim_diff": 'SIM',
            "fcst": 'PRV', "fcst_diff": 'PRV'
        }
        for k, v in valids.items():
            self.assertEqual(GRPRT_Fcst.get_lineprefix(datatype=k), v)

    def test_datatype(self):
        """
        Test des types de données GRPRT_Fcst
        """
        self.assertEqual(
            ['fcst', 'fcst_diff', 'obs', 'obs_diff', 'sim', 'sim_diff'],
            GRPRT_Fcst.get_types()
        )
