#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_Sacha in pyspc.io.sacha

To run all tests just type:
    python -m unittest test_data_Sacha

To run only a class test:
    python -m unittest test_data_Sacha.TestSacha

To run only a specific test:
    python -m unittest test_data_Sacha.TestSacha.test_init

"""
# Imports
from datetime import datetime as dt
import os
import unittest

from pyspc.core.series import Series
from pyspc.io.sacha import read_Sacha


class TestSacha(unittest.TestCase):
    """
    Sacha_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'io', 'dbase')
        self.source = os.path.join(self.dirname, 'sacha_montpezat.mdb')

    def test_read_P_hydro2(self):
        """
        Test de la lecture de données de précipitations sol
        """
        # =====================================================================
        stations = ['K0009910', 'K0029910', 'K0100020', '07154005']
        varname = 'PH'
        first_dt = dt(2008, 11, 1, 23)
        last_dt = dt(2008, 11, 2, 2)
        # =====================================================================
        series = read_Sacha(
            filename=self.source,
            codes=stations,
            varname=varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=False,
            prcp_src='gauge',
            hydro_version='hydro2'
        )
        self.assertIsInstance(series, Series)
        for s in stations:
            self.assertIn((s, varname, None), series)
        # =====================================================================

    def test_read_Q_realtime(self):
        """
        Test de la lecture de données de débit - TEMPS REEL
        """
        # =====================================================================
        stations = ['K0030020', 'K0100020']
        varname = 'QH'
        first_dt = dt(2016, 11, 22, 15)
        last_dt = dt(2016, 11, 22, 18)
        # =====================================================================
        series = read_Sacha(
            filename=self.source,
            codes=stations,
            varname=varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=True,
            hydro_version='hydro2'
        )
        self.assertIsInstance(series, Series)
        for s in stations:
            self.assertIn((s, varname, None), series)
        # =====================================================================

    def test_read_H_hydro3(self):
        """
        Test de la lecture de données de débit
        """
        # =====================================================================
        stations = ['K001002010', 'K001872010']
        varname = 'HH'
        first_dt = dt(2008, 11, 1, 23)
        last_dt = dt(2008, 11, 2, 2)
        # =====================================================================
        series = read_Sacha(
            filename=self.source,
            codes=stations,
            varname=varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=False,
            hydro_version='hydro3'
        )
        self.assertIsInstance(series, Series)
        for s in stations:
            self.assertIn((s, varname, None), series)
        # =====================================================================

    def test_read_T(self):
        """
        Test de la lecture de données de température
        """
        # =====================================================================
        stations = ['07154005']
        varname = 'TH'
        first_dt = dt(2008, 11, 1, 23)
        last_dt = dt(2008, 11, 2, 2)
        # =====================================================================
        series = read_Sacha(
            filename=self.source,
            codes=stations,
            varname=varname,
            first_dt=first_dt,
            last_dt=last_dt,
            realtime=False,
            hydro_version='hydro2'
        )
        self.assertIsInstance(series, Series)
        for s in stations:
            self.assertIn((s, varname, None), series)
        # =====================================================================
