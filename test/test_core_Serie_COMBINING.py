#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Serie in pyspc.core.serie - combining

To run all tests just type:
    python -m unittest test_core_Serie_COMBINING

To run only a class test:
    python -m unittest test_core_Serie_COMBINING.TestSerie

To run only a specific test:
    python -m unittest test_core_Serie_COMBINING.TestSerie.test_add

"""
# Imports
from datetime import datetime as dt, timedelta as td
import numpy as np
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest
import warnings

# Imports pyspc
from pyspc.core.serie import Serie


class TestSerie(unittest.TestCase):
    """
    Serie class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        self.code = 'K0000000'
        self.provider = 'SPC'
        self.varname = 'QH'

    def test_comp(self):
        """
        Test méthode comp
        """
        # =====================================================================
        varname = 'QH'
        serie = pnd.DataFrame(
            {(self.code, varname): [6, 7, 8, 9, 10, 11, 12, np.nan, np.nan,
                                    np.nan, np.nan, np.nan, np.nan]},
            index=pnd.date_range(
                dt(2021, 1, 1, 6), dt(2021, 1, 1, 18), freq=td(hours=1))
        )
        serie = Serie(
            datval=serie,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        other = pnd.DataFrame(
            {(self.code, varname): [10, 11, 12, 13, 14, 15, np.nan,
                                    np.nan, np.nan, np.nan, 200, 210, 220]},
            index=pnd.date_range(
                dt(2021, 1, 1, 10), dt(2021, 1, 1, 22), freq=td(hours=1))
        )
        other = Serie(
            datval=other,
            code='x',
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid = [
            '--- x\n', '+++ K0000000\n', '@@ -1,13 +1,13 @@\n',
            '+2021010106\t6.000', '+2021010107\t7.000', '+2021010108\t8.000',
            '+2021010109\t9.000', ' 2021010110\t10.000',
            ' 2021010111\t11.000', ' 2021010112\t12.000',
            '-2021010113\t13.000', '-2021010114\t14.000',
            '-2021010115\t15.000', '+2021010113\tnan', '+2021010114\tnan',
            '+2021010115\tnan', ' 2021010116\tnan', ' 2021010117\tnan',
            ' 2021010118\tnan', '-2021010119\tnan', '-2021010120\t200.000',
            '-2021010121\t210.000', '-2021010122\t220.000'
        ]
        comp = serie.comp(other, media=None)
        self.assertEqual(comp, valid)
        # =====================================================================

    def test_cpy(self):
        """
        Test méthode copy_year
        """
        # =====================================================================
        varname = 'QH'
        serie = pnd.DataFrame(
            {(self.code, varname): [
                35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000,
                115.000, 103.000, 92.500, 86.400, 82.300, 78.300]},
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1))
        )
        serie = Serie(
            datval=serie,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        other = pnd.DataFrame(
            {(self.code, varname): [
                115.0000, 110.0000, 103.0000, 101.0000, 103.0000, 118.0000,
                133.0000, 155.0000, 178.0000, 193.0000, 197.0000, 188.0000,
                164.0000, 139.0000, 121.0000, 107.0000, 97.4000, 89.4000,
                81.4000]},
            index=pnd.date_range(
                dt(2003, 11, 5, 0), dt(2003, 11, 5, 18), freq=td(hours=1))
        )
        other = Serie(
            datval=other,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid = pnd.DataFrame(
            {(self.code, varname): [
                35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000,
                115.000, 103.000, 92.500, 86.400, 82.300, 78.300,
                np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan,
                np.nan, np.nan, np.nan,
                115.0000, 110.0000, 103.0000, 101.0000, 103.0000, 118.0000,
                133.0000, 155.0000, 178.0000, 193.0000, 197.0000, 188.0000,
                164.0000, 139.0000, 121.0000, 107.0000, 97.4000, 89.4000,
                81.4000, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan]},
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 6, 0), freq=td(hours=1))
        )
        valid.columns = [(self.code, varname)]
        serie.copy_year(
            src=other,
            year=2003,
            start=dt(2014, 11, 5),
            end=dt(2014, 11, 6)
        )
        assert_frame_equal(serie.data_frame, valid)
        # =====================================================================

    def test_update(self):
        """
        Test méthode update
        """
        # =====================================================================
        varname = 'QH'
        serie = pnd.DataFrame(
            {(self.code, varname): [6, 7, 8, 9, 10, 11, 12, np.nan, np.nan,
                                    np.nan, np.nan, np.nan, np.nan]},
            index=pnd.date_range(
                dt(2021, 1, 1, 6), dt(2021, 1, 1, 18), freq=td(hours=1))
        )
        serie = Serie(
            datval=serie,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        other = pnd.DataFrame(
            {(self.code, varname): [100, 110, 120, 130, 140, 150, np.nan,
                                    np.nan, np.nan, np.nan, 200, 210, 220]},
            index=pnd.date_range(
                dt(2021, 1, 1, 10), dt(2021, 1, 1, 22), freq=td(hours=1))
        )
        other = Serie(
            datval=other,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid = pnd.DataFrame(
            {(self.code, varname): [6, 7, 8, 9, 10, 11, 12, 130, 140, 150,
                                    np.nan, np.nan, np.nan, np.nan, 200, 210,
                                    220]},
            index=pnd.date_range(
                dt(2021, 1, 1, 6), dt(2021, 1, 1, 22), freq=td(hours=1))
        )
        valid.columns = [(self.code, varname)]
        serie.update(other=other, overwrite=False)
        assert_frame_equal(serie.data_frame, valid)
        # =====================================================================

    def test_update_overwrite(self):
        """
        Test méthode update
        """
        # =====================================================================
        varname = 'QH'
        serie = pnd.DataFrame(
            {(self.code, varname): [6, 7, 8, 9, 10, 11, 12, np.nan, np.nan,
                                    np.nan, np.nan, np.nan, np.nan]},
            index=pnd.date_range(
                dt(2021, 1, 1, 6), dt(2021, 1, 1, 18), freq=td(hours=1))
        )
        serie = Serie(
            datval=serie,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        other = pnd.DataFrame(
            {(self.code, varname): [100, 110, 120, 130, 140, 150, np.nan,
                                    np.nan, np.nan, np.nan, 200, 210, 220]},
            index=pnd.date_range(
                dt(2021, 1, 1, 10), dt(2021, 1, 1, 22), freq=td(hours=1))
        )
        other = Serie(
            datval=other,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid = pnd.DataFrame(
            {(self.code, varname): [6, 7, 8, 9, 100, 110, 120, 130, 140, 150,
                                    np.nan, np.nan, np.nan, np.nan, 200, 210,
                                    220]},
            index=pnd.date_range(
                dt(2021, 1, 1, 6), dt(2021, 1, 1, 22), freq=td(hours=1))
        )
        valid.columns = [(self.code, varname)]
        serie.update(other=other, overwrite=True)
        assert_frame_equal(serie.data_frame, valid)
        # =====================================================================

    def test_update_errors(self):
        """
        Test méthode update - erreurs
        """
        # =====================================================================
        varname = 'QH'
        source = pnd.DataFrame(
            {(self.code, varname): [6, 7, 8, 9, 10, 11, 12, np.nan, np.nan,
                                    np.nan, np.nan, np.nan, np.nan]},
            index=pnd.date_range(
                dt(2021, 1, 1, 6), dt(2021, 1, 1, 18), freq=td(hours=1))
        )
        # =====================================================================
        other = pnd.DataFrame(
            {(self.code, varname): [100, 110, 120, 130, 140, 150, np.nan,
                                    np.nan, np.nan, np.nan, 200, 210, 220]},
            index=pnd.date_range(
                dt(2021, 1, 1, 10), dt(2021, 1, 1, 22), freq=td(hours=1))
        )
        other = Serie(
            datval=other,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        # =====================================================================
        valid = pnd.DataFrame(
            {(self.code, varname): [6, 7, 8, 9, 100, 110, 120, 130, 140, 150,
                                    np.nan, np.nan, np.nan, np.nan, 200, 210,
                                    220]},
            index=pnd.date_range(
                dt(2021, 1, 1, 6), dt(2021, 1, 1, 22), freq=td(hours=1))
        )
        # =====================================================================
        serie = Serie(
            datval=source,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        with self.assertRaises(ValueError):
            serie.update()
        # =====================================================================
        other = Serie(
            datval=valid,
            code='x',
            provider=self.provider,
            varname=varname,
        )
        try:
            serie.update(other=other, strict=False)
        except ValueError:
            self.fail("Erreur dans le contrôle de la session")
        with self.assertRaises(ValueError):
            serie.update(other=other, strict=True)
        # =====================================================================
        other = Serie(
            datval=valid,
            code=self.code,
            provider=self.provider,
            varname=varname,
        )
        other._spc_varname = 'PH'
        with self.assertRaises(ValueError):
            serie.update(other=other, strict=False)
        with self.assertRaises(ValueError):
            serie.update(other=other, strict=True)
        # =====================================================================
