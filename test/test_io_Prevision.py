#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_Prevision* and write_Prevision* in pyspc.io.prevision

To run all tests just type:
    python -m unittest test_data_Prevision

To run only a class test:
    python -m unittest test_data_Prevision.TestPrevision

To run only a specific test:
    python -m unittest test_data_Prevision.TestPrevision.test_init

"""
# Imports
from datetime import datetime as dt
import os
import unittest

from pyspc.core.series import Series
from pyspc.io.prevision import (
    read_Prevision14, read_Prevision17, read_Prevision19)


class TestPrevision(unittest.TestCase):
    """
    Prevision_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'io', 'dbase')

    def test_Prevision14_hydro2(self):
        """
        Test Prevision14_hydro2
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'prevision_2014.mdb')
        first_dt = dt(2013, 5, 3, 12)
        last_dt = dt(2013, 5, 3, 23)
        codes = ['K1251810']
        # =====================================================================
        series = read_Prevision14(
            filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
            valid=False, hydro_version='hydro2')
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 27)
        cases = [(v, str(s))
                 for v in ['PH', 'QH', 'HH'] for s in range(2001, 2010)]
        for c in cases:
            self.assertIn(
                (codes[0], c[0], (last_dt, 'LCI', c[1], None)), series)
        # =====================================================================
        series = read_Prevision14(
            filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
            valid=True, hydro_version='hydro2')
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(
            (codes[0], 'HH', (last_dt, 'LCI-valid', '2006', None)), series)
        self.assertIn(
            (codes[0], 'QH', (last_dt, 'LCI-valid', '2006', None)), series)
        # =====================================================================

    def test_Prevision14_hydro3(self):
        """
        Test Prevision14_hydro3
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'prevision_2014_hydro3.mdb')
        first_dt = dt(2016, 5, 31, 12)
        last_dt = dt(2016, 5, 31, 18)
        codes = ['K6373020']
        # =====================================================================
        series = read_Prevision14(
            filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
            valid=False, hydro_version='hydro3')
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 8)
        cases = [(r, i)
                 for r in [dt(2016, 5, 31, 13), dt(2016, 5, 31, 16)]
                 for i in ['Val', 'Val10', 'Val50', 'Val90']]
        for c in cases:
            self.assertIn(
                (codes[0], 'QH', (c[0], 'LCI', '2001', c[1])), series)
        # =====================================================================
        series = read_Prevision14(
            filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
            valid=True, hydro_version='hydro3')
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 8)
        cases = [(r, i)
                 for r in [dt(2016, 5, 31, 13), dt(2016, 5, 31, 16)]
                 for i in ['Val', 'Val10', 'Val50', 'Val90']]
        for c in cases:
            self.assertIn(
                (codes[0], 'QH', (c[0], 'LCI-valid', '2001', c[1])), series)
        # =====================================================================

    def test_Prevision17_hydro3(self):
        """
        Test Prevision17_hydro3
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'prevision_2017.mdb')
        first_dt = dt(2016, 11, 21, 12)
        last_dt = dt(2016, 11, 21, 12)
        codes = ['K0403010']
        # =====================================================================
        series = read_Prevision17(
            filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
            valid=False, released=False)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 64)
        cases = [(v, s, i)
                 for v in ['QH', 'HH']
                 for s in ['2001', '2002', '2003', '2004',
                           '2006', '2007', '2008', '2009']
                 for i in ['', '10', '50', '90']]
        for c in cases:
            self.assertIn(
                (codes[0], c[0], (last_dt, 'LCI', c[1], c[2])), series)
        # =====================================================================
        series = read_Prevision17(
            filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
            valid=True, released=False)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 8)
        cases = [(v, s, i)
                 for v in ['QH', 'HH']
                 for s in ['2003']
                 for i in ['', '10', '50', '90']]
        for c in cases:
            self.assertIn(
                (codes[0], c[0], (last_dt, 'pilote', c[1], c[2])), series)
        # =====================================================================
        series = read_Prevision17(
            filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
            valid=True, released=True)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 8)
        cases = [(v, s, i)
                 for v in ['QH', 'HH']
                 for s in ['2003']
                 for i in ['', '10', '50', '90']]
        for c in cases:
            self.assertIn(
                (codes[0], c[0], (last_dt, 'pilote', c[1], c[2])), series)
        # =====================================================================

    def test_Prevision19_hydro3(self):
        """
        Test Prevision19_hydro3
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'PRV_201801.mdb')
        codes = ['K1321810']
        first_dt = dt(2018, 1, 4)
        last_dt = dt(2018, 1, 5)
        # =====================================================================
        series = read_Prevision19(
            filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
            valid=False, released=False)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 72)
        cases = [(v, s, i)
                 for v in ['QH', 'HH']
                 for s in ['2007', '1007', '2011',
                           '5210', '5010', '5110', '5200', '5000', '5100']
                 for i in ['', '10', '50', '90']]
        for c in cases:
            self.assertIn(
                (codes[0], c[0],
                 (dt(2018, 1, 4, 12), 'LCI', c[1], c[2])), series)
        # =====================================================================
        series = read_Prevision19(
            filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
            valid=True, released=False)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 8)
        cases = [(v, s, i)
                 for v in ['QH', 'HH']
                 for s in ['5200']
                 for i in ['', '10', '50', '90']]
        for c in cases:
            self.assertIn(
                (codes[0], c[0],
                 (dt(2018, 1, 4, 12), 'expresso', c[1], c[2])), series)
        # =====================================================================
        series = read_Prevision19(
            filename=filename, codes=codes, first_dt=first_dt, last_dt=last_dt,
            valid=True, released=True)
        self.assertIsNone(series)
        # =====================================================================
