#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Export in pyspc.webservice.hydro

To run all tests just type:
    python -m unittest test_webservice_Hydro2_Export

To run only a class test:
    python -m unittest test_webservice_Hydro2_Export.TestHydro2_Export

To run only a specific test:
    python -m unittest test_webservice_Hydro2_Export.TestHydro2_Export.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import filecmp
import os
import unittest
from unittest import mock
import warnings

# Imports pyspc
from pyspc.webservice.hydro2 import Export


class TestHydro2_Export(unittest.TestCase):
    """
    Hydro2_Export class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'webservice', 'hydro2')
        self.dtypes = ['H-TEMPS', 'QJM', 'QTFIX', 'QTVAR',
                       'DEBCLA', 'CRUCAL', 'TOUSMOIS', 'SYNTHESE']
        warnings.filterwarnings(action="ignore", category=ResourceWarning)

    def test_init(self):
        """
        Test de la création de l'instance
        """
        with self.assertRaises(ValueError):
            proc = Export()
        with self.assertRaises(ValueError):
            proc = Export(datatype='toto')
        for dtype in self.dtypes:
            proc = Export(datatype=dtype)
            self.assertEqual(proc.datatype, dtype)
            self.assertIsNone(proc.stations)
            self.assertIsNone(proc.first_dt)
            self.assertIsNone(proc.last_dt)
            self.assertIsNone(proc.exports)

    def test_datatype(self):
        """
        Test des types de procédure d'export
        """
        valids = self.dtypes
        unvalids = ['CORJOU', 'CORMEN', 'DERIVE', 'ENTRE2', 'INTERA',
                    'JAUGEAGE', 'PERIODE', 'Q-TEMPS', 'QMNA', 'STATION',
                    'TARAGE', 'TOUS-QIX', 'VCN-QCN', 'VCX-CX']
        datatypes = Export.get_datatypes()
        for v in valids:
            self.assertIn(v, datatypes)
        for u in unvalids:
            self.assertNotIn(u, datatypes)

    def test_dtfmt(self):
        """
        Test de la méthode privée définissant le format des dates
        dans les procédures d'export
        """
        dtypes = self.dtypes
        controls = ["%d/%m/%Y", "%d/%m/%Y", "%d/%m/%Y %H:%M", "%d/%m/%Y %H:%M",
                    ["%d/%m", "%Y"], ["%d/%m", "%Y"], ["%d/%m", "%Y"], None]
        for dtype, ctl in zip(dtypes, controls):
            proc = Export(
                datatype=dtype
            )
            f = proc.get_dtfmt()
            self.assertEqual(f, ctl)
            self.assertEqual(proc.dtfmt, ctl)

    def test_maxperiod(self):
        """
        Test de la méthode privée définissant la longueur de données
        relative à une procédure d'export
        """
        controls = [td(days=2000), td(days=1000), td(days=330), td(days=330),
                    None, None, None, None]
        for d, ctl in zip(self.dtypes, controls):
            proc = Export(datatype=d)
            mp = proc.get_maxperiod()
            self.assertEqual(mp, ctl)

    def test_precision(self):
        """
        Test de la précision
        """
        valids = [None, 0, 1, 2, 3]
        for v in valids:
            try:
                Export(datatype='TOUSMOIS', precision=v)
            except ValueError:
                self.fail('Précision incorrecte')
        unvalids = [-1, 5, 10, 'a', '']
        for v in unvalids:
            with self.assertRaises(ValueError):
                Export(datatype='TOUSMOIS', precision=v)

    def test_shortfilename(self):
        """
        Test de la méthode privée définissant le nom court des fichiers
        contenant les données exportées
        """
        station = 'K4350010'
        dtypes = self.dtypes
        valid = ['K435h000.txt', 'K435j001.txt',
                 'K435f002.txt', 'K435v003.txt',
                 'K4350DC4.txt', 'K4350QC5.txt',
                 'K435m006.txt', 'K4350SY7.txt']
        for k, dtype in enumerate(dtypes):
            proc = Export(
                datatype=dtype
            )
            sf = proc.set_shortfilename(station=station, index=k)
            self.assertEqual(sf, valid[k])

    def test_index(self):
        """
        Test de la méthode privée définissant le premier index valide
        utilisé par la suite pour le nom court des fichiers
        """
        proc = Export(datatype='H-TEMPS')
        stations = ['K0403010', 'K0403011', 'K0409999']
        indexes = {}
        for k, s in enumerate(stations):
            res, indexes = proc.set_index_export(s, indexes)
            self.assertEqual(res, k)

    @mock.patch('pyspc.webservice.hydro2.Export.set_export_synthese')
    @mock.patch('pyspc.webservice.hydro2.Export.set_export_tousmois')
    @mock.patch('pyspc.webservice.hydro2.Export.set_export_crucal')
    @mock.patch('pyspc.webservice.hydro2.Export.set_export_debcla')
    @mock.patch('pyspc.webservice.hydro2.Export.set_export_series')
    def test_export(self, mock_df, mock_dc, mock_qc, mock_tm, mock_sy):
        """
        Test du fichier des procédures d'export
        """
        # =====================================================================
        for d in ['H-TEMPS', 'QTVAR', 'QTFIX', 'QJM']:
            proc = Export(datatype=d)
            proc.set_export()
            mock_df.assert_called()
            mock_dc.assert_not_called()
            mock_qc.assert_not_called()
            mock_tm.assert_not_called()
            mock_sy.assert_not_called()

            mock_df.reset_mock()
            mock_dc.reset_mock()
            mock_qc.reset_mock()
            mock_tm.reset_mock()
            mock_sy.reset_mock()
        # =====================================================================
        proc = Export(datatype='DEBCLA')
        proc.set_export()
        mock_df.assert_not_called()
        mock_dc.assert_called()
        mock_qc.assert_not_called()
        mock_tm.assert_not_called()
        mock_sy.assert_not_called()
        mock_df.reset_mock()
        mock_dc.reset_mock()
        mock_qc.reset_mock()
        mock_tm.reset_mock()
        mock_sy.reset_mock()
        # =====================================================================
        proc = Export(datatype='CRUCAL')
        proc.set_export()
        mock_df.assert_not_called()
        mock_dc.assert_not_called()
        mock_qc.assert_called()
        mock_tm.assert_not_called()
        mock_sy.assert_not_called()
        mock_df.reset_mock()
        mock_dc.reset_mock()
        mock_qc.reset_mock()
        mock_tm.reset_mock()
        mock_sy.reset_mock()
        # =====================================================================
        proc = Export(datatype='TOUSMOIS')
        proc.set_export()
        mock_df.assert_not_called()
        mock_dc.assert_not_called()
        mock_qc.assert_not_called()
        mock_tm.assert_called()
        mock_sy.assert_not_called()
        mock_df.reset_mock()
        mock_dc.reset_mock()
        mock_qc.reset_mock()
        mock_tm.reset_mock()
        mock_sy.reset_mock()
        # =====================================================================
        proc = Export(datatype='SYNTHESE')
        proc.set_export()
        mock_df.assert_not_called()
        mock_dc.assert_not_called()
        mock_qc.assert_not_called()
        mock_tm.assert_not_called()
        mock_sy.assert_called()
        mock_df.reset_mock()
        mock_dc.reset_mock()
        mock_qc.reset_mock()
        mock_tm.reset_mock()
        mock_sy.reset_mock()
        # =====================================================================

    def test_export_series(self):
        """
        Test du fichier des procédures d'export de séries de données
        - H-TEMPS
        - QTVAR
        - QTFIX
        - QJM
        """
        # =====================================================================
        start = dt(2020, 3, 5, 6)
        end = dt(2020, 3, 7, 6)
        # =====================================================================
        tmpfile = os.path.join('data', 'test_export_htemps.txt')
        proc = Export(stations=['K1321810'], datatype='H-TEMPS',
                      first_dt=start, last_dt=end)
        proc.set_export_series()
        proc.write(filename=tmpfile)
        filename = os.path.join(self.dirname, 'H-TEMPS_K1321810.txt')
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        tmpfile = os.path.join('data', 'test_export_qtvar.txt')
        proc = Export(stations=['K1321810'], datatype='QTVAR',
                      first_dt=start, last_dt=end)
        proc.set_export_series()
        proc.write(filename=tmpfile)
        filename = os.path.join(self.dirname, 'QTVAR_K1321810.txt')
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        tmpfile = os.path.join('data', 'test_export_qtfix.txt')
        proc = Export(stations=['K1321810'], datatype='QTFIX',
                      first_dt=start, last_dt=end)
        proc.set_export_series()
        proc.write(filename=tmpfile)
        filename = os.path.join(self.dirname, 'QTFIX_K1321810.txt')
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        tmpfile = os.path.join('data', 'test_export_qjm.txt')
        proc = Export(stations=['K1321810'], datatype='QJM',
                      first_dt=start, last_dt=end)
        proc.set_export_series()
        proc.write(filename=tmpfile)
        filename = os.path.join(self.dirname, 'QJM_K1321810.txt')
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_export_debcla(self):
        """
        Test du fichier des procédures d'export DEBCLA
        """
        # =====================================================================
        tmpfile = os.path.join('data', 'test_export_debcla.txt')
        proc = Export(
            stations=['K1321810'], datatype='DEBCLA',
            first_dt=dt(1948, 9, 1), last_dt=dt(2020, 8, 31))
        proc.set_export_debcla()
        proc.write(filename=tmpfile)
        filename = os.path.join(self.dirname, 'DEBCLA_K1321810.txt')
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        tmpfile = os.path.join('data', 'test_export_debcla_1.txt')
        proc = Export(
            stations=['K1251810', 'K1321810', 'K1341810'], datatype='DEBCLA',
            first_dt=dt(1948, 9, 1), last_dt=dt(2020, 8, 31))
        proc.set_export_debcla(onefile='debcla1.txt')
        proc.write(filename=tmpfile)
        filename = os.path.join(self.dirname, 'DEBCLA_Arroux.txt')
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_export_crucal(self):
        """
        Test du fichier des procédures d'export CRUCAL
        """
        # =====================================================================
        tmpfile = os.path.join('data', 'test_export_crucal.txt')
        proc = Export(
            stations=['K1321810'], datatype='CRUCAL',
            first_dt=dt(1948, 9, 1), last_dt=dt(2020, 8, 31))
        proc.set_export_crucal()
        proc.write(filename=tmpfile)
        filename = os.path.join(self.dirname, 'CRUCAL_K1321810.txt')
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        tmpfile = os.path.join('data', 'test_export_crucal_1.txt')
        proc = Export(
            stations=['K1251810', 'K1321810', 'K1341810'], datatype='CRUCAL',
            first_dt=dt(1948, 9, 1), last_dt=dt(2020, 8, 31))
        proc.set_export_crucal(onefile='crucal1.txt')
        proc.write(filename=tmpfile)
        filename = os.path.join(self.dirname, 'CRUCAL_Arroux.txt')
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_export_synthese(self):
        """
        Test du fichier des procédures d'export SYNTHESE
        """
        # =====================================================================
        tmpfile = os.path.join('data', 'test_export_synthese.txt')
        proc = Export(stations=['K1321810'], datatype='SYNTHESE')
        proc.set_export_synthese()
        proc.write(filename=tmpfile)
        filename = os.path.join(self.dirname, 'SYNTHESE_K1321810.txt')
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        tmpfile = os.path.join('data', 'test_export_synthese_1.txt')
        proc = Export(
            stations=['K1251810', 'K1321810', 'K1341810'],
            datatype='SYNTHESE'
        )
        proc.set_export_synthese(onefile='synth1.txt')
        proc.write(filename=tmpfile)
        filename = os.path.join(self.dirname, 'SYNTHESE_Arroux.txt')
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_export_tousmois(self):
        """
        Test du fichier des procédures d'export TOUSMOIS
        """
        # =====================================================================
        tmpfile = os.path.join('data', 'test_export_tousmois.txt')
        proc = Export(stations=['K1321810'], datatype='TOUSMOIS',
                      first_dt=dt(2018, 1, 1), last_dt=dt(2020, 12, 31))
        proc.set_export_tousmois()
        proc.write(filename=tmpfile)
        filename = os.path.join(self.dirname, 'TOUSMOIS_K1321810.txt')
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        tmpfile = os.path.join('data', 'test_export_tousmois_prec1.txt')
        proc = Export(
            stations=['K1251810', 'K1321810', 'K1341810'], datatype='TOUSMOIS',
            first_dt=dt(2018, 1, 1), last_dt=dt(2020, 12, 31), precision=1)
        proc.set_export_tousmois(onefile='tm1prec1.txt')
        proc.write(filename=tmpfile)
        filename = os.path.join(self.dirname, 'TOUSMOIS_precision1.txt')
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
