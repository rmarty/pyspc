#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary csv2grpRT

To run all tests just type:
    python -m unittest test_bin_csv2grpRT

To run only a class test:
    python -m unittest test_bin_csv2grpRT.Test_csv2grpRT

To run only a specific test:
    python -m unittest test_bin_csv2grpRT.Test_csv2grpRT.test_data

"""
# Imports
import filecmp
import os
import sys
import unittest

from pyspc.binutils.args import csv2grpRT as _args
from pyspc.binutils.runs import csv2grpRT as _runs
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_csv2grpRT(unittest.TestCase):
    """
    csv2grpRT bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'csv2grpRT')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_grp16 = os.path.join('data', 'model', 'grp16', 'cal')
        cls.dir_grp18 = os.path.join('data', 'model', 'grp18', 'cal')
        cls.dir_grp20 = os.path.join('data', 'model', 'grp20', 'cal')
        cls.dir_grp22 = os.path.join('data', 'model', 'grp22', 'cal')
        cls.dir_pyspc = os.path.join('data', 'core', 'csv')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.stations_list_file = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)

    def test_grp16_data(self):
        """
        Test fichier d'observations
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'grp16.txt')
        processArgs = [
            'python',
            '../bin/csv2grpRT.py',
            '-I', self.dir_grp16,
            '-l', self.stations_list_file,
            '-O', self.dir_out,
            '-n', 'PH', 'QH',
            '-t', 'grp16_rt_data',
            '-C', 'grp16',
            '-w'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2grpRT()
        self.assertEqual(args.input_dir, self.dir_grp16)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, ['PH', 'QH'])
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'grp16_rt_data')
        self.assertEqual(args.csv_type, 'grp16')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.csv2grpRT(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion des données depuis le format csv de "\
            "GRP v2016 ({C}) des séries listées dans {l}, les grandeurs {n}, "\
            "dont les fichiers sont dans le répertoire {I} "\
            "vers les fichiers d'entrée pour GRP RT v2016 ({t}) "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                l=args.stations_list_file, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_grp18_data(self):
        """
        Test fichier d'observations
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'grp18.txt')
        processArgs = [
            'python',
            '../bin/csv2grpRT.py',
            '-I', self.dir_grp18,
            '-l', self.stations_list_file,
            '-O', self.dir_out,
            '-n', 'PH', 'QI',
            '-t', 'grp18_rt_data',
            '-C', 'grp18',
            '-w'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2grpRT()
        self.assertEqual(args.input_dir, self.dir_grp18)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, ['PH', 'QI'])
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'grp18_rt_data')
        self.assertEqual(args.csv_type, 'grp18')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.csv2grpRT(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion des données depuis le format csv de "\
            "GRP v2018 ({C}) des séries listées dans {l}, les grandeurs {n}, "\
            "dont les fichiers sont dans le répertoire {I} "\
            "vers les fichiers d'entrée pour GRP RT v2018 ({t}) "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                l=args.stations_list_file, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_grp20_data(self):
        """
        Test fichier d'observations
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'grp20.txt')
        processArgs = [
            'python',
            '../bin/csv2grpRT.py',
            '-I', self.dir_grp20,
            '-l', self.stations_list_file,
            '-O', self.dir_out,
            '-n', 'PH', 'QI',
            '-t', 'grp20_rt_data',
            '-C', 'grp20',
            '-w'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2grpRT()
        self.assertEqual(args.input_dir, self.dir_grp20)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, ['PH', 'QI'])
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'grp20_rt_data')
        self.assertEqual(args.csv_type, 'grp20')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.csv2grpRT(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion des données depuis le format csv de "\
            "GRP v2020 ({C}) des séries listées dans {l}, les grandeurs {n}, "\
            "dont les fichiers sont dans le répertoire {I} "\
            "vers les fichiers d'entrée pour GRP RT v2020 ({t}) "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                l=args.stations_list_file, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_grp22_data(self):
        """
        Test fichier d'observations
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'grp22.txt')
        processArgs = [
            'python',
            '../bin/csv2grpRT.py',
            '-I', self.dir_grp22,
            '-l', self.stations_list_file,
            '-O', self.dir_out,
            '-n', 'PH', 'QI',
            '-t', 'grp22_rt_data',
            '-C', 'grp22',
            '-w'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2grpRT()
        self.assertEqual(args.input_dir, self.dir_grp22)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, ['PH', 'QI'])
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'grp22_rt_data')
        self.assertEqual(args.csv_type, 'grp22')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.csv2grpRT(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion des données depuis le format csv de "\
            "GRP v2022 ({C}) des séries listées dans {l}, les grandeurs {n}, "\
            "dont les fichiers sont dans le répertoire {I} "\
            "vers les fichiers d'entrée pour GRP RT v2022 ({t}) "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                l=args.stations_list_file, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_pyspc_archive20(self):
        """
        Test fichier d'archive GRP
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'grp20.txt')
        processArgs = [
            'python',
            '../bin/csv2grpRT.py',
            '-I', self.dir_grp20,
            '-l', self.stations_list_file,
            '-O', self.dir_out,
            '-n', 'PH', 'QI',
            '-t', 'grp20_rt_archive',
            '-C', 'grp20',
            '-w',
            '-v'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2grpRT()
        self.assertEqual(args.input_dir, self.dir_grp20)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, ['PH', 'QI'])
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'grp20_rt_archive')
        self.assertEqual(args.csv_type, 'grp20')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.csv2grpRT(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, 'grp20', os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion des données depuis le format csv de type "\
            "{C} des séries liées à {s}, aux grandeurs {n}, "\
            "dont les fichiers sont dans le répertoire {I} "\
            "vers les fichiers d'entrée pour GRP RT v2020 ({t}) "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_pyspc_archive22(self):
        """
        Test fichier d'archive GRP
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'grp22.txt')
        processArgs = [
            'python',
            '../bin/csv2grpRT.py',
            '-I', self.dir_grp22,
            '-l', self.stations_list_file,
            '-O', self.dir_out,
            '-n', 'PH', 'QI',
            '-t', 'grp22_rt_archive',
            '-C', 'grp22',
            '-w',
            '-v'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2grpRT()
        self.assertEqual(args.input_dir, self.dir_grp22)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, ['PH', 'QI'])
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'grp22_rt_archive')
        self.assertEqual(args.csv_type, 'grp22')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.csv2grpRT(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, 'grp22', os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion des données depuis le format csv de type "\
            "{C} des séries liées à {s}, aux grandeurs {n}, "\
            "dont les fichiers sont dans le répertoire {I} "\
            "vers les fichiers d'entrée pour GRP RT v2022 ({t}) "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_pyspc_data16(self):
        """
        Test fichier d'observations
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2grpRT.py',
            '-I', self.dir_pyspc,
            '-s', 'LaLoireChadrac',
            '-O', self.dir_out,
            '-n', 'PH', 'QH',
            '-t', 'grp16_rt_data',
            '-C', 'pyspc',
            '-w'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2grpRT()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, ['PH', 'QH'])
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'LaLoireChadrac')
        self.assertEqual(args.data_type, 'grp16_rt_data')
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.csv2grpRT(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion des données depuis le format csv de type "\
            "{C} des séries liées à {s}, aux grandeurs {n}, "\
            "dont les fichiers sont dans le répertoire {I} "\
            "vers les fichiers d'entrée pour GRP RT v2016 ({t}) "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_pyspc_data18(self):
        """
        Test fichier d'observations
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2grpRT.py',
            '-I', self.dir_pyspc,
            '-s', 'LaLoireChadrac',
            '-O', self.dir_out,
            '-n', 'PH', 'QH',
            '-t', 'grp18_rt_data',
            '-C', 'pyspc',
            '-w'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2grpRT()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, ['PH', 'QH'])
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'LaLoireChadrac')
        self.assertEqual(args.data_type, 'grp18_rt_data')
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.csv2grpRT(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion des données depuis le format csv de type "\
            "{C} des séries liées à {s}, aux grandeurs {n}, "\
            "dont les fichiers sont dans le répertoire {I} "\
            "vers les fichiers d'entrée pour GRP RT v2018 ({t}) "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_pyspc_data20(self):
        """
        Test fichier d'observations
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2grpRT.py',
            '-I', self.dir_pyspc,
            '-s', 'LaLoireChadrac',
            '-O', self.dir_out,
            '-n', 'PH', 'QH',
            '-t', 'grp20_rt_data',
            '-C', 'pyspc',
            '-w'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2grpRT()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, ['PH', 'QH'])
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'LaLoireChadrac')
        self.assertEqual(args.data_type, 'grp20_rt_data')
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.csv2grpRT(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, 'grp20', os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion des données depuis le format csv de type "\
            "{C} des séries liées à {s}, aux grandeurs {n}, "\
            "dont les fichiers sont dans le répertoire {I} "\
            "vers les fichiers d'entrée pour GRP RT v2020 ({t}) "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_pyspc_data22(self):
        """
        Test fichier d'observations
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2grpRT.py',
            '-I', self.dir_pyspc,
            '-s', 'LaLoireChadrac',
            '-O', self.dir_out,
            '-n', 'PH', 'QH',
            '-t', 'grp22_rt_data',
            '-C', 'pyspc',
            '-w'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2grpRT()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, ['PH', 'QH'])
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'LaLoireChadrac')
        self.assertEqual(args.data_type, 'grp22_rt_data')
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.csv2grpRT(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, 'grp22', os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion des données depuis le format csv de type "\
            "{C} des séries liées à {s}, aux grandeurs {n}, "\
            "dont les fichiers sont dans le répertoire {I} "\
            "vers les fichiers d'entrée pour GRP RT v2022 ({t}) "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_pyspc_scenmeteo16(self):
        """
        Test fichier d'observations
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2grpRT.py',
            '-I', self.dir_pyspc,
            '-s', 'Gazeille_2017061312',
            '-O', self.dir_out,
            '-n', 'PH',
            '-t', 'grp16_rt_data',
            '-C', 'pyspc',
            '-w'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2grpRT()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, ['PH'])
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'Gazeille_2017061312')
        self.assertEqual(args.data_type, 'grp16_rt_data')
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.csv2grpRT(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion des données depuis le format csv de type "\
            "{C} des séries liées à {s}, aux grandeurs {n}, "\
            "dont les fichiers sont dans le répertoire {I} "\
            "vers les fichiers de scénarios météo pour GRP RT v2016 ({t}) "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_pyspc_scenmeteo18(self):
        """
        Test fichier d'observations
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2grpRT.py',
            '-I', self.dir_pyspc,
            '-s', 'Gazeille_2017061312',
            '-O', self.dir_out,
            '-n', 'PH',
            '-t', 'grp18_rt_metscen',
            '-C', 'pyspc',
            '-w'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2grpRT()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, ['PH'])
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'Gazeille_2017061312')
        self.assertEqual(args.data_type, 'grp18_rt_metscen')
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.csv2grpRT(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion des données depuis le format csv de type "\
            "{C} des séries liées à {s}, aux grandeurs {n}, "\
            "dont les fichiers sont dans le répertoire {I} "\
            "vers les fichiers de scénarios météo pour GRP RT v2018 ({t}) "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_pyspc_scenmeteo20(self):
        """
        Test fichier d'observations
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2grpRT.py',
            '-I', self.dir_pyspc,
            '-s', 'Gazeille_2017061312',
            '-O', self.dir_out,
            '-n', 'PH',
            '-t', 'grp20_rt_metscen',
            '-C', 'pyspc',
            '-w'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2grpRT()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, ['PH'])
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'Gazeille_2017061312')
        self.assertEqual(args.data_type, 'grp20_rt_metscen')
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.csv2grpRT(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, 'grp20', os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion des données depuis le format csv de type "\
            "{C} des séries liées à {s}, aux grandeurs {n}, "\
            "dont les fichiers sont dans le répertoire {I} "\
            "vers les fichiers de scénarios météo pour GRP RT v2020 ({t}) "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_pyspc_scenmeteo22(self):
        """
        Test fichier d'observations
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2grpRT.py',
            '-I', self.dir_pyspc,
            '-s', 'Gazeille_2017061312',
            '-O', self.dir_out,
            '-n', 'PH',
            '-t', 'grp22_rt_metscen',
            '-C', 'pyspc',
            '-w'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2grpRT()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, ['PH'])
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, 'Gazeille_2017061312')
        self.assertEqual(args.data_type, 'grp22_rt_metscen')
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.csv2grpRT(args)
        for f in filenames:
            self.assertTrue(filecmp.cmp(
                f, os.path.join(self.dir_ctl, 'grp22', os.path.basename(f))))
            os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion des données depuis le format csv de type "\
            "{C} des séries liées à {s}, aux grandeurs {n}, "\
            "dont les fichiers sont dans le répertoire {I} "\
            "vers les fichiers de scénarios météo pour GRP RT v2022 ({t}) "\
            "dans le répertoire {O}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================
