#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Series in pyspc.core.series

To run all tests just type:
    python -m unittest test_core_Series_INIT

To run only a class test:
    python -m unittest test_core_Series_INIT.TestSeries

To run only a specific test:
    python -m unittest test_core_Series_INIT.TestSeries.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import pandas as pnd
import unittest
import warnings

# Imports pyspc
from pyspc.core.serie import Serie
from pyspc.core.series import Series


class TestSeries(unittest.TestCase):
    """
    Series class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        # =====================================================================
        provider1 = 'SPC'
        varname1 = 'QH'
        code1 = 'K0000000'
        serie1 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500, 86.400, 82.300, 78.300],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie1.columns = [(code1, varname1)]
        self.serie1 = Serie(
            datval=serie1, code=code1, provider=provider1, varname=varname1,
        )
        # =====================================================================
        provider2 = 'SPC'
        varname2 = 'QH'
        code2 = 'K9999999'
        serie2 = pnd.DataFrame(
            [10, 12, 14, 17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie2.columns = [(code2, varname2)]
        self.serie2 = Serie(
            datval=serie2, code=code2, provider=provider2, varname=varname2
        )
        # =====================================================================
        provider3 = 'SPC'
        varname3 = 'QH'
        code3 = 'K3333333'
        serie3 = pnd.DataFrame(
            [17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 3), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie3.columns = [(code3, varname3)]
        self.serie3 = Serie(
            datval=serie3, code=code3, provider=provider3, varname=varname3
        )
        # =====================================================================
        provider4 = 'SPC'
        varname4 = 'QH'
        code4 = 'K4444444'
        serie4 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 9), freq=td(hours=1)
            )
        )
        serie4.columns = [(code4, varname4)]
        self.serie4 = Serie(
            datval=serie4, code=code4, provider=provider4, varname=varname4,
        )
        # =====================================================================
        self.model = 'PLATHYNES'
        self.series = Series(datatype='obs')
        self.series.add(serie=self.serie1)
        self.series.add(serie=self.serie2)
        self.series.add(serie=self.serie3)
        self.series.add(serie=self.serie4)
        # =====================================================================

    def test_init(self):
        """
        Test de la création de l'instance
        """
        datatype = 'obs'
        series = Series(datatype=datatype)
        self.assertEqual(series.datatype, datatype)
        self.assertEqual(series.codes, [])
        self.assertEqual(series.varnames, [])
        self.assertEqual(series.meta, [])
        with self.assertRaises(ValueError):
            Series()
        with self.assertRaises(ValueError):
            Series(datatype='event')

    def test_add(self):
        """
        Test de la méthode add
        """
        # =====================================================================
        datatype = 'obs'
        series = Series(datatype=datatype)
        # =====================================================================
        series.add(code=self.serie1.code, serie=self.serie1)
        self.assertEqual(list(series.keys()), [('K0000000', 'QH', None)])
        self.assertEqual(series.codes, ['K0000000'])
        self.assertEqual(series.varnames, ['QH'])
        self.assertEqual(series.meta, [None])
        # =====================================================================
        series.add(serie=self.serie2, meta=self.model)
        self.assertEqual(
            list(series.keys()),
            [('K0000000', 'QH', None), ('K9999999', 'QH', self.model)])
        self.assertEqual(series.codes, ['K0000000', 'K9999999'])
        self.assertEqual(series.varnames, ['QH', 'QH'])
        self.assertEqual(series.meta, [None, self.model])
        # =====================================================================

    def test_arg2dict(self):
        """
        Test de la mise en dictionnaire d'un argument
        """
        # =====================================================================
        valid_arg = {
            ('K0000000', 'QH', None): 1,
            ('K9999999', 'QH', None): 1,
            ('K3333333', 'QH', None): 1,
            ('K4444444', 'QH', None): 1,
        }
        tmparg = self.series._arg2dict(1)
        self.assertDictEqual(valid_arg, tmparg)
        # =====================================================================
        in_arg = {
            ('K0000000', 'QH', None): 1,
            ('K3333333', 'QH', None): 3,
        }
        valid_arg = {
            ('K0000000', 'QH', None): 1,
            ('K9999999', 'QH', None): None,
            ('K3333333', 'QH', None): 3,
            ('K4444444', 'QH', None): None,
        }
        tmparg = self.series._arg2dict(in_arg)
        self.assertDictEqual(valid_arg, tmparg)
        # =====================================================================
        valid_arg = {
            ('K0000000', 'QH', None): 1,
            ('K9999999', 'QH', None): 2,
            ('K3333333', 'QH', None): 3,
            ('K4444444', 'QH', None): 4,
        }
        tmparg = self.series._arg2dict(valid_arg)
        self.assertDictEqual(valid_arg, tmparg)
        # =====================================================================

    def test_asobs(self):
        """
        Test de la méthode _asobs
        """
        # =====================================================================
        datatype = 'fcst'
        series = Series(datatype=datatype)
        series.add(serie=self.serie1, meta=(dt(2014, 11, 4, 0), '1'))
        series.add(serie=self.serie2, meta=(dt(2014, 11, 4, 0), '1'))
        # =====================================================================
        obs_series = series.asobs()
        self.assertEqual(
            list(obs_series.keys()),
            [('K0000000_2014110400_1', 'QH', None),
             ('K9999999_2014110400_1', 'QH', None)])
        # =====================================================================

    def test_extend(self):
        """
        Test de la méthode extend
        """
        # =====================================================================
        datatype = 'obs'
        source = Series(datatype=datatype)
        series = Series(datatype=datatype)
        # =====================================================================
        source.add(code=self.serie1.code, serie=self.serie1)
        source.add(serie=self.serie2, meta=self.model)
        # =====================================================================
        series.extend(source)
        self.assertEqual(
            list(series.keys()),
            [('K0000000', 'QH', None), ('K9999999', 'QH', self.model)])
        self.assertEqual(series.codes, ['K0000000', 'K9999999'])
        self.assertEqual(series.varnames, ['QH', 'QH'])
        self.assertEqual(series.meta, [None, self.model])
        # =====================================================================

    def test_check_series(self):
        """
        Test du contrôle du contenu de la collection
        """
        # =====================================================================
        datatype = 'obs'
        # =====================================================================
        series = Series(datatype=datatype)
        series.add(serie=self.serie1)
        series.add(serie=self.serie2)
        self.assertTrue(series.check_series())
        # =====================================================================
        series = Series(datatype=datatype)
        series.add(serie=self.serie1)
        series.add(serie=self.serie2)
        series[('K9999999', 'QH', None)] = None
        self.assertFalse(series.check_series())
        # =====================================================================

    def test_datatype(self):
        """
        Test des types de collections
        """
        self.assertEqual(Series.get_types(), ['obs', 'fcst'])

    def test_replace_keys(self):
        """
        Test de la méthode replace_keys
        """
        # =====================================================================
        datatype = 'fcst'
        series = Series(datatype=datatype)
        series.add(serie=self.serie1, meta=(dt(2014, 11, 4, 0), '1'))
        series.add(serie=self.serie2, meta=(dt(2014, 11, 4, 0), '1'))
        # =====================================================================
        series.replace_keys(
            {('K0000000', 'QH', (dt(2014, 11, 4, 0), '1')):
                ('K0000000', 'QH', (dt(2014, 11, 4, 0), '2'))}
        )
        self.assertNotIn(
            ('K0000000', 'QH', (dt(2014, 11, 4, 0), '1', None, None)), series)
        self.assertIn(
            ('K0000000', 'QH', (dt(2014, 11, 4, 0), '2', None, None)), series)
        # =====================================================================

    def test_unique_code(self):
        """
        Test de l'unicité des codes
        """
        # =====================================================================
        datatype = 'obs'
        series = Series(datatype=datatype)
        series.add(serie=self.serie1)
        series.add(code='K0000000', serie=self.serie2)
        self.assertTrue(series.check_unique_code())
        # =====================================================================
        series = Series(datatype=datatype)
        series.add(serie=self.serie1)
        series.add(serie=self.serie2)
        self.assertFalse(series.check_unique_code())
        # =====================================================================

    def test_unique_runtime(self):
        """
        Test de l'unicité des codes
        """
        # =====================================================================
        datatype = 'fcst'
        series = Series(datatype=datatype)
        series.add(serie=self.serie1, meta=(dt(2014, 11, 4, 0), 1, None))
        series.add(serie=self.serie2, meta=(dt(2014, 11, 4, 0), 1, None))
        self.assertTrue(series.check_unique_runtime())
        # =====================================================================
        series = Series(datatype=datatype)
        series.add(serie=self.serie1, meta=(dt(2014, 11, 4, 0), 1, None))
        series.add(serie=self.serie2, meta=(dt(2014, 11, 5, 0), 1, None))
        self.assertFalse(series.check_unique_runtime())
        # =====================================================================

    def test_unique_varname(self):
        """
        Test de l'unicité des variables
        """
        # =====================================================================
        datatype = 'obs'
        # =====================================================================
        series = Series(datatype=datatype)
        series.add(serie=self.serie1)
        series.add(serie=self.serie2)
        self.assertTrue(series.check_unique_varname())
        # =====================================================================
        series = Series(datatype=datatype)
        series.add(serie=self.serie1)
        self.serie2._spc_varname = 'PH'
        series.add(serie=self.serie2)
        self.assertFalse(series.check_unique_varname())
        # =====================================================================
