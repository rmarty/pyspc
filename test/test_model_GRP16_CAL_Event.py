#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP_Event in pyspc.model.grp16

To run all tests just type:
    python -m unittest test_model_GRP16_CAL_Event

To run only a class test:
    python -m unittest test_model_GRP16_CAL_Event.TestGRP_Event

To run only a specific test:
    python -m unittest test_model_GRP16_CAL_Event.TestGRP_Event.test_init

"""
# Imports
from datetime import datetime as dt
import filecmp
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.model.grp16 import GRP_Event


class TestGRP_Event(unittest.TestCase):
    """
    GRP_Event class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp16', 'cal')
        self.valid = pnd.DataFrame(
            {'Date': pnd.date_range(
                dt(2017, 6, 13, 12), dt(2017, 6, 14, 12), freq='H'),
             'Pluie(mm/h)': [
                0.0000, 0.0000, 0.3000, 2.7500, 2.6900, 9.2300, 46.6800,
                61.3250, 15.7500, 5.0500, 0.0600, 0.0000, 0.0000, 0.0000,
                0.0000, 0.0000, 0.0600, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
                0.0000, 0.0000, 0.8400],
             'Debit(m3/s)': [
                 0.57, 0.57, 0.56, 0.59, 0.58, 0.70, 11.20, 121.00, 203.00,
                 159.00, 92.80, 53.80, 38.30, 28.00, 22.90, 20.40, 18.90,
                 17.00, 15.50, 14.30, 13.10, 12.40, 11.80, 11.20, 10.60]},
        )

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'K0114030-EV0001.DAT')
        reader = GRP_Event(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.station, 'K0114030')
        self.assertEqual(reader.event, 'EV0001')

    def test_read(self):
        """
        Test de la lecture d'un fichier GRP Event
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K0114030-EV0001.DAT')
        reader = GRP_Event(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid, df)
        # =====================================================================

    def test_write(self):
        """
        Test de l'écriture d'un fichier Event
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K0114030-EV0001.DAT')
        tmpfile = os.path.join('data', 'K0114030-EV0001.DAT')
        writer = GRP_Event(filename=tmpfile)
        writer.write(data=self.valid)
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
