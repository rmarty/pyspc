#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Vigicrues_Reach in pyspc.metadata.vigicrues

To run all tests just type:
    python -m unittest test_data_Vigicrues

To run only a class test:
    python -m unittest test_data_Vigicrues.TestVigicrues_Data

To run only a specific test:
    python -m unittest test_data_Vigicrues.TestVigicrues_Data.test_init

"""
# Imports
from datetime import datetime as dt
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.data.vigicrues import Vigicrues_Data, Vigicrues_Fcst


class TestVigicrues_Data(unittest.TestCase):
    """
    Vigicrues class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'vigicrues')

    def test_init(self):
        """
        Test des méthodes de lecture - Utile si unittest + mock sur urllib
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K118001010_observation.json')
        vigi = Vigicrues_Data(filename=filename)
        self.assertEqual(vigi.filename, filename)
        # =====================================================================

    def test_read(self):
        """
        Test des méthodes de lecture
        """
        valid = pnd.DataFrame(
            {('K118001010', 'Q'): [
                19.2, 19.1, 19.1, 19.0, 19.3, 19.0, 19.4, 19.6, 19.7, 19.4,
                19.4, 19.6, 19.7, 19.6, 19.7, 19.7, 19.6, 19.6, 19.6, 19.3,
                19.4, 19.4, 19.4, 19.4, 19.4, 19.7, 19.6, 19.3, 19.3, 19.6,
                19.6, 19.6, 19.7, 19.7, 19.4, 19.4, 19.7, 19.7, 19.6, 19.6,
                19.6, 19.7, 19.7, 19.6, 19.8, 19.6, 19.7, 19.6, 19.7, 19.6]},
            index=[dt(2022, 3, 29, 10,), dt(2022, 3, 29, 10, 5),
                   dt(2022, 3, 29, 10, 20), dt(2022, 3, 29, 10, 25),
                   dt(2022, 3, 29, 10, 30), dt(2022, 3, 29, 10, 35),
                   dt(2022, 3, 29, 10, 50), dt(2022, 3, 29, 10, 55),
                   dt(2022, 3, 29, 11,), dt(2022, 3, 29, 11, 5),
                   dt(2022, 3, 29, 11, 10), dt(2022, 3, 29, 11, 15),
                   dt(2022, 3, 29, 11, 20), dt(2022, 3, 29, 11, 25),
                   dt(2022, 3, 29, 11, 30), dt(2022, 3, 29, 11, 35),
                   dt(2022, 3, 29, 11, 40), dt(2022, 3, 29, 11, 45),
                   dt(2022, 3, 29, 11, 50), dt(2022, 3, 29, 11, 55),
                   dt(2022, 3, 29, 12,), dt(2022, 3, 29, 12, 5),
                   dt(2022, 3, 29, 12, 10), dt(2022, 3, 29, 12, 15),
                   dt(2022, 3, 29, 12, 20), dt(2022, 3, 29, 12, 25),
                   dt(2022, 3, 29, 12, 30), dt(2022, 3, 29, 12, 35),
                   dt(2022, 3, 29, 12, 40), dt(2022, 3, 29, 12, 45),
                   dt(2022, 3, 29, 12, 50), dt(2022, 3, 29, 12, 55),
                   dt(2022, 3, 29, 13,), dt(2022, 3, 29, 13, 5),
                   dt(2022, 3, 29, 13, 10), dt(2022, 3, 29, 13, 20),
                   dt(2022, 3, 29, 13, 25), dt(2022, 3, 29, 13, 30),
                   dt(2022, 3, 29, 13, 35), dt(2022, 3, 29, 13, 40),
                   dt(2022, 3, 29, 13, 45), dt(2022, 3, 29, 13, 50),
                   dt(2022, 3, 29, 13, 55), dt(2022, 3, 29, 14,),
                   dt(2022, 3, 29, 14, 5), dt(2022, 3, 29, 14, 10),
                   dt(2022, 3, 29, 14, 15), dt(2022, 3, 29, 14, 20),
                   dt(2022, 3, 29, 14, 25), dt(2022, 3, 29, 14, 30)]
        )
        # =====================================================================
        filename = os.path.join(self.dirname, 'K118001010_observation.json')
        vigi = Vigicrues_Data(filename=filename)
        df = vigi.read()
        assert_frame_equal(valid, df)
        # =====================================================================

    def test_varname(self):
        """
        Test de la correspondance des variables Vigicrues et pyspc
        """
        valid = ['H', 'Q']
        self.assertEqual(Vigicrues_Data.get_varnames(), valid)


class TestVigicrues_Fcst(unittest.TestCase):
    """
    Vigicrues class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'data', 'vigicrues')

    def test_init(self):
        """
        Test des méthodes de lecture - Utile si unittest + mock sur urllib
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'K118001010_prevision.json')
        vigi = Vigicrues_Fcst(filename=filename)
        self.assertEqual(vigi.filename, filename)
        # =====================================================================

    def test_read(self):
        """
        Test des méthodes de lecture
        """
        valid = pnd.DataFrame(
            {
                ('K118001010', 'Q', dt(2022, 3, 29, 12, 6, 52), 'ResMinPrev'):
                    [19.00, 18.97, 18.95, 18.92, 18.90, 18.87, 18.84, 18.82,
                     18.79],
                ('K118001010', 'Q', dt(2022, 3, 29, 12, 6, 52), 'ResMoyPrev'):
                    [19.61, 19.60, 19.59, 19.58, 19.57, 19.55, 19.54, 19.53,
                     19.52],
                ('K118001010', 'Q', dt(2022, 3, 29, 12, 6, 52), 'ResMaxPrev'):
                    [20.10, 20.11, 20.11, 20.11, 20.11, 20.12, 20.12, 20.12,
                     20.12],
            },
            index=pnd.date_range(dt(2022, 3, 29, 15), dt(2022, 3, 29, 23),
                                 freq='H')
        )
        # =====================================================================
        filename = os.path.join(self.dirname, 'K118001010_prevision.json')
        vigi = Vigicrues_Fcst(filename=filename)
        df = vigi.read()
        assert_frame_equal(valid, df)
        # =====================================================================

    def test_trend(self):
        """
        Test de la correspondance des variables Vigicrues et pyspc
        """
        valid = ['ResMaxPrev', 'ResMinPrev', 'ResMoyPrev']
        self.assertEqual(Vigicrues_Fcst.get_trends(), valid)

    def test_varname(self):
        """
        Test de la correspondance des variables Vigicrues et pyspc
        """
        valid = ['H', 'Q']
        self.assertEqual(Vigicrues_Fcst.get_varnames(), valid)
