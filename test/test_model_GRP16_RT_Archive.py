#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Archive in pyspc.model.grp16

To run all tests just type:
    python -m unittest test_model_GRP16_RT_Archive

To run only a class test:
    python -m unittest test_model_GRP16_RT_Archive.TestGRPRT_Archive

To run only a specific test:
    python -m unittest test_model_GRP16_RT_Archive.TestGRPRT_Archive.test_init

"""
# Imports
from datetime import datetime as dt
import filecmp
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.model.grp16 import GRPRT_Archive


class TestGRPRT_Archive(unittest.TestCase):
    """
    GRPRT_Archive class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp16', 'rt')
        self.valid_P = pnd.DataFrame(
            {'43091005': [0.000, 0.000, 0.000, 23.800, 2.600, 2.200,
                          1.200, 4.900, 34.200, 0.200, 0.000, 0.000]},
            index=pnd.date_range(
                dt(2017, 6, 13, 12), dt(2017, 6, 13, 23), freq='H')
        )
        self.valid_P.columns.name = 'Code'
        self.valid_P.index.name = 'Date (TU)'
        self.valid_Q = pnd.DataFrame(
            {'K0114030': [0.570, 0.570, 0.560, 0.590, 0.580, 0.700,
                          11.200, 121.000, 203.000, 159.000, 92.800, 53.800]},
            index=pnd.date_range(
                dt(2017, 6, 13, 12), dt(2017, 6, 13, 23), freq='H')
        )
        self.valid_Q.columns.name = 'Code'
        self.valid_Q.index.name = 'Date (TU)'
        self.valid_T = pnd.DataFrame(
            {'07105003': [0.9, 1.1, 0.7, 0.6, 0.2, -0.5, -0.5, -1.4, -1.9,
                          -2.3, -2.8, -3.1, -3.4, -3.7, -4.0, -4.0, -4.2, -4.6,
                          -4.7, -4.9, -4.9, -3.6, -1.7, 0.3, 1.1, 2.1, 2.0,
                          2.2, 2.6, 2.1, 1.2]},
            index=pnd.date_range(
                dt(2014, 1, 14, 12),  dt(2014, 1, 15, 18),  freq='H')
        )
        self.valid_T.columns.name = 'Code'
        self.valid_T.index.name = 'Date (TU)'

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'K0403010', 'QV_10A.DAT')
        reader = GRPRT_Archive(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'QV')

    def test_read_PV(self):
        """
        Test de lecture d'un fichier Archive de GRP Temps Réel - Pluie hor.
        """
        filename = os.path.join(self.dirname, 'PV_10A.DAT')
        reader = GRPRT_Archive(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_P, df)

    def test_read_QV(self):
        """
        Test de lecture d'un fichier Archive de GRP Temps Réel - Débit inst.
        """
        filename = os.path.join(self.dirname, 'QV_10A.DAT')
        reader = GRPRT_Archive(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_Q, df)

    def test_read_TV(self):
        """
        Test de lecture d'un fichier Archive de GRP Temps Réel - Temp hor.
        """
        filename = os.path.join(self.dirname, 'TV_10A.DAT')
        reader = GRPRT_Archive(filename=filename)
        df = reader.read()
        assert_frame_equal(self.valid_T, df)

    def test_write_PV(self):
        """
        Test de l'écriture d'un fichier Archive de GRP Temps Réel - PV
        """
        basename = 'PV_10A.DAT'
        tmpfile = os.path.join('data', basename)
        writer = GRPRT_Archive(filename=tmpfile)
        writer.write(data=self.valid_P)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, basename),
            tmpfile
        ))
        os.remove(tmpfile)

    def test_write_QV(self):
        """
        Test de l'écriture d'un fichier Archive de GRP Temps Réel - QV
        """
        basename = 'QV_10A.DAT'
        tmpfile = os.path.join('data', basename)
        writer = GRPRT_Archive(filename=tmpfile)
        writer.write(data=self.valid_Q)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, basename),
            tmpfile
        ))
        os.remove(tmpfile)

    def test_write_TV(self):
        """
        Test de l'écriture d'un fichier Archive de GRP Temps Réel - TV
        """
        basename = 'TV_10A.DAT'
        tmpfile = os.path.join('data', basename)
        writer = GRPRT_Archive(filename=tmpfile)
        writer.write(data=self.valid_T)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, basename),
            tmpfile
        ))
        os.remove(tmpfile)

    def test_basename(self):
        """
        Test nom de base
        """
        valid = {
            'PV': 'PV_10A.DAT',
            'QV': 'QV_10A.DAT',
            'TV': 'TV_10A.DAT',
        }
        for k, v in valid.items():
            self.assertEqual(
                 GRPRT_Archive.split_basename(filename=v),
                 k
            )

    def test_varname(self):
        """
        Test de la correspondance des variables selon GRP et selon SPC_LCI
        """
        valid = ['PV', 'QV', 'TV']
        self.assertEqual(GRPRT_Archive.get_varnames(), valid)
