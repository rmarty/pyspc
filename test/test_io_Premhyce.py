#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_Premhyce and write_Premhyce in pyspc.data.premhyce

To run all tests just type:
    python -m unittest test_data_Premhyce

To run only a class test:
    python -m unittest test_data_Premhyce.TestPremhyce

To run only a specific test:
    python -m unittest test_data_Premhyce.TestPremhyce.test_init

"""
# Imports
from datetime import datetime as dt
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.core.series import Series
from pyspc.io.premhyce import read_Premhyce  # , write_Premhyce


class TestPremhyce(unittest.TestCase):
    """
    Premhyce_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'premhyce')

    def test_read_obs(self):
        """
        Test de lecture d'un fichier de données QMJ pour PREMHYCE
        """
        # =====================================================================
        valid = {
            'K0253030': pnd.DataFrame(
                [1.07, 0.984, 0.919, 53.5, 37.8, 13.3],
                index=pnd.date_range(dt(2020, 6, 9), dt(2020, 6, 14), freq='D')
            ),
            'K0260020': pnd.DataFrame(
                [4.34, 5.22, 5.22, 207.0, 219.0, 86.8],
                index=pnd.date_range(dt(2020, 6, 9), dt(2020, 6, 14), freq='D')
            ),
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'Debits.txt')
        series = read_Premhyce(filename=filename)
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            self.assertIn(c, valid)
            valid[c].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[c],
                               check_less_precise=3)
#        for k in series:
#            s = series[k]
#            print("'{}': pnd.DataFrame(".format(s.code))
#            print("    {},".format([int(v*1000)/1000. for v in s.data_frame.values]))
#            print("    index=pnd.date_range(dt(2020, 6, 9), dt(2020, 6, 14), freq='D')")
#            print("),")
        # =====================================================================

    def test_read_fcst(self):
        """
        Test de lecture d'un fichier de prévisions QMJ par PREMHYCE
        """
        # =====================================================================
        valid = {
            'K0253030_2020061200_GR6J_CEPcf': pnd.DataFrame(
                [5.059, 12.811, 8.096, 5.506, 4.661, 4.413, 3.782, 3.3],
                index=pnd.date_range(dt(2020, 6, 12), dt(2020, 6, 19), freq='D')
            ),
            'K0253030_2020061200_GR6J_CEPpf1': pnd.DataFrame(
                [3.907, 6.181, 3.927, 3.137, 2.976, 3.738, 3.909, 3.266],
                index=pnd.date_range(dt(2020, 6, 12), dt(2020, 6, 19), freq='D')
            ),
            'K0253030_2020061200_GR6J_CEPpf2': pnd.DataFrame(
                [3.117, 2.821, 2.013, 1.816, 1.748, 1.871, 1.795, 1.646],
                index=pnd.date_range(dt(2020, 6, 12), dt(2020, 6, 19), freq='D')
            ),
            'K0253030_2020061200_GR6J_CEPpf3': pnd.DataFrame(
                [4.524, 9.394, 5.792, 4.255, 3.579, 3.283, 3.276, 2.947],
                index=pnd.date_range(dt(2020, 6, 12), dt(2020, 6, 19), freq='D')
            ),
            'K0253030_2020061200_GR6J_CEPpf4': pnd.DataFrame(
                [4.482, 9.159, 5.665, 4.184, 3.501, 3.007, 2.675, 2.475],
                index=pnd.date_range(dt(2020, 6, 12), dt(2020, 6, 19), freq='D')
            ),
            'K0253030_2020061200_GR6J_CEPpf5': pnd.DataFrame(
                [4.054, 6.9, 4.366, 3.411, 2.926, 2.662, 2.867, 3.453],
                index=pnd.date_range(dt(2020, 6, 12), dt(2020, 6, 19), freq='D')
            ),
            'K0253030_2020061200_GR6J_CEPpf6': pnd.DataFrame(
                [4.838, 11.319, 7.054, 4.963, 4.108, 3.696, 3.479, 3.03],
                index=pnd.date_range(dt(2020, 6, 12), dt(2020, 6, 19), freq='D')
            ),
            'K0253030_2020061200_GR6J_CEPpf7': pnd.DataFrame(
                [3.16, 2.983, 2.095, 1.885, 1.713, 1.57, 1.449, 1.346],
                index=pnd.date_range(dt(2020, 6, 12), dt(2020, 6, 19), freq='D')
            ),
            'K0253030_2020061200_GR6J_CEPpf8': pnd.DataFrame(
                [4.821, 11.248, 7.086, 4.969, 4.065, 3.438, 2.977, 2.624],
                index=pnd.date_range(dt(2020, 6, 12), dt(2020, 6, 19), freq='D')
            ),
            'K0253030_2020061200_GR6J_CEPpf9': pnd.DataFrame(
                [4.564, 9.725, 6.228, 4.497, 3.752, 3.225, 2.847, 2.528],
                index=pnd.date_range(dt(2020, 6, 12), dt(2020, 6, 19), freq='D')
            ),
            'K0253030_2020061200_GR6J_CEPpf10': pnd.DataFrame(
                [3.449, 4.132, 2.676, 2.351, 2.229, 2.644, 3.109, 2.506],
                index=pnd.date_range(dt(2020, 6, 12), dt(2020, 6, 19), freq='D')
            ),
        }
        # =====================================================================
        filename = os.path.join(self.dirname, '20200612_K0253030_GR6J_CEP.txt')
        series = read_Premhyce(filename=filename)
        self.assertIsInstance(series, Series)
        for k in series:
            c = series[k].code
            v = series[k].spc_varname
            self.assertIn(c, valid)
            valid[c].columns = [(c, v)]
            assert_frame_equal(series[k].data_frame, valid[c],
                               check_less_precise=3)
#        dfs = pnd.concat([series[k].data_frame for k in series], axis=1)
#        dfs.columns = ['{}'.format(c[0].split('_')[-1]) for c in dfs.columns]
#        dfs.to_csv(
#            "data/K0253030_2020061200_GR6J_QJ.txt",
#            sep=';',
#            float_format='%.3f',
#            index=True,
#            index_label='AAAAMMJJ',
#            date_format='%Y%m%d',
#            line_terminator='\n'
#        )
        # =====================================================================
