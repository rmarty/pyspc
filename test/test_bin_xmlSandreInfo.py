#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary xmlSandreInfo

To run all tests just type:
    python -m unittest test_bin_xmlSandreInfo

To run only a class test:
    python -m unittest test_bin_xmlSandreInfo.Test_xmlSandreInfo

To run only a specific test:
    python -m unittest test_bin_xmlSandreInfo.Test_xmlSandreInfo.test_loc_hydro_site

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import xmlSandreInfo as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_xmlSandreInfo(unittest.TestCase):
    """
    xmlSandreInfo bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'xmlSandreInfo')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dirname2 = os.path.join('data', 'metadata', 'sandre')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_loc_hydro_zone_child(self):
        """
        Sites hydro3
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandreInfo.py',
            '-I', self.dirname2,
            '-d', 'ZoneHydro_child.xml',
            '-t', 'loc_hydro',
            '-c', os.path.join(self.dir_out, 'ZoneHydro_child.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.xmlSandreInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'ZoneHydro_child.csv'))
        self.assertEqual(args.xml_filename, 'ZoneHydro_child.xml')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.data_type, 'loc_hydro')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' issues de la "\
            "PHyC contenues dans le fichier {d} au format xml Sandre situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type, c=args.output_filename
                )
        # =====================================================================

    def test_loc_hydro_site(self):
        """
        Sites hydro3
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandreInfo.py',
            '-I', self.dirname2,
            '-d', 'SiteHydro.xml',
            '-t', 'loc_hydro',
            '-c', os.path.join(self.dir_out, 'SiteHydro.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.xmlSandreInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'SiteHydro.csv'))
        self.assertEqual(args.xml_filename, 'SiteHydro.xml')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.data_type, 'loc_hydro')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' issues de la "\
            "PHyC contenues dans le fichier {d} au format xml Sandre situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type, c=args.output_filename
                )
        # =====================================================================

    def test_loc_hydro_site_child(self):
        """
        Sites hydro3
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandreInfo.py',
            '-I', self.dirname2,
            '-d', 'SiteHydro_child.xml',
            '-t', 'loc_hydro',
            '-c', os.path.join(self.dir_out, 'SiteHydro_child.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.xmlSandreInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'SiteHydro_child.csv'))
        self.assertEqual(args.xml_filename, 'SiteHydro_child.xml')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.data_type, 'loc_hydro')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' issues de la "\
            "PHyC contenues dans le fichier {d} au format xml Sandre situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type, c=args.output_filename
                )
        # =====================================================================

    def test_loc_hydro_station(self):
        """
        Stations hydro3
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandreInfo.py',
            '-I', self.dirname2,
            '-d', 'StationHydro.xml',
            '-t', 'loc_hydro',
            '-c', os.path.join(self.dir_out, 'StationHydro.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.xmlSandreInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'StationHydro.csv'))
        self.assertEqual(args.xml_filename, 'StationHydro.xml')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.data_type, 'loc_hydro')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' issues de la "\
            "PHyC contenues dans le fichier {d} au format xml Sandre situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type, c=args.output_filename
                )
        # =====================================================================

    def test_loc_hydro_station_child(self):
        """
        Stations hydro3
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandreInfo.py',
            '-I', self.dirname2,
            '-d', 'StationHydro_child.xml',
            '-t', 'loc_hydro',
            '-c', os.path.join(self.dir_out, 'StationHydro_child.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.xmlSandreInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'StationHydro_child.csv'))
        self.assertEqual(args.xml_filename, 'StationHydro_child.xml')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.data_type, 'loc_hydro')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' issues de la "\
            "PHyC contenues dans le fichier {d} au format xml Sandre situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type, c=args.output_filename
                )
        # =====================================================================

    def test_loc_hydro_capteur(self):
        """
        Stations hydro3
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandreInfo.py',
            '-I', self.dirname2,
            '-d', 'CapteurHydro.xml',
            '-t', 'loc_hydro',
            '-c', os.path.join(self.dir_out, 'CapteurHydro.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.xmlSandreInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'CapteurHydro.csv'))
        self.assertEqual(args.xml_filename, 'CapteurHydro.xml')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.data_type, 'loc_hydro')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' issues de la "\
            "PHyC contenues dans le fichier {d} au format xml Sandre situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type, c=args.output_filename
                )
        # =====================================================================

    def test_loc_meteo(self):
        """
        Sites météo
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandreInfo.py',
            '-I', self.dirname2,
            '-d', 'SiteMeteo.xml',
            '-t', 'loc_meteo',
            '-c', os.path.join(self.dir_out, 'SiteMeteo.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.xmlSandreInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, 'SiteMeteo.csv'))
        self.assertEqual(args.xml_filename, 'SiteMeteo.xml')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.data_type, 'loc_meteo')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' issues de la "\
            "PHyC contenues dans le fichier {d} au format xml Sandre situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type, c=args.output_filename
                )
        # =====================================================================

    def test_user_admin(self):
        """
        User admin
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandreInfo.py',
            '-I', self.dirname2,
            '-d', '404_user_admin.xml',
            '-t', 'user',
            '-c', os.path.join(self.dir_out, '404_user_admin.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.xmlSandreInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, '404_user_admin.csv'))
        self.assertEqual(args.xml_filename, '404_user_admin.xml')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.data_type, 'user')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' issues de la "\
            "PHyC contenues dans le fichier {d} au format xml Sandre situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type, c=args.output_filename
                )
        # =====================================================================

    def test_user_loc_meteo(self):
        """
        User loc_meteo
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandreInfo.py',
            '-I', self.dirname2,
            '-d', '404_user_loc_meteo.xml',
            '-t', 'user',
            '-c', os.path.join(self.dir_out, '404_user_loc_meteo.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.xmlSandreInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, '404_user_loc_meteo.csv'))
        self.assertEqual(args.xml_filename, '404_user_loc_meteo.xml')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.data_type, 'user')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' issues de la "\
            "PHyC contenues dans le fichier {d} au format xml Sandre situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type, c=args.output_filename
                )
        # =====================================================================

    def test_user_site_hydro(self):
        """
        User site_hydro
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandreInfo.py',
            '-I', self.dirname2,
            '-d', '404_user_site_hydro.xml',
            '-t', 'user',
            '-c', os.path.join(self.dir_out, '404_user_site_hydro.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.xmlSandreInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out, '404_user_site_hydro.csv'))
        self.assertEqual(args.xml_filename, '404_user_site_hydro.xml')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.data_type, 'user')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' issues de la "\
            "PHyC contenues dans le fichier {d} au format xml Sandre situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type, c=args.output_filename
                )
        # =====================================================================

    def test_user_station_hydro(self):
        """
        User station_hydro
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/xmlSandreInfo.py',
            '-I', self.dirname2,
            '-d', '404_user_station_hydro.xml',
            '-t', 'user',
            '-c', os.path.join(self.dir_out, '404_user_station_hydro.csv')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.xmlSandreInfo()
        self.assertEqual(args.output_filename,
                         os.path.join(self.dir_out,
                                      '404_user_station_hydro.csv'))
        self.assertEqual(args.xml_filename, '404_user_station_hydro.xml')
        self.assertEqual(args.input_dir, self.dirname2)
        self.assertEqual(args.data_type, 'user')
#        self.assertEqual(args.verbose, False)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.assertTrue(filecmp.cmp(
            args.output_filename,
            os.path.join(self.dir_ctl, os.path.basename(args.output_filename)),
        ))
        os.remove(args.output_filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraire les méta-données de type '{t}' issues de la "\
            "PHyC contenues dans le fichier {d} au format xml Sandre situé "\
            "dans le répertoire {I}. Ces informations sont écrites dans le "\
            "fichier {c}."\
            "".format(
                I=args.input_dir, d=args.xml_filename,
                t=args.data_type, c=args.output_filename
                )
        # =====================================================================
