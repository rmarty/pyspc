#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Dbase in pyspc.io.dbase.dbase

To run all tests just type:
    python -m unittest test_io_dbase_Dbase

To run only a class test:
    python -m unittest test_io_dbase_Dbase.TestDbase

To run only a specific test:
    python -m unittest test_io_dbase_Dbase.TestDbase.test_init

"""
# Imports
import os
import unittest
import pyspc.io.dbase.dbase as _dbase


class TestDbase(unittest.TestCase):
    """
    Dbase class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'io', 'dbase')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        with self.assertRaises(ValueError):
            _dbase.Dbase()
        filename = os.path.join(self.dirname, 'sacha_montpezat.mdb')
        reader = _dbase.Dbase(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertIsNone(reader._dbase_connect)
        self.assertIsNone(reader._dbase_cursor)
        self.assertIsNone(reader.sql)
        self.assertIsNone(reader._tables)
