#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP_Fcst in pyspc.model.grp16

To run all tests just type:
    python -m unittest test_model_GRP16_CAL_Fcst

To run only a class test:
    python -m unittest test_model_GRP16_CAL_Fcst.TestGRP_Fcst

To run only a specific test:
    python -m unittest test_model_GRP16_CAL_Fcst.TestGRP_Fcst.test_init

"""
# Imports
from datetime import datetime as dt
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.model.grp16 import GRP_Fcst


class TestGRP_Fcst(unittest.TestCase):
    """
    GRP_Fcst class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp16', 'cal')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(
            self.dirname, 'H_K0114030_GRP_SMN_RNA_006_PP_P1P2.TXT')
        reader = GRP_Fcst(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.station, 'K0114030')
        self.assertEqual(reader.model, 'GRP')
        self.assertEqual(reader.snow, 'SMN')
        self.assertEqual(reader.error, 'RNA')
        self.assertEqual(reader.ltime, 6)
        self.assertEqual(reader.rainfall, 'PP')
        self.assertEqual(reader.periods, 'P1P2')

    def test_read(self):
        """
        Test de la lecture du fichier
        """
        # =====================================================================
        valid = pnd.DataFrame(
            {
                '      OBS000': [87.5000, 109.0000, 118.0000, 118.0000,
                                 93.7000, 77.0000, 66.7000],
                '      OBS001': [109.0000, 118.0000, 118.0000, 93.7000,
                                 77.0000, 66.7000, 54.5000],
                '      PRV001': [96.8811, 112.5370, 107.6801, 98.9367,
                                 81.6000, 67.3828, 59.4912],
                '      OBS003': [118.0000, 93.7000, 77.0000, 66.7000,
                                 54.5000, 41.9000, 37.1000],
                '      PRV003': [93.3325, 88.8415, 79.2574, 69.8446,
                                 62.9213, 54.1299, 45.7360],
                '      OBS006': [66.7000, 54.5000, 41.9000, 37.1000,
                                 34.4000, 30.8000, 28.5000],
                '      PRV006': [61.0790, 59.6308, 53.8012, 48.4250,
                                 42.7485, 38.8937, 35.1245],
                '      OBS009': [37.1000, 34.4000, 30.8000, 28.5000,
                                 26.9000, 25.5000, 23.9000],
                '      PRV009': [43.9939, 41.2272, 38.7904, 36.3978,
                                 32.5838, 29.0221, 25.3258],
                '      OBS012': [28.5000, 26.9000, 25.5000, 23.9000,
                                 22.5000, 21.1000, 20.4000],
                '      PRV012': [34.0956, 31.7594, 28.9885, 26.2405,
                                 23.8233, 22.8651, 22.6630],
            },
            index=pnd.date_range(
                dt(2019, 11, 23, 6),
                dt(2019, 11, 23, 12),
                freq='H'
            )
        )
        valid.index.name = '      DATE'
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'H_K0114030_GRP_SMN_RNA_006_PP_P1P2.TXT')
        reader = GRP_Fcst(filename=filename)
        df = reader.read()
        assert_frame_equal(valid, df)
        # =====================================================================
