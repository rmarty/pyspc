#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2022  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Intern in pyspc.model.grp22

To run all tests just type:
    python -m unittest test_model_GRP22_RT_Intern

To run only a class test:
    python -m unittest test_model_GRP22_RT_Intern.TestGRPRT_Intern

To run only a specific test:
    python -m unittest test_model_GRP22_RT_Intern.TestGRPRT_Intern.test_init

"""
# Imports
from datetime import datetime as dt
import numpy as np
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.model.grp22 import GRPRT_Intern


class TestGRPRT_Intern(unittest.TestCase):
    """
    GRPRT_Intern class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp22', 'rt', 'intern')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = 'PQE_1A.DAT'
        reader = GRPRT_Intern(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertTrue(reader.realtime)

    def test_read(self):
        """
        Test de lecture d'un fichier
        """
        # =====================================================================
        valid = pnd.DataFrame(
            {
                'Qsim(mm)': [
                    0.3648, 0.3603, 0.3556, 0.3762, 0.435, 0.5277, 0.6924,
                    0.8931, 1.117, 1.3697, 1.7101, 2.0794, 2.4511, 2.8393,
                    3.1849, 3.456, 3.6906, 3.9358, 4.2034, 4.5017, 4.7756,
                    4.9946, 5.1346, 5.1242, 4.9236, 4.6138, 4.2787, 3.991,
                    3.7529, 3.5319, 3.3167],
                'Qobs(mm)': [
                    0.3278, 0.3221, 0.3164, 0.3147, 0.3170, 0.3363, 0.4108,
                    0.5981, 0.9132, 1.1836, 1.4474, 1.7065, 1.9306, 2.1194,
                    2.2889, 2.3727, 2.5446, 2.7203, 2.7348, 2.8931, 3.4033,
                    3.9310, 4.1923, 4.2232, 4.0355, 3.5419, 3.1272, 2.9025,
                    2.7505, 2.6294, 2.4929],
                'ETP(mm)': [
                    0.0339, 0.0339, 0.0319, 0.0281, 0.0223, 0.0149, 0.0054,
                    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0059, 0.0159, 0.0238, 0.0301, 0.0339, 0.0361,
                    0.0361, 0.0339, 0.0301, 0.0238, 0.0159, 0.0059],
                'P(mm)': [
                    0.1600, 0.1200, 0.2000, 3.0400, 3.3600, 4.1600, 8.4800,
                    3.7200, 7.6000, 6.3200, 9.4400, 6.6400, 7.8800, 8.4400,
                    5.3200, 6.6400, 5.9600, 6.7200, 7.2400, 7.4400, 6.6000,
                    6.4800, 5.3600, 2.9600, 1.3200, 1.0800, 0.4800, 1.6000,
                    1.0000, 0.7200, 0.8000],
                'T(C)': [np.nan]*31,
                'R(mm)': [
                    25.7599, 25.6019, 25.4333, 26.1551, 28.1094, 30.9363,
                    35.3924, 40.1438, 44.8361, 49.5843, 55.315, 60.9011,
                    66.0251, 70.9631, 75.0713, 78.1332, 80.683, 83.2597,
                    85.9768, 88.9016, 91.4981, 93.5186, 94.786, 94.6924,
                    92.8688, 89.974, 86.7252, 83.8275, 81.3463, 78.9676,
                    76.5763],
                'S(mm)': [
                    247.0715, 247.0227, 246.9898, 247.5004, 248.0608, 248.7550,
                    250.1835, 250.7462, 251.9237, 252.8453, 254.1844, 255.0535,
                    256.0480, 257.0599, 257.6386, 258.3505, 258.9535, 259.6137,
                    260.2969, 260.9651, 261.5183, 262.0343, 262.4276, 262.5975,
                    262.6214, 262.6241, 262.5745, 262.6235, 262.6202, 262.5932,
                    262.5743],
            },
            index=pnd.date_range(
                dt(2007, 1, 18, 12), dt(2007, 1, 19, 18), freq='H')
        )
        valid.index.name = 'DATE (TU)'
        # =====================================================================
        filename = os.path.join(self.dirname, 'PQE_1A_D.DAT')
        reader = GRPRT_Intern(filename=filename)
        df = reader.read()
        assert_frame_equal(valid, df)
        # =====================================================================

    def test_read_journalier(self):
        """
        Test de lecture d'un fichier
        """
        # =====================================================================
        valid = pnd.DataFrame(
            {
                'Qsim(mm)': [
                    5.1849, 5.5327, 25.1317, 62.9393, 47.462, 24.448, 14.5041,
                    9.0383],
                'Qobs(mm)': [
                    np.nan, 6.0837, 13.6173, 68.1413, 33.2340, 22.5610,
                    14.3262, 10.6448],
                'ETP(mm)': [
                    0.2546, 0.2728, 0.2909, 0.2819, 0.2091, 0.2, 0.2, 0.1819],
                'P(mm)': [
                    0.4000, 7.8400, 70.0800, 82.9600, 10.4800, 9.0000, 2.0400,
                    1.1200],
                'T(C)': [
                    4.4627, 6.3798, 8.2152, 8.8437, 7.8131, 3.2555, 0.411,
                    -2.3771],
                'R(mm)': [
                    23.1456, 23.8299, 45.1947, 63.1359, 57.2976, 44.7088,
                    36.1865, 29.5908],
                'S(mm)': [
                    245.4039, 245.3841, 254.9574, 261.7264, 260.5772, 259.3664,
                    257.4668, 255.3979],
                'G1(mm)': [
                    0.5394, 0.4787, 0.4256, 0.3788, 0.3376, 0.3012, 0.8419,
                    1.8532],
                'G2(mm)': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.7585, 1.8278],
                'G3(mm)': [
                    0.261, 0.2298, 0.2029, 0.1795, 0.1592, 0.1414, 1.1869,
                    2.3069],
                'G4(mm)': [
                    0.5557, 0.486, 0.4265, 0.3755, 0.3315, 0.9163, 2.6124,
                    3.7904],
                'G5(mm)': [
                    0.8873, 0.7726, 0.6757, 0.593, 0.5221, 2.352, 4.5154,
                    5.7624],
            },
            index=pnd.date_range(dt(2007, 1, 17), dt(2007, 1, 24), freq='D')
        )
        valid.index.name = 'DATE (TU)'
        # =====================================================================
        filename = os.path.join(self.dirname, 'PQE_1A_D_journalier.DAT')
        reader = GRPRT_Intern(filename=filename)
        df = reader.read()
        assert_frame_equal(valid, df)
        # =====================================================================

    def test_write(self):
        """
        Test de l'écriture d'un fichier Archive de GRP Temps Réel
        """
        filename = 'PQE_1A.DAT'
        writer = GRPRT_Intern(filename=filename)
        with self.assertRaises(NotImplementedError):
            writer.write()

    def test_datatype(self):
        """
        Test des types de données GRPRT_Intern
        """
        self.assertEqual(["intern", "intern_diff"], GRPRT_Intern.get_types())

    def test_splitbasename(self):
        """
        Test des types de données GRPRT_Intern
        """
        valid = {'PQE_1A.DAT': True, 'PQE_1A_D.DAT': False}
        for k, v in valid.items():
            diff = GRPRT_Intern.split_basename(filename=k)
            self.assertEqual(diff, v)
