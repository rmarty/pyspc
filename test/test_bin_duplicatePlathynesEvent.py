#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary duplicatePlathynesEvent

To run all tests just type:
    python -m unittest test_bin_duplicatePlathynesEvent

To run only a class test:
    python -m unittest test_bin_duplicatePlathynesEvent.Test_duplicatePlathynesEvent

To run only a specific test:
    python -m unittest test_bin_duplicatePlathynesEvent.Test_duplicatePlathynesEvent.test_project

"""
# Imports
import filecmp
import os
import shutil
import subprocess
import sys
import unittest

from pyspc.binutils.args import duplicatePlathynesEvent as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_duplicatePlathynesEvent(unittest.TestCase):
    """
    duplicatePlathynesEvent bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'duplicatePlathynesEvent')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_pla = os.path.join(cls.dirname, 'plathynes')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_event(self):
        """
        Tests PROJET PLATHYNES
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/duplicatePlathynesEvent.py',
            '-I', self.dir_pla,
            '-d', 'ARNON_MAREUIL.prj',
            '-O', self.dir_out,
            '-c', os.path.join(self.dir_in, 'events.csv'),
            '-S', 'TEST',
            '-o',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.duplicatePlathynesEvent()
        self.assertEqual(args.projet_filename, 'ARNON_MAREUIL.prj')
        self.assertEqual(args.input_dir, self.dir_pla)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.events_filename,
                         os.path.join(self.dir_in, 'events.csv'))
        self.assertEqual(args.event, 'TEST')
        self.assertEqual(args.overwrite, True)
        # ====================================================================#
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # ====================================================================
        self.assertTrue(
            filecmp.dircmp(self.dir_out, self.dir_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.dir_out, self.dir_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.dir_out, self.dir_ctl).left_only)
        self.assertFalse(
            filecmp.dircmp(self.dir_out, self.dir_ctl).right_only)
        shutil.rmtree(self.dir_out)
        # ====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Dupliquer l'événement {S} du projet PLATHYNES {d} "\
            "situé dans le répertoire {I} pour créer un autre projet "\
            "PLATHYNES dans le répertoire {O} en ignorant les événements "\
            "existant (option -o: {o}). Les événements à créer sont listés "\
            "dans le fichier {c}. "\
            "".format(
                I=args.input_dir, d=args.projet_filename, S=args.event,
                c=args.events_filename, O=args.output_dir, o=args.overwrite)
        # ====================================================================
