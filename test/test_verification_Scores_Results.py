#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Data in pyspc.verification.scores

To run all tests just type:
    python -m unittest test_verification_Scores_Data

To run only a class test:
    python -m unittest test_verification_Scores_Data.TestScores_Data

To run only a specific test:
    python -m unittest test_verification_Scores_Data.TestScores_Data.test_init

"""
# Imports
import filecmp
import os
import pandas as pnd
import unittest

# Imports pyspc
from pyspc.verification.scores import Results


class TestScores_Data(unittest.TestCase):
    """
    Scores_Data class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        # =====================================================================
        self.dirname = os.path.join('data', 'verification', 'scores')
        # =====================================================================

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'resultat_simulation.xml')
        reader = Results(filename=filename)
        self.assertEqual(reader.filename, filename)

    def test_read_sim(self):
        """
        Test de la lecture - CAS SIMULATION
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'resultat_simulation.xml')
        reader = Results(filename=filename)
        df, df_7 = reader.read()
        self.assertIsInstance(df, pnd.DataFrame)
        self.assertIsInstance(df_7, pnd.DataFrame)
        self.assertEqual(len(df.index), 13)
        self.assertEqual(len(df.columns), 31)
        self.assertEqual(len(df_7.index), 2)
        self.assertEqual(len(df_7.columns), 10)
        # =====================================================================

    def test_read_prv(self):
        """
        Test de la lecture - CAS PREVISION
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'resultat_prevision.xml')
        reader = Results(filename=filename)
        df, df_7 = reader.read()
        self.assertEqual(len(df.index), 50)
        self.assertEqual(len(df.columns), 22)
        self.assertIsNone(df_7)
        # =====================================================================

    def test_csv_sim(self):
        """
        Test de l'export CSV - CAS SIMULATION
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'resultat_simulation.xml')
        reader = Results(filename=filename)
        reader.read()
        file_error, file_table = reader.to_csv(dirname='data')
        self.assertIsNotNone(file_error)
        self.assertIsNotNone(file_table)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(file_error)),
            file_error
        ))
        os.remove(file_error)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(file_table)),
            file_table
        ))
        os.remove(file_table)
        # =====================================================================

    def test_csv_prv(self):
        """
        Test de l'export CSV - CAS PREVISION
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'resultat_prevision.xml')
        reader = Results(filename=filename)
        reader.read()
        file_error, file_table = reader.to_csv(dirname='data')
        self.assertIsNotNone(file_error)
        self.assertIsNone(file_table)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(file_error)),
            file_error
        ))
        os.remove(file_error)
        # =====================================================================

    def test_plot_sim(self):
        """
        Test de l'export PNG - CAS SIMULATION
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'resultat_simulation.xml')
        reader = Results(filename=filename)
        reader.read()
        files_error, file_table = reader.to_png(dirname='data')
        for f in files_error:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dirname, os.path.basename(f)),
                f
            ))
            os.remove(f)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(file_table)),
            file_table
        ))
        os.remove(file_table)
        # =====================================================================

    def test_plot_prv(self):
        """
        Test de l'export PNG - CAS SIMULATION
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'resultat_prevision.xml')
        reader = Results(filename=filename)
        reader.read()
        files_error, file_table = reader.to_png(dirname='data')
        for f in files_error:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.dirname, os.path.basename(f)),
                f
            ))
            os.remove(f)
        self.assertIsNone(file_table)
        # =====================================================================
