#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary plotCsvData

To run all tests just type:
    python -m unittest test_bin_plotCsvData

To run only a class test:
    python -m unittest test_bin_plotCsvData.Test_plotCsvData

To run only a specific test:
    python -m unittest test_bin_plotCsvData.Test_plotCsvData.test_hydro

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import plotCsvData as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_plotCsvData(unittest.TestCase):
    """
    plotCsvData bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'plotCsvData')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_grp16 = os.path.join('data', 'model', 'grp16', 'cal')
        cls.dir_grp18 = os.path.join('data', 'model', 'grp18', 'cal')
        cls.dir_pyspc = os.path.join('data', 'core', 'csv')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.cfg_filename = None
        self.figure = None
        self.stations_list_file = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        config=self.cfg_filename, figure=self.figure,
                        stations_list_file=self.stations_list_file)

    def test_hydro(self):
        """
        Test de la fonction graphique 'hydrogramme'
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/plotCsvData.py',
            '-c', os.path.join(self.dir_cfg, 'hydro.txt'),
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.plotCsvData()
        self.assertEqual(args.cfg_filename,
                         os.path.join(self.dir_cfg, 'hydro.txt'))
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.plottype, None)
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.input_dir, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.output_dir, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'hydro.png'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cfg_filename = args.cfg_filename
        self.figure = filename
        self.cline = cline
        self.label = "Création de la figure selon le fichier de "\
            "condiguration {c}."\
            "".format(c=args.cfg_filename)
        # =====================================================================

    def test_hydro_nocfg(self):
        """
        Test de la fonction graphique 'hydrogramme' - Cas sans config
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'chadrac_bas.txt')
        processArgs = [
            'python',
            '../bin/plotCsvData.py',
            '-I', os.path.join(self.dir_in, 'hydro'),
            '-l', self.stations_list_file,
            '-n', 'QH',
            '-F', '2008103100',
            '-L', '2008110323',
            '-O', self.dir_out,
            '-C', 'pyspc',
            '-t', 'hydro'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.plotCsvData()
        self.assertEqual(args.cfg_filename, None)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, 'QH')
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.plottype, 'hydro')
        self.assertEqual(args.first_dtime, '2008103100')
        self.assertEqual(args.input_dir, os.path.join(self.dir_in, 'hydro'))
        self.assertEqual(args.last_dtime, '2008110323')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'chadrac_bas_2008103100-2008110323.png'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.figure = filename
        self.label = "Création de la figure de type {t} à partir des "\
            "données au format {C} des séries listées dans {l} pour la "\
            "grandeur {n} sur la période du {F} au {L}. Les données sont "\
            "lues depuis le répertoire {I} et la figure est créée dans le "\
            "répertoire {O}."\
            "".format(
                I=args.input_dir, l=args.stations_list_file, n=args.varname,
                C=args.csv_type,
                t=args.plottype, O=args.output_dir,
                F=args.first_dtime, L=args.last_dtime
            )
        # =====================================================================

    def test_hyeto(self):
        """
        Test de la fonction graphique 'hyétogramme'
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/plotCsvData.py',
            '-c', os.path.join(self.dir_cfg, 'hyeto.txt')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.plotCsvData()
        self.assertEqual(args.cfg_filename,
                         os.path.join(self.dir_cfg, 'hyeto.txt'))
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.plottype, None)
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.input_dir, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.output_dir, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'hyeto.png'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cfg_filename = args.cfg_filename
        self.figure = filename
        self.cline = cline
        self.label = "Création de la figure selon le fichier de "\
            "condiguration {c}."\
            "".format(c=args.cfg_filename)
        # =====================================================================

    def test_hydrolimni(self):
        """
        Test de la fonction graphique 'hydrogramme / limnigramme'
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/plotCsvData.py',
            '-c', os.path.join(self.dir_cfg, 'hydrolimni.txt')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.plotCsvData()
        self.assertEqual(args.cfg_filename,
                         os.path.join(self.dir_cfg, 'hydrolimni.txt'))
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.plottype, None)
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.input_dir, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.output_dir, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'hydrolimni.png'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cfg_filename = args.cfg_filename
        self.figure = filename
        self.cline = cline
        self.label = "Création de la figure selon le fichier de "\
            "condiguration {c}."\
            "".format(c=args.cfg_filename)
        # =====================================================================

    def test_event(self):
        """
        Test de la fonction graphique 'événement'
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/plotCsvData.py',
            '-c', os.path.join(self.dir_cfg, 'event.txt')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.plotCsvData()
        self.assertEqual(args.cfg_filename,
                         os.path.join(self.dir_cfg, 'event.txt'))
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.plottype, None)
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.input_dir, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.output_dir, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'event.png'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cfg_filename = args.cfg_filename
        self.figure = filename
        self.cline = cline
        self.label = "Création de la figure selon le fichier de "\
            "condiguration {c}."\
            "".format(c=args.cfg_filename)
        # =====================================================================

    def test_reservoir_H(self):
        """
        Test de la fonction graphique 'reservoir_H'
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/plotCsvData.py',
            '-c', os.path.join(self.dir_cfg, 'reservoir_H.txt')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.plotCsvData()
        self.assertEqual(args.cfg_filename,
                         os.path.join(self.dir_cfg, 'reservoir_H.txt'))
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.plottype, None)
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.input_dir, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.output_dir, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'reservoir_H.png'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cfg_filename = args.cfg_filename
        self.figure = filename
        self.cline = cline
        self.label = "Création de la figure selon le fichier de "\
            "condiguration {c}."\
            "".format(c=args.cfg_filename)
        # =====================================================================

    def test_reservoir_Z(self):
        """
        Test de la fonction graphique 'reservoir_Z'
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/plotCsvData.py',
            '-c', os.path.join(self.dir_cfg, 'reservoir_Z.txt')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.plotCsvData()
        self.assertEqual(args.cfg_filename,
                         os.path.join(self.dir_cfg, 'reservoir_Z.txt'))
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.plottype, None)
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.input_dir, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.output_dir, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'reservoir_Z.png'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cfg_filename = args.cfg_filename
        self.figure = filename
        self.cline = cline
        self.label = "Création de la figure selon le fichier de "\
            "condiguration {c}."\
            "".format(c=args.cfg_filename)
        # =====================================================================

    def test_subreach(self):
        """
        Test de la fonction graphique 'subreach'
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/plotCsvData.py',
            '-c', os.path.join(self.dir_cfg, 'subreach.txt')
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.plotCsvData()
        self.assertEqual(args.cfg_filename,
                         os.path.join(self.dir_cfg, 'subreach.txt'))
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, None)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.plottype, None)
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.input_dir, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.output_dir, None)
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'subreach_aron.png'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cfg_filename = args.cfg_filename
        self.figure = filename
        self.cline = cline
        self.label = "Création de la figure selon le fichier de "\
            "condiguration {c}."\
            "".format(c=args.cfg_filename)
        # =====================================================================
