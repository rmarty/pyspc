#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Locations in pyspc.core.location

To run all tests just type:
    python -m unittest test_core_Locations

To run only a class test:
    python -m unittest test_core_Locations.TestLocations

To run only a specific test:
    python -m unittest test_core_Locations.TestLocations.test_init

"""
# Imports
import os
import unittest

from pyspc.core.config import Config
from pyspc.core.location import Location, Locations


class TestLocations(unittest.TestCase):
    """
    Locations class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'core', 'location')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        name = 'toto'
        locs = Locations(name=name)
        self.assertEqual(locs.name, name)
        self.assertEqual(locs.codes, [])

    def test_add(self):
        """
        Test de la méthode add
        """
        # =====================================================================
        valid = {
            'code': 'K0550010',
            'name': 'La Loire à Bas-en-Basset',
            'loctype': 'basin',
            'longname': None,
            'reach': None,
            'locality': None,
            'x': 787596.0,
            'y': 6466797.0,
            'z': 442.89,
            'area': 3234.0
        }
        loc = Location(**valid)
        code = valid['code']
        # =====================================================================
        locs = Locations()
        with self.assertRaises(ValueError):
            locs.add(code=1, loc=loc)
        with self.assertRaises(ValueError):
            locs.add(code=code, loc=1)
        with self.assertRaises(ValueError):
            locs.add(code=code, loc='toto')
        locs.add(code=code, loc=loc)
        self.assertEqual(list(locs.keys()), [code])
        self.assertEqual(locs.codes, [code])
        # =====================================================================
        locs = Locations()
        locs.add(loc=loc)
        self.assertEqual(list(locs.keys()), [code])
        self.assertEqual(locs.codes, [code])
        # =====================================================================

    def test_check_locs(self):
        """
        Test du contrôle du contenu de la collection
        """
        # =====================================================================
        valid = {
            'code': 'K0550010',
            'name': 'La Loire à Bas-en-Basset',
            'loctype': 'basin',
            'longname': None,
            'reach': None,
            'locality': None,
            'x': 787596.0,
            'y': 6466797.0,
            'z': 442.89,
            'area': 3234.0
        }
        loc = Location(**valid)
        code = valid['code']
        locs = Locations()
        locs.add(code=code, loc=loc)
        self.assertTrue(locs.check_locs())

    def test_Config(self):
        """
        Test création à partir d'un fichier de configuration
        """
        # =====================================================================
        valid = {
           'K0550010': {
                'code': 'K0550010',
                'name': 'La Loire à Bas-en-Basset',
                'loctype': 'basin',
                'x': 787596.0,
                'y': 6466797.0,
                'z': 442.89,
                'area': 3234.0
            }
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'locations.txt')
        locs = Locations.from_Config(config=filename)
        self.assertEqual(locs.name, 'locations')
        for k, v in valid.items():
            self.assertIn(k, locs)
            self.assertEqual(locs[k].code, v['code'])
            self.assertEqual(locs[k].name, v['name'])
        # =====================================================================
        cfg = locs.to_Config()
        self.assertIsInstance(cfg, Config)
        for k, v in valid.items():
            self.assertIn(k, cfg)
            for k2, v2 in v.items():
                self.assertEqual(cfg[k][k2], v2)
        # =====================================================================
