#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for BdImage in pyspc.data.lamedo

To run all tests just type:
    python -m unittest test_data_BdImage

To run only a class test:
    python -m unittest test_data_BdImage.TestBdImage

To run only a specific test:
    python -m unittest test_data_BdImage.TestBdImage.test_init

"""
# Imports
import os
import unittest

from pyspc.data.lamedo import BdImage
from libbdimage.bdixml import r2016 as _xml


class TestBdImage(unittest.TestCase):
    """
    BdImage class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        # =====================================================================
        self.dirname = os.path.join('data', 'data', 'lamedo')
        # =====================================================================

    def test_init(self):
        """
        Test initialisation de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname,
                                'getObsStatsByZones_antilope-j1-rr_000100.xml')
        bdimage = BdImage(filename=filename)
        self.assertEqual(bdimage.filename, filename)
        # =====================================================================

    def test_read(self):
        """
        Test de lecture
        """
        # =====================================================================
        filename = os.path.join(self.dirname,
                                'getObsStatsByZones_antilope-j1-rr_000100.xml')
        bdimage = BdImage(filename=filename)
        content = bdimage.read()
        self.assertIsInstance(content, _xml.Message)
        # =====================================================================
