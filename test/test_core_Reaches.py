#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Reaches in pyspc.core.reach

To run all tests just type:
    python -m unittest test_core_Reaches

To run only a class test:
    python -m unittest test_core_Reaches.TestReaches

To run only a specific test:
    python -m unittest test_core_Reaches.TestReaches.test_init

"""
# Imports
import os
import unittest

# Imports pyspc
from pyspc.core.config import Config
from pyspc.core.reach import Reach, Reaches


class TestReaches(unittest.TestCase):
    """
    Reaches class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'core', 'reach')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        name = 'toto'
        reaches = Reaches(name=name)
        self.assertEqual(reaches.name, name)
        self.assertEqual(reaches.codes, [])

    def test_add(self):
        """
        Test de la méthode add
        """
        # =====================================================================
        valid = {
            'code': 'LC110',
            'name': 'Haut-bassin de la Loire'
        }
        reach = Reach(**valid)
        code = valid['code']
        # =====================================================================
        reaches = Reaches()
        with self.assertRaises(ValueError):
            reaches.add(code=1, reach=reach)
        with self.assertRaises(ValueError):
            reaches.add(code=code, reach=1)
        with self.assertRaises(ValueError):
            reaches.add(code=code, reach='toto')
        reaches.add(code=code, reach=reach)
        self.assertEqual(list(reaches.keys()), [code])
        self.assertEqual(reaches.codes, [code])
        # =====================================================================

    def test_check_reaches(self):
        """
        Test du contrôle du contenu de la collection
        """
        # =====================================================================
        valid = {
            'code': 'LC110',
            'name': 'Haut-bassin de la Loire'
        }
        code = valid['code']
        # =====================================================================
        reach = Reach(**valid)
        reaches = Reaches()
        reaches.add(code=code, reach=reach)
        self.assertTrue(reaches.check_reaches())
        # =====================================================================

    def test_Config(self):
        """
        Test création à partir d'un fichier de configuration
        """
        # =====================================================================
        valid = {
           'LC110': {
            'code': 'LC110',
            'name': 'Haut-bassin de la Loire'
            }
        }
        filename = os.path.join(self.dirname, 'reaches.txt')
        # =====================================================================
        reaches = Reaches.from_Config(config=filename)
        self.assertEqual(reaches.name, 'reaches')
        for k, v in valid.items():
            self.assertIn(k, reaches)
            self.assertEqual(reaches[k].code, v['code'])
            self.assertEqual(reaches[k].name, v['name'])
        # =====================================================================
        cfg = reaches.to_Config()
        self.assertIsInstance(cfg, Config)
        for k, v in valid.items():
            self.assertIn(k, cfg)
            for k2, v2 in v.items():
                self.assertEqual(cfg[k][k2], v2)
        # =====================================================================

#    def test_RefSPC(self):
#        """
#        Test création à partir du référentiel SPC LCI
#        """
#        # =====================================================================
#        filename = os.path.join('data', 'dbase', 'BDD_light.sqlite')
#        codes = ['LC105', 'LC134', 'LC235']
#        # =====================================================================
#        valid = {
#            'LC105': {
#                'code': 'LC105',
#                'name': 'Loire vellave'
#            },
#            'LC134': {
#                'code': 'LC134',
#                'name': 'Arroux'
#            },
#            'LC235': {
#                'code': 'LC235',
#                'name': 'Arnon amont'
#            },
#        }
#        # =====================================================================
#        reaches = Reaches.from_RefSPC(
#            filename=filename,
#            codes=codes
#        )
#        self.assertEqual(reaches.name, 'RefSPC')
#        for k, v in valid.items():
#            self.assertIn(k, reaches)
#            self.assertEqual(reaches[k].code, v['code'])
#            self.assertEqual(reaches[k].name, v['name'])
#        # =====================================================================
#
#    def test_Vigicrues(self):
#        """
#        Test du json Vigicrues - Tronçon de vigilance
#        """
#        filename = os.path.join('data', 'schapi', 'vigicrues', 'troncons.json')
#        codes = ['LC110']
#        reaches = Reaches.from_Vigicrues(
#            filename=filename,
#            codes=codes
#        )
#        self.assertEqual(reaches.codes, codes)
