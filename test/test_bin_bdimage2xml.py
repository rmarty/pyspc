#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary bdimage2xml

To run all tests just type:
    python -m unittest test_bin_bdimage2xml

To run only a class test:
    python -m unittest test_bin_bdimage2xml.Test_bdimage2xml

To run only a specific test:
    python -m unittest test_bin_bdimage2xml.Test_bdimage2xml.test_P5M_radar

"""
# Imports
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import bdimage2xml as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_bdimage2xml(unittest.TestCase):
    """
    bdimage2xml bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'bdimage2xml')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.file_cfg = os.path.join('..', 'bin', 'bdimage2xml_RM.txt')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_P5m_radar_bbox(self):
        """
        Test PANTHERE - P5M - VALUES - BBOX
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimage2xml.py',
            '-O', self.dir_out,
            '-S', 'Bx772000,6422000,773000,6421000',
            '-F', '201706131730',
            '-L', '201706131930',
            '-t', 'panthere', 'france', 'rr',
            '-n', 'P5m'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.bdimage2xml()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.spatial_domain,
                         'Bx772000,6422000,773000,6421000')
        self.assertEqual(args.data_type, ['panthere', 'france', 'rr'])
        self.assertEqual(args.first_dtime, '201706131730')
        self.assertEqual(args.last_dtime, '201706131930')
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.varname, 'P5m')
        self.assertEqual(args.update_element, None)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = os.path.join(
            self.dir_out,
            'Bx772000,6422000,773000,6421000_panthere-france-rr_'
            '201706131730-201706131930_000005.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraction des valeurs de précipitations de l'image "\
            "radar '{t}' entre les instants {F} et {L} sur le domaine {S} "\
            "en ciblant la grandeur {n}, dans un fichier placé dans le "\
            "répertoire local {O}".format(
                t=' '.join(args.data_type),
                F=args.first_dtime, L=args.last_dtime,
                n=args.varname, S=args.spatial_domain, O=args.output_dir)
        # =====================================================================

    def test_P15m_aromepi_stats(self):
        """
        Test AROME-PI - P15m - STATS stats - ZONES LO808
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimage2xml.py',
            '-O', self.dir_out,
            '-S', 'LO808',
            '-F', '202308310915',
            '-L', '202308311015',
            '-r', '202308310900',
            '-t', 'arome-pi', 'rr', 'total',
            '-n', 'P15m',
            '-U', 'stats', 'standard'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.bdimage2xml()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.spatial_domain, 'LO808')
        self.assertEqual(args.data_type, ['arome-pi', 'rr', 'total'])
        self.assertEqual(args.first_dtime, '202308310915')
        self.assertEqual(args.last_dtime, '202308311015')
        self.assertEqual(args.runtime, '202308310900')
        self.assertEqual(args.varname, 'P15m')
        self.assertEqual(args.update_element, [['stats', 'standard']])
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = os.path.join(
            self.dir_out,
            'LO808_arome-pi-rr-total_'
            '202308310900-202308310915-202308311015_000015.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraction des lames d'eau de l'image "\
            "entre les instants {F} et {L} sur le domaine {S} "\
            "issue de la prévision immédiate '{t}' émise à {r} "\
            "en ciblant la grandeur {n}, dans un fichier placé dans le "\
            "répertoire local {O}. Les lames extraites correspondent "\
            "aux '{U}'.".format(
                t=' '.join(args.data_type),
                F=args.first_dtime.strip('0'), L=args.last_dtime.strip('0'),
                r=args.runtime.strip('0'),
                n=args.varname, S=args.spatial_domain, O=args.output_dir,
                U=' '.join(args.update_element[0]))
        # =====================================================================

    def test_PH_radar_stats(self):
        """
        Test ANTILOPE - PH - STATS stats - ZONES
        LO8087+LO12844
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimage2xml.py',
            '-O', self.dir_out,
            '-S', 'LO8087+LO12844',
            '-F', '2008103106',
            '-L', '2008110206',
            '-t', 'antilope', 'temps-reel', 'rr',
            '-n', 'PH',
            '-U', 'stats', 'complete'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.bdimage2xml()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.spatial_domain, 'LO8087+LO12844')
        self.assertEqual(args.data_type, ['antilope', 'temps-reel', 'rr'])
        self.assertEqual(args.first_dtime, '200810310600')
        self.assertEqual(args.last_dtime, '200811020600')
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.varname, 'PH')
        self.assertEqual(args.update_element, [['stats', 'complete']])
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = os.path.join(
            self.dir_out,
            'LO8087+LO12844_antilope-temps-reel-rr_'
            '200810310600-200811020600_000100.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraction des lames d'eau de l'image "\
            "radar '{t}' entre les instants {F} et {L} sur le domaine {S} "\
            "en ciblant la grandeur {n}, dans un fichier placé dans le "\
            "répertoire local {O}. Les lames extraites correspondent "\
            "aux '{U}'.".format(
                t=' '.join(args.data_type),
                F=args.first_dtime.strip('0'), L=args.last_dtime.strip('0'),
                n=args.varname, S=args.spatial_domain, O=args.output_dir,
                U=' '.join(args.update_element[0]))
        # =====================================================================

    def test_EH_sim_pixel(self):
        """
        Test SIM - EH - Values - PIXELS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimage2xml.py',
            '-O', self.dir_out,
            '-S', '656368,6565385',
            '-F', '20180101',
            '-L', '20180102',
            '-t', 'sim', 'evap', 'rr',
            '-n', 'EH'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.bdimage2xml()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.spatial_domain,
                         '656368,6565385')
        self.assertEqual(args.data_type, ['sim', 'evap', 'rr'])
        self.assertEqual(args.first_dtime, '201801010000')
        self.assertEqual(args.last_dtime, '201801020000')
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.varname, 'EH')
        self.assertEqual(args.update_element, None)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = os.path.join(
            self.dir_out,
            '656368.0,6565385.0_sim-evap-rr_'
            '201801010000-201801020000_000100.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraction des valeurs d'évaporation de l'image "\
            "radar '{t}' entre les instants {F} et {L} sur le domaine {S} "\
            "en ciblant la grandeur {n}, dans un fichier placé dans le "\
            "répertoire local {O}".format(
                t=' '.join(args.data_type),
                F=args.first_dtime.strip('0'), L=args.last_dtime.strip('0'),
                n=args.varname, S=args.spatial_domain, O=args.output_dir)
        # =====================================================================

    def test_TH_sim_zone(self):
        """
        Test SIM - TH - Values - PIXELS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimage2xml.py',
            '-O', self.dir_out,
            '-S', 'LO6498',
            '-F', '20180101',
            '-L', '20180102',
            '-t', 'sim', 't', 't',
            '-n', 'TI'
        ]
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.bdimage2xml()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.spatial_domain, 'LO6498')
        self.assertEqual(args.data_type, ['sim', 't', 't'])
        self.assertEqual(args.first_dtime, '201801010000')
        self.assertEqual(args.last_dtime, '201801020000')
        self.assertEqual(args.runtime, None)
        self.assertEqual(args.varname, 'TI')
        self.assertEqual(args.update_element, None)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = os.path.join(
            self.dir_out,
            'LO6498_sim-t-t_201801010000-201801020000_000000.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraction des valeurs de températures de l'image "\
            "radar '{t}' entre les instants {F} et {L} sur le domaine {S} "\
            "en ciblant la grandeur {n}, dans un fichier placé dans le "\
            "répertoire local {O}".format(
                t=' '.join(args.data_type),
                F=args.first_dtime.strip('0'), L=args.last_dtime.strip('0'),
                n=args.varname, S=args.spatial_domain, O=args.output_dir)
        # =====================================================================

    def test_P3H_sympo(self):
        """
        Test SYMPO - P3H - STATS - ZONE
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimage2xml.py',
            '-O', self.dir_out,
            '-S', 'LO8087',
            '-F', '2018110506',
            '-r', '2018110506',
            '-L', '2018110806',
            '-t', 'sympo', 'rr', 'rr',
            '-n', 'P3H',
            '-U', 'stats', 'standard',
            '-U', 'precision', 'standard'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.bdimage2xml()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.spatial_domain, 'LO8087')
        self.assertEqual(args.data_type, ['sympo', 'rr', 'rr'])
        self.assertEqual(args.first_dtime, '201811050600')
        self.assertEqual(args.last_dtime, '201811080600')
        self.assertEqual(args.runtime, '201811050600')
        self.assertEqual(args.varname, 'P3H')
        self.assertEqual(args.update_element, [['stats', 'standard'],
                                               ['precision', 'standard']])
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = os.path.join(
            self.dir_out,
            'LO8087_sympo-rr-rr_'
            '201811050600-201811050600-201811080600_000300.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraction des statistiques de prévision de "\
            "précipitations émises le {r} de l'image "\
            "prévue '{t}' entre les instants {F} et {L} sur le domaine {S} "\
            "en ciblant la grandeur {n}, dans un fichier placé dans le "\
            "répertoire local {O}".format(
                t=' '.join(args.data_type),
                r=args.runtime.strip('0'),
                F=args.first_dtime.strip('0'), L=args.last_dtime.strip('0'),
                n=args.varname, S=args.spatial_domain, O=args.output_dir)
        # =====================================================================

    def test_PH_arome(self):
        """
        Test AROME - PH - STATS - ZONE
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimage2xml.py',
            '-O', self.dir_out,
            '-S', 'LO8087',
            '-F', '2022033107',
            '-r', '2022033106',
            '-L', '2022040200',
            '-t', 'arome', 'rr', 'total',
            '-n', 'PH',
            '-U', 'stats', 'standard',
            '-U', 'precision', 'standard'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.bdimage2xml()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.spatial_domain, 'LO8087')
        self.assertEqual(args.data_type, ['arome', 'rr', 'total'])
        self.assertEqual(args.first_dtime, '202203310700')
        self.assertEqual(args.last_dtime, '202204020000')
        self.assertEqual(args.runtime, '202203310600')
        self.assertEqual(args.varname, 'PH')
        self.assertEqual(args.update_element, [['stats', 'standard'],
                                               ['precision', 'standard']])
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = os.path.join(
            self.dir_out,
            'LO8087_arome-rr-total_'
            '202203310600-202203310700-202204020000_000100.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraction des statistiques de prévision de "\
            "précipitations émises le {r} de l'image "\
            "prévue '{t}' entre les instants {F} et {L} sur le domaine {S} "\
            "en ciblant la grandeur {n}, dans un fichier placé dans le "\
            "répertoire local {O}".format(
                t=' '.join(args.data_type),
                r=args.runtime.strip('0'),
                F=args.first_dtime.strip('0'), L=args.last_dtime.strip('0'),
                n=args.varname, S=args.spatial_domain, O=args.output_dir)
        # =====================================================================

    def test_PH_aromeifs(self):
        """
        Test AROME-IFS - PH - STATS - ZONE
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimage2xml.py',
            '-O', self.dir_out,
            '-S', 'LO8087',
            '-F', '2022033113',
            '-r', '2022033112',
            '-L', '2022040212',
            '-t', 'arome-ifs', 'rr', 'total',
            '-n', 'PH',
            '-U', 'stats', 'standard',
            '-U', 'precision', 'standard'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.bdimage2xml()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.spatial_domain, 'LO8087')
        self.assertEqual(args.data_type, ['arome-ifs', 'rr', 'total'])
        self.assertEqual(args.first_dtime, '202203311300')
        self.assertEqual(args.last_dtime, '202204021200')
        self.assertEqual(args.runtime, '202203311200')
        self.assertEqual(args.varname, 'PH')
        self.assertEqual(args.update_element, [['stats', 'standard'],
                                               ['precision', 'standard']])
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = os.path.join(
            self.dir_out,
            'LO8087_arome-ifs-rr-total_'
            '202203311200-202203311300-202204021200_000100.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraction des statistiques de prévision de "\
            "précipitations émises le {r} de l'image "\
            "prévue '{t}' entre les instants {F} et {L} sur le domaine {S} "\
            "en ciblant la grandeur {n}, dans un fichier placé dans le "\
            "répertoire local {O}".format(
                t=' '.join(args.data_type),
                r=args.runtime.strip('0'),
                F=args.first_dtime.strip('0'), L=args.last_dtime.strip('0'),
                n=args.varname, S=args.spatial_domain, O=args.output_dir)
        # =====================================================================

    def test_TI_sympo(self):
        """
        Test SYMPO - TI - STATS - ZONE
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimage2xml.py',
            '-O', self.dir_out,
            '-S', 'LO8087',
            '-F', '2018110506',
            '-r', '2018110506',
            '-L', '2018110806',
            '-t', 'sympo', 't', 't',
            '-n', 'TI',
            '-U', 'stats', 'standard',
            '-U', 'precision', 'standard'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.bdimage2xml()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.spatial_domain, 'LO8087')
        self.assertEqual(args.data_type, ['sympo', 't', 't'])
        self.assertEqual(args.first_dtime, '201811050600')
        self.assertEqual(args.last_dtime, '201811080600')
        self.assertEqual(args.runtime, '201811050600')
        self.assertEqual(args.varname, 'TI')
        self.assertEqual(args.update_element, [['stats', 'standard'],
                                               ['precision', 'standard']])
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = os.path.join(
            self.dir_out,
            'LO8087_sympo-t-t_'
            '201811050600-201811050600-201811080600_000000.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraction des statistiques de prévision de "\
            "températures émises le {r} de l'image "\
            "prévue '{t}' entre les instants {F} et {L} sur le domaine {S} "\
            "en ciblant la grandeur {n}, dans un fichier placé dans le "\
            "répertoire local {O}".format(
                t=' '.join(args.data_type),
                r=args.runtime.strip('0'),
                F=args.first_dtime.strip('0'), L=args.last_dtime.strip('0'),
                n=args.varname, S=args.spatial_domain, O=args.output_dir)
        # =====================================================================

    def test_TI_sympo_pix(self):
        """
        Test SYMPO - TI - VALUES - PIXELS
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimage2xml.py',
            '-O', self.dir_out,
            '-S', '784507,6402274+794057,6413270',
            '-F', '2018110506',
            '-r', '2018110506',
            '-L', '2018110806',
            '-t', 'sympo', 't', 't',
            '-n', 'TI'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.bdimage2xml()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.spatial_domain, '784507,6402274+794057,6413270')
        self.assertEqual(args.data_type, ['sympo', 't', 't'])
        self.assertEqual(args.first_dtime, '201811050600')
        self.assertEqual(args.last_dtime, '201811080600')
        self.assertEqual(args.runtime, '201811050600')
        self.assertEqual(args.varname, 'TI')
        self.assertEqual(args.update_element, None)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = os.path.join(
            self.dir_out,
            '784507.0,6402274.0+794057.0,6413270.0_sympo-t-t_'
            '201811050600-201811050600-201811080600_000000.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraction des statistiques de prévision de "\
            "températures émises le {r} de l'image "\
            "prévue '{t}' entre les instants {F} et {L} sur le domaine {S} "\
            "en ciblant la grandeur {n}, dans un fichier placé dans le "\
            "répertoire local {O}".format(
                t=' '.join(args.data_type),
                r=args.runtime.strip('0'),
                F=args.first_dtime.strip('0'), L=args.last_dtime.strip('0'),
                n=args.varname, S=args.spatial_domain, O=args.output_dir)
        # =====================================================================

    def test_TI_arpege(self):
        """
        Test ARPEGE - TI - STATS - ZONE
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/bdimage2xml.py',
            '-O', self.dir_out,
            '-S', 'LO8087',
            '-F', '2022033106',
            '-r', '2022033106',
            '-L', '2022040306',
            '-t', 'arpege', 't', 't',
            '-n', 'TI',
            '-U', 'stats', 'standard',
            '-U', 'precision', 'standard'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.bdimage2xml()
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.spatial_domain, 'LO8087')
        self.assertEqual(args.data_type, ['arpege', 't', 't'])
        self.assertEqual(args.first_dtime, '202203310600')
        self.assertEqual(args.last_dtime, '202204030600')
        self.assertEqual(args.runtime, '202203310600')
        self.assertEqual(args.varname, 'TI')
        self.assertEqual(args.update_element, [['stats', 'standard'],
                                               ['precision', 'standard']])
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = os.path.join(
            self.dir_out,
            'LO8087_arpege-t-t_'
            '202203310600-202203310600-202204030600_000000.xml')
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Extraction des statistiques de prévision de "\
            "températures émises le {r} de l'image "\
            "prévue '{t}' entre les instants {F} et {L} sur le domaine {S} "\
            "en ciblant la grandeur {n}, dans un fichier placé dans le "\
            "répertoire local {O}".format(
                t=' '.join(args.data_type),
                r=args.runtime.strip('0'),
                F=args.first_dtime.strip('0'), L=args.last_dtime.strip('0'),
                n=args.varname, S=args.spatial_domain, O=args.output_dir)
        # =====================================================================
