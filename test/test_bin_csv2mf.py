#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary csv2mf

To run all tests just type:
    python -m unittest test_bin_csv2mf

To run only a class test:
    python -m unittest test_bin_csv2mf.Test_csv2mf

To run only a specific test:
    python -m unittest test_bin_csv2mf.Test_csv2mf.test_data

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import csv2mf as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_csv2mf(unittest.TestCase):
    """
    csv2mf bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'csv2mf')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_grp16 = os.path.join('data', 'model', 'grp16', 'cal')
        cls.dir_grp18 = os.path.join('data', 'model', 'grp18', 'cal')
        cls.dir_pyspc = os.path.join('data', 'core', 'csv')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.stations_list_file = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)

    def test_data_grp16_PH(self):
        """
        Test conversion Publithèque
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2mf.py',
            '-I', self.dir_grp16,
            '-O', self.dir_out,
            '-n', 'PH',
            '-C', 'grp16',
            '-t', 'data',
            '-s', '43091005'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2mf()
        self.assertEqual(args.input_dir, self.dir_grp16)
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'PH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, '43091005')
        self.assertEqual(args.data_type, 'data')
        self.assertEqual(args.csv_type, 'grp16')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.data'.format(args.station_name, args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion au format Publithèque ('{t}') des données "\
            "de précipitations horaires ('{n}') à la station {s} "\
            "depuis le fichier csv de type GRP v2016 ('{C}') dans le "\
            "répertoire {I} vers le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_data_grp16_THbwd(self):
        """
        Test conversion Publithèque
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2mf.py',
            '-I', self.dir_grp16,
            '-O', self.dir_out,
            '-n', 'TH',
            '-C', 'grp16',
            '-t', 'data',
            '-s', '43091005',
            '-F', '2017061314',
            '-L', '2017061318',
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2mf()
        self.assertEqual(args.input_dir, self.dir_grp16)
        self.assertEqual(args.first_dtime, '2017061314')
        self.assertEqual(args.last_dtime, '2017061318')
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'TH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, '43091005')
        self.assertEqual(args.data_type, 'data')
        self.assertEqual(args.csv_type, 'grp16')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.data'.format(args.station_name, args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion au format Publithèque ('{t}') des données "\
            "de températures horaires ('{n}') à la station {s} "\
            "depuis le fichier csv de type GRP v2016 ('{C}') dans le "\
            "répertoire {I} vers le répertoire {O}. "\
            "Les données sont restreintes à la période {F} à {L}."\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                F=args.first_dtime, L=args.last_dtime,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_data_grp18_PHlist(self):
        """
        Test conversion Publithèque
        """
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_in, 'list_grp18.txt')
        processArgs = [
            'python',
            '../bin/csv2mf.py',
            '-I', self.dir_grp18,
            '-O', self.dir_out,
            '-n', 'PH',
            '-C', 'grp18',
            '-t', 'data',
            '-l', self.stations_list_file,
            '-w'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2mf()
        self.assertEqual(args.input_dir, self.dir_grp18)
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.varname, 'PH')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.data_type, 'data')
        self.assertEqual(args.csv_type, 'grp18')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = 'list_grp18_{}.data'.format(args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion au format Publithèque ('{t}') des données "\
            "de précipitations horaires ('{n}') aux stations listées dans "\
            "{l} depuis le fichier csv de type GRP v2018 ('{C}') dans le "\
            "répertoire {I} vers le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir, C=args.csv_type,
                l=args.stations_list_file, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================

    def test_data_pyspc(self):
        """
        Test conversion Publithèque
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/csv2mf.py',
            '-I', self.dir_pyspc,
            '-O', self.dir_out,
            '-n', 'P6m',
            '-C', 'pyspc',
            '-t', 'data',
            '-s', '43111002',
            '-w'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.csv2mf()
        self.assertEqual(args.input_dir, self.dir_pyspc)
        self.assertEqual(args.first_dtime, None)
        self.assertEqual(args.last_dtime, None)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.varname, 'P6m')
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.station_name, '43111002')
        self.assertEqual(args.data_type, 'data')
        self.assertEqual(args.csv_type, 'pyspc')
#        self.assertEqual(args.verbose, True)
#        self.assertEqual(args.warning, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        filename = '{}_{}.data'.format(args.station_name, args.varname)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Conversion au format Publithèque ('{t}') des données "\
            "de précipitations 6-min ('{n}') de la station {s} "\
            "depuis le fichier csv de type '{C}' dans le "\
            "répertoire {I} vers le répertoire {O}"\
            ""\
            "".format(
                I=args.input_dir, C=args.csv_type,
                s=args.station_name, n=args.varname,
                O=args.output_dir, t=args.data_type)
        # =====================================================================
