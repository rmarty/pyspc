#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary comparePeakFlow

To run all tests just type:
    python -m unittest test_bin_comparePeakFlow

To run only a class test:
    python -m unittest test_bin_comparePeakFlow.Test_comparePeakFlow

To run only a specific test:
    python -m unittest test_bin_comparePeakFlow.Test_comparePeakFlow.test_long

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import comparePeakFlow as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_comparePeakFlow(unittest.TestCase):
    """
    comparePeakFlow bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'comparePeakFlow')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_besbre(self):
        """
        Test comparaison Besbre
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/comparePeakFlow.py',
            '-I', self.dir_in,
            '-n', 'QH',
            '-c', 'K1524010.csv',
            '-d', 'K1533010.csv',
            '-O', self.dir_out
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.comparePeakFlow()
        self.assertEqual(args.upfile, 'K1524010.csv')
        self.assertEqual(args.downfile, 'K1533010.csv')
        self.assertEqual(args.input_dir, self.dir_in)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.varname, 'QH')
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        basename = 'peakflow_K1524010-K1533010.csv'
        self.assertTrue(filecmp.cmp(
            os.path.join(args.output_dir, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(args.output_dir, basename))
        basename = 'peakflow_K1524010-K1533010.png'
        self.assertTrue(filecmp.cmp(
            os.path.join(args.output_dir, basename),
            os.path.join(self.dir_ctl, basename),
        ))
        os.remove(os.path.join(args.output_dir, basename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Comparaison des pointes de crues, de la grandeur {n}, "\
            "amont lues dans le fichier {c} et aval dans le fichier {d}, "\
            "tous deux placés dans le répertoire {I}. "\
            "Le fichier csv associant les pointes de crues "\
            "et le fichier png de l'ajustement sont écrits dans {O}"\
            "".format(
                I=args.input_dir, c=args.upfile, d=args.downfile,
                n=args.varname, O=args.output_dir)
        # =====================================================================
