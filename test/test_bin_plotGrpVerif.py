#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary plotGrpVerif

To run all tests just type:
    python -m unittest test_bin_plotGrpVerif

To run only a class test:
    python -m unittest test_bin_plotGrpVerif.Test_plotGrpVerif

To run only a specific test:
    python -m unittest test_bin_plotGrpVerif.Test_plotGrpVerif.test_plot

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import plotGrpVerif as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_plotGrpVerif(unittest.TestCase):
    """
    _plotGrpVerif class test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'plotGrpVerif')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_2016 = os.path.join('data', 'model', 'grp16', 'cal')
        cls.dir_2018 = os.path.join('data', 'model', 'grp18', 'cal')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.figure = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        figure=self.figure)

    def test_plot(self):
        """
        Test création figure
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/plotGrpVerif.py',
            '-I', self.dir_2016,
            '-d', 'synthese.txt',
            '-O', self.dir_out,
            '-U', 'SC', 'ref', 'DC50', 'DC90', 'DC95',
            '-U', 'HC', 'ref',
            '-U', 'SV', 'ref', 'DC95',
            '-U', 'MODE', 'SMN_TAN', 'SMN_RNA'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.plotGrpVerif()
        self.assertEqual(args.data_filename, 'synthese.txt')
        self.assertEqual(args.input_dir, self.dir_2016)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.grp_config,
                         [['SC', 'ref', 'DC50', 'DC90', 'DC95'],
                          ['HC', 'ref'],
                          ['SV', 'ref', 'DC95'],
                          ['MODE', 'SMN_TAN', 'SMN_RNA']])
#        self.assertEqual(args.verbose, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        # =====================================================================
        self.figure = []
        for filename in os.listdir(self.dir_out):
            if filename.startswith('synthese_'):
                self.assertTrue(filecmp.cmp(
                    os.path.join(self.dir_out, filename),
                    os.path.join(self.dir_ctl, filename),
                ))
                self.figure.append(filename)
                os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Tracer les valeurs des critères de performance des "\
            "fiches de calage de GRP dont la synthèse est située dans le "\
            "fichier {d} dans le répertoie {I}. Les figures sont créées "\
            "dans le fichier {O}. La synthèse correspond aux expériences "\
            "où le seuil de calage {sck} est dans la liste {scv} "\
            "où l'horizon de calage {hck} est dans la liste {hcv} "\
            "où le seuil de vigilance {svk} est dans la liste {svv} "\
            "où le {mdk} est dans la liste {mdv} "\
            "".format(
                d=args.data_filename, I=args.input_dir, O=args.output_dir,
                sck=args.grp_config[0][0], scv=args.grp_config[0][1:],
                hck=args.grp_config[1][0], hcv=args.grp_config[1][1:],
                svk=args.grp_config[2][0], svv=args.grp_config[2][1:],
                mdk=args.grp_config[3][0], mdv=args.grp_config[3][1:],
                )
        # =====================================================================
