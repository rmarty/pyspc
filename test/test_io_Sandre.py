#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_Sandre in pyspc.io.vigicrues

To run all tests just type:
    python -m unittest test_io_Sandre

To run only a class test:
    python -m unittest test_io_Sandre.TestSandre

To run only a specific test:
    python -m unittest test_io_Sandre.TestSandre.test_init

"""
# Imports
from datetime import datetime as dt
import os.path
import unittest

from pyspc.core.config import Config
from pyspc.core.location import Locations
from pyspc.core.ratingcurve import RatingCurves
from pyspc.core.series import Series
from pyspc.io.sandre import read_Sandre


class TestSandre(unittest.TestCase):
    """
    Sandre_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dir_meta = os.path.join('data', 'metadata', 'sandre')
        self.dir_series = os.path.join('data', 'data', 'sandre')
        self.dir_vigi = os.path.join('data', 'webservice', 'vigicrues')

    def test_loc_hydro(self):
        """
        Test des méthodes de lecture - loc_hydro
        """
        # =====================================================================
        filename = os.path.join(self.dir_meta, 'ZoneHydro_child.xml')
        datatype = 'loc_hydro'
        locs = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(locs, Locations)
        self.assertEqual(len(locs), 4)
        self.assertIn('K0253010', locs)
        self.assertIn('K0253020', locs)
        self.assertIn('K0258010', locs)
        self.assertIn('K0258011', locs)
        # =====================================================================
        filename = os.path.join(self.dir_meta, 'SiteHydro.xml')
        datatype = 'loc_hydro'
        locs = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(locs, Locations)
        self.assertEqual(len(locs), 2)
        self.assertIn('K0550010', locs)
        self.assertIn('K0260010', locs)
        # =====================================================================
        filename = os.path.join(self.dir_meta, 'SiteHydro_child.xml')
        datatype = 'loc_hydro'
        locs = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(locs, Locations)
        self.assertEqual(len(locs), 4)
        self.assertIn('K418001010', locs)
        self.assertIn('K418001020', locs)
        self.assertIn('K418001021', locs)
        self.assertIn('K418001022', locs)
        # =====================================================================
        filename = os.path.join(self.dir_meta, 'StationHydro.xml')
        datatype = 'loc_hydro'
        locs = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(locs, Locations)
        self.assertEqual(len(locs), 2)
        self.assertIn('K435001020', locs)
        self.assertIn('K435001010', locs)
        # =====================================================================
        filename = os.path.join(self.dir_meta, 'StationHydro_child.xml')
        datatype = 'loc_hydro'
        locs = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(locs, Locations)
        self.assertEqual(len(locs), 2)
        self.assertIn('K19300101001', locs)
        self.assertIn('K19300101002', locs)
        # =====================================================================
        filename = os.path.join(self.dir_meta, 'CapteurHydro.xml')
        datatype = 'loc_hydro'
        locs = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(locs, Locations)
        self.assertEqual(len(locs), 3)
        self.assertIn('K1930010', locs)
        self.assertIn('K193001010', locs)
        self.assertIn('K19300101001', locs)
        # =====================================================================

    def test_loc_meteo(self):
        """
        Test des méthodes de lecture - loc_meteo
        """
        # =====================================================================
        filename = os.path.join(self.dir_meta, 'SiteMeteo.xml')
        datatype = 'loc_meteo'
        locs = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(locs, Locations)
        self.assertEqual(len(locs), 3)
        self.assertIn('43130002', locs)
        self.assertIn('43091005', locs)
        self.assertIn('43051003', locs)
        # =====================================================================

    def test_ratingcurves(self):
        """
        Test des méthodes de lecture - ratingcurves
        """
        # =====================================================================
        filename = os.path.join(self.dir_meta, 'RatingCurves.xml')
        datatype = 'ratingcurve'
        curves = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(curves, RatingCurves)
        self.assertEqual(len(curves), 2)
        self.assertIn(('K055001010', 'H201416', 'PHyC'), curves)
        self.assertIn(('K055001010', 'H201620', 'PHyC'), curves)
        # =====================================================================
        filename = os.path.join(self.dir_meta, 'RatingCurves.xml')
        levelcor = os.path.join(self.dir_series, 'levelcor.xml')
        datatype = 'ratingcurve'
        curves = read_Sandre(
            filename=filename, datatype=datatype, levelcor=levelcor)
        self.assertIsInstance(curves, RatingCurves)
        self.assertEqual(len(curves), 2)
        self.assertIn(('K055001010', 'H201416', 'PHyC'), curves)
        self.assertIn(('K055001010', 'H201620', 'PHyC'), curves)
        # =====================================================================

    def test_levelcor(self):
        """
        Test des méthodes de lecture - levelcor
        """
        # =====================================================================
        filename = os.path.join(self.dir_series, 'levelcor.xml')
        datatype = 'levelcor'
        series = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('K055001010', 'HI', 'levelcor'), series)
        # =====================================================================

    def test_data_fcst_hydro(self):
        """
        Test des méthodes de lecture - data_fcst_hydro
        """
        # =====================================================================
        filename = os.path.join(self.dir_series, 'datafcst_hydro.xml')
        datatype = 'data_fcst_hydro'
        series = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 3)
        self.assertIn(
            ('K1930010', 'QH',
             (dt(2016, 4, 20, 7), '45hEAOtt00', 'scen', '10')), series)
        self.assertIn(
            ('K1930010', 'QH',
             (dt(2016, 4, 20, 7), '45hEAOtt00', 'scen', '50')), series)
        self.assertIn(
            ('K1930010', 'QH',
             (dt(2016, 4, 20, 7), '45hEAOtt00', 'scen', '90')), series)
        # =====================================================================
        filename = os.path.join(self.dir_series, 'spcmo.xml')
        datatype = 'data_fcst_hydro'
        series = read_Sandre(
            filename=filename, datatype=datatype, codes=['Y2100020'])
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 12)
        self.assertIn(
            ('Y2100020', 'QH',
             (dt(2019, 11, 19, 6), '11gGRPd130', 'ctl', '10')), series)
        self.assertIn(
            ('Y2100020', 'QH',
             (dt(2019, 11, 19, 6), '11gGRPd130', 'ctl', '50')), series)
        self.assertIn(
            ('Y2100020', 'QH',
             (dt(2019, 11, 19, 6), '11gGRPd130', 'ctl', '90')), series)
        self.assertIn(
            ('Y2100020', 'QH',
             (dt(2019, 11, 19, 6, 0, 4), '11gGRPd130', 'mem', '10')), series)
        self.assertIn(
            ('Y2100020', 'QH',
             (dt(2019, 11, 19, 6, 0, 4), '11gGRPd130', 'mem', '50')), series)
        self.assertIn(
            ('Y2100020', 'QH',
             (dt(2019, 11, 19, 6, 0, 4), '11gGRPd130', 'mem', '90')), series)
        self.assertIn(
            ('Y2100020', 'QI',
             (dt(2019, 11, 19, 5, 55), '11sPLA0001', 'ctl', '10')), series)
        self.assertIn(
            ('Y2100020', 'QI',
             (dt(2019, 11, 19, 5, 55), '11sPLA0001', 'ctl', '50')), series)
        self.assertIn(
            ('Y2100020', 'QI',
             (dt(2019, 11, 19, 5, 55), '11sPLA0001', 'ctl', '90')), series)
        self.assertIn(
            ('Y2100020', 'QI',
             (dt(2019, 11, 19, 5, 55), '11sPLA0001', 'mem', '10')), series)
        self.assertIn(
            ('Y2100020', 'QI',
             (dt(2019, 11, 19, 5, 55), '11sPLA0001', 'mem', '50')), series)
        self.assertIn(
            ('Y2100020', 'QI',
             (dt(2019, 11, 19, 5, 55), '11sPLA0001', 'mem', '90')), series)
        # =====================================================================

    def test_data_obs_hydro(self):
        """
        Test des méthodes de lecture - data_obs_hydro
        """
        # =====================================================================
        datatype = 'data_obs_hydro'
        # =====================================================================
        filename = os.path.join(self.dir_series, 'dataobs_hydro_Q.xml')
        series = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('K5183020', 'QH', None), series)
        # ====================================================================
        filename = os.path.join(self.dir_series, 'dataobs_hydro_H.xml')
        series = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('K518302001', 'HH', None), series)
        # ====================================================================
        filename = os.path.join(
            self.dir_series, 'K0550010_198009060000_198010060000_Q_obs.xml')
        series = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('K0550010', 'QJ', 'QmnJ'), series)
        # ====================================================================
        # test cas avec 1 seule valeur horaire en début de chronique
        filename = os.path.join(
            self.dir_series, 'K4180010_202403080000_202403210000_Q_obs.xml')
        series = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(('K4180010', 'QH', None), series)
        self.assertIn(('K4180010', 'QI', None), series)
        self.assertEqual(series[('K4180010', 'QH', None)].length, 1)
        self.assertEqual(series[('K4180010', 'QI', None)].length, 132)
        # =====================================================================
        # test cas avec valeur heure ronde isolée dans la chronique
        filename = os.path.join(
            self.dir_series, 'K0600010_202403280000_202403300000_Q_obs.xml')
        series = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(('K0600010', 'QH', None), series)
        self.assertIn(('K0600010', 'QI', None), series)
        self.assertEqual(series[('K0600010', 'QH', None)].length, 1)
        self.assertEqual(series[('K0600010', 'QI', None)].length, 48)
        # =====================================================================

    def test_data_obs_meteo(self):
        """
        Test des méthodes de lecture - data_obs_meteo
        """
        # =====================================================================
        filename = os.path.join(self.dir_series, 'dataobs_meteo.xml')
        datatype = 'data_obs_meteo'
        series = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('23209001', 'PH', None), series)
        # =====================================================================

    def test_user_admin(self):
        """
        Test des méthodes de lecture - data_obs_meteo
        """
        # =====================================================================
        filename = os.path.join(self.dir_meta, '404_user_admin.xml')
        datatype = 'user'
        config = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(config, Config)
        self.assertEqual(len(config), 1)
        self.assertIn('404', config)
        self.assertEqual(len(config[('404')]), 17)
        # =====================================================================

    def test_user_loc_meteo(self):
        """
        Test des méthodes de lecture - data_obs_meteo
        """
        # =====================================================================
        filename = os.path.join(self.dir_meta, '404_user_loc_meteo.xml')
        datatype = 'user'
        config = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(config, Config)
        self.assertEqual(len(config), 15)
        self.assertIn('58306001_404_DB', config)
        self.assertEqual(len(config[('58306001_404_DB')]), 7)
        # =====================================================================

    def test_user_site_hydro(self):
        """
        Test des méthodes de lecture - data_obs_meteo
        """
        # =====================================================================
        filename = os.path.join(self.dir_meta, '404_user_site_hydro.xml')
        datatype = 'user'
        config = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(config, Config)
        self.assertEqual(len(config), 12)
        self.assertIn('K1251810_404_EXP', config)
        self.assertEqual(len(config[('K1251810_404_EXP')]), 7)
        # =====================================================================

    def test_user_station_hydro(self):
        """
        Test des méthodes de lecture - data_obs_meteo
        """
        # =====================================================================
        filename = os.path.join(self.dir_meta, '404_user_station_hydro.xml')
        datatype = 'user'
        config = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(config, Config)
        self.assertEqual(len(config), 12)
        self.assertIn('K118001010_404_DC', config)
        self.assertEqual(len(config[('K118001010_404_DC')]), 7)
        # =====================================================================

    def test_vigicrues(self):
        """
        Test des méthodes de lecture - data_obs_hydro
        """
        # =====================================================================
        filename = os.path.join(
            self.dir_vigi, 'vigicrues_obs_202101291400.xml')
        datatype = 'data_obs_hydro'
        series = read_Sandre(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('K055001010', 'QI', None), series)
        # =====================================================================
