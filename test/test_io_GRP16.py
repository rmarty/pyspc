#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for read_GRP16 in pyspc.io.grp16

To run all tests just type:
    python -m unittest test_io_GRP16

To run only a class test:
    python -m unittest test_io_GRP16.TestGRP16

To run only a specific test:
    python -m unittest test_io_GRP16.TestGRP16.test_init

"""
# Imports
from datetime import datetime as dt
import os
import unittest

from pyspc.core.series import Series
from pyspc.io.grp16 import read_GRP16


class TestGRP16(unittest.TestCase):
    """
    GRP16_Stat class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname_cal = os.path.join('data', 'model', 'grp16', 'cal')
        self.dirname_rt = os.path.join('data', 'model', 'grp16', 'rt')

    def test_cal_data(self):
        """
        CALAGE - Cas de données d'observation
        """
        # =====================================================================
        datatype = 'grp16_cal_data'
        valid = {
            ('43091005', 'PH', None): os.path.join(
                self.dirname_cal, '43091005_P.txt'),
            ('K0114030', 'EH', None): os.path.join(
                self.dirname_cal, 'K0114030_EH.txt'),
            ('K0114030', 'QH', None): os.path.join(
                self.dirname_cal, 'K0114030_Q.txt'),
            ('43091005', 'TH', None): os.path.join(
                self.dirname_cal, '43091005_T.txt')
        }
        for k, v in valid.items():
            series = read_GRP16(filename=v, datatype=datatype)
            self.assertIsInstance(series, Series)
            self.assertEqual(len(series), 1)
            self.assertIn(k, series)
        # =====================================================================
        series = read_GRP16(
            datatype=datatype, dirname=self.dirname_cal,
            stations=['43091005', 'K0114030'],
            varnames=['PH', 'EH', 'QH', 'TH'])
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 4)
        for k in valid.keys():
            self.assertIn(k, series)
        # =====================================================================

    def test_cal_event(self):
        """
        CALAGE - Cas de données d'événements
        """
        # =====================================================================
        datatype = 'grp16_cal_event'
        filename = os.path.join(self.dirname_cal, 'K0114030-EV0001.DAT')
        series = read_GRP16(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(('K0114030', 'PH', 'EV0001'), series)
        self.assertIn(('K0114030', 'QH', 'EV0001'), series)
        # =====================================================================

    def test_cal_fcst(self):
        """
        CALAGE - Cas de données de prévision
        """
        # =====================================================================
        datatype = 'grp16_cal_fcst'
        filename = os.path.join(
            self.dirname_cal, 'H_K0114030_GRP_SMN_RNA_006_PP_P1P2.TXT')
        series = read_GRP16(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 11)
        for x in [0, 1, 3, 6, 9, 12]:
            self.assertIn(('K0114030', 'QH', 'OBS{0:03d}'.format(x)), series)
        for x in [1, 3, 6, 9, 12]:
            self.assertIn(('K0114030', 'QH', 'PRV{0:03d}'.format(x)), series)
        # =====================================================================

    def test_rt_archive(self):
        """
        TEMPS-REEL - Cas de données d'archive
        """
        # =====================================================================
        datatype = 'grp16_rt_archive'
        filename = os.path.join(self.dirname_rt, 'PV_10A.DAT')
        series = read_GRP16(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('43091005', 'PH', None), series)
        # =====================================================================
        filename = os.path.join(self.dirname_rt, 'QV_10A.DAT')
        series = read_GRP16(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('K0114030', 'QH', None), series)
        # =====================================================================
        filename = os.path.join(self.dirname_rt, 'TV_10A.DAT')
        series = read_GRP16(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 1)
        self.assertIn(('07105003', 'TH', None), series)
        # =====================================================================

    def test_rt_data(self):
        """
        TEMPS-REEL - Cas de données d'observation
        TEMPS-REEL - Cas de scénarios météorologiques
        """
        # =====================================================================
        datatype = 'grp16_rt_data'
        filename = os.path.join(self.dirname_rt, 'Debit.txt')
        series = read_GRP16(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(('K0114020', 'QH', None), series)
        self.assertIn(('K0114030', 'QH', None), series)
        # =====================================================================
        filename = os.path.join(self.dirname_rt, 'Scen_006_PluMA.txt')
        series = read_GRP16(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(
            ('K0114020', 'PH', (dt(2017, 6, 13, 12), 'grp16', '006', None)),
            series)
        self.assertIn(
            ('K0114030', 'PH', (dt(2017, 6, 13, 12), 'grp16', '006', None)),
            series)
        # =====================================================================

    def test_rt_fcst(self):
        """
        TEMPS-REEL - Cas d'export d'observation
        TEMPS-REEL - Cas de prévision sans assimilation
        TEMPS-REEL - Cas de prévision avec assimilation
        """
        # =====================================================================
        datatype = 'grp16_rt_obs_diff'
        filename = os.path.join(self.dirname_rt, 'GRP_D_Obs.txt')
        series = read_GRP16(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(('K6173130', 'QH', 'grp16'), series)
        self.assertIn(('K6173130', 'PH', 'grp16'), series)
        # =====================================================================
        datatype = 'grp16_rt_sim_diff'
        filename = os.path.join(self.dirname_rt, 'GRP_D_Simu_2001.txt')
        series = read_GRP16(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(
            ('K6173130', 'QH',
             (dt(2016, 5, 31, 16), 'grp16-sim', '2001', None)), series)
        self.assertIn(
            ('K6173130', 'PH',
             (dt(2016, 5, 31, 16), 'grp16-sim', '2001', None)), series)
        # =====================================================================
        datatype = 'grp16_rt_fcst_diff'
        filename = os.path.join(self.dirname_rt, 'GRP_D_Prev_2001.txt')
        series = read_GRP16(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 2)
        self.assertIn(
            ('K6173130', 'QH',
             (dt(2016, 5, 31, 16), 'grp16', '2001', None)), series)
        self.assertIn(
            ('K6173130', 'PH',
             (dt(2016, 5, 31, 16), 'grp16', '2001', None)), series)
        # =====================================================================

    def test_rt_intern(self):
        """
        TEMPS-REEL - Cas de données internes au modèle
        """
        # =====================================================================
        datatype = 'grp16_rt_intern_diff'
        filename = os.path.join(self.dirname_rt, 'intern', 'PQE_1A_D.DAT')
        series = read_GRP16(filename=filename, datatype=datatype)
        self.assertIsInstance(series, Series)
        self.assertEqual(len(series), 4)
        self.assertIn(('K1251810', 'QH', None), series)
        self.assertIn(('K1251810', 'QH', 'grp16'), series)
        self.assertIn(('K1251810', 'PH', None), series)
        self.assertIn(('K1251810', 'EH', None), series)
        # =====================================================================
