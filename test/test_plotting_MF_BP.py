#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Config in pyspc.plotting

To run all tests just type:
    python -m unittest test_plotting_MF_BP

To run only a class test:
    python -m unittest test_plotting_MF_BP.TestBP

To run only a specific test:
    python -m unittest test_plotting_MF_BP.TestBP.test_plot

"""
# Imports
from datetime import datetime as dt
import filecmp
import os.path
# import pandas as pnd
import unittest

# from pyspc.core.serie import Serie
# from pyspc.core.series import Series
from pyspc.io.pyspcfile import read_PyspcFile
from pyspc.plotting.bp import plot_bp_byzone, plot_bp_byday  # , plot_bp_v2


class TestBP(unittest.TestCase):
    """
    Colormap class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'plotting', 'bp')
        self.bp = read_PyspcFile(
            dirname=os.path.join(self.dirname, 'bp'),
            stations=['41003', '41004', '41005'],
            varnames='PJ',
            runtimes=[dt(2019, 10, 17, 5), dt(2019, 10, 17, 11),
                      dt(2019, 10, 18, 5), dt(2019, 10, 18, 11),
                      dt(2019, 10, 19, 5), dt(2019, 10, 19, 11),
                      dt(2019, 10, 20, 5), dt(2019, 10, 20, 11),
                      dt(2019, 10, 21, 5), dt(2019, 10, 21, 10)],
            models='BP',
            scens=['MoyInf', 'MoySup']
        )
        self.prcp = read_PyspcFile(
            dirname=os.path.join(self.dirname, 'prcp'),
            stations=['41003', '41004', '41005'],
            varnames='PJ',
        )
        self.radar = read_PyspcFile(
            dirname=os.path.join(self.dirname, 'radar'),
            stations=['41003', '41004', '41005'],
            varnames='PJ',
        )

    def test_plot_byzone(self):
        """
        Test création figure BP vs PRCP vs RADAR - 1 image par zone
        """
        # =====================================================================
        tmp_files = plot_bp_byzone(
            bp_series=self.bp,
            radar_series=self.radar,
            prcp_series=self.prcp,
            dirname='data'
        )
        # =====================================================================
        for tmp_file in tmp_files:
            filename = os.path.join(self.dirname, os.path.basename(tmp_file))
            self.assertTrue(filecmp.cmp(
                filename,
                tmp_file
            ))
            os.remove(tmp_file)
        # =====================================================================

    def test_plot_byday(self):
        """
        Test création figure BP vs PRCP vs RADAR - 1 image par jour
        """
        # =====================================================================
        target_days = [dt(2019, 10, 20)]
        tmp_files = plot_bp_byday(
            bp_series=self.bp,
            radar_series=self.radar,
            prcp_series=self.prcp,
            target_days=target_days,
            dirname='data'
        )
        # =====================================================================
        for tmp_file in tmp_files:
            filename = os.path.join(self.dirname, os.path.basename(tmp_file))
            self.assertTrue(filecmp.cmp(
                filename,
                tmp_file
            ))
            os.remove(tmp_file)
        # =====================================================================
#
#    def test_plot_v2(self):
#        """Test d'une nouvelle version"""
#        # ====================================================================
#        refs = pnd.DataFrame(
#            {
#                ('43002', 'PJ', 'radar'): [6.5, 113.4, 131.9, 5.6],
#                ('43003', 'PJ', 'radar'): [0.8, 41.8, 113.6, 6.3],
#                ('43015', 'PJ', 'radar'): [5.5, 53.0, 55.8, 1.2],
#                ('43002', 'PJ', 'prcp'): [6.5, 113.4, 131.9, 5.6],
#                ('43003', 'PJ', 'prcp'): [0.8, 41.8, 113.6, 6.3],
#                ('43015', 'PJ', 'prcp'): [5.5, 53.0, 55.8, 1.2]
#            },
#            index=pnd.date_range(dt(2024, 10, 15), dt(2024, 10, 18), freq='D')
#        )
#        ref_series = Series(datatype='obs', name='ref')
#        for c in refs.columns:
#            df = refs[c].to_frame()
#            s = Serie(df, code=c[0], varname=c[1])
#            ref_series.add(s, meta=c[2])
#        # ====================================================================
#        # ====================================================================
#        # ====================================================================
#        tmp_files = plot_bp_v2(
#            ref_series=ref_series
#        )
#        print(tmp_files)
#        # ====================================================================
