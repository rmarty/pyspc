#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Config in pyspc.plotting

To run all tests just type:
    python -m unittest test_plotting_config

To run only a class test:
    python -m unittest test_plotting_config.TestConfig

To run only a specific test:
    python -m unittest test_plotting_config.TestConfig.test_load

"""
# Imports
from datetime import datetime as dt
import os.path
import unittest

from pyspc.plotting.config import Config


class TestConfig(unittest.TestCase):
    """
    Colormap class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'plotting')

    def test_load(self):
        """
        test chargement d'une configuration 'image' depuis un fichier
        """
        # =====================================================================
        valid = {
            'figure': {
                'plottype': 'event',
                'dirname': 'data',
                'filename': 'test_plot_event',
                'verbose': 'True',
                'dpi': 150,
                'format': 'png',
                'title': 'test_plot_event',
                'xtitle': 'temps',
                'xlim': [dt(2014, 11, 3), dt(2014, 11, 6)],
                'xfmt': '%Y-%m-%d\n%H:%M',
                'xminor': [int(x) for x in '0,25,6'.split(',')],
                'ytitlep': 'prcp ($mm.h^{-1}$)',
                'ytitlet': 'temp ($Celsius$)',
                'ytitleq': 'debit ($m^3.s^{-1}$)',
                'ylimp': [float(x) for x in '0.0,30.0'.split(',')],
                'ylimt': [float(x) for x in '-5.0,20.0'.split(',')],
                'ylimq': [float(x) for x in '0.0,200.0'.split(',')],
                'legend': 0,
                'lfontsize': 8,
                'xfontsize': 10,
                'yfontsize': 10,
                'tfontsize': 14
            },
            'defaut': {
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'qh_QH': {
                'color': [float(x) for x in '0.2,0.6,1.0'.split(',')],
                'label': 'Station 1 (QH)',
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'qi_QI': {
                'color': [float(x) for x in '0.0,0.0,1.0'.split(',')],
                'label': 'Station 2 (QI)',
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'ph1_PH': {
                'color': [float(x) for x in '1.0,0.0,0.4'.split(',')],
                'label': 'Station 1 (PH)',
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'th_TH': {
                'color': [float(x) for x in '1.0,0.6,0.6'.split(',')],
                'label': 'Station 1 (TH)',
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'ph2_PH': {
                'color': [float(x) for x in '0.0,0.4,0.0'.split(',')],
                'label': 'Station 2 (PH)',
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
            'ti_TI': {
                'color': [float(x) for x in '0.4,1.0,0.4'.split(',')],
                'label': 'Station 2 (TI)',
                'linewidth': 1,
                'linestyle': '-',
                'marker': '',
                'markersize': 0
            },
        }
        # =====================================================================
        filename = os.path.join(self.dirname, 'event.txt')
        config = Config(filename=filename)
        config.load()
#        self.maxDiff = None
        self.assertDictEqual(config, valid)
#        for s in config:
#            for o in config[s]:
#                print(s, o, config[s][o], valid[s][o])
#                self.assertEqual(config[s][o], valid[s][o])
        # =====================================================================
