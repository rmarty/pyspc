#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP_Basin in pyspc.model.grp16

To run all tests just type:
    python -m unittest test_model_GRP16_CAL_Basin

To run only a class test:
    python -m unittest test_model_GRP16_CAL_Basin.TestGRP_Basin

To run only a specific test:
    python -m unittest test_model_GRP16_CAL_Basin.TestGRP_Basin.test_init

"""
# Imports
import filecmp
import os
import unittest

from pyspc.model.grp16 import GRP_Basin


class TestGRP_Basin(unittest.TestCase):
    """
    GRP_Basin class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp16', 'cal')
        self.valid = {
            'E': {
                'K0114030': {'n': 'Code et ponderation poste ETP', 'w': 1.0},
            },
            'P': {
                '43157008': {'n': 'LE PUY-EN-VELAY-SPC', 'w': 0.05},
                '43135005': {'n': 'LE-MONASTIER-SUR-GAZEILLE2-SPC', 'w': 0.30},
                '43101002': {'n': 'GOUDET-SPC', 'w': 0.30},
                '43091005': {'n': 'LES-ESTABLES_SAPC', 'w': 0.05},
                '43186003': {'n': 'SAINT-FRONT - MACHABERT', 'w': 0.30},
            },
            'T': {
                '43091005': {'n': 'LES ESTABLES_SAPC', 'w': 0.9, 'z': 1350.0},
                '43130002': {'n': 'MAZET-VOLAMONT', 'w': 0.1, 'z': 1130.0},
            }
        }

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'K0114030.dat')
        basin = GRP_Basin(filename=filename)
        self.assertEqual(basin.filename, filename)

    def test_read(self):
        """
        Test de la lecture du fichier
        """
        filename = os.path.join(self.dirname, 'K0114030.dat')
        basin = GRP_Basin(filename=filename)
        basin.read()
        self.assertDictEqual(basin, self.valid)

    def test_write(self):
        """
        Test de l'écriture du fichier
        """
        filename = os.path.join(self.dirname, 'K0114030.dat')
        tmpfile = os.path.join('data', 'K0114030.dat')
        basin = GRP_Basin(filename=tmpfile)
        basin.update(self.valid)
        basin.write()
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
