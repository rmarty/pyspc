#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2022  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRPRT_Metscen in pyspc.model.grp22

To run all tests just type:
    python -m unittest test_model_GRP22_RT_Metscen

To run only a class test:
    python -m unittest test_model_GRP22_RT_Metscen.TestGRPRT_Metscen

To run only a specific test:
    python -m unittest test_model_GRP22_RT_Metscen.TestGRPRT_Metscen.test_init

"""
# Imports
import filecmp
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.model.grp22 import GRPRT_Metscen


class TestGRPRT_Metscen(unittest.TestCase):
    """
    GRPRT_Metscen class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp22', 'rt')
        self.valid_scen = pnd.DataFrame({
            'PREFIX': ['PLU']*20,
            'CODE': ['RH10585x']*20,
            'DATETIME': [202401250100, 202401250200, 202401250300,
                         202401250400, 202401250500, 202401250600,
                         202401250700, 202401250800, 202401250900,
                         202401251000, 202401251100, 202401251200,
                         202401251300, 202401251400, 202401251500,
                         202401251600, 202401251700, 202401251800,
                         202401251900, 202401252000],
            'VALUE': [0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 1.07, 1.07, 1.07,
                      2.37, 2.37, 2.37, 3.35, 3.35, 3.35, 4.07, 4.07, 4.07,
                      0.00, 0.00],
        })
        self.valid_scenT = pnd.DataFrame({
            'PREFIX': ['TEM']*20,
            'CODE': ['RH10585x']*20,
            'DATETIME': [202401250100, 202401250200, 202401250300,
                         202401250400, 202401250500, 202401250600,
                         202401250700, 202401250800, 202401250900,
                         202401251000, 202401251100, 202401251200,
                         202401251300, 202401251400, 202401251500,
                         202401251600, 202401251700, 202401251800,
                         202401251900, 202401252000],
            'VALUE': [0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 1.07, 1.07, 1.07,
                      2.37, 2.37, 2.37, 3.35, 3.35, 3.35, 4.07, 4.07, 4.07,
                      0.00, 0.00],
        })

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'Scen_0001_PluSY_00J01H00M.txt')
        reader = GRPRT_Metscen(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'P')
        self.assertEqual(reader.lineprefix, 'PLU')
        self.assertEqual(reader.scen, '0001')
        self.assertEqual(reader.timestep, '00J01H00M')
        # =====================================================================

    def test_read_scen(self):
        """
        Test de lecture d'un fichier de données OBS pour GRP RT - Cas Pluie
        """
        filename = os.path.join(self.dirname, 'Scen_0001_PluSY_00J01H00M.txt')
        reader = GRPRT_Metscen(filename=filename)
        df = reader.read()
        assert_frame_equal(df, self.valid_scen)

    def test_read_scen_T(self):
        """
        Test de lecture d'un fichier de données OBS pour GRP RT - Cas Temp.
        """
        filename = os.path.join(self.dirname, 'ScenT_0001_TemSY_00J01H00M.txt')
        reader = GRPRT_Metscen(filename=filename)
        df = reader.read()
        assert_frame_equal(df, self.valid_scenT)

    def test_write_scen(self):
        """
        Test d'écriture d'un fichier de données OBS pour GRP RT - Cas Pluie
        """
        filename = os.path.join(self.dirname, 'Scen_0001_PluSY_00J01H00M.txt')
        tmpfile = os.path.join('data', 'Scen_0001_PluSY_00J01H00M.txt')
        writer = GRPRT_Metscen(filename=tmpfile)
        writer.write(self.valid_scen)
        self.assertTrue(filecmp.cmp(filename, tmpfile))
        os.remove(tmpfile)

    def test_write_scen_T(self):
        """
        Test d'écriture d'un fichier de données OBS pour GRP RT - Cas Temp.
        """
        filename = os.path.join(self.dirname, 'ScenT_0001_TemSY_00J01H00M.txt')
        tmpfile = os.path.join('data', 'ScenT_0001_TemSY_00J01H00M.txt')
        writer = GRPRT_Metscen(filename=tmpfile)
        writer.write(self.valid_scenT)
        self.assertTrue(filecmp.cmp(filename, tmpfile))
        os.remove(tmpfile)
