#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary hydroportailStats

To run all tests just type:
    python -m unittest test_bin_hydroportailStats

To run only a class test:
    python -m unittest test_bin_hydroportailStats.Test_hydroportailStats

To run only a specific test:
    python -m unittest test_bin_hydroportailStats.Test_hydroportailStats.test_debcla

"""
# Imports
import os
# import subprocess
import sys
import unittest

from pyspc.binutils.args import hydroportailStats as _args
from pyspc.binutils.runs import hydroportailStats as _runs
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_hydroportailStats(unittest.TestCase):
    """
    hydroportailStats bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'hydroportailStats')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.file_cfg = os.path.join(cls.dir_cfg, 'config.txt')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None
        self.stations_list_file = None

    def tearDown(self):
        """Applied after test"""
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init,
                        stations_list_file=self.stations_list_file)

    def test_deblca(self):
        """
        Test requête PHyC (data_fcst_hydro)
        """
        valid = {
            ('K1321810', 'DEBCLA'): [
                r'data\_bin\hydroportailStats\out\K1321810_DEBCLA_0.csv'],
            ('K1341810', 'DEBCLA'): [
                r'data\_bin\hydroportailStats\out\K1341810_DEBCLA_0.csv']}
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_cfg, 'siteshydro.txt')
        processArgs = [
            'python',
            '../bin/hydroportailStats.py',
            '-l', self.stations_list_file,
            '-t', 'DEBCLA',
            '-c', self.file_cfg,
            '-O', self.dir_out,
            '-v'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.hydroportailStats()
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.stations_list_file, self.stations_list_file)
        self.assertEqual(args.station_name, None)
        self.assertEqual(args.datatype, 'DEBCLA')
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
        with captured_output():
            filenames = _runs.hydroportailStats(args)
        self.assertDictEqual(valid, filenames)
        for fs in filenames.values():
            for f in fs:
                os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Récupérer des statistiques hydrologiques ({t}) "\
            "établies par Hydroportail pour le lieu {s} et enregistrer les "\
            "fichiers csv dans le répertoire {O}. "\
            "L'adresse de l'Hydroportail est "\
            "renseignée dans le fichier {c}"\
            .format(
                t=args.datatype, c=args.cfg_filename,
                s=args.station_name, O=args.output_dir)
        # =====================================================================

    def test_qix(self):
        """
        Test requête PHyC (data_fcst_hydro)
        """
        valid = {
            ('K1251810', 'Q-X'): [
                r'data\_bin\hydroportailStats\out\K1251810_Q-X_seasons.csv',
                r'data\_bin\hydroportailStats\out\K1251810_Q-X_seasons-ouahs.csv',
                r'data\_bin\hydroportailStats\out\K1251810_Q-X_result-quantile.csv',
                r'data\_bin\hydroportailStats\out\K1251810_Q-X_result-empirical.csv',
                r'data\_bin\hydroportailStats\out\K1251810_Q-X_result-parameters.csv',
                r'data\_bin\hydroportailStats\out\K1251810_Q-X_result-observations.csv',
                r'data\_bin\hydroportailStats\out\K1251810_Q-X_result-test.csv',
                r'data\_bin\hydroportailStats\out\K1251810_Q-X_analysis.csv',
                r'data\_bin\hydroportailStats\out\K1251810_Q-X_descriptiveStats.csv'
        ]}
        # =====================================================================
        self.stations_list_file = os.path.join(self.dir_cfg, 'siteshydro.txt')
        processArgs = [
            'python',
            '../bin/hydroportailStats.py',
            '-s', 'K1251810',
            '-t', 'Q-X',
            '-c', self.file_cfg,
            '-O', self.dir_out,
            '-v'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.hydroportailStats()
        self.assertEqual(args.cfg_filename, self.file_cfg)
        self.assertEqual(args.stations_list_file, None)
        self.assertEqual(args.station_name, 'K1251810')
        self.assertEqual(args.datatype, 'Q-X')
        self.assertEqual(args.output_dir, self.dir_out)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
#        print(' '.join(processArgs))
#        processRun = subprocess.Popen(
#            processArgs, universal_newlines=True,
#            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
#        processRun.wait()
        with captured_output():
            filenames = _runs.hydroportailStats(args)
        self.maxDiff = None
        self.assertDictEqual(valid, filenames)
        for fs in filenames.values():
            for f in fs:
                os.remove(f)
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Récupérer des statistiques hydrologiques ({t}) "\
            "établies par Hydroportail pour le lieu {s} et enregistrer les "\
            "fichiers csv dans le répertoire {O}. "\
            "L'adresse de l'Hydroportail est "\
            "renseignée dans le fichier {c}"\
            .format(
                t=args.datatype, c=args.cfg_filename,
                s=args.station_name, O=args.output_dir)
        # =====================================================================
