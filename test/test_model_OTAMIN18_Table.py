#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Table in pyspc.model.otamin18

To run all tests just type:
    python -m unittest test_model_OTAMIN18_Table

To run only a class test:
    python -m unittest test_model_OTAMIN18_Table.TestOtamin_Table

To run only a specific test:
    python -m unittest test_model_OTAMIN18_Table.TestOtamin_Table.test_init

"""
# Imports
from datetime import timedelta as td
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.model.otamin18 import Table


class TestOtamin_Table(unittest.TestCase):
    """
    Table class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        # =====================================================================
        self.dirname = os.path.join('data', 'model', 'otamin18')
        # =====================================================================

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'A6701210_57gGRPd000_QUOIQUE_EM_INC_TAB.csv')
        station = 'A6701210'
        model = '57gGRPd000'
        method = 'QUOIQUE'
        error = 'EM'
        reader = Table(filename=filename)
        # =====================================================================
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.station, station)
        self.assertEqual(reader.model, model)
        self.assertEqual(reader.method, method)
        self.assertEqual(reader.error, error)
        self.assertEqual(reader.datatype, 'TAB')
        # =====================================================================

    def test_read(self):
        """
        Test de la lecture
        """
        # =====================================================================
        valid = pnd.DataFrame(
            {
                'HorPrevi': [td(hours=12), td(hours=12), td(hours=12),
                             td(hours=12), td(hours=12), td(hours=12),
                             td(hours=12), td(hours=12), td(hours=12),
                             td(hours=12), td(hours=24), td(hours=24),
                             td(hours=24), td(hours=24), td(hours=24),
                             td(hours=24), td(hours=24), td(hours=24),
                             td(hours=24), td(hours=24)],
                'df_val': [21.550, 22.662, 23.935, 25.481, 26.924, 28.791,
                           32.765, 38.384, 45.088, 123.543, 21.187, 22.076,
                           23.287, 24.542, 26.042, 27.819, 30.995, 36.257,
                           44.937, 138.550],
                'U10': [15.838, 16.654, 17.590, 18.727, 19.787, 21.159, 24.080,
                        28.209, 33.136, 90.794, 14.825, 15.446, 16.294,
                        17.172, 18.222, 19.465, 21.687, 25.369, 31.442,
                        96.944],
                'U20': [17.791, 18.709, 19.760, 21.037, 22.227, 23.769, 27.050,
                        31.689, 37.223, 101.994, 17.092, 17.809, 18.786,
                        19.799, 21.009, 22.443, 25.004, 29.250, 36.252,
                        111.771],
                'U30': [19.176, 20.164, 21.298, 22.674, 23.957, 25.619, 29.155,
                        34.155, 40.120, 109.930, 18.473, 19.247, 20.304,
                        21.398, 22.705, 24.255, 27.024, 31.612, 39.179,
                        120.798],
                'U40': [20.342, 21.391, 22.593, 24.053, 25.414, 27.177, 30.928,
                        36.232, 42.560, 116.617, 19.791, 20.620, 21.752,
                        22.924, 24.325, 25.986, 28.952, 33.867, 41.975,
                        129.417],
                'U50': [21.467, 22.574, 23.842, 25.382, 26.819, 28.679, 32.638,
                        38.235, 44.913, 123.064, 20.927, 21.804, 23.001,
                        24.240, 25.722, 27.478, 30.614, 35.812, 44.385,
                        136.847],
                'U60': [22.541, 23.703, 25.035, 26.653, 28.162, 30.115, 34.271,
                        40.149, 47.161, 129.223, 22.120, 23.047, 24.312,
                        25.622, 27.188, 29.044, 32.359, 37.853, 46.915,
                        144.648],
                'U70': [23.821, 25.049, 26.457, 28.166, 29.761, 31.824, 36.217,
                        42.428, 49.839, 136.560, 23.404, 24.386, 25.724,
                        27.110, 28.767, 30.731, 34.238, 40.052, 49.640,
                        153.049],
                'U80': [25.361, 26.669, 28.167, 29.987, 31.685, 33.882, 38.559,
                        45.172, 53.061, 145.389, 25.384, 26.449, 27.901,
                        29.404, 31.201, 33.331, 37.135, 43.440, 53.839,
                        165.997],
                'U90': [27.947, 29.388, 31.040, 33.045, 34.915, 37.337, 42.490,
                        49.778, 58.471, 160.214, 29.113, 30.334, 31.999,
                        33.723, 35.784, 38.226, 42.589, 49.821, 61.747,
                        190.379],
            }
        )
        # =====================================================================
        filename = os.path.join(
            self.dirname, 'A6701210_57gGRPd000_QUOIQUE_EM_INC_TAB.csv')
        reader = Table(filename=filename)
        df = reader.read()
        assert_frame_equal(valid, df)
        # =====================================================================
