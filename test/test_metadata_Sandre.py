#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Sandre in pyspc.metadata.sandre

To run all tests just type:
    python -m unittest test_metadata_Sandre

To run only a class test:
    python -m unittest test_metadata_Sandre.TestSandre

To run only a specific test:
    python -m unittest test_metadata_Sandre.TestSandre.test_init

"""
# Imports
from datetime import datetime as dt
import os
import unittest

from pyspc.metadata.sandre import Sandre


class TestSandre(unittest.TestCase):
    """
    Sandre class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'metadata', 'sandre')

    def test_init(self):
        """
        Test des méthodes de lecture
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'SiteHydro.xml')
        sandre = Sandre(filename=filename)
        self.assertEqual(sandre.filename, filename)
        # =====================================================================

    def test_read_zonehydro_child(self):
        """
        Test read_sitehydro
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'ZoneHydro_child.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        # =====================================================================
        valid = {
            'K0253010': {
                'code': 'K0253010',
                'typesite': 'STANDARD',
                'libelle': 'La Borne à Chadrac [Ancienne]',
                'stations': [],
                'statut': 1,
            },
            'K0253020': {
                'code': 'K0253020',
                'typesite': 'STANDARD',
                'libelle': 'La Borne à Aiguilhe et à Espaly-Saint-Marcel',
                'stations': [],
                'statut': 1,
            },
            'K0258010': {
                'code': 'K0258010',
                'typesite': 'STANDARD',
                'libelle': 'Le Dolaizon à Vals-près-le-Puy',
                'stations': [],
                'statut': 1,
            },
            'K0258011': {
                'code': 'K0258011',
                'typesite': 'PONCTUEL',
                'libelle': 'Le ruisseau de Ceyssac à Bains',
                'stations': [],
                'statut': 1,
            },
        }
        for s in content.siteshydro:
            self.assertIn(s.code, valid)
            v = valid[s.code]
            self.assertEqual(s.code, v['code'])
            self.assertEqual(s.typesite, v['typesite'])
            self.assertEqual(s.libelle, v['libelle'])
            self.assertEqual(s.stations, v['stations'])
            self.assertEqual(s.statut, v['statut'])
        # =====================================================================

    def test_read_sitehydro(self):
        """
        Test read_sitehydro
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'SiteHydro.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        # =====================================================================
        valid = {
            'K0550010': {
                'code': 'K0550010',
                'codeh2': 'K0550010',
                'typesite': 'STANDARD',
                'libelle': 'La Loire à Bas-en-Basset',
                'libelleusuel': 'Bas-en-Basset',
                # 'coord': '',
                # 'altitude': '',
                'stations': [],
                'tronconhydro': 'K0550000',
                'bvtopo': 3234.0,
                'statut': 1,
            },
            'K0260010': {
                'code': 'K0260010',
                'codeh2': 'K0260010',
                'typesite': 'STANDARD',
                'libelle': 'La Loire à Chadrac',
                'libelleusuel': 'Chadrac',
                # 'coord': '',
                # 'altitude': '',
                'stations': [],
                'tronconhydro': 'K0260000',
                'bvtopo': 1310.0,
                'statut': 1,
            },
        }
        for s in content.siteshydro:
            self.assertIn(s.code, valid)
            v = valid[s.code]
            self.assertEqual(s.code, v['code'])
            self.assertEqual(s.codeh2, v['codeh2'])
            self.assertEqual(s.typesite, v['typesite'])
            self.assertEqual(s.libelle, v['libelle'])
            self.assertEqual(s.libelleusuel, v['libelleusuel'])
            # self.assertEqual(s.coord, '')
            # self.assertEqual(s.altitude, '')
            self.assertEqual(s.stations, v['stations'])
            self.assertEqual(s.tronconhydro, v['tronconhydro'])
            self.assertEqual(s.bvtopo, v['bvtopo'])
            self.assertEqual(s.statut, v['statut'])
        # =====================================================================

    def test_read_sitehydro_child(self):
        """
        Test read_stationhydro
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'SiteHydro_child.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        # =====================================================================
        valid = {
            'K418001010': {
                'code': 'K418001010',
                'typestation': 'STD',
                'libelle': 'La Loire à Gien - Vieux Pont',
                'capteurs': [],
            },
            'K418001020': {
                'code': 'K418001020',
                'typestation': 'STD',
                'libelle': 'La Loire à Gien - Le Colombier',
                'capteurs': [],
            },
            'K418001021': {
                'code': 'K418001021',
                'typestation': 'STD',
                'libelle': 'La Loire à Gien - avant Villerest à Gien',
                'capteurs': [],
            },
            'K418001022': {
                'code': 'K418001022',
                'typestation': 'STD',
                'libelle': 'La Loire à Gien - Nouveau Pont',
                'capteurs': [],
            },
        }
        for s in content.siteshydro:
            for s2 in s.stations:
                self.assertIn(s2.code, valid)
                v = valid[s2.code]
                self.assertEqual(s2.code, v['code'])
                self.assertEqual(s2.typestation, v['typestation'])
                self.assertEqual(s2.libelle, v['libelle'])
                self.assertEqual(s2.capteurs, v['capteurs'])
        # =====================================================================

    def test_read_stationhydro(self):
        """
        Test read_stationhydro
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'StationHydro.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        # =====================================================================
        valid = {
            'K435001020': {
                'code': 'K435001020',
                'codeh2': 'K4350020',
                'typestation': 'STD',
                'libelle': 'La Loire à Orléans - Quai du Roi',
                'libellecomplement': 'Quai du Roi',
                # 'coord': '',
                'capteurs': [],
            },
            'K435001010': {
                'code': 'K435001010',
                'codeh2': 'K4350010',
                'typestation': 'STD',
                'libelle': 'La Loire à Orléans - Pont Royals',
                'libellecomplement': 'Pont Royals',
                # 'coord': '',
                'capteurs': [],
            },
        }
        for s in content.siteshydro:
            for s2 in s.stations:
                self.assertIn(s2.code, valid)
                v = valid[s2.code]
                self.assertEqual(s2.code, v['code'])
                self.assertEqual(s2.codeh2, v['codeh2'])
                self.assertEqual(s2.typestation, v['typestation'])
                self.assertEqual(s2.libelle, v['libelle'])
                self.assertEqual(s2.libellecomplement, v['libellecomplement'])
                # self.assertEqual(s2.coord, '')
                self.assertEqual(s2.capteurs, v['capteurs'])
        # =====================================================================

    def test_read_stationhydro_child(self):
        """
        Test read_capteurhydro
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'StationHydro_child.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        # =====================================================================
        valid = {
            'K19300101001': {
                'code': 'K19300101001',
                'codeh2': 'K1930010',
                'typecapteur': 5,
                'typemesure': 'H',
                'libelle': 'Nevers - Sonde',
            },
            'K19300101002': {
                'code': 'K19300101002',
                'codeh2': None,
                'typecapteur': 2,
                'typemesure': 'H',
                'libelle': 'Nevers - Bulle à bulle',
            },
        }
        for s in content.siteshydro:
            for s2 in s.stations:
                for s3 in s2.capteurs:
                    self.assertIn(s3.code, valid)
                    v = valid[s3.code]
                    self.assertEqual(s3.code, v['code'])
                    self.assertEqual(s3.codeh2, v['codeh2'])
                    self.assertEqual(s3.typecapteur, v['typecapteur'])
                    self.assertEqual(s3.typemesure, v['typemesure'])
                    self.assertEqual(s3.libelle, v['libelle'])
        # =====================================================================

    def test_read_capteurhydro(self):
        """
        Test read_capteurhydro
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'CapteurHydro.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        # =====================================================================
        valid = {
            'K19300101001': {
                'code': 'K19300101001',
                'codeh2': 'K1930010',
                'typecapteur': 5,
                'typemesure': 'H',
                'libelle': 'Nevers - Sonde',
            },
        }
        for s in content.siteshydro:
            for s2 in s.stations:
                for s3 in s2.capteurs:
                    self.assertIn(s3.code, valid)
                    v = valid[s3.code]
                    self.assertEqual(s3.code, v['code'])
                    self.assertEqual(s3.codeh2, v['codeh2'])
                    self.assertEqual(s3.typecapteur, v['typecapteur'])
                    self.assertEqual(s3.typemesure, v['typemesure'])
                    self.assertEqual(s3.libelle, v['libelle'])
        # =====================================================================

    def test_read_sitemeteo(self):
        """
        Test read_sitemeteo
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'SiteMeteo.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        # =====================================================================
        valid = {
            '043130002': {
                'code': '043130002',
                'libelle': 'MAZET-VOLAMONT',
                'libelleusuel': 'MAZET-VOLAMONT',
            },
            '043091005': {
                'code': '043091005',
                'libelle': 'LES ESTABLES_SAPC',
                'libelleusuel': 'LES ESTABLES_SAPC',
            },
            '043051003': {
                'code': '043051003',
                'libelle': 'LE-CHAMBON-SUR-LIGNON-SPC',
                'libelleusuel': 'LE-CHAMBON-SUR-LIGNON-SPC',
            },
        }
        for s in content.sitesmeteo:
            self.assertIn(s.code, valid)
            v = valid[s.code]
            self.assertEqual(s.code, v['code'])
            self.assertEqual(s.libelle, v['libelle'])
            self.assertEqual(s.libelleusuel, v['libelleusuel'])
            # self.assertEqual(s.coord, '')
        # =====================================================================

    def test_read_ratingcurves(self):
        """
        Test read_ratingcurves
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'RatingCurves.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        # =====================================================================
        valid = \
            {'10016':
                {'code': '10016',
                 'typect': 4,
                 'libelle': 'H201416',
                 'limiteinf': -1360.0,
                 'limitesup': 5240.0,
                 'station': 'K055001010',
                 'dtmaj': dt(2017, 1, 13, 13, 53, 15),
                 'periodes': [(dt(2014, 11, 5, 0, 40),
                               dt(2016, 11, 23, 2, 29, 59)),
                              (dt(2014, 11, 5, 0, 40),
                               dt(2016, 11, 23, 2, 30))],
                 'pivots': [
                     {'h': -1440.0, 'vara': 1.0, 'varb': 1.0, 'varh': 1.0},
                     {'h': -1195.0, 'vara': 0.0004542, 'varb': 1.7491,
                      'varh': -1529.0},
                     {'h': -1004.0, 'vara': 0.0001926, 'varb': 1.9167,
                      'varh': -1510.0},
                     {'h': -554.0, 'vara': 0.004863, 'varb': 1.4757,
                      'varh': -1368.0},
                     {'h': 702.0, 'vara': 0.004886, 'varb': 1.4706,
                      'varh': -1385.0},
                     {'h': 2393.0, 'vara': 0.000218, 'varb': 1.8376,
                      'varh': -1761.1},
                     {'h': 3628.0, 'vara': 0.002174, 'varb': 1.6179,
                      'varh': -715.6},
                     {'h': 5638.0, 'vara': 0.001281, 'varb': 1.6926,
                      'varh': -474.0}]},
             '10017':
                 {'code': '10017',
                  'typect': 4,
                  'libelle': 'H201620',
                  'limiteinf': -1360.0,
                  'limitesup': 5240.0,
                  'station': 'K055001010',
                  'dtmaj': dt(2017, 1, 13, 13, 53, 15),
                  'periodes': [(dt(2016, 11, 23, 2, 30),
                                dt(2020, 1, 1, 0, 0))],
                  'pivots': [{'h': -1438.0, 'vara': 1.0, 'varb': 1.0,
                              'varh': 1.0},
                             {'h': -1006.0, 'vara': 0.0002319, 'varb': 1.9167,
                              'varh': -1438.0},
                             {'h': -554.0, 'vara': 0.005271, 'varb': 1.4757,
                              'varh': -1325.0},
                             {'h': 702.0, 'vara': 0.004886, 'varb': 1.4706,
                              'varh': -1385.0},
                             {'h': 2393.0, 'vara': 0.000218, 'varb': 1.8376,
                              'varh': -1761.1},
                             {'h': 3628.0, 'vara': 0.002174, 'varb': 1.6179,
                              'varh': -715.6},
                             {'h': 5638.0, 'vara': 0.001281, 'varb': 1.6926,
                              'varh': -474.0}]}}
        for s in content.courbestarage:
            c = s.code
            self.assertIn(c, valid)
            self.assertEqual(s.code, valid[c]['code'])
            self.assertEqual(s.typect, valid[c]['typect'])
            self.assertEqual(s.libelle, valid[c]['libelle'])
            self.assertEqual(s.limiteinf, valid[c]['limiteinf'])
            self.assertEqual(s.limitesup, valid[c]['limitesup'])
            self.assertEqual(s.station.code, valid[c]['station'])
            self.assertEqual(s.dtmaj, valid[c]['dtmaj'])
            for k, p in enumerate(s.pivots):
                self.assertEqual(p.hauteur, valid[c]['pivots'][k]['h'])
                self.assertEqual(p.vara, valid[c]['pivots'][k]['vara'])
                self.assertEqual(p.varb, valid[c]['pivots'][k]['varb'])
                self.assertEqual(p.varh, valid[c]['pivots'][k]['varh'])
            for k, p in enumerate(s.periodes):
                self.assertEqual(p.dtdeb, valid[c]['periodes'][k][0])
                self.assertEqual(p.dtfin, valid[c]['periodes'][k][1])
        # =====================================================================

    def test_read_user(self):
        """
        Test read_user
        """
        # =====================================================================
        filename = os.path.join(self.dirname, '404_user_admin.xml')
        sandre = Sandre(filename=filename)
        content = sandre.read()
        valid = \
            {'1111': {
                'code': '1111',
                'origine': 'SANDRE',
                'nom': 'Service de Prévision des Crues (SPC) X-Y-Z',
                'mnemo': 'SPC XYZ',
                'contacts': [
                    {
                        'code': '404',
                        'nom': 'X',
                        'prenom': 'Monsieur',
                        'mel': 'monsieur.x@x.fr',
                     },
                ]
            }}
        for i in content.intervenants:
            c = i.code
            self.assertIn(c, valid)
            self.assertEqual(i.code, valid[c]['code'])
            self.assertEqual(i.origine, valid[c]['origine'])
            self.assertEqual(i.mnemo, valid[c]['mnemo'])
            for k, p in enumerate(i.contacts):
                self.assertEqual(p.code, valid[c]['contacts'][k]['code'])
                self.assertEqual(p.nom, valid[c]['contacts'][k]['nom'])
                self.assertEqual(p.prenom, valid[c]['contacts'][k]['prenom'])
                self.assertEqual(p.mel, valid[c]['contacts'][k]['mel'])
        # =====================================================================
#        filename = os.path.join(self.dirname, '404_user_loc_meteo.xml')
#        sandre = Sandre(filename=filename)
#        content = sandre.read()
#        filename = os.path.join(self.dirname, '404_user_site_hydro.xml')
#        sandre = Sandre(filename=filename)
#        content = sandre.read()
#        filename = os.path.join(self.dirname, '404_user_station_hydro.xml')
#        sandre = Sandre(filename=filename)
#        content = sandre.read()
        # =====================================================================

    def test_types(self):
        """
        Test des types de fichier XML
        """
        valid = ['loc_hydro', 'loc_hydro_child', 'loc_meteo',
                 'ratingcurve', 'user']
        self.assertEqual(valid, Sandre.get_types())
