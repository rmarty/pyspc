#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for binary duplicateScoresCfg

To run all tests just type:
    python -m unittest test_bin_duplicateScoresCfg

To run only a class test:
    python -m unittest test_bin_duplicateScoresCfg.Test_duplicateScoresCfg

To run only a specific test:
    python -m unittest test_bin_duplicateScoresCfg.Test_duplicateScoresCfg.test_update

"""
# Imports
import filecmp
import os
import subprocess
import sys
import unittest

from pyspc.binutils.args import duplicateScoresCfg as _args
from pyspc.binutils.captured_output import captured_output
from pyspc.binutils.rst.append_examples import append_examples, init_examples


class Test_duplicateScoresCfg(unittest.TestCase):
    """
    duplicateScoresCfg bin test
    """
    @classmethod
    def setUpClass(cls):
        """
        Function called once before running all test methods
        """
        # =====================================================================
        cls.dirname = os.path.join('data', '_bin', 'duplicateScoresCfg')
        cls.dir_in = os.path.join(cls.dirname, 'in')
        cls.dir_out = os.path.join(cls.dirname, 'out')
        cls.dir_cfg = os.path.join(cls.dirname, 'cfg')
        cls.dir_ctl = os.path.join(cls.dirname, 'ctl')
        cls.dir_scores = os.path.join('data', 'verification', 'scores')
        if not os.path.exists(cls.dir_out):
            os.makedirs(cls.dir_out)
        # =====================================================================
        cls.rst_examples_init = True  # True, False, None
        cls.rst_filename = os.path.join(
            cls.dirname, os.path.basename(cls.dirname) + '_example.rst')
        init_examples(filename=cls.rst_filename, test=cls.rst_examples_init)
        # =====================================================================

    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.cline = None
        self.label = None

    def tearDown(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        append_examples(filename=self.rst_filename, cline=self.cline,
                        label=self.label, test=self.rst_examples_init)

    def test_update_0(self):
        """
        Test Mise à jour config SCORES
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/duplicateScoresCfg.py',
            '-I', self.dir_scores,
            '-D', 'config_defaut.cfg',
            '-O', self.dir_out,
            '-c', 'config_0.txt'
        ]
        # =====================================================================
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = cline.split(' ')
        with captured_output():
            args = _args.duplicateScoresCfg()
        self.assertEqual(args.cfg_filename, 'config_0.txt')
        self.assertEqual(args.duplicata_src, 'config_defaut.cfg')
        self.assertEqual(args.input_dir, self.dir_scores)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.update_cfg, None)
#        self.assertEqual(args.verbose, True)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        filename = 'config_0.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
#        print(cline)
        self.cline = cline
        self.label = "Mise à jour de la configuration Scores {c} dans le "\
            "répertoire {O} à partir du fichier 'source' {D} situé dans le "\
            "répertoire {I}. Puisque l'option U n'est pas défini, cela "\
            "revient à copier le fichier source".format(
                I=args.input_dir, D=args.duplicata_src, O=args.output_dir,
                c=args.cfg_filename)
        # =====================================================================

    def test_update_1(self):
        """
        Test Mise à jour config SCORES - Avec options U
        """
        # =====================================================================
        processArgs = [
            'python',
            '../bin/duplicateScoresCfg.py',
            '-I', self.dir_ctl,
            '-D', 'config_0.txt',
            '-O', self.dir_out,
            '-c', 'config_1.txt',
            '-U', 'GENERAL', 'ENTITE', 'K5383020',
            '-U', 'GENERAL', 'GRANDEUR', 'Q',
            '-U', 'OBSERVATION', 'DONNEES_OBSERVATION',
            '"obs/fichier_1.csv, obs/fichier_2.csv"',
            '-U', 'PREVISION', 'DONNEES_PREVISION',
            '"fcst/fichier_1.csv, fcst/fichier_2.csv"',
            '-U', 'PREVISION', 'LISTE_MODELE', '45gGRPd000',
            '-U', 'PREVISION', 'LISTE_ECHEANCE', '"0, 6, 12, 24, 48, 72"',
            '-U', 'EXPORT', 'DOSSIER_EXPORT_PREVISION_ECHEANCE_FIXE',
            'output/fcst_ltime',
            '-U', 'CALCUL', 'FICHIER_RESULTAT', 'output/results.xml',
            '-U', 'CALCUL', 'LISTE_GROUPES_SCORES', '"1, 3, 7"'
        ]
        # =====================================================================
        valids = [
            ['GENERAL', 'ENTITE', 'K5383020'],
            ['GENERAL', 'GRANDEUR', 'Q'],
            ['OBSERVATION', 'DONNEES_OBSERVATION',
             '"obs/fichier_1.csv, obs/fichier_2.csv"'],
            ['PREVISION', 'DONNEES_PREVISION',
             '"fcst/fichier_1.csv, fcst/fichier_2.csv"'],
            ['PREVISION', 'LISTE_MODELE', '45gGRPd000'],
            ['PREVISION', 'LISTE_ECHEANCE', '"0, 6, 12, 24, 48, 72"'],
            ['EXPORT', 'DOSSIER_EXPORT_PREVISION_ECHEANCE_FIXE',
             'output/fcst_ltime'],
            ['CALCUL', 'FICHIER_RESULTAT', 'output/results.xml'],
            ['CALCUL', 'LISTE_GROUPES_SCORES', '"1, 3, 7"']
        ]
        cline = ' '.join(processArgs).replace('python', '').strip()
        sys.argv = processArgs[1:]
        with captured_output():
            args = _args.duplicateScoresCfg()
        self.assertEqual(args.cfg_filename, 'config_1.txt')
        self.assertEqual(args.duplicata_src, 'config_0.txt')
        self.assertEqual(args.input_dir, self.dir_ctl)
        self.assertEqual(args.output_dir, self.dir_out)
        self.assertEqual(args.update_cfg, valids)
        # =====================================================================
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=sys.stdout, stderr=sys.stderr)  # nosec
        processRun.wait()
        filename = 'config_1.txt'
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dir_out, filename),
            os.path.join(self.dir_ctl, filename),
        ))
        os.remove(os.path.join(self.dir_out, filename))
        # =====================================================================
        self.cline = cline
        self.label = "Mise à jour de la configuration Scores {c} dans le "\
            "répertoire {O} à partir du fichier 'source' {D} situé dans le "\
            "répertoire {I}. Celle-ci concerne les sections/options "\
            "suivantes : ".format(
                I=args.input_dir, D=args.duplicata_src, O=args.output_dir,
                c=args.cfg_filename)
        self.label += ', '.join(["({0}, {1}) -> {2}".format(*v)
                                 for v in args.update_cfg])
        # =====================================================================
