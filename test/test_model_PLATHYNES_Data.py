#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Data in pyspc.model.plathynes

To run all tests just type:
    python -m unittest test_model_PLATHYNES_Data

To run only a class test:
    python -m unittest test_model_PLATHYNES_Data.TestPLATHYNES_Data

To run only a specific test:
    python -m unittest test_model_PLATHYNES_Data.TestPLATHYNES_Data.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import numpy as np
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import filecmp
from unittest import mock
import unittest

# Imports pyspc
from pyspc.model.plathynes import Data


class TestPLATHYNES_Data(unittest.TestCase):
    """
    PLATHYNES_Data class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'plathynes')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'PLATHYNES_Data_1.mqo')
        reader = Data(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'Q')
        self.assertFalse(reader.injection)
        # =====================================================================
        filename = os.path.join(self.dirname, 'PLATHYNES_Data_1.mqi')
        reader = Data(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'Q')
        self.assertTrue(reader.injection)
        # =====================================================================
        filename = os.path.join(self.dirname, 'PLATHYNES_Data_1.mgr')
        reader = Data(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.varname, 'P')
        self.assertFalse(reader.injection)
        # =====================================================================

    @mock.patch('pyspc.model.plathynes.Data.read_mgr')
    @mock.patch('pyspc.model.plathynes.Data.read_mqoi')
    def test_read(self, mock_read_mqoi, mock_read_mgr):
        """
        Test lecture
        """
        # =====================================================================
        filename = os.path.join(self.dirname, '8001_RRobs.mgr')
        reader = Data(filename=filename)
        reader.read()
        mock_read_mgr.assert_called()
        mock_read_mqoi.assert_not_called()
        # =====================================================================
        mock_read_mgr.reset_mock()
        mock_read_mqoi.reset_mock()
        # =====================================================================
        filename = os.path.join(self.dirname, '8001_1.mqo')
        reader = Data(filename=filename)
        reader.read()
        mock_read_mgr.assert_not_called()
        mock_read_mqoi.assert_called()
        # =====================================================================

    def test_read_mgr(self):
        """
        Test de la lecture des fichiers .mgr
        """
        # =====================================================================
        valid_meta = {
            'Type de donnees': 'PLUVIO',
            'Station': 'PROJECT_SET',
            'Pas de temps': td(hours=1),
            'Facteur multiplicatif': 1
        }
        valid_sta = {
            'LE_PUY_CHADRAC':
                {'x': 722740.000000,
                 'y': 2007270.000000,
                 'code': 'LE_PUY_CHADRAC'},
            'MAZAN_ABBAYE_RAD':
                {'x': 738420.000000,
                 'y': 1971872.000000,
                 'code': 'MAZAN_ABBAYE_RAD'},
            'CAYRES':
                {'x': 716010.000000,
                 'y': 1992118.000000,
                 'code': 'CAYRES'},
        }
        valid_data = pnd.DataFrame({
            'LE_PUY_CHADRAC': [0.0, 14.0, 20.0, 28.0, 38.0, 42.0, 52.0],
            'MAZAN_ABBAYE_RAD': [50.0, 68.0, 74.0, 82.0, 88.0, 132.0, 107.0],
            'CAYRES': [18.0, 36.0, 42.0, 68.0, 62.0, 48.0, 74.0]},
            index=pnd.date_range(
                    dt(2019, 11, 23, 0),
                    dt(2019, 11, 23, 6),
                    freq='H'
                )
        )
        # =====================================================================
        filename = os.path.join(self.dirname, '8001_RRobs.mgr')
        reader = Data(filename=filename)
        data, meta = reader.read_mgr()
        # =====================================================================
        for k, v in valid_meta.items():
            self.assertIn(k, meta)
            self.assertEqual(meta[k], v)
        self.assertIn('locs', meta)
        self.assertEqual(len(meta['locs'].keys()), 3)
        for k, v in valid_sta.items():
            self.assertIn(k, meta['locs'])
            self.assertDictEqual(meta['locs'][k], v)
        assert_frame_equal(valid_data, data)
        # =====================================================================

    def test_read_mhi(self):
        """
        Test de la lecture des fichiers .mhi
        """
        # =====================================================================
        valid_meta = {
            'loc': 'Senechas',
            'loc0': 'Senechas',
            'x': '0.0',
            'y': '0.0',
            'obj': 'Hobs',
            'value': '0.0'
        }
        valid_data = pnd.DataFrame({
            'Senechas': [244.72, 244.72]},
            index=[dt(2019, 11, 22, 11, 30), dt(2019, 11, 22, 12)]
        )
        # =====================================================================
        filename = os.path.join(self.dirname, '11_2029_1.mhi')
        reader = Data(filename=filename)
        data, meta = reader.read_mqoi()
        # =====================================================================
        self.assertDictEqual(meta, valid_meta)
        assert_frame_equal(valid_data, data)
        # =====================================================================

    def test_read_mqi(self):
        """
        Test de la lecture des fichiers .mqi
        """
        # =====================================================================
        valid_meta = {
            'loc': 'Pont_la_Borie',
            'loc0': 'Pont_la_Borie',
            'x': '735667.0',
            'y': '1981483.0',
            'obj': 'Qobs',
            'value': '0.000'
        }
        valid_data = pnd.DataFrame({
            'Pont_la_Borie': [22.900, 27.500, 32.500, 39.300, 48.200, 58.000,
                              144.000, 157.000, 158.000, 353.000, 347.000,
                              306.000, 281.000, 230.000, 187.000, 158.000,
                              136.000, 120.000, 107.000]},
            index=pnd.date_range(
                    dt(2019, 11, 23, 0),
                    dt(2019, 11, 23, 18),
                    freq='H'
                )
        )
        # =====================================================================
        filename = os.path.join(self.dirname, '8001_1.mqi')
        reader = Data(filename=filename)
        data, meta = reader.read_mqoi()
        # =====================================================================
        self.assertDictEqual(meta, valid_meta)
        assert_frame_equal(valid_data, data)
        # =====================================================================

    def test_read_mqo(self):
        """
        Test de la lecture des fichiers .mqo
        """
        # =====================================================================
        valid_meta = {
            'loc': 'LaLoireChadrac',
            'loc0': 'LaLoireChadrac',
            'x': '239.0',
            'y': '2008719.0',
            'obj': 'Qobs',
            'value': '0.000'
        }
        valid_data = pnd.DataFrame({
            'LaLoireChadrac': [71.2, 81.3, 90.7, 104.0, 122.0, 155.0, 207.0,
                               287.0, 429.0, 582.0, 706.0, 808.0, 842.0, 850.0,
                               888.0, 879.0, 810.0, 751.0, 675.0]},
            index=pnd.date_range(
                    dt(2019, 11, 23, 0),
                    dt(2019, 11, 23, 18),
                    freq='H'
                )
        )
        # =====================================================================
        filename = os.path.join(self.dirname, '8001_1.mqo')
        reader = Data(filename=filename)
        data, meta = reader.read_mqoi()
        # =====================================================================
        self.assertDictEqual(meta, valid_meta)
        assert_frame_equal(valid_data, data)
        # =====================================================================

    @mock.patch('pyspc.model.plathynes.Data.write_mgr')
    @mock.patch('pyspc.model.plathynes.Data.write_mqoi')
    def test_write(self, mock_write_mqoi, mock_write_mgr):
        """
        Test écriture
        """
        # =====================================================================
        valid_meta = {
            'loc': 'Pont_la_Borie',
            'loc0': 'Pont_la_Borie',
            'x': '735667.0',
            'y': '1981483.0',
            'obj': 'Qobs',
            'value': '0.000'
        }
        valid = [
            [dt(2019, 11, 22), 2.040],
            [dt(2019, 11, 22, 12), 2.180],
            [dt(2019, 11, 23, 6), 144.000],
            [dt(2019, 11, 23, 14), 187.000],
            [dt(2019, 11, 24), 63.100],
            [dt(2019, 11, 25, 9), 10.400],
            [dt(2019, 11, 25, 10), np.nan],
            [dt(2019, 11, 25, 12), np.nan],
            [dt(2019, 11, 25, 20), np.nan],
            [dt(2019, 11, 25, 23), np.nan]
        ]
        valid = pnd.DataFrame(
            {'Pont_la_Borie': [v[1] for v in valid]},
            index=[v[0] for v in valid]
        )
        # =====================================================================
        tmp_filename = os.path.join('data', 'test_write.mgr')
        writer = Data(filename=tmp_filename)
        tmp_filename = writer.write(data=valid, meta=valid_meta)
        mock_write_mgr.assert_called()
        mock_write_mqoi.assert_not_called()
        # =====================================================================
        mock_write_mgr.reset_mock()
        mock_write_mqoi.reset_mock()
        # =====================================================================
        filename = os.path.join(self.dirname, 'test_write.mqo')
        writer = Data(filename=filename)
        tmp_filename = writer.write(data=valid, meta=valid_meta)
        mock_write_mgr.assert_not_called()
        mock_write_mqoi.assert_called()
        # =====================================================================

    def test_write_mgr(self):
        """
        Test de l'écriture des fichiers .mgr
        """
        # =====================================================================
        valid_meta = {
            'Type de donnees': 'PLUVIO',
            'Station': 'PROJECT_SET',
            'Pas de temps': td(hours=1),
            'Facteur multiplicatif': 1,
            'locs': {
                'MAZAN_ABBAYE_RAD':
                    {'x': 738420.000000,
                     'y': 1971872.000000,
                     'code': 'MAZAN_ABBAYE_RAD'}
            }
        }
        valid = [
            [dt(2019, 11, 22, 20), 14.00],
            [dt(2019, 11, 23), 18.00],
            [dt(2019, 11, 23, 6), 70.00],
            [dt(2019, 11, 23, 14), 14.00],
            [dt(2019, 11, 24), 22.00],
            [dt(2019, 11, 25, 9), 0.00],
            [dt(2019, 11, 25, 10), np.nan],
            [dt(2019, 11, 25, 12), np.nan],
            [dt(2019, 11, 25, 20), np.nan],
            [dt(2019, 11, 25, 23), np.nan]
        ]
        valid = pnd.DataFrame(
            {'MAZAN_ABBAYE_RAD': [v[1] for v in valid]},
            index=[v[0] for v in valid]
        )
        # =====================================================================
        filename = os.path.join(self.dirname, 'test_write.mgr')
        tmp_filename = os.path.join('data', 'test_write.mgr')
        writer = Data(filename=tmp_filename)
        tmp_filename2 = writer.write_mgr(data=valid, meta=valid_meta)
        self.assertEqual(tmp_filename2, tmp_filename)
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_filename
        ))
        os.remove(tmp_filename)
        # =====================================================================

    def test_write_mhi(self):
        """
        Test de l'écriture des fichiers .mhi
        """
        # =====================================================================
        valid_meta = {
            'loc': 'Senechas',
            'loc0': 'Senechas',
            'x': '0.0',
            'y': '0.0',
            'obj': 'Hobs',
            'value': '0.0'
        }
        valid = pnd.DataFrame({
            'Senechas': [244.72, 244.72]},
            index=[dt(2019, 11, 22, 11, 30), dt(2019, 11, 22, 12)]
        )
        # =====================================================================
        filename = os.path.join(self.dirname, 'test_write.mhi')
        tmp_filename = os.path.join('data', 'test_write.mhi')
        writer = Data(filename=tmp_filename)
        tmp_filename2 = writer.write_mhoi(data=valid, meta=valid_meta)
        self.assertEqual(tmp_filename2, tmp_filename)
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_filename
        ))
        os.remove(tmp_filename)
        # =====================================================================

    def test_write_mqi(self):
        """
        Test de l'écriture des fichiers .mqi
        """
        # =====================================================================
        valid_meta = {
            'loc': 'Pont_la_Borie',
            'loc0': 'Pont_la_Borie',
            'x': '735667.0',
            'y': '1981483.0',
            'obj': 'Qobs',
            'value': '0.000'
        }
        valid = [
            [dt(2019, 11, 22), 2.040],
            [dt(2019, 11, 22, 12), 2.180],
            [dt(2019, 11, 23, 6), 144.000],
            [dt(2019, 11, 23, 14), 187.000],
            [dt(2019, 11, 24), 63.100],
            [dt(2019, 11, 25, 9), 10.400],
            [dt(2019, 11, 25, 10), np.nan],
            [dt(2019, 11, 25, 12), np.nan],
            [dt(2019, 11, 25, 20), np.nan],
            [dt(2019, 11, 25, 23), np.nan]
        ]
        valid = pnd.DataFrame(
            {'Pont_la_Borie': [v[1] for v in valid]},
            index=[v[0] for v in valid]
        )
        # =====================================================================
        filename = os.path.join(self.dirname, 'test_write.mqi')
        tmp_filename = os.path.join('data', 'test_write.mqi')
        writer = Data(filename=tmp_filename)
        tmp_filename2 = writer.write_mqoi(data=valid, meta=valid_meta)
        self.assertEqual(tmp_filename2, tmp_filename)
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_filename
        ))
        os.remove(tmp_filename)
        # =====================================================================

    def test_write_mqo(self):
        """
        Test de l'écriture des fichiers .mqo
        """
        # =====================================================================
        valid_meta = {
            'loc': 'LaLoireChadrac',
            'loc0': 'LaLoireChadrac',
            'x': '239.0',
            'y': '2008719.0',
            'obj': 'Qobs',
            'value': '0.000'
        }
        valid = [
            [dt(2019, 11, 22), 29.000],
            [dt(2019, 11, 22, 12), 39.700],
            [dt(2019, 11, 23, 6), 207.000],
            [dt(2019, 11, 23, 14), 888.000],
            [dt(2019, 11, 24), 376.000],
            [dt(2019, 11, 25, 9), 113.000],
            [dt(2019, 11, 25, 10), np.nan],
            [dt(2019, 11, 25, 12), np.nan],
            [dt(2019, 11, 25, 20), np.nan],
            [dt(2019, 11, 25, 23), np.nan]
        ]
        valid = pnd.DataFrame(
            {'LaLoireChadrac': [v[1] for v in valid]},
            index=[v[0] for v in valid]
        )
        # =====================================================================
        filename = os.path.join(self.dirname, 'test_write.mqo')
        tmp_filename = os.path.join('data', 'test_write.mqo')
        writer = Data(filename=tmp_filename)
        tmp_filename2 = writer.write_mqoi(data=valid, meta=valid_meta)
        self.assertEqual(tmp_filename2, tmp_filename)
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_filename
        ))
        os.remove(tmp_filename)
        # =====================================================================
