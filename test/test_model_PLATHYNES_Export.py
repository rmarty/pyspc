#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Export in pyspc.model.plathynes

To run all tests just type:
    python -m unittest test_model_PLATHYNES_Export

To run only a class test:
    python -m unittest test_model_PLATHYNES_Export.TestPLATHYNES_Export

To run only a specific test:
    python -m unittest test_model_PLATHYNES_Export.TestPLATHYNES_Export.test_init

"""
# Imports
from datetime import datetime as dt
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.model.plathynes import Export


class TestPLATHYNES_Export(unittest.TestCase):
    """
    PLATHYNES_Export class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'plathynes')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'plathynes_export_1.txt')
        reader = Export(filename=filename)
        self.assertEqual(reader.filename, filename)

    def test_read(self):
        """
        Test de lecture d'un fichier de données QMJ pour PREMHYCE
        """
        Chadrac_2008_11 = pnd.DataFrame({
            'Date': pnd.date_range(
                dt(2008, 11, 1, 18),
                dt(2008, 11, 2, 18),
                freq='H'
            ),
            'RR': [5.18, 5.60, 4.85, 5.72, 6.58, 3.97, 7.41, 8.28, 9.39, 9.08,
                   10.84, 9.81, 10.07, 6.93, 1.97, 0.36, 0.13, 0.15, 0.00,
                   0.02, 0.16, 0.47, 0.39, 1.69, 8.80],
            'Qobs': [37.800, 50.700, 86.300, 139.000, 192.000, 292.000,
                     393.000, 492.000, 592.000, 742.000, 1040.000, 1130.000,
                     1130.000, 1080.000, 1030.000, 976.000, 923.000, 862.000,
                     783.000, 704.000, 627.000, 545.000, 475.000, 432.000,
                     414.000],
            'Qsim': [38.035, 44.906, 53.811, 64.923, 79.728, 99.110, 128.635,
                     167.130, 217.723, 276.985, 347.986, 526.110, 758.124,
                     957.992, 1091.555, 1112.929, 1060.861, 1002.470, 932.845,
                     845.717, 752.635, 660.793, 575.557, 507.451, 459.594],
        })
        valid = [
            ('2008_11', 'LaLoireChadrac'), ('2008_11', 'Goudet'),
            ('2017_06', 'LaLoireChadrac'), ('2017_06', 'Goudet'),
        ]
        filename = os.path.join(self.dirname, 'plathynes_export_1.txt')
        reader = Export(filename=filename)
        data = reader.read()
        self.assertIsInstance(data, dict)
        for k in valid:
            self.assertIn(k, data)
        assert_frame_equal(
            Chadrac_2008_11,
            data[('2008_11', 'LaLoireChadrac')]
        )

    def test_write(self):
        """
        Test d'écriture d'un fichier de données QMJ pour PREMHYCE
        """
        filename = os.path.join('data', 'plathynes_export_1.txt')
        writer = Export(filename=filename)
        with self.assertRaises(NotImplementedError):
            writer.write()
