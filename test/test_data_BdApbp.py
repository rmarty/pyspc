#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for BdApbp in pyspc.data.lamedo

To run all tests just type:
    python -m unittest test_data_BdApbp

To run only a class test:
    python -m unittest test_data_BdApbp.TestBdApbp

To run only a specific test:
    python -m unittest test_data_BdApbp.TestBdApbp.test_init

"""
# Imports
from datetime import datetime as dt
import filecmp
import numpy as np
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.data.lamedo import BdApbp


class TestBdApbp(unittest.TestCase):
    """
    BdApbp class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        # =====================================================================
#        self.dirname = os.path.join('data', 'webservice', 'lamedo')
        self.dirname = os.path.join('data', 'data', 'lamedo')
        # =====================================================================
        self.valid_short = pnd.DataFrame({
            'CODE': sorted(['41003', '41005']*3),
            'NAME': [np.nan]*6,
            'DATE': [dt(2020, 6, 13), dt(2020, 6, 14), dt(2020, 6, 15),
                     dt(2020, 6, 13), dt(2020, 6, 14), dt(2020, 6, 15)],
            'MOY': [850, 110, 0, 1150, 110, 0],
            'DELTA': [np.nan]*6,
            'LOC': [1100, np.nan, np.nan, 2000, np.nan, np.nan],
            'LOCDELTA': [np.nan]*6,
            'DTPROD': [dt(2020, 6, 12, 6)]*6,
        })
        # =====================================================================
        self.valid_long = pnd.DataFrame({
            'CODE': sorted(['41003', '41005']*3),
            'NAME': sorted(['Borne - Ance du Nord', 'Source Loire']*3),
            'DATE': [dt(2020, 6, 13), dt(2020, 6, 14), dt(2020, 6, 15),
                     dt(2020, 6, 13), dt(2020, 6, 14), dt(2020, 6, 15)],
            'MOY': [850, 110, 0, 1150, 110, 0],
            'DELTA': [150, 40, np.nan, 150, 40, np.nan],
            'LOC': [1100, np.nan, np.nan, 2000, np.nan, np.nan],
            'LOCDELTA': [np.nan]*6,
            'DTPROD': [dt(2020, 6, 12, 5, 26)]*6,
        })
        # =====================================================================

    def test_init(self):
        """
        Test initialisation de l'instance
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'bp_short.json')
        bdapbp = BdApbp(filename=filename)
        self.assertEqual(bdapbp.filename, filename)
        # =====================================================================

    def test_read_short(self):
        """
        Test de lecture - Format de retour 'short'
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'bp_short.json')
        bdapbp = BdApbp(filename=filename)
        content = bdapbp.read()
        assert_frame_equal(content, self.valid_short)
        # =====================================================================

    def test_read_long(self):
        """
        Test de lecture - Format de retour 'long'
        """
        # =====================================================================
        filename = os.path.join(self.dirname, 'bp_long.json')
        bdapbp = BdApbp(filename=filename)
        content = bdapbp.read()
        assert_frame_equal(content, self.valid_long)
        # =====================================================================

    def test_write_short(self):
        """
        Test écriture - Format de retour 'short'
        """
        # =====================================================================
        tmpfile = os.path.join('data', 'test_write_short.json')
        dtype = 'short'
        # =====================================================================
        bdapbp = BdApbp(filename=tmpfile)
        bdapbp.write(data=self.valid_short, datatype=dtype)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(tmpfile)),
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_write_long(self):
        """
        Test écriture - Format de retour 'long'
        """
        # =====================================================================
        tmpfile = os.path.join('data', 'test_write_long.json')
        dtype = 'long'
        # =====================================================================
        bdapbp = BdApbp(filename=tmpfile)
        bdapbp.write(data=self.valid_long, datatype=dtype)
        self.assertTrue(filecmp.cmp(
            os.path.join(self.dirname, os.path.basename(tmpfile)),
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_datatype(self):
        """
        Test des types de données ApBp au format JSON
        """
        valid = ['long', 'short']
        self.assertEqual(valid, BdApbp.get_types())
