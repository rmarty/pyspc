#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Results in pyspc.model.plathynes

To run all tests just type:
    python -m unittest test_model_PLATHYNES_Results

To run only a class test:
    python -m unittest test_model_PLATHYNES_Results.TestPLATHYNES_Results

To run only a specific test:
    python -m unittest test_model_PLATHYNES_Results.TestPLATHYNES_Results.test_init

"""
# Imports
from datetime import datetime as dt
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

# Imports pyspc
from pyspc.model.plathynes import Results


class TestPLATHYNES_Results(unittest.TestCase):
    """
    PLATHYNES_Results class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'plathynes')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        filename = os.path.join(self.dirname, 'PLATHYNES_Results_1.txt')
        reader = Results(filename=filename)
        self.assertEqual(reader.filename, filename)

    def test_read(self):
        """
        Test de la lecture des fichiers *ResultsRaw.txt
        """
        # =====================================================================
        valid_meta = {
            'evt': '8001',
            'loc': 'LaLoireChadrac',
            'x': '723755.56',
            'y': '2007188.38'
        }
        valid_data = pnd.DataFrame({
            'Date     Time': pnd.date_range(
                dt(2019, 10, 20, 12),
                dt(2019, 10, 21, 6),
                freq='H'
            ),
            'Rainfall [mm/h]': [5.85262, 6.52331, 7.72051, 2.05037, 4.51873,
                                9.89279, 1.68118, 0.35094, 0.57987, 0.54195,
                                0.21910, 0.33221, 0.11985, 0.05655, 0.02360,
                                0.00000, 0.00000, 0.00000, 0.00000],
            'QSim [m3/s]': [226.858, 306.928, 415.942, 548.677, 731.774,
                            872.356, 967.920, 1157.065, 1248.181, 1252.210,
                            1178.181, 1075.628, 941.526, 811.066, 822.547,
                            747.877, 651.639, 549.495, 457.696],
            'CSim [-]': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                         0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        })
        # =====================================================================
        filename = os.path.join(self.dirname, '8001_ResultsRaw.txt')
        reader = Results(filename=filename)
        data, meta = reader.read()
        self.assertDictEqual(meta, valid_meta)
        assert_frame_equal(valid_data, data)
        # =====================================================================

    def test_write(self):
        """
        Test d'écriture d'un fichier de données QMJ pour PREMHYCE
        """
        filename = os.path.join('data', '8001_ResultsRaw.txt')
        writer = Results(filename=filename)
        with self.assertRaises(NotImplementedError):
            writer.write()
