#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for functions in pyspc.statistics.freq

To run all tests just type:
    python -m unittest test_statistics_freq

To run only a class test:
    python -m unittest test_statistics_freq.TestFreq

To run only a specific test:
    python -m unittest test_statistics_freq.TestFreq.test_toperiod

"""
# Imports
import os
import unittest

from pyspc.statistics.freq import from_rank, to_period, to_ugumbel


class TestFreq(unittest.TestCase):
    """
    statistics class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'metadata', 'statistics')

    def test_fromrank(self):
        """
        Test de la conversion d'un rang en fréquence
        """
        n = 10
        # ====================================================================
        # CAS DEFAUT : HAZEN
        # ====================================================================
        valid = {1: 0.05, 5: 0.45, 10: 0.95}
        for r, v in valid.items():
            self.assertEqual(from_rank(r, n), v)
        # ====================================================================
        # CAS METHODE SPECIFIQUE
        # ====================================================================
        m = 'Gringorten'
        valid = {1: 0.056, 5: 0.451, 10: 0.945}
        for r, v in valid.items():
            self.assertAlmostEqual(from_rank(r, n, method=m), v, places=2)
#            print(">>> from_rank({}, {}, method='{}')".format(r, n, m))
#            print(from_rank(r, n, method=m))
        # ====================================================================
        # CAS A OU B FOURNI
        # ====================================================================
        valids = {
            (0, 0): {1: 0.1, 5: 0.5, 10: 1},
            (0.5, 0): {1: 0.05, 5: 0.45, 10: 0.95},
            (1, 0): {1: 0, 5: 0.4, 10: 0.90},
        }
        for case, valid in valids.items():
            for r, v in valid.items():
                self.assertEqual(from_rank(r, n, a=case[0], b=case[1]), v)
        # ====================================================================
        # ERREURS
        # ====================================================================

    def test_toperiod(self):
        """
        Test de la conversion d'un temps de retour en fréquence
        """
        # ====================================================================
        # CAS HAUTS DEBITS
        # ====================================================================
        valid = {2: 0.5, 5: 0.8, 10: 0.9, 20: 0.95, 50: 0.98,
                 100: 0.99, 200: 0.995, 500: 0.998, 1000: 0.999}
        for t, v in valid.items():
            self.assertEqual(to_period(v), t)
            self.assertEqual(to_period(v, highflow=True), t)
        # ====================================================================
        # CAS BAS DEBITS
        # ====================================================================
        valid = {2: 0.5, 5: 0.2, 10: 0.1, 20: 0.05, 50: 0.02,
                 100: 0.01, 200: 0.005, 500: 0.002, 1000: 0.001}
        for t, v in valid.items():
            self.assertEqual(to_period(v, highflow=False), t)
        # ====================================================================
        # ERREURS
        # ====================================================================
        with self.assertRaises(ValueError):
            to_period(0)
        with self.assertRaises(ValueError):
            to_period(1)
        with self.assertRaises(ValueError):
            to_period('0')
        with self.assertRaises(ValueError):
            to_period(0.5, highflow='a')

    def test_tougumbel(self):
        """
        Test de la conversion d'un temps de retour en fréquence
        """
        # ====================================================================
        # CAS HAUTS DEBITS
        # ====================================================================
        valid = {0.80: 1.50, 0.90: 2.25, 0.95: 2.97,
                 0.98: 3.90, 0.99: 4.60, 0.998: 6.21, 0.999: 6.91}
        for t, v in valid.items():
            self.assertAlmostEqual(to_ugumbel(t), v, places=2)
        # ====================================================================
        # ERREURS
        # ====================================================================
        with self.assertRaises(ValueError):
            to_ugumbel(0)
        with self.assertRaises(ValueError):
            to_ugumbel(1)
        with self.assertRaises(ValueError):
            to_ugumbel('0')
