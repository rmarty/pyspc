#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2022  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP_Verif in pyspc.model.grp22

To run all tests just type:
    python -m unittest test_model_GRP22_CAL_Verif

To run only a class test:
    python -m unittest test_model_GRP22_CAL_Verif.TestGRP_Verif

To run only a specific test:
    python -m unittest test_model_GRP22_CAL_Verif.TestGRP_Verif.test_init

"""
# Imports
import numpy as np
import os
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.model.grp22.cal_verif import GRP_Verif


class TestGRP_Verif(unittest.TestCase):
    """
    GRP_Verif class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'model', 'grp22', 'cal')

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        filename = os.path.join(
            self.dirname,
            'Perf_CALAG_GRP_RH10585x_PDT_00J01H00M_HOR_00J03H00M_'
            'Scal_5d00_Svig_5d00.DAT'
        )
        # =====================================================================
        reader = GRP_Verif(filename=filename)
        self.assertEqual(reader.filename, filename)
        self.assertEqual(reader.datatype, 'rtime')
        self.assertEqual(reader.model, 'GRP')
        self.assertEqual(reader.loc, 'RH10585x')
        self.assertEqual(reader.timestep, '00J01H00M')
        self.assertEqual(reader.leadtime, '00J03H00M')
        self.assertEqual(reader.threshold, '5d00')
        self.assertEqual(reader.threshold_cal, '5d00')
        # =====================================================================

    def test_splitbasename(self):
        """
        Test de la configuration lue à partir d'une ficher de type CAL
        """
        # =====================================================================
        filename = os.path.join(
            self.dirname,
            'Perf_CALAG_GRP_RH10585x_PDT_00J01H00M_HOR_00J03H00M_'
            'Scal_5d00_Svig_5d00.DAT'
        )
        # =====================================================================
        meta = GRP_Verif.split_basename(filename=filename)
        self.assertEqual(meta['datatype'], 'rtime')
        self.assertEqual(meta['model'], 'GRP')
        self.assertEqual(meta['loc'], 'RH10585x')
        self.assertEqual(meta['timestep'], '00J01H00M')
        self.assertEqual(meta['leadtime'], '00J03H00M')
        self.assertEqual(meta['threshold_cal'], '5d00')
        self.assertEqual(meta['threshold'], '5d00')
        # =====================================================================

    def test_read_cal(self):
        """
        Test de la lecture d'une ficher de type CAL
        """
        # =====================================================================
        valids = {
            'AMN_RNA': {'Eff_Cal': 0.844, 'Eff_Val': 0.814,
                        'POD': 78.9, 'FAR': 37.4, 'CSI': 53.6},
            'AMN_TAN': {'Eff_Cal': 0.824, 'Eff_Val': 0.804,
                        'POD': 78.2, 'FAR': 37.9, 'CSI': 53.0},
            'SMN_RNA': {'Eff_Cal': 0.843, 'Eff_Val': 0.806,
                        'POD': 76.7, 'FAR': 39.7, 'CSI': 51.0},
            'SMN_TAN': {'Eff_Cal': 0.813, 'Eff_Val': 0.790,
                        'POD': 77.6, 'FAR': 40.8, 'CSI': 50.6},
        }
        # =====================================================================
        filename = os.path.join(
            self.dirname,
            'Perf_TESTS_GRP_RH10585x_PDT_00J01H00M_HOR_00J03H00M_'
            'Scal_5d00_Svig_5d00.DAT'
        )
        # =====================================================================
        reader = GRP_Verif(filename=filename)
        data = reader.read()
        self.maxDiff = None
        self.assertDictEqual(data, valids)
        # =====================================================================

    def test_read_rtime(self):
        """
        Test de la lecture d'une ficher de type RTIME
        """
        # =====================================================================
        valids = {
            'SMN_TAN': {'Eff_Cal': np.nan, 'Eff_Val': 0.807,
                        'POD': 78.2, 'FAR': 40.3, 'CSI': 51.2},
        }
        # =====================================================================
        filename = os.path.join(
            self.dirname,
            'Perf_CALAG_GRP_RH10585x_PDT_00J01H00M_HOR_00J03H00M_'
            'Scal_5d00_Svig_5d00.DAT'
        )
        # =====================================================================
        reader = GRP_Verif(filename=filename)
        data = reader.read()
        self.assertDictEqual(data, valids)
        # =====================================================================

    def test_datatype(self):
        """
        Test des types de fiche de performance
        """
        valids = ['cal', 'rtime']
        self.assertEqual(GRP_Verif.get_datatypes(), valids)

    def test_concat(self):
        """
        Test concat
        """
        # =====================================================================
        loc = 'K0403010'
        timestep = '00J01H00M'
        datatype = 'rtime'
        # =====================================================================
        valid = pnd.DataFrame({'HOR': ['00J03H00M', '00J03H00M'],
                               'SC': [1.83, 1.83],
                               'SA_RT': ['SMN_RNA', 'SMN_RNA'],
                               'SV': [41.0, 82.0],
                               'Eff_Cal': [np.nan, np.nan],
                               'Eff_Val': [0.89, 0.91],
                               'POD': [75.0, 58.6],
                               'FAR': [30.2, 0.0],
                               'CSI': [56.6, 58.6]})
        # =====================================================================
        df = GRP_Verif.concat(loc=loc, timestep=timestep, dirname=self.dirname,
                              datatype=datatype)
        assert_frame_equal(valid, df)
        # =====================================================================
