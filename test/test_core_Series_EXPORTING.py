#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Series in pyspc.core.series

To run all tests just type:
    python -m unittest test_core_Series_INIT

To run only a class test:
    python -m unittest test_core_Series_INIT.TestSeries

To run only a specific test:
    python -m unittest test_core_Series_INIT.TestSeries.test_init

"""
# Imports
from datetime import datetime as dt, timedelta as td
import filecmp
import os.path
import pandas as pnd
import shutil
import unittest
import warnings

# Imports pyspc
from pyspc.core.serie import Serie
from pyspc.core.series import Series


def check_files(test, tmpfiles, dirname):
    """Contrôler les fichiers"""
    for f in tmpfiles:
        test.assertTrue(filecmp.cmp(
            os.path.join(dirname, os.path.basename(f)),
            f
        ))
        os.remove(f)


class TestSeries(unittest.TestCase):
    """
    Series class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        warnings.filterwarnings("ignore", message="numpy.dtype size changed")
        warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
        warnings.filterwarnings("ignore", category=FutureWarning)
        # =====================================================================
        # OBSERVATION QH
        # ---------------------------------------------------------------------
        serie1 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500, 86.400, 82.300, 78.300],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie1.columns = [('K0000000', 'QH')]
        self.serie1 = Serie(datval=serie1, code='K0000000', varname='QH')
        serie2 = pnd.DataFrame(
            [10, 12, 14, 17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie2.columns = [('K9999999', 'QH')]
        self.serie2 = Serie(datval=serie2, code='K9999999', varname='QH')
        serie3 = pnd.DataFrame(
            [17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 3), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie3.columns = [('K3333333', 'QH')]
        self.serie3 = Serie(datval=serie3, code='K3333333', varname='QH')
        serie4 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 9), freq=td(hours=1)
            )
        )
        serie4.columns = [('K4444444', 'QH')]
        self.serie4 = Serie(datval=serie4, code='K4444444', varname='QH')
        # =====================================================================
        # OBSERVATION QI
        # ---------------------------------------------------------------------
        serie1 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500, 86.400, 82.300, 78.300],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie1.columns = [('K0000000', 'QI')]
        self.serie1i = Serie(datval=serie1, code='K0000000', varname='QI')
        serie2 = pnd.DataFrame(
            [10, 12, 14, 17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie2.columns = [('K9999999', 'QI')]
        self.serie2i = Serie(datval=serie2, code='K9999999', varname='QI')
        serie3 = pnd.DataFrame(
            [17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 3), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        serie3.columns = [('K3333333', 'QI')]
        self.serie3i = Serie(datval=serie3, code='K3333333', varname='QI')
        serie4 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 9), freq=td(hours=1)
            )
        )
        serie4.columns = [('K4444444', 'QI')]
        self.serie4i = Serie(datval=serie4, code='K4444444', varname='QI')
        # =====================================================================
        # PREVISION QH
        # ---------------------------------------------------------------------
        fcst_15 = pnd.DataFrame(
            [35.600, 41.200, 46.900, 62.900, 85.700, 106.000, 118.000, 115.000,
             103.000, 92.500, 86.400, 82.300, 78.300],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        fcst_11 = fcst_15 * 0.9
        fcst_19 = fcst_15 * 1.1
        self.fcst_11 = Serie(datval=fcst_11, code='K0000000', varname='QH')
        self.fcst_15 = Serie(datval=fcst_15, code='K0000000', varname='QH')
        self.fcst_19 = Serie(datval=fcst_19, code='K0000000', varname='QH')
        fcst_25 = pnd.DataFrame(
            [10, 12, 14, 17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        self.fcst_25 = Serie(datval=fcst_25, code='K0000000', varname='QH')
        fcst_35 = pnd.DataFrame(
            [17, 21, 24, 28, 31, 34, 37, 39, 41, 42],
            index=pnd.date_range(
                dt(2014, 11, 4, 3), dt(2014, 11, 4, 12), freq=td(hours=1)
            )
        )
        self.fcst_35 = Serie(datval=fcst_35, code='K9999999', varname='QH')
        # =====================================================================
        # OBSERVATION PH
        # ---------------------------------------------------------------------
        serie_PH1 = pnd.DataFrame(
            [0.0, 0.0, 0.0, 0.0, 3.6, 37.0, 2.5, 23.5, 71.0, 0.0],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 9), freq=td(hours=1)
            ))
        serie_PH1.columns = [('07075001', 'PH')]
        self.serie_PH1 = Serie(datval=serie_PH1, code='07075001', varname='PH')
        serie_PH2 = pnd.DataFrame(
            [0.2, 0.1, 0.0, 0.2, 2.2, 28.8, 1.6, 26.7, 65.7, 0.1],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 9), freq=td(hours=1)
            ))
        serie_PH2.columns = [('07105001', 'PH')]
        self.serie_PH2 = Serie(datval=serie_PH2, code='07105001', varname='PH')
        serie_PH3 = pnd.DataFrame(
            [0.0, 0.0, 0.0, 0.0, 4.6, 42.2, 2.6, 30.0, 74.3, 1.0],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 9), freq=td(hours=1)
            ))
        serie_PH3.columns = [('07154005', 'PH')]
        self.serie_PH3 = Serie(datval=serie_PH3, code='07154005', varname='PH')
        # =====================================================================
        # OBSERVATION TH
        # ---------------------------------------------------------------------
        serie_TH1 = pnd.DataFrame(
            [0.0, 0.0, 0.0, 0.0, 3.6, 37.0, 2.5, 23.5, 71.0, 0.0],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 9), freq=td(hours=1)
            ))
        serie_TH1.columns = [('07075001', 'TH')]
        self.serie_TH1 = Serie(datval=serie_TH1, code='07075001', varname='TH')
        serie_TH2 = pnd.DataFrame(
            [0.2, 0.1, 0.0, 0.2, 2.2, 28.8, 1.6, 26.7, 65.7, 0.1],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 9), freq=td(hours=1)
            ))
        serie_TH2.columns = [('07105001', 'TH')]
        self.serie_TH2 = Serie(datval=serie_TH2, code='07105001', varname='TH')
        serie_TH3 = pnd.DataFrame(
            [0.0, 0.0, 0.0, 0.0, 4.6, 42.2, 2.6, 30.0, 74.3, 1.0],
            index=pnd.date_range(
                dt(2014, 11, 4, 0), dt(2014, 11, 4, 9), freq=td(hours=1)
            ))
        serie_TH3.columns = [('07154005', 'TH')]
        self.serie_TH3 = Serie(datval=serie_TH3, code='07154005', varname='TH')
        # =====================================================================
        # OBSERVATION PJ
        # ---------------------------------------------------------------------
        serie_PJ1 = pnd.DataFrame(
            [0.0, 0.0, 0.0, 0.0, 3.6, 37.0, 2.5, 23.5, 71.0, 0.0],
            index=pnd.date_range(dt(2013, 5, 10), dt(2013, 5, 19), freq='D'))
        serie_PJ1.columns = [('07075001', 'PJ')]
        self.serie_PJ1 = Serie(datval=serie_PJ1, code='07075001', varname='PJ')
        serie_PJ2 = pnd.DataFrame(
            [0.2, 0.1, 0.0, 0.2, 2.2, 28.8, 1.6, 26.7, 65.7, 0.1],
            index=pnd.date_range(dt(2013, 5, 10), dt(2013, 5, 19), freq='D'))
        serie_PJ2.columns = [('07105001', 'PJ')]
        self.serie_PJ2 = Serie(datval=serie_PJ2, code='07105001', varname='PJ')
        serie_PJ3 = pnd.DataFrame(
            [0.0, 0.0, 0.0, 0.0, 4.6, 42.2, 2.6, 30.0, 74.3, 1.0],
            index=pnd.date_range(dt(2013, 5, 10), dt(2013, 5, 19), freq='D'))
        serie_PJ3.columns = [('07154005', 'PJ')]
        self.serie_PJ3 = Serie(datval=serie_PJ3, code='07154005', varname='PJ')
        # =====================================================================
        # PREVISION PJ
        # ---------------------------------------------------------------------
        fcst_PJ1 = pnd.DataFrame(
            [37.0, 2.5, 23.5, 71.0, 0.0],
            index=pnd.date_range(dt(2013, 5, 15), dt(2013, 5, 19), freq='D'))
        fcst_PJ1.columns = [('41005', 'PJ')]
        self.fcst_PJ1 = Serie(datval=fcst_PJ1, code='41005', varname='PJ')
        fcst_PJ2 = pnd.DataFrame(
            [28.8, 1.6, 26.7, 65.7, 0.1],
            index=pnd.date_range(dt(2013, 5, 15), dt(2013, 5, 19), freq='D'))
        fcst_PJ2.columns = [('41005', 'PJ')]
        self.fcst_PJ2 = Serie(datval=fcst_PJ2, code='41005', varname='PJ')
        fcst_PJ3 = pnd.DataFrame(
            [42.2, 2.6, 30.0, 74.3, 1.0],
            index=pnd.date_range(dt(2013, 5, 15), dt(2013, 5, 19), freq='D'))
        fcst_PJ3.columns = [('41005', 'PJ')]
        self.fcst_PJ3 = Serie(datval=fcst_PJ3, code='41005', varname='PJ')
        # =====================================================================
        # PREVISION PH
        # ---------------------------------------------------------------------
        fcst_PH1 = pnd.DataFrame(
            [37.0, 2.5, 23.5, 71.0, 0.0],
            index=pnd.date_range(
                dt(2014, 11, 4, 12), dt(2014, 11, 4, 16), freq='H'))
        fcst_PH1.columns = [('07075001', 'PH')]
        self.fcst_PH1 = Serie(datval=fcst_PH1, code='07075001', varname='PH')
        fcst_PH2 = pnd.DataFrame(
            [28.8, 1.6, 26.7, 65.7, 0.1],
            index=pnd.date_range(
                dt(2014, 11, 4, 12), dt(2014, 11, 4, 16), freq='H'))
        fcst_PH2.columns = [('07105001', 'PH')]
        self.fcst_PH2 = Serie(datval=fcst_PH2, code='07105001', varname='PH')
        fcst_PH3 = pnd.DataFrame(
            [42.2, 2.6, 30.0, 74.3, 1.0],
            index=pnd.date_range(
                dt(2014, 11, 4, 12), dt(2014, 11, 4, 16), freq='H'))
        fcst_PH3.columns = [('07154005', 'PH')]
        self.fcst_PH3 = Serie(datval=fcst_PH3, code='07154005', varname='PH')
        # =====================================================================
        # PREVISION PH
        # ---------------------------------------------------------------------
        fcst_TH1 = pnd.DataFrame(
            [37.0, 2.5, 23.5, 71.0, 0.0],
            index=pnd.date_range(
                dt(2014, 11, 4, 12), dt(2014, 11, 4, 16), freq='H'))
        fcst_TH1.columns = [('07075001', 'TH')]
        self.fcst_TH1 = Serie(datval=fcst_TH1, code='07075001', varname='TH')
        fcst_TH2 = pnd.DataFrame(
            [28.8, 1.6, 26.7, 65.7, 0.1],
            index=pnd.date_range(
                dt(2014, 11, 4, 12), dt(2014, 11, 4, 16), freq='H'))
        fcst_TH2.columns = [('07105001', 'TH')]
        self.fcst_TH2 = Serie(datval=fcst_TH2, code='07105001', varname='TH')
        fcst_TH3 = pnd.DataFrame(
            [42.2, 2.6, 30.0, 74.3, 1.0],
            index=pnd.date_range(
                dt(2014, 11, 4, 12), dt(2014, 11, 4, 16), freq='H'))
        fcst_TH3.columns = [('07154005', 'TH')]
        self.fcst_TH3 = Serie(datval=fcst_TH3, code='07154005', varname='TH')
        # =====================================================================

    def test_csv(self):
        """
        Test export csv
        """
        # =====================================================================
        series = Series(datatype='obs', name='test_csv_obs')
        series.add(serie=self.serie1)
        series.add(serie=self.serie2)
        series.add(serie=self.serie3)
        series.add(serie=self.serie4, meta='sim')
        tmpfile = series.to_csv(dirname='data')
        filename = os.path.join('data', 'io', 'csv', os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(filename, tmpfile))
        os.remove(tmpfile)
        # =====================================================================
        series = Series(datatype='fcst', name='test_csv_fcst')
        series.add(serie=self.fcst_PJ1,
                   meta=(dt(2013, 5, 15), 'foo', 'MoyInf'))
        series.add(serie=self.fcst_PJ2,
                   meta=(dt(2013, 5, 15), 'foo', 'MoySup'))
        series.add(serie=self.fcst_PJ3,
                   meta=(dt(2013, 5, 15), 'foo', 'LocInf'))
        series.add(serie=self.fcst_PJ1,
                   meta=(dt(2013, 5, 15), 'bar', 'MoyInf'))
        series.add(serie=self.fcst_PJ3,
                   meta=(dt(2013, 5, 15), 'bar', 'MoySup'))
        tmpfile = series.to_csv(dirname='data')
        filename = os.path.join('data', 'io', 'csv', os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(filename, tmpfile))
        os.remove(tmpfile)
        # =====================================================================

    def test_BdApbp(self):
        """
        Test export BdApbp
        """
        # =====================================================================
        series = Series(datatype='fcst', name='test_BdApbp')
        series.add(serie=self.fcst_PJ1,
                   meta=(dt(2013, 5, 15), 'foo', 'MoyInf'))
        series.add(serie=self.fcst_PJ2,
                   meta=(dt(2013, 5, 15), 'foo', 'MoySup'))
        series.add(serie=self.fcst_PJ3,
                   meta=(dt(2013, 5, 15), 'foo', 'LocInf'))
        series.add(serie=self.fcst_PJ1,
                   meta=(dt(2013, 5, 15), 'bar', 'MoyInf'))
        series.add(serie=self.fcst_PJ3,
                   meta=(dt(2013, 5, 15), 'bar', 'MoySup'))
        # =====================================================================
        tmpfiles = series.to_BdApbp(dirname='data', datatype='long')
        check_files(self, tmpfiles, os.path.join('data', 'io', 'lamedo'))
        # =====================================================================

    def test_GRP16(self):
        """
        Test export GRP16
        """
        # =====================================================================
        # GRP_Data : Cas AVEC mise-à-jour des données existantes
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='test_series_Q')
        shutil.copy2(
            os.path.join('data', 'io', 'grp16', 'K0000000_Q_ini.txt'),
            os.path.join('data', 'K0000000_Q.txt'),
        )
        series.add(serie=self.serie1)
        series.add(serie=self.serie2)
        series.add(serie=self.serie3)
        series.add(serie=self.serie4, meta='sim')
        tmpfiles = series.to_GRP_Data(dirname='data', how='overwrite')
        self.assertListEqual(
            tmpfiles,
            ['data\\K0000000_Q.txt', 'data\\K9999999_Q.txt',
             'data\\K3333333_Q.txt']
        )
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp16'))
        # =====================================================================
        # GRP_Data : Cas SANS mise-à-jour des données existantes
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp16')
        series.add(serie=self.serie2)
        shutil.copy2(
            os.path.join('data', 'io', 'grp16', 'K9999999_Q_ini.txt'),
            os.path.join('data', 'K9999999_Q.txt'),
        )
        tmpfiles = series.to_GRP_Data(dirname='data', how='replace')
        self.assertListEqual(tmpfiles, ['data\\K9999999_Q.txt'])
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp16'))
        # =====================================================================
        # GRP_Data : Cas SANS séries adaptées
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp16')
        series.add(serie=self.serie_PJ1)
        series.add(serie=self.serie_PJ2)
        series.add(serie=self.serie_PJ3)
        tmpfiles = series.to_GRP_Data(dirname='data')
        self.assertEqual(len(tmpfiles), 0)
        # =====================================================================
        # GRP_RT_Data : Cas séries OBSERVATION
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp16')
        series.add(serie=self.serie1)
        series.add(serie=self.serie2)
        series.add(serie=self.serie3)
        series.add(serie=self.serie4, meta='sim')
        series.add(serie=self.serie_PH1)
        series.add(serie=self.serie_PH2)
        series.add(serie=self.serie_PH3)
        tmpfiles = series.to_GRPRT_Data(dirname='data')
        self.assertListEqual(
            sorted(tmpfiles),
            ['data\\Debit_grp16.txt', 'data\\Pluie_grp16.txt']
        )
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp16'))
        # =====================================================================
        # GRP_RT_Data : Cas séries PREVISION METEOROLOGIQUE
        # ---------------------------------------------------------------------
        series = Series(datatype='fcst', name='grp16')
        series.add(serie=self.fcst_PH1,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        series.add(serie=self.fcst_PH2,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        series.add(serie=self.fcst_PH3,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2002'))
        tmpfiles = series.to_GRPRT_Data(dirname='data')
        self.assertListEqual(
            sorted(tmpfiles),
            ['data\\Scen_001_Plugrp16.txt', 'data\\Scen_002_Plugrp16.txt']
        )
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp16'))
        # =====================================================================
        # =====================================================================
        # GRP_RT_Archive : Cas des archives [P,Q,T]V_10A.DAT
        # ---------------------------------------------------------------------
        out_dirname = os.path.join('data', 'io', 'grp16', 'rt_archive')
        ctl_dirname = os.path.join('data', 'io', 'grp16', 'rt_archive_ctl')
        series = Series(datatype='obs', name='grp16')
        series.add(serie=self.serie1)
        series.add(serie=self.serie_PH1)
        series.add(serie=self.serie_TH1)
        tmpfiles = series.to_GRPRT_Archive(dirname=out_dirname)
        self.assertListEqual(
            sorted(tmpfiles),
            ['data\\io\\grp16\\rt_archive\\07075001\\PV_10A.DAT',
             'data\\io\\grp16\\rt_archive\\07075001\\TV_10A.DAT',
             'data\\io\\grp16\\rt_archive\\K0000000\\QV_10A.DAT']
        )
        check_files(self, tmpfiles, ctl_dirname)
        # =====================================================================

    def test_GRP18(self):
        """
        Test export GRP18
        """
        # =====================================================================
        # GRP_Data : Cas AVEC mise-à-jour des données existantes
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='test_series_Q')
        shutil.copy2(
            os.path.join('data', 'io', 'grp18', 'K0000000_Q_ini.txt'),
            os.path.join('data', 'K0000000_Q.txt'),
        )
        series.add(serie=self.serie1i)
        series.add(serie=self.serie2i)
        series.add(serie=self.serie3i)
        series.add(serie=self.serie4i, meta='sim')
        tmpfiles = series.to_GRP_Data(
            dirname='data', version='2018', how='overwrite')
        self.assertListEqual(
            tmpfiles,
            ['data\\K0000000_Q.txt', 'data\\K9999999_Q.txt',
             'data\\K3333333_Q.txt']
        )
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp18'))
        # =====================================================================
        # GRP_Data : Cas SANS mise-à-jour des données existantes
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp18')
        series.add(serie=self.serie2i)
        shutil.copy2(
            os.path.join('data', 'io', 'grp18', 'K9999999_Q_ini.txt'),
            os.path.join('data', 'K9999999_Q.txt'),
        )
        tmpfiles = series.to_GRP_Data(
            dirname='data', version='2018', how='replace')
        self.assertListEqual(tmpfiles, ['data\\K9999999_Q.txt'])
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp18'))
        # =====================================================================
        # GRP_Data : Cas de séries avec pas de temps horaire
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp18')
        series.add(serie=self.serie_PH1)
        series.add(serie=self.serie_PH2)
        series.add(serie=self.serie_PH3)
        tmpfiles = series.to_GRP_Data(dirname='data', version='2018')
        tmpfiles = series.to_GRP_Data(
            dirname='data', version='2018', how='replace')
        self.assertListEqual(
            tmpfiles,
            ['data\\07075001_P_00J01H00M.txt',
             'data\\07105001_P_00J01H00M.txt',
             'data\\07154005_P_00J01H00M.txt'])
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp18'))
        # =====================================================================
        # GRP_Data : Cas de séries avec pas de temps journalier
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp18')
        series.add(serie=self.serie_PJ1)
        series.add(serie=self.serie_PJ2)
        series.add(serie=self.serie_PJ3)
        tmpfiles = series.to_GRP_Data(dirname='data', version='2018')
        tmpfiles = series.to_GRP_Data(
            dirname='data', version='2018', how='replace')
        self.assertListEqual(
            tmpfiles,
            ['data\\07075001_P_01J00H00M.txt',
             'data\\07105001_P_01J00H00M.txt',
             'data\\07154005_P_01J00H00M.txt'])
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp18'))
        # =====================================================================
        # GRP_RT_Data : Cas séries OBSERVATION
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp18')
        series.add(serie=self.serie1)
        series.add(serie=self.serie2i)
        series.add(serie=self.serie3)
        series.add(serie=self.serie4, meta='sim')
        series.add(serie=self.serie_PH1)
        series.add(serie=self.serie_PH2)
        series.add(serie=self.serie_PH3)
        tmpfiles = series.to_GRPRT_Data(dirname='data', version='2018')
        self.assertListEqual(
            sorted(tmpfiles),
            ['data\\Debitgrp18.txt', 'data\\Pluiegrp18_00J01H00M.txt']
        )
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp18'))
        # =====================================================================
        # GRP_RT_Data : Cas séries PREVISION METEOROLOGIQUE
        # ---------------------------------------------------------------------
        series = Series(datatype='fcst', name='grp18')
        series.add(serie=self.fcst_PH1,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        series.add(serie=self.fcst_PH2,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2002'))
        series.add(serie=self.fcst_PH3,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        series.add(serie=self.fcst_TH1,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        series.add(serie=self.fcst_TH2,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2002'))
        series.add(serie=self.fcst_TH3,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        tmpfiles = series.to_GRPRT_Metscen(dirname='data', version='2018')
        self.assertListEqual(
            sorted(tmpfiles),
            ['data\\ScenT_001_Temgrp18_00J01H00M.txt',
             'data\\ScenT_002_Temgrp18_00J01H00M.txt',
             'data\\Scen_001_Plugrp18_00J01H00M.txt',
             'data\\Scen_002_Plugrp18_00J01H00M.txt']
        )
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp18'))
        # =====================================================================

    def test_GRP20(self):
        """
        Test export GRP20
        """
        # =====================================================================
        # GRP_Data : Cas AVEC mise-à-jour des données existantes
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='test_series_Q')
        shutil.copy2(
            os.path.join('data', 'io', 'grp20', 'K0000000_Q_ini.txt'),
            os.path.join('data', 'K0000000_Q.txt'),
        )
        series.add(serie=self.serie1i)
        series.add(serie=self.serie2i)
        series.add(serie=self.serie3i)
        series.add(serie=self.serie4i, meta='sim')
        tmpfiles = series.to_GRP_Data(
            dirname='data', version='2020', how='overwrite')
        self.assertListEqual(
            tmpfiles,
            ['data\\K0000000_Q.txt', 'data\\K9999999_Q.txt',
             'data\\K3333333_Q.txt']
        )
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp20'))
        # =====================================================================
        # GRP_Data : Cas SANS mise-à-jour des données existantes
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp20')
        series.add(serie=self.serie2i)
        shutil.copy2(
            os.path.join('data', 'io', 'grp20', 'K9999999_Q_ini.txt'),
            os.path.join('data', 'K9999999_Q.txt'),
        )
        tmpfiles = series.to_GRP_Data(
            dirname='data', version='2020', how='replace')
        self.assertListEqual(tmpfiles, ['data\\K9999999_Q.txt'])
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp20'))
        # =====================================================================
        # GRP_Data : Cas de séries avec pas de temps horaire
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp20')
        series.add(serie=self.serie_PH1)
        series.add(serie=self.serie_PH2)
        series.add(serie=self.serie_PH3)
        tmpfiles = series.to_GRP_Data(dirname='data', version='2020')
        tmpfiles = series.to_GRP_Data(
            dirname='data', version='2020', how='replace')
        self.assertListEqual(
            tmpfiles,
            ['data\\07075001_P_00J01H00M.txt',
             'data\\07105001_P_00J01H00M.txt',
             'data\\07154005_P_00J01H00M.txt'])
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp20'))
        # =====================================================================
        # GRP_Data : Cas de séries avec pas de temps journalier
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp20')
        series.add(serie=self.serie_PJ1)
        series.add(serie=self.serie_PJ2)
        series.add(serie=self.serie_PJ3)
        tmpfiles = series.to_GRP_Data(dirname='data', version='2020')
        tmpfiles = series.to_GRP_Data(
            dirname='data', version='2020', how='replace')
        self.assertListEqual(
            tmpfiles,
            ['data\\07075001_P_01J00H00M.txt',
             'data\\07105001_P_01J00H00M.txt',
             'data\\07154005_P_01J00H00M.txt'])
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp20'))
        # =====================================================================
        # GRP_RT_Data : Cas séries OBSERVATION
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp20')
        series.add(serie=self.serie1)
        series.add(serie=self.serie2i)
        series.add(serie=self.serie3)
        series.add(serie=self.serie4, meta='sim')
        series.add(serie=self.serie_PH1)
        series.add(serie=self.serie_PH2)
        series.add(serie=self.serie_PH3)
        tmpfiles = series.to_GRPRT_Data(dirname='data', version='2020')
        self.assertListEqual(
            sorted(tmpfiles),
            ['data\\Debitgrp20.txt', 'data\\Pluiegrp20_00J01H00M.txt']
        )
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp20'))
        # =====================================================================
        # GRP_RT_Data : Cas séries PREVISION METEOROLOGIQUE
        # ---------------------------------------------------------------------
        series = Series(datatype='fcst', name='grp20')
        series.add(serie=self.fcst_PH1,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        series.add(serie=self.fcst_PH2,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2002'))
        series.add(serie=self.fcst_PH3,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        series.add(serie=self.fcst_TH1,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        series.add(serie=self.fcst_TH2,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2002'))
        series.add(serie=self.fcst_TH3,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        tmpfiles = series.to_GRPRT_Metscen(dirname='data', version='2020')
        self.assertListEqual(
            sorted(tmpfiles),
            ['data\\ScenT_0001_Temgrp20_00J01H00M.txt',
             'data\\ScenT_0002_Temgrp20_00J01H00M.txt',
             'data\\Scen_0001_Plugrp20_00J01H00M.txt',
             'data\\Scen_0002_Plugrp20_00J01H00M.txt']
        )
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp20'))
        # =====================================================================
        # GRP_RT_Archive : Cas séries ARCHIVE METEOROLOGIQUE
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp20')
        series.add(serie=self.serie1)
        series.add(serie=self.serie2i)
        series.add(serie=self.serie_PH1)
        series.add(serie=self.serie_TH1)
        tmpfiles = series.to_GRPRT_Archive(dirname='data', version='2020')
        self.assertListEqual(
            sorted(tmpfiles),
            ['data\\PV_00J01H00M_2014.DAT', 'data\\QV_00J01H00M_2014.DAT',
             'data\\QV_2014.DAT', 'data\\TV_00J01H00M_2014.DAT']
        )
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp20'))
        # =====================================================================

    def test_GRP22(self):
        """
        Test export GRP22
        """
        # =====================================================================
        # GRP_Data : Cas AVEC mise-à-jour des données existantes
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='test_series_Q')
        shutil.copy2(
            os.path.join('data', 'io', 'grp22', 'K0000000_Q_ini.txt'),
            os.path.join('data', 'K0000000_Q.txt'),
        )
        series.add(serie=self.serie1i)
        series.add(serie=self.serie2i)
        series.add(serie=self.serie3i)
        series.add(serie=self.serie4i, meta='sim')
        tmpfiles = series.to_GRP_Data(
            dirname='data', version='2020', how='overwrite')
        self.assertListEqual(
            tmpfiles,
            ['data\\K0000000_Q.txt', 'data\\K9999999_Q.txt',
             'data\\K3333333_Q.txt']
        )
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp22'))
        # =====================================================================
        # GRP_Data : Cas SANS mise-à-jour des données existantes
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp22')
        series.add(serie=self.serie2i)
        shutil.copy2(
            os.path.join('data', 'io', 'grp22', 'K9999999_Q_ini.txt'),
            os.path.join('data', 'K9999999_Q.txt'),
        )
        tmpfiles = series.to_GRP_Data(
            dirname='data', version='2020', how='replace')
        self.assertListEqual(tmpfiles, ['data\\K9999999_Q.txt'])
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp22'))
        # =====================================================================
        # GRP_Data : Cas de séries avec pas de temps horaire
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp22')
        series.add(serie=self.serie_PH1)
        series.add(serie=self.serie_PH2)
        series.add(serie=self.serie_PH3)
        tmpfiles = series.to_GRP_Data(dirname='data', version='2020')
        tmpfiles = series.to_GRP_Data(
            dirname='data', version='2020', how='replace')
        self.assertListEqual(
            tmpfiles,
            ['data\\07075001_P_00J01H00M.txt',
             'data\\07105001_P_00J01H00M.txt',
             'data\\07154005_P_00J01H00M.txt'])
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp22'))
        # =====================================================================
        # GRP_Data : Cas de séries avec pas de temps journalier
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp22')
        series.add(serie=self.serie_PJ1)
        series.add(serie=self.serie_PJ2)
        series.add(serie=self.serie_PJ3)
        tmpfiles = series.to_GRP_Data(dirname='data', version='2020')
        tmpfiles = series.to_GRP_Data(
            dirname='data', version='2020', how='replace')
        self.assertListEqual(
            tmpfiles,
            ['data\\07075001_P_01J00H00M.txt',
             'data\\07105001_P_01J00H00M.txt',
             'data\\07154005_P_01J00H00M.txt'])
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp22'))
        # =====================================================================
        # GRP_RT_Data : Cas séries OBSERVATION
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp22')
        series.add(serie=self.serie1)
        series.add(serie=self.serie2i)
        series.add(serie=self.serie3)
        series.add(serie=self.serie4, meta='sim')
        series.add(serie=self.serie_PH1)
        series.add(serie=self.serie_PH2)
        series.add(serie=self.serie_PH3)
        tmpfiles = series.to_GRPRT_Data(dirname='data', version='2020')
        self.assertListEqual(
            sorted(tmpfiles),
            ['data\\Debitgrp22.txt', 'data\\Pluiegrp22_00J01H00M.txt']
        )
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp22'))
        # =====================================================================
        # GRP_RT_Data : Cas séries PREVISION METEOROLOGIQUE
        # ---------------------------------------------------------------------
        series = Series(datatype='fcst', name='grp22')
        series.add(serie=self.fcst_PH1,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        series.add(serie=self.fcst_PH2,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2002'))
        series.add(serie=self.fcst_PH3,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        series.add(serie=self.fcst_TH1,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        series.add(serie=self.fcst_TH2,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2002'))
        series.add(serie=self.fcst_TH3,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        tmpfiles = series.to_GRPRT_Metscen(dirname='data', version='2020')
        self.assertListEqual(
            sorted(tmpfiles),
            ['data\\ScenT_0001_Temgrp22_00J01H00M.txt',
             'data\\ScenT_0002_Temgrp22_00J01H00M.txt',
             'data\\Scen_0001_Plugrp22_00J01H00M.txt',
             'data\\Scen_0002_Plugrp22_00J01H00M.txt']
        )
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp22'))
        # =====================================================================
        # GRP_RT_Archive : Cas séries ARCHIVE METEOROLOGIQUE
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='grp22')
        series.add(serie=self.serie1)
        series.add(serie=self.serie2i)
        series.add(serie=self.serie_PH1)
        series.add(serie=self.serie_TH1)
        tmpfiles = series.to_GRPRT_Archive(dirname='data', version='2022')
        self.assertListEqual(
            sorted(tmpfiles),
            ['data\\PV_00J01H00M_2014.DAT', 'data\\QV_00J01H00M_2014.DAT',
             'data\\QV_2014.DAT', 'data\\TV_00J01H00M_2014.DAT']
        )
        check_files(self, tmpfiles, os.path.join('data', 'io', 'grp22'))
        # =====================================================================

    def test_MF_Data(self):
        """
        Test export MF_Data
        """
        # =====================================================================
        series = Series(datatype='obs', name='test_MF_Data')
        series.add(serie=self.serie_PJ1)
        series.add(serie=self.serie_PJ2)
        series.add(serie=self.serie_PJ3)
        tmpfile = series.to_MF_Data(dirname='data')
        filename = os.path.join(
            'data', 'io', 'meteofrance', os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        series = Series(datatype='fcst', name='test_MF_Data_fcst')
        series.add(serie=self.serie_PJ1, meta=(dt(2013, 5, 10), '1'))
        series.add(serie=self.serie_PJ2, meta=(dt(2013, 5, 10), '2'))
        series.add(serie=self.serie_PJ2, meta=(dt(2013, 5, 10), '3'))
        tmpfile = series.to_MF_Data(dirname='data', asobs=False)
        filename = os.path.join(
            'data', 'io', 'meteofrance', os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        series = Series(datatype='fcst', name='test_MF_Data_asobs')
        series.add(serie=self.serie_PJ1, meta=(dt(2013, 5, 10), '1'))
        series.add(serie=self.serie_PJ2, meta=(dt(2013, 5, 10), '2'))
        series.add(serie=self.serie_PJ2, meta=(dt(2013, 5, 10), '3'))
        tmpfile = series.to_MF_Data(dirname='data', asobs=True)
        filename = os.path.join(
            'data', 'io', 'meteofrance', os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_PLATHYNES_Data(self):
        """
        Test export fichier d'observation pour PLATHYNES
        """
        # =====================================================================
        series = Series(datatype='obs', name='test_plathynes')
        injections = {'K9999999': True}
        suffix = {'K0000000': '1', 'K3333333': '2', 'K9999999': '1'}
        series.add(serie=self.serie1)  # 1
        series.add(serie=self.serie2)  # 1 + injection
        series.add(serie=self.serie3)  # 2
        series.add(serie=self.serie_PH1)
        series.add(serie=self.serie_PH2)
        series.add(serie=self.serie_PH3)
        # =====================================================================
        tmpfiles = series.to_PLATHYNES_Data(
            dirname='data', injections=injections, suffix=suffix)
        check_files(self, tmpfiles, os.path.join('data', 'io', 'plathynes'))
        # =====================================================================

    def test_Prevision19(self):
        """
        Test export Prevision19
        """
        # =====================================================================
        source = os.path.join('data', 'prevision_2019.mdb')
        if not os.path.exists(source):
            src_filename = os.path.join(
                os.path.dirname(__file__), '..',
                'resources', 'dbase', 'Prevision_2019.mdb'
            )
            shutil.copy2(src_filename, source)
        # =====================================================================
        # CAS : PREVISION BRUTE
        # ---------------------------------------------------------------------
        valid = {
            ('K0000000', dt(2014, 11, 4), 2001, 'scen1'): {
                'series': 1,
                'values': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
            },
            ('K0000000', dt(2014, 11, 4), 2002, 'scen2'): {
                'series': 2,
                'values': [14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26]
            },
            ('K9999999', dt(2014, 11, 4), 2001, 'scen1'): {
                'series': 3,
                'values': [27, 28, 29, 30, 31, 32, 33, 34, 35, 36]
            },
        }
        series = Series(datatype='fcst', name='Prevision19')
        series.add(serie=self.fcst_11,
                   meta=(dt(2014, 11, 4), 2001, 'scen1', '10'))
        series.add(serie=self.fcst_15,
                   meta=(dt(2014, 11, 4), 2001, 'scen1', '50'))
        series.add(serie=self.fcst_19,
                   meta=(dt(2014, 11, 4), 2001, 'scen1', '90'))
        series.add(serie=self.fcst_25,
                   meta=(dt(2014, 11, 4), 2002, 'scen2', '50'))
        series.add(serie=self.fcst_35,
                   meta=(dt(2014, 11, 4), 2001, 'scen1', None))
        rows = series.to_Prevision19(filename=source)
        self.assertEqual(rows, valid)
        # =====================================================================
        os.remove(source)
        # =====================================================================

    def test_prv(self):
        """
        Test export prv
        """
        # =====================================================================
        dirname = os.path.join('data', 'io', 'prv')
        # =====================================================================
        # CAS : PREVISION OTAMIN
        # ---------------------------------------------------------------------
        datatype = 'otamin16_fcst'
        series = Series(datatype='fcst', name='test_{}'.format(datatype))
        series.add(serie=self.fcst_15,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', None))
        series.add(serie=self.fcst_25,
                   meta=(dt(2014, 11, 4), 'model1', 'scen2', None))
        series.add(serie=self.fcst_35,
                   meta=(dt(2014, 11, 4), 'model2', 'scen1', None))
        tmpfile = series.to_prv(dirname='data', datatype=datatype)
        filename = os.path.join(dirname, os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        # CAS : PREVISION OTAMIN - CAS RR,TA
        # ---------------------------------------------------------------------
        datatype = 'otamin16_fcst'
        series = Series(datatype='fcst', name='test_{}_RRTA'.format(datatype))
        series.add(serie=self.fcst_PH1,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        series.add(serie=self.fcst_PH2,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2001'))
        series.add(serie=self.fcst_PH3,
                   meta=(dt(2014, 11, 4, 12), 'foo', '2002'))
        tmpfile = series.to_prv(dirname='data', datatype=datatype)
        filename = os.path.join(dirname, os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        # CAS : TENDANCE OTAMIN
        # ---------------------------------------------------------------------
        datatype = 'otamin16_trend'
        with self.assertRaises(ValueError):
            series.to_prv(dirname='data', datatype=datatype)
        series = Series(datatype='fcst', name='test_{}'.format(datatype))
        series.add(serie=self.fcst_11,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', '10'))
        series.add(serie=self.fcst_15,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', '50'))
        series.add(serie=self.fcst_19,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', '90'))
        series.add(serie=self.fcst_25,
                   meta=(dt(2014, 11, 4), 'model1', 'scen2', '50'))
        series.add(serie=self.fcst_35,
                   meta=(dt(2014, 11, 4), 'model2', 'scen1', None))
        tmpfile = series.to_prv(dirname='data', datatype=datatype)
        filename = os.path.join(dirname, os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        # CAS : OBSERVATION SCORES
        # ---------------------------------------------------------------------
        datatype = 'scores_obs'
        with self.assertRaises(ValueError):
            series.to_prv(dirname='data', datatype=datatype)
        series = Series(datatype='obs', name='test_{}'.format(datatype))
        series.add(serie=self.serie1)
        series.add(serie=self.serie2)
        series.add(serie=self.serie3)
        tmpfile = series.to_prv(dirname='data', datatype=datatype)
        filename = os.path.join(dirname, os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        # CAS : SIMULATION SCORES
        # ---------------------------------------------------------------------
        datatype = 'scores_sim'
        series = Series(datatype='obs', name='test_{}'.format(datatype))
        series.add(serie=self.serie1, meta='sim1')
        series.add(serie=self.serie2, meta='sim2')
        series.add(serie=self.serie3, meta='sim3')
        tmpfile = series.to_prv(dirname='data', datatype=datatype)
        filename = os.path.join(dirname, os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        # CAS : PREVISION SCORES
        # ---------------------------------------------------------------------
        datatype = 'scores_fcst'
        series = Series(datatype='fcst', name='test_{}'.format(datatype))
        series.add(serie=self.fcst_15,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', None))
        series.add(serie=self.fcst_25,
                   meta=(dt(2014, 11, 4), 'model1', 'scen2', None))
        series.add(serie=self.fcst_35,
                   meta=(dt(2014, 11, 4), 'model2', 'scen1', None))
        tmpfile = series.to_prv(dirname='data', datatype=datatype)
        filename = os.path.join(dirname, os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        # CAS : PREVISION SCORES avec TENDANCE
        # ---------------------------------------------------------------------
        datatype = 'scores_fcst'
        series = Series(datatype='fcst', name='test_{}_trend'.format(datatype))
        series.add(serie=self.fcst_11,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', '10'))
        series.add(serie=self.fcst_15,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', '50'))
        series.add(serie=self.fcst_19,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', '90'))
        series.add(serie=self.fcst_25,
                   meta=(dt(2014, 11, 4), 'model1', 'scen2', '50'))
        series.add(serie=self.fcst_35,
                   meta=(dt(2014, 11, 4), 'model2', 'scen1', None))
        tmpfile = series.to_prv(dirname='data', datatype=datatype)
        filename = os.path.join(dirname, os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_PyspcFile(self):
        """
        Test export PyspcFile
        """
        # =====================================================================
        dirname = os.path.join('data', 'io', 'pyspcfile')
        # =====================================================================
        # CAS : MULTIPLE FILES + OBS/SIM
        # ['.\\data\\K0000000_QH.txt',
        #  '.\\data\\K9999999_QI.txt',
        #  '.\\data\\K3333333_sim3_QH.txt']
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='test_multi_obssim')
        series.add(serie=self.serie1)
        series.add(serie=self.serie2i)
        series.add(serie=self.serie3, meta='sim3')
        tmpfiles = series.to_PyspcFile(dirname='data')
        check_files(self, tmpfiles, dirname)
        # =====================================================================
        # CAS : MULTIPLE FILES + FCST
        # ['.\\data\\K0000000_2014110400_model1_scen1_10_QH.txt',
        #  '.\\data\\K0000000_2014110400_model1_scen1_50_QH.txt',
        #  '.\\data\\K0000000_2014110400_model1_scen1_90_QH.txt']
        # ---------------------------------------------------------------------
        series = Series(datatype='fcst', name='test_multi_fcst')
        series.add(serie=self.fcst_11,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', '10'))
        series.add(serie=self.fcst_15,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', '50'))
        series.add(serie=self.fcst_19,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', '90'))
        tmpfiles = series.to_PyspcFile(dirname='data')
        check_files(self, tmpfiles, dirname)
        # =====================================================================
        # CAS : MULTIPLE FILES + EXPLICITCOLNAME
        # ['.\\data\\K0000000_sim1_QH.txt',
        #  '.\\data\\K9999999_sim2_QI.txt']
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='test_multi_EXPLICITCOLNAME')
        series.add(serie=self.serie1, meta='sim1')
        series.add(serie=self.serie2i, meta='sim2')
        tmpfiles = series.to_PyspcFile(dirname='data', explicitcolname=True)
        check_files(self, tmpfiles, dirname)
        # =====================================================================
        # CAS : MULTIPLE FILES + HOW overwrite
        # ['.\\data\\07075001_PH.txt']
        # ---------------------------------------------------------------------
        shutil.copy2(
            os.path.join('data', 'io', 'pyspcfile', 'ini_07075001_PH.txt'),
            os.path.join('data', '07075001_PH.txt'),
        )
        series = Series(datatype='obs', name='test_multi_HOW_overwrite')
        series.add(serie=self.serie_PH1)
        tmpfiles = series.to_PyspcFile(dirname='data', how='overwrite')
        check_files(self, tmpfiles, dirname)
        # =====================================================================
        # CAS : MULTIPLE FILES + HOW fillna
        # ['.\\data\\07105001_PH.txt']
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='test_multi_HOW_fillna')
        shutil.copy2(
            os.path.join('data', 'io', 'pyspcfile', 'ini_07105001_PH.txt'),
            os.path.join('data', '07105001_PH.txt'),
        )
        series = Series(datatype='obs', name='test_multi_HOW_overwrite')
        series.add(serie=self.serie_PH2)
        tmpfiles = series.to_PyspcFile(dirname='data', how='fillna')
        check_files(self, tmpfiles, dirname)
        # =====================================================================
        # CAS : ONE FILE + MULTI-TIMESTEPS -> ValueError
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='test_onefile_valueerror')
        series.add(serie=self.serie1)
        series.add(serie=self.serie2i)
        with self.assertRaises(ValueError):
            series.to_PyspcFile(dirname='data', onefile=True)
        # =====================================================================
        # CAS : ONE FILE + OBS/SIM + 1 LOC + 1 VAR
        # ['.\\data\\K9999999_QH.txt']
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='testOnefileObssimOnelocOnevar')
        series.add(serie=self.serie2)
        series.add(serie=self.serie2, meta='sim2')
        tmpfiles = series.to_PyspcFile(dirname='data', onefile=True)
        check_files(self, tmpfiles, dirname)
        # =====================================================================
        # CAS : ONE FILE + OBS/SIM + 1 LOC + N VAR
        # ['.\\data\\testOnefileObssimOnelocNvar_TH.txt']
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='testOnefileObssimOnelocNvar')
        series.add(serie=self.serie_PH1)
        series.add(serie=self.serie_TH1)
        tmpfiles = series.to_PyspcFile(dirname='data', onefile=True)
        check_files(self, tmpfiles, dirname)
        # =====================================================================
        # CAS : ONE FILE + OBS/SIM + 1 LOC + EXPLICITCOLNAME
        # ['.\\data\\testOnefileObssimOnelocExplicit_QH.txt']
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='testOnefileObssimOnelocExplicit')
        series.add(serie=self.serie2)
        series.add(serie=self.serie2, meta='sim2')
        tmpfiles = series.to_PyspcFile(
            dirname='data', onefile=True, explicitcolname=True)
        check_files(self, tmpfiles, dirname)
        # =====================================================================
        # CAS : ONE FILE + OBS/SIM + N LOC + CODE
        # ['.\\data\\bassinK_QH.txt']
        # ---------------------------------------------------------------------
        series = Series(datatype='obs', name='testOnefileObssimNlocCode')
        series.add(serie=self.serie1)
        series.add(serie=self.serie2)
        tmpfiles = series.to_PyspcFile(dirname='data', onefile=True,
                                       code='bassinK')
        check_files(self, tmpfiles, dirname)
        # =====================================================================
        # CAS : MULTIPLE FILES + FCST
        # ['.\\data\\K0000000_2014110400_model1_scen1_QH.txt',
        #  '.\\data\\K0000000_2014110400_model1_scen2_QH.txt',
        #  '.\\data\\K9999999_2014110400_model2_scen1_QH.txt']
        # ---------------------------------------------------------------------
        series = Series(datatype='fcst', name='testMultifileFcst')
        series.add(serie=self.fcst_15,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', None))
        series.add(serie=self.fcst_25,
                   meta=(dt(2014, 11, 4), 'model1', 'scen2', None))
        series.add(serie=self.fcst_35,
                   meta=(dt(2014, 11, 4), 'model2', 'scen1', None))
        tmpfiles = series.to_PyspcFile(dirname='data')
        check_files(self, tmpfiles, dirname)
        # =====================================================================
        # CAS : ONE FILE + FCST (no common)
        # ['.\\data\\testOnefileFcstNocommon_QH.txt']
        # ---------------------------------------------------------------------
        series = Series(datatype='fcst', name='testOnefileFcstNocommon')
        series.add(serie=self.fcst_15,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', None))
        series.add(serie=self.fcst_25,
                   meta=(dt(2014, 11, 4), 'model1', 'scen2', None))
        series.add(serie=self.fcst_35,
                   meta=(dt(2014, 11, 4, 3), 'model2', 'scen1', None))
        tmpfiles = series.to_PyspcFile(dirname='data', onefile=True)
        check_files(self, tmpfiles, dirname)
        # =====================================================================
        # CAS : ONE FILE + FCST (commun=run)
        # ['.\\data\\K0000000_2014110400_QH.txt']
        # ---------------------------------------------------------------------
        series = Series(datatype='fcst', name='testOnefileFcstCommonrun')
        series.add(serie=self.fcst_15,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', None))
        series.add(serie=self.fcst_25,
                   meta=(dt(2014, 11, 4), 'model2', 'scen2', None))
        tmpfiles = series.to_PyspcFile(dirname='data', onefile=True)
        check_files(self, tmpfiles, dirname)
        # =====================================================================
        # CAS : ONE FILE + FCST (commun=model)
        # ['.\\data\\K0000000_2014110400_model1_QH.txt']
        # ---------------------------------------------------------------------
        series = Series(datatype='fcst', name='testOnefileFcstCommonmodel')
        series.add(serie=self.fcst_15,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', None))
        series.add(serie=self.fcst_25,
                   meta=(dt(2014, 11, 4), 'model1', 'scen2', None))
        tmpfiles = series.to_PyspcFile(dirname='data', onefile=True)
        check_files(self, tmpfiles, dirname)
        # =====================================================================
        # CAS : ONE FILE + FCST (commun=scen)
        # ['.\\data\\K0000000_2014110400_model1_scen3_QH.txt']
        # ---------------------------------------------------------------------
        series = Series(datatype='fcst', name='testOnefileFcstCommonscen')
        series = Series(datatype='fcst', name='testOnefileFcstCommonmodel')
        series.add(serie=self.fcst_11,
                   meta=(dt(2014, 11, 4), 'model1', 'scen3', '10'))
        series.add(serie=self.fcst_15,
                   meta=(dt(2014, 11, 4), 'model1', 'scen3', '50'))
        series.add(serie=self.fcst_19,
                   meta=(dt(2014, 11, 4), 'model1', 'scen3', '90'))
        series.add(serie=self.fcst_25,
                   meta=(dt(2014, 11, 4), 'model1', 'scen3', None))
        tmpfiles = series.to_PyspcFile(dirname='data', onefile=True)
        check_files(self, tmpfiles, dirname)
        # =====================================================================

    def test_Sandre(self):
        """
        Test export Sandre
        """
        # =====================================================================
        sender = 'me'
        user = 'org'
        target = 'you'
        # =====================================================================
        datatype = 'data_obs_hydro'
        runtime = dt(2014, 11, 4, 12)
        series = Series(datatype='obs', name='test_series_Q')
        series.add(serie=self.serie1)
        series.add(serie=self.serie2)
        series.add(serie=self.serie3)
        series.add(serie=self.serie4)
        tmpfile = series.to_Sandre(
            dirname='data', datatype=datatype, runtime=runtime,
            sender=sender, user=user, target=target)
        filename = os.path.join(
            'data', 'io', 'sandre', os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        datatype = 'data_obs_meteo'
        runtime = dt(2013, 5, 20)
        series = Series(datatype='obs', name='test_series_RR')
        series.add(serie=self.serie_PJ1)
        series.add(serie=self.serie_PJ2)
        series.add(serie=self.serie_PJ3)
        tmpfile = series.to_Sandre(
            dirname='data', datatype=datatype, runtime=runtime,
            sender=sender, user=user, target=target)
        filename = os.path.join(
            'data', 'io', 'sandre', os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================
        datatype = 'data_fcst_hydro'
        runtime = dt(2014, 11, 4)
        series = Series(datatype='fcst', name='test_series_fcst')
        series.add(serie=self.fcst_11,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', '10'))
        series.add(serie=self.fcst_15,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', '50'))
        series.add(serie=self.fcst_19,
                   meta=(dt(2014, 11, 4), 'model1', 'scen1', '90'))
        series.add(serie=self.fcst_25,
                   meta=(dt(2014, 11, 4), 'model1', 'scen2', '50'))
        series.add(serie=self.fcst_25,
                   meta=(dt(2014, 11, 4), 'model2', 'scen1', '50'))
        tmpfile = series.to_Sandre(
            dirname='data', datatype=datatype, runtime=runtime,
            sender=sender, user=user, target=target)
        filename = os.path.join(
            'data', 'io', 'sandre', os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(
            filename,
            tmpfile
        ))
        os.remove(tmpfile)
        # =====================================================================

    def test_xls(self):
        """
        Test export xls
        """
        # =====================================================================
        series = Series(datatype='obs', name='test_xls_obs')
        series.add(serie=self.serie1)
        series.add(serie=self.serie2)
        series.add(serie=self.serie3)
        series.add(serie=self.serie4, meta='sim')
        tmpfile = series.to_xls(dirname='data')
        filename = os.path.join('data', 'io', 'csv', os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(filename, tmpfile))
        os.remove(tmpfile)
        # =====================================================================
        series = Series(datatype='fcst', name='test_xls_fcst')
        series.add(serie=self.fcst_PJ1,
                   meta=(dt(2013, 5, 15), 'foo', 'MoyInf'))
        series.add(serie=self.fcst_PJ2,
                   meta=(dt(2013, 5, 15), 'foo', 'MoySup'))
        series.add(serie=self.fcst_PJ3,
                   meta=(dt(2013, 5, 15), 'foo', 'LocInf'))
        series.add(serie=self.fcst_PJ1,
                   meta=(dt(2013, 5, 15), 'bar', 'MoyInf'))
        series.add(serie=self.fcst_PJ3,
                   meta=(dt(2013, 5, 15), 'bar', 'MoySup'))
        tmpfile = series.to_xls(dirname='data')
        filename = os.path.join('data', 'io', 'csv', os.path.basename(tmpfile))
        self.assertTrue(filecmp.cmp(filename, tmpfile))
        os.remove(tmpfile)
        # =====================================================================
