#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pySPC>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for Reservoir and Table in pyspc.core.reservoir

To run all tests just type:
    python -m unittest test_core_Reservoir

To run only a class test:
    python -m unittest test_core_Reservoir.TestReservoir

To run only a specific test:
    python -m unittest test_core_Reservoir.TestReservoir.test_init

"""
# Imports
import filecmp
import os.path
import pandas as pnd
from pandas.util.testing import assert_frame_equal
import unittest

from pyspc.core.reservoir import Reservoir, Table


class TestReservoir(unittest.TestCase):
    """
    Reservoir class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'core', 'reservoir')

    def test_init(self):
        """Test init"""
        # =====================================================================
        reservoir = Reservoir()
        self.assertIsNone(reservoir.code)
        self.assertIsNone(reservoir.name)
        self.assertIsNone(reservoir.tables)
        self.assertIsNone(reservoir.Z0)
        # =====================================================================
        code = 'K001002010'
        name = 'Barrage-de-la-Palisse'
        Z0 = 1010
        reservoir = Reservoir(code=code, name=name, Z0=Z0)
        self.assertEqual(reservoir.code, code)
        self.assertEqual(reservoir.name, name)
        self.assertEqual(reservoir.Z0, Z0)
        # =====================================================================

    def test_load(self):
        """Test init par fichier de config"""
        # =====================================================================
        code = 'K001002010'
        name = 'Barrage-de-la-Palisse'
        Z0 = 1010
        # =====================================================================
        filename = os.path.join(self.dirname, 'reservoir.txt')
        reservoir = Reservoir()
        reservoir.load(filename=filename)
        self.assertIsInstance(reservoir, Reservoir)
        self.assertEqual(reservoir.code, code)
        self.assertEqual(reservoir.name, name)
        self.assertEqual(reservoir.Z0, Z0)
        self.assertIsInstance(reservoir.tables, dict)
        self.assertIn('llp', reservoir.tables)
        self.assertIsInstance(reservoir.tables['llp'], Table)
        # =====================================================================


class TestTable(unittest.TestCase):
    """
    Table class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        self.dirname = os.path.join('data', 'core', 'reservoir')
        self.classic = {
            'Z': [
                280.80, 281.80, 282.80, 283.80, 284.80, 285.80, 286.80,
                287.80, 288.80, 289.80, 290.80, 291.80, 292.80, 293.80,
                294.80, 295.80, 296.80, 297.80, 298.80, 299.80, 300.80,
                301.80, 302.80
            ],
            'Qd': [
                0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
                0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
                0.000, 0.000, 0.564, 226.062, 630.306, 1179.923, 1874.914
            ],
            'Vd1': [
                0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
                0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
                0.000, 0.000, 0.002, 0.814, 2.269, 4.248, 6.750
            ],
            'V': [
                4.155, 4.663, 5.214, 5.809, 6.448, 7.130, 7.856, 8.626,
                9.440, 10.298, 11.199, 12.144, 13.133, 14.165, 15.241,
                16.361, 17.525, 18.732, 19.983, 21.278, 22.617, 23.999, 25.425
            ],
            'V+0.5*Vd1': [
                4.155, 4.663, 5.214, 5.809, 6.448, 7.130, 7.856, 8.626,
                9.440, 10.298, 11.199, 12.144, 13.133, 14.165, 15.241,
                16.361, 17.525, 18.732, 19.984, 21.685, 23.751, 26.123, 28.800
            ]
        }
        self.classic = pnd.DataFrame(self.classic)

    def test_init(self):
        """
        Test de la création de l'instance
        """
        # =====================================================================
        bareme = Table(datatype='table')
        self.assertIsNone(bareme.filename)
        self.assertIsNone(bareme.table)
        self.assertIsNone(bareme.cols)
        self.assertIsNone(bareme.assoc)
        # =====================================================================
        filename = os.path.join(self.dirname, 'classic.txt')
        bareme = Table(filename=filename, datatype='table')
        self.assertEqual(bareme.filename, filename)
        self.assertIsNone(bareme.table)
        self.assertIsNone(bareme.cols)
        self.assertIsNone(bareme.assoc)
        # =====================================================================

    def test_apply_zdz(self):
        """Test Table.apply_zdz """
        # =====================================================================
        filename = os.path.join(self.dirname, 'zdz.txt')
        # =====================================================================
        zt = 809.10
        valid = {
            0.00: 0,
            0.10: 0,
            0.12: 20,
            0.15: 20,
            0.16: 40,
            0.19: 40,
            0.20: 60,
            0.24: 60,
            0.25: 100,
            0.30: 100,
            0.31: 150,
            0.35: 150,
            0.36: 200,
            0.50: 200
        }
        # =====================================================================
        bareme = Table(filename=filename, datatype='ZdZ')
        bareme.load()
        # =====================================================================
        for k, v in valid.items():
            c = bareme.apply_zdz(
                zt=zt,
                dzt=k
            )
            self.assertEqual(c, v)
        # =====================================================================

    def test_isover_zdz(self):
        """Test Table.isover_zdz"""
        # =====================================================================
        filename = os.path.join(self.dirname, 'zdz.txt')
        # =====================================================================
        zt = 809.10
        col = 200
        valid = {
            0.00: False,
            0.10: False,
            0.12: False,
            0.15: False,
            0.16: False,
            0.19: False,
            0.20: False,
            0.24: False,
            0.25: False,
            0.30: False,
            0.31: False,
            0.35: False,
            0.36: True,
            0.50: True
        }
        # =====================================================================
        bareme = Table(filename=filename, datatype='ZdZ')
        bareme.load()
        # =====================================================================
        for k, v in valid.items():
            c = bareme.isover_zdz(
                zt=zt,
                dzt=k,
                col=col
            )
            self.assertEqual(c, v)
        # =====================================================================

    def test_convert(self):
        """Test Table.convert """
        # =====================================================================
        ratios = {
            'Vd1': 1000000,
            'V': 1000000,
            'V+0.5*Vd1': 1000000
        }
        bareme = Table(datatype='table', ratios=ratios)
        bareme._table = self.classic
        bareme._cols = self.classic.columns
        # =====================================================================
        x = bareme.convert(value=200, col1='Qd', col2='Z')
        self.assertAlmostEqual(x, 299.68, places=2)
        # =====================================================================
        x = bareme.convert(value=299.68, col1='Z', col2='Qd')
        self.assertAlmostEqual(x, 199, places=2)
        # =====================================================================

    def test_find_line(self):
        """Test Table.find_line """
        # =====================================================================
        bareme = Table(datatype='table')
        bareme._table = self.classic
        bareme._cols = self.classic.columns
        valid = 630.306
        # =====================================================================
        x = bareme.find_line(value=300.800, col1='Z', col2='Qd')
        self.assertEqual(x, valid)
        # =====================================================================

    def test_plot_classic(self):
        """Test Table.plot - Classic"""
        # =====================================================================
        title = 'classic Table'
        fig_filename = os.path.join('data', 'classic.png')
        # =====================================================================
        bareme = Table(datatype='table')
        bareme._table = self.classic
        bareme._cols = self.classic.columns
        tmp_file = bareme.plot(filename=fig_filename, title=title)
        filename = os.path.join(self.dirname, os.path.basename(tmp_file))
        self.assertTrue(filecmp.cmp(
            filename,
            tmp_file
        ))
        os.remove(tmp_file)
        # =====================================================================

    def test_load_classic(self):
        """Test Table.read - Classic"""
        # =====================================================================
        cols = ['Z', 'Qd', 'Vd1', 'V', 'V+0.5*Vd1']
        assoc = {'Z': 'Z', 'Q': 'Qd', 'V': 'V+0.5*Vd1'}
        filename = os.path.join(self.dirname, 'classic.txt')
        # =====================================================================
        bareme = Table(filename=filename, datatype='table')
        bareme.load()
        self.assertListEqual(cols, list(bareme.cols))
        self.assertDictEqual(assoc, bareme.assoc)
        assert_frame_equal(bareme.table, self.classic)
        # =====================================================================

    def test_load_error(self):
        """Test Table.read - Error"""
        # =====================================================================
        bareme = Table(datatype='table')
        with self.assertRaises(ValueError):
            bareme.load()
        # =====================================================================

    def test_types(self):
        """
        Test des types de Tables de Reservoir
        """
        dtypes = ['table', 'ZdZ']
        self.assertEqual(Table.get_types(), dtypes)
